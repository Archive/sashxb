/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

   
 IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Implementation of the core extension

*****************************************************************/

#include <unistd.h>
#include "sashIActionRuntime.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "sashtools.h"
#include "SashLinux.h"
#include "security.h"
#include "secman.h"
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnomeui/gnome-pixmap.h>
#include <gnome.h>
#include <string.h>
#include "sashCollection.h"
#include "sashIGenericConstructor.h"

SASH_EXTENSION_IMPL(SashLinux, sashILinux, 
					SASHLINUX, "Linux", "Sash Linux");

SashLinux::SashLinux() : m_collectionConstructor(NULL)
{
  NS_INIT_ISUPPORTS();
  DEBUGMSG(linux, "Creating linux extension\n");
}

SashLinux::~SashLinux()
{
	 NS_IF_RELEASE(m_collectionConstructor);
}

NS_IMETHODIMP 
SashLinux::ProcessEvent() {
  return NS_OK;
}

NS_IMETHODIMP 
SashLinux::Cleanup() {
  return NS_OK;
}

NS_IMETHODIMP
SashLinux::Initialize(sashIActionRuntime *act, 
			   const char *registrationXml, 
			   const char *webGuid,
			   JSContext *cx, JSObject *obj)
{
  DEBUGMSG(linux, "initializing linux\n");


  // Save all of the initialization parameters
  m_rt = act;
  m_registration = registrationXml;
  m_webguid = webGuid;
  m_cx = cx;
  m_obj = obj;

  act->GetSecurityManager(&m_secMan);
  NewSashConstructor(m_rt, this, SASHCOLLECTION_CONTRACT_ID, 
					 SASHICOLLECTION_IID_STR, &m_collectionConstructor);
  return NS_OK;
}

static GtkWidget* file_selector = NULL;

static void store_filename(GtkWidget *button, gpointer user_data) {
	 char** filename = (char**) user_data;
	 *filename = gtk_file_selection_get_filename(GTK_FILE_SELECTION(file_selector));
	 gtk_main_quit();
}

gint app_close_callback(GtkWidget * widget, GdkEvent * event, void * data) {
	 gtk_main_quit();
	 return true;
}

NS_IMETHODIMP SashLinux::FileDialog(const char* def_name, char** retval) {
	 char* filename = NULL;
	 if (file_selector == NULL) {
		  file_selector = gtk_file_selection_new("Please select a file.");
		  gtk_window_set_modal(GTK_WINDOW(file_selector), true);
	 }
	 gtk_signal_connect(GTK_OBJECT(file_selector),
						"delete-event", GTK_SIGNAL_FUNC(app_close_callback), &filename);
	 gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(file_selector)->ok_button),
						"clicked", GTK_SIGNAL_FUNC(store_filename), &filename);
	 gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(file_selector)->cancel_button), "clicked",
						GTK_SIGNAL_FUNC(gtk_main_quit), &filename);
		  
	 if (def_name)
		  gtk_file_selection_set_filename(GTK_FILE_SELECTION(file_selector), def_name);
	 
	 gtk_widget_show(GTK_WIDGET(file_selector));
	 gtk_main();	
	 gtk_widget_hide(GTK_WIDGET(file_selector));
	 
	 *retval = filename ? XP_STRDUP(filename) : XP_STRDUP("");
	 
	 return NS_OK;
}

NS_IMETHODIMP SashLinux::Dialog(const char* title, const char* prompt, nsIVariant* button_list, nsIVariant* def_button, nsIVariant* icon, PRInt32* _retval) {
	 GtkWidget *pixmap, *hbox;
	 char* c;
	 int icon_type = VariantIsNumber(icon) ? (int) VariantGetNumber(icon) : -1;

	 if (icon_type == 2) c = gnome_pixmap_file("gnome-error.png");
	 else if (icon_type == 1) c = gnome_pixmap_file("gnome-warning.png");
	 else if (icon_type == 0) c = gnome_pixmap_file("gnome-info.png");
	 else c = gnome_pixmap_file("gnome-default-dlg.png");
	 if (c == NULL) return NS_ERROR_NULL_POINTER;
	 pixmap = gnome_pixmap_new_from_file(c);
	 if (pixmap == NULL) return NS_ERROR_NULL_POINTER;

	 int button = VariantIsNumber(def_button) ? (int) VariantGetNumber(def_button) : 0;
	 GtkWidget * label = gtk_label_new(prompt);

	 const gchar* stock_buttons[] = {GNOME_STOCK_BUTTON_OK, NULL};
	 const gchar** buttons;

	 if (VariantIsArray(button_list)) {
		  vector <string> buttonv;
		  VariantGetArray(button_list, buttonv);
		  int length = buttonv.size();
		  buttons = new const gchar*[length + 1];
		  for (int i = 0 ; i < length ; i++) {
			   string s = buttonv[i];
			   buttons[i] = strdup(s.c_str());
		  }
		  buttons[length] = NULL;
	 } else {
		  buttons = (const gchar**) stock_buttons;
	 }
	 hbox = gtk_hbox_new(false, 0);
	 gtk_box_pack_start(GTK_BOX(hbox), pixmap, FALSE, TRUE, 0);
	 gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);
	 gtk_widget_show(hbox);
	 gtk_widget_show(label);
	 gtk_widget_show(pixmap);
	 GtkWidget* d;
	 d = gnome_dialog_newv(title, buttons);
	 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), hbox, TRUE, TRUE, 0);

	 gnome_dialog_set_default(GNOME_DIALOG(d), button);

	 *_retval = gnome_dialog_run_and_close(GNOME_DIALOG(d));
	 return NS_OK;
}

NS_IMETHODIMP SashLinux::GetArguments(nsIVariant ** v) {
	 return m_rt->GetArguments(v);
}


/* readonly attribute sashIGenericConstructor Collection; */
NS_IMETHODIMP SashLinux::GetCollection(sashIGenericConstructor * *aCollection)
{
	 assert (m_collectionConstructor);
	 NS_ADDREF(m_collectionConstructor);
	 *aCollection = m_collectionConstructor;
	 return NS_OK;
}
