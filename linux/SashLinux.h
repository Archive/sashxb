/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Sash.Linux implementation class definition

*****************************************************************/

#ifndef SASHCORE_H
#define SASHCORE_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashILinux.h"

#include <string>
// {cbe9be22-45b5-41a7-9e5f-4d260e22724e}
#define SASHLINUX_CID {0xcbe9be22, 0x45b5, 0x41a7, \
                     {0x9e, 0x5f, 0x4d, 0x26, 0x0e, 0x22, 0x72, 0x4e}}
    
NS_DEFINE_CID(ksashLinuxCID, SASHLINUX_CID);

#define SASHLINUX_CONTRACT_ID "@gnome.org/SashXB/SashLinux;1"

class sashIGenericConstructor;

class SashLinux : public sashILinux,
				 public sashIExtension
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHILINUX
  
  SashLinux();
  virtual ~SashLinux();
 private:
  JSContext * m_cx;
  JSObject * m_obj;
  sashIActionRuntime * m_rt;
  string m_registration;
  string m_webguid;
  sashISecurityManager * m_secMan;
  sashIGenericConstructor *m_collectionConstructor;
};

#endif
