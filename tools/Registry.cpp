
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): AJ Shankar, Andrew Wu, Andrew Chatham

Hierarchical XML registry; used to store settings throughout sash

*****************************************************************/

// Registry header file
// Library class for editing sash weblication registry files
// AJ Shankar, Andrew Wu
// created June 2000
// updated July 2000

#include "Registry.h"
#include "sash_error.h"
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <stdlib.h>
#include <sys/stat.h>

#include <algorithm>
#include <cassert>
#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
#include "debugmsg.h"
#include "FileSystem.h"
#include "url_tools.h"
#include "config.h"
#include "sash_version.h"

using namespace std;
const char* Registry::s_VersionName = "version";
const char* Registry::s_DefaultValue = "(Default)";
const char* Registry::s_TagKey = "key";
const char* Registry::s_TagValue = "value";

const char* SashRegistryIdentifier = "SashRegistry";
const char* SashRegistryTopNode = "^Top";

static const char* RegistryValueTypeStrings_static[] = {
	 "string",
	 "number",
	 "array",
	 "binary",
	 "invalid"
};

const char **RegistryValueTypeStrings = RegistryValueTypeStrings_static;

Registry::Registry() {
	 CreateNewDoc();
}

bool Registry::OpenFile(const string& filename) {
	 m_Filename = filename;
	 if (FileSystem::FileExists(filename)) {
		  return OpenByFilename();
	 } else {
		  // new file
		  // make sure the directory exists
		  if (FileSystem::FolderExists(URLGetDirectory(filename))) {
			   CreateNewDoc();
			   return true;
		  } else {
			   return false;
		  }
	 }
  
}

Registry::~Registry() { }


// ------------- Key Manipulation

bool Registry::Rename(const string& key, const string& newname) {
	 XMLNode node = GetNode(key, false);
	 if (node.isNull()) return false;
	 node.setAttribute("name", newname.c_str());
	 return true;
}

bool Registry::CreateKey(const string& key) {
	 // GetNode will create the keys if necessary
	 XMLNode node = GetNode(key, true);
  
	 return !node.isNull();
}

bool Registry::DeleteKey(const string& key, bool delete_deep) {
	 XMLNode n = GetNode(key, false);

	 // node does not exist
	 if (n.isNull()) 
		  return false;

	 // node has children
	 if (! (n.getFirstChildNode().isNull()) && ! delete_deep)
		  return false;

	 XMLNode parent = n.getParent();
	 parent.removeChild(n);

	 // automagical memory management will clean up deleted node
  
	 return true;
}

vector<string> Registry::EnumNames(const string& key, const string& name) {
  DEBUGMSG(registry, "Getting Node %s\n", key.c_str());
	 XMLNode n = GetNode(key, false);
	 DEBUGMSG(registry, "got node\n");

	 vector<string> v;
	 if (n.isNull()) {
	   DEBUGMSG(registry, "Returning empty array\n");
		  return v;
	 }

	 vector<XMLNode> children = n.getChildNodes(name);
	 DEBUGMSG(registry, "node has %d children\n", children.size());
	 for (unsigned int k = 0; k < children.size(); k++) {
		  string temp =  GetAttributeInNode("name", children[k]).getString();
		  v.push_back(temp);
	 }

	 DEBUGMSG(registry, "Returning %d values:\n", v.size());
	 for (unsigned int i = 0; i < v.size(); i++) {
	   DEBUGMSG(registry, "value %d: %s\n", i, v[i].c_str());
	 }
	 return v;
}


vector<string> Registry::EnumKeys(const string& key) {
	 //returns names of keys
	 return EnumNames(key, s_TagKey);
}



// ------------- Value Manipulation

vector<string> Registry::EnumValues(const string& key) {
  DEBUGMSG(registry, "EnumValues on key %s\n", key.c_str());
	 //returns names of values
	 return EnumNames(key, s_TagValue);
}

void Registry::QueryValue(const string& key, const string& value_name, 
						  RegistryValue* v) {
	 string full_key(key);
  
	 // make sure /'s are uniform at beginning and end
	 if ((full_key.size() == 0) || full_key[full_key.size() - 1] != '\\') {
		  full_key += '\\';
	 }
  
	 // if the value name is empty, assume (Default) value node
	 full_key += (value_name == "" ? s_DefaultValue : value_name);
	 XMLNode node = GetNode(full_key, false);
	 //check if node exists
	 if (node.isNull()) {
		  //reset to default value
		  *v = RegistryValue();
		  return;
	 }
  
	 if (node.getName() == s_TagKey) {
		  Error("Default value found as key!");
	 }
  
	 XMLString type = GetAttributeInNode("type", node);
	 RegistryValueType rvt = QueryValueType(type);
  
	 XMLString data_string = node.getText();
  
	 v->SetValueType(rvt);
	 const char* buffer = data_string.c_str();
	 int length = data_string.getString().size();

	 if (buffer == NULL) {
		  Error("Data not present where expected.");
	 }

	 switch (rvt) {
    
	 case RVT_NUMBER:
		  v->m_Number = atof(buffer);
		  break;
    
	 case RVT_STRING:
		  v->m_String = HexToString((const unsigned char*) buffer, length);
    
		  break;
      
	 case RVT_BINARY:
		  v->m_Binary = new char[length/2];
		  memcpy(v->m_Binary, HexToString((const unsigned char*)buffer, length).c_str(), length/2);
		  v->m_BinaryLength = length/2;
		  break;
    
	 case RVT_ARRAY:
	 {
		  int num_strings = 0, string_length;
		  const char* pos = buffer;
		  int stuff = sscanf(pos, "%d ", &num_strings);
		  // bogus or empty array
		  if (stuff == 0 || num_strings == 0) break;

		  if (num_strings < 0)
			   Error("Array of strings malformed.");
      
		  while (*pos != ' ') pos++;
		  pos++;
      
		  v->m_Array.clear();

		  v->m_Array.reserve(num_strings);
      
		  for (int i = 0 ; i < num_strings ; i++) {
			   sscanf(pos, "%d ", &string_length);
			   while (*pos != ' ') pos++;
			   pos++;
	
			   v->m_Array.push_back(HexToString((const unsigned char *)pos, string_length*2));
	
			   pos += (string_length*2);
		  }

	 } 
	 break;
    
	 case RVT_INVALID:
	 default:
		  break;
	 }

}

void Registry::SetValue(const string& key, const string& value_name, 
						RegistryValue* v) {
  
  DEBUGMSG(registry, "Setting key (%s, %s)\n", key.c_str(), value_name.c_str());

	 XMLNode node = GetNode(key, true);

	 DEBUGMSG(registry, "got node\n");

	 XMLNode child = node.getFirstChildNode();

	 string full_name = (value_name == "" ? s_DefaultValue : value_name);

	 // determine if full_name exists in key
	 while (!child.isNull()) {
		  if (GetAttributeInNode("name", child) == full_name) { 
			   break;
		  }
		  child = child.getNextSibling();
	 }

	 XMLNode newnode;

	 if (child.isNull()) {
		  // if value does not exist, create it
		  newnode = m_Doc.createNode(s_TagValue);

		  newnode.setAttribute("name", full_name);
		  node.appendChild(newnode);

		  XMLNode newdata = m_Doc.createCDATASection("");
		  newnode.appendChild(newdata);
	 } else {
		  // value exists:
		  if (child.getName() == s_TagKey) {

			   // child is a key:
			   // make sure default value does not already exist
			   XMLNode gchild = child.getFirstChildNode();
			   while (!gchild.isNull()) {
					if (GetAttributeInNode("name", gchild) == s_DefaultValue) {
						 newnode = gchild;
						 break;
					}
					gchild = gchild.getNextSibling();
			   }
			   if (newnode.isNull()) {
					newnode = m_Doc.createNode(s_TagValue);
					newnode.setAttribute("name", s_DefaultValue);
					newnode.setAttribute("type", RegistryValueTypeStrings[v->GetValueType()]);
					child.appendChild(newnode);
					XMLNode newdata = m_Doc.createCDATASection("");
					newnode.appendChild(newdata); 
			   }   
		  } else {
			   // child is an element
			   newnode = child;
		  }
	 }

	 DEBUGMSG(registry, "setting attribute type to %s\n", 
			  RegistryValueTypeStrings[v->GetValueType()]);
	 newnode.setAttribute("type", RegistryValueTypeStrings[v->GetValueType()]);

	 if (!newnode.isElement()) {
		  Error("Value node not found where expected.");
	 }
	 string s;
	 v->ReturnString(s);
	 newnode.setText(s, true);
	 DEBUGMSG(registry, "returning from SetValue\n");
}

bool Registry::DeleteValue(const string& key, const string& value_name) {
	 XMLNode node = GetNode(key);
	 if (node.isNull()) {
		  return false;
	 }

	 XMLNode child = node.getFirstChildNode();

	 // determine if value_name exists in key
	 while (!child.isNull()) {
		  if (GetAttributeInNode("name", child) == value_name) {	  
			   break;
		  }
		  child = child.getNextSibling();
	 }

	 if (child.isNull()) {
		  // value does not exist so we cannot delete it
		  return false;
	 } else {
		  // value exists:
		  if (child.getName() == s_TagKey) {
			   // child is a key:
			   OutputError("Registry: Cannot delete value! Key found instead.");	       
		  } else {
			   // child is an element, so delete it from node
			   node.removeChild(child);
		  }
	 }
    
	 return true;
}

void Registry::DebugPrint() {
	 cout << "Registry::DebugPrint() [" << endl;
	 cout << m_Doc;
	 cout << "]" << endl;

}


// ------------- Value Data Manipulation

RegistryValueType Registry::QueryValueType(XMLString type) {
	 assert(type.length() > 0);
	 switch(type.charAt(0)) {
	 case 's':
		  return RVT_STRING;
	 case 'n':
		  return RVT_NUMBER;
	 case 'b':
		  return RVT_BINARY;
	 case 'a':
		  return RVT_ARRAY;
	 }
	 return RVT_INVALID;
}

void ToHexString(unsigned char* data, int length, string& s) {
	 char b1, b2;
	 unsigned int slen = s.length();
	 s.resize(slen + length*2);
	 for (int i = 0 ; i < length ; i++) {
		  b1 = GetHex((int)data[i] / 16);
		  b2 = GetHex((int)data[i] % 16);
		  s[slen + i*2] = b1;
		  s[slen + i*2+1] = b2;
	 }
}

/*
  void PrintHexString(XMLString s) {
  cout << HexToString((unsigned char*)s.rawBuffer(), s.length());
  }
*/

string HexToString(const unsigned char* s, int length) {
	 if (length % 2 != 0) {
		  OutputError("Registry: Data in value failed parity check!");
	 }

	 string returnString;
	 returnString.reserve( length/2 );


	 for (int i=0; i < length ; i += 2) {
		  returnString += HexToChar(s[i], s[i+1]);
	 }

	 return returnString;
}

void RegistryValue::ReturnString(string& s) {

	 switch (GetValueType()) {
	 case RVT_NUMBER: 
	 {
		  char num[128];
		  sprintf(num, "%f", m_Number);
		  s = num;
	 }
	 break;
     
	 case RVT_STRING:
		  ToHexString((unsigned char*)m_String.c_str(), m_String.size(), s);
		  break;

	 case RVT_BINARY:
		  ToHexString((unsigned char*)m_Binary, m_BinaryLength, s);
		  break;

	 case RVT_ARRAY:
	 {
		  char string_length[128];
	  
		  // number of string in array 
		  assert(m_Array.size() < 256);
		  sprintf(string_length, "%d ", (m_Array.size()));

		  s += string_length;
	  
		  // iterate through each string and print length and string
		  for (unsigned int i = 0 ; i < m_Array.size() ; i++) {
			   int size = m_Array[i].size();

			   sprintf(string_length, "%d ", size);
			   s += string_length;

			   ToHexString((unsigned char*) (m_Array[i].c_str()), m_Array[i].size(), s);
		  }

	 }
	 break;

	 default:
		  OutputError("Registry: Attempting to store invalid data type!");	  
	 }

}

unsigned char GetHex(int c) {
	 char HexLookup[] = {'0', '1', '2', '3', '4', '5', '6', '7',
						 '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
	 assert (c < 16 && c >= 0);
	 return HexLookup[c];
}

char HexToChar(unsigned char c1, unsigned char c2) {

	 if ((! isdigit(c1) && !(c1 >= 'a' && c1 <= 'f')) ||
		 (! isdigit(c2) && !(c2 >= 'a' && c2 <= 'f'))) {
		  OutputError("Registry: Data value failed validity check "
					  "-- character not in hex matrix!");
	 }

	 if (isdigit(c1)) 
		  c1 -= '0';
	 else
		  c1 = c1 - 'a' + 10;

	 if (isdigit(c2)) 
		  c2 -= '0';
	 else 
		  c2 = c2 - 'a' + 10;

	 char num = c1 << 4;
	 num += c2;

	 return num;     
}

void Registry::CreateNewDoc() {
	 DEBUGMSG(registry, "Creating new document\n");

	 //create a new DOM Document
	 XMLDocument doc(SashRegistryIdentifier);
	 m_Doc = doc;

	 XMLNode newnode = m_Doc.createNode(s_TagKey);
	 newnode.setAttribute("name", SashRegistryTopNode);
	 m_Doc.getRootNode().appendChild(newnode);

	 m_Doc.getRootNode().setAttribute(s_VersionName, SASH_VERSION);
}

// ------------- File Manipulation
bool Registry::OpenByFilename() {
	 XMLDocument tempdoc(m_Doc);
	 XMLDocument doc;
	 m_Doc = doc;

	 DEBUGMSG(registry, "opening file [%s]\n", m_Filename.c_str());
	 bool res = m_Doc.loadFromFile(m_Filename);

	 if (res) {
		  DEBUGMSG(registry, "File parsed successfully\n");
	 } else {
		  // TODO: check to see if error is because file does not exist

		  DEBUGMSG(registry, "Creating new Document (error occurred).\n");
		  // file does not exist so we must create a new document
		  CreateNewDoc();
    
		  return true;
	 }
   
	 XMLNode rElem = m_Doc.getRootNode();
	 if (rElem.isNull()) {
		  DEBUGMSG(registry, "File read, but root document element is invalid!\n");
		  CreateNewDoc();
		  return false;
	 }

	 // attempt to determine the version of the registry
	 XMLString s = rElem.getAttribute(s_VersionName);
     
	 if (s == "") {
		  DEBUGMSG(registry, "Invalid registry file format!\n");
		  return false;
	 }

	 Version doc_version(s.c_str());
	 DEBUGMSG(registry, "document version: %s; Registry version %s\n",
			  s.c_str(), SASH_VERSION);

	 if (doc_version > SASH_VERSION) {
		  OutputMessage("Registry Warning: Registry file version is newer "
						"than this version of the Registry. "
						"File may not parse correctly.");
	 }
	 return true;
     
}

XMLNode Registry::GetNode(const std::string &a_key, bool create_subnodes) {
  DEBUGMSG(registry, "GetNode(%s, %d)\n", a_key.c_str(), create_subnodes);

	 string key = a_key;
	 XMLNode Top = m_Doc.getRootNode(); 
	 if (!Top.isNull()) {
		  Top = FindDirectChildElement(Top, SashRegistryTopNode);
	 } else {
		  OutputError("Registry: File is corrupted! Consider reinstalling.");
	 }

	 XMLNode child = Top;

	 DEBUGMSG(registry, "key size is %d\n", key.size());
	 if (key.size() == 0) {
	   DEBUGMSG(registry, "key is empty\n");
		  return Top;
	 }
	 unsigned int pos;
	 while ((pos = key.find("\\\\")) != string::npos) {
		  key.erase(pos, 1);
	 }

	 // append trailing \ if necessary
	 if (key[key.size() - 1] != '\\') {
		  key += '\\'; // warning, resize may invalidate iterator!
	 }

	 string::iterator begin = key.begin();
	 string::iterator end;

	 // ignore leading backslashes ('\\') if present
	 while (begin != key.end() && *begin == '\\') {
		  ++begin;
	 }

	 if (begin == key.end()) {
		  return Top;
	 } 

	 DEBUGMSG(registry, "searching through key %s\n", key.c_str());
     
	 XMLNode parent;
	 while((end = find(begin, key.end(), '\\')) != key.end()) {
		  const string myname(begin, end);
		  parent = child;
		  child = parent.getFirstChildNode(); 

		  ++end;
		  begin = end;

		  // attempt to find the proper subkey
		  while (!child.isNull() &&
				 ! (GetAttributeInNode("name", child) == myname)) {
			   child = child.getNextSibling();
		  }

		  // could not find the right node
		  if (child.isNull()) {
			   if (create_subnodes) {
					//return empty node if we're trying to insert into a value node,
					// or if we're trying to create a s_DefaultValue key
					if (parent.getName() == s_TagValue ||
						(myname == s_DefaultValue)) {
						 return XMLNode();
					}
					// create the necessary keys 
					XMLNode newnode = m_Doc.createNode(s_TagKey);
					newnode.setAttribute("name", myname.c_str());
					parent.appendChild(newnode);
					child = newnode;
			   } else {
					return XMLNode();
			   }
		  }

	 }

	 return child;
     
}

void Registry::SetFilename(const string& filename) {
	 m_Filename = filename;
}

void Registry::WriteToDisk(const string& filename) {
	 string fname = filename;
	 // does nothing if no filename is specified
	 if (fname == "") {
		  if (m_Filename != "") {
			   fname = m_Filename;
		  } else {
			   DEBUGMSG(registry, "Trying to save file but no filename specified!\n");
			   return;
		  }
	 }

	 ofstream f(fname.c_str());

	 //update version before writing to disk
	 XMLNode rootElem = m_Doc.getRootNode();
	 rootElem.setAttribute(s_VersionName, SASH_VERSION);     

	 f << m_Doc << endl;
	 f.close();
	 DEBUGMSG(registry, "Registry file written to disk [%s]\n", fname.c_str());
}


// ------------- General Helper Functions

XMLString Registry::GetAttributeInNode(const string& attribute_name, XMLNode n) {
	 if (n.isNull()) 
		  return XMLString();
  
	 return XMLString(n.getAttribute(attribute_name));
}

void Registry::Error(const string& s) {
	 string q = "Registry: Registry file appears to be corrupted!\n\t " + s;
	 OutputError(q.c_str());
}


void PrintNodeType(XMLNode node) {
	 cout << "Node type: ";
	 switch (node.getType()) {
	 case ELEMENT_NODE:
		  cout << "Element" << endl;
		  break;
	 case CDATA_SECTION_NODE:
		  cout << "CData" << endl;
		  break;
	 case TEXT_NODE:
		  cout << "Text" << endl;
		  break;
	 default:
		  cout << "Unknown" << endl;
	 }
}

XMLNode
Registry::FindDirectChildElement(XMLNode begin, const char *element_name) {
	 vector<XMLNode> list = begin.getChildNodes();

	 for (unsigned int i = 0; i < list.size(); i++) {
		  XMLNode node = list[i];
		  if(node.isElement() && GetAttributeInNode("name", node) == element_name) {
			   return node;
		  }
	 }
	 return XMLNode();
}

XMLNode Registry::GetRegistryXMLStructure() {
   // 'twas not painful, was it?
   XMLNode result= m_Doc.getRootNode();
	 return result;
}

void Registry::SetRegistryXMLStructure(XMLNode contents) {
   XMLNode root= m_Doc.getRootNode();
   root.setXML(contents.getXML());
}
