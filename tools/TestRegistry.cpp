
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): AJ Shankar, Andrew Wu

Extensive test of the Registry class

*****************************************************************/


//
// Registry unit tests
//

#include <string>
#include <stdio.h>
#include "sashtools.h"
#include "sash_error.h"

#include "SashGUID.h"
#include <algorithm>
#include <vector>
#include <string>
#include <unistd.h>
#include "Registry.h"

using namespace std;

void testEnumKeys(Registry& r) {
  cout << "EnumKeys:" << endl;
  vector<string> vs = r.EnumKeys("");
  copy(vs.begin(), vs.end(), ostream_iterator<string>(cout, ",\n"));
}

void testEnumValues(Registry& r) {
  cout << "EnumValues:" << endl;
  vector<string> vs = r.EnumValues("");
  copy(vs.begin(), vs.end(), ostream_iterator<string>(cout, ",\n"));
}


void testCreateDeleteKeys(Registry& r) {
  cout << "\nTesting key creation and deletion:" << endl;

  // make sure key isn't there already
  r.DeleteKey("\\Andrew\\Wu");

  // create key and verify it exists
  r.CreateKey("\\Andrew\\Wu");

  vector<string> vs( r.EnumKeys("\\Andrew") );
  assert( find(vs.begin(), vs.end(), "Wu") != vs.end() );
  cout << "\tKey created and found in enumerated keys" << endl;
	     			
  if (r.DeleteKey("\\Andrew\\Wu")) {
    vs = r.EnumKeys("\\Andrew");
    assert( find(vs.begin(), vs.end(), "Wu") == vs.end() );
    cout << "\tKey deleted and not found in enumerated key list" 
	 << endl;
  } else {
    OutputError("\tDeleteKey failed!");
  }

  cout << "Key create/delete unit test... OK" << endl;
}

void testRename(Registry& r) {
  cout << "\nTesting key renaming:" << endl;

  // make sure key isn't there already
  r.DeleteKey("\\Andrew", true);
  // create key and verify it exists
  r.CreateKey("\\Andrew\\Wu\\Dog");
  assert( r.Rename("\\Andrew\\Wu", "Woo") );
  
  vector<string> vs( r.EnumKeys("\\Andrew") );
  assert( find(vs.begin(), vs.end(), "Wu") == vs.end() );
  assert( find(vs.begin(), vs.end(), "Woo") != vs.end() );

  cout << "Key rename unit test... OK" << endl;
}

void testSetAndQueryValues(Registry& r) {
  RegistryValue rv;

  cout << "\nTesting Registry::SetValue() and Registry::QueryValue()" 
       << endl;

  //string
  cout << "\tTesting STRING set, " << flush;
  rv.SetValueType(RVT_STRING);
  rv.m_String = "aj_shankar";

  r.SetValue("\\BigBoss\\name", "some_string", &rv);

  //array
  cout << "ARRAY set, " << flush;
  rv.SetValueType(RVT_ARRAY);
  
  rv.m_Array.push_back("andrew");
  rv.m_Array.push_back("AJ");
  rv.m_Array.push_back("ooga booga this might work");

  r.SetValue("\\BigBoss\\name", "ari", &rv);

  //binary
  cout << "BINARY set: " << endl;
  rv.SetValueType(RVT_BINARY);
  char binary_string[] = "this is a binary string";
  rv.m_Binary = (void*)(&binary_string);
  rv.m_BinaryLength = sizeof(binary_string);

  r.SetValue("\\BigBoss\\name", "binary_test", &rv);

  //retrieve string, array and binary to test
  RegistryValue new_value;
  r.QueryValue("\\BigBoss\\name", "some_string", &new_value);

  assert(new_value.GetValueType() == RVT_STRING &&
	 new_value.m_String == "aj_shankar");
  cout << "\tSTRING retrieved. " << flush;

  r.QueryValue("\\BigBoss\\name", "ari", &new_value);
  assert(new_value.GetValueType() == RVT_ARRAY &&
	 new_value.m_Array.front() == "andrew" &&
	 new_value.m_Array.back()  == "ooga booga this might work" &&
	 new_value.m_Array.size() == 3);
  cout << "ARRAY retrieved. " << flush;

  r.QueryValue("\\BigBoss\\name", "binary_test", &new_value);

  assert(new_value.GetValueType() == RVT_BINARY &&
	 strcmp((char*) new_value.m_Binary, binary_string) == 0);

  cout << "BINARY retrieved." << endl;

  cout << "Set and query values unit test... OK" << endl;
}

// tests Registry::DeleteValue() by adding a value,
// deleting it, then verifying it was deleted.
void testZeroLengthArray(Registry& r) {
	 cout << endl << "Testing zero-length array:" << endl;
  RegistryValue set_rv;
  set_rv.SetValueType(RVT_ARRAY);
  r.SetValue("\\zla", "", &set_rv);
	 RegistryValue get_rv;
	 r.QueryValue("\\zla", "", &get_rv);
	 assert(get_rv.GetValueType() == RVT_ARRAY);
	 assert(get_rv.m_Array.size() == 0);
	 cout << "Zero-length array unit test... OK" << endl;
}

void testChangeOfValueType(Registry& r) {
	 cout << endl << "Testing changing value type:" << endl;
  RegistryValue set_rv;
  set_rv.SetValueType(RVT_STRING);
  set_rv.m_String = "testing";
  r.SetValue("\\cvt", "", &set_rv);
  set_rv.SetValueType(RVT_ARRAY);
  r.SetValue("\\cvt", "", &set_rv);
  cout << "Changed from string to array " << endl;
	 RegistryValue get_rv;
	 r.QueryValue("\\cvt", "", &get_rv);
	 assert(get_rv.GetValueType() == RVT_ARRAY);
	 assert(get_rv.m_Array.size() == 0);
  
	 cout << "Zero-length array unit test... OK" << endl;
}

// tests Registry::DeleteValue() by adding a value,
// deleting it, then verifying it was deleted.
void testDeleteValues(Registry& r) {
  cout << "\nTesting DeleteValue(): " << endl;
  cout << "\tAdding value..." << endl;

  RegistryValue set_rv;
  set_rv.SetValueType(RVT_STRING);
  string s = "3.14159";
  set_rv.m_String = s;

  // add value
  r.SetValue("\\BigBoss\\name", "gryph_mo", &set_rv);

  RegistryValue get_rv;
  r.QueryValue("\\BigBoss\\name", "gryph_mo", &get_rv);
  assert( get_rv.GetValueType() != RVT_INVALID ); // rv should be valid
  assert( get_rv.GetValueType() == set_rv.GetValueType() );

  assert( get_rv.m_String == set_rv.m_String );

  cout << "\tValue added." << endl;

  // delete value and make sure it was deleted
  bool deleted = r.DeleteValue("\\BigBoss\\name", "gryph_mo");
  assert( deleted );
  r.QueryValue("\\BigBoss\\name", "gryph_mo", &get_rv);
  assert( get_rv.GetValueType() == RVT_INVALID ); // rv should be invalid

  cout << "\tValue deleted properly." << endl;
  cout << "Value deletion unit test... OK" << endl;
}


void testBigBinaryStorage(Registry& r) {
  cout << "\nTesting storage/query of large binary data: " << endl;
  int bin_length = 1<<20;
  cout << "\tAdding binary garbage of length: " 
       << bin_length << " bytes." << endl;

  RegistryValue rv;
  rv.SetValueType(RVT_BINARY);
  rv.m_BinaryLength = bin_length;
  rv.m_Binary = new char[bin_length];
      
  // put numeric chars at begin and end
  char* pBin = static_cast<char*>(rv.m_Binary);
  pBin[0] = '0';
  pBin[1] = '1';
  pBin[2] = '2';
  pBin[bin_length-1] = '9';     
  pBin[bin_length-2] = '8';
  pBin[bin_length-3] = '7';

  // add large amount of binary data
  char* key_name   = "BinaryData";
  char* value_name = "garbage";
  r.SetValue(key_name, value_name, &rv);
  delete[] (char*) rv.m_Binary;

  // retrieve binary data
  RegistryValue get_rv;
  r.QueryValue(key_name, value_name, &get_rv);
  char* pRetBin = static_cast<char*>(get_rv.m_Binary);
  assert( get_rv.GetValueType() == RVT_BINARY ); // get_rv should be valid

  // check length and beginning/end chars
  assert( get_rv.m_BinaryLength = bin_length );
  assert( pRetBin[0] == '0' &&
	  pRetBin[1] == '1' &&
	  pRetBin[2] == '2' );
  assert( pRetBin[bin_length-1] == '9' &&
	  pRetBin[bin_length-2] == '8' &&
	  pRetBin[bin_length-3] == '7' );
  delete[] (char*) get_rv.m_Binary;
	     
  cout << "\tStart and end of " << bin_length 
       << " binary data match." << endl;

  cout << "\tRemoving binary data from registry..." << endl;
  r.DeleteValue(key_name, value_name);

  cout << "Large binary storage unit test... OK" << endl;
}

void testBocaGrandeBug(Registry& r) {     
  cout << "\nTesting 'BocaGrande' bug:" << endl;

  RegistryValue rv;
  rv.SetValueType(RVT_STRING);
  rv.m_String = "burrito_grande";

  // check for bug where string resizes and invalidated iterators,
  //  but only if the length of the value and key adds up to 14.
  char* key_name   = "data";
  char* value_name = "BocaGrande";
  r.SetValue(key_name, value_name, &rv);
     
  // retrieve number data
  RegistryValue get_rv;
  r.QueryValue(key_name, value_name, &get_rv);
  assert( get_rv.GetValueType() == RVT_STRING);
  assert( get_rv.m_String == rv.m_String );

  cout << "'BocaGrande' unit test... OK" << endl;
}

void testZeroLength(Registry& r) {     
  cout << "\nTesting zero length strings (and multiple default values):" << endl;

  RegistryValue rv;
  rv.SetValueType(RVT_STRING);
  rv.m_String = "alpha beta gamma delta";

  char* key_name   = "";
  char* value_name = "";
  r.SetValue(key_name, value_name, &rv);
  r.SetValue(key_name, value_name, &rv);

  vector<string> keys = r.EnumKeys(key_name);
  sort(keys.begin(), keys.end());
  vector<string>::iterator unique_keys = unique(keys.begin(), keys.end());
  assert(unique_keys == keys.end()); 

  RegistryValue get_rv;
  r.QueryValue(key_name, value_name, &get_rv);

  assert( get_rv.GetValueType() == RVT_STRING );
  assert( get_rv.m_String == rv.m_String );

  cout << "Zero length (and multiple default value) unit test... OK" << endl;
}

void testXMLNode(Registry& r, Registry& s) {
     
  cout << "\nTesting retrieval and setting of XMLNodes:" << endl;

  cout << "\tGetting one XML node" << endl;

  char* wudog = "WuDogggg";

  RegistryValue set_rv;
  set_rv.SetValueType(RVT_STRING);
	 
  set_rv.m_String = wudog;
  r.SetValue("\\wudog\\pillow\\", "keyboard", &set_rv);

  //     XMLNode n = r.GetXMLNode("\\wudog");


  cout << "\tAppending that node to another node" << endl;
	 
  //     s.SetXMLNode("", n);

  RegistryValue get_rv;
  s.QueryValue("\\pillow", "keyboard", &get_rv);

  assert(get_rv.m_String == wudog);
  s.WriteToDisk();

  cout << "XMLNode unit test... OK" << endl;
}

void testKeyValuePair(Registry& r, const char* key, const char* value) {
  string s("hack_mozilla_dot_org");

  RegistryValue set_rv;
  set_rv.SetValueType(RVT_STRING);
  set_rv.m_String = s;

  //cout << "\tSetting value" << endl;

  r.SetValue(key, value, &set_rv);

  //cout << "\tQuerying value" << endl;

  RegistryValue get_rv;
  r.QueryValue(key, value, &get_rv);

  assert(set_rv.GetValueType() == get_rv.GetValueType());
  assert(set_rv.m_String == get_rv.m_String);
}

void testMultipleStrokes(Registry& r) {
  cout << "\nTesting multiple backslash access:" << endl;

  char* key   = "\\\\"; // <-- multiple \'s should be ignored
  char* value = "root";

  testKeyValuePair(r, key, value);

  key   = "";
  value = "\\root"; // <-- unnecessary \'s in value should be ignored

  testKeyValuePair(r, key, value);

  cout << "Multiple backslash access unit test... OK" << endl;
}

void testArrayStuff(Registry & r) {
  cout << "\nTesting array modification:" << endl;

  for (int i = 0; i < 5; i++) {

  vector<string> values;

  int r1 = rand() % 10;
  for (int j = 0; j < r1; j++) {
	values.push_back("v");
  }

  RegistryValue v;
  v.SetValueType(RVT_ARRAY);
  v.m_Array = values;
  r.SetValue("", "array-test", &v);

  vector<string> values2;

  int r2 = rand() % 10;
  for (int j = 0; j < r2; j++) {
	values2.push_back("v");
  }

  v.m_Array = values2;
  r.SetValue("", "array-test", &v);

  RegistryValue result;
  r.QueryValue("", "array-test", &result);

  assert(result.GetValueType() == RVT_ARRAY);
  assert(result.m_Array.size() == (unsigned int) r2);
  }
  cout << "Array modification... OK" << endl;
}

int main(int argc, char* argv[]) {
  
  // make sure we can write an XML Registry from scratch:
  remove("bob.xml");
  Registry q;
  q.WriteToDisk("bob.xml");
  q.SetFilename("bob.xml");

  // unit tests on carlos.xml:
  Registry r;
  r.OpenFile("bob.xml");
  r.OpenFile("carlos.xml");

   testCreateDeleteKeys(r);
  testRename(r);
  testSetAndQueryValues(r);
  testDeleteValues(r);
  testBigBinaryStorage(r);
  testBocaGrandeBug(r);
  testZeroLength(r);     
  testMultipleStrokes(r);
  testZeroLengthArray(r);
  testChangeOfValueType(r);
  testArrayStuff(r);
  //     testXMLNode(r, q);

  cout << endl;

//       testEnumKeys(r);
//       testEnumValues(r);

  r.WriteToDisk();
     
}

