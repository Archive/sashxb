
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Ari Heitner

Class to implement communication with the weblication task manager

*****************************************************************/

#ifndef WEBLICATION_REGISTER_H
#define WEBLICATION_REGISTER_H

#include <netinet/in.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "sashtools.h"

#include <string>

struct tcpServerInfo {
  int sock;
  sockaddr_in serverAddr;
  hostent *server;
  int portNum;
};

/*
 * TODO: right now this is running over a loopback. Is there any reason it 
 *   shouldn't be running over a local unix socket?
 * also TODO: we should make this thing automagically run in a different thread,
 *   so if it needs to timeout it doesn't block the weblication.
 */
struct WeblicationRegister {
	 std::string webl_name;  // The weblication's name
	 std::string webl_guid;  // The weblication's guid
	 std::string act_name;   // action's name
	 std::string loc_name;  // The location's name
	 void registerMe(); // call this after you've filled in the above info
	 bool parseRegistrationString(const std::string& r);

	 WeblicationRegister(int port = DEFAULT_PORT);
	 std::string pid; // will be set for you

	 void* data;  // store some associated data here... initially set to NULL

	 private:
	 int setupTCPServer();
	 tcpServerInfo tsi;
	 static const std::string delim;

};

#endif
