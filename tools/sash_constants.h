
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Ari Heitner, AJ Shankar, Andrew Wu

global constants pertaining to sash runtime

*****************************************************************/

#ifndef SASH_CONSTANTS_H
#define SASH_CONSTANTS_H

#include <string>
#include <ctype.h>
#include <algorithm>

extern "C" {

extern const int DEFAULT_PORT;
extern const char * SASH_VERSION;
extern const char * CACHEDIR;
extern const char * DATADIR;
extern const char * BINDIR;
extern const char * COMPONENTDIR;
extern const char * RUNTIME_NAME;
extern const char * PLATFORM;
extern const char * DEFAULT_EXTENSIONS[];
extern const char * GlobalSecurityFileName;

string GetSashShareDirectory();
string GetSashComponentsDirectory();


inline const std::string GetSashBinaryPath() { 
	 return getenv("SASH_BIN") + std::string("/") + RUNTIME_NAME; 
}

inline const std::string RegistryFileName(const std::string& GUID) {
	 return GUID + ".dat"; 
}

inline const std::string WDFFileName(const std::string& GUID) {
	 return GUID + ".wdf"; 
}

inline const std::string SecurityFileName(const std::string& GUID) {
	 return RegistryFileName(GUID + ".security"); 
}

inline const std::string LibraryFileName(const std::string& GUID) {
	 return "lib" + GUID + ".so";
}

inline const std::string BinDirectory(const std::string& sash_install_dir) {
	 return sash_install_dir + "/" + BINDIR;
}

inline const std::string ComponentDirectory(const std::string& sash_install_dir) {
	 return BinDirectory(sash_install_dir) + "/" + COMPONENTDIR;
}

inline const std::string WeblicationDirectory(const std::string& installation_dir, const std::string& GUID) {
	 return installation_dir + "/" + GUID;
}

inline const std::string DataDirectory(const std::string& weblication_dir) {
	 return weblication_dir + "/" + DATADIR;
}

inline const std::string CacheDirectory(const std::string& weblication_dir) {
	 return weblication_dir + "/" + CACHEDIR;
}

// used extensively to guarantee uppercaseness of guids, etc.
// makes a new copy of the string!
inline std::string InPlaceToUpper(std::string& s) {
	 transform (s.begin(), s.end(), s.begin(), toupper);
	 return s;
}

inline std::string InPlaceToLower(std::string& s) {
	 transform (s.begin(), s.end(), s.begin(), tolower);
	 return s;
}

inline std::string ToUpper(const std::string& s) {
	 std::string str(s);
	 return InPlaceToUpper(str);
}

inline std::string ToLower(const std::string& s) {
	 std::string str(s);
	 return InPlaceToLower(str);
}

typedef struct
{
	 std::string OS;
	 std::string Architecture;
	 std::string Distribution;
} SashSystemInfo;

void GetSashSystemInfo(SashSystemInfo & info);

}

#endif
