#!/usr/bin/perl
#/***************************************************************
#    Sash for Linux
#    The Sash Runtime for Linux
#
#    Copyright (C) 2000 IBM Corporation
#
#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#    Contact:
#    sashxb@sashxb.org
#
#    IBM Advanced Internet Technology Group
#    c/o Lotus Development Corporation
#    One Rogers Street
#    Cambridge, MA 02142
#    USA
#
#*****************************************************************
#
# Contributor(s): AJ Shankar
#
# Creates all marshalling code (from/to JavaScript) from an IDL file
# Useful when creating extensions or locations for sash
#
#*****************************************************************/

# generates mozilla-happy marshalling wrapper code for javascript extensions to 
# mozilla from idl files

###################################################################
# to add additional datatypes, edit the idltypetemplates.pl file! #
###################################################################

# an ugly hack to get the running directory -- there's gotta be a good way 
# to get this (an env variable?) but I don't remember what it is!
my $myname = $0;
$myname =~ /^(.*\/)[^\/]*$/;
do "$1idltypetemplates.pl" or die "can't find idltypetemplates.pl!";


if (scalar @ARGV != 2) {
	print <<usage;

idl to func

Converts .idl files to C-style object handlers 

Usage
idltofunc [filename] [sash_name]

[filename] 
processes filename

[sash_name]
name exposed to Sash. in JavaScript

usage
	exit 1;
}

my $line;
my $stripped_name;
	
# determine what files to process
my @files = (shift @ARGV);
my $sash_name = shift @ARGV;


# process each file
foreach my $file (@files) {

	$stripped_name = $file;
	my ($path) = $stripped_name =~ s!^(.*/)([^/]+)$!$2!;
	$stripped_name =~ s/\.[^.]*$//;
	my $non_idl = $stripped_name;	
	$stripped_name =~ s/nsI([^\/]*)$/$1/;

    my @funcmap;
	my @properties;
	my @events;

	# open input file
	open(in, $file);
	undef $/; # read in whole file
	my $data = <in>;
	close in;

	open(out, ">".$path."nsJS".$stripped_name.".cpp");
	print "Generating ${path}nsJS${stripped_name}.cpp... ";
	my @date = localtime(time);
	$date[5] += 1900;
	for my $y(0..2) {
		$date[$y] = "0".$date[$y] if (length($date[$y]) == 1);
	}

    print out <<header;

// file created with idltofunc on $date[2]:$date[1]:$date[0], $date[4]/$date[3]/$date[5]

#include "SashInclude.h"
#include "nsSashScriptObjectManager.h"
#include "${non_idl}.h"
#include "sash_error.h"
#include "nsSashScriptClass.h"

// this is what your header file should be called
#include "ns$stripped_name.h"

#define ${stripped_name}_OFFSET 0x020

char* ${non_idl}Name = "$sash_name";
ns${stripped_name} * g${stripped_name} = NULL;
header

	# go through each interface in the file
	while ($data =~ /interface\s*(\w*).*?{(.*?)};/msg) {
	  my $interface = $1;
	  $_ = $2;
	  
	  # grab each property, event or function in the interface
	  while (/(property\s*\w+\s*\w*?\s*;)|(event\s*\w+\s*;)|((void|\w*)\s*\w+\s*\(\s*.*?\s*\);)/msg) {
	  
		if ($1) {
		  my $prop = $1;
		  # keep track of properties and events
		  $prop =~ /property\s*(\w+)\s*(\w*?)\s*;/ms;
		  push @properties, [$1, ($2 ? 1 : 0)];
		  next;

		} elsif ($2) {
		  my $event = $2;
		  $event =~ (/event\s*(\w+)\s*;/ms);
		  push @events, $1;
		  next;

		} elsif ($3) {
		  my $func = $3;
		  # now we're ready to rumble
		  my ($rval, $name, $params) = $func =~ /(void|\w*)\s*(\w+)\s*\(\s*(.*?)\s*\);/ms;
		  
		  my @pms = split(/\s*,\s*/, $params);
		  if ($rval ne "void") {
			push @pms, "out $rval idlrval";
		  }

		  # print out the function
		  push @funcmap, [$name, &handle_function(\*out, $interface, $name, @pms)];

		  next;
		  
		} else {
		  die "Internal error!";
		}
	  }
	}

	# output function map
	print out "\n\nstatic ExtObjMethodMap $stripped_name"."Methods[] = \n{\n";
	foreach (@funcmap) {
		print out "\t{\"$_->[0]\",\t$stripped_name$_->[0],\t$_->[1]},\n";
	}
	print out "\t{0}\n";
	print out "};\n\n";

	# output properties
	print out "\n\nstatic ExtObjPropertyMap $stripped_name"."Prop[] = \n{\n";
	my $j = 0;
	foreach (@properties) {
		print out "\t{\"$_->[0]\",\t\t${stripped_name}_OFFSET + $j,\t", ($_->[1] ? "PR_TRUE" : "PR_FALSE"), "},\n";
		$j++;
	}
	print out "\t{0}\n";
	print out "};\n\n";

	# output events
	print out "\n\nstatic ExtObjEventMap $stripped_name"."Event[] = \n{\n";

	foreach (@events) {
		print out "\t{\"$_\",\t\t${stripped_name}_OFFSET + $j},\n";
		$j++;
	}
	print out "\t{0}\n";
	print out "};\n\n";

	print out <<callfunc;

nsresult Init${stripped_name}InExtender(nsSashScriptObjectManager *pExtender)
{
\tnsSashScriptClass* pSashScriptClass;
\tJSExtenderInfo CommonExtenderInfo;
\tCommonExtenderInfo.pClassName = ${non_idl}Name;
\tCommonExtenderInfo.pMethodSpec = ${stripped_name}Methods;
\tCommonExtenderInfo.pPropSpec = ${stripped_name}Prop;
\tCommonExtenderInfo.pEventSpec = ${stripped_name}Event;
\tpExtender->AddScriptExtension(&CommonExtenderInfo);

\tif (g${stripped_name} == NULL)
\t\tg${stripped_name} = new ns${stripped_name}();

\tNSGetSashScriptClass(&pSashScriptClass);
\tif (pSashScriptClass) {
\t\tpSashScriptClass->AddSashExtension(g${stripped_name},${non_idl}Name);
\t} else {
\t\tOutputMessage("Sash Error: Unable to register extension %s!",${non_idl}Name);
\t}

\treturn NS_OK;
}

nsresult Destroy${stripped_name}InExtender() {
\tdelete g${stripped_name};

}
callfunc

	close out;

  print "Done.\n";
}

#########################
sub handle_function {
	my ($out, $interface, $fname, @params) = @_;
	my $has_out;
	my $returnval;
	my @ps;

	# extract each function in three elements:
	# in/out, type, name
	foreach my $p (@params) {
		my ($io, @info) = split(" ", $p);
		
		if (scalar @info != 2) {
			print STDERR "Warning ($interface): Incorrectly defined variable $info[1] in function " . 
			  "$fname.\n";
		}

		# only allow one return value
		if ($io =~ /^out/i) {
			if ($has_out) {
				print STDERR "Warning ($interface): Ignoring additional out variable ". 
				  "$info[1] found in function $fname.\n";
				next;
			}

			$has_out = 1;
			$returnval = &generate_data_type(@info);
		} else {

			push (@ps, &generate_data_type(@info));
		}
	}

	my $num_params = (scalar grep { ! &get_prop("do-not-count-param", $_) } @ps) || 0;
	my @check_func;
	my $context = $user_vars{'CONTEXT'};

	print $out <<dec;


PR_STATIC_CALLBACK(JSBool) ${stripped_name}$fname\(JSContext *$context, JSObject *obj, uintN argc, jsval *argv, jsval *rval) {
\tJSBool nsRet = JS_FALSE;
\t*rval = JSVAL_NULL;

\tif (
dec
	
	push @check_func, "\t\targc == $num_params";
	my $i = 0;
	
    # first, check params
	foreach my $p (@ps) {
		next if (&get_prop("do-not-count-param", $p));

	    $p->{'ARG'} = "argv[$i]";
		&convert_items($p);

		$i++;
		push @check_func, &get_prop("check-param", $p);
	}
	
	print $out join(" && \n\t\t", @check_func);
    &convert_items($returnval);
	
	print $out <<next;

\t) {

\t\tif (g${stripped_name}) {
next

    # convert params
	foreach my $p(@ps) {
	  print $out "\t\t\t" , &get_prop("convert-param", $p), ";\n" if 
	    &get_prop("convert-param", $p);
	}

    if ($returnval) {
	  print $out "\t\t\t",  &get_prop("declare-returnval", $returnval),  ";\n" if
	     &get_prop("declare-returnval", $returnval);
	}

    # get params in list form 
    my @param_names = map { $_->{'PARAM'} } @ps;

    my $outconv;
    if ($returnval) {
	  push @param_names, &get_prop("returnval-argument", $returnval);
	  

	  $outconv = "*rval = " . &get_prop("convert-returnval", $returnval) . ";" if
             &get_prop("convert-returnval", $returnval);


    }

    my $params = join(',', @param_names);
	print $out <<compute;

\t\t\tif (g${stripped_name}->$fname($params) == NS_OK) {
\t\t\t\tnsRet = JS_TRUE;
\t\t\t\t$outconv
\t\t\t}

\t\t}
\t}

\treturn nsRet;
}
compute

  return $num_params + ($returnval ? 1 : 0);
}

###################
sub generate_data_type {
  my ($type, $name) = @_;
  my %self = %user_vars;

  die "Error ($interface): Type $type not supported! Add to templates file if desired..." if
	! defined $conv{$type};
  my %convtype = %{$conv{$type}};
  $self{'properties'} = \%convtype;

  $self{'PARAM'} = $name;
  $self{'TYPE'} = $type;

  # check the property name 
  return \%self;
}

###################
sub convert_items {
  my ($self) = (@_);

  # allows for setting the parameter name, regardless of what was specified in the idl file
  if ($self->{'properties'}{'set-param-name'}) {
	$self->{'PARAM'} = $self->{'properties'}{'set-param-name'};
	$self->{'PARAM'} =~ s/\$(\w+)/$self->{$1}/g;
  }

  # interpolate the variables in the strings
  foreach (keys %{$self->{'properties'}}) {
    $self->{'properties'}{$_} =~ s/\$(\w+)/$self->{$1}/g;
	# replace fake newlines with real ones
    $self->{'properties'}{$_} =~ s/\\n/\n\t\t\t/g;
  }
}

###################
sub get_prop {
  my ($property, $p) = @_;

  return $p->{'properties'}{$property};
  
}
