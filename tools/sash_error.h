
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Text and graphical error and message functions used throughout sash

*****************************************************************/

// AJ Shankar 
// June 2000
//
// Gnome update -- John Corwin
// July 2000

#ifndef ERROR_H
#define ERROR_H

extern "C" {

typedef enum {
	 SASH_ERROR_TEXT,
	 SASH_ERROR_GNOME
} SashErrorMode;

// This function can be changed to reroute all messages
void OutputString(const char *s);

// Outputs error messages, dies
void OutputError(const char* str, ...);

// Outputs non-error messages, e.g. status/warning
int OutputMessage(const char* str, ...);

// Outputs a message and gives the user a (yes/no) prompt
// Returns true if the user selected yes, false if the user selected no
bool OutputQuery(const char* str, ...);

// Asks a question with three possible responses.
// Returns the index of the response chosen.
int Output3Choice(const char * ch0, const char * ch1, const char * ch2, 
		  const char * query, ...);

// SashErrorPushMode and SashErrorPopMode -- push and pop the message mode
//   for functions in this module.
SashErrorMode SashErrorPushMode(SashErrorMode mode);
SashErrorMode SashErrorPopMode();

// Sets the GTK parent window for messages
// the window parameter should be a GtkWindow
// set window to NULL if SashError should no longer use a parent window
void SashErrorSetWindow(void * window);

}

#endif // error_h


