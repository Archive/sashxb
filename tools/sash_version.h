#ifndef _SASH_VERSION_H_
#define _SASH_VERSION_H_
#include <string>

struct Version { 
	 unsigned int major, major1, minor, maintenance; 
	 Version(); // initializes to "0.0.0.0"
	 Version(const Version& s);
	 Version(const std::string& s); // initializes from string of type "1.2.3.4"
	 Version(unsigned int maj, unsigned int maj1, unsigned int min, unsigned int maint);
	 std::string ToString() const;

	 // compares versus a string of form "1.2.3.4"
	 bool operator>(const std::string& s) const;
	 bool operator>(const Version& b) const;
	 bool operator==(const std::string& s) const;
	 bool operator==(const Version& b) const;
	 Version& operator=(const std::string& b);
};

#endif
