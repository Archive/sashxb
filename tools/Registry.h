
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): AJ Shankar, Andrew Wu, Andrew Chatham

Hierarchical XML registry; used to store settings throughout sash

*****************************************************************/

// Registry header file
// Library class for editing sash weblication registry files
// AJ Shankar, Andrew Wu
// June 2000
// last updated 14 June 2000

// Each individual registry file is stored under the GUID of the 
// weblication.

// Two backslash characters must preceed each subkey 
//    (e.g."ExampleSubKey1\\ExampleSubKey1_1")

// In setting values and keys, the keys necessary to reach the desired 
// level will be automatically created

// All data passed to and from the Registry is transmitted via
// RegistryValue structs

#ifndef REGISTRY_LIBRARY_H
#define REGISTRY_LIBRARY_H

#include <fstream.h>
#include <string>
#include <vector>
#include "sash_error.h"
#include "sash_constants.h"
#include "XMLNode.h"
#include "XMLString.h"

extern const char* SashRegistryIdentifier;
extern const char* SashRegistryTopNode;

// *** The following are helper functions which are only called internally
// converts a number fom 0-15 to hex (char)
unsigned char GetHex(int c);
// converts from two parts of a byte (stored as hex) to the original char
char HexToChar(unsigned char c1, unsigned char c2);
// converts from a hex char* to a non-hex std::string
std::string HexToString(const unsigned char* s, int length);


// types of values that can be stored in the registry

enum RegistryValueType {
  RVT_STRING = 0,
  RVT_NUMBER,
  // array of strings
  RVT_ARRAY,
  RVT_BINARY,
  // invalid type -- *cannot* be stored as value!
  RVT_INVALID = -1
};

extern const char** RegistryValueTypeStrings;

// basic data type for passing information to and from the Registry
struct RegistryValue {

  RegistryValue() : m_Number(0.), m_Binary(NULL), m_BinaryLength(0), m_Type(RVT_INVALID) {}

  // returns value type of currently stored value
  inline RegistryValueType GetValueType() const {
    return m_Type;
  }
  
  // sets value type 
  inline void SetValueType(RegistryValueType r) {
    m_Type = r;
  }

  // returns true iff a valid type has been stored in the registry
  inline bool IsValid() const {
    return (m_Type != RVT_INVALID);
  }

  // must set one of these (or both for binary type) before 
  // passing object in to a Registry set value function
  float m_Number;
  std::string m_String;
  std::vector<std::string> m_Array;
  // both of these much be set (the latter usually with sizeof())
  // to store binary data types
  void* m_Binary;
  int m_BinaryLength;

  // converts data of all types to XMLString for CDATA storage
  // should not be called externally
  void ReturnString(std::string& s);

  private:
  
  RegistryValueType m_Type;

};


class Registry {
  static const char* s_VersionName;  // SashRegistry version information
  static const char* s_DefaultValue; // Default key name
  static const char* s_TagKey;       // tag name for keys
  static const char* s_TagValue;     // tag name for values

 public:
  // creates a new Registry
  Registry();

  // destructor
  ~Registry();

  // opens the registry file specified
  // by the filename
  bool OpenFile(const std::string& Filename);

  // writes out contents to disk
  // must be called *explicitly* to save registry to disk
  // if no filename is given, uses the file the registry was 
  // instantiated under; if that wasn't specified either 
  // (i.e. the default constructor was used), then does nothing
  void WriteToDisk(const std::string& filename = "");

  // sets the default filename to use when writing to disk
  void SetFilename(const std::string& filename);

  // Adds a key to to the registry. 
  // Will create as many keys as necessary to reach desired level.
  // Will not complain if key already exists, but returns false iff we try 
  // to insert into a value node
  // e.g. calling CreateKey("\\bob\\phil\\") on a new registry
  // will create the \bob key, as well as the \phil key underneath it
  bool CreateKey(const std::string& key);

  // deletes a key from the registry
  // returns true iff key is deleted
  // key must not have subkeys, unless delete_deep is set
  // can technically also delete value nodes (don't rely on that!)
  bool DeleteKey(const std::string& key, bool delete_deep = false);

  // Rename("\\bob\\phil\\jane", "judy") changes the key to
  // "\\bob\\phil\\judy" 
  bool Rename(const std::string& key, const std::string& newname);

  // returns a vector of the subkeys under the given key
  std::vector<std::string> EnumKeys(const std::string& key);

  // returns a vector of the value names under the given key
  std::vector<std::string> EnumValues(const std::string& key);

  // sets the RegistryValue struct to the data element specified by key and 
  // value_name
  // if value does not exist, sets type to RV_INVALID
  void QueryValue(const std::string& key, const std::string& value_name, 
		  RegistryValue* v);

  // stores the value specified in RegistryValue in the Registry, creating the value
  // under the given key if necessary
  // use "" or "(Default)" as the value_name to access the default value for the key
  void SetValue(const std::string& key, const std::string& value_name, 
		RegistryValue* v);

  // delete the value under a given key. 
  // returns true iff data is succesfully deleted.
  bool DeleteValue(const std::string& key, const std::string& value_name);

  // prints out XML for debugging purposes
  void DebugPrint();
  
  // A hook for saving the registry in the WDE wpf files. Should not affect
  // anything else. Please do not remove unless you also want to fix the
  // WDE side of things.
  XMLNode GetRegistryXMLStructure();
  
  // The same as above, but in reverse.
  void SetRegistryXMLStructure(XMLNode contents);

 private:
  // opens given registry (passes it to parser)
  bool OpenByFilename();

  // helper function for OpenByGUID() -- creates a new, empty Registry in memory
  void CreateNewDoc();

  // returns the value type for the value name specified under key
  RegistryValueType QueryValueType(XMLString type);

  // searches a given node for a key of supplied name
  XMLString GetAttributeInNode(const std::string& attribute_name, XMLNode n);

  // error helper
  void Error(const std::string& s);

  // filename of registry
  std::string m_Filename;

  // helper function to find appropriate node
  // if create_subnodes is true, when recursing it will create any subnodes necessary to 
  // get to the desired key
  XMLNode GetNode(const std::string& key, bool create_subnodes = false);

  // helper function to return the names of subnodes of a specific type
  std::vector<std::string> EnumNames(const std::string& key, 
				     const std::string& name);

  // Gets the first CData subnode of a value node
  XMLNode FindDirectChildElement(XMLNode begin, const char *element_name);

  XMLDocument m_Doc;

};  


#endif // REGISTRY_LIBRARY_H
