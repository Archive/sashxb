#!/bin/sh

BINARY_NAME=`basename $0`
PACKAGE=sashxb

dist_bin=`dirname $0`
dist_lib=$dist_bin/../lib/$PACKAGE
dist_share=$dist_bin/../share/$PACKAGE 
dist_components=$dist_lib/components
curr_dir=`pwd`

if test -n "$SASH_HOME"; then
    sashdir=$SASH_HOME;
else
    sashdir=$HOME/.sash
fi

if test -d $sashdir; then 
    true
elif [ -d $dist_share/sash-skel ]; then
    cp -r $dist_share/sash-skel $sashdir
    if [ -d $dist_share/default-exts ]; then
	echo Installing default extensions...
	for p in `ls $dist_share/default-exts/*/*.wdf`
	do
	    cd `dirname $p`
	    sash-install `basename $p` -f
	    cd $curr_dir
	done
    else
	echo "Warning: no default extensions to install. Running from a bare installation."
    fi
else
    echo "SashXB is not installed properly on your system; the $dist_share/sash-skel directory does not exist. Please reinstall SashXB."
    exit
fi


dist_moz=$sashdir/mozilla-bin
local_components=$sashdir/components

if test -n "$MOZILLA_FIVE_HOME"; then
	true
elif [ -f `echo /usr/lib/mozilla-*/mozilla-bin` ]; then 
	MOZILLA_FIVE_HOME=`echo /usr/lib/mozilla-*`
elif [ -f /usr/local/mozilla/mozilla-bin ]; then 
	MOZILLA_FIVE_HOME=/usr/local/mozilla
elif [ -f /usr/lib/mozilla/mozilla-bin ]; then 
	MOZILLA_FIVE_HOME=/usr/lib/mozilla
elif [ -f /usr/lib/mozilla/mozilla-bin ]; then
	MOZILLA_FIVE_HOME=/usr/lib/mozilla
elif [ -f $HOME/mozilla/mozilla-bin ]; then
	MOZILLA_FIVE_HOME=$HOME/mozilla
elif [ -f $HOME/mozilla/dist/lib/mozilla-bin ]; then
	MOZILLA_FIVE_HOME=$HOME/mozilla/dist/bin
elif [ -f /opt/gnome/lib/mozilla/mozilla-bin ]; then
	MOZILLA_FIVE_HOME=/opt/gnome/lib/mozilla
elif [ -f /opt/gnome/lib/mozilla-bin ]; then
	MOZILLA_FIVE_HOME=/opt/gnome/lib/
else 
	echo "Cannot find mozilla installation directory. Please set MOZILLA_FIVE_HOME to your mozilla directory."
	exit	
fi

#check to see if $dist_moz exists, create it if not
if test -e $dist_moz; then
    true
else
    mkdir $dist_moz
fi

if test -e $dist_moz/components; then
    true
else
    mkdir $dist_moz/components
fi

for p in `ls $MOZILLA_FIVE_HOME`
do
    if test -e  $dist_moz/`basename $p`; then
	true
    elif test `basename $p` = 'component.reg'; then
	true
    else
	ln -f -s $MOZILLA_FIVE_HOME/`basename $p` $dist_moz/`basename $p`
    fi
done

for p in `ls $MOZILLA_FIVE_HOME/components`
do
    if test -e  $dist_moz/components/`basename $p`; then
	true
    elif test `basename $p` = 'compreg.dat'; then
	true
    else
	ln -f -s $MOZILLA_FIVE_HOME/components/`basename $p` $dist_moz/components/`basename $p`
    fi
done


##echo "Forcing jc links"
##ln -f -s $dist_moz/components/libjcloader.so $dist_components/libjcloader.so 
##ln -f -s $dist_moz/components/libjcjavaservices.so $dist_components/libjcjavaservices.so

export MOZILLA_FIVE_HOME=$dist_moz

#check to see if $local_components exists, create it if not
if test -e $local_components; then
    true
else
    mkdir $local_components
fi

for p in `ls $dist_components | grep -v xpti.*\.dat`
do
    if test -e  $local_components/`basename $p`; then
	true
    else
	ln -f -s $dist_components/`basename $p` $local_components/`basename $p`
    fi
done

export LD_LIBRARY_PATH=$dist_lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$MOZILLA_FIVE_HOME:$MOZILLA_FIVE_HOME/components:$LD_LIBRARY_PATH

SASH_HOME=$sashdir
SASH_COMPONENTS=$local_components
SASH_SHARE=$dist_share
SASH_BIN=$dist_bin
export SASH_HOME SASH_COMPONENTS SASH_SHARE SASH_BIN
for k in "$@"
do
	args="$args \"$k\""
done
eval $dist_bin/$BINARY_NAME-bin $args
