
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Jibtool

GUID generator

*****************************************************************/

#include "SashGUID.h"
#include <string>
#include <ctype.h>
#include "sash_constants.h"

#ifdef __cplusplus
extern "C" {
#endif
#include <uuid/uuid.h>
#ifdef __cplusplus
}
#endif

using namespace std;


string GetNewGUID() {
  uuid_t newid;
  char outid[39];

  uuid_generate_random(newid);
  uuid_unparse(newid, outid + 1);
  outid[0] = '{';
  outid[37] = '}';
  outid[38] = '\0';

  return ToUpper(outid);
}

// added by AJ, 9/01
bool VerifyGUID(const string& s) {
	 // verify that the guid is okay; supposed to look like this:
	 // {F0F0756B-33B0-45D7-AF2B-2ABABF5CFCE6}
	 if (s.length() != 38 || s[0] != '{' || s[37] != '}') 
		  return false;
	 int go[] = {8, 4, 4, 4, 12, 0};
	 int* g = go; // stupid C-ism
	 int gpos = 1;
	 while (*g != 0) {
		  while ((*g)-- != 0) {
			   if (! isxdigit(s[gpos++])) return false;
		  }
		  if (*(++g) != 0 && s[gpos++] != '-') return false;
	 }
	 return true;
}
