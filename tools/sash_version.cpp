#include "sash_version.h"
#include <string>
#include <stdio.h>
#include <strstream>
Version::Version() {
	 major = major1 = minor = maintenance = 0;
}

Version::Version(const Version& b) {
	 major = b.major;
	 major1 = b.major1;
	 minor = b.minor;
	 maintenance = b.maintenance;
}

Version::Version(unsigned int maj, unsigned int maj1, unsigned int min, unsigned int maint) {
	 major = maj; major1 = maj1; minor = min; maintenance = maint;
}

Version::Version(const string& s) {
	 unsigned int m = 0, m1 = 0, mi = 0, ma = 0;
	 sscanf(s.c_str(), "%u.%u.%u.%u", &m, &m1, &mi, &ma);
	 major = m; major1 = m1; minor = mi; maintenance = ma;
}

bool Version::operator>(const string& a) const {
	 const Version b(a);
	 return (*this) > b;
}

bool Version::operator>(const Version& b) const {
	 if (major > b.major) return true; if (major < b.major) return false;
	 if (major1 > b.major1) return true; if (major1 < b.major1) return false;
	 if (minor > b.minor) return true; if (minor < b.minor) return false;
	 if (maintenance > b.maintenance) return true; return false;
}

bool Version::operator==(const string& a) const {
	 const Version b(a);
	 return (*this) == b;
}

bool Version::operator==(const Version& b) const {
	 return (major == b.major &&
			 major1 == b.major1 && 
			 minor == b.minor && 
			 maintenance == b.maintenance);
}

Version& Version::operator=(const string& b) {
	 *this = Version(b);
	 return *this;
}

string Version::ToString() const {
	 ostrstream o;
	 o << major << '.' << major1 << '.' << minor << '.' << maintenance << '\0';
	 return string(o.str());
}

