
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

// Some functions for fooling around with URL strings

// AJ Shankar
// June 2001

#ifndef _URL_TOOLS_H_
#define _URL_TOOLS_H_

#include <string>
#include <glib.h>

// returns true if string starts with a protocol (i.e. "http://", "file://", etc.)
inline bool URLHasProtocol(const std::string& url) {
	 return (url.find("://") != std::string::npos);
}

// returns the protocol, or empty string if it has none
inline std::string URLGetProtocol(const std::string& url) {
  	 string::size_type p = url.find("://");
	 if (p == std::string::npos) return "";
	 return url.substr(0, p);
}

// returns the substring after the rightmost '/'
inline std::string URLGetFileName(const std::string& url) {
	 string::size_type p = url.rfind('/');
	 return url.substr((p == std::string::npos) ? 0 : p+1, url.size());
}

// returns the substring to the left of the rightmost '/'
inline std::string URLGetDirectory(const std::string& url) {
	 string::size_type p = url.rfind('/');
	 return (p == std::string::npos) ? "" : url.substr(0, p);
}

// takes a local file, makes it absolute, and prepends "file://"
inline std::string URLMakeLocalBrowserCompatible(const std::string& url) {
	 string ret(url);
	 if (! g_path_is_absolute(url.c_str())) {
		  gchar* g = g_get_current_dir();
		  ret = (char*) g + string("/") + ret;
		  g_free(g);
	 } 
	 return "file://" + ret;
}

#endif // _URL_TOOLS_H_
