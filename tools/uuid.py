#!/usr/bin/env python
'''
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Code that deals with guids.
'''

import re
import sys
import os

reg = re.compile(r'(.{8})-(.{4})-(.{4})-(.{2})(.{2})-' + r'(.{2})' * 6)

def getUUID():
    '''Return a UUID as a string'''
    return os.popen('uuidgen').read()[:-1]
    
def conv(uuid):
    m = reg.match(uuid)
    if not m:
        print "Didn't match regular expression"
        sys.exit(1)
    res = '{0x%s, 0x%s, 0x%s, {0x%s, 0x%s, 0x%s, 0x%s, 0x%s, 0x%s, 0x%s, 0x%s}}' % m.groups()
    return res

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "Usage: %s <uuid>" % (sys.argv[0])
        sys.exit(1)
    print conv(sys.argv[1])
