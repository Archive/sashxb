
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef DEBUGMSG_H
#define DEBUGMSG_H

/**
   The first argument to DEBUGMSG is the debug group that the messages
   should go into. A message will only be printed if you have the
   variable "D_" + the group in your environment. 

   If you do not define the DEBUG symbol while compiling, the macro
   expands to nothing. If you do have it set, it will print a message
   when you tell it to.

   The other arguments are treated like a call to printf (so the first 
   argument is a format string and the other arguments are the variables
   to substitute into the format string.

   For example:

   DEBUGMSG(foo, "I have %d fingers\n", 1024);

   This will print the string "I have 1024 fingers" to stdout iff you
   have the D_foo environment variable set (by typing "export D_foo=1"
   at the shell, for example)
*/

#if SASH_DEBUG
#include <stdio.h>
#include <stdlib.h>
#define DEBUGMSG(group, args...) { if (getenv("D_" #group)) { \
   fprintf(stderr, #group ": "); \
   fprintf(stderr, ## args); \
   fflush(stderr); \
} }
#else 
#define DEBUGMSG(group, args...)
#endif
#endif // DEBUGMSG_H
