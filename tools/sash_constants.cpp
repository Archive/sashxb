#include "debugmsg.h"
#include "sash_constants.h"
#include "sash_error.h"
#include "FileSystem.h"
#include "config.h"

#include <unistd.h>
//#include <sys/types.h>
//#include <sys/wait.h>
#include <sys/utsname.h>

using namespace std;
const int DEFAULT_PORT = 21337;
const char * SASH_VERSION = VERSION;
const char * CACHEDIR = "cache";
const char * DATADIR = "data";
const char * BINDIR = "bin";
const char * COMPONENTDIR = "components";
const char * RUNTIME_NAME = "sash-runtime";
const char * PLATFORM = "Linux";
const char * DEFAULT_EXTENSIONS[] = {
	 "{70BCBE85-0567-4FFC-879E-CF31D15FC5B4}",
	 "{CBE9BE22-45B5-41A7-9E5F-4D260E22724E}",
	 NULL
};

const char * GlobalSecurityFileName = "security.dat";

string GetSashComponentsDirectory() {
  DEBUGMSG(tools, "Attempting to determine components directory...\n");

	 string my_dir;

	 // try to determine the sash installation directory
	 // first, check sash_components
	 char* test = getenv("SASH_COMPONENTS");
	 if (test == NULL) {
		  // no environment variable, so try default
		  my_dir = string("/usr/lib/SashXB/components");
	 } else {
		  my_dir = test;
	 }

	 DEBUGMSG(tools, "Looking for '%s'\n", my_dir.c_str());
	 // make sure this directory exists
	 if (! FileSystem::FolderExists(my_dir)) {
		  OutputError("Cannot locate Sash components directory! Consider reinstalling Sash.");
	 }

	 return my_dir;
}

string GetSashShareDirectory() {
	 DEBUGMSG(tools, "Attempting to determine share direcotry...\n");
	 string my_dir;

	 // try to determine the sash installation directory
	 // first, check sash_components
	 char* test = getenv("SASH_SHARE");
	 if (test == NULL) {
		  // no environment variable, so try default
		  my_dir = string("/usr/share/SashXB");
	 } else {
		  my_dir = test;
	 }
	   
	 DEBUGMSG(tools, "Looking for '%s'\n", my_dir.c_str());
	 // make sure this directory exists
	 if (! FileSystem::FolderExists(my_dir)) {
		  OutputError("Cannot locate Sash share directory! Consider reinstalling Sash.");
	 }
	   
	 return my_dir;
}

/* 
void ExecWithResult(char ** prog, string & result) {
	 int fd[2];

	 pipe(fd);
	 int pid = fork();
	 if (pid == 0) {
		  close(1);
		  dup(fd[1]);
		  close(fd[0]);
		  execvp(prog[0], prog);
		  _exit(0);
	 }
	 else if (pid > 0) {
		  char buf[128];
		  int bytes;

		  close(fd[1]);
		  while ((bytes = read(fd[0], buf, 127)) > 0) {
			   buf[bytes] = '\0';
			   result += buf;
		  }
		  close(fd[0]);
		  waitpid(pid, NULL, 0);
		  if (result.size() > 0 && result[result.size() - 1] == '\n')
			   result.erase(result.size() - 1, 1);
	 }
}
*/

struct file_dist_entry {
	 const char * file;
	 const char * dist;
};

static file_dist_entry file_dist_map[] = {
	 {"/etc/turbolinux-release", "TurboLinux"},
	 {"/etc/mandrake-release", "Mandrake"},
	 {"/etc/SuSE-release", "SuSE"},
	 {"/etc/redhat-release", "RedHat"},
	 {"/etc/debian_version", "Debian"},	 
	 {"/etc/yellowdog-release", "YellowDog"}
};

static unsigned int file_dist_map_size = 
(sizeof(file_dist_map)/sizeof(file_dist_entry));

void GetDist(string & name) {
// we could check distributions based on OS/architecture (if necessary)

	 for (unsigned int i = 0; i < file_dist_map_size; i++) {
		  if (FileSystem::FileExists(file_dist_map[i].file)) {
			   name = file_dist_map[i].dist;
			   return;
		  }
	 }
	 name = "Unknown";
}

void ModifyArchitecture(string & arch) {
	 // change i?86 to x86
	 if (arch.length() == 4) {
		  if (arch[0] == 'i' && arch[2] == '8' && arch[3] == '6')
			   arch = "x86";
	 }
}

void GetSashSystemInfo(SashSystemInfo & info) {
	 struct utsname dat;
	 if(uname(&dat) == -1){
		  info.OS = "Unknown";
		  info.Architecture = "Unknown";
	 } else {
		  info.OS = dat.sysname;
		  info.Architecture = dat.machine;
	 }

	 ModifyArchitecture(info.Architecture);
	 GetDist(info.Distribution);
}

