
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Ari Heitner, AJ Shankar

Class to implement communication with the weblication task manager

*****************************************************************/

#include "WeblicationRegister.h"
#include <strstream>

const std::string WeblicationRegister::delim = "!@#";

int WeblicationRegister::setupTCPServer() {
	 tsi.server = gethostbyname("localhost");
	 if (!tsi.server) {
	 	  DEBUGMSG(webreg, "failed to look up \"localhost\"; loopback may be down\n");
		  return -1;
	 }

	 memset( (char*)&tsi.serverAddr, 0, sizeof(tsi.serverAddr));
	 tsi.serverAddr.sin_family = AF_INET;
	 memcpy( (char*)tsi.server->h_addr,
			 (char*)&tsi.serverAddr.sin_addr.s_addr,
			 tsi.server->h_length );
         
	 tsi.serverAddr.sin_port = htons( tsi.portNum );         
	 return 0;
}

void WeblicationRegister::registerMe() {
	 if ( setupTCPServer() ) {
	 	  DEBUGMSG(webreg, "not registering with server\n");
		  return;
	 }
	 tsi.sock = socket(AF_INET, SOCK_STREAM, 0);
	 if (tsi.sock < 0) {
		  DEBUGMSG(webreg, "could not open TCP socket\n");
		  return;
	 }

	 if ( connect(tsi.sock, (sockaddr*)&tsi.serverAddr, 
				  sizeof(tsi.serverAddr) ) < 0 ) {
		  DEBUGMSG(webreg, "could not connect() to task manager server\n");
		  return;
	 }
  
	 ostrstream o;
	 o << webl_name << delim << webl_guid << delim << act_name << delim << loc_name << delim << getpid() << delim <<'\0';
  
	 if ( write(tsi.sock, o.str(), o.pcount()) < 0) {
		  DEBUGMSG(webreg, "could not write to task manager server\n");
		  return;
	 }
}

bool WeblicationRegister::parseRegistrationString(const string& p) {
	 int dl = delim.size();
      
	 unsigned int pos = 0, next;
	 if ((next = p.find(delim, pos)) == string::npos) return false;
	 webl_name = p.substr(pos, next-pos);
	 pos = next+dl;
	 if ((next = p.find(delim, pos)) == string::npos) return false;
	 webl_guid = p.substr(pos, next-pos);
	 pos = next+dl;
	 if ((next = p.find(delim, pos)) == string::npos) return false;
	 act_name = p.substr(pos, next-pos);
	 pos = next+dl;
	 if ((next = p.find(delim, pos)) == string::npos) return false;
	 loc_name = p.substr(pos, next-pos);
	 pos = next+dl;
	 if ((next = p.find(delim, pos)) == string::npos) return false;
	 pid = p.substr(pos, next-pos).c_str();
	 return true;
}

WeblicationRegister::WeblicationRegister(int port) : data(NULL) {
	 tsi.portNum = port;
}
