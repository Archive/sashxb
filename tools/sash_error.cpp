
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Text and graphical error and message functions used throughout sash

*****************************************************************/

// AJ Shankar 
// June 2000
//
// Gnome update -- John Corwin
// July 2000

#include "sash_error.h"
#include <iostream.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include <string>
#include <stack>
#include <gnome.h>

static stack<SashErrorMode> modeStack;
GtkWindow * SashErrorWindow = NULL;

// SashErrorPushMode and SashErrorPopMode -- push and pop the message mode
//   for functions in this module.
SashErrorMode SashErrorPushMode(SashErrorMode mode)
{
	 SashErrorMode oldMode;
	 oldMode = (modeStack.empty()) ? SASH_ERROR_TEXT : modeStack.top();
		 
	 modeStack.push(mode);
	 return oldMode;
}

SashErrorMode SashErrorPopMode()
{
	 SashErrorMode curMode;
	 if (!modeStack.empty()) {
		  curMode = modeStack.top();
		  modeStack.pop();
	 } else
		  curMode = SASH_ERROR_TEXT;
	 return curMode;
}

void SashErrorSetWindow(void * window)
{
  if (window)
    SashErrorWindow = GTK_WINDOW(window); // don't call GTK_WINDOW with NULL
  else
    SashErrorWindow = NULL;
}

// This function can be changed to reroute all messages
void OutputString(const char *s) {
	cerr << s << endl;
}

// Outputs error messages, dies
void OutputError(const char* str, ...) {

  static char output[512];
  va_list vars;
  int returnval;
  va_start(vars, str);
  returnval=vsnprintf(output, 512, str, vars);
  va_end(vars);

  if (!modeStack.empty() && modeStack.top() == SASH_ERROR_GNOME) {
    if (SashErrorWindow) {
		 GtkWidget* w = gnome_error_dialog_parented(_(output), SashErrorWindow);
		 gnome_dialog_run_and_close(GNOME_DIALOG(w));
	} else {
		 GtkWidget* w = gnome_error_dialog(_(output));
		 gnome_dialog_run_and_close(GNOME_DIALOG(w));
	}
  }
  else {
    OutputString(output);
  }

  _exit(1);
}

// Outputs non-error messages, e.g. status/warning
int OutputMessage(const char* str, ...) {

  static char output[512];
  va_list vars;
  int returnval;
  va_start(vars, str);
  returnval=vsnprintf(output, 512, str, vars);
  va_end(vars);

  if (!modeStack.empty() && modeStack.top() == SASH_ERROR_GNOME) {
    if (SashErrorWindow) {
		 GtkWidget* w = gnome_ok_dialog_parented(_(output), SashErrorWindow);
		 gnome_dialog_run_and_close(GNOME_DIALOG(w));
	} else {
		 GtkWidget* w = gnome_ok_dialog(_(output));
		 gnome_dialog_run_and_close(GNOME_DIALOG(w));
	}
  }
  else {
    OutputString(output);
  }

  return returnval;
}


// Outputs a message and gives the user a (yes/no) prompt
bool OutputQuery(const char* str, ...) {

  string input;
  static char output[512];
  va_list vars;
  va_start(vars, str);
  vsnprintf(output, 512, str, vars);
  va_end(vars);

  if (!modeStack.empty() && modeStack.top() == SASH_ERROR_GNOME) {
	   GtkWidget * dialog = gnome_message_box_new(_(output), GNOME_MESSAGE_BOX_QUESTION,
											   GNOME_STOCK_BUTTON_YES,
											   GNOME_STOCK_BUTTON_NO, NULL);

    if (SashErrorWindow)
      gnome_dialog_set_parent(GNOME_DIALOG(dialog),SashErrorWindow);

    return (gnome_dialog_run_and_close(GNOME_DIALOG(dialog)) == 0);
  } else {
    
    while (true) {
      cout << output << " (yes/no): ";
      cin >> input;
      if (input.size() >= 1) {
	if (tolower(input[0]) == 'y') return true;
	else if (tolower(input[0]) == 'n') return false;
      }
    }

  }

}

// Asks a question with three possible responses.
// Returns the index of the response chosen.
int Output3Choice(const char * ch0, const char * ch1, const char * ch2, 
		  const char * str, ...)
{

	 string input;
  static char output[512];
  va_list vars;
  va_start(vars, str);
  vsnprintf(output, 511, str, vars);
  va_end(vars);

  if (!modeStack.empty() && modeStack.top() == SASH_ERROR_GNOME) {
    GtkWidget * dialog = gnome_message_box_new(_(output),GNOME_MESSAGE_BOX_QUESTION,
					       _(ch0),_(ch1),_(ch2),NULL);
    if (SashErrorWindow) {
      gnome_dialog_set_parent(GNOME_DIALOG(dialog),SashErrorWindow);
    }
    int dialog_res= (gnome_dialog_run_and_close(GNOME_DIALOG(dialog)));
    return dialog_res;
  }
  else {
    
    while (true) {
      cout << output << " (" << ch0 << "/" << ch1 << "/" << ch2 << "): ";
	  getline(cin, input);
      if (strcasecmp(input.c_str(), ch0) == 0) return 0;
      else if (strcasecmp(input.c_str(), ch1) == 0) return 1;
      else if (strcasecmp(input.c_str(), ch2) == 0) return 2;
    }
  }
}
