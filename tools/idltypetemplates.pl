
#/***************************************************************
#    Sash for Linux
#    The Sash Runtime for Linux
#
#    Copyright (C) 2000 IBM Corporation
#
#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#    Contact:
#    sashxb@sashxb.org
#
#    IBM Advanced Internet Technology Group
#    c/o Lotus Development Corporation
#    One Rogers Street
#    Cambridge, MA 02142
#    USA
#
#*****************************************************************
#
# Contributor(s): AJ Shankar
#
# Data file for idltofunc.pl; edit this to change idl data types
#
#*****************************************************************/

##### This is the file to edit to add handling of additional datatypes

# to add additional types, add a new hash like the ones below
# any string in the hash can have any variable in the %user_vars hash
# make sure to enclose the strings in single quotes!
# finally, add a reference to your hash in the %conv hash at the bottom

# right now, only the hash keys listed here are supported
# more support will be added as necessary


# do *NOT* edit this hash! (unless you also add support for maintaining it in the main idltofunc.pl file)
%user_vars = (
			  'CONTEXT' => 'cx', # the JavaScript context passed to the function
			  'ARG' => '',		# the 'argv[x]' representation of the parameter
			  'TYPE' => '',		# the data type of the parameter (i.e. int, PRBool, etc.)
			  'PARAM' => '',	# the variable name of the parameter, as specified in the idl
			 );

%integer = (
			'check-param' => 'JSVAL_IS_INT($ARG)',
			'convert-param' => '$TYPE $PARAM = JSVAL_TO_INT($ARG)',
			'declare-returnval' => '$TYPE $PARAM',
			'returnval-argument' => '&$PARAM',
			'convert-returnval' => 'INT_TO_JSVAL($PARAM)',
		   );

%double = (
		   'check-param' => 'JSVAL_IS_DOUBLE($ARG)',
		   'convert-param' => '$TYPE $PARAM = JSVAL_TO_DOUBLE($ARG)',
		   'declare-returnval' => '$TYPE $PARAM',
		   'returnval-argument' => '&$PARAM',
		   'convert-returnval' => 'DOUBLE_TO_JSVAL($PARAM)',
		  );

%string = (
		   'check-param' => 'JSVAL_IS_STRING($ARG)',
		   'convert-param' => 'JSString* pJSStringVar$PARAM = JS_ValueToString($CONTEXT,$ARG);\n' .
                               'char* $PARAM = JS_GetStringBytes(pJSStringVar$PARAM)',
		   'declare-returnval' => 'char* $PARAM',
		   'returnval-argument' => '&$PARAM',
		   'convert-returnval' => 'STRING_TO_JSVAL(JS_NewStringCopyZ($CONTEXT, $PARAM));\n'  . 
		                          'delete[] $PARAM',
		  );

%bool = (
		 'check-param' => 'JSVAL_IS_BOOLEAN($ARG)',
		 'convert-param' => '$TYPE $PARAM = JSVAL_TO_BOOLEAN($ARG)',
		 'declare-returnval' => '$TYPE $PARAM',
		 'returnval-argument' => '&$PARAM',
		 'convert-returnval' => 'BOOLEAN_TO_JSVAL($PARAM)',
		);

%object = (
		   'check-param' => 'JSVAL_IS_OBJECT($ARG)',
		   'convert-param' => '$TYPE* $PARAM = JSVAL_TO_OBJECT($ARG)',
		   'declare-returnval' => '$TYPE* $PARAM',
		   'returnval-argument' => '&$PARAM',
		   'convert-returnval' => 'OBJECT_TO_JSVAL($PARAM)',
		  );

%context = (
			'do-not-count-param' => 1,
			'set-param-name' => '$CONTEXT', # for context, always use the actual js context
#			'check-param' => '',   # no need to check parameters, so do not include
#			'convert-param' => '', # again, nothing to do
#			'declare-returnval' => '',
#			'returnval-argument' => '',
#			'convert-returnval' => '',
		   );

%supports = (
#			'check-param' => '', // no need to check nsISupports params
			 'convert-param' => '$TYPE* $PARAM = ($TYPE *) &($ARG)',
#			 'declare-returnval' => '$TYPE* $PARAM',
			 'returnval-argument' => '($TYPE **) &rval',
#			 'convert-returnval' => '(jsval) $PARAM',
			);


# add a reference to your hash here, along with the data type with which it corresponds 
%conv = (
		 'PRInt32' => \%integer,
		 'int' => \%integer,
		 'long' => \%integer,

		 'double' => \%double,

		 'string' => \%string,

		 'bool' => \%bool,
		 'PRBool' => \%bool,
	 	 'boolean' => \%bool,

		 'JSObject' => \%object,

		 'JSContext' => \%context,

		 'nsISupports' => \%supports
		);


