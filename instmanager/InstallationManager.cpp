
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Keeps track of all installed Sash components

*****************************************************************/

#include <unistd.h>
#include <algorithm>
#include <iterator>
#include <string>
#include "sash_error.h"
#include "wdf.h"
#include "prenv.h"
#include "InstallationManager.h"
#include "sash_constants.h"
#include "SecurityHash.h"
#include "FileSystem.h"
#include "Registry.h"

using namespace std;

Registry* InstallationManager::s_WeblicationRegistry = NULL;
const std::string GlobalRegistryName = "sash.dat";

InstallationManager::InstallationManager() {
	 m_Directory = StaticGetSashInstallDirectory();
	 m_Component = GetSashComponentsDirectory();

	 m_Share = GetSashShareDirectory();

	 DEBUGMSG(instmanager, "Loading in global registry...\n");
 
	 // try opening the registry
	 if (s_WeblicationRegistry == NULL) {
		  s_WeblicationRegistry = new Registry();
		  if (! s_WeblicationRegistry->OpenFile(m_Directory + "/" + GlobalRegistryName)) {
			   DEBUGMSG(instmanager, "InstallManager: [FAILED]\n");
			   OutputError("Error opening Sash global registry!");
		  }
	 }
	 m_pRegistry = s_WeblicationRegistry;

	 DEBUGMSG(instmanager, "[SUCCEEDED]\n");
}

InstallationManager::~InstallationManager() {
	//	 delete m_pRegistry;
}

string InstallationManager::StaticGetSashInstallDirectory() {
  DEBUGMSG(instmanager, "Attempting to determine install directory...\n");

	 string my_dir;

	 // try to determine the sash installation directory
	 // first, check sash_home
	 char* test = PR_GetEnv("SASH_HOME");
	 if (test == NULL) {
		  // no environment variable, so try default
		  my_dir = PR_GetEnv("HOME") + string("/.sash");
	 } else {
		  my_dir = test;
	 }

	 DEBUGMSG(instmanager, "Looking for '%s'\n", my_dir.c_str());
	 // make sure this directory exists
	 if (! FileSystem::FolderExists(my_dir)) {
		  OutputError("Cannot locate Sash install directory! Consider reinstalling Sash.");
	 }


	 return my_dir;

}

void InstallationManager::ReloadGlobalRegistry() {
	 m_pRegistry->OpenFile(m_Directory + "/" + GlobalRegistryName);
}

// notes the installation of a weblication, location, or extension
void InstallationManager::RegisterInstallation(const WDF* w, const string& wdf_location, 
											   vector<string>& uninst_files) {

	 DEBUGMSG(instmanager, "Adding extension %s to global registry...\n", w->GetTitle().c_str());

	 string GUID = w->GetID();
	 m_pRegistry->CreateKey(GUID);
	 
	 RegistryValue v;
	 v.SetValueType(RVT_STRING);
	 v.m_String = w->GetTitle();
	 m_pRegistry->SetValue(GUID, "name", &v);

	 v.SetValueType(RVT_NUMBER);
	 v.m_Number = w->GetExtensionType();
	 m_pRegistry->SetValue(GUID, "type", &v);

	 v.SetValueType(RVT_STRING);
	 v.m_String = w->GetVersion().weblication.ToString();
	 m_pRegistry->SetValue(GUID, "version", &v);

	 v.SetValueType(RVT_STRING);
	 v.m_String = wdf_location;
	 m_pRegistry->SetValue(GUID, "wdf-location", &v);

	 // if we're installing over a previous installation,
	 // we want to take the union of the uninstall files
	 sort(uninst_files.begin(), uninst_files.end());
	 m_pRegistry->QueryValue(GUID, "uninstall-files", &v);
	 vector<string> old = v.m_Array;
	 v.m_Array.clear();
	 sort(old.begin(), old.end());
	 set_union(uninst_files.begin(), uninst_files.end(), old.begin(), old.end(),
			   inserter(v.m_Array, v.m_Array.begin()));
	 v.SetValueType(RVT_ARRAY);
	 m_pRegistry->SetValue(GUID, "uninstall-files", &v);

	 vector<WDFDependency> deps = w->GetDependencies();
	 vector<string> strdeps;
	 vector<WDFDependency>::const_iterator ci = deps.begin(), di = deps.end();
	 while (ci != di) {
		  strdeps.push_back(ci->id);
		  ++ci;
	 }
	 v.SetValueType(RVT_ARRAY);
	 v.m_Array = strdeps;
	 m_pRegistry->SetValue(GUID, "depends-on", &v);
	 
	 string action_loc = GUID + "\\actions";
	 vector<WDFAction> actions = w->GetActions();
	 vector<WDFAction>::const_iterator ai = actions.begin(), bi = actions.end();
	 while (ai != bi) {
		  v.SetValueType(RVT_STRING);
		  v.m_String = ai->name;
		  m_pRegistry->SetValue(action_loc+"\\"+ai->id, "name", &v);
		  v.SetValueType(RVT_STRING);
		  v.m_String = ai->locationid;
		  m_pRegistry->SetValue(action_loc+"\\"+ai->id, "locationid", &v);
		  ++ai;
	 }

	 m_pRegistry->WriteToDisk();

	 DEBUGMSG(instmanager, "[SUCCEEDED]\n");
}

bool InstallationManager::IsInstalled(const string& GUID) const {
	 RegistryValue rv;
	 string my_guid = ToUpper(GUID);
	 
	 m_pRegistry->QueryValue(my_guid, "name", &rv);
	 if (rv.GetValueType() == RVT_STRING)
		  return true;

	 return false;
}

bool InstallationManager::IsExtension(const string& GUID) const {
	 RegistryValue rv;
	 m_pRegistry->QueryValue(GUID, "type", &rv);
	 return (rv.GetValueType() == RVT_NUMBER &&
			 (SashExtensionType) rv.m_Number == SET_EXTENSION);
}

bool InstallationManager::IsLocation(const string& GUID) const {
	 RegistryValue rv;
	 m_pRegistry->QueryValue(GUID, "type", &rv);
	 return (rv.GetValueType() == RVT_NUMBER &&
			 (SashExtensionType) rv.m_Number == SET_LOCATION);
}

void InstallationManager::Uninstall(const string& GUID, bool force) {
	 // what is necessary to uninstall an extension?
	 // first, delete its directory and subdirectory
	 // then, remove its entry from the registry

	 DEBUGMSG(instmanager, "Removing extension %s from global registry\n", GUID.c_str());
	 
	 string weblication_dir = WeblicationDirectory(m_Directory, GUID);
	 string data_dir = DataDirectory(weblication_dir);
	 if (! chdir((weblication_dir + '/' + DATADIR).c_str())) {
		  RegistryValue v;
		  m_pRegistry->QueryValue(GUID, "uninstall-files", &v);
		  vector<string>::iterator ai = v.m_Array.begin(), bi = v.m_Array.end();
		  while (ai != bi) {
			   FileSystem::DeleteFile(data_dir + "/" + *ai);
			   ++ai;
		  }

		  FileSystem::DeleteFolder(weblication_dir + "/" + CACHEDIR);
		  remove(data_dir.c_str());
		  // if data/ directory still exists, let the user know
		  if (FileSystem::FolderExists(weblication_dir + "/" + DATADIR)) {
			   // didn't get the data directory
			   if (! force && OutputQuery("For your convenience, weblication data files "
							   "created after\ninstallation have not "
							   "been deleted. Delete them anyway?")) {
					FileSystem::DeleteFolder(data_dir);
			   }
		  }

		  FileSystem::DeleteFile(weblication_dir + "/" + RegistryFileName(GUID));
		  FileSystem::DeleteFile(weblication_dir + "/" + WDFFileName(GUID));
		  FileSystem::DeleteFile(weblication_dir + "/" + SecurityFileName(GUID));
		  // try to get rid of weblication directory
		  remove(weblication_dir.c_str());
	 }
	 
	 // if it's an extension or location, remove its custom security settings (if any)
	 // from the global security table
	 SashExtensionItem se = GetInfo(GUID);
	 if (se.ext_type != SET_WEBLICATION) {
	   SecurityHash sec_hash(m_Directory + "/" + GlobalSecurityFileName);
	   sec_hash::iterator a = sec_hash.GetHash().begin(), b = sec_hash.GetHash().end(), old;
	   while (a != b) {
		 old = a;
		 ++a;
		 if (old->second.m_Parent == GUID) 
			  sec_hash.GetHash().erase(old);
	   }
	   sec_hash.Save(m_Directory + "/" + GlobalSecurityFileName);
	 }

	 // take care of the registry entries
	 m_pRegistry->DeleteKey(GUID, true);

	 // to avoid corruption, synchronous writes to disk
	 m_pRegistry->WriteToDisk();

	 DEBUGMSG(instmanager, "[SUCCEEDED]\n");

}

gboolean InstallationManager::RemoveSecurityByGuid(gpointer key, gpointer value, gpointer data) {
	 char* c = (char*) data;
	 SecurityItem* si = (SecurityItem*) value;
	 return si->m_Parent == c;
}

bool nocase_prefix_cmp(const string& a, const string& b) {
	 return (strncasecmp(a.c_str(), b.c_str(), a.length()) == 0);
}

// returns all installed items of type s; use SET_UNDEFINED to get everything
vector<SashExtensionItem> InstallationManager::GetInfoAllInstalled(const SashExtensionType s, 
																   const string& name) const {
	 vector<SashExtensionItem> items;
	 vector<string> guids = m_pRegistry->EnumKeys("\\");
	 vector<string>::const_iterator bi = guids.begin(), ei = guids.end();
	 bool search = (name.length() > 0);
	 // if SET_UNDEFINED, return every installed item
	 if (s == SET_UNDEFINED) {
		  DEBUGMSG(instmanager, "Searching for all extensions...\n");
		  while (bi != ei) {
			   SashExtensionItem sei = GetInfo(*bi);
			   if (! search || nocase_prefix_cmp(name, sei.name))
					items.push_back(sei);
			   ++bi;
		  }

		  DEBUGMSG(instmanager, "[SUCCEEDED]\n");
		  return items;
	 }

	 // if s is specified, return only items of that type
	 while (bi != ei) {
		  RegistryValue rv;
		  m_pRegistry->QueryValue(*bi, "type", &rv);
		  if (rv.GetValueType() == RVT_NUMBER && (SashExtensionType) rv.m_Number == s) {
			   SashExtensionItem sei = GetInfo(*bi);
			   if (! search || nocase_prefix_cmp(name, sei.name))
					items.push_back(sei);
		  }

		  ++bi;
	 }

	 DEBUGMSG(instmanager, "[SUCCEEDED]\n");
	 return items;
}

SashExtensionItem InstallationManager::GetInfo(const string& GUID) const {
	 RegistryValue rv;
	 SashExtensionItem i;
	 i.guid = ToUpper(GUID);

	 DEBUGMSG(instmanager, "Getting information for component with GUID %s ...\n", i.guid.c_str());

	 m_pRegistry->QueryValue(i.guid, "name", &rv);
	 if (rv.GetValueType() == RVT_STRING)
		  i.name = rv.m_String;

	 m_pRegistry->QueryValue(i.guid, "type", &rv);
	 if (rv.GetValueType() == RVT_NUMBER)
		  i.ext_type = (SashExtensionType) rv.m_Number;

	 m_pRegistry->QueryValue(i.guid, "version", &rv);
	 if (rv.GetValueType() == RVT_STRING)
		  i.version = rv.m_String;

	 m_pRegistry->QueryValue(i.guid, "wdf-location", &rv);
	 if (rv.GetValueType() == RVT_STRING)
		  i.wdf_location = rv.m_String;

	 m_pRegistry->QueryValue(i.guid, "depends-on", &rv);
	 if (rv.GetValueType() == RVT_ARRAY)
		  i.dependencies = rv.m_Array;

	 string action_dir = i.guid + "\\actions";
	 vector<string> actions = m_pRegistry->EnumKeys(action_dir);
	 DEBUGMSG(instmanager, "%d actions found\n", actions.size());
	 vector<string>::const_iterator ai = actions.begin(), bi = actions.end();
	 while (ai != bi) {
		  SashExtensionItem::Action a;
		  a.id = *ai;
		  m_pRegistry->QueryValue(action_dir+"\\"+*ai, "name", &rv);
		  a.name = rv.m_String;
		  m_pRegistry->QueryValue(action_dir+"\\"+*ai, "locationid", &rv);
		  a.locationid = rv.m_String;
		  
		  i.actions.push_back(a);
		  ++ai;
	 }

	 DEBUGMSG(instmanager, "[SUCCEEDED]\n");
	 return i;
}

string InstallationManager::GetWeblicationDataDirectory(const string &GUID) const {
	return DataDirectory(WeblicationDirectory(GetSashInstallDirectory(), GUID));
}

// Return a list of GUIDs that need to be loaded
// there is a peculiar set of rules for this:
//    1. for the weblication being called, search all extensions
//       and locations in the dependency section recursively
//    2. in lower levels, search only extensions recursively
//    3. only add GUIDs of extensions
//    4. make sure not to follow any circular dependencies
void InstallationManager::GetAllExtensions(const string& webl_guid, 
										   const string& action_guid, 
										   vector<string>& exts) const {
	 RegistryValue rv;
	 GetToplevelExtensions(webl_guid, exts);

	 if (! action_guid.empty()) {
		  m_pRegistry->QueryValue(webl_guid + "\\actions\\" + action_guid, "locationid", &rv);
		  exts.push_back(rv.m_String);
	 }

	 GetAllGivenDependencies(exts);
}

void InstallationManager::GetAllGivenDependencies(vector<string>& deps, 
												  bool return_only_exts) const {
	 DEBUGMSG(instmanager, "Fetching all extensions recursively...\n");
	 strinthash h;

	 vector<string>::iterator bai = deps.begin(), bei = deps.end();
	 while (bai != bei) {
		  RegistryValue rv;
		  m_pRegistry->QueryValue(*bai, "type", &rv);
		  SashExtensionType stype = (SashExtensionType) rv.m_Number;

		  if (stype == SET_EXTENSION || stype == SET_LOCATION) {
			   // add the extension to the table
			   if (stype == SET_EXTENSION || ! return_only_exts) {
					h[*bai] = 1;
			   }
			   GetAllHelper(h, *bai);
		  }
		  ++bai;
	 }

	 deps.clear();
	 for(strinthash::iterator ai = h.begin() ; ai != h.end() ; ai++) {
		  deps.push_back(ai->first);
	 }
}

void InstallationManager::GetAllHelper(strinthash& h, const string& guid) const {
	 vector<string> q;
	 GetToplevelExtensions(guid, q);

	 vector<string>::iterator aid = q.begin(), bid = q.end();
	 while (aid != bid) {
		  // make sure we don't do anything circular
		  if (h.count(*aid) == 0) {
			   DEBUGMSG(instmanager, "Recursively getting extension info for %s\n", aid->c_str());
			   GetAllHelper(h, *aid);
		  }
		  h[*aid] = 1;
		  ++aid;
	 }
}

void InstallationManager::GetToplevelExtensions(const string& guid, vector<string>& exts) const {
	 DEBUGMSG(instmanager, "Fetching toplevel extensions...\n");
	 RegistryValue rv;
	 m_pRegistry->QueryValue(guid, "depends-on", &rv);
	 if (rv.GetValueType() != RVT_ARRAY) {
		  OutputError("No dependency information for this component in the SashXB registry!\n"
					  "Please reinstall it...");
	 }
	 vector<string>::iterator aid = rv.m_Array.begin(), bid = rv.m_Array.end();
	 while (aid != bid) {
		  if (IsExtension(*aid)) exts.push_back(*aid);
		  ++aid;
	 }
}

bool remove_by_name_length(const SashExtensionItem i, unsigned int length) {
	 return (i.name.length() > length);
}

void FindComponentByName(const InstallationManager& im, 
						 const string& p, 
						 vector<SashExtensionItem>& matches,
						 SashExtensionType s)  {
	 matches = im.GetInfoAllInstalled(s, p);
	 if (matches.size() > 1) {
		  vector<SashExtensionItem>::iterator i = 
			   remove_if(matches.begin(), matches.end(), 
						 bind2nd(ptr_fun(remove_by_name_length), p.length()));
		  if (i == matches.begin() + 1)
			   matches.erase(i, matches.end());
	 }
}

// sets the ordered list of locator services to check
void InstallationManager::SetLocatorServices(const bool use_locators, const vector<string>& list,
											 const bool fall_back_on_default) {
	 RegistryValue rv;
	 rv.SetValueType(RVT_NUMBER);
	 rv.m_Number = (float) use_locators;
	 m_pRegistry->SetValue("", "use-locators", &rv);
	 rv.SetValueType(RVT_ARRAY);
	 rv.m_Array = list;
	 m_pRegistry->SetValue("", "locators", &rv);
	 rv.SetValueType(RVT_NUMBER);
	 rv.m_Number = (float) fall_back_on_default;
	 m_pRegistry->SetValue("", "use-default-locator", &rv);
	 m_pRegistry->WriteToDisk();
}

// gets the ordered list of locator services
void InstallationManager::GetLocatorServices(bool& use_locators, vector<string>& list,
											 bool& fall_back_on_default) const {	
	 RegistryValue rv;
	 m_pRegistry->QueryValue("", "use-locators", &rv);
	 use_locators = (bool) rv.m_Number;
	 m_pRegistry->QueryValue("", "locators", &rv);
	 list = rv.m_Array;
	 m_pRegistry->QueryValue("", "use-default-locator", &rv);
	 fall_back_on_default = (bool) rv.m_Number;
}
