
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Tests the installation manager by looking at installed Sash stuff

*****************************************************************/

//
// Installer test
//
//

#include <string>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include "sash_constants.h"
#include "sash_error.h"
#include "InstallationManager.h"
#include "sashtools.h"

using namespace std;

int main(int argc, char* argv[]) {
     InstallationManager im;

	 cout << "Analyzing installed Sash items..." << endl;
	 vector<SashExtensionItem> s = im.GetInfoAllInstalled(SET_UNDEFINED);
	 vector<SashExtensionItem>::iterator bi, ei;
	 bi = s.begin();
	 ei = s.end();
	 while (bi != ei) {
		  SashExtensionItem& sei = *bi;
		  cout << endl << sei.name << ": " << endl <<
			   "\tGUID: " << sei.guid << endl << 
			   "\tType: ";
		  if (sei.ext_type == SET_UNDEFINED) 
			   cout << "[UNDEFINED]";
		  else 
			   cout << (int)sei.ext_type;
		  cout << endl << "\tVersion: " << sei.version << endl;

		  if (sei.ext_type == SET_EXTENSION || sei.ext_type == SET_LOCATION) {
			   cout << "Library location: " <<
					DataDirectory(WeblicationDirectory(im.GetSashInstallDirectory(), sei.guid)) + "/" + LibraryFileName(sei.guid) << endl;
		  }

		  vector<string> a;
		  im.GetAllExtensions(sei.guid, "", a);
		  cout << "Dep extensions: " << endl;
		  copy(a.begin(), a.end(), ostream_iterator<string>(cout, " "));
		  cout << endl;

		  ++bi;
	 }


	 cout << endl << endl;

	 cout << "Uninstalling weblication..." << endl;
	 im.Uninstall("{3113EF09-4484-447A-8385-DEADBEEFD2F0}");

	 cout << endl;

	 exit(0);
}







