
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Keeps track of all installed Sash components

*****************************************************************/

#ifndef _INSTALLATION_MANANGER_H_
#define _INSTALLATION_MANANGER_H_

// Installation Manager
// AJ Shankar July 2000

// Keeps track of installed weblications, locations, and passive objects
// Should be used all over the place: at install time, with the task manager, and at runtime
// *THIS* is the place to get the sash install directory

#include <string>
#include <hash_map>
#include "Registry.h"
#include "wdf.h"
#include "sash_constants.h"
#include "debugmsg.h"
#include "SecurityHash.h"

typedef hash_map<string, int, strhash> strinthash;

struct SashExtensionItem {
	 struct Action { std::string id, name, locationid; };
	 
	 SashExtensionItem() : ext_type(SET_UNDEFINED) {}

	 SashExtensionType ext_type;
	 std::string guid, name, version, wdf_location;
	 vector<string> dependencies;
	 vector<Action> actions;
};

class InstallationManager {
public:
	 InstallationManager();
	 ~InstallationManager();

	 // call this function if you want the InstManager to guess the directory
	 // without being instantiated
	 static std::string StaticGetSashInstallDirectory();

	 // call this function if you have an instantiated installation manager
	 std::string GetSashInstallDirectory() const { return m_Directory; }

	 // forces a reload of the global registry from the disk
	 void ReloadGlobalRegistry();

	 // notes the installation of a weblication, location, or extension
	 void RegisterInstallation(const WDF* w, const std::string& wdf_location, 
							   std::vector<std::string>& uninstall_files);

	 bool IsInstalled(const std::string& GUID) const;

	 void Uninstall(const std::string& GUID, bool force = false);

	 // returns information about a specific extension
	 // if item is not found, ExtensionType is SE_UNDEFINED
	 SashExtensionItem GetInfo(const std::string& GUID) const;
	 
	 // returns true iff the GUID is one of an extension
	 bool IsExtension(const std::string& GUID) const;

	 // returns true iff the GUID is one of a location
	 bool IsLocation(const std::string& GUID) const;
   
	 // returns all installed items of type s; use SET_UNDEFINED to get everything
	 std::vector<SashExtensionItem> GetInfoAllInstalled(const SashExtensionType s, 
														const std::string& name_begins_with = "") const;
	 
 	 string GetWeblicationDataDirectory(const std::string &GUID) const;

	 // Sets exts to a list of GUIDs that need to be registered at runtime
	 void GetAllExtensions(/* in */ const std::string& webl_guid, 
                           /* in */ const std::string& action_guid, 
                           /* out */ std::vector<std::string>& exts) const;
	 // call this if you already have a list of toplevel dependencies
	 // if return_only_exts is set to false, will return any locations, too
	 void GetAllGivenDependencies(/* in/out */ std::vector<std::string>& deps, 
                                  /* in */ bool return_only_exts = true) const;
	 
	 // Returns a list of the guids of the extensions that need to be loaded at runtime
	 void GetToplevelExtensions(/* in */ const std::string& guid, 
                                /* out */ std::vector<std::string>& exts) const;

	 // sets the ordered list of locator services to check
	 void SetLocatorServices(const bool use_locators, const std::vector<std::string>& list,
							 const bool fall_back_on_default);
	 // gets the ordered list of locator services
	 void GetLocatorServices(/* all out */ bool& use_locators, std::vector<std::string>& list,
							 bool& fall_back_on_default) const;

private:
	 static Registry* s_WeblicationRegistry;

	 // sash global registry
	 Registry* m_pRegistry;

	 // the sash install directory
	 std::string m_Directory;
	 std::string m_Component;
	 std::string m_Share;

	 // for uninstalling extensions
	 static gboolean RemoveSecurityByGuid(gpointer key, gpointer value, gpointer data);

	 // helper for GetAllExtensions
	 void GetAllHelper(strinthash& h, const std::string& guid) const;
};

// this helper function attempts to locate a Sash component by name prefix
// if it finds nothing, returns empty vector
// if it finds several matches, but one is the same length as the given prefix
// it returns that one
// otherwise, it returns all components that start with that prefix
void FindComponentByName(const InstallationManager& im, 
						 const string& prefix_or_full_name,
						 /* out */ vector<SashExtensionItem>& matches,
						 SashExtensionType s = SET_UNDEFINED);
#endif // _INSTALLATION_MANANGER_H_
