#ifndef SASHNET_H
#define SASHNET_H

#include "streamBuffer.h"

const int RECEIVE_BUFSIZE = 2048;
const int SEND_BUFSIZE = 1024;
const int LINE_BUFSIZE = 128;
const int LIST_BUFSIZE = 256;

void setRemoteAddr(struct sockaddr_in *remoteAddr,
				   const char *remoteHost, unsigned short remotePort);
bool bindSocket(int sockfd, int port);
int openConnection(struct sockaddr_in *remoteAddr);
int fullSend(int sockfd, int size, char * data);
int receiveAllFromConnection(int dataSockFD, streamBuffer *sb);

int sendNormalFTPCommand (int sockfd, const char *ftpCommand, 
						  const char *argstr);
int receiveFTPResponse(int sockfd, streamBuffer **response = NULL);
bool processFTPResponseCode(int responseCode, string **errorMessage);
int parsePASVResponse(streamBuffer *sb, struct sockaddr_in *dataAddr);


#endif
