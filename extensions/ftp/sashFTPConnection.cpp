
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation of the FTP Connection object

not quite done yet - 
i don't handle a lot of the response codes returned from the 
ftp server.

******************************************************************/

#include "sashFTPConnection.h"

#include "sash_constants.h"
#include "sashISashRuntime.h"
#include "sashIActionRuntime.h"
#include "sashGenericConstructor.h"
#include "DataManager.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "extensiontools.h"
#include <unistd.h>
#include "secman.h"
#include "sashnet.h"
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <glib.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <iostream.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "streamBuffer.h"
#include "ftpcodes.h"
#include "sashFTP.h"

NS_IMPL_ISUPPORTS3(sashFTPConnection, sashIFTPConnection, sashIUnknown, sashIConstructor);
UNKNOWN_IMPL(sashFTPConnection, SASHIFTPCONNECTION, sashIFTPConnection);

sashFTPConnection::sashFTPConnection()
{
	 DEBUGMSG(ftp, "ftp connection constructor\n");
	 NS_INIT_ISUPPORTS();

	 m_connected = false;

	 m_localPort = 0;
	 m_mainSockFD = -1;
	 m_dataSockFD = -1;
	 m_loginMessage = NULL;
	 m_notifyIntervalK = -1;
}

sashFTPConnection::~sashFTPConnection()
{
	 pthread_mutex_destroy(&m_connectionLock);
	 pthread_mutex_destroy(&m_transferLock);
	 pthread_mutex_destroy(&m_idleLock);
	 pthread_cond_destroy(&m_idleCond);
}

NS_IMETHODIMP
sashFTPConnection::InitializeNewObject(sashIExtension * ext, JSContext * cx, 
							  sashIVariant **argv, PRUint32 argc, PRBool *ret)
{
	 DEBUGMSG(ftp, "initializing new ftp connection object\n");
	 m_cx = cx;

	 pthread_mutex_init(&m_connectionLock, NULL);
	 pthread_mutex_init(&m_transferLock, NULL);

	 pthread_mutex_init(&m_idleLock, NULL);
	 pthread_cond_init(&m_idleCond, NULL);

	 *ret = true;

	 start();
	 return NS_OK;
}

static void * start_run(void *manager){
	 DEBUGMSG(ftp, "about to run...\n");
	 sashFTPConnection *c = (sashFTPConnection *) manager;
	 c->run();
	 return NULL;
}

bool sashFTPConnection::start(){
	 DEBUGMSG(ftp, "starting ftp connection thread...\n");

	 pthread_attr_t attr;
	 pthread_attr_init(&attr);

	 lockConnection();
	 int res = pthread_create(&m_transferFiles, &attr, 
							  start_run, (void *) this);  
	 m_running = (res == 0);
	 unlockConnection();
	 
	 return m_running;
}

void sashFTPConnection::waitForFiles()
{
	 DEBUGMSG(ftp, "sashFTPConnection::waitForFiles\n");
	 pthread_mutex_lock(&m_idleLock);

	 lockTransferQueue();
	 while (m_transferQueue.empty()) {
		  unlockTransferQueue();
		  DEBUGMSG(ftp, "keep waiting for data\n");
		  pthread_cond_wait(&m_idleCond, &m_idleLock);
		  lockTransferQueue();
	 }
	 unlockTransferQueue();
	 pthread_mutex_unlock(&m_idleLock);
}

void sashFTPConnection::run()
{
	 transferRequest *currReq;
	 m_running = true;
	 while (m_running){
		  
		  // sleep until there's a file to be sent.
		  waitForFiles();
		  
		  // when a file is supposed to be sent or received, wake up,
		  // send files.
		  lockTransferQueue();
		  while (! m_transferQueue.empty()){
			   currReq = m_transferQueue.front();
			   m_transferQueue.pop_front();
			   unlockTransferQueue();
			   if (currReq->isGet){
					getSingleFile(currReq->filename);
			   } else {
					DEBUGMSG(ftp, "putting single file: %s\n", 
							 currReq->filename.c_str());
					putSingleFile(currReq->filename);
			   }
			   lockTransferQueue();
		  }
		  unlockTransferQueue();
	 }
}

/* readonly attribute PRBool Connected; */
NS_IMETHODIMP sashFTPConnection::GetConnected(PRBool *aConnected)
{
	 lockConnection();
	 *aConnected = m_connected;
	 unlockConnection();
	 return NS_OK;
}

/* PRBool Connect (in string remoteAddr, in PRInt32 remotePort, in sashIVariant localPort); */
NS_IMETHODIMP sashFTPConnection::Connect(const char *remoteAddr, PRInt32 remotePort, sashIVariant *localPort, PRBool *_retval)
{
	 // set up remote address
	 setRemoteAddr((struct sockaddr_in *)&m_remoteAddr, remoteAddr, 
				   remotePort);

	 m_localPort 
		  = (VariantIsNull(localPort))? 0 : (int)(VariantGetNum(localPort));

	 // call socket
	 // call bind on localport (if given)
	 if ((m_mainSockFD = openConnection(&m_remoteAddr))==-1){
		  DEBUGMSG(ftp, "couldn't open connection\n");
	 }

	 int responseCode = receiveFTPResponse(m_mainSockFD);
	 DEBUGMSG(ftp, "connected response code: %d\n", responseCode);

	 *_retval = true;
	 m_connected = true;
	 return NS_OK;
}

/* PRBool Login (in string username, in string password); */
NS_IMETHODIMP sashFTPConnection::Login(const char *username, const char *password, PRBool *_retval)
{
	 (*_retval) = true;
	 sendNormalFTPCommand (m_mainSockFD, "user", username);
	 int responseCode = receiveFTPResponse(m_mainSockFD);

	 sendNormalFTPCommand (m_mainSockFD, "pass", password);

	 streamBuffer *lm = new streamBuffer();
	 responseCode = receiveFTPResponse(m_mainSockFD, &lm);
	 
	 int size = lm->getSize();
	 
	 if (m_loginMessage){
		  delete [] m_loginMessage;
	 }
	 m_loginMessage = new char [size + 1];
	 lm->getData(-1, m_loginMessage);
	 m_loginMessage[size] = '\0';

	 delete lm;

	 // handle login code. 
	 if (responseCode == LOGIN_INCORRECT){
		  (*_retval) = false;
	 }
	 return NS_OK;
}

/* PRBool Logout (); */
NS_IMETHODIMP sashFTPConnection::Logout(PRBool *_retval)
{
	 sendNormalFTPCommand (m_mainSockFD, "quit", NULL);
	 int responseCode = receiveFTPResponse(m_mainSockFD);

	 // does it matter what response code this is?
	 return NS_OK;
}

/* readonly attribute string currentDirectory; */
NS_IMETHODIMP sashFTPConnection::GetCurrentDirectory(char * *aCurrentDirectory)
{
	 sendNormalFTPCommand (m_mainSockFD, "pwd", NULL);
	 streamBuffer *sb = new streamBuffer();
	 int responseCode = receiveFTPResponse(m_mainSockFD, &sb);

	 char * buf;

	 switch(responseCode){
	 case DIRECTORY_ACTION_NOT_TAKEN:
		  buf = new char [1];
		  buf[0] = '\0';
		  break;
	 case DIRECTORY_ACTION_COMPLETED:
		  DEBUGMSG(ftp, "%d bytes in sb\n", sb->getSize());
		  
		  int leftQuote = sb->findInBuffer('"');
//		  DEBUGMSG(ftp, "leftQuoteat: %d\n", leftQuote);

		  if (leftQuote != -1)
			   sb->getData(leftQuote + 1, NULL);

		  int rightQuote = sb->findInBuffer('"');
//		  DEBUGMSG(ftp, "rightQuoteat: %d\n", rightQuote);

		  if ((leftQuote == -1) || (rightQuote == -1)){
			   DEBUGMSG(ftp, "return from pwd not well-formed!\n");
		  }

		  int len = rightQuote;
		  buf = new char [len + 1];
		  sb->getData(len, buf);
		  buf[len] = '\0';
//		  DEBUGMSG(ftp, "pwd: %s\n", buf);
		  break;
	 }
	 delete sb;
//	 DEBUGMSG(ftp, "pwd: %s\n", buf);
	 XP_RETURN_STRING(buf, aCurrentDirectory);
}

/* attribute PRInt32 NotifyInterval_K; */
NS_IMETHODIMP sashFTPConnection::GetNotifyInterval_K(PRInt32 *aNotifyInterval_K)
{
	 *aNotifyInterval_K = m_notifyIntervalK;
	 return NS_OK;
}

NS_IMETHODIMP sashFTPConnection::SetNotifyInterval_K(PRInt32 aNotifyInterval_K)
{
	 m_notifyIntervalK = aNotifyInterval_K;
	 return NS_OK;
}

/* readonly attribute string LoginMessage; */
NS_IMETHODIMP sashFTPConnection::GetLoginMessage(char * *aLoginMessage)
{
	 DEBUGMSG(ftp, "trying to get login message...\n");
	 XP_RETURN_STRING (m_loginMessage, aLoginMessage);
}

/* PRInt32 get (in sashIVariant fileNames); */
NS_IMETHODIMP sashFTPConnection::Get(sashIVariant *fileNames, PRInt32 *_retval)
{
	 sashIVariant *currItem;
	 transferRequest currReq;
	 string currFileName;
	 *_retval = 0;

	 // check to see whether it's a single file.
	 if (VariantIsString(fileNames)){
		  currReq.isGet = true;
		  currReq.filename = VariantGetString(fileNames);
		  lockTransferQueue();
		  m_transferQueue.push_back(&currReq);
		  unlockTransferQueue();
		  (*_retval) ++;
	 } else if (VariantIsArray(fileNames)){
		  for (unsigned int i=0; i < VariantGetArrayLength(fileNames); i++){
			   currItem = VariantGetArrayItem(fileNames, i);
			   assert (VariantIsString(currItem));
			   
			   currReq.isGet = true;
			   currReq.filename = VariantGetString(currItem);
			   lockTransferQueue();
			   m_transferQueue.push_back(&currReq);
			   unlockTransferQueue();
			   (*_retval) ++;
		  }
	 }

	 // if files were added, then signal.
	 if (*_retval > 0){
		  pthread_mutex_lock(&m_idleLock);
		  pthread_cond_signal(&m_idleCond);
		  pthread_mutex_unlock(&m_idleLock);
	 }
	 return NS_OK;
}

int sashFTPConnection::getSingleFile(string fileName){
	 callOnFileReceiveStartEvent(this, fileName);

	 sendNormalFTPCommand(m_mainSockFD, "pasv", NULL);
	 streamBuffer *sb = new streamBuffer();
	 int responseCode = receiveFTPResponse(m_mainSockFD, &sb);
	 parsePASVResponse(sb, &m_dataAddr);
	 sb->clear();
	 
	 if ((m_dataSockFD = openConnection(&m_dataAddr))==-1){
		  DEBUGMSG(ftp, "couldn't open connection\n");
	 }

	 const char *filenameCstr = fileName.c_str();

	 sendNormalFTPCommand(m_mainSockFD, "retr", filenameCstr);
	 // receive the start transfer message (150)
	 responseCode = receiveFTPResponse(m_mainSockFD);

	 switch (responseCode){
	 case FILE_START_TRANSFER:
		  // started transfer.
		  break;
	 case FILE_ACTION_NOT_TAKEN:
		  // close the data connection.
		  close(m_dataSockFD);
		  callOnFileReceiveErrorEvent(this, fileName);
		  return -1;
		  break;
	 default:

		  DEBUGMSG(ftp, "unexpected reply to retr: %d\n", responseCode);
		  break;
	 }

	 int received = receiveIntoFile(m_dataSockFD, fileName);
	 DEBUGMSG(ftp, "received %d bytes for file: \n", received);

	 // receive the transfer complete message (226)
	 responseCode = receiveFTPResponse(m_mainSockFD);
	 switch (responseCode){
	 case FILE_TRANSFER_COMPLETE:
		  DEBUGMSG(ftp, "file received successfully.\n");
		  break;
	 }

	 callOnFileReceivedEvent(this, fileName);
	 return 1;
}


/* PRInt32 put (in sashIVariant fileNames); */
NS_IMETHODIMP sashFTPConnection::Put(sashIVariant *fileNames, PRInt32 *_retval)
{
	 transferRequest currReq;
	 sashIVariant *currItem;
	 string currFileName;
	 *_retval = 0;

	 // check to see whether it's a single file.
	 if (VariantIsString(fileNames)){
		  currReq.isGet = false;
		  currReq.filename = VariantGetString(fileNames);
		  lockTransferQueue();
		  m_transferQueue.push_back(&currReq);
		  unlockTransferQueue();
		  (*_retval) ++;
	 } else if (VariantIsArray(fileNames)){
		  for (unsigned int i=0; i < VariantGetArrayLength(fileNames); i++){
			   currItem = VariantGetArrayItem(fileNames, i);
			   assert (VariantIsString(currItem));

			   currReq.isGet = false;
			   currReq.filename = VariantGetString(currItem);
			   lockTransferQueue();
			   m_transferQueue.push_back(&currReq);
			   unlockTransferQueue();
			   (*_retval) ++;
		  }
	 }
	 // if files were added, then signal.
	 if (*_retval > 0){
		  pthread_mutex_lock(&m_idleLock);
		  pthread_cond_signal(&m_idleCond);
		  pthread_mutex_unlock(&m_idleLock);
	 }

	 return NS_OK;
}

int sashFTPConnection::putSingleFile(string fileName){
	 callOnFileSendStartEvent(this, fileName);

	 sendNormalFTPCommand(m_mainSockFD, "pasv", NULL);
	 streamBuffer *sb = new streamBuffer();
	 int responseCode = receiveFTPResponse(m_mainSockFD, &sb);
	 parsePASVResponse(sb, &m_dataAddr);
	 sb->clear();
	 
	 if ((m_dataSockFD = openConnection(&m_dataAddr)) == -1){
		  DEBUGMSG(ftp, "couldn't open connection\n");
	 }

	 sendNormalFTPCommand(m_mainSockFD, "stor", fileName.c_str());
	 // receive the start transfer message (150)
	 responseCode = receiveFTPResponse(m_mainSockFD);

	 switch (responseCode){
	 case FILE_START_TRANSFER:
		  // started transfer.
		  break;
	 case FILE_ACTION_NOT_TAKEN:
		  // close the data connection.
		  close(m_dataSockFD);
		  callOnFileSendErrorEvent(this, fileName);
		  return -1;
		  break;
	 default:
		  DEBUGMSG(ftp, "unexpected reply to stor: %d\n", responseCode);
		  break;
	 }

	 int filefd = sashFTP::m_fs->OpenFile(fileName, true);
	 int filesize = FileSystem::FileSize(fileName);
	 int amtRead = 0;
	 int amtLeft = filesize;

	 char *databuf = new char [SEND_BUFSIZE];
	 while(amtLeft != 0){
		  amtRead = sashFTP::m_fs->ReadFromFile(databuf, SEND_BUFSIZE, filefd);
		  fullSend(m_dataSockFD, amtRead, databuf);
		  amtLeft -= amtRead;
	 }
	 sashFTP::m_fs->CloseFile(filefd);
	 close(m_dataSockFD);

	 // receive the transfer complete message (226)
	 responseCode = receiveFTPResponse(m_mainSockFD);
	 switch (responseCode){
	 case FILE_TRANSFER_COMPLETE:
		  DEBUGMSG(ftp, "file sent successfully.\n");
		  break;
	 }

	 callOnFileSentEvent(this, fileName);
	 return 1;
}

/* PRBool cd (in string directory); */
NS_IMETHODIMP sashFTPConnection::Cd(const char *directory, PRBool *_retval)
{
	 sendNormalFTPCommand (m_mainSockFD, "cwd", directory);
	 streamBuffer *sb = new streamBuffer();
	 int responseCode = receiveFTPResponse(m_mainSockFD, &sb);

	 switch(responseCode){
	 case CD_ACTION_NOT_TAKEN:
		  *_retval = false;
		  break;
	 case CD_ACTION_COMPLETED:
		  *_retval = true;
		  break;
	 }
	 return NS_OK;
}

/* sashIVariant list (in sashIVariant dirName); */
NS_IMETHODIMP sashFTPConnection::List(sashIVariant *dirName, 
									  sashIVariant **_retval)
{
	 if(! genericList("nlst", dirName, _retval)){
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}

/* sashIVariant detailedlist (in sashIVariant dirName); */
NS_IMETHODIMP sashFTPConnection::Detailedlist(sashIVariant *dirName, sashIVariant **_retval)
{
	 if(! genericList("list", dirName, _retval)){
		  return NS_ERROR_FAILURE;
	 }
	 return NS_OK;
}

bool sashFTPConnection::genericList(const char *dirCommand,
									sashIVariant *dirName, 
									sashIVariant **_retval){
	 sendNormalFTPCommand(m_mainSockFD, "pasv", NULL);
	 streamBuffer *sb = new streamBuffer();
	 int responseCode = receiveFTPResponse(m_mainSockFD, &sb);
	 
	 parsePASVResponse(sb, &m_dataAddr);
	 sb->clear();
	 
	 if ((m_dataSockFD = openConnection(&m_dataAddr)) == -1){
		  DEBUGMSG(ftp, "error opening data connection!\n");
		  cout << "error opening data connection on port " << ntohs(m_dataAddr.sin_port)<< endl;
	 }

	 sendNormalFTPCommand(m_mainSockFD, dirCommand, 
						  (VariantIsNull(dirName) ? 
						   NULL : (VariantGetString(dirName).c_str())));

	 responseCode = receiveFTPResponse(m_mainSockFD);

	 switch(responseCode){
	 case FILE_ACTION_NOT_TAKEN:
		  // if it returns 550, then there are no files. 
		  // close the data connection
		  close (m_dataSockFD);
		  emptyStringArray(_retval);
		  break;
	 case FILE_START_TRANSFER:
		  int received = receiveAllFromConnection(m_dataSockFD, sb);

		  // ? ***
		  close (m_dataSockFD);
		  DEBUGMSG(ftp, "received %d bytes: \n", received);
		  if (! streamBufferToStringArray(sb, _retval)){
			   return false;
		  }

		  responseCode = receiveFTPResponse(m_mainSockFD);
	 }
	 delete sb;
	 return true;
}


/* PRBool rename (in string oldFileName, in string newFileName); */
NS_IMETHODIMP sashFTPConnection::Rename(const char *oldFileName, const char *newFileName, PRBool *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* PRBool delete (in string removeFileName); */
NS_IMETHODIMP sashFTPConnection::Delete(const char *removeFileName, PRBool *_retval)
{
	 sendNormalFTPCommand (m_mainSockFD, "dele", removeFileName);
	 int responseCode = receiveFTPResponse(m_mainSockFD);

	 switch(responseCode){
	 case FILE_ACTION_NOT_TAKEN:
		  *_retval = false;
		  break;
	 case FILE_ACTION_COMPLETED:
		  *_retval = true;
		  break;
	 }

	 return NS_OK;
}

/* PRBool mkdir (in string remoteDir); */
NS_IMETHODIMP sashFTPConnection::Mkdir(const char *remoteDir, PRBool *_retval)
{
	 sendNormalFTPCommand (m_mainSockFD, "mkd", remoteDir);
	 int responseCode = receiveFTPResponse(m_mainSockFD);

	 switch(responseCode){
	 case DIRECTORY_ACTION_NOT_TAKEN:
		  *_retval = false;
		  break;
	 case DIRECTORY_ACTION_COMPLETED:
		  *_retval = true;
		  break;
	 }
	 return NS_OK;
}

/* PRBool rmdir (in string remoteDir); */
NS_IMETHODIMP sashFTPConnection::Rmdir(const char *remoteDir, PRBool *_retval)
{
	 sendNormalFTPCommand (m_mainSockFD, "rmd", remoteDir);
	 int responseCode = receiveFTPResponse(m_mainSockFD);

	 switch(responseCode){
	 case FILE_ACTION_NOT_TAKEN:
		  *_retval = false;
		  break;
	 case FILE_ACTION_COMPLETED:
		  *_retval = true;
		  break;
	 }

	 return NS_OK;
}

/* attribute string OnFileSendStart; */
NS_IMETHODIMP sashFTPConnection::GetOnFileSendStart(char * *aOnFileSendStart)
{
	 XP_RETURN_STRING(m_onFileSendStart.c_str(), aOnFileSendStart);
}
NS_IMETHODIMP sashFTPConnection::SetOnFileSendStart(const char * aOnFileSendStart)
{
	 m_onFileSendStart = aOnFileSendStart;
	 return NS_OK;

}


/* attribute string OnFileSendStart; */
NS_IMETHODIMP sashFTPConnection::GetOnFileReceiveStart(char * *aOnFileReceiveStart)
{
	 XP_RETURN_STRING(m_onFileReceiveStart.c_str(), aOnFileReceiveStart);
}
NS_IMETHODIMP sashFTPConnection::SetOnFileReceiveStart(const char * aOnFileReceiveStart)
{
	 m_onFileReceiveStart = aOnFileReceiveStart;
	 return NS_OK;

}


/* attribute string OnFileSent; */
NS_IMETHODIMP sashFTPConnection::GetOnFileSent(char * *aOnFileSent)
{
	 XP_RETURN_STRING(m_onFileSent.c_str(), aOnFileSent);
}
NS_IMETHODIMP sashFTPConnection::SetOnFileSent(const char * aOnFileSent)
{
	 m_onFileSent = aOnFileSent;
	 return NS_OK;
}

/* attribute string OnFileReceived; */
NS_IMETHODIMP sashFTPConnection::GetOnFileReceived(char * *aOnFileReceived)
{
	 XP_RETURN_STRING(m_onFileReceived.c_str(), aOnFileReceived);
}
NS_IMETHODIMP sashFTPConnection::SetOnFileReceived(const char * aOnFileReceived)
{
	 m_onFileReceived = aOnFileReceived;
	 return NS_OK;
}

/* attribute string OnFileSendError; */
NS_IMETHODIMP sashFTPConnection::GetOnFileSendError(char * *aOnFileSendError)
{
	 XP_RETURN_STRING(m_onFileSendError.c_str(), aOnFileSendError);
}
NS_IMETHODIMP sashFTPConnection::SetOnFileSendError(const char * aOnFileSendError)
{
	 m_onFileSendError = aOnFileSendError;
	 return NS_OK;
}

/* attribute string OnFileReceiveError; */
NS_IMETHODIMP sashFTPConnection::GetOnFileReceiveError(char * *aOnFileReceiveError)
{
	 XP_RETURN_STRING(m_onFileReceiveError.c_str(), aOnFileReceiveError);
}
NS_IMETHODIMP sashFTPConnection::SetOnFileReceiveError(const char * aOnFileReceiveError)
{
	 m_onFileReceiveError = aOnFileReceiveError;
	 return NS_OK;
}

/* attribute string OnDefaultError; */
NS_IMETHODIMP sashFTPConnection::GetOnDefaultError(char * *aOnDefaultError)
{
	 XP_RETURN_STRING(m_onDefaultError.c_str(), aOnDefaultError);
}
NS_IMETHODIMP sashFTPConnection::SetOnDefaultError(const char * aOnDefaultError)
{
	 m_onDefaultError = aOnDefaultError;
	 return NS_OK;
}

/* attribute string OnProgress; */
NS_IMETHODIMP sashFTPConnection::GetOnProgress(char * *aOnProgress)
{
	 XP_RETURN_STRING(m_onProgress.c_str(), aOnProgress);
}
NS_IMETHODIMP sashFTPConnection::SetOnProgress(const char * aOnProgress)
{
	 m_onProgress = aOnProgress;
	 return NS_OK;
}

void sashFTPConnection::callOnFileSendStartEvent
(sashFTPConnection *conn, string filename){
	 string callback;
	 XP_GET_STRING(conn->GetOnFileSendStart, callback);
	 callFTPConnEvent(callback, conn, filename);
}

void sashFTPConnection::callOnFileReceiveStartEvent
(sashFTPConnection *conn, string filename){
	 string callback;
	 XP_GET_STRING(conn->GetOnFileReceiveStart, callback);
	 callFTPConnEvent(callback, conn, filename);
}

void sashFTPConnection::callOnFileSentEvent
(sashFTPConnection *conn, string filename){
	 string callback;
	 XP_GET_STRING(conn->GetOnFileSent, callback);
	 callFTPConnEvent(callback, conn, filename);
}

void sashFTPConnection::callOnFileReceivedEvent
(sashFTPConnection *conn, string filename){
	 string callback;
	 XP_GET_STRING(conn->GetOnFileReceived, callback);
	 callFTPConnEvent(callback, conn, filename);
}

void sashFTPConnection::callOnFileSendErrorEvent
(sashFTPConnection *conn, string filename){
	 string callback;
	 XP_GET_STRING(conn->GetOnFileSendError, callback);
	 callFTPConnEvent(callback, conn, filename);
}

void sashFTPConnection::callOnFileReceiveErrorEvent
(sashFTPConnection *conn, string filename){
	 string callback;
	 XP_GET_STRING(conn->GetOnFileReceiveError, callback);
	 callFTPConnEvent(callback, conn, filename);
}

void sashFTPConnection::callOnProgressEvent
(sashFTPConnection *conn, string filename, int amtTransferred){

	 string callback;
	 XP_GET_STRING(conn->GetOnProgress, callback);

	 vector <sashIVariant *> args;
	 DEBUGMSG(comm, "about to call onEvent %s\n",
			  callback.c_str());

	 if (callback != "") {
		  sashIVariant * connVariant;
		  sashIVariant * filenameVar;
		  sashIVariant * amtVar;

		  NewSashVariant(&connVariant);
		  VariantSetInterface(connVariant, 
							  (nsISupports *)(sashIUnknown *)conn);
		  args.push_back(connVariant);
								   
		  NewSashVariant(&filenameVar);
		  VariantSetString(filenameVar, filename);
		  args.push_back(filenameVar);

		  NewSashVariant(&amtVar);
		  VariantSetNum(amtVar, (double)amtTransferred);
		  args.push_back(amtVar);

		  CallEvent(m_cx, 
					callback.c_str(), args);
	 }
}

void sashFTPConnection::callFTPConnEvent
(string callback, sashFTPConnection *conn, string filename){
	 vector <sashIVariant *> args;
	 DEBUGMSG(comm, "about to call onEvent %s\n",
			  callback.c_str());

	 if (callback != "") {
		  sashIVariant * connVariant;
		  sashIVariant * filenameVar;

		  NewSashVariant(&connVariant);
		  VariantSetInterface(connVariant, 
							  (nsISupports *)(sashIUnknown *)conn);
		  args.push_back(connVariant);
								   
		  NewSashVariant(&filenameVar);
		  VariantSetString(filenameVar, filename);
								   
		  args.push_back(filenameVar);
		  CallEvent(m_cx, 
					callback.c_str(), args);
	 }
}


int sashFTPConnection::receiveIntoFile(int dataSockFD, 
									   string fileName){
	 int filefd = sashFTP::m_fs->OpenFile(fileName, false);
	 char *buf = new char [RECEIVE_BUFSIZE+1];

	 // receive until connection closed
	 int totalAmtReceived =0;
	 int amtReceived=1;
	 int blockNum = 1;
	 while(amtReceived){
		  amtReceived = recv(dataSockFD, buf, RECEIVE_BUFSIZE, 0);

		  if (amtReceived != -1){
			   sashFTP::m_fs->WriteToFile(buf, amtReceived, filefd);
			   totalAmtReceived += amtReceived;
			   
			   if (m_notifyIntervalK > 0){
					if (totalAmtReceived >=
						blockNum * m_notifyIntervalK * 1024){
						 blockNum ++;
						 callOnProgressEvent(this, fileName, totalAmtReceived);
					}

			   }
		  } else {
			   perror("recv");
		  }
	 }
	 delete buf;

	 sashFTP::m_fs->CloseFile(filefd);

	 return totalAmtReceived;
}

bool sashFTPConnection::streamBufferToStringArray(streamBuffer *sb,
												  sashIVariant **v){
	 // create a new array.
	 if (NewSashVariant (v) != NS_OK){
		  return false;
	 }

	 (*v)->SetType(sashIVariant::VT_ARRAY);

	 // read through, line by line.
	 // each line after that contains a filename.
	 int indexOfNL = sb->findInBuffer('\n');
	 char *currline = new char [LIST_BUFSIZE];
	 string currstr;
	 while(indexOfNL != -1){
		  assert (indexOfNL < LIST_BUFSIZE);
		  sb->getData(indexOfNL + 1, currline);
		  currline[indexOfNL] = '\0';
		  currstr = currline;
		  DEBUGMSG(ftp, "currstr: %s\n", currstr.c_str());

		  sashIVariant* sv;
		  if (NewSashVariant(&sv) == NS_OK) {
			   sv->SetType(sashIVariant::VT_STR);
			   sv->SetSVal(currstr.c_str());
			   (*v)->ArrayAppend(sv);
		  } else {
			   // you're screwed.
			   return false;
		  }

		  indexOfNL = sb->findInBuffer('\n');
	 }
	 delete [] currline;

	 return true;
}

bool sashFTPConnection::emptyStringArray(sashIVariant **v){
	 // create a new array.
	 if (NewSashVariant (v) != NS_OK){
		  return false;
	 }

	 (*v)->SetType(sashIVariant::VT_ARRAY);
	 return true;
}
