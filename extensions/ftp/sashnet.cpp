#include "sashnet.h"
#include <strstream>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include "debugmsg.h"
#include "streamBuffer.h"


#include "sashFTP.h"
#include "ftpcodes.h"

void setRemoteAddr(struct sockaddr_in *remoteAddr,
				   const char *remoteHost, unsigned short remotePort){
	 struct hostent *h;
	 ostrstream o;
	 if ((h=gethostbyname(remoteHost)) == NULL){
		  o << '\0';
	 }
	 else{
		  if (h->h_addr != NULL){
			   o << inet_ntoa(*((struct in_addr *)h->h_addr)) << '\0';
		  } else {
			   // not sure if this is covered by the first if condition.
			   o << '\0';
		  }
	 }
	 
	 DEBUGMSG(net, "setting remote address: %s, port %d\n", 
			  o.str(), remotePort);

	 remoteAddr->sin_family = AF_INET;
	 remoteAddr->sin_port = htons(remotePort);
	 remoteAddr->sin_addr.s_addr = inet_addr(o.str());
	 memset(&(remoteAddr->sin_zero), '\0', 8);
}

bool bindSocket(int sockfd, int port){
	 struct sockaddr_in my_addr;
	 my_addr.sin_family = AF_INET;
	 my_addr.sin_port = htons(port);
	 my_addr.sin_addr.s_addr = INADDR_ANY;
	 memset(&(my_addr.sin_zero), '\0', 8);
	 
	 if (bind(sockfd, (struct sockaddr *)&my_addr, 
			  sizeof(struct sockaddr)) == -1){
		  perror("bind");
		  return false;
	 }
	 return true;
}

int fullSend(int sockfd, int size, char * data){
	 int amtLeft = size;
	 int amtSent = 0;
	 while (amtLeft > 0){
		  amtSent = send(sockfd, data, size, 0);
		  if (amtSent == -1) {
			   perror("send");
			   return -1;
		  } else {
			   amtLeft -= amtSent;
		  }
	 }
	 return size; 
}

int openConnection(struct sockaddr_in *remoteAddr){
	 // create a new socket
	 int sockfd = socket (AF_INET, SOCK_STREAM, 0);
	 DEBUGMSG(net, "socket call successful (fd%d).\n", sockfd);

	 // connect to the new port
	 if (connect(sockfd, 
				 (struct sockaddr *)remoteAddr, 
				 sizeof(struct sockaddr)) == -1){
		  perror("connect");
		  DEBUGMSG(net, "error connecting to port %d\n", ntohs(remoteAddr->sin_port));

		  return -1;
	 }
	 DEBUGMSG(net, "connect call successful.\n");
	 return sockfd;
}

int receiveAllFromConnection(int dataSockFD, streamBuffer *strbuf){
	 char *buf = new char [RECEIVE_BUFSIZE+1];

	 // receive until connection closed
	 int totalAmtReceived =0;
	 int amtReceived=1;
	 while(amtReceived){
		  amtReceived = recv(dataSockFD, buf, RECEIVE_BUFSIZE, 0);

		  if (amtReceived != -1){
			   strbuf->appendData(amtReceived, buf);
			   totalAmtReceived += amtReceived;
		  } else {
			   perror("recv");
		  }
	 }
	 delete [] buf;
	 return totalAmtReceived;
}



int sendNormalFTPCommand (int sockfd, const char *ftpCommand, 
						  const char *argstr){
	 char *buf;
	 if (argstr == NULL){
		  int len = strlen(ftpCommand);
		  buf = new char [len + 2];
		  memcpy (buf, ftpCommand, len);
		  buf[len] = '\n';

		  buf[len+1] = '\0';

	 } else{
		  buf = new char [strlen(argstr) + 7];
		  sprintf(buf, "%s %s\n\0", ftpCommand, argstr);
	 }

	 DEBUGMSG(ftp, "sending command: %s\n", buf);
	 if (fullSend(sockfd, strlen(buf), buf) == -1){
		  return -1;
	 }
	 
	 delete [] buf;

	 return 1;
}


/*
  gets the ftp response, which consists of a response code and 
  a brief message. most responses are only one line.

  multi-line responses start with the response code, followed by the
  '-' character. the last line of the response starts with the response
  code followed by a space. the lines in between can start with anything 
  _but_ the response code followed by a space.
*/
int receiveFTPResponse (int sockfd, streamBuffer **buff){
	 // receive until '\n' for single line messages
	 
	 // use select to wait until there's data

	 // keep receiving while there's still data

	 // if there's a multi-line message, or you haven't read a newline
	 // yet, then you still need to receive more data
	 char * buf = new char [LINE_BUFSIZE + 1];
	 char * currline = new char [LINE_BUFSIZE + 1];
	 char * returnCode = new char [4];


	 streamBuffer *strbuf;
	 bool deleteStrBuf = false;
	 if (buff == NULL){
		  strbuf = new streamBuffer();
		  deleteStrBuf = true;
	 } else {
		  strbuf = *buff;
	 }
	 streamBuffer *holdbuf = new streamBuffer();

	 int received;
	 received = recv(sockfd, buf, LINE_BUFSIZE, 0);
	 if (received != -1){
		  buf[received] = '\0';
		  
		  strbuf->appendData(received, buf);
		  DEBUGMSG(ftp, "fd%d: %d bytes received: %s\n",
				   sockfd, received, buf);
	 }

	 int indexOfNL, indexOfCR;
	 int previousRCode = -1;
	 bool isDone = false;

	 while (! isDone){
		  indexOfNL = strbuf->findInBuffer('\n');
		  indexOfCR = strbuf->findInBuffer('\r');

		  // receive data until you get a newline.
		  while (indexOfNL == -1){
			   received = recv(sockfd, buf, LINE_BUFSIZE, 0);
			   buf[received] = '\0';
			   if (received != -1){
					strbuf->appendData(received, buf);
					DEBUGMSG(ftp, "fd%d: %d bytes received: %s\n",
							 sockfd, received, buf);
			   }
			   indexOfNL = strbuf->findInBuffer('\n');
		  }

		  // keep receiving until you've got at least one newline.
		  // at that point, process all whole lines, one at a time.
		  while (indexOfNL != -1){
			   strbuf->getData(indexOfNL + 1, currline);
			   currline[indexOfNL + 1] = '\0';
			   holdbuf->appendData(indexOfNL + 1, currline);
			   
			   DEBUGMSG(ftp, "currline: %s\n", currline);

			   // with the current line, there are several possibilities.
			   // the line could be a one-line response
			   // the line could be the beginning of a multi-line response
			   // the line could be in the middle of a multi-line response
			   // the line could be at the end of a multi-line response

			   // not the first line of a multi-line response
			   if (previousRCode != -1){
					if ((returnCode[0] == currline[0]) 
						&& (returnCode[1] == currline[1]) 
						&& (returnCode[2] == currline[2]) 
						&& (currline[3] == ' ')){
						 // this is the last line of the multi-line response
						 // all done.
						 DEBUGMSG(net, "last line of multiline respones\n");
						 indexOfNL = -1;
						 isDone = true;

					} else {
						 // this isn't the last line of the multi-line response
						 // ignore it and keep going
						 DEBUGMSG(net, "not the last line of multiline respones\n");
						 indexOfNL = strbuf->findInBuffer('\n');
					}
			   } else {
					// this is the first line
					memcpy(returnCode, currline, 3);
					returnCode[3] = '\0';

					// if it's a multi-line response, you want it to 
					// get more data.
					if (currline[3] == '-'){
						 DEBUGMSG(net, "first line of multi-line response\n");
						 indexOfNL = strbuf->findInBuffer('\n');
						 previousRCode = atoi(returnCode);
					} else {
						 assert(currline[3] == ' ');
						 DEBUGMSG(net, "single-line response, %s\n",
								  returnCode);
						 indexOfNL = -1;
						 isDone = true;
					}
			   }
		  }
	 }

	 if (deleteStrBuf){
		  delete strbuf;
		  delete holdbuf;
	 } else {
		  delete strbuf;
		  *buff = holdbuf;
	 }

	 int retval = atoi(returnCode);

	 delete [] buf;
	 delete [] currline;
	 delete [] returnCode;
	 return retval;
}

int parsePASVResponse(streamBuffer *sb, struct sockaddr_in *dataAddr){
	 // match all the way to the '(';
	 int leftparen = sb->findInBuffer('(');
	 int rightparen = sb->findInBuffer(')');
	 if ((leftparen == -1) || (rightparen == -1)) {
		  DEBUGMSG(net, "PASV response not well formed\n");
		  return -1;
	 }
	 
	 // can junk everything in front of the first paren
	 char *buf = new char [LINE_BUFSIZE];
	 sb->getData(leftparen, NULL);

	 int len = rightparen - leftparen;
	 sb->getData(len, buf);
	 buf[len] = '\0';

	 int a,b,c,d,e,f;
	 sscanf(buf, "(%d, %d, %d, %d, %d, %d)", &a,&b,&c,&d,&e,&f);

	 sprintf(buf, "%d.%d.%d.%d", a,b,c,d);
	 
	 if (dataAddr) setRemoteAddr(dataAddr, buf, 256 * e + f);
	 delete [] buf;
	 return 1;
}

bool processFTPResponseCode(int responseCode, string **errorMessage){
	 bool retval = true;

	 int firstDigit = (int) responseCode / 100;
	 int secondDigit = (int) (responseCode - firstDigit * 100) / 10;
	 int thirdDigit = (int) (responseCode - firstDigit * 100 - secondDigit * 10);

	 switch (firstDigit){
	 case FIRST_PRELIM_POSITIVE:
		  DEBUGMSG(ftp, "prelim positive\n");
		  break;
	 case FIRST_COMPLETE_POSITIVE:
		  DEBUGMSG(ftp, "complete positive\n");
		  break;
	 case FIRST_INTERMEDIATE_POSITIVE:
		  DEBUGMSG(ftp, "intermediate positive\n");
		  break;
	 case FIRST_TRANSIENT_NEGATIVE:
	 case FIRST_PERMANENT_NEGATIVE:
		  retval = false;
		  // if there's some sort of error, 
		  switch (secondDigit){
		  case SECOND_SYNTAX:
			   DEBUGMSG(ftp, "syntax error\n");
			   *errorMessage = new string ("syntax error");
			   break;
		  case SECOND_INFORMATION:
			   break;

		  case SECOND_CONNECTION:
			   DEBUGMSG(ftp, "connection error\n");
			   *errorMessage = new string ("connection error");
			   break;
		  case SECOND_AUTHENTICATION:
			   DEBUGMSG(ftp, "authentication error\n");
			   *errorMessage = new string ("authentication error");
			   break;
		  case SECOND_FILESYSTEM:
			   DEBUGMSG(ftp, "filesystem error\n");
			   *errorMessage = new string ("filesystem error");
			   break;
		  }
		  break;
	 }
	 return retval;
}


/*
	 DEBUGMSG(ftp, "test return code stuff\n");
	 string *str;
	 if (processFTPResponseCode(226, &str)){
		  DEBUGMSG(ftp, "successful!\n");
	 }else{
		  DEBUGMSG(ftp,"unsuccessful: %s\n", str->c_str());
	 }

	 if (processFTPResponseCode(530, &str)){
		  DEBUGMSG(ftp, "successful!\n");
	 }else{
		  DEBUGMSG(ftp,"unsuccessful: %s\n", str->c_str());
	 }
*/

