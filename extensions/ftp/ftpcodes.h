
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for FTP command and error codes
******************************************************************/


#ifndef FTPCODES_H
#define FTPCODES_H

const int FILE_ACTION_NOT_TAKEN = 550;
const int FILE_ACTION_COMPLETED = 250;
const int DIRECTORY_ACTION_COMPLETED = 257;
const int DIRECTORY_ACTION_NOT_TAKEN = 521;
const int CD_ACTION_COMPLETED = 200;
const int CD_ACTION_NOT_TAKEN = 431;
const int FILE_START_TRANSFER = 150;
const int FILE_TRANSFER_COMPLETE = 226;
const int LOGIN_INCORRECT = 530;

const int FIRST_PRELIM_POSITIVE = 1;
const int FIRST_COMPLETE_POSITIVE = 2;
const int FIRST_INTERMEDIATE_POSITIVE = 3;
const int FIRST_TRANSIENT_NEGATIVE = 4;
const int FIRST_PERMANENT_NEGATIVE = 5;

const int SECOND_SYNTAX = 0;
const int SECOND_INFORMATION = 1;
const int SECOND_CONNECTION = 2;
const int SECOND_AUTHENTICATION = 3;
const int SECOND_FILESYSTEM = 5;


#endif
