
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the FTP Connection object 
******************************************************************/
#ifndef SASHFTPCONNECTION_H
#define SASHFTPCONNECTION_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIFTPConnection.h"
#include "sashIExtension.h"
#include "sashIConstructor.h"
#include "SecurityManager.h"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <iostream.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string>
#include <deque>
#include "debugmsg.h"
#include "streamBuffer.h"

// (11835ff0-a9b9-464a-b580-8e3910f96004)
#define SASHFTPCONNECTION_CID {0x11835ff0, 0xa9b9, 0x464a, {0xb5, 0x80, 0x8e, 0x39, 0x10, 0xf9, 0x60, 0x04}}

NS_DEFINE_CID(ksashFTPConnectionCID, SASHFTPCONNECTION_CID);

#define SASHFTPCONNECTION_CONTRACT_ID "@gnome.org/SashMo/FTP/sashftpconnection;1"


typedef struct{
	 bool isGet;
	 string filename;
} transferRequest;

class sashFTPConnection : public sashIFTPConnection,
						  public sashIConstructor
{
public:
	 NS_DECL_ISUPPORTS;
	 NS_DECL_SASHIUNKNOWN;
	 NS_DECL_SASHICONSTRUCTOR;
	 NS_DECL_SASHIFTPCONNECTION;

	 sashFTPConnection();
	 virtual ~sashFTPConnection();

	 bool start();
	 void run();
	 bool m_running;

protected:
	 void waitForFiles();

	 pthread_mutex_t m_connectionLock;
	 pthread_t m_transferFiles;

	 pthread_mutex_t m_idleLock; 
	 pthread_cond_t m_idleCond;

	 pthread_mutex_t m_transferLock; 
	 deque<transferRequest*> m_transferQueue;

	 void lockConnection(){
		  DEBUGMSG(ftp, "locking connection...\n");
		  pthread_mutex_lock(&m_connectionLock);
		  DEBUGMSG(ftp, "locked connection...\n");
	 }
	 void unlockConnection(){
		  DEBUGMSG(ftp, "unlocking connection...\n");
		  pthread_mutex_unlock(&m_connectionLock);
		  DEBUGMSG(ftp, "unlocked connection...\n");
	 }


	 void lockTransferQueue(){
		  DEBUGMSG(ftp, "locking tranfer queue...\n");
		  pthread_mutex_lock(&m_transferLock);
		  DEBUGMSG(ftp, "locked transfer queue...\n");
	 }
	 void unlockTransferQueue(){
		  DEBUGMSG(ftp, "unlocking transfer queue...\n");
		  pthread_mutex_unlock(&m_transferLock);
		  DEBUGMSG(ftp, "unlocked transfer queue...\n");
	 }

	 JSContext *m_cx;
	 
	 bool m_connected;

	 int m_localPort;
	 struct sockaddr_in m_remoteAddr;
	 int m_mainSockFD;

	 struct sockaddr_in m_dataAddr;
	 int m_dataSockFD;


	 char * m_loginMessage;
	 int m_notifyIntervalK;

	 int getSingleFile(string fileName);
	 int putSingleFile(string fileName);
	 int receiveIntoFile(int dataSockFD, 
						 string fileName);

	 bool genericList(const char *dirCommand,
					  sashIVariant *dirName, 
					  sashIVariant **_retval);

	 bool streamBufferToStringArray(streamBuffer *sb,
									sashIVariant **v);
	 bool emptyStringArray(sashIVariant **v);

	 // callbacks
	 string m_onFileSendStart;
	 string m_onFileReceiveStart;
	 string m_onFileSent;
	 string m_onFileReceived;
	 string m_onFileSendError;
	 string m_onFileReceiveError;
	 string m_onDefaultError;
	 string m_onProgress;


	 void callOnFileSendStartEvent(sashFTPConnection *conn, 
								   string filename);
	 void callOnFileReceiveStartEvent(sashFTPConnection *conn, 
									  string filename);
	 void callOnFileSentEvent(sashFTPConnection *conn, 
							  string filename);
	 void callOnFileReceivedEvent(sashFTPConnection *conn, 
								  string filename);
	 void callOnFileSendErrorEvent(sashFTPConnection *conn, 
								   string filename);
	 void callOnFileReceiveErrorEvent(sashFTPConnection *conn, 
									  string filename);

	 void callFTPConnEvent(string callback, sashFTPConnection *conn, 
						   string filename);


	 void callOnProgressEvent
		  (sashFTPConnection *conn, string filename, int amtTransferred);
};

#endif
