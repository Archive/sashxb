/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for the stream buffer object

This is a helper class that for tcp client objects. Each TCP client object
has two buffers - one to send and one to receive. 

Send buffer: added to when the ClientSocket calls send.
Receive buffer: added to when the SocketConnectionManager thread gets finds
data on a socket.

Internally, the buffer is stored as a list of data chunks (streamBufferItem).


*** this should be the file as the one for the communications/sockets extension

******************************************************************/

#ifndef STREAMBUFFER_H
#define STREAMBUFFER_H

#include <deque>
#include <string>
#include <stdio.h>

class streamBufferItem{
public:
	 streamBufferItem(int size, char*data){
		  m_size = size;
		  m_data = new char[size+1];
		  if (m_data == NULL){
			   size = 0;
		  }
		  memcpy (m_data, data, size);
		  m_data[size] = '\0';
	 }

	 ~streamBufferItem(){
		  delete[] m_data;
	 }

	 void printStreamBufferItem(){
		  char * buf = new char[m_size+1];
		  memcpy (buf, m_data, m_size);
		  buf[m_size] = '\0';

		  printf("%d bytes: %s\n", m_size, buf);

	 }
protected:

private:
	 int m_size;
	 char *m_data;

	 friend class streamBuffer;
};


class streamBuffer{
public:
	 streamBuffer();
	 ~streamBuffer();

	 bool appendData(int size, char *buf, bool lock = true);
	 bool prependData(int size, char *buf);

	 int getSizeOfFirstItem(bool lock=true);
	 int getDataFromFirstItem(char *buf, bool lock = true);
	 int getData(int size, char *buf, bool lock = true);
	 int findInBuffer(string str, int startpos = 0);
	 int findInBuffer(char c, int startpos = 0);
	 int getSize(bool lock=true);
	 bool checkTotalSize(bool lock=true);
	 bool empty(bool lock=true);
	 void clear(bool lock=true);
	 void printStreamBuffer();

private:
	 pthread_mutex_t m_buffer_lock;

	 deque<streamBufferItem*> m_bufferItems;
	 int m_totalSize;
	 int splitFirstItem(int size, char *buf);
	 int discardFirstItem();
	 int flatten();
};


#endif
