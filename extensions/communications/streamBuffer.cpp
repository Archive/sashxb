
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for the stream buffer object

*** this should be the file as the one for the communications/sockets extension

******************************************************************/
#include <stdlib.h>
#include "streamBuffer.h"
#include "assert.h"

streamBuffer::streamBuffer(){
	 m_totalSize = 0;
	 pthread_mutex_init(&m_buffer_lock, NULL);
}

streamBuffer::~streamBuffer(){
	 clear();
	 pthread_mutex_destroy(&m_buffer_lock);
}

bool streamBuffer::	 appendData(int size,char *buf, bool lock){
	 bool retval;

	 if (lock) pthread_mutex_lock(&m_buffer_lock);

	 if (size <= 0){ 
		  retval = false;
	 }else{
		  streamBufferItem *sbi = new streamBufferItem(size, buf);
		  if (sbi == NULL){
			   retval = false;
		  } else {
			   m_bufferItems.push_back(sbi);
			   m_totalSize += sbi->m_size;
		  }
	 }

	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return retval;
}

bool streamBuffer::	 prependData(int size,char *buf){
	 pthread_mutex_lock(&m_buffer_lock);
	 bool retval = true;
	 if (size <= 0) {
		  retval = false;
	 } else {
		  streamBufferItem *sbi = new streamBufferItem(size, buf);
		  if (sbi == NULL){
			   retval = false;
		  }else{
			   m_bufferItems.push_front(sbi);
			   m_totalSize += sbi->m_size;
		  }
	 }
	 pthread_mutex_unlock(&m_buffer_lock);
	 return retval;
}

int streamBuffer::getSizeOfFirstItem(bool lock){
	 int retval=-1;
	 if (lock) pthread_mutex_lock(&m_buffer_lock);			   

	 if (! m_bufferItems.empty()){
		  streamBufferItem *sbi = m_bufferItems.front();
		  retval = sbi->m_size;
	 }

	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return retval;
}

int streamBuffer::getDataFromFirstItem(char *buf, bool lock){
	 int size = -1;
	 if (lock) pthread_mutex_lock(&m_buffer_lock);
	 if (! m_bufferItems.empty()){
		  streamBufferItem *sbi = m_bufferItems.front();
		  m_bufferItems.pop_front();
		  
		  if (sbi != NULL){ 
			   size = sbi->m_size;
			   memcpy(buf, sbi->m_data, size);
			   m_totalSize -= size;
			   delete sbi;
		  }
	 }
	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return size;
}

int streamBuffer::getData(int size, char *buf, bool lock){
	 if (lock) pthread_mutex_lock(&m_buffer_lock);

	 int amtLeft = size;
	 if (size == -1) amtLeft = m_totalSize;
	 if (size > m_totalSize) amtLeft = m_totalSize;

	 int amtRead = 0;
	 int sizeFirst = 0;
	 int itemSize =0;

	 if (! m_bufferItems.empty()){

		  while ((! m_bufferItems.empty()) 
				 && ((sizeFirst = getSizeOfFirstItem(false)) <= amtLeft)){
			   // get a whole item.

			   if (buf)
					itemSize = getDataFromFirstItem(&buf[amtRead], false);
			   else
					itemSize = discardFirstItem();
		  
			   assert(itemSize = sizeFirst);
			   amtRead += itemSize;
			   amtLeft -= itemSize;
		  
			   assert (amtLeft >= 0);
		  }

		  if (amtLeft != 0){
			   assert(amtLeft < getSizeOfFirstItem(false));
			   assert(! m_bufferItems.empty());

			   char *currpos = NULL;
			   if (buf)  
					currpos = &(buf[amtRead]);

			   if ((itemSize = splitFirstItem(amtLeft, currpos)) == -1) {
					amtRead = -1;
			   }
			   else
			   {
					amtRead += itemSize;
					amtLeft -= itemSize;
			   }
		  }
	 }

	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return amtRead;
}

int streamBuffer::discardFirstItem(){
	 int retval;
	 if (! m_bufferItems.empty()){
		  streamBufferItem *sbi = m_bufferItems.front();
		  m_bufferItems.pop_front();
		  
		  if (sbi != NULL){ 
			   retval = sbi->m_size;
			   delete [] sbi->m_data;
			   delete sbi;
		  }
	 }
	 return retval;
}


int streamBuffer::splitFirstItem(int size, char *buf){
	 if (m_bufferItems.empty()) return -1;

	 // no need to split if they're the same size or if you need more
	 // than is contained in the first item.
	 int sizeOfFirst;
	 if ((sizeOfFirst = getSizeOfFirstItem(false)) <= size) return -1;
	 
	 streamBufferItem *firstItem = m_bufferItems.front();
	 m_bufferItems.pop_front();
	 m_totalSize -= sizeOfFirst;

	 // leftover size.
	 int newsize = sizeOfFirst - size;
	 assert (newsize > 0);

	 // copy data from beginning of old buffer into the buf.
	 if (buf) 
		  memcpy (buf, firstItem->m_data, size);

	 // copy data from end of old buffer into a new buffer.
	 char *tempdata = new char[newsize];
	 if (tempdata == NULL){
		  return -1;
	 }

	 memcpy (tempdata, &((firstItem->m_data)[size]), newsize);
	 firstItem->m_size = newsize;
	 
	 delete [] firstItem->m_data;
	 firstItem->m_data = tempdata;

	 // stick this back onto the front of the list.
	 m_bufferItems.push_front(firstItem);
	 m_totalSize += firstItem->m_size;

	 assert(checkTotalSize(false));
	 return size;
}

int streamBuffer::findInBuffer(string str, int startpos){
	 pthread_mutex_lock(&m_buffer_lock);
	 int size = m_totalSize;
	 pthread_mutex_unlock(&m_buffer_lock);

	 if (size == 0){
		  return -1;
	 }

	 pthread_mutex_lock(&m_buffer_lock);
	 // lump it all into one.
	 if (flatten() == -1){
		  // error.
	 }
	 
	 // read the data as a string.
	 streamBufferItem *sbi = m_bufferItems.front();

	 int index = -1;

	 if (sbi){
		  string *receiveBuf = new string(sbi->m_data, m_totalSize);
		  index = receiveBuf->find(str, startpos);
		  delete receiveBuf;
	 }

	 pthread_mutex_unlock(&m_buffer_lock);
	 return index;
}

int streamBuffer::findInBuffer(char c, int startpos){
	 pthread_mutex_lock(&m_buffer_lock);	 

	 // lump it all into one.
	 if (flatten() == -1){
		  // error.
	 }
	 
	 // read the data as a string.
	 streamBufferItem *sbi = m_bufferItems.front();

	 int index = -1;

	 if (sbi){
		  string *receiveBuf = new string(sbi->m_data, m_totalSize);
		  index = receiveBuf->find(c, startpos);
		  delete receiveBuf;
	 }

	 pthread_mutex_unlock(&m_buffer_lock);
	 return index;
}

int streamBuffer::flatten(){
	 if (m_totalSize == 0){
		  return 0;
	 }


	 assert(checkTotalSize(false));
	 int size = m_totalSize;
	 char * buf = new char[size];

	 if (buf == NULL)
		  return -1;
	 
	 getData(size, buf, false);

	 // this is a really lazy way to do it.
	 // ***there are faster ways to do this.
	 appendData(size, buf, false);
	 
	 assert (size==m_totalSize);
	 assert (m_bufferItems.size() == 1);

	 return size;
}

bool streamBuffer::empty(bool lock){
	 bool retval;
	 if (lock) pthread_mutex_lock(&m_buffer_lock);
	 retval = m_bufferItems.empty();
	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return retval;
}

int streamBuffer::getSize(bool lock){		 
	 int size;
	 if (lock)pthread_mutex_lock(&m_buffer_lock);
	 size = m_totalSize;
	 if (lock)pthread_mutex_unlock(&m_buffer_lock);
	 return size;
}

bool streamBuffer::checkTotalSize(bool lock){
	 bool retval = true;
	 if (lock) pthread_mutex_lock(&m_buffer_lock);

	 int totalSize=0;
	 deque<streamBufferItem *>::iterator a = m_bufferItems.begin(), 
		  b = m_bufferItems.end();

	 while (a != b){
		  totalSize += (*a)->m_size;
		  ++a;
	 }
	 if (totalSize != m_totalSize){
		  retval = false;
	 }

	 if (lock) pthread_mutex_unlock(&m_buffer_lock);
	 return retval;
}

void streamBuffer::printStreamBuffer(){
	 pthread_mutex_lock(&m_buffer_lock);
	 if (m_bufferItems.empty()){
		  printf("empty stream buffer\n");
	 }else{
		  printf("total bytes: %d\n", m_totalSize);
	 
		  deque<streamBufferItem *>::iterator a = m_bufferItems.begin(), 
			   b = m_bufferItems.end();

		  while (a != b){
			   (*a)->printStreamBufferItem();
			   ++a;
		  }
	 }
}

void streamBuffer::clear(bool lock){
	 if (lock) pthread_mutex_lock(&m_buffer_lock);
	 while (! m_bufferItems.empty()){
		  streamBufferItem *sbi = m_bufferItems.front();
		  m_bufferItems.pop_front();
		  if (sbi != NULL){ 
			   delete sbi;
		  }
	 }
	 m_totalSize = 0;
	 if (lock) pthread_mutex_unlock(&m_buffer_lock);

}
