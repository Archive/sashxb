/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s):
    Stefan Atev (atevstef@luther.edu)

*****************************************************************/

#include "XPWDFStructs.h"

/***
  sashIXPVersion
***/

NS_IMPL_ISUPPORTS1_CI(XPVersion, sashIXPVersion)

XPVersion::XPVersion() {
  NS_INIT_ISUPPORTS();
}

XPVersion::~XPVersion() {
}

// Maybe should put all these in a macro?
NS_IMETHODIMP XPVersion::GetMajor(PRInt32 *aMajor) {
  GETXPINT(m_version.major, aMajor);  
}
NS_IMETHODIMP XPVersion::SetMajor(PRInt32 aMajor) {
  SETXPINT(m_version.major, aMajor);
}
NS_IMETHODIMP XPVersion::GetMajor1(PRInt32 *aMajor1) {
  GETXPINT(m_version.major1, aMajor1);  
}
NS_IMETHODIMP XPVersion::SetMajor1(PRInt32 aMajor1) {
  SETXPINT(m_version.major1, aMajor1);
}
NS_IMETHODIMP XPVersion::GetMinor(PRInt32 *aMinor) {
  GETXPINT(m_version.minor, aMinor);  
}
NS_IMETHODIMP XPVersion::SetMinor(PRInt32 aMinor) {
  SETXPINT(m_version.minor, aMinor);
}
NS_IMETHODIMP XPVersion::GetMaintenance(PRInt32 *aMaintenance) {
  GETXPINT(m_version.maintenance, aMaintenance);  
}
NS_IMETHODIMP XPVersion::SetMaintenance(PRInt32 aMaintenance) {
  SETXPINT(m_version.maintenance, aMaintenance);
}
/* string toString (); */
NS_IMETHODIMP XPVersion::ToString(char **_retval) {
  std::string result= m_version.ToString();
  GETXPSTR(result, _retval);
}

/***
  sashIXPWDFDependency
***/

NS_IMPL_ISUPPORTS1_CI(XPWDFDependency, sashIXPWDFDependency)

XPWDFDependency::XPWDFDependency() {
  NS_INIT_ISUPPORTS();
}

XPWDFDependency::~XPWDFDependency() {
}

NS_IMETHODIMP XPWDFDependency::GetId(char * *aId) {
  GETXPSTR(m_dependency.id, aId);
}
NS_IMETHODIMP XPWDFDependency::SetId(const char * aId) {
  SETXPSTR(m_dependency.id, aId);
}
NS_IMETHODIMP XPWDFDependency::GetFilename(char * *aFilename) {
  GETXPSTR(m_dependency.filename, aFilename);
}
NS_IMETHODIMP XPWDFDependency::SetFilename(const char * aFilename) {
  SETXPSTR(m_dependency.filename, aFilename);
}
NS_IMETHODIMP XPWDFDependency::GetTitle(char * *aTitle) {
  GETXPSTR(m_dependency.title, aTitle);
}
NS_IMETHODIMP XPWDFDependency::SetTitle(const char * aTitle) {
  SETXPSTR(m_dependency.title, aTitle);
}
NS_IMETHODIMP XPWDFDependency::GetRequiredVersion(sashIXPVersion * *aRequiredVersion) {
  GETXPVERSION(m_dependency.required_version, aRequiredVersion);  
  return NS_OK;
}
NS_IMETHODIMP XPWDFDependency::SetRequiredVersion(sashIXPVersion * aRequiredVersion) {
  SETXPVERSION(m_dependency.required_version, aRequiredVersion);
  return NS_OK;
}

/***
  sashIXPWDFExpiration
***/

NS_IMPL_ISUPPORTS1_CI(XPWDFExpiration, sashIXPWDFExpiration)

XPWDFExpiration::XPWDFExpiration() {
  NS_INIT_ISUPPORTS();
}

XPWDFExpiration::~XPWDFExpiration() {
}

NS_IMETHODIMP XPWDFExpiration::GetDate(char * *aDate) {
  GETXPSTR(m_expiration.date, aDate);
}
NS_IMETHODIMP XPWDFExpiration::SetDate(const char * aDate) {
  SETXPSTR(m_expiration.date, aDate);
}
NS_IMETHODIMP XPWDFExpiration::GetTime(char * *aTime) {
  GETXPSTR(m_expiration.time, aTime);
}
NS_IMETHODIMP XPWDFExpiration::SetTime(const char * aTime) {
  SETXPSTR(m_expiration.time, aTime);
}
NS_IMETHODIMP XPWDFExpiration::GetDays(char * *aDays) {
  GETXPSTR(m_expiration.days, aDays);
}
NS_IMETHODIMP XPWDFExpiration::SetDays(const char * aDays) {
  SETXPSTR(m_expiration.days, aDays);
}

/***
  sashIXPWDFUpdate
***/

NS_IMPL_ISUPPORTS1_CI(XPWDFUpdate, sashIXPWDFUpdate)

XPWDFUpdate::XPWDFUpdate() {
  NS_INIT_ISUPPORTS();
}

XPWDFUpdate::~XPWDFUpdate() {
}

NS_IMETHODIMP XPWDFUpdate::GetCheck(PRBool *aCheck) {
  GETXPINT(m_update.check, aCheck);
}
NS_IMETHODIMP XPWDFUpdate::SetCheck(PRBool aCheck) {
  SETXPINT(m_update.check, aCheck);
}
NS_IMETHODIMP XPWDFUpdate::GetNotify(PRBool *aNotify) {
  GETXPINT(m_update.notify, aNotify);
}
NS_IMETHODIMP XPWDFUpdate::SetNotify(PRBool aNotify) {
  SETXPINT(m_update.notify, aNotify);
}
NS_IMETHODIMP XPWDFUpdate::GetFrequency(char * *aFrequency) {
  GETXPSTR(m_update.frequency, aFrequency);
}
NS_IMETHODIMP XPWDFUpdate::SetFrequency(const char * aFrequency) {
  SETXPSTR(m_update.frequency, aFrequency);
}

/***
  sashIXPWDFAction
***/

NS_IMPL_ISUPPORTS1_CI(XPWDFAction, sashIXPWDFAction)

XPWDFAction::XPWDFAction() {
  NS_INIT_ISUPPORTS();
}

XPWDFAction::~XPWDFAction() {
}

NS_IMETHODIMP XPWDFAction::GetId(char * *aId) {
  GETXPSTR(m_action.id, aId);
}
NS_IMETHODIMP XPWDFAction::SetId(const char * aId) {
  SETXPSTR(m_action.id, aId);
}
NS_IMETHODIMP XPWDFAction::GetLocationid(char * *aLocationid) {
  GETXPSTR(m_action.locationid, aLocationid);
}
NS_IMETHODIMP XPWDFAction::SetLocationid(const char * aLocationid) {
  SETXPSTR(m_action.locationid, aLocationid);
}
NS_IMETHODIMP XPWDFAction::GetName(char * *aName) {
  GETXPSTR(m_action.name, aName);
}
NS_IMETHODIMP XPWDFAction::SetName(const char * aName) {
  SETXPSTR(m_action.name, aName);
}
NS_IMETHODIMP XPWDFAction::GetRegistration(char * *aRegistration) {
  GETXPSTR(m_action.registration, aRegistration);
}
NS_IMETHODIMP XPWDFAction::SetRegistration(const char * aRegistration) {
  SETXPSTR(m_action.registration, aRegistration);
}

/***
  sashIXPWDFVersion
***/

NS_IMPL_ISUPPORTS1_CI(XPWDFVersion, sashIXPWDFVersion)

XPWDFVersion::XPWDFVersion() {
  NS_INIT_ISUPPORTS();
}

XPWDFVersion::~XPWDFVersion() {
}

NS_IMETHODIMP XPWDFVersion::GetWeblication(sashIXPVersion * *aWeblication) {
  GETXPVERSION(m_version.weblication, aWeblication);
  return NS_OK;
}
NS_IMETHODIMP XPWDFVersion::SetWeblication(sashIXPVersion * aWeblication) {
  SETXPVERSION(m_version.weblication, aWeblication);
  return NS_OK;
}
NS_IMETHODIMP XPWDFVersion::GetSashMinimum(sashIXPVersion * *aSashMinimum) {
  GETXPVERSION(m_version.sash.minimum, aSashMinimum);
  return NS_OK;
}
NS_IMETHODIMP XPWDFVersion::SetSashMinimum(sashIXPVersion * aSashMinimum) {
  SETXPVERSION(m_version.sash.minimum, aSashMinimum);
  return NS_OK;
}
NS_IMETHODIMP XPWDFVersion::GetSashRecommended(sashIXPVersion * *aSashRecommended) {
  GETXPVERSION(m_version.sash.recommended, aSashRecommended);
  return NS_OK;
}
NS_IMETHODIMP XPWDFVersion::SetSashRecommended(sashIXPVersion * aSashRecommended) {
  SETXPVERSION(m_version.sash.recommended, aSashRecommended);
  return NS_OK;
}
NS_IMETHODIMP XPWDFVersion::GetMozillaMinimum(sashIXPVersion * *aMozillaMinimum) {
  GETXPVERSION(m_version.mozilla.minimum, aMozillaMinimum);
  return NS_OK;
}
NS_IMETHODIMP XPWDFVersion::SetMozillaMinimum(sashIXPVersion * aMozillaMinimum) {
  SETXPVERSION(m_version.mozilla.minimum, aMozillaMinimum);
  return NS_OK;
}
NS_IMETHODIMP XPWDFVersion::GetMozillaRecommended(sashIXPVersion * *aMozillaRecommended) {
  GETXPVERSION(m_version.mozilla.recommended, aMozillaRecommended);
  return NS_OK;
}
NS_IMETHODIMP XPWDFVersion::SetMozillaRecommended(sashIXPVersion * aMozillaRecommended) {
  SETXPVERSION(m_version.mozilla.recommended, aMozillaRecommended);
  return NS_OK;
}

/***
  sashIXPWDFInstallScreen
***/

NS_IMPL_ISUPPORTS1_CI(XPWDFInstallScreen, sashIXPWDFInstallScreen)

XPWDFInstallScreen::XPWDFInstallScreen() {
  NS_INIT_ISUPPORTS();
}

XPWDFInstallScreen::~XPWDFInstallScreen() {
}

NS_IMETHODIMP XPWDFInstallScreen::GetType(PRInt32 *aType) {
  GETXPINT(m_installscreen.type, aType);
}
NS_IMETHODIMP XPWDFInstallScreen::SetType(PRInt32 aType) {
  SETXPINT((int) m_installscreen.type, aType);
}
NS_IMETHODIMP XPWDFInstallScreen::GetTitle(char * *aTitle) {
  GETXPSTR(m_installscreen.title, aTitle);
}
NS_IMETHODIMP XPWDFInstallScreen::SetTitle(const char * aTitle) {
  SETXPSTR(m_installscreen.title, aTitle);
}
NS_IMETHODIMP XPWDFInstallScreen::GetPageFile(char * *aPageFile) {
  GETXPSTR(m_installscreen.page.file, aPageFile);
}
NS_IMETHODIMP XPWDFInstallScreen::SetPageFile(const char * aPageFile) {
  SETXPSTR(m_installscreen.page.file, aPageFile);
}
NS_IMETHODIMP XPWDFInstallScreen::GetPageLocation(PRInt32 *aPageLocation) {
  GETXPINT(m_installscreen.page.location, aPageLocation);
}
NS_IMETHODIMP XPWDFInstallScreen::SetPageLocation(PRInt32 aPageLocation) {
  SETXPINT((int) m_installscreen.page.location, aPageLocation);
}
NS_IMETHODIMP XPWDFInstallScreen::GetImageFile(char * *aImageFile) {
  GETXPSTR(m_installscreen.image.file, aImageFile);
}
NS_IMETHODIMP XPWDFInstallScreen::SetImageFile(const char * aImageFile) {
  SETXPSTR(m_installscreen.image.file, aImageFile);
}
NS_IMETHODIMP XPWDFInstallScreen::GetImageLocation(PRInt32 *aImageLocation) {
  GETXPINT(m_installscreen.image.location, aImageLocation);
}
NS_IMETHODIMP XPWDFInstallScreen::SetImageLocation(PRInt32 aImageLocation) {
  SETXPINT((int) m_installscreen.image.location, aImageLocation);
}
NS_IMETHODIMP XPWDFInstallScreen::GetTextFile(char * *aTextFile) {
  GETXPSTR(m_installscreen.text.file, aTextFile);
}
NS_IMETHODIMP XPWDFInstallScreen::SetTextFile(const char * aTextFile) {
  SETXPSTR(m_installscreen.text.file, aTextFile);
}
NS_IMETHODIMP XPWDFInstallScreen::GetTextLocation(PRInt32 *aTextLocation) {
  GETXPINT(m_installscreen.text.location, aTextLocation);
}
NS_IMETHODIMP XPWDFInstallScreen::SetTextLocation(PRInt32 aTextLocation) {
  SETXPINT((int) m_installscreen.text.location, aTextLocation);
}
NS_IMETHODIMP XPWDFInstallScreen::GetNext(PRInt32 *aNext) {
  GETXPINT(m_installscreen.next, aNext);
}
NS_IMETHODIMP XPWDFInstallScreen::SetNext(PRInt32 aNext) {
  SETXPINT(m_installscreen.next, aNext);
}
NS_IMETHODIMP XPWDFInstallScreen::GetBack(PRInt32 *aBack) {
  GETXPINT(m_installscreen.back, aBack);
}
NS_IMETHODIMP XPWDFInstallScreen::SetBack(PRInt32 aBack) {
  SETXPINT(m_installscreen.back, aBack);
}

/***
  sashIXPWDFFile
***/

NS_IMPL_ISUPPORTS1_CI(XPWDFFile, sashIXPWDFFile)

XPWDFFile::XPWDFFile() {
  NS_INIT_ISUPPORTS();
}

XPWDFFile::~XPWDFFile() {
}

NS_IMETHODIMP XPWDFFile::GetPrecache(PRBool *aPrecache) {
  GETXPINT(m_file.precache, aPrecache);
}
NS_IMETHODIMP XPWDFFile::SetPrecache(PRBool aPrecache) {
  SETXPINT(m_file.precache, aPrecache);
}
NS_IMETHODIMP XPWDFFile::GetLocation(PRInt32 *aLocation) {
  GETXPINT(m_file.location, aLocation);
}
NS_IMETHODIMP XPWDFFile::SetLocation(PRInt32 aLocation) {
  SETXPINT((int) m_file.location, aLocation);
}
NS_IMETHODIMP XPWDFFile::GetFilename(char * *aFilename) {
  GETXPSTR(m_file.filename, aFilename);
}
NS_IMETHODIMP XPWDFFile::SetFilename(const char * aFilename) {
  SETXPSTR(m_file.filename, aFilename);
}
