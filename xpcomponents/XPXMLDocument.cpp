
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "XPXMLDocument.h"
#include "XPXMLNode.h"
#include "XMLDocument.h"
#include <string>
#include <vector>
#include "sashVariantUtils.h"

NS_IMPL_ISUPPORTS1_CI(XPXMLDocument, sashIXPXMLDocument)

XPXMLDocument::XPXMLDocument() {
	 DEBUGMSG(xpxml, "XPXMLDocument constructor\n");
  NS_INIT_ISUPPORTS();
}

XPXMLDocument::~XPXMLDocument() {
	 DEBUGMSG(xpxml, "XPXMLDocument destructor\n");
}

/* attribute sashIXMLNode rootNode; */
NS_IMETHODIMP 
XPXMLDocument::GetRootNode(sashIXPXMLNode **aRootNode) 
{
	 DEBUGMSG(xpxml, "GetRootNode\n");
	 XMLNode tempnode = xd.getRootNode();
	 *aRootNode = NewSashXPXMLNode(&tempnode);
	 return (*aRootNode ? NS_OK : NS_ERROR_FAILURE);
}

/* void loadFromFile (in string file_name); */
NS_IMETHODIMP 
XPXMLDocument::LoadFromFile(const char *file_name, PRBool *_retval) 
{
	 DEBUGMSG(xpxml, "LoadFromFile %s\n", file_name);
	 if (file_name == NULL) 
		  *_retval = false;
	 else {
		  *_retval = xd.loadFromFile(file_name);
	 }
	 return NS_OK;
}

/* void saveToFile (in string file_name); */
NS_IMETHODIMP 
XPXMLDocument::SaveToFile(const char *file_name, PRBool *_retval) 
{
	 DEBUGMSG(xpxml, "SaveToFile: %s\n", file_name);
  *_retval= xd.saveToFile(file_name);
  return NS_OK;
}

/* void loadFromString (in string xml); */
NS_IMETHODIMP 
XPXMLDocument::LoadFromString(const char *xml, PRBool *_retval) 
{
	 DEBUGMSG(xpxml, "LoadFromString: %s\n", xml);
  *_retval= xd.loadFromString(xml);
  return NS_OK;
}

/* string saveToString (in string xml); */
NS_IMETHODIMP 
XPXMLDocument::SaveToString(char **_retval) 
{
  DEBUGMSG(xpxml, "SaveToString\n");
  XP_COPY_STRING(xd.saveToString(), _retval);
  return NS_OK;
}

/* void createRootNode (in string tag_name); */
NS_IMETHODIMP 
XPXMLDocument::CreateRootNode(const char *tag_name) 
{
  DEBUGMSG(xpxml, "CreateRootNode\n");
  xd.createRootNode(tag_name);
  return NS_OK;
}

/* sashIXPXMLNode createNode (in string tag_name); */
NS_IMETHODIMP XPXMLDocument::CreateNode(const char *tag_name, sashIXPXMLNode **_retval)
{
	 DEBUGMSG(xpxml, "createNode\n");
	 XMLNode tempnode = xd.createNode(tag_name);
	 *_retval = NewSashXPXMLNode(&tempnode);
	 return (*_retval ? NS_OK : NS_ERROR_FAILURE);
}
