#ifndef XPWDFSTRUCTS_H
#define XPWDFSTRUCTS_H

/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s):
    Stefan Atev (atevstef@luther.edu)

*****************************************************************/

#include "sashIXPWDFStructs.h"
#include "sash_version.h"
#include "wdf.h"
#include "xpcomtools.h"

// 9c7252c8-921a-46f8-bd3b-879edfbf01f7
NS_DEFINE_CID(kXPVersionCID, XPVERSION_CID);

class XPVersion: public sashIXPVersion {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPVERSION

  XPVersion();
  virtual ~XPVersion();
private:
  Version m_version;
};

// 357e78ac-c1c8-4895-9061-7c3d898fea13
NS_DEFINE_CID(kXPWDFDependencyCID, XPWDFDEPENDENCY_CID);

class XPWDFDependency : public sashIXPWDFDependency {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPWDFDEPENDENCY

  XPWDFDependency();
  virtual ~XPWDFDependency();
private:
  WDFDependency m_dependency;
};

// 2d8ed389-909a-4e0d-962f-70cd0d7dd80f
NS_DEFINE_CID(kXPWDFExpirationCID, XPWDFEXPIRATION_CID);

class XPWDFExpiration : public sashIXPWDFExpiration {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPWDFEXPIRATION

  XPWDFExpiration();
  virtual ~XPWDFExpiration();
private:
  WDFExpiration m_expiration;
};

// f42b194b-5722-4c68-80b5-be7bdd4b099c
NS_DEFINE_CID(kXPWDFUpdateCID, XPWDFUPDATE_CID);

class XPWDFUpdate : public sashIXPWDFUpdate {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPWDFUPDATE

  XPWDFUpdate();
  virtual ~XPWDFUpdate();
private:
  WDFUpdate m_update;
};

// 0faeb4d7-7a9e-4f77-a5c2-b90313147f3f
NS_DEFINE_CID(kXPWDFActionCID, XPWDFACTION_CID);

class XPWDFAction : public sashIXPWDFAction {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPWDFACTION

  XPWDFAction();
  virtual ~XPWDFAction();
private:
  WDFAction m_action;
};

// b89c451b-2220-4eaa-8d10-e0142ef20b76
NS_DEFINE_CID(kXPWDFVersionCID, XPWDFVERSION_CID);

class XPWDFVersion : public sashIXPWDFVersion {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPWDFVERSION

  XPWDFVersion();
  virtual ~XPWDFVersion();
private:
  WDFVersion m_version;
};

// 70b8de8f-cdec-40d6-84e4-d57a690da7f9
NS_DEFINE_CID(kXPWDFInstallScreenCID, XPWDFINSTALLSCREEN_CID);

class XPWDFInstallScreen : public sashIXPWDFInstallScreen {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPWDFINSTALLSCREEN

  XPWDFInstallScreen();
  virtual ~XPWDFInstallScreen();
private:
  WDFInstallScreen m_installscreen;
};

// f75288c9-effd-455c-aa25-935c7bc92de3
NS_DEFINE_CID(kXPWDFFileCID, XPWDFFILE_CID);

class XPWDFFile : public sashIXPWDFFile {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPWDFFILE

  XPWDFFile();
  virtual ~XPWDFFile();
private:
  WDFFile m_file;
};

#define GETXPINT(member, outer) *(outer)= (member); return NS_OK;
#define SETXPINT(member, outer) (member)= (outer); return NS_OK;
#define GETXPSTR(member, outer) XP_COPY_STRING(member.c_str(), outer); return NS_OK;
#define SETXPSTR(member, outer) (member)= (outer); return NS_OK;

// Version <-> sashIXPVersion
#define GETXPVERSION(ver, xpver) { \
  nsCOMPtr<sashIXPVersion> _res= do_CreateInstance(XPVERSION_CONTRACT_ID); \
  _res->SetMajor((ver).major); \
  _res->SetMajor1((ver).major1); \
  _res->SetMinor((ver).minor); \
  _res->SetMaintenance((ver).maintenance); \
  *(xpver)= _res; \
  NS_IF_ADDREF(*(xpver)); \
  } 
#define SETXPVERSION(ver, xpver) { \
  PRInt32 tt; \
  (xpver)->GetMajor(&tt); \
  (ver).major= tt; \
  (xpver)->GetMajor1(&tt); \
  (ver).major1= tt; \
  (xpver)->GetMinor(&tt); \
  (ver).minor= tt; \
  (xpver)->GetMaintenance(&tt); \
  (ver).maintenance= tt; \
  }

// WDFExpiration <-> sashIXPWDFExpiration
#define GETXPWDFEXPIRATION(ex, xpex) { \
  nsCOMPtr<sashIXPWDFExpiration> _res= do_CreateInstance(XPWDFEXPIRATION_CONTRACT_ID); \
  _res->SetDate((ex).date.c_str()); \
  _res->SetTime((ex).date.c_str()); \
  _res->SetDays((ex).date.c_str()); \
  *(xpex)= _res; \
  NS_IF_ADDREF(*(xpex)); \
  }
#define SETXPWDFEXPIRATION(ex, xpex) { \
  std::string tt; \
  XP_GET_STRING((xpex)->GetDate, tt); \
  (ex).date= tt; \
  XP_GET_STRING((xpex)->GetTime, tt); \
  (ex).time= tt; \
  XP_GET_STRING((xpex)->GetDays, tt); \
  (ex).days= tt; \
  }
  
// WDFUpdate <-> sashIXPWDFUpdate
#define GETXPWDFUPDATE(up, xpup) { \
  nsCOMPtr<sashIXPWDFUpdate> _res= do_CreateInstance(XPWDFUPDATE_CONTRACT_ID); \
  _res->SetCheck((up).check); \
  _res->SetNotify((up).notify); \
  _res->SetFrequency((up).frequency.c_str()); \
  *(xpup)= _res; \
  NS_IF_ADDREF(*(xpup)); \
  }
#define SETXPWDFUPDATE(up, xpup) { \
  PRBool bb; \
  (xpup)->GetCheck(&bb); \
  (up).check= bb; \
  (xpup)->GetNotify(&bb); \
  (up).notify= bb; \
  std::string tt; \
  XP_GET_STRING((xpup)->GetFrequency, tt); \
  (up).frequency= tt; \
  }

// WDFVersion <-> sashIXPWDFVersion
#define GETXPWDFVERSION(ver, xpver) { \
  nsCOMPtr<sashIXPWDFVersion> _res= do_CreateInstance(XPWDFVERSION_CONTRACT_ID); \
  sashIXPVersion *vv; \
  GETXPVERSION((ver).weblication, &vv); \
  _res->SetWeblication(vv); \
  NS_IF_RELEASE(vv); \
  GETXPVERSION((ver).sash.minimum, &vv); \
  _res->SetSashMinimum(vv); \
  NS_IF_RELEASE(vv); \
  GETXPVERSION((ver).sash.recommended, &vv); \
  _res->SetSashRecommended(vv); \
  NS_IF_RELEASE(vv); \
  GETXPVERSION((ver).mozilla.minimum, &vv); \
  _res->SetMozillaMinimum(vv); \
  NS_IF_RELEASE(vv); \
  GETXPVERSION((ver).mozilla.recommended, &vv); \
  _res->SetMozillaRecommended(vv); \
  NS_IF_RELEASE(vv); \
  *(xpver)= _res; \
  NS_IF_ADDREF(*(xpver)); \
  }
#define SETXPWDFVERSION(ver, xpver) { \
  sashIXPVersion *vv; \
  (xpver)->GetWeblication(&vv); \
  SETXPVERSION((ver).weblication, vv); \
  NS_IF_RELEASE(vv); \
  (xpver)->GetSashMinimum(&vv); \
  SETXPVERSION((ver).sash.minimum, vv); \
  NS_IF_RELEASE(vv); \
  (xpver)->GetSashRecommended(&vv); \
  SETXPVERSION((ver).sash.recommended, vv); \
  NS_IF_RELEASE(vv); \
  (xpver)->GetMozillaMinimum(&vv); \
  SETXPVERSION((ver).mozilla.minimum, vv); \
  NS_IF_RELEASE(vv); \
  (xpver)->GetMozillaRecommended(&vv); \
  SETXPVERSION((ver).mozilla.recommended, vv); \
  NS_IF_RELEASE(vv); \
  }  

// WDFDependency <-> sashIXPWDFDependency
#define GETXPWDFDEPENDENCY(dep, xpdep) { \
  nsCOMPtr<sashIXPWDFDependency> _res= do_CreateInstance(XPWDFDEPENDENCY_CONTRACT_ID); \
  _res->SetId((dep).id.c_str()); \
  _res->SetFilename((dep).filename.c_str()); \
  _res->SetTitle((dep).title.c_str()); \
  sashIXPVersion *vv; \
  GETXPVERSION((dep).required_version, &vv); \
  _res->SetRequiredVersion(vv); \
  NS_IF_RELEASE(vv); \
  *(xpdep)= _res; \
  NS_IF_ADDREF(*(xpdep)); \
  }
#define SETXPWDFDEPENDENCY(dep, xpdep) { \
  std::string tt; \
  XP_GET_STRING((xpdep)->GetId, tt); \
  (dep).id= tt; \
  XP_GET_STRING((xpdep)->GetFilename, tt); \
  (dep).filename= tt; \
  XP_GET_STRING((xpdep)->GetTitle, tt); \
  (dep).title= tt; \
  sashIXPVersion *vv; \
  (xpdep)->GetRequiredVersion(&vv); \
  SETXPVERSION((dep).required_version, vv); \
  NS_IF_RELEASE(vv); \
  }

// WDFAction <-> sashIXPWDFAction
#define GETXPWDFACTION(ac, xpac) { \
  nsCOMPtr<sashIXPWDFAction> _res= do_CreateInstance(XPWDFACTION_CONTRACT_ID); \
  _res->SetId((ac).id.c_str()); \
  _res->SetLocationid((ac).locationid.c_str()); \
  _res->SetName((ac).name.c_str()); \
  _res->SetRegistration((ac).registration.c_str()); \
  *(xpac)= _res; \
  NS_IF_ADDREF(*(xpac)); \
  }
#define SETXPWDFACTION(ac, xpac) { \
  std::string tt; \
  XP_GET_STRING((xpac)->GetId, tt); \
  (ac).id= tt; \
  XP_GET_STRING((xpac)->GetLocationid, tt); \
  (ac).locationid= tt; \
  XP_GET_STRING((xpac)->GetName, tt); \
  (ac).name= tt; \
  XP_GET_STRING((xpac)->GetRegistration, tt); \
  (ac).registration= tt; \
  }
    
// WDFFile <-> sashIXPWDFFile
#define GETXPWDFFILE(fi, xpfi) { \
  nsCOMPtr<sashIXPWDFFile> _res= do_CreateInstance(XPWDFFILE_CONTRACT_ID); \
  _res->SetPrecache((fi).precache); \
  _res->SetLocation((fi).location); \
  _res->SetFilename((fi).filename.c_str()); \
  *(xpfi)= _res; \
  NS_IF_ADDREF(*(xpfi)); \
  }
#define SETXPWDFFILE(fi, xpfi) { \
  PRBool bb; \
  (xpfi)->GetPrecache(&bb); \
  (fi).precache= bb; \
  PRInt32 ii; \
  (xpfi)->GetLocation(&ii); \
  (fi).location= (FileLocationType) ii; \
  std::string tt; \
  XP_GET_STRING((xpfi)->GetFilename, tt); \
  (fi).filename= tt; \
  }
  
// WDFInstallScreen <-> sashIXPWDFInstallScreen
#define GETXPWDFINSTALLSCREEN(in, xpin) { \
  nsCOMPtr<sashIXPWDFInstallScreen> _res= do_CreateInstance(XPWDFINSTALLSCREEN_CONTRACT_ID); \
  _res->SetType((in).type); \
  _res->SetTitle((in).title.c_str()); \
  _res->SetPageFile((in).page.file.c_str()); \
  _res->SetPageLocation((in).page.location); \
  _res->SetImageFile((in).image.file.c_str()); \
  _res->SetImageLocation((in).image.location); \
  _res->SetTextFile((in).text.file.c_str()); \
  _res->SetTextLocation((in).text.location); \
  _res->SetNext((in).next); \
  _res->SetBack((in).back); \
  *(xpin)= _res; \
  NS_IF_ADDREF(*(xpin)); \
  }
#define SETXPWDFINSTALLSCREEN(in, xpin) { \
  PRInt32 ii; \
  (xpin)->GetType(&ii); \
  (in).type= (InstallScreenType) ii; \
  string tt; \
  XP_GET_STRING((xpin)->GetTitle, tt); \
  (in).title= tt; \
  XP_GET_STRING((xpin)->GetPageFile, tt); \
  (in).page.file= tt; \
  (xpin)->GetPageLocation(&ii); \
  (in).page.location= (FileLocationType) ii; \
  XP_GET_STRING((xpin)->GetImageFile, tt); \
  (in).image.file= tt; \
  (xpin)->GetImageLocation(&ii); \
  (in).image.location= (FileLocationType) ii; \
  XP_GET_STRING((xpin)->GetTextFile, tt); \
  (in).text.file= tt; \
  (xpin)->GetTextLocation(&ii); \
  (in).text.location= (FileLocationType) ii; \
  PRBool bb; \
  (xpin)->GetNext(&bb); \
  (in).next= bb; \
  (xpin)->GetBack(&bb); \
  (in).back= bb; \
  }

#endif
