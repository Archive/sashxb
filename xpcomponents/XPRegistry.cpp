#include "XPRegistry.h"
#include "sashVariantUtils.h"
#include <string>

using namespace std;

NS_IMPL_ISUPPORTS1_CI(XPRegistry, sashIXPRegistry);

XPRegistry::XPRegistry()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
	m_registry = new Registry;
}

XPRegistry::~XPRegistry()
{
  /* destructor code */
	if(m_registry) delete m_registry;
}

/* boolean OpenFile (in string filename); */
NS_IMETHODIMP XPRegistry::OpenFile(const char *filename, PRBool *_retval)
{
	*_retval = m_registry->OpenFile(string(filename));
	return NS_OK;
}

/* void WriteToDisk (in string fileName); */
NS_IMETHODIMP XPRegistry::WriteToDisk(const char *fileName)
{
	m_registry->WriteToDisk(string(fileName));
	return NS_OK;
}

/* void SetFilename (in string filename); */
NS_IMETHODIMP XPRegistry::SetFilename(const char *filename)
{
	m_registry->SetFilename(string(filename));
	return NS_OK;
}

/* boolean CreateKey (in string key); */
NS_IMETHODIMP XPRegistry::CreateKey(const char *key, PRBool *_retval)
{
	*_retval = m_registry->CreateKey(string(key));
	return NS_OK;
}

/* boolean DeleteKey (in string key, in boolean delete_deep); */
NS_IMETHODIMP XPRegistry::DeleteKey(const char *key, PRBool delete_deep, PRBool *_retval)
{
	*_retval = m_registry->DeleteKey(string(key), delete_deep);
	return NS_OK;
}

/* boolean Rename (in string key, in string newname); */
NS_IMETHODIMP XPRegistry::Rename(const char *key, const char *newname, PRBool *_retval)
{
	*_retval = m_registry->Rename(string(key), string(newname));
	return NS_OK;
}

/* nsIVariant EnumKeys (in string key); */
NS_IMETHODIMP XPRegistry::EnumKeys(const char *key, nsIVariant **_retval)
{
	vector<string> keys = m_registry->EnumKeys(string(key));
	NewVariant(_retval);
	VariantSetArray(*_retval, keys);	
	return NS_OK;
}

/* nsIVariant EnumValues (in string key); */
NS_IMETHODIMP XPRegistry::EnumValues(const char *key, nsIVariant **_retval)
{
	vector<string> vals = m_registry->EnumValues(string(key));
	NewVariant(_retval);
	VariantSetArray(*_retval, vals);
	return NS_OK;
}

void RegistryValue2Variant(RegistryValue *rv, nsIVariant *var)
{
	RegistryValueType rvt = rv->GetValueType();
	switch(rvt)
	{
	case RVT_STRING:
		VariantSetString(var, rv->m_String);
		break;
	case RVT_NUMBER:
		VariantSetNumber(var, (double) rv->m_Number);
		break;
	case RVT_ARRAY:
		VariantSetArray(var, rv->m_Array);
		break;
	case RVT_INVALID:
	case RVT_BINARY:
		break;
	};
}

void Variant2RegistryValue(nsIVariant *var, RegistryValue *rv)
{
  if (VariantIsBoolean(var)) {
	rv->SetValueType(RVT_NUMBER);
	rv->m_Number = (VariantGetBoolean(var) == true);
  }
  else if (VariantIsString(var)) {
	rv->SetValueType(RVT_STRING);
	rv->m_String = VariantGetString(var);
  }
  else if (VariantIsNumber(var)) {
	rv->SetValueType(RVT_NUMBER);
	rv->m_Number = (float) VariantGetNumber(var);
  }
  else if (VariantIsArray(var)) {
	rv->SetValueType(RVT_ARRAY);
	VariantGetArray(var, rv->m_Array);
  }
  else {
	rv->SetValueType(RVT_INVALID);
  }
}

/* nsIVariant QueryValue (in string key, in string value_name); */
NS_IMETHODIMP XPRegistry::QueryValue(const char *key, const char *value_name, nsIVariant **_retval)
{
	RegistryValue rv;
	m_registry->QueryValue(string(key), string(value_name), &rv);
	NewVariant(_retval);
	RegistryValue2Variant(&rv, *_retval);
	return NS_OK;
}

/* void SetValue (in string key, in string value_name, in nsIVariant v); */
NS_IMETHODIMP XPRegistry::SetValue(const char *key, const char *value_name, nsIVariant *v)
{
	RegistryValue rv;
	Variant2RegistryValue(v, &rv);
	if(rv.IsValid()) m_registry->SetValue(string(key), string(value_name), &rv);
	return NS_OK;
}

/* boolean DeleteValue (in string key, in string value_name); */
NS_IMETHODIMP XPRegistry::DeleteValue(const char *key, const char *value_name, PRBool *_retval)
{
	*_retval = m_registry->DeleteValue(string(key), string(value_name));
	return NS_OK;
}

/* void DebugPrint (); */
NS_IMETHODIMP XPRegistry::DebugPrint()
{
	m_registry->DebugPrint();
	return NS_OK;
}
