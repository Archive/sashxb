#include "XPFileSystem.h"
#include "sashVariantUtils.h"
#include <string>

using namespace std;

NS_IMPL_ISUPPORTS1_CI(XPFileSystem, sashIXPFileSystem);

XPFileSystem::XPFileSystem()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
  m_FileSystem = new FileSystem;
}

XPFileSystem::~XPFileSystem()
{
  /* destructor code */
}

/* string GetTempName (); */
NS_IMETHODIMP XPFileSystem::GetTempName(char **_retval)
{
	 string temp = FileSystem::GetTempName();
	 XP_COPY_STRING (temp.c_str(), _retval);
	 return NS_OK;
}

/* boolean CopyFile (in string src, in string dest, in boolean overwrite); */
NS_IMETHODIMP XPFileSystem::CopyFile(const char *src, const char *dest, PRBool overwrite, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::CopyFile(src, dest, overwrite);
	 return NS_OK;
}

/* boolean MoveFile (in string fname, in string dest); */
NS_IMETHODIMP XPFileSystem::MoveFile(const char *fname, const char *dest, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::MoveFile(fname, dest);
    return NS_OK;
}

/* boolean DeleteFile (in string fname); */
NS_IMETHODIMP XPFileSystem::DeleteFile(const char *fname, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::DeleteFile(fname);
	 return NS_OK;
}

/* boolean FileExists (in string fname); */
NS_IMETHODIMP XPFileSystem::FileExists(const char *fname, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::FileExists(fname);
	 return NS_OK;
}

/* PRInt32 FileSize (in string fname); */
NS_IMETHODIMP XPFileSystem::FileSize(const char *fname, PRInt32 *_retval)
{
	 *_retval = (PRInt32) FileSystem::FileSize(fname);
	 return NS_OK;
}

/* PRInt32 FileLastModified (in string fname); */
NS_IMETHODIMP XPFileSystem::FileLastModified(const char *fname, PRInt32 *_retval)
{
	 *_retval = (PRInt32) FileSystem::FileLastModified(fname);
	 return NS_OK;
}

/* PRInt32 FileLastAccessed (in string fname); */
NS_IMETHODIMP XPFileSystem::FileLastAccessed(const char *fname, PRInt32 *_retval)
{
	 *_retval = (PRInt32) FileSystem::FileLastModified(fname);
	 return NS_OK;
}

/* boolean CreateSymlink (in string resource, in string link_name); */
NS_IMETHODIMP XPFileSystem::CreateSymlink(const char *resource, const char *link_name, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::CreateSymlink(resource, link_name);
	 return NS_OK;
}

/* boolean IsSymlink (in string fname); */
NS_IMETHODIMP XPFileSystem::IsSymlink(const char *fname, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::IsSymlink(fname);
	 return NS_OK;
}

/* boolean IsLocal (in string path, in string localdir); */
NS_IMETHODIMP XPFileSystem::IsLocal(const char *path, const char *localdir, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::IsLocal(path, localdir);
	 return NS_OK;
}

/* boolean CreateFolder (in string fname); */
NS_IMETHODIMP XPFileSystem::CreateFolder(const char *fname, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::CreateFolder(fname);
	 return NS_OK;
}

/* boolean FolderExists (in string dname); */
NS_IMETHODIMP XPFileSystem::FolderExists(const char *dname, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::FolderExists(dname);
	 return NS_OK;
}

/* PRInt32 FolderSize (in string fname); */
NS_IMETHODIMP XPFileSystem::FolderSize(const char *fname, PRInt32 *_retval)
{
	 *_retval = (PRInt32) FileSystem::FolderSize(fname);
	 return NS_OK;
}

/* boolean DeleteFolder (in string fname); */
NS_IMETHODIMP XPFileSystem::DeleteFolder(const char *fname, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::DeleteFolder(fname);
	 return NS_OK;
}

/* boolean CopyFolder (in string source, in string dest); */
NS_IMETHODIMP XPFileSystem::CopyFolder(const char *source, const char *dest, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::CopyFolder(source, dest);
	 return NS_OK;

}

/* boolean MoveFolder (in string fname, in string dest); */
NS_IMETHODIMP XPFileSystem::MoveFolder(const char *fname, const char *dest, PRBool *_retval)
{
	 *_retval = (PRBool) FileSystem::MoveFolder(fname, dest);
	 return NS_OK;
}

/* nsIVariant EnumFiles (in string dname); */
NS_IMETHODIMP XPFileSystem::EnumFiles(const char *dname, nsIVariant **_retval)
{
	 vector<string> files = FileSystem::EnumFiles(dname);
	 NewVariant(_retval);
	 VariantSetArray(*_retval, files);	
	 return NS_OK;
}

/* nsIVariant EnumDirs (in string dname); */
NS_IMETHODIMP XPFileSystem::EnumDirs(const char *dname, nsIVariant **_retval)
{
	 vector<string> dirs = FileSystem::EnumDirs(dname);
	 NewVariant(_retval);
	 VariantSetArray(*_retval, dirs);	
	 return NS_OK;
}

/* PRInt32 OpenFile (in string fname, in boolean read); */
NS_IMETHODIMP XPFileSystem::OpenFile(const char *fname, PRBool read, PRInt32 *_retval)
{
	 *_retval = (PRInt32) m_FileSystem->OpenFile(fname, read);
	 
	 return NS_OK;
}

/* void CloseFile (in PRInt32 fdesc); */
NS_IMETHODIMP XPFileSystem::CloseFile(PRInt32 fdesc)
{
	 m_FileSystem->CloseFile(fdesc);
	 return NS_OK;
}

/* PRInt32 WriteToFile (in string data, in PRInt32 length, in PRInt32 fdesc); */
NS_IMETHODIMP XPFileSystem::WriteToFile(const char *data, PRInt32 length, PRInt32 fdesc, PRInt32 *_retval)
{
	 *_retval = (PRInt32) m_FileSystem->WriteToFile(data, length, fdesc);
	 return NS_OK;
}

/* PRInt32 ReadFromFile (in string data, in PRInt32 length, in PRInt32 fdesc); */
NS_IMETHODIMP XPFileSystem::ReadFromFile(const char *data, PRInt32 length, PRInt32 fdesc, PRInt32 *_retval)
{
	 *_retval = (PRInt32) m_FileSystem->ReadFromFile((char *)data, length, fdesc);
	 return NS_OK;
}

/* PRInt32 SetFilePos (in PRInt32 fdesc, in PRInt32 pos); */
NS_IMETHODIMP XPFileSystem::SetFilePos(PRInt32 fdesc, PRInt32 pos, PRInt32 *_retval)
{
	 *_retval = (PRInt32) m_FileSystem->SetFilePos(fdesc, pos);
	 return NS_OK;
}
