
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "XPXMLIterator.h"
#include "XPXMLNode.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"

NS_IMPL_ISUPPORTS1_CI(XPXMLIterator, sashIXPXMLIterator)

XPXMLIterator::XPXMLIterator() 
  : xi(XMLNode(), "") {
  NS_INIT_ISUPPORTS();
}

XPXMLIterator::~XPXMLIterator() {
}

NS_IMETHODIMP XPXMLIterator::Initialize(sashIXPXMLNode *node, const char *str) {
  XMLNode other;
  if (node)
	   node->GetInternalNode(&other);
  xi= XMLIterator(other, str);
  xi.begin();
  return NS_OK;
}
NS_IMETHODIMP XPXMLIterator::HasNext(PRBool *_retval) {
  *_retval= xi.hasNext();
  return NS_OK;
}

NS_IMETHODIMP XPXMLIterator::GetNext(sashIXPXMLNode **_retval) {
	 XMLNode tempnode = xi.getNext();
	 *_retval = NewSashXPXMLNode(&tempnode);
	 return (*_retval ? NS_OK : NS_ERROR_FAILURE);
}

NS_IMETHODIMP XPXMLIterator::Begin() {
  xi.begin();
  return NS_OK;
}
