#ifndef _XPFILESYSTEM_H
#define _XPFILESYSTEM_H

#include "sashIXPFileSystem.h"
#include "FileSystem.h"

//0c936734-cd20-4479-aa3e-b377d38eb4e2
#define XPFILESYSTEM_CID {0x0c936734, 0xcd20, 0x4479, {0xaa, 0x3e, 0xb3, 0x77, 0xd3, 0x8e, 0xb4, 0xe2}}

NS_DEFINE_CID(kXPFileSystemCID, XPFILESYSTEM_CID);

#define XPFILESYSTEM_CONTRACT_ID "@gnome.org/SashXB/XPFileSystem;1"

class XPFileSystem : public sashIXPFileSystem
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPFILESYSTEM

  XPFileSystem();
  virtual ~XPFileSystem();
  /* additional members */

private:
	FileSystem *m_FileSystem;
};

#endif
