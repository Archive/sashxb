#ifndef _SASHFULLREGISTRY_H
#define _SASHFULLREGISTRY_H

#include "sashIXPRegistry.h"
#include "Registry.h"
#include "sashIExtension.h"

//E5728C93-9A6F-48B7-9CFA-F9B42946FCF5
#define XPREGISTRY_CID {0xE5728C93, 0x9A6F, 0x48B7, {0x9C, 0xFA, 0xF9, 0xB4, 0x29, 0x46, 0xFC, 0xF5}}
NS_DEFINE_CID(kXPRegistryCID, XPREGISTRY_CID);

#define XPREGISTRY_CONTRACT_ID "@gnome.org/SashXB/XPRegistry;1"

class XPRegistry : public sashIXPRegistry
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPREGISTRY

  XPRegistry();
  virtual ~XPRegistry();
  /* additional members */

private:
	Registry *m_registry;
};

#endif
