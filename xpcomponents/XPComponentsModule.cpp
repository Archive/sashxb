
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142

    USA

*****************************************************************

Contributor(s): Wing Yung

module for the XPRegistry
******************************************************************/

#include "XPRegistry.h"
#include "XPFileSystem.h"
#include "XPXMLNode.h"
#include "XPXMLIterator.h"
#include "XPXMLDocument.h"
#include "XPWDFStructs.h"
#include "XPWDF.h"
#include "extensiontools.h"

NS_DECL_CLASSINFO(XPRegistry);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPRegistry);
NS_DECL_CLASSINFO(XPFileSystem);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPFileSystem);
NS_DECL_CLASSINFO(XPXMLNode);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPXMLNode);
NS_DECL_CLASSINFO(XPXMLIterator);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPXMLIterator);
NS_DECL_CLASSINFO(XPXMLDocument);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPXMLDocument);

// WDF related
NS_DECL_CLASSINFO(XPVersion);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPVersion);
NS_DECL_CLASSINFO(XPWDFDependency);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPWDFDependency);
NS_DECL_CLASSINFO(XPWDFExpiration);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPWDFExpiration);
NS_DECL_CLASSINFO(XPWDFUpdate);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPWDFUpdate);
NS_DECL_CLASSINFO(XPWDFAction);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPWDFAction);
NS_DECL_CLASSINFO(XPWDFVersion);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPWDFVersion);
NS_DECL_CLASSINFO(XPWDFInstallScreen);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPWDFInstallScreen);
NS_DECL_CLASSINFO(XPWDFFile);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPWDFFile);
NS_DECL_CLASSINFO(XPWDF);
NS_GENERIC_FACTORY_CONSTRUCTOR(XPWDF);

static nsModuleComponentInfo components[] = {
	 MODULE_COMPONENT_ENTRY(XPRegistry, XPREGISTRY, "XPRegistry Object"),
	 MODULE_COMPONENT_ENTRY(XPFileSystem, XPFILESYSTEM, "XPFileSystem Object"),
	 MODULE_COMPONENT_ENTRY(XPXMLNode, XPXMLNODE, "XPXMLNode Object"),
	 MODULE_COMPONENT_ENTRY(XPXMLIterator, XPXMLITERATOR, "XPXMLIterator Object"),
	 MODULE_COMPONENT_ENTRY(XPXMLDocument, XPXMLDOCUMENT, "XPXMLDocument Object"),
   // WDF related
	 MODULE_COMPONENT_ENTRY(XPVersion, XPVERSION, "XPVersion Object"),
	 MODULE_COMPONENT_ENTRY(XPWDFDependency, XPWDFDEPENDENCY, "XPWDFDependency Object"),
	 MODULE_COMPONENT_ENTRY(XPWDFExpiration, XPWDFEXPIRATION, "XPWDFExpiration Object"),
	 MODULE_COMPONENT_ENTRY(XPWDFUpdate, XPWDFUPDATE, "XPWDFUpdate Object"),
	 MODULE_COMPONENT_ENTRY(XPWDFAction, XPWDFACTION, "XPWDFAction Object"),
	 MODULE_COMPONENT_ENTRY(XPWDFVersion, XPWDFVERSION, "XPWDFVersion Object"),
	 MODULE_COMPONENT_ENTRY(XPWDFInstallScreen, XPWDFINSTALLSCREEN, "XPWDFInstallScreen Object"),
	 MODULE_COMPONENT_ENTRY(XPWDFFile, XPWDFFILE, "XPWDFFile Object"),
	 MODULE_COMPONENT_ENTRY(XPWDF, XPWDF, "XPWDF Object")
};

NS_IMPL_NSGETMODULE(XPRegistry, components)
