/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s):
    Stefan Atev (atevstef@luther.edu)

*****************************************************************/

#include "XPWDFStructs.h"
#include "XPWDF.h"
#include "xpcomtools.h" 
#include "prmem.h"
#include "sashVariantUtils.h"

NS_IMPL_ISUPPORTS1_CI(XPWDF, sashIXPWDF)

XPWDF::XPWDF() {
  NS_INIT_ISUPPORTS();
}

XPWDF::~XPWDF() {
}

/* boolean OpenAmbigousFile (in string filename); */
NS_IMETHODIMP XPWDF::OpenAmbiguousFile(const char *filename, PRBool *_retval) {
  *_retval= m_wdf.OpenAmbiguousFile(filename);
  return NS_OK;
}

/* boolean LoadFromFile (in string filename); */
NS_IMETHODIMP XPWDF::LoadFromFile(const char *filename, PRBool *_retval) {
  *_retval= m_wdf.LoadFromFile(filename);
  return NS_OK;
}

/* void SaveToFile (in string filename); */
NS_IMETHODIMP XPWDF::SaveToFile(const char *filename) {
  m_wdf.SaveToFile(filename);
  return NS_OK;
}

/* boolean ConvertFromWDFDoc (in string filename); */
NS_IMETHODIMP XPWDF::ConvertFromWDFDoc(const char *filename, PRBool *_retval) {
  *_retval= m_wdf.ConvertFromWDFDoc(filename);
  return NS_OK;
}

/* void Clear (); */
NS_IMETHODIMP XPWDF::Clear() {
  m_wdf.Clear();
  return NS_OK;
}

/* attribute string id; */
NS_IMETHODIMP XPWDF::GetId(char * *aId) {
  GETXPSTR(m_wdf.GetID(), aId);
}
NS_IMETHODIMP XPWDF::SetId(const char * aId) {
  m_wdf.SetID(aId);
  return NS_OK;
}

/* attribute long extensionType; */
NS_IMETHODIMP XPWDF::GetExtensionType(PRInt32 *aExtensionType) {
  GETXPINT(m_wdf.GetExtensionType(), aExtensionType)
}
NS_IMETHODIMP XPWDF::SetExtensionType(PRInt32 aExtensionType) {
  m_wdf.SetExtensionType((SashExtensionType) aExtensionType);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::GetPlatforms(nsIVariant **plats) {
  vector<string> _plats= m_wdf.GetPlatforms();
  NewVariant(plats);
  VariantSetArray(*plats, _plats);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::SetPlatforms(nsIVariant *plats) {
  vector<string> _plats;
  if (!VariantIsArray(plats)) return NS_ERROR_ILLEGAL_VALUE;
  VariantGetArray(plats, _plats);
  m_wdf.SetPlatforms(_plats);
  return NS_OK;
}

/* attribute sashIXPWDFExpiration expiration; */
NS_IMETHODIMP XPWDF::GetExpiration(sashIXPWDFExpiration * *aExpiration) {
  WDFExpiration tmp= m_wdf.GetExpiration();
  GETXPWDFEXPIRATION(tmp, aExpiration);
  return NS_OK;
}
NS_IMETHODIMP XPWDF::SetExpiration(sashIXPWDFExpiration * aExpiration) {
  WDFExpiration tmp;
  SETXPWDFEXPIRATION(tmp, aExpiration);
  m_wdf.SetExpiration(tmp);
  return NS_OK;
}

/* attribute sashIXPWDFUpdate update; */
NS_IMETHODIMP XPWDF::GetUpdate(sashIXPWDFUpdate * *aUpdate) {
  WDFUpdate tmp= m_wdf.GetUpdate();
  GETXPWDFUPDATE(tmp, aUpdate);
  return NS_OK;
}
NS_IMETHODIMP XPWDF::SetUpdate(sashIXPWDFUpdate * aUpdate) {
  WDFUpdate tmp;
  SETXPWDFUPDATE(tmp, aUpdate);
  m_wdf.SetUpdate(tmp);
  return NS_OK;
}

/* attribute sashIXPWDFVersion version; */
NS_IMETHODIMP XPWDF::GetVersion(sashIXPWDFVersion * *aVersion) {
  WDFVersion tmp= m_wdf.GetVersion();
  GETXPWDFVERSION(tmp, aVersion);
  return NS_OK;
}
NS_IMETHODIMP XPWDF::SetVersion(sashIXPWDFVersion * aVersion) {
  WDFVersion tmp;
  SETXPWDFVERSION(tmp, aVersion);
  m_wdf.SetVersion(tmp);
  return NS_OK;
}

/* attribute string author; */
NS_IMETHODIMP XPWDF::GetAuthor(char * *aAuthor) {
  GETXPSTR(m_wdf.GetAuthor(), aAuthor);
}
NS_IMETHODIMP XPWDF::SetAuthor(const char * aAuthor) {
  m_wdf.SetAuthor(aAuthor);
  return NS_OK;
}

/* attribute string title; */
NS_IMETHODIMP XPWDF::GetTitle(char * *aTitle) {
  GETXPSTR(m_wdf.GetTitle(), aTitle);
}
NS_IMETHODIMP XPWDF::SetTitle(const char * aTitle) {
  m_wdf.SetTitle(aTitle);
  return NS_OK;
}

/* attribute string abstractText; */
NS_IMETHODIMP XPWDF::GetAbstractText(char * *aAbstract) {
  GETXPSTR(m_wdf.GetAbstract(), aAbstract);
}
NS_IMETHODIMP XPWDF::SetAbstractText(const char * aAbstract) {
  m_wdf.SetAbstract(aAbstract);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::GetDependencies(nsIVariant **deps) {
  vector<WDFDependency> _deps= m_wdf.GetDependencies();
  vector<nsISupports *> _depsXP;
  NewVariant(deps);
  vector<WDFDependency>::iterator i;
  for (i= _deps.begin(); i< _deps.end(); ++i) {
    sashIXPWDFDependency *tmp;
    nsISupports *tmpns;
    GETXPWDFDEPENDENCY(*i, &tmp);
    tmp->QueryInterface(NS_GET_IID(nsISupports), (void **) &tmpns);
    NS_IF_RELEASE(tmp);
    _depsXP.push_back(tmpns);
  }
  VariantSetArray(*deps, _depsXP);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::SetDependencies(nsIVariant *deps) {
  if (!VariantIsArray(deps)) return NS_ERROR_ILLEGAL_VALUE;
  int length= VariantGetArrayLength(deps);
  vector<WDFDependency> _deps(length);
  vector<nsISupports *> _depsXP;
  VariantGetArray(deps, _depsXP);
  vector<nsISupports *>::iterator i;
  int j;
  for (i= _depsXP.begin(), j= 0; i< _depsXP.end(); ++i, ++j) {
    sashIXPWDFDependency *tmp;
    nsresult res;
    res= (*i)->QueryInterface(NS_GET_IID(sashIXPWDFDependency), (void **) &tmp);
    SETXPWDFDEPENDENCY(_deps[j], tmp);
    NS_IF_RELEASE(tmp);
  }
  m_wdf.SetDependencies(_deps);
  return NS_OK;
}

/* void AddDependency (in sashIXPWDFDependency dep); */
NS_IMETHODIMP XPWDF::AddDependency(sashIXPWDFDependency *dep) {
  WDFDependency tmp;
  SETXPWDFDEPENDENCY(tmp, dep);
  m_wdf.AddDependency(tmp);
  return NS_OK;
}

/* boolean RemoveDependency (in string id); */
NS_IMETHODIMP XPWDF::RemoveDependency(const char *id, PRBool *_retval) {
  *_retval= m_wdf.RemoveDependency(id);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::GetInstallScreens(nsIVariant **inst) {
  vector<WDFInstallScreen> _inst= m_wdf.GetInstallScreens();
  vector<nsISupports *> _instXP;
  NewVariant(inst);
  vector<WDFInstallScreen>::iterator i;
  for (i= _inst.begin(); i< _inst.end(); ++i) {
    sashIXPWDFInstallScreen *tmp;
    nsISupports *tmpns;
    GETXPWDFINSTALLSCREEN(*i, &tmp);
    tmp->QueryInterface(NS_GET_IID(nsISupports), (void **) &tmpns);
    NS_IF_RELEASE(tmp);
    _instXP.push_back(tmpns);
  }
  VariantSetArray(*inst, _instXP);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::SetInstallScreens(nsIVariant *inst) {
  if (!VariantIsArray(inst)) return NS_ERROR_ILLEGAL_VALUE;
  int length= VariantGetArrayLength(inst);
  vector<WDFInstallScreen> _inst(length);
  vector<nsISupports *> _instXP;
  VariantGetArray(inst, _instXP);
  vector<nsISupports *>::iterator i;
  int j;
  for (i= _instXP.begin(), j= 0; i< _instXP.end(); ++i, ++j) {
    sashIXPWDFInstallScreen *tmp;
    nsresult res;
    res= (*i)->QueryInterface(NS_GET_IID(sashIXPWDFInstallScreen), (void **) &tmp);
    SETXPWDFINSTALLSCREEN(_inst[j], tmp);
    NS_IF_RELEASE(tmp);
  }
  m_wdf.SetInstallScreens(_inst);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::GetActions(nsIVariant **acts) {
  vector<WDFAction> _acts= m_wdf.GetActions();
  vector<nsISupports *> _actsXP;
  NewVariant(acts);
  vector<WDFAction>::iterator i;
  for (i= _acts.begin(); i< _acts.end(); ++i) {
    sashIXPWDFAction *tmp;
    nsISupports *tmpns;
    GETXPWDFACTION(*i, &tmp);
    tmp->QueryInterface(NS_GET_IID(nsISupports), (void **) &tmpns);
    NS_IF_RELEASE(tmp);
    _actsXP.push_back(tmpns);
  }
  VariantSetArray(*acts, _actsXP);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::SetActions(nsIVariant *acts) {
  if (!VariantIsArray(acts)) return NS_ERROR_ILLEGAL_VALUE;
  int length= VariantGetArrayLength(acts);
  vector<WDFAction> _acts(length);
  vector<nsISupports *> _actsXP;
  VariantGetArray(acts, _actsXP);
  vector<nsISupports *>::iterator i;
  int j;
  for (i= _actsXP.begin(), j= 0; i< _actsXP.end(); ++i, ++j) {
    sashIXPWDFAction *tmp;
    nsresult res;
    res= (*i)->QueryInterface(NS_GET_IID(sashIXPWDFAction), (void **) &tmp);
    SETXPWDFACTION(_acts[j], tmp);
    NS_IF_RELEASE(tmp);
  }
  m_wdf.SetActions(_acts);
  return NS_OK;
}

/* sashIXPWDFAction GetAction (in string guid); */
NS_IMETHODIMP XPWDF::GetAction(const char *guid, sashIXPWDFAction **_retval) {
  WDFAction tmp= m_wdf.GetAction(guid);
  GETXPWDFACTION(tmp, _retval);
  return NS_OK;
}

/* void AddAction (in sashIXPWDFAction act); */
NS_IMETHODIMP XPWDF::AddAction(sashIXPWDFAction *act) {
  WDFAction tmp;
  SETXPWDFACTION(tmp, act);
  m_wdf.AddAction(tmp);
  return NS_OK;
}

/* boolean RemoveAction (in string guid); */
NS_IMETHODIMP XPWDF::RemoveAction(const char *guid, PRBool *_retval) {
  *_retval= m_wdf.RemoveAction(guid);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::GetFiles(nsIVariant **files) {
  vector<WDFFile> _files= m_wdf.GetFiles();
  vector<nsISupports *> _filesXP;
  NewVariant(files);
  vector<WDFFile>::iterator i;
  for (i= _files.begin(); i< _files.end(); ++i) {
    sashIXPWDFFile *tmp;
    nsISupports *tmpns;
    GETXPWDFFILE(*i, &tmp);
    tmp->QueryInterface(NS_GET_IID(nsISupports), (void **) &tmpns);
    NS_IF_RELEASE(tmp);
    _filesXP.push_back(tmpns);
  }
  VariantSetArray(*files, _filesXP);
  return NS_OK;
}

NS_IMETHODIMP XPWDF::SetFiles(nsIVariant *files) {
  if (!VariantIsArray(files)) return NS_ERROR_ILLEGAL_VALUE;
  int length= VariantGetArrayLength(files);
  vector<WDFFile> _files(length);
  vector<nsISupports *> _filesXP;
  VariantGetArray(files, _filesXP);
  vector<nsISupports *>::iterator i;
  int j;
  for (i= _filesXP.begin(), j= 0; i< _filesXP.end(); ++i, ++j) {
    sashIXPWDFFile *tmp;
    nsresult res;
    res= (*i)->QueryInterface(NS_GET_IID(sashIXPWDFFile), (void **) &tmp);
    SETXPWDFFILE(_files[j], tmp);
    NS_IF_RELEASE(tmp);
  }
  m_wdf.SetFiles(_files);
  return NS_OK;
}

/* void AddFile (in sashIXPWDFFile file); */
NS_IMETHODIMP XPWDF::AddFile(sashIXPWDFFile *file) {
  WDFFile tmp;
  SETXPWDFFILE(tmp, file);
  m_wdf.AddFile(tmp);
  return NS_OK;
}

/* boolean RemoveFile (in string filename); */
NS_IMETHODIMP XPWDF::RemoveFile(const char *filename, PRBool *_retval) {
  *_retval= m_wdf.RemoveFile(filename);
  return NS_OK;
}

/* sashIXPRegistry GetRegistry (in string wdfPath); */
NS_IMETHODIMP XPWDF::GetRegistry(const char *wdfPath, sashIXPRegistry **_retval) {
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* void SetRegistry (in sashIXPRegistry reg, in string regPath); */
NS_IMETHODIMP XPWDF::SetRegistry(sashIXPRegistry *reg, const char *regPath) {
  return NS_ERROR_NOT_IMPLEMENTED;
}
