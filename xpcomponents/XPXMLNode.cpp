
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "XPXMLNode.h"
#include "XMLNode.h"
#include <string>
#include <vector>
#include "sashVariantUtils.h"

NS_IMPL_ISUPPORTS1_CI(XPXMLNode, sashIXPXMLNode)

XPXMLNode::XPXMLNode() {
  NS_INIT_ISUPPORTS();
}

XPXMLNode::~XPXMLNode() {
}

NS_IMETHODIMP 
XPXMLNode::Initialize(const char * str) {
	 xn = XMLNode(str);
	 return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::GetName(char * *aName) 
{
	 if (xn.isNull()) {
		  XP_COPY_STRING("", aName);
	 } else {
		  XP_COPY_STRING(xn.getName(), aName);
	 }
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::GetText(char * *aText) 
{
  XP_COPY_STRING(xn.getText(), aText);
  return NS_OK;
}

NS_IMETHODIMP
XPXMLNode::SetText(const char * aText) 
{
  xn.setText(aText);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::GetXml(char * *aXml) 
{
	 if (xn.isNull()) {
		  XP_COPY_STRING("", aXml);
	 } else {
		  XP_COPY_STRING(xn.getXML(), aXml);
	 }
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::SetXml(const char * aXml) 
{
  xn.setXML(aXml);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::HasAttribute(const char *attribute_name, PRBool *_retval) 
{
  *_retval= xn.hasAttribute(attribute_name);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::HasAttributes(const char *pattern, PRUint32 *_retval) 
{
	 string patstr= pattern;
  *_retval= xn.hasAttributes(patstr);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::GetAttribute(const char *attribute_name, char **_retval) 
{
	 XP_COPY_STRING(xn.getAttribute(attribute_name), _retval);
	 return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::SetAttribute(const char *attribute_name, 
													const char *value, PRBool *_retval) 
{
	 DEBUGMSG(xpxml, "Setting attribute %s to %s\n", attribute_name, value);
  *_retval= xn.setAttribute(attribute_name, value);
  DEBUGMSG(xpxml, "Returning from XPXMLNode::SetAttribute\n");
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::RemoveAttribute(const char *attribute_name, PRBool *_retval) 
{
  *_retval= xn.removeAttribute(attribute_name);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::GetAttributeNames(const char *pattern, nsIVariant **_retval) 
{
  string patstr= pattern;
  vector<string> res;
  res= xn.getAttributeNames(patstr);
  
  NewVariant(_retval);
  VariantSetArray(*_retval, res);
	
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::HasChildNodes(const char *pattern, PRUint32 *_retval) 
{
  string patstr= pattern;
  *_retval= xn.hasChildNodes(patstr);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::GetChildNodes(const char *pattern, nsIVariant **_retval) 
{
  string patstr= pattern;
  vector<XMLNode> res;
  res= xn.getChildNodes(patstr);  
  sashIXPXMLNode *tmp_node;

  NewVariant(_retval);

  vector <nsISupports *> cnodes;

  for (unsigned int i= 0; i< res.size(); ++i) {
	   tmp_node = NewSashXPXMLNode(&(res[i]));
	   cnodes.push_back(tmp_node);
  }

  VariantSetArray(*_retval, cnodes);
  
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::GetFirstChildNode(const char *pattern, sashIXPXMLNode **_retval) 
{
	 string patstr= pattern;
	 XMLNode tempnode = xn.getFirstChildNode(patstr);

	 *_retval = NewSashXPXMLNode(&tempnode);
	 return (*_retval ? NS_OK : NS_ERROR_FAILURE);
}

NS_IMETHODIMP 
XPXMLNode::GetNextSibling(const char *pattern, sashIXPXMLNode **_retval) 
{
  string patstr= pattern;
  XMLNode tempnode = xn.getNextSibling(patstr);
  *_retval = NewSashXPXMLNode(&tempnode);
  return (*_retval ? NS_OK : NS_ERROR_FAILURE);
}

NS_IMETHODIMP 
XPXMLNode::GetParent(sashIXPXMLNode **aParent) 
{
  XMLNode tempnode = xn.getParent();
  *aParent = NewSashXPXMLNode(&tempnode);

  return (*aParent ? NS_OK : NS_ERROR_FAILURE);
}

NS_IMETHODIMP 
XPXMLNode::RemoveChildNodes(const char *pattern, PRUint32 *_retval) 
{
  string patstr= pattern;
  *_retval= xn.removeChildNodes(patstr);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::RemoveChild(sashIXPXMLNode *child, PRBool *_retval) 
{
  XMLNode other;
  child->GetInternalNode(&other);
  *_retval= xn.removeChild(other);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::AppendChild(sashIXPXMLNode *child, PRBool *_retval) 
{
  XMLNode other;
  child->GetInternalNode(&other);
  *_retval= xn.importChild(other);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::AppendChildNodes(nsIVariant *children, PRUint32 *_retval) 
{
  PRBool ttt;
  nsCOMPtr<sashIXPXMLNode> tmp_node;
  *_retval= 0;
  if (!VariantIsArray(children)) 
    return NS_OK;

  DEBUGMSG(xpxml, "XPXMLNode::AppendChildNodes\n");
  vector<nsISupports*> stuff;
  VariantGetArray(children, stuff);

  DEBUGMSG(xpxml, "Got %d children\n", stuff.size());
  for (unsigned int i= 0; i< stuff.size(); ++i) {
	   tmp_node = do_QueryInterface(stuff[i]);
	   if (tmp_node) {
			AppendChild(tmp_node, &ttt);
			if (ttt) ++(*_retval);
	   }
  }
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::AppendXML(const char *xml, PRBool *_retval) 
{
  *_retval= xn.appendXML(xml);
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::GetInternalNode(XMLNode *retval) 
{
	 DEBUGMSG(xpxml, "Getting internal node, name=%s\n", xn.getName().c_str());
  *retval= xn;
  return NS_OK;
}

NS_IMETHODIMP 
XPXMLNode::SetInternalNode(XMLNode *retval) {
  xn= *retval;
  return NS_OK;
}

NS_IMETHODIMP
XPXMLNode::Clone(sashIXPXMLNode ** newNode) {
	 *newNode = NewSashXPXMLNode(&xn);
	 return NS_OK;
}

NS_IMETHODIMP
XPXMLNode::IsNull(PRBool * _retval) {
	 *_retval = xn.isNull();
	 return NS_OK;
}
