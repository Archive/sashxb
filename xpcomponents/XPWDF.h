#ifndef XPWDF_H
#define XPWDF_H

/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s):
    Stefan Atev (atevstef@luther.edu)

*****************************************************************/

#include "sashIXPWDFStructs.h"
#include "sashIXPWDF.h"
#include "sash_version.h"
#include "wdf.h"
#include "xpcomtools.h"
#include <string>

// 30ca2d91-a252-472a-94bd-5794dbc8f663
NS_DEFINE_CID(kXPWDFCID, XPWDF_CID);

class XPWDF: public sashIXPWDF {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIXPWDF

  XPWDF();
  virtual ~XPWDF();
private:
  WDF m_wdf;
};

#endif
