
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include <iostream>
#include <string>
#include "jsapi.h"
#include "sashIConstructor.h"
#include "sashGenericConstructor.h"
#include "extensiontools.h"
#include "RuntimeTools.h"
#include "sashVariantUtils.h"

NS_IMPL_ISUPPORTS3_CI(sashGenericConstructor, sashIGenericConstructor, sashIGenericScriptableConstructor, nsIXPCScriptable)

sashGenericConstructor::sashGenericConstructor() {
  NS_INIT_ISUPPORTS();
}

sashGenericConstructor::~sashGenericConstructor() {
}

/* all we want is to intercept Construct */
#define XPC_MAP_CLASSNAME sashGenericConstructor
#define XPC_MAP_QUOTED_CLASSNAME "sashGenericConstructor"
#define XPC_MAP_WANT_CONSTRUCT
#define XPC_MAP_FLAGS 0
#include "xpc_map_end.h"

/* this is called when you use new on the object */
NS_IMETHODIMP sashGenericConstructor::Construct(
  nsIXPConnectWrappedNative *wrapper,
  JSContext *cx, JSObject * obj,
  PRUint32 argc, jsval *argv,
  jsval *vp,
  PRBool *_retval) {
    
  DEBUGMSG(constructor, "Construct()\n");
  CustomConstruct(wrapper, cx, obj, argc, argv, vp, _retval);
  return NS_OK;
}

NS_IMETHODIMP sashGenericConstructor::SetConstructorProperties(
	 sashIActionRuntime * actionRuntime,
	 sashIExtension *parentExtension,
	 const char *contractid,
	 const char *interface_id_holder) 
{
	 m_contractid = contractid;
	 m_interface= interface_id_holder;
	 m_ext  = parentExtension;
	 m_act = actionRuntime;
	 return NS_OK;
}

/* [noscript] boolean CustomConstruct (in nsIXPConnectWrappedNative wrapper, in pJSContext cx, in pJSObject obj, in PRUint32 argc, in pjsval argv, in pjsval vp); */
NS_IMETHODIMP sashGenericConstructor::CustomConstruct(
													  nsIXPConnectWrappedNative *wrapper,
													  JSContext *cx,
													  JSObject *obj,
													  PRUint32 argc,
													  jsval *argv,
													  jsval *vp,
													  PRBool *_retval) 
{    
  nsISupports *result;
  nsresult res;
  nsCID cid;
  nsIID iid;

  /* we have achieved nothing yet */  
  *_retval= false;
  *vp= nsnull;
  string iid_full= m_interface;
  if (iid_full[0]!= '{')
    iid_full= "{"+ iid_full+ "}";
  if (!iid.Parse(iid_full.c_str())) {
	cout << "Unable to parse IID " << iid_full << endl;
	return NS_ERROR_FAILURE;
  }

  res= nsComponentManager::ContractIDToClassID(m_contractid.c_str(), &cid);
  if (res!= NS_OK) {
	cout << "Unable to convert contract ID to class ID" << endl;
	return res;
  }
  DEBUGMSG(runtimetools, "Generic constructor: creating instance of object %s, iid %s\n", cid.ToString(), iid_full.c_str());
  res= nsComponentManager::CreateInstance(cid, NULL, iid, (void **) &result);
  if (res!= NS_OK) {
	cout << "Unable to create instance of object " << cid.ToString() << endl;
	return res;
  }

  nsCOMPtr<sashIConstructor> construct_result = do_QueryInterface(result);
  if (construct_result) {
    nsIVariant **arr;
    arr= new (nsIVariant *)[argc];

    for (PRUint32 i= 0; i< argc; ++i)
	  arr[i] = RuntimeTools::CreateVariantFromJSVal(cx, argv[i]);

	DEBUGMSG(runtimetools, "Generic constructor: initializing new object\n");
    construct_result->InitializeNewObject(m_act, m_ext, cx, argc, arr, _retval);
    for (PRUint32 i= 0; i< argc; ++i)
      arr[i]->Release();
    delete [] arr;
  }
  else
  {
	   nsCOMPtr<sashIJSConstructor> js_constructor = do_QueryInterface(result);
	   if (js_constructor) {
			js_constructor->InitializeNewObject(m_act, m_ext, cx, argc, argv, _retval);
	   }
	   else {
			// Try scriptable constructor
			nsCOMPtr<sashIScriptableConstructor> script_constructor= do_QueryInterface(result);
			if (script_constructor) {
				PRInt32 contextID;
				if (NS_FAILED(m_act->RegisterScriptContext(cx, &contextID)))
					printf("Unable to register scriptable context with runtime\n");
				else {
					nsIVariant **arr;
					arr= new (nsIVariant *)[argc];

					for (PRUint32 i= 0; i< argc; ++i)
						arr[i]= RuntimeTools::CreateVariantFromJSVal(cx, argv[i]);
						
					script_constructor->InitializeNewObject(
						m_act, m_ext2, contextID, argc, arr, _retval
					);
					for (PRUint32 i= 0; i< argc; ++i)
						arr[i]->Release();
					delete [] arr;
				}
			}
			else
				printf("Unable to get sashIConstructor interface from object\n");
	   }
  }
  /* wrap native object around */
  JSObject *jsobj;

  RuntimeTools::WrapSashObject(iid, result, cx, &jsobj);
  NS_IF_RELEASE(result);
  *vp= OBJECT_TO_JSVAL(jsobj);
  DEBUGMSG(runtimetools, "Generic constructor: done!\n");
  return NS_OK;
}

/* void SetScriptableConstructorProperties (in sashIActionRuntime actionRuntime, in sashIExtension2 parentExtension, in string contractid, in string interface_id_holder); */
NS_IMETHODIMP sashGenericConstructor::SetScriptableConstructorProperties(sashIActionRuntime *actionRuntime, sashIExtension2 *parentExtension, const char *contractid, const char *interface_id_holder) {
	m_contractid= contractid;
	m_interface= interface_id_holder;
	m_ext2= parentExtension;
	m_act= actionRuntime;
	return NS_OK;
}

NS_DECL_CLASSINFO(sashGenericConstructor);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashGenericConstructor);

#define _ENTRY MODULE_COMPONENT_ENTRY(sashGenericConstructor, SASHGENERICCONSTRUCTOR, "Generic Constructor")

static nsModuleComponentInfo sashGenericConstructor_components[]= {_ENTRY};
NS_IMPL_NSGETMODULE(sashGenericConstructor, sashGenericConstructor_components);
