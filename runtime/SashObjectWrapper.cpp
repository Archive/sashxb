
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Kevin Gibbs, John Corwin, AJ Shankar

*****************************************************************/

#include "SashObjectWrapper.h"

#include "nsIXPConnect.h"
#include "xptinfo.h"
#include "nsCOMPtr.h"
#include "nsIServiceManager.h"
#include "nsString.h"
#include "extensiontools.h"
#include "InstallationManager.h"
#include "debugmsg.h"

#include <string>
#include <map>
#include <utility>
#include "sashVariantUtils.h"
#include "RuntimeTools.h"

using namespace std;

const string SashHandlerName = "SashFunctionHandler";

hash_map<string, WrapperInfo*, strhash> WrapperTable;

// todo: query for sashIUnknown
struct Prop {
	 string name;
	 bool has_setter;
};

struct Func {
	 bool operator==(const Func& q) const { return (name == q.name); }
	 bool operator==(const char* c) const { return (c == name); }
	 bool operator<(const Func& q) const { return (name < q.name); }
	 Func(string n) : name(n), has_retval(false) { }
	 string name;
	 vector<bool> args; // true if arg is variant
	 bool has_retval;
};

struct WrapperInfo {
	 vector<Func> FunctionList;
	 vector<Prop> PropertyList;
};

void SashObjectWrapper::CreateSashObject(nsIID objIID, nsISupports* in, JSContext* cx, JSObject** j)
{
	 vector<nsIID> ids;
	 DEBUGMSG(runtimetools, "Wrappering %s\n", objIID.ToString());
	 int jsret;

	 *j = JS_NewObject(cx, NULL, NULL, JS_GetGlobalObject(cx));
	 assert(*j != NULL);

	 WrapperInfo* w;
	 string id;
	 
	 // want to see what interfaces it supports
	 nsCOMPtr<nsIClassInfo> ci(do_QueryInterface(in));
	 if (ci == NULL) {
		 id = objIID.ToString(); // fall back on the given IID
	 } else {
		  nsCID* cid;
		  ci->GetClassID(&cid);
		  id = cid->ToString();
		  nsMemory::Free(cid);
	 }
	 if (WrapperTable.count(id) != 0) {
		  w = WrapperTable[id];
	 } else {
		  w = new WrapperInfo;
		  WrapperTable[id] = w;
  
		  if (ci == NULL) {
			   // no class info; use supplied iid
			   ids.push_back(objIID);
		  } else {
			   PRUint32 length;
			   nsIID** idary;
			   nsresult rv = ci->GetInterfaces(&length, &idary);
			   assert(NS_SUCCEEDED(rv));
			   for (PRUint32 i = 0 ; i < length ; i++) {
					ids.push_back(*idary[i]);
			   }
			   // clean up here?
		  }
		  vector<nsIID>::iterator aii = ids.begin(), bii = ids.end();
		  while (aii != bii) {
			   WrapObjectInterface(*j, *aii, w);
			   ++aii;
		  }
	 }

	 vector<Prop>::iterator ai = w->PropertyList.begin(), bi = w->PropertyList.end();
	 while (ai != bi) {
	   JSPropertyOp getter = SashObjectWrapper::ObjectGetProperty;
	   JSPropertyOp setter = (ai->has_setter ? 
							  SashObjectWrapper::ObjectSetProperty
							  : NULL);

	   jsret = JS_DefineProperty(cx, *j, ai->name.c_str(), JSVAL_NULL, getter, setter,
								 JSPROP_ENUMERATE | JSPROP_PERMANENT |
								 (ai->has_setter ? 0 : JSPROP_READONLY));
	   assert(jsret);
	   ++ai;
	 }

	 vector<Func>::iterator aif = w->FunctionList.begin(), bif = w->FunctionList.end();
	 // uniquify function list 
	 sort(aif, bif);
	 w->FunctionList.erase(unique(aif, bif), bif);
	 bif = w->FunctionList.end();
	 while (aif != bif) {
		  string s = "return this." + SashHandlerName + "('"+aif->name+"','" + id+"', arguments);\n";
		  JSFunction * jsfun;
		  jsfun = JS_CompileFunction(cx, *j, aif->name.c_str(), 0, NULL, s.c_str(), s.size(), "Sash-builtin", 1);
		  assert(jsfun != NULL);
		  ++aif;
	 }

	 JSFunction * newFun = JS_DefineFunction(cx, *j, SashHandlerName.c_str(),
											 SashObjectWrapper::SuperFunction, 3, 0);
	 assert(newFun);

	 JSFunction * toString = JS_DefineFunction(cx, *j, "toString", 
											   SashObjectWrapper::PrintObject, 0, 0);

	 assert(toString);

	 JSObject* obj = RuntimeTools::InterfaceToJSObject(cx, in, objIID);
	 assert(obj);

	 jsval modelobject = OBJECT_TO_JSVAL(obj);
	 jsret = JS_SetProperty(cx, *j, "ModelObject", &modelobject);
	 assert(jsret);
}

void SashObjectWrapper::WrapObjectInterface(JSObject* j, nsIID objIID, WrapperInfo* w) {

	 nsCOMPtr<nsIInterfaceInfo> iface;
	 nsCOMPtr<nsIInterfaceInfoManager> iim = dont_AddRef(XPTI_GetInterfaceInfoManager());
	 NS_ASSERTION(iim, "No InterfaceManager");

	 nsresult rv = iim->GetInfoForIID(&objIID, getter_AddRefs(iface));
	 if (!NS_SUCCEEDED(rv)) OutputError("Could not get info on selected IID %s", objIID.ToString());
	 // NOOP function to ensure interface is resolved from file 
	 if (iface == NULL) OutputError("Failed to get requested interface");

	 PRUint16 numMethods, numParams, numConstants;
	 rv = iface->GetMethodCount(&numMethods);
	 if (!NS_SUCCEEDED(rv)) OutputError( "Could not get number of methods");
	 rv = iface->GetConstantCount(&numConstants);
	 if (!NS_SUCCEEDED(rv)) OutputError("Could not get number of constants");

	 for (int i = 0; i < numConstants; i++) {
		  const nsXPTConstant *constInfo;
		  rv = iface->GetConstant(i, &constInfo);
		  if (!NS_SUCCEEDED(rv)) OutputError( "Could not get info on constant");
		  Prop p = {constInfo->GetName(), false};
		  w->PropertyList.push_back(p);
	 }
  
	 for (int i = 0; i < numMethods; i++) {
		  const nsXPTMethodInfo* methodInfo;
		  rv = iface->GetMethodInfo(i, &methodInfo);
		  NS_ASSERTION(NS_SUCCEEDED(rv), "Could not get info on method");
		  if (methodInfo->IsNotXPCOM() || methodInfo->IsHidden())
			continue;
		  numParams = methodInfo->GetParamCount();
		  //printf("handling method %s\n", methodInfo->GetName());

		  if (methodInfo->IsGetter() || methodInfo->IsSetter()) { // is a property
			//			   nsXPTParamInfo paramInfo = methodInfo->GetParam(0);
			   Prop p;
			   p.name = methodInfo->GetName();
			   p.has_setter = methodInfo->IsSetter();
			   bool found = false;
			   for (unsigned int r = 0 ; r < w->PropertyList.size() ; r++) {
					if (w->PropertyList[r].name == p.name) {
						 w->PropertyList[r].has_setter = w->PropertyList[r].has_setter || p.has_setter;
						 found = true;
						 DEBUGMSG(runtimetools, "Modifying property %s\n", p.name.c_str());
					}
			   }
			   if (! found) {
					w->PropertyList.push_back(p);
					DEBUGMSG(runtimetools, "Found property %s\n", p.name.c_str());
			   }
		  } else { // is a function
			   Func f(methodInfo->GetName());
			   DEBUGMSG(runtimetools, "Found method %s\n", f.name.c_str());
			   for (int j = 0; j < numParams; j++) {
				 //					nsXPTParamInfo paramInfo = methodInfo->GetParam(j);
					bool param_is_variant = false;

					if (methodInfo->GetParam(j).IsRetval()) {
						 f.has_retval = true;
					} else {
						 f.args.push_back(param_is_variant);
					}
			   }
			   w->FunctionList.push_back(f);
		  }
	 }
}

JSBool SashObjectWrapper::ObjectGetProperty(JSContext *cx, JSObject *obj, jsval id, jsval *vp) {
	 char *propName = JS_GetStringBytes(JSVAL_TO_STRING(id));
	 assert(propName);

	 DEBUGMSG(runtimetools, "Getting property %s\n", propName);

	 int jsret;
	 jsval realobj;
	 jsret = JS_GetProperty(cx, obj, "ModelObject", &realobj);
	 assert(jsret);
	 assert(JSVAL_IS_OBJECT(realobj));
	 
	 if (JS_GetProperty(cx, JSVAL_TO_OBJECT(realobj), propName, vp)) {
		  FixRetVal(cx, vp);
		  DEBUGMSG(runtimetools, "Got property %s with cx %p\n", propName, cx);
		  return true;
	 }

	 return false;
}

JSBool SashObjectWrapper::ObjectSetProperty(JSContext *cx, JSObject *obj, jsval id, jsval *vp) {
	 char *propName = JS_GetStringBytes(JSVAL_TO_STRING(id));
	 jsval realobj;
	 int jsret;
	 DEBUGMSG(runtimetools, "Setting NON-variant property %s\n", propName);
	 jsret = JS_GetProperty(cx, obj, "ModelObject", &realobj);
	 assert(jsret);
	 assert(JSVAL_IS_OBJECT(realobj));
	 if (JSVAL_IS_OBJECT(*vp) && ! JSVAL_IS_NULL(*vp)) {
		  jsval realvp;
		  DEBUGMSG(runtimetools, "new value is an object\n");
		  int jsret2 = JS_GetProperty(cx, JSVAL_TO_OBJECT(*vp), "ModelObject", &realvp);
		  if (jsret2 && JSVAL_IS_OBJECT(realvp)) {
			   DEBUGMSG(runtimetools, "new value appears to have a ModelObject -- using that value\n");
			   *vp = realvp;
		  }
	 }
	 
	 return JS_SetProperty(cx, JSVAL_TO_OBJECT(realobj), propName, vp);
}



nsISupports * SashObjectWrapper::GetNativeObject(JSContext * cx, JSObject * obj) {
	nsISupports * nativeObj;

	nsCOMPtr<nsIXPConnect> xpc = do_GetService(nsIXPConnect::GetCID()); 
	nsCOMPtr<nsIXPConnectWrappedNative> wrappedNative;
	nsresult rval;

	rval = xpc->GetWrappedNativeOfJSObject(cx, obj, getter_AddRefs(wrappedNative));
	if (!NS_SUCCEEDED(rval)) {
	  DEBUGMSG(runtimetools, "Unable to get native object wrapper\n");
	  return NULL;
	}
	
	rval = wrappedNative->GetNative(&nativeObj);
	if (!NS_SUCCEEDED(rval)) {
	  DEBUGMSG(runtimetools, "Unable to get native object\n");
	  return NULL;
	}

	return nativeObj;
}

JSBool SashObjectWrapper::PrintObject(JSContext* cx, JSObject* obj, uintN argc, 
								   jsval* argv, jsval* rval) 
{
  *rval = STRING_TO_JSVAL(JS_NewStringCopyZ(cx, "[Sash wrapped object]"));
  return true;
}

JSBool SashObjectWrapper::SuperFunction(JSContext* cx, JSObject* obj, uintN argc, 
								   jsval* argv, jsval* rval) 
{
  bool js_ret;
  char arg_buf[32];

	 assert(argc == 3);
	 const char *func_name = JS_GetStringBytes(JSVAL_TO_STRING(argv[0]));
	 const char *hashid = JS_GetStringBytes(JSVAL_TO_STRING(argv[1]));
	 assert(func_name);
	 assert(hashid);
	 assert(JSVAL_IS_OBJECT(argv[2]));

	 JSObject* arg_array = JSVAL_TO_OBJECT(argv[2]);
	 DEBUGMSG(runtimetools, "SUPERFUNCTION for %s\n", func_name);
	 assert(WrapperTable.count(hashid) > 0);
	 WrapperInfo* wrapper_info = WrapperTable[hashid];
	 vector<Func>::iterator pos = find(wrapper_info->FunctionList.begin(), 
									   wrapper_info->FunctionList.end(), func_name);
	 if (pos == wrapper_info->FunctionList.end()) 
	   OutputError("Internal error -- function '%s' not found in internal Sash function table!", func_name);
	 Func myfunc = *pos;
	 const unsigned int num_params = myfunc.args.size();
	 jsval args[num_params];
	 
	 jsval jsv_len;
	 js_ret = JS_GetProperty(cx, arg_array, "length", &jsv_len);
	 assert(js_ret == true);
	 jsuint length = JSVAL_TO_INT(jsv_len);
   	 DEBUGMSG(runtimetools, "%d arguments, expecting %d\n", length, num_params);

	 // for each supplied parameter...
	 for (jsuint i = 0 ; i < length && i < num_params ; i++) {
		  jsval j;
		  snprintf(arg_buf, sizeof(arg_buf), "%d", i);
		  js_ret = JS_GetProperty(cx, arg_array, arg_buf, &j);
		  assert(js_ret);
		  if (JSVAL_IS_OBJECT(j) && ! JSVAL_IS_NULL(j)) {
			jsval realobj;
			int jsret = JS_GetProperty(cx, JSVAL_TO_OBJECT(j), "ModelObject", &realobj);
			if (jsret && JSVAL_IS_OBJECT(realobj))
			  args[i] = realobj;
			else 
			  args[i] = j;
		  } else {
			args[i] = j;
		  }
		  
	 }

	 // fill in the parameters not supplied by the JS code
	 while (length < num_params) {
	   DEBUGMSG(runtimetools, "Can't add dummy arg %d -- not a variant! Adding JSVAL_NULL instead\n", length);
	   args[length++] = JSVAL_NULL;
	 }

	 jsval realobj;
	 js_ret = JS_GetProperty(cx, obj, "ModelObject", &realobj);
	 assert(js_ret);
	 assert(JSVAL_IS_OBJECT(realobj));
	 DEBUGMSG(runtimetools, "invoking function %s\n", myfunc.name.c_str());
	 js_ret = JS_CallFunctionName(cx, JSVAL_TO_OBJECT(realobj), myfunc.name.c_str(), num_params, args, rval);
	 if (js_ret) {
		  FixRetVal(cx, rval);
	 }
	 else {
		  *rval = JSVAL_NULL;
	 }

	 DEBUGMSG(runtimetools, "returning from superfunction for %s\n", func_name);
	 return js_ret;
}

void SashObjectWrapper::FixRetVal(JSContext * cx, jsval * val) {
}
