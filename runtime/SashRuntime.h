
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Ari Heitner, AJ Shankar, Tyeler Quentmeyer

Sash's main class -- runs sash, loads extensions and locations, etc.

*****************************************************************/

#ifndef _SASHRUNTIME_H
#define _SASHRUNTIME_H

#include "sashIRuntime.h"
#include "InstallationManager.h"

//E7E50DD0-7A8A-4930-89B5-7AB75742E486
#define SASHRUNTIME_CID {0xE7E50DD0, 0x7A8A, 0x4930, {0x89, 0xB5, 0x7A, 0xB7, 0x57, 0x42, 0xE4, 0x86}}
NS_DEFINE_CID(kSashRuntimeCID, SASHRUNTIME_CID);

#define SASHRUNTIME_CONTRACT_ID "@gnome.org/SashXB/SashRuntime;1"

class SashRuntime : public sashIRuntime
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIRUNTIME
		 
  SashRuntime();
  
 protected:
  InstallationManager * m_instMan;
  SashExtensionItem GetWeblication(const string & arg);
  vector<string> GetActions(const SashExtensionItem & weblication,
							const string & arg);
  void InitializeGNOME();
  void RunAction(const string & webGUID, const string & action,
				 PRUint32 argc, const char ** argv);
  void ForkActions(const string & webGUID, const vector<string> & actGUIDs,
				   PRUint32 argc, const char ** argv);

};

#endif
