
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Kevin Gibbs, John Corwin

*****************************************************************/

// Uncomment the following line to enable wrapping sash objects
//#define WRAP_SASH_OBJECTS

#include "RuntimeTools.h"
#include "debugmsg.h"

#include <string>
#include <vector>
#include <utility>

#include "gtkmozembed.h"
#include "gtkmozembed_internal.h"
#include "SashAppFileLocationProvider.h"
#include "nsAppDirectoryServiceDefs.h"
#include "nsISimpleEnumerator.h"
#include "nsIXPConnect.h"
#include "nsIPref.h"
#include "prenv.h"
#include "nsIDocShell.h"

#include "sashVariantUtils.h"
#include "sash_error.h"
#include "SashObjectWrapper.h"
#include "sashIGtkBrowser.h"

using namespace std;

bool RuntimeTools::s_SashNamespaceEnabled = false;
vector<sashIGtkBrowser *> RuntimeTools::s_browsers;

class SashDirectoryEnumerator:public nsISimpleEnumerator {
protected:
  vector < string > m_componentDirs;
  int m_index;

public:
  NS_DECL_ISUPPORTS SashDirectoryEnumerator(vector < string > &componentDirs) {
	   NS_INIT_ISUPPORTS();
	   m_componentDirs = componentDirs;
	   m_index = 0;
  } 
  
  virtual ~ SashDirectoryEnumerator() {}
  NS_IMETHODIMP HasMoreElements(PRBool * result) {
    *result = (m_index < (int) m_componentDirs.size());
    DEBUGMSG(runtimetools,
             "Has more elements: %s, m_index = %d, #components = %d\n",
             *result ? "TRUE" : "FALSE", m_index, m_componentDirs.size());

    return NS_OK;
  }

  NS_IMETHODIMP GetNext(nsISupports ** result) {
    DEBUGMSG(runtimetools, "SashDirectoryEnumerator::GetNext\n");
    if (m_index < (int) m_componentDirs.size()) {
      nsCOMPtr < nsILocalFile > localFile;
      DEBUGMSG(runtimetools, "Returning directory %s\n",
               m_componentDirs[m_index].c_str());
      nsAutoString filename;
      filename.AssignWithConversion(m_componentDirs[m_index].c_str());
      nsresult rv =
        NS_NewLocalFile(filename, PR_TRUE, getter_AddRefs(localFile));
      *result = localFile;
      NS_IF_ADDREF(*result);
      m_index++;
      return rv;
    }
    return NS_ERROR_FAILURE;
  }

};

NS_IMPL_ISUPPORTS1(SashDirectoryEnumerator, nsISimpleEnumerator)

class SashAppFileLocationProviderEx:public SashAppFileLocationProvider {
protected:
  vector < string > m_componentDirs;

public:
  SashAppFileLocationProviderEx(vector < string > &componentDirs) {
    m_componentDirs = componentDirs;
  } 
  
  NS_IMETHODIMP GetFiles(const char *prop, nsISimpleEnumerator ** retval) {
    DEBUGMSG(runtimetools,
             "SashAppFileLocationProviderEx::GetFiles prop = %s\n", prop);
    if (!nsCRT::strcmp(prop, NS_APP_PLUGINS_DIR_LIST)) {
      SashDirectoryEnumerator *sashDirs =
        new SashDirectoryEnumerator(m_componentDirs);
      *retval = sashDirs;
      NS_IF_ADDREF(*retval);
      return NS_OK;
    } else
      return NS_ERROR_FAILURE;
  }
};

int SashRegisterSashRuntime(vector < string > componentDirs)
{
  char *home = PR_GetEnv("SASH_HOME");
  if (!home)
    OutputError("Failed to get $SASH_HOME");
  gtk_moz_embed_set_profile_path(home, "runtime_profile");

  DEBUGMSG(runtimetools, "Starting XPCOM...\n");
  SashAppFileLocationProviderEx *appFileLocProvider =
    new SashAppFileLocationProviderEx(componentDirs);
  gtk_moz_embed_set_directory_service_provider(appFileLocProvider);
  DEBUGMSG(runtimetools, "Pushing startup\n");
  gtk_moz_embed_push_startup();

  RuntimeTools::RegisterDirectories(componentDirs);

  DEBUGMSG(runtimetools, "Registration completed\n");

  return 1;
}

void RuntimeTools::RegisterDirectories(vector < string > &componentDirs)
{
  nsILocalFile *bin_path;

  for (int i = 0; i < (int) componentDirs.size(); i++) {
    nsAutoString filename;
    filename.AssignWithConversion(componentDirs[i].c_str());
    NS_NewLocalFile(filename, PR_FALSE, &bin_path);
    DEBUGMSG(runtimetools, "Registering components in directory %s\n",
             componentDirs[i].c_str());
    nsComponentManager::AutoRegister(nsIComponentManagerObsolete::NS_Startup,
                                     bin_path);
  }
}

//NS_DEFINE_CID(kPrefServiceCID, NS_PREF_CID);

void RuntimeTools::SetSashProxy(const SashProxyInfo & info)
{
  nsCOMPtr < nsIPref > prefs(do_GetService(NS_PREF_CONTRACTID));
  if (!prefs) {
    DEBUGMSG(runtimetools, "Unable to get Mozilla prefs service!\n");
    return;
  }
//       prefs->ReadUserPrefs(NULL);
  DEBUGMSG(runtimetools, "Got Mozilla proxy service\n");
  prefs->SetIntPref("network.proxy.type", info.type);
  DEBUGMSG(runtimetools, "Set proxy type to %d\n", info.type);
  prefs->SetCharPref("network.proxy.http", info.host.c_str());
  DEBUGMSG(runtimetools, "Set proxy host to %s\n", info.host.c_str());
  prefs->SetIntPref("network.proxy.http_port", info.port);
  DEBUGMSG(runtimetools, "Set proxy port to %d\n", info.port);
}

sashIActionRuntime *g_SashGlobalActionRuntime = NULL;

sashIActionRuntime *RuntimeTools::GetSashGlobalNameSpaceActionRuntime()
{
  return g_SashGlobalActionRuntime;
}

void RuntimeTools::SetSashGlobalNameSpaceActionRuntime(sashIActionRuntime *
                                                       rt)
{
  g_SashGlobalActionRuntime = rt;
}

void GetVariantArray(nsIVariant * v, nsIVariant *** items, PRUint32 * count)
{
  PRUint32 length, i;
  PRUint16 type;
  nsIID iid;
  void *data;

  nsIVariant *vItem;
  PRInt32 *ints;
  double *doubles;
  char **strings;
  PRUnichar **wstrings;
  nsISupports **ifaces;
  nsIVariant *vIface;
  *items = NULL;
  *count = 0;

  if (v) {
    if (VariantGetType(v) == nsIDataType::VTYPE_EMPTY_ARRAY)
      return;
    if (NS_SUCCEEDED(v->GetAsArray(&type, &iid, &length, &data)) &&
        length > 0) 
    {
      *count = length;
      *items = new(nsIVariant *)[length];
      switch (type) {
      case nsIDataType::VTYPE_BOOL: {
          PRBool *bools = (PRBool *) data;
          for (i = 0; i < length; i++) {
            NewVariant(&vItem);
            VariantSetBoolean(vItem, bools[i]);
            (*items)[i] = vItem;
          }
          break;
      }
      case nsIDataType::VTYPE_INT32:
        ints = (PRInt32 *) data;
        for (i = 0; i < length; i++) {
          NewVariant(&vItem);
          VariantSetInteger(vItem, ints[i]);
          (*items)[i] = vItem;
        }
        break;
      case nsIDataType::VTYPE_DOUBLE:
        doubles = (double *) data;
        for (i = 0; i < length; i++) {
          NewVariant(&vItem);
          VariantSetNumber(vItem, doubles[i]);
          (*items)[i] = vItem;
        }
        break;
      case nsIDataType::VTYPE_CHAR_STR:
        strings = (char **) data;
        for (i = 0; i < length; i++) {
          NewVariant(&vItem);
          VariantSetString(vItem, strings[i]);
          (*items)[i] = vItem;
        }
        break;
      case nsIDataType::VTYPE_WCHAR_STR:
        wstrings = (PRUnichar **) data;
        for (i = 0; i < length; i++) {
          NewVariant(&vItem);
          nsCOMPtr < nsIWritableVariant > wv = do_QueryInterface(vItem);
          wv->SetAsWString(wstrings[i]);
          (*items)[i] = vItem;
        }
        break;
      case nsIDataType::VTYPE_INTERFACE:
      case nsIDataType::VTYPE_INTERFACE_IS:
        ifaces = (nsISupports **) data;
        for (i = 0; i < length; i++) {
          if (NS_SUCCEEDED(ifaces[i]->QueryInterface(NS_GET_IID(nsIVariant),
                                                     (void **) &vIface))) 
          {
            (*items)[i] = vIface;
          } else {
            NewVariant(&vItem);
            VariantSetInterface(vItem, ifaces[i]);
            (*items)[i] = vItem;
          }
        }
        break;
      default:
        printf("Array type %d unexpected.\n", type);
        delete[]items;
        *items = NULL;
      }
      nsMemory::Free(data);
    }
  }
}

void RuntimeTools::WrapSashObject(nsIID objIID, nsISupports * in,
                                  JSContext * cx, JSObject ** j)
{
#ifdef WRAP_SASH_OBJECTS
  SashObjectWrapper::CreateSashObject(objIID, in, cx, j);
#else
  *j = InterfaceToJSObject(cx, in, objIID);
#endif
}

JSObject *RuntimeTools::InterfaceToJSObject(JSContext * cx, nsISupports * s,
                                            nsIID objIID)
{
  JSObject *ret_obj;
  nsCOMPtr < nsIXPConnect > xpc = do_GetService(nsIXPConnect::GetCID());
  nsresult ret;

  DEBUGMSG(runtimetools, "InterfaceToJSObject: Wrappering object\n");
  nsIXPConnectJSObjectHolder *wrapper;
  ret = xpc->WrapNative(cx, JS_GetGlobalObject(cx), s, objIID, &wrapper);
  assert(NS_SUCCEEDED(ret));

  wrapper->GetJSObject(&ret_obj);
  NS_IF_RELEASE(wrapper);
  DEBUGMSG(runtimetools, "InterfaceToJSObject: returning JSObject\n");
  return ret_obj;
}

nsIVariant *RuntimeTools::CreateVariantFromJSVal(JSContext * cx, jsval val)
{
  nsIVariant *v;
  NewVariant(&v);

  jsdouble num;
  JSObject *jsobj;

  if (JSVAL_IS_NULL(val))
    VariantSetEmpty(v);
  else if (JSVAL_IS_BOOLEAN(val))
    VariantSetBoolean(v, JSVAL_TO_BOOLEAN(val));
  else if (JSVAL_IS_DOUBLE(val) || JSVAL_IS_INT(val)) {
    JS_ValueToNumber(cx, val, &num);
    VariantSetNumber(v, num);
  } else if (JSVAL_IS_STRING(val)) {
    VariantSetString(v, JS_GetStringBytes(JSVAL_TO_STRING(val)));
  } else if (JSVAL_IS_OBJECT(val)) {
    jsobj = JSVAL_TO_OBJECT(val);
    if (JS_IsArrayObject(cx, jsobj)) {
      // -- Array object --
      vector < nsIVariant * >items;
      jsuint arrayLen;
      jsval arrayVal;

      JS_GetArrayLength(cx, jsobj, &arrayLen);
      for (jsuint i = 0; i < arrayLen; i++) {
        JS_GetElement(cx, jsobj, i, &arrayVal);
        items.push_back(CreateVariantFromJSVal(cx, arrayVal));
      }
      VariantSetArray(v, items);
    } else {
      // -- non-array object --
      JSObject *ModelObject;
      nsIXPConnectWrappedNative *wrapper;
      nsISupports *iface;

#ifdef WRAP_SASH_OBJECTS
      // -- look for a ModelObject --
      jsval ModelObjVal;
      int jsret = JS_GetProperty(cx, jsobj, "ModelObject", &ModelObjVal);
      if (jsret && JSVAL_IS_OBJECT(ModelObjVal))
        ModelObject = JSVAL_TO_OBJECT(ModelObjVal);
      else
#endif
        ModelObject = jsobj;

      // -- get the native object pointer --
      nsCOMPtr < nsIXPConnect > xpc = do_GetService(nsIXPConnect::GetCID());
      xpc->GetWrappedNativeOfJSObject(cx, ModelObject, &wrapper);
      if (wrapper) {
        wrapper->GetNative(&iface);
        VariantSetInterface(v, iface);
      } else {
        printf("WARNING:  RuntimeTools::CreateVariantFromJSVal: "
               "unable to get the native XPCOM interface "
               "from the JS object!\n");
        VariantSetEmpty(v);
      }
    }
  }

  return v;
}

jsval RuntimeTools::VariantGetJSVal(JSContext * cx, nsIVariant * v)
{
  jsval val = JSVAL_NULL;

  if (VariantIsBoolean(v)) {
    val = BOOLEAN_TO_JSVAL(VariantGetBoolean(v));
  } else if (VariantIsNumber(v)) {
    JS_NewNumberValue(cx, VariantGetDouble(v), &val);
  } else if (VariantIsString(v)) {
    JSString *str = JS_NewStringCopyZ(cx, VariantGetString(v).c_str());
    val = STRING_TO_JSVAL(str);
  } else if (VariantIsInterface(v)) {
    nsIID iid;
    nsCOMPtr < nsISupports > iface = VariantGetInterface(v);
    if (iface) {
      iid = NS_GET_IID(nsISupports);

      JSObject *jsobj;
      RuntimeTools::WrapSashObject(iid, iface, cx, &jsobj);
      val = OBJECT_TO_JSVAL(jsobj);
    }
  } else if (VariantIsArray(v)) {
    vector < nsIVariant * >items;
    VariantGetArray(v, items);
    JSObject *array = JS_NewArrayObject(cx, items.size(), nsnull);
    if (array) {
      val = OBJECT_TO_JSVAL(array);
      bool OK = true;
      for (unsigned int i = 0; (i < items.size()) && OK; i++) {
        jsval current = VariantGetJSVal(cx, items[i]);
        OK = JS_SetElement(cx, array, i, &current);
      }
    }
  }

  return val;
}


jsval *RuntimeTools::MarshalSashVariantArray(JSContext * cx, PRUint32 argc,
                                             nsIVariant ** argv)
{
  jsval *jsArgs = NULL;

  if (argc > 0) {
    jsArgs = new jsval[argc];
    for (unsigned int i = 0; i < argc; i++) {
      jsArgs[i] = VariantGetJSVal(cx, argv[i]);
    }
  }

  return jsArgs;
}

void RuntimeTools::CallEventName(JSContext * cx, const string & eventName,
                                 PRUint32 argc, nsIVariant ** argv,
                                 nsIVariant ** result)
{
  jsval ret;
  jsval *jsArgs = MarshalSashVariantArray(cx, argc, argv);

  DEBUGMSG(extensiontools, "Calling JS function %s with %d args\n",
           eventName.c_str(), argc);

  if (!JS_CallFunctionName
      (cx, JS_GetGlobalObject(cx), eventName.c_str(), argc, jsArgs, &ret))
    ret = JSVAL_NULL;

  DEBUGMSG(extensiontools, "returning from JS function\n");
  if (jsArgs)
    delete[]jsArgs;
  if (result)
    *result = CreateVariantFromJSVal(cx, ret);
}

void RuntimeTools::CallEvent(const string & event, JSContext * cx,
                             PRUint32 argc, nsIVariant ** argv,
                             nsIVariant ** result)
{
  static const char funcHeader[] = "\nfunction ";
  int headerSize = strlen(funcHeader);
  string eventName;
  DEBUGMSG(extensiontools, "Event is %s, context is %p\n", event.c_str(), cx);
  if (strncmp(event.c_str(), funcHeader, headerSize) == 0) {
    int paren = event.find('(');
    eventName = event.substr(headerSize, paren - headerSize);
    DEBUGMSG(extensiontools, "Calling function %s\n", eventName.c_str());
    if (eventName == "anonymous") {
      string script = "_SashJSEvent = " + event;
      jsval ret;
      if (!JS_EvaluateScript(cx, JS_GetGlobalObject(cx), script.c_str(),
                             script.size(), "sash event handler", 0, &ret)) {
        DEBUGMSG(extensiontools,
                 "Sash Event Handler: error calling anonymous function\n");
        return;
      }
      eventName = "_SashJSEvent";
    }
  } else if (event.find('(') != string::npos) {
    jsval ret;
    if (!JS_EvaluateScript
        (cx, JS_GetGlobalObject(cx), event.c_str(), event.length(),
         "sash event handler", 0, &ret)) {
      DEBUGMSG(extensiontools,
               "Sash Event Handler: error evaling code block\n");
    } else {
   	if (result)
	      *result = CreateVariantFromJSVal(cx, ret);
    }
    return;
  } else {
    eventName = event;
  }

  CallEventName(cx, eventName, argc, argv, result);
}

void RuntimeTools::RegisterSashGtkBrowser(sashIGtkBrowser * browser) {
	 DEBUGMSG(nameset, "registering browser %p\n", browser);
	 s_browsers.push_back(browser);
}

void RuntimeTools::UnregisterSashGtkBrowser(sashIGtkBrowser * browser) {
	 vector<sashIGtkBrowser *>::iterator a = s_browsers.begin(), b = s_browsers.end();

	 while (a != b) {
		  if ((*a) == browser) {
			   s_browsers.erase(a);
			   a = s_browsers.begin();
			   b = s_browsers.end();
		  }
		  ++a;
	 }
}

sashIGtkBrowser * RuntimeTools::FindSashGtkBrowser(nsIDocShell * docShell) {
	 vector<sashIGtkBrowser *>::iterator a = s_browsers.begin(), b = s_browsers.end();
	 while (a != b) { 
		  
		  nsIDocShell * browserDocShell;
		  (*a)->GetDocShell(&browserDocShell);
		  DEBUGMSG(nameset, "checking browser %p, browser doc shell is %p\n", *a, browserDocShell);
		  if (browserDocShell == docShell) {
			   NS_RELEASE(browserDocShell);
			   return *a;
		  } 
		  else {
			   NS_RELEASE(browserDocShell);
		  }
		  ++a;
	 }
	 return NULL;
}
