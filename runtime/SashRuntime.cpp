
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Ari Heitner, AJ Shankar, Tyeler Quentmeyer

*****************************************************************/

#include <string>
#include <vector>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "sash_constants.h"
#include "SashRuntime.h"
#include "ActionRuntime.h"
#include "SashGUID.h"
extern "C" {
#include <libgnorba/gnorba.h>
}

NS_IMPL_ISUPPORTS1(SashRuntime, sashIRuntime)

SashRuntime::SashRuntime()
{
	 NS_INIT_ISUPPORTS();
	 m_instMan = new InstallationManager();
}

SashExtensionItem SashRuntime::GetWeblication(const string & arg) {
	 SashExtensionItem weblication;
	 if (VerifyGUID(arg)) {
		  // first argument is a guid
		  weblication = m_instMan->GetInfo(arg);
	 }
	 else {
		  WDF w;
		  if (w.OpenAmbiguousFile(arg)) {
			   // first argument is a WDF file
			   weblication = m_instMan->GetInfo(w.GetID());
		  }
		  else {
			   // try doing it by name 
			   // try doing it by name 
			   vector<SashExtensionItem> inst;
			   FindComponentByName(*m_instMan, arg, inst, SET_WEBLICATION);
			   if (inst.size() == 0) {
					OutputError("Cannot identify '%s' " 
								"as a filename, guid, or component name!", arg.c_str());
			   } else if (inst.size() > 1) {
					string options;
					for (unsigned int i = 0 ; i < inst.size() ; i++) 
						 options += "\t" + inst[i].name + "\n";
					OutputError("%d possibilities for '%s':\n%sPlease be more specific!",
								inst.size(), arg.c_str(), options.c_str());
			   }
			   weblication = inst[0];
		  }
	 }
	 if (weblication.ext_type != SET_WEBLICATION) {
		  OutputError("%s is not a SashXB weblication\n", arg.c_str());
	 }
	 return weblication;
}

bool nocase_prefix_cmp(const string& a, const string& b) {
	 return (strncasecmp(a.c_str(), b.c_str(), a.length()) == 0);
}

vector<string> SashRuntime::GetActions(const SashExtensionItem & weblication, 
									   const string & arg) 
{
	 vector<string> actions;
	 if (VerifyGUID(arg)) {
		  // argument is a GUID
		  for (unsigned int i = 0; i < weblication.actions.size(); i++) {
			   if (strcasecmp(arg.c_str(), weblication.actions[i].id.c_str()) == 0)
					actions.push_back(weblication.actions[i].id);
		  }
	 }
	 else {
		  // argument is an action name
		  for (unsigned int i = 0; i < weblication.actions.size(); i++) {
			   if (nocase_prefix_cmp(arg, weblication.actions[i].name))
					actions.push_back(weblication.actions[i].id);
		  }
	 }
	 if (actions.size() == 0) {
		  string names;
		  for (unsigned int i = 0; i < weblication.actions.size(); i++) {
			   names += weblication.actions[i].name + "\t\t" + 
					weblication.actions[i].id + "\n";
		  }
		  OutputError("Action %s not found in weblication %s.\nCandidates are\n%s",
					  arg.c_str(), weblication.name.c_str(), names.c_str());
	 }
	 return actions;
}

void SashRuntime::InitializeGNOME() {
	 CORBA_Environment ev;
	 string actionGuid;
	 int argc = 1;
	 char * argv[] = {"sash-runtime-bin", NULL};

	 gnome_CORBA_init("Sash", SASH_VERSION, &argc, argv, (GnorbaInitFlags) 0, &ev);
	 SashErrorPushMode(SASH_ERROR_GNOME);
}

void SashRuntime::RunAction(const string & webGUID, const string & action,
							PRUint32 argc, const char ** argv) 
{
	 InitializeGNOME();
	 ActionRuntime * actRun = new ActionRuntime;
	 nsresult res = actRun->Initialize(this, webGUID.c_str(), action.c_str(), 
						argc, argv);
	 if (! NS_SUCCEEDED(res))
		  OutputError("Can't create action!");
	 actRun->Run();
}

void SashRuntime::ForkActions(const string & webGUID, const vector<string> & actGUIDs,
							  PRUint32 argc, const char ** argv) {
	 vector<int> pids;
	 for (unsigned int i = 0; i < actGUIDs.size(); i++) {
		  int pid = fork();
		  if (pid == 0) {
			   // child -- run action
			   RunAction(webGUID, actGUIDs[i], argc, argv);
		  }
		  else {
			   pids.push_back(pid);
		  }
	 }
	 for (unsigned int i = 0; i < pids.size(); i++) {
		  int status;
		  // wait for the children to terminate
		  waitpid(pids[i], &status, 0);
	 }
}

NS_IMETHODIMP SashRuntime::Run(const char * web, const char * act,
							   PRUint32 argc, const char ** argv) 
{
	 // Formats: 
	 // {weblication-guid} {action-guid}
	 // {weblication-guid}
	 // weblication-name action-name
	 // weblication-name {action-guid}
	 // weblication-name
	 // weblication.wdf action-name
	 // weblication.wdf {action-guid}
	 // weblication.wdf

	 SashExtensionItem weblication;
	 vector<string> actions;

	 weblication = GetWeblication(web);
	 if (strcmp(act, "") != 0)
		  actions = GetActions(weblication, act);
	 else {
		  for (unsigned int i = 0; i < weblication.actions.size(); i++)
			   actions.push_back(weblication.actions[i].id);
	 }

	 if (actions.size() > 1)
		  ForkActions(weblication.guid, actions, argc, argv);
	 else if (actions.size() == 1)
		  RunAction(weblication.guid, actions[0], argc, argv);
	 else
		  OutputError("Weblication %s has no actions!\n", weblication.name.c_str());
	 
	 return NS_OK;
}
