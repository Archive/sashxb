
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Ari Heitner

contains the main() function for the sash runtime

*****************************************************************/

#include "SashRuntime.h"
#include <glib.h>
#include <gnome.h>

#define EXECUTABLE_NAME "sash-runtime"

void usage()
{
	 g_print("SashXB Runtime Usage:\n%s weblication [action] [--args arg1 arg2 ...]\n",
			 EXECUTABLE_NAME);
}

int main(int argc, char * argv[])
{
	 string weblication, action;
	 vector<string> args;
	 bool PassArgs = false;
	 const char ** cargs = NULL;

	 for (int arg = 1; arg < argc; arg++) {
		  if (PassArgs)
			   args.push_back(argv[arg]);
		  else if (strcmp(argv[arg], "--args") == 0)
			   PassArgs = true;
		  else if (arg == 1)
			   weblication = argv[arg];
		  else if (arg == 2)
			   action = argv[arg];
		  else {
			   usage();
			   return 1;
		  }
	 }

	 if (args.size() > 0) {
		  cargs = new (const char*)[args.size()];
		  for (unsigned int i = 0; i < args.size(); i++)
			   cargs[i] = args[i].c_str();
	 }

	 if (weblication != "") {
		  sashIRuntime *rt = new SashRuntime;
		  if (NS_SUCCEEDED(rt->Run(weblication.c_str(), action.c_str(),
									args.size(), cargs)))
			   return 0;
	 }

	 usage();
	 return 1;
}
