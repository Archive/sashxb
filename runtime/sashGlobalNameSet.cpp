
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

// Necessary to associate context and object, rather than just object.
// This may not be necessary.
//#define JS_THREADSAFE

#include "nsCOMPtr.h"
#include "nsIModule.h"
#include "nsIGenericFactory.h"
#include "nsIPrincipal.h"
#include "nsCodebasePrincipal.h"
#include "nsIScriptNameSpaceManager.h"
#include "nsIScriptExternalNameSet.h"
#include "nsIScriptContext.h"
#include <nsIXPConnect.h>
#include "nsICategoryManager.h"
#include "nsIServiceManager.h"
#include "nsXPIDLString.h"
#include "nsCOMPtr.h"
#include "nsIScriptContextOwner.h"
#include "nsIScriptGlobalObject.h"
#include "nsIDocShell.h"
#include "sashIGtkBrowser.h"

#include "ActionRuntime.h"
#include "RuntimeTools.h"
#include "sashGlobalNameSet.h"

#include "jsapi.h"
#include "sashGlobalNameSetSecurity.h"

#include "debugmsg.h"

///////////////////////
// sashGlobalNameSet //
///////////////////////

static FILE *gOutFile = stdout;
static FILE *gErrFile = stderr;
JS_STATIC_DLL_CALLBACK(JSBool) SashPrint(JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval);
JS_STATIC_DLL_CALLBACK(JSBool) Load(JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval);
JS_STATIC_DLL_CALLBACK(void) defaultErrorReporter(JSContext *cx, const char *message, JSErrorReport *report);

static JSFunctionSpec glob_functions[] = {
    {"printsys",           SashPrint,          0},
    {"load",            Load,           1},
    {0}
};

class sashGlobalNameSet : public nsIScriptExternalNameSet 
{
public:
    sashGlobalNameSet();
    virtual ~sashGlobalNameSet();
    
    NS_DECL_ISUPPORTS

    NS_IMETHOD InitializeNameSet(nsIScriptContext* aScriptContext);

private:
    sashGlobalNameSetSecurity *m_secMan;
};

sashGlobalNameSet::sashGlobalNameSet(): m_secMan(NULL)
{
    NS_INIT_REFCNT();
}

sashGlobalNameSet::~sashGlobalNameSet()
{
}

NS_IMPL_ISUPPORTS1(sashGlobalNameSet, nsIScriptExternalNameSet);

char *
getStringArgument(JSContext *cx, JSObject *obj, PRUint16 argNum, uintN argc, jsval *argv)
{
    if (argc <= argNum || !JSVAL_IS_STRING(argv[argNum])) {
        JS_ReportError(cx, "String argument expected");
        return nsnull;
    }
    /*
     * We don't want to use JS_ValueToString because we want to be able
     * to have an object to represent a target in subsequent versions.
     */
    JSString *str = JSVAL_TO_STRING(argv[argNum]);
    if (!str)
        return nsnull;

    return JS_GetStringBytes(str);
}


NS_IMETHODIMP 
 sashGlobalNameSet::InitializeNameSet(nsIScriptContext* aScriptContext)
{
	 DEBUGMSG(nameset, "InitializeNameSet: scriptContext = %p\n", aScriptContext);

	 nsCOMPtr<nsIScriptContextOwner> scOwner;
	 nsCOMPtr<nsIScriptGlobalObject> scriptGlobalObject;
	 aScriptContext->GetGlobalObject(getter_AddRefs(scriptGlobalObject));
	 if (scriptGlobalObject) {
		  DEBUGMSG(nameset, "Got script global object\n");
		  nsCOMPtr<nsIDocShell> docShell;
		  scriptGlobalObject->GetDocShell(getter_AddRefs(docShell));
		  if (docShell) {
			   DEBUGMSG(nameset, "InitializeNameSet: got doc shell\n");
			   sashIGtkBrowser * browser = RuntimeTools::FindSashGtkBrowser(docShell);
			   if (browser) {
					PRBool enabled;
					browser->GetEnableSashContext(&enabled);
					if (!enabled) {
						 DEBUGMSG(nameset, "NOT enabling the sash context in this browser\n");
						 return NS_OK;
					}
					else {
						 DEBUGMSG(nameset, "enabling the sash context in this browser\n");
					}
			   }
			   else {
					DEBUGMSG(nameset, "InitializeNameSet: warning: Browser for doc shell not found!\n");
			   }
		  }
	 }

    JSContext *cx = (JSContext *) aScriptContext->GetNativeContext();
    JSObject *global = JS_GetGlobalObject(cx);

	DEBUGMSG(runtime, "Initializing name set for context %p\n", cx);

    nsresult rv = NS_ERROR_FAILURE;

    sashIActionRuntime *actRuntime = RuntimeTools::GetSashGlobalNameSpaceActionRuntime();

    //setup error reporting
    if (!JS_SetErrorReporter(cx, defaultErrorReporter)) {
      fprintf(stderr, "failed to set error reporter!\n");
    }

    if (!JS_DefineFunctions(cx, global, glob_functions)) {
      fprintf(stderr, "failed to define global functions!\n");
    }

    
	//get xpconnect
    nsCOMPtr<nsIXPConnect> xpc = do_GetService(nsIXPConnect::GetCID(), &rv);
	if(!xpc)
		return 0; //DIE("FAILED to get xpconnect service\n");

	m_secMan = new sashGlobalNameSetSecurity();
	if(!m_secMan) return 0;
	if(!NS_SUCCEEDED(xpc->SetSecurityManagerForJSContext(cx, m_secMan, nsIXPCSecurityManager::HOOK_ALL)))
		return 0;

    if (actRuntime != NULL) {
      DEBUGMSG(runtime, "Loading Sash global name set\n");
      rv = actRuntime->CreateSashObject(aScriptContext);
      if (!NS_SUCCEEDED(rv)) 
	fprintf(stderr, "Could not load Sash global name set\n");
      else 
	DEBUGMSG(runtime, "Loading of Sash global name set succeeded.\n");
    }
    /*
      else {
        printf("Sash global name set runtime does not exist!\n");
    }
    */

    return rv;

    // What follows is the better and canonically correct way to create
    // the Sash object. Code should be switched to use this method.

    /*

    // *
    // * Find Object.prototype's class by walking up the global object's
    // * prototype chain.
    // *
    JSObject *obj = global;
    JSObject *proto;
    while ((proto = JS_GetPrototype(cx, obj)) != nsnull)
        obj = proto;
    JSClass *objectClass = JS_GetClass(cx, obj);

    jsval v;
    if (!JS_GetProperty(cx, global, SASH_NAME, &v))
        return NS_ERROR_FAILURE;

    JSObject *sashObj;
    if (JSVAL_IS_OBJECT(v)) {
        // *
        // * "Sash" object already exists, so don't recreate it.
        // *
        sashObj = JSVAL_TO_OBJECT(v);
    } else {
        // * define "Sash" object
        sashObj = JS_DefineObject(cx, global, SASH_NAME, objectClass, nsnull, JSPROP_ENUMERATE);
        if (sashObj == nsnull)
            return NS_ERROR_FAILURE;
    }

    return NS_OK;
    */
}

JS_STATIC_DLL_CALLBACK(JSBool) SashPrint(JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval)
{
    uintN i, n;
    JSString *str;

    for (i = n = 0; i < argc; i++) {
        str = JS_ValueToString(cx, argv[i]);
        if (!str)
            return JS_FALSE;
        fprintf(gOutFile, "%s%s", i ? " " : "", JS_GetStringBytes(str));
    }
    n++;
    if (n)
        fputc('\n', gOutFile);
    return JS_TRUE;
}

JS_STATIC_DLL_CALLBACK(JSBool) Load(JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval)
{
    uintN i;
    JSString *str;
    const char *filename;
    JSScript *script;
    JSBool ok;
    jsval result;

    for (i = 0; i < argc; i++) {
        str = JS_ValueToString(cx, argv[i]);
        if (!str)
            return JS_FALSE;
        argv[i] = STRING_TO_JSVAL(str);
        filename = JS_GetStringBytes(str);
        script = JS_CompileFile(cx, obj, filename);
        if (!script)
            ok = JS_FALSE;
        else {
            ok = JS_ExecuteScript(cx, obj, script, &result);
            JS_DestroyScript(cx, script);
        }
        if (!ok)
            return JS_FALSE;
    }
    return JS_TRUE;
}

JS_STATIC_DLL_CALLBACK(void) defaultErrorReporter(JSContext *cx, const char *message, JSErrorReport *report)
{
    fprintf(gErrFile, message);
}

NS_GENERIC_FACTORY_CONSTRUCTOR(sashGlobalNameSet)


static NS_METHOD 
RegisterSashGlobalNameSet(nsIComponentManager *aCompMgr,
                        nsIFile *aPath,
                        const char *registryLocation,
                        const char *componentType,
                        const nsModuleComponentInfo *info)
{
  nsresult rv = NS_OK;

  nsCOMPtr<nsICategoryManager> catman =
    do_GetService(NS_CATEGORYMANAGER_CONTRACTID, &rv);

  if (NS_FAILED(rv))
    return rv;

  nsXPIDLCString previous;
  rv = catman->AddCategoryEntry(JAVASCRIPT_GLOBAL_STATIC_NAMESET_CATEGORY,
                                "SashGlobal",
                                NS_SASHGLOBALNAMESET_CONTRACTID,
                                PR_TRUE, PR_TRUE, getter_Copies(previous));
  NS_ENSURE_SUCCESS(rv, rv);

  return rv;
}

static nsModuleComponentInfo components[] =
{
    { "Sash Global Script Name Set",
      NS_SASHGLOBALNAMESET_CID,
      NS_SASHGLOBALNAMESET_CONTRACTID,
      sashGlobalNameSetConstructor,
      RegisterSashGlobalNameSet
    }
};


NS_IMPL_NSGETMODULE(nsSashGlobalNameSetModule, components);


