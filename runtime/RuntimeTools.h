
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Kevin Gibbs

*****************************************************************/

#ifndef _RUNTIMETOOLS_H
#define _RUNTIMETOOLS_H

#include <string>
#include <vector>
#include <nsIID.h>
#include <nsIDirectoryService.h>
#include "nsIVariant.h"
#include <jsapi.h>

class sashIActionRuntime;
class sashIGtkBrowser;
class nsIDocShell;

enum SashProxyType {
  SASH_PROXY_NONE = 0,
  SASH_PROXY_HTTP = 1,
  SASH_PROXY_AUTO = 2
};

struct SashProxyInfo {
  SashProxyType type;
  string host;
  unsigned int port;
};

class RuntimeTools {
protected:
  static bool s_SashNamespaceEnabled;

  static vector<sashIGtkBrowser *> s_browsers;

  static void CallEventName(JSContext * cx, const string & eventName,
                            PRUint32 argc, nsIVariant ** argv,
                            nsIVariant ** result);
  static jsval *MarshalSashVariantArray(JSContext * cx, PRUint32 argc,
                                        nsIVariant ** argv);

public:
  // ---- Registration ----
  static void RegisterDirectories(vector < string > &componentDirs);
  static void SetSashProxy(const SashProxyInfo & info);

  static sashIActionRuntime *GetSashGlobalNameSpaceActionRuntime();
  static void SetSashGlobalNameSpaceActionRuntime(sashIActionRuntime * rt);

  // ---- Object Wrappering ----
  static void WrapSashObject(nsIID objIID, nsISupports * in, JSContext * cx,
                             JSObject ** j);
  static nsIVariant *CreateVariantFromJSVal(JSContext * cx, jsval val);
  static JSObject *InterfaceToJSObject(JSContext * cx, nsISupports * s,
                                       nsIID objIID);
  static jsval VariantGetJSVal(JSContext * cx, nsIVariant * v);

  // ---- Event Handling ----
  static void CallEvent(const string & event, JSContext * cx,
                        PRUint32 argc, nsIVariant ** argv,
                        nsIVariant ** result);

  static void RegisterSashGtkBrowser(sashIGtkBrowser * browser);
  static void UnregisterSashGtkBrowser(sashIGtkBrowser * browser);
  static sashIGtkBrowser * FindSashGtkBrowser(nsIDocShell * docShell);
};

#endif /* _RUNTIMETOOLS_H */
