
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _SASHGLOBALNAMESET_H
#define _SASHGLOBALNAMESET_H

#include "nsID.h"

//71C26526-B571-4BDA-825B-4727F7EB305D
#define NS_SASHGLOBALNAMESET_CID \
 { 0x71C26526, 0xB571, 0x4BDA, \
 { 0x82, 0x5B, 0x47, 0x27, 0xF7, 0xEB, 0x30, 0x5D } }
#define NS_SASHGLOBALNAMESET_CONTRACTID "@gnome.org/SashXB/sashGlobalNameSet;1"

NS_DEFINE_CID(ksashGlobalNameSetCID, NS_SASHGLOBALNAMESET_CID);


class ActionRuntime;

extern ActionRuntime *gSashGlobalNameSetActionRuntime;

#endif /* _SASHGLOBALNAMESET_H */
