
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Ari Heitner, AJ Shankar, Tyeler Quentmeyer, Stefan Atev


*****************************************************************/

#ifndef _ACTIONRUNTIME_H
#define _ACTIONRUNTIME_H

#include <glib.h>
#include <gnome.h>
#include <gmodule.h>
#include <string>
#include <vector>
#include <deque>
#include <set>

#include "nsID.h"
#include "nsIFactory.h"
#include "nsIScriptContextOwner.h"
#include "jsapi.h"

#include "sashIActionRuntime.h"
#include "sashIExtension.h"
#include "sashILocation.h"
#include "sashIRuntime.h"
#include "InstallationManager.h"
#include "plhash.h"

class PRMonitor;

//1239E8DD-95C7-4F7D-893D-D9C64BD85D35
#define ACTIONRUNTIME_CID {0x1239E8DD, 0x95C7, 0x4F7D, {0x89, 0x3D, 0xD9, 0xC6, 0x4B, 0xD8, 0x5D, 0x35}}
NS_DEFINE_CID(kActionRuntimeCID, ACTIONRUNTIME_CID);

#define ACTIONRUNTIME_CONTRACT_ID "@gnome.org/SashXB/ActionRuntime;1"

#define SASH_NAME "Sash"

class ActionRuntime : public sashIActionRuntime
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIACTIONRUNTIME

  ActionRuntime();
  virtual ~ActionRuntime();

  /**
	 Return the next extension with a pending event, blocking if necessary.
	 If quit has been signaled, this returns NULL (and never otherwise).
  */
  nsISupports * GetNextPendingExtension();
  void EventHandler();

protected:

  std::vector<string> all_extension_guids;
  std::vector<string> namespace_extension_guids;

	//! Extensions which are waiting for event processing
	std::set<nsISupports *> m_pendingExtensions;

	//! The monitor to make m_pendingExtensions safe
	PRMonitor *m_mon;

	//! The event loop continues as long as this is true
	bool m_continueEvents;

  nsCOMPtr<nsISupports> m_location;
  InstallationManager *m_instMan;
  string m_actGuid, m_webGuid;
  nsCOMPtr<sashIRuntime> m_sashRuntime;
  nsCOMPtr<sashISecurityManager> m_SecurityManager;
  WDF *m_WDF;
  string m_WeblicationDirectory;
  int m_eventID;
  
  //initialization functions
  void RegisterWithTaskManager();
  nsresult initModules();
  nsresult loadLocation(string& locID);
  JSObject * AddExtensionToSash(JSContext * jcx, JSObject * proto, nsISupports * ext);
  nsISupports * CreateExtension(const nsCID & libCID);
  string GetDirectoryForGUID(string GUID);

  //functions to add sash. to the js namespace
  void CreateSashObjectItems(JSContext * jcx, JSObject * sashObj);
  jsval GetExtensionJSObject(JSContext * jcx, JSObject * jsGObj, char * iface, char * contract);  
  WDFAction m_act;
  
  // maps contextID to JSContexts  
  PLHashTable *m_contextIDs;
  PRInt32 m_contextCount;
  // maps JSContexts to contextID
  PLHashTable *m_contexts;

  deque <nsISupports*> m_Extensions;

  vector<string> m_args;
};

#endif // #define __SASHRUNTIMEBASE_H__
