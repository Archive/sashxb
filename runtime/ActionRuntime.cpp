
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Ari Heitner, AJ Shankar, Andrew Chatham

Sash's main class -- runs sash, loads extensions and locations, etc.

*****************************************************************/

#include "nsDebug.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <gnome.h>
#include <algorithm>

#include "sash_constants.h"
#include "nscore.h"
#include "prenv.h"
#include "prtypes.h"

#include <prmon.h>
#include <nsCOMPtr.h>
#include <nsString.h>
#include <nsIDocShell.h>
#include <nsIFactory.h>
#include <nsIFile.h>
#include <nsILocalFile.h>
#include <nsComponentManagerUtils.h>
#include <nsIServiceManager.h>
#include <nsIGenericFactory.h>
#include <nsIXPConnect.h>

#include "wdf.h"
#include "debugmsg.h"

#include "sashIExtension2.h"
#include "sashILocation2.h"
#include "sashIRuntime.h"
#include "ActionRuntime.h"
#include "RuntimeTools.h"
#include "SashObjectWrapper.h"
#include "extensiontools.h"
#include "WeblicationRegister.h"
#include "secman.h"
#include "sashVariantUtils.h"
#include "FileSystem.h"
#include "SashGUID.h"
#include "sashISecurityManager.h"

/******
	Hash related functions ofr the context ID map
******/
PR_EXTERN(PLHashNumber) ActRuntime_HashInt(const void *key) {
	return (PLHashNumber) *(PRInt32 *) key;
}

PR_EXTERN(PLHashNumber) ActRuntime_HashContext(const void *key) {
	return (PLHashNumber) key;
}

PR_EXTERN(PRIntn) ActRuntime_CompareInt(const void *v1, const void *v2) {
	return *((PRInt32 *) v1)== *((PRInt32 *) v2);
}

static PRIntn PR_CALLBACK
ActRuntime_ReleaseIDs(PLHashEntry *he, PRIntn i, void *arg) {
	delete (PRInt32 *) he->key;
    return HT_ENUMERATE_REMOVE;
}
/******
	Done with hash
******/

NS_IMPL_ISUPPORTS1(ActionRuntime, sashIActionRuntime)
	 
ActionRuntime::ActionRuntime() : 
	m_continueEvents(true),
	m_location(NULL), 
	m_instMan(NULL), 
	m_actGuid(""), 
	m_webGuid(""),
	m_contextIDs(0),
	m_contextCount(0),
	m_contexts(0)
{
  NS_INIT_ISUPPORTS();
	m_mon = PR_NewMonitor();
}

ActionRuntime::~ActionRuntime()
{
	 m_location = NULL;
	 PR_DestroyMonitor(m_mon);
	 if (m_contextIDs)
	 	PL_HashTableDestroy(m_contextIDs);
	 m_contextIDs= 0;
}

/* readonly attribute sashILocation location; */
NS_IMETHODIMP 
ActionRuntime::GetLocation(nsISupports ** aLocation)
{
	 *aLocation = m_location;
	 NS_ADDREF(*aLocation);
	 return NS_OK;
}

/* void Initialize (in string weblicationGuid, in string actionGuid); */
NS_IMETHODIMP 
ActionRuntime::Initialize(sashIRuntime *sashRuntime, 
						  const char *weblicationGuid, 
						  const char *actionGuid,
						  PRUint32 argc, const char ** argv)
{
  DEBUGMSG(runtime, "ActionRuntime::Initialize\n");

  for (PRUint32 i = 0; i < argc; i++)
	   m_args.push_back(argv[i]);
  m_sashRuntime = sashRuntime;
  m_webGuid = ToUpper(weblicationGuid);
  m_actGuid = ToUpper(actionGuid);

  m_instMan = new InstallationManager();
  if (!m_instMan->IsInstalled(m_webGuid))
	   OutputError("Cannot find weblication!");

  m_WDF = new WDF();
  m_WeblicationDirectory = 
	   WeblicationDirectory(m_instMan->GetSashInstallDirectory(), m_webGuid);
  if (!m_WDF->LoadFromFile(m_WeblicationDirectory + "/" + WDFFileName(m_webGuid)))
	   OutputError("Could not load WDF!");

  chdir(DataDirectory(m_WeblicationDirectory).c_str());

  return initModules();
}

string ActionRuntime::GetDirectoryForGUID(string GUID) {
	 return DataDirectory(WeblicationDirectory(
							   m_instMan->GetSashInstallDirectory(), 
							   GUID)) + "/";
}

// loads the extensions/locations, will be called after initRuntime, actGuid must be set
nsresult
ActionRuntime::initModules()
{
  m_act = m_WDF->GetAction(m_actGuid);
  if (m_act.id.empty()) OutputError("Cannot find action %s\n", m_actGuid.c_str());

  vector<WDFDependency> ds = m_WDF->GetDependencies();
  vector<WDFDependency>::iterator ai = ds.begin(), bi = ds.end();
  while (ai != bi) {
	   if (!m_instMan->IsInstalled(ai->id))
			OutputError("Weblication dependency '%s' is not installed -- aborting!", ai->title.c_str());
	   ++ai;
  }

  // the guids of all extensions that might be accessed internally or via javascript
  m_instMan->GetAllExtensions(m_webGuid, m_actGuid, all_extension_guids);
  // the toplevel, dependent extensions that need to be instantiated and put in the
  // Sash namespace

  m_instMan->GetToplevelExtensions(m_webGuid, namespace_extension_guids);

  // always want to add the default extensions, if they're installed
  for (const char** a = DEFAULT_EXTENSIONS ; *a != NULL ; a++) {
	   if (m_instMan->IsInstalled(*a)) {
			if (find(all_extension_guids.begin(), all_extension_guids.end(), *a) == all_extension_guids.end())
				 all_extension_guids.push_back(*a);
			if (find(namespace_extension_guids.begin(), namespace_extension_guids.end(), *a) == 
				namespace_extension_guids.end())
				 namespace_extension_guids.push_back(*a);
	   }
  }

  vector<string> componentDirs;

  string sash_components = GetSashComponentsDirectory();
  componentDirs.push_back(sash_components);

  for (int i = 0; i < (int)all_extension_guids.size(); i++)
	   componentDirs.push_back(GetDirectoryForGUID(all_extension_guids[i]));
  componentDirs.push_back(GetDirectoryForGUID(m_act.locationid));
  SashRegisterSashRuntime(componentDirs);
  DEBUGMSG(runtime, "finished SashRegisterSashRuntime\n");

  sashISecurityManager* sec;
  nsresult res1 = CreateSecurityManager(&sec);
  if (! NS_SUCCEEDED(res1)) {
	   DEBUGMSG(runtime, "Can't create security manager!\n");
	   return res1;
  }
  m_SecurityManager = getter_AddRefs(sec);
  m_SecurityManager->Initialize((m_WeblicationDirectory + "/" + 
				 SecurityFileName(m_webGuid)).c_str());
  
  string loc = m_act.locationid;
  nsresult res = loadLocation(loc);
  return res;
}

nsresult
ActionRuntime::loadLocation(string& locID)
{
  DEBUGMSG(runtime, "Loading location %s\n", locID.c_str());

  nsCID libCID;
  libCID.Parse(locID.c_str());

  nsISupports * location;
  nsresult res = nsComponentManager::CreateInstance(libCID,
													nsnull,
													NS_GET_IID(nsISupports),
													(void**)&location);
  m_location = getter_AddRefs(location);
	   
  if (! NS_SUCCEEDED(res) || m_location == NULL) {
    DEBUGMSG(runtime, "Failed to CreateInstance on location %s\n", locID.c_str());
    OutputError("Failed to instantiate location %s", locID.c_str());
   }

  m_SecurityManager->SetLocation(m_location);

  DEBUGMSG(runtime, "Initializing location\n");
  nsCOMPtr<sashILocation2> loc2 = do_QueryInterface(m_location);
  if (loc2) {
	   return loc2->Initialize(this,
							   m_act.registration.c_str(), 
							   m_webGuid.c_str(), -1);
  }
  else {
	   nsCOMPtr<sashILocation> loc1 = do_QueryInterface(m_location);
	   if (loc1) {
			return loc1->Initialize(this,
									m_act.registration.c_str(), 
									m_webGuid.c_str(),
									NULL, NULL);
	   }
	   else {
			OutputError("Unable to initialize location: location does not\n"
						"support the sashILocation or sashILocation2 interface!");
	   }
  }
  return NS_ERROR_FAILURE;
}

nsISupports * ActionRuntime::CreateExtension(const nsCID & libCID)
{
	 nsISupports *ext;

	 DEBUGMSG(runtime, "Creating extension with cid %s\n", libCID.ToString());

	 nsresult res = nsComponentManager::CreateInstance(libCID,
							   nsnull,
							   NS_GET_IID(nsISupports),
							   (void**)&ext);
	 
	 if (!NS_SUCCEEDED(res)) {
	   DEBUGMSG(runtime, "Failed to create extension %s, err 0x%x\n", libCID.ToString(), res);
	   OutputError("Failed to instantiate extension %s", libCID.ToString());
	 }

	 DEBUGMSG(runtime, "Extension %s created\n", libCID.ToString());

/* ---------- Classinfo test ----------
	 printf("checking classinfo for extension %s\n", libCID.ToString());
	 nsIClassInfo * ci = NULL;
	 if (NS_SUCCEEDED(ext->QueryInterface(NS_GET_IID(nsIClassInfo), (void **)&ci))) {
		  printf("QI for classinfo succeeded -- calling GetInterfaces\n");
		  PRUint32 count;
		  nsIID ** ifaces;
		  if (NS_SUCCEEDED(ci->GetInterfaces(&count, &ifaces))) {
			   printf("got %d interfaces\n", count);
			   for (PRUint32 i = 0; i < count; i++)
					printf("interface %d: %s\n", i, ifaces[i]->ToString());
		  }
		  else {
			   printf("failed to get interfaces\n");
		  }
	 } 
	 else {
		  printf("failed to get classinfo\n");
	 }
--------- End classinfo test -------- */

	 
	 return ext;
}

JSObject * ActionRuntime::AddExtensionToSash(JSContext * jcx, JSObject * proto, nsISupports * ext)
{
	 JSObject * wrappedObj;
	 jsval wrappedVal;
	 string sashName;

	 nsCOMPtr<sashIExtension2> ext2 = do_QueryInterface(ext);
	 if (ext2) {
		  XP_GET_STRING(ext2->GetSashName, sashName);
	 }
	 else {
		  nsCOMPtr<sashIExtension> ext1 = do_QueryInterface(ext);
		  if (ext1) {
			   XP_GET_STRING(ext1->GetSashName, sashName);
		  }
		  else {
			   nsCOMPtr<nsIClassInfo> ci = do_QueryInterface(ext);
			   if (ci) {
					string contract;
					XP_GET_STRING(ci->GetContractID, contract);
					OutputError("Extension %s does not support the sashIExtension or\n"
								"sashIExtension2 interface!", contract.c_str());
			   }
		  }
	 }
	 RuntimeTools::WrapSashObject(NS_GET_IID(nsISupports), ext, jcx, &wrappedObj);
	 if (wrappedObj == NULL) {
		  OutputError("Failed to get wrapped JSObject from WrapVariantSashObject\n");
	 }
	 
	 wrappedVal = OBJECT_TO_JSVAL(wrappedObj);


	 DEBUGMSG(runtime, "Adding extension %s to the Sash namespace\n", sashName.c_str());
	   
	 JS_SetProperty(jcx, proto, sashName.c_str(), &wrappedVal);

	 return wrappedObj;
}

void ActionRuntime::CreateSashObjectItems(JSContext * jcx, JSObject * sashObj) 
{
  JSObject * wrappedObj;

  nsISupports * ext = NULL;
  int numExt;

  DEBUGMSG(runtime, "Creating Sash object items\n");

  // Load each extension and add it to the Sash object
  numExt = namespace_extension_guids.size();
  for (int i = 0; i < numExt; i++)
  {
	   nsCID libCID;
	   libCID.Parse(namespace_extension_guids[i].c_str());
	   DEBUGMSG(runtime, "Creating extension %s\n", namespace_extension_guids[i].c_str());
	   ext = CreateExtension(libCID);
	   if (!ext) {
			fprintf(stderr, "Unable to get extension info for %s\n", namespace_extension_guids[i].c_str());
			continue;
	   } else {
			m_Extensions.push_front(ext);
	   }

	   wrappedObj = AddExtensionToSash(jcx, sashObj, ext);

	   nsCOMPtr<sashIExtension2> ext2 = do_QueryInterface(ext);
	   if (ext2) {
			PRInt32 cxid;
			if (NS_SUCCEEDED(RegisterScriptContext(jcx, &cxid))) {
				ext2->Initialize(this, 
							 m_act.registration.c_str(), m_webGuid.c_str(),
							 cxid);
			}
	   }
	   else {
			nsCOMPtr<sashIExtension> ext1 = do_QueryInterface(ext);
			if (ext1)
				 ext1->Initialize(this, 
								  m_act.registration.c_str(), m_webGuid.c_str(),
								  jcx, wrappedObj);
	   }
  }
  
  AddExtensionToSash(jcx, sashObj, m_location);
}

NS_IMETHODIMP 
ActionRuntime::CreateSashObjectNative(JSContext * jcx)
{
  JSObject * sash;

  DEBUGMSG(runtime, "Creating Sash top-level object\n");
  
  // Define the new sash object as permanent and read-only
  sash = JS_DefineObject(jcx, JS_GetGlobalObject(jcx), SASH_NAME, NULL, NULL, JSPROP_ENUMERATE);
  if (!sash)
    {
      OutputError("** Failed to create the Sash object!\n");
      return NS_ERROR_FAILURE;
    }
  CreateSashObjectItems(jcx, sash);
  return NS_OK;
}

NS_IMETHODIMP 
ActionRuntime::CreateSashObject(nsIScriptContext * jsContext)
{
  JSContext * jcx;
  DEBUGMSG(runtime, "In CreateSashObject with context %p\n", jsContext);
  jcx = (JSContext *) jsContext->GetNativeContext();
  if (!jcx)
  {
      OutputError("** Failed to get the native JS context!\n");
      return NS_ERROR_FAILURE;
  }
  DEBUGMSG(runtime, "About to call createsashobjectnative\n");
  return CreateSashObjectNative(jcx);
}

/* void Run (); */
NS_IMETHODIMP 
ActionRuntime::Run()
{
	 RegisterWithTaskManager();
  DEBUGMSG(runtime, "Running location...\n");
  
  nsCOMPtr<sashILocation2> loc2 = do_QueryInterface(m_location);
  if (loc2) {
	   return loc2->Run();
  }
  else {
	   nsCOMPtr<sashILocation> loc1 = do_QueryInterface(m_location);
	   if (loc1) {
			return loc1->Run();
	   }
	   else {
			OutputError("Unable to initialize location: location does not\n"
						"support the sashILocation or sashILocation2 interface!");
	   }
  }
  return NS_ERROR_FAILURE;
}

/* readonly attribute string guid; */
NS_IMETHODIMP ActionRuntime::GetGuid(char * *aGuid)
{
	 XP_COPY_STRING(m_webGuid.c_str(), aGuid);
	 return NS_OK;
}

/* [noscript] readonly attribute nativeSecurityManager SecurityManager; */
NS_IMETHODIMP ActionRuntime::GetSecurityManager(sashISecurityManager * *aSecurityManager)
{
	 *aSecurityManager = m_SecurityManager;
	 NS_ADDREF(*aSecurityManager);
  return NS_OK;
}

/* readonly attribute string directory; */
NS_IMETHODIMP 
ActionRuntime::GetDataDirectory(char * *aDirectory)
{
	 XP_COPY_STRING(DataDirectory(m_WeblicationDirectory).c_str(), aDirectory);
	 return NS_OK;
}

/* readonly attribute string directory; */
NS_IMETHODIMP 
ActionRuntime::GetDirectory(char * *aDirectory)
{
	 XP_COPY_STRING(m_WeblicationDirectory.c_str(), aDirectory);
	 return NS_OK;
}

/* readonly attribute string directory; */
NS_IMETHODIMP 
ActionRuntime::GetSashDirectory(char * *aDirectory)
{
	 XP_COPY_STRING(m_instMan->GetSashInstallDirectory().c_str(), aDirectory);
	 return NS_OK;
}

/* readonly attribute string directory; */
NS_IMETHODIMP 
ActionRuntime::GetSashBinaryPath(char * *aDirectory)
{
	 XP_COPY_STRING(::GetSashBinaryPath().c_str(), aDirectory);
	 return NS_OK;
}

/* [noscript] nativeJSObject WrapSashObject (in nsIIDRef objIID, in nsISupports nativeObj, in nativeJSContext cx); */
NS_IMETHODIMP ActionRuntime::WrapSashObject(const nsIID & objIID, nsISupports *nativeObj, 
											JSContext * cx, JSObject * *_retval)
{
	 RuntimeTools::WrapSashObject(objIID, nativeObj, cx, _retval);
	 return NS_OK;
}


/* boolean WeblicationIsInstalled (in string guid); */
NS_IMETHODIMP ActionRuntime::WeblicationIsInstalled(const char *guid, PRBool *_retval) {
	 *_retval = m_instMan->IsInstalled(guid);
	 return NS_OK;
}

int event_callback(void * data) {
  ActionRuntime * act = (ActionRuntime *) data;
  act->EventHandler();

  return FALSE;
}

NS_IMETHODIMP
ActionRuntime::ProcessEvent(nsISupports *ext)
{
	PR_EnterMonitor(m_mon);

	if (m_continueEvents) {
	    if (m_pendingExtensions.empty())
		  m_eventID = gtk_idle_add(event_callback, this);
		m_pendingExtensions.insert(ext);

		PR_Notify(m_mon);
	}

	PR_ExitMonitor(m_mon);
	return NS_OK;
}

nsISupports * 
ActionRuntime::GetNextPendingExtension()
{
	PR_EnterMonitor(m_mon);

	//	while (m_pendingExtensions.empty() && m_continueEvents)
	//		PR_Wait(m_mon, PR_INTERVAL_NO_TIMEOUT);

	// if m_continueEvents is now false, quit now
	if (!m_continueEvents || m_pendingExtensions.empty()) {
		PR_ExitMonitor(m_mon);
		return NULL;
	}

	assert(!m_pendingExtensions.empty());

	set<nsISupports *>::iterator it = m_pendingExtensions.begin();
	nsISupports *ext = *it;
	m_pendingExtensions.erase(it);
	assert (ext != NULL);

	PR_ExitMonitor(m_mon);
	return ext;
}

void ActionRuntime::EventHandler() {
  nsISupports *ext;

  while ((ext = GetNextPendingExtension())) {
	   nsCOMPtr<sashIExtension2> ext2 = do_QueryInterface(ext);
	   if (ext2) {
			ext2->ProcessEvent();
	   }
	   else {
			nsCOMPtr<sashIExtension> ext1 = do_QueryInterface(ext);
			if (ext1)
				 ext1->ProcessEvent();
	   }
  }
}

NS_IMETHODIMP
ActionRuntime::EventLoop()
{
  m_eventID = gtk_idle_add(event_callback, this);

  gtk_main();

  return NS_OK;
}

NS_IMETHODIMP
ActionRuntime::SignalQuit()
{
	PR_EnterMonitor(m_mon);

	m_continueEvents = false;
	PR_Notify(m_mon);

	PR_ExitMonitor(m_mon);
	return NS_OK;
}


void ActionRuntime::RegisterWithTaskManager() {
	 DEBUGMSG(runtime, "Registering action with task manager\n");
		 SashExtensionItem info = m_instMan->GetInfo(m_webGuid);
		 WDFAction a = m_WDF->GetAction(m_actGuid);
		 SashExtensionItem loc = m_instMan->GetInfo(a.locationid);

		 WeblicationRegister w(DEFAULT_PORT);
		 w.webl_name = info.name;
		 w.webl_guid = info.guid;
		 w.act_name = a.name;
		 w.loc_name = loc.name;
		 w.registerMe();
}

NS_IMETHODIMP ActionRuntime::Cleanup(){
	if (m_contextIDs || m_contexts) {
		PL_HashTableEnumerateEntries(m_contextIDs, ActRuntime_ReleaseIDs, 0);
		PL_HashTableDestroy(m_contextIDs);
		m_contextIDs= 0;
		PL_HashTableDestroy(m_contexts);
		m_contexts= 0;
	}
	 deque<nsISupports *>::iterator a = m_Extensions.begin(),
		  b = m_Extensions.end();
	 while (a != b){
		  nsCOMPtr<sashIExtension2> ext2 = do_QueryInterface(*a);
		  if (ext2) {
			   ext2->Cleanup();
		  }
		  else {
			   nsCOMPtr<sashIExtension> ext1 = do_QueryInterface(*a);
			   if (ext1)
					ext1->Cleanup();
		  }
		  ++a;
	 }
	 return NS_OK;
}

NS_GENERIC_FACTORY_CONSTRUCTOR(ActionRuntime)

  static nsModuleComponentInfo components[] = {
    { "ActionRuntime component",
      ACTIONRUNTIME_CID,
      ACTIONRUNTIME_CONTRACT_ID,
      ActionRuntimeConstructor,
      NULL,
      NULL
    }
  };

/* [noscript] void CallEvent (in string event, in nativeJSContext cx, in PRUint32 argc, [array, size_is (argc)] in nsIVariant argv); */
NS_IMETHODIMP ActionRuntime::CallEvent(const char *event, JSContext * cx, 
									   PRUint32 argc, nsIVariant **argv,
									   nsIVariant ** result)
{
	 RuntimeTools::CallEvent(event, cx, argc, argv, result);
	 return NS_OK;
}

/* [noscript] nativeJSVal VariantGetJSVal (in nsIVariant variant); */
NS_IMETHODIMP ActionRuntime::VariantGetJSVal(nsIVariant *variant, JSContext * cx,
											 jsval & _retval)
{
	 _retval = RuntimeTools::VariantGetJSVal(cx, variant);
	 return NS_OK;
}

/* [noscript] nsIVariant CreateVariantFromJSVal (in nativeJSVal val); */
NS_IMETHODIMP ActionRuntime::CreateVariantFromJSVal(JSContext * cx, jsval & val, 
													nsIVariant **_retval)
{
	 *_retval = RuntimeTools::CreateVariantFromJSVal(cx, val);
	 return NS_OK;
}

NS_IMETHODIMP ActionRuntime::AssertFSAccess(const char* path) {
	 bool local = FileSystem::IsLocal(path, DataDirectory(m_WeblicationDirectory));

	 if (local)
		  AssertSecurity(m_SecurityManager, GSS_FILESYSTEM_ACCESS, (float)FSS_LOCAL);
	 else
		  AssertSecurity(m_SecurityManager, GSS_FILESYSTEM_ACCESS, (float)FSS_GLOBAL);

	 return NS_OK;
}

NS_IMETHODIMP ActionRuntime::GetArguments(nsIVariant ** args) {
	 NewVariant(args);
	 VariantSetArray(*args, m_args);
	 return NS_OK;
}

NS_IMETHODIMP ActionRuntime::GenerateGUID(char ** guid) {
	 string s = GetNewGUID();
	 XP_COPY_STRING(s, guid);
	 return NS_OK;
}

NS_IMETHODIMP ActionRuntime::GUIDIsValid(const char * guid) {
	 return (VerifyGUID(guid) ? NS_OK : NS_ERROR_FAILURE);
}

NS_IMETHODIMP ActionRuntime::EnableSashNamespaceInMozEmbed(PRBool enable) {
//	 RuntimeTools::EnableSashNamespaceInMozEmbed(enable);
	 return NS_OK;
}

NS_IMETHODIMP ActionRuntime::RegisterScriptContext(JSContext * cx, PRInt32 * handle) {
	if (!handle)
		return NS_ERROR_NULL_POINTER;
	if (!m_contextIDs) {
		m_contextIDs= PL_NewHashTable(
			16, ActRuntime_HashInt,
			ActRuntime_CompareInt, PL_CompareValues, 0, 0
		);
		m_contexts= PL_NewHashTable(
			16, ActRuntime_HashContext,
			PL_CompareValues, ActRuntime_CompareInt, 0, 0
		);
	}
	if (!m_contextIDs || !m_contexts)
		return NS_ERROR_FAILURE;
	PRInt32 *cxid= (PRInt32 *) PL_HashTableLookup(m_contexts, cx);
	if (cxid) { // we've already registered this context
		*handle= *cxid;
		return NS_OK;
	}
	// need new contextID
	*handle= m_contextCount;
	PRInt32 *newHandle= new int(*handle);
	++m_contextCount;
	if (PL_HashTableAdd(m_contextIDs, newHandle, cx) && PL_HashTableAdd(m_contexts, cx, newHandle))
		return NS_OK;
	else
		return NS_ERROR_FAILURE;
}

NS_IMETHODIMP ActionRuntime::CallEventByID(const char *event, PRInt32 contextID, PRUint32 argc, nsIVariant **argv, nsIVariant **result) {
	JSContext *cx;
	cx= (JSContext *) PL_HashTableLookup(m_contextIDs, &contextID);
	if (!cx)
		return NS_ERROR_FAILURE;
	return CallEvent(event, cx, argc, argv, result);
}
//NS_IMPL_NSGETMODULE("sashActionRuntimeModule", components)

