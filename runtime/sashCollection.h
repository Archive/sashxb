
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung, AJ Shankar

collection object
******************************************************************/

#ifndef SASHCOLLECTION_H
#define SASHCOLLECTION_H

#include "sashICollection.h"
#include "sashIConstructor.h"
#include <vector>
#include <hash_map>

struct eqstr
{
  bool operator()(const char* s1, const char* s2) const
  {
    return strcmp(s1, s2) == 0;
  }
};

typedef hash_map<const char*, nsIVariant*, hash<const char*>, eqstr> CollectionMap;

class sashCollection : public sashICollection,
					   public sashIConstructor
{
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHICOLLECTION
	 NS_DECL_SASHICONSTRUCTOR
		  
	 sashCollection();
	 virtual ~sashCollection();

private:
  CollectionMap m_collection;
  void printVariantInfo(nsIVariant *var);
  void printCollection();
};

#endif
