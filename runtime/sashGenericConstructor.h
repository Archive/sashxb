
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHTESTATEV_H
#define SASHTESTATEV_H

#include "sashIGenericConstructor.h"
#include "nsMemory.h"
#include "nsIXPConnect.h"
#include "nsIXPCScriptable.h"
#include "jsapi.h"
#include "prmem.h"
#include <string>

class sashIExtension;
class sashIExtension2;

class sashGenericConstructor: public sashIGenericConstructor
{
  
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_NSIXPCSCRIPTABLE
    NS_DECL_SASHIGENERICCONSTRUCTOR
    NS_DECL_SASHIGENERICSCRIPTABLECONSTRUCTOR

    sashGenericConstructor();
    virtual ~sashGenericConstructor();
    
  private:
    string m_contractid;
    string m_interface;
    sashIExtension *m_ext;
    sashIExtension2 *m_ext2;
	nsCOMPtr<sashIActionRuntime> m_act;
};


#endif // SASHTESTATEV_H
