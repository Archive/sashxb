
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung, AJ Shankar

implementation of the collection object
******************************************************************/

#include "sashCollection.h"
#include "sashVariantUtils.h"
#include <string>

NS_IMPL_ISUPPORTS2_CI(sashCollection, sashICollection, sashIConstructor);

sashCollection::sashCollection()
{
  NS_INIT_ISUPPORTS();
  DEBUGMSG(coll, "collection constructor\n");
}

sashCollection::~sashCollection()
{
	 DEBUGMSG(coll, "collection destructor: %p", this);
}

NS_IMETHODIMP
sashCollection::InitializeNewObject(sashIActionRuntime * actionRuntime, 
									sashIExtension * ext, JSContext * cx, 
									PRUint32 argc, nsIVariant **argv, 
									PRBool *ret) {
	 DEBUGMSG(coll, "sashCollection::InitiailizeNewObject(): %p\n", this);
	 *ret = true;
	 return NS_OK;
}

void sashCollection::printCollection(){
	 CollectionMap::iterator a = m_collection.begin(),
		  b = m_collection.end();
	 
	 DEBUGMSG(coll, "======START: %p\n", this);
	 DEBUGMSG(coll, "hash_map contains %d items\n", m_collection.size());

	 unsigned int i =0;
	 while ((a != b) && (++i <= m_collection.size())){
		  DEBUGMSG(coll, "key: %s, value: ", a->first);
		  printVariantInfo(a->second);
		  ++a;
	 }
	 if (i > m_collection.size()){
		  DEBUGMSG(coll, "error in printCollection: confused iterator\n");
	 }
	 DEBUGMSG(coll, "======END: %p\n", this);
}

void sashCollection::printVariantInfo(nsIVariant *var){
	 if (var == NULL){
		  DEBUGMSG(coll, "variant is null\n");
		  return;
	 }
	 if (VariantIsString(var)){
		  DEBUGMSG(coll, "variant is a string: %s\n",
				   VariantGetString(var).c_str());
	 } else if (VariantIsArray(var)){
		  vector<string> strVec;
		  VariantGetArray(var, strVec);
		  if (strVec.size() > 0){
			   DEBUGMSG(coll, "variant is an array(%d elements): first: %s\n",
						strVec.size(),
						strVec[0].c_str());
		  } else {
			   DEBUGMSG(coll, "variant is an array: empty\n");
		  }

	 }
}

/* void Add (in nsIVariant object); */
NS_IMETHODIMP sashCollection::Add(const char* key, nsIVariant *object)
{
	 DEBUGMSG(coll, "sashCollection::Add (key: %s)\n", key);
	 char *mykey = strdup(key);
	 nsIVariant *newvar;
	 NewVariant(&newvar);
	 VariantSetVariant(newvar, object);
	 m_collection[mykey] = newvar;
	 return NS_OK;
}

/* nsIVariant Item (in nsIVariant name); */
NS_IMETHODIMP sashCollection::Item(const char* key, nsIVariant **_retval) {
	 DEBUGMSG(coll, "sashCollection::Item (key: %s)\n", key);
	 NewVariant(_retval);
	 if (m_collection.count(key) == 1){
		  DEBUGMSG(coll, "trying to return item: \n");
		  printVariantInfo(m_collection[key]);
		  VariantSetVariant(*_retval, m_collection[key]);
	 } else {
		  DEBUGMSG(coll, "null variant for key\n");
		  VariantSetEmpty(*_retval);
	 }
	 return NS_OK;
}

/* nsIVariant Item (in nsIVariant name); */
NS_IMETHODIMP sashCollection::Exists(const char* key, PRBool *_retval) {
	 *_retval = (m_collection.count(key) == 1);
	 return NS_OK;
}

/* nsIVariant Remove (in nsIVariant id); */
NS_IMETHODIMP sashCollection::Remove(const char* key, nsIVariant **_retval) {
	 DEBUGMSG(coll, "sashCollection::Remove (key: %s)\n", key);
	 NewVariant(_retval);
	 if (m_collection.count(key) == 1) {
		  DEBUGMSG(coll, "erasing key: %s\n", key);
		  VariantSetVariant(*_retval, m_collection[key]);
		  NS_IF_RELEASE(m_collection[key]);
		  m_collection.erase(key);
	 }
	 return NS_OK;
}

/* void RemoveAll (); */
NS_IMETHODIMP sashCollection::RemoveAll() {
	 CollectionMap::iterator a = m_collection.begin(), b = m_collection.end();
	 while (a != b) {
		  NS_IF_RELEASE(a->second);
		  ++a;
	 }
	 m_collection.clear();
	 return NS_OK;
}

NS_IMETHODIMP sashCollection::GetKeys(nsIVariant ** _retval) {
	 DEBUGMSG(coll, "sashCollection::GetKeys()\n");
	 NewVariant(_retval);
	 CollectionMap::iterator a = m_collection.begin(), b = m_collection.end();
	 vector<string> keys;
	 string currkey;

	 unsigned int i=0;
	 while ((a != b) && (++i <= m_collection.size())) {
		  currkey = a->first;
		  DEBUGMSG(coll, "currkey: %s\n", currkey.c_str());
		  keys.push_back(currkey);
		  ++a;
	 }
	 VariantSetArray(*_retval, keys);
	 return NS_OK;
}

NS_IMETHODIMP sashCollection::GetItems(nsIVariant ** _retval) {
	 NewVariant(_retval);
	 CollectionMap::iterator a = m_collection.begin(), b = m_collection.end();
	 vector<nsIVariant*> items;
	 while (a != b) {
		  items.push_back(a->second);
		  ++a;
	 }
	 VariantSetArray(*_retval, items);

	 return NS_OK;
}

/* attribute long Count; */
NS_IMETHODIMP sashCollection::GetCount(PRUint32 *aCount)
{
	 *aCount = m_collection.size();
	 return NS_OK;
}


NS_DECL_CLASSINFO(sashCollection);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashCollection);

#define _ENTRY MODULE_COMPONENT_ENTRY(sashCollection, SASHCOLLECTION, "Collection")

static nsModuleComponentInfo sashCollection_components[]= {_ENTRY};
NS_IMPL_NSGETMODULE(sashCollection, sashCollection_components);
