
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _SASHOBJECTWRAPPER_H
#define _SASHOBJECTWRAPPER_H

#include <string>
#include <nsIID.h>
#include <nsCOMPtr.h>
#include "nsIInterfaceInfoManager.h"
#include "nsIVariant.h"
#include "jsapi.h"

struct WrapperInfo;

class SashObjectWrapper {
 public:
  static void CreateSashObject(nsIID n, nsISupports* in, JSContext* cx, JSObject** j);
  static JSBool PrintObject(JSContext* cx, JSObject* obj, uintN argc, jsval* argv, jsval* rval);

private:
  static JSBool ObjectGetProperty(JSContext *cx, JSObject *obj, jsval id, jsval *vp);
  static JSBool ObjectSetVariantProperty(JSContext *cx, JSObject *obj, jsval id, jsval *vp);
  static JSBool ObjectSetProperty(JSContext *cx, JSObject *obj, jsval id, jsval *vp);
  static nsISupports * GetNativeObject(JSContext * cx, JSObject * obj);
  static void FixRetVal(JSContext * cx, jsval * v);
  static JSBool SuperFunction(JSContext* cx, JSObject* obj, uintN argc, jsval* argv, jsval* rval);
  static bool IsVariant(int i, const nsXPTParamInfo *arg, nsCOMPtr<nsIInterfaceInfo> iface);
  static void WrapObjectInterface(JSObject* obj, nsIID id, WrapperInfo* w);
};

#endif 

