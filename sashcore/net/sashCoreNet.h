
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for sashcorenet

*****************************************************************/

#ifndef SASHCORENETEXT_H
#define SASHCORENETEXT_H

#include "sashICoreNet.h"
#include "sashIConstructor.h"
#include "nsID.h"
#include "nsIFactory.h"
#include "SecurityManager.h"
#include <string>
#include <vector>
#include "sashURLConnection.h"

// (5e297604-7631-47cf-83b7-1e7d71d1f344)
#define SASHCORENET_CID {0x5e297604, 0x7631, 0x47cf, {0x83, 0xb7, 0x1e, 0x7d, 0x71, 0xd1, 0xf3, 0x44}}

NS_DEFINE_CID(ksashCoreNetCID, SASHCORENET_CID);

#define SASHCORENET_CONTRACT_ID "@gnome.org/SashXB/Net;1"

class sashIGenericConstructor;

class sashCoreNet : public sashICoreNet {
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHICORENET

  sashCoreNet();
  virtual ~sashCoreNet();

  void addAsyncConnection (sashURLConnection *conn);

private:
  sashIGenericConstructor *m_URLConnectionConstructor;
  sashIExtension *m_sashCore;
  sashIActionRuntime *m_rt;
  sashISecurityManager* m_secMan;
  string m_onConnectStateChange;
  JSContext *m_cx;

  bool anyAsyncConnections ();
  vector<sashURLConnection*> m_connections;
  pthread_mutex_t m_connLock;

  void lockConnList(){
	   pthread_mutex_lock(&m_connLock);
  }
  void unlockConnList(){
	   pthread_mutex_unlock(&m_connLock);
  }

  string m_homeServer;
};

#endif
