
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for sashURLConnection

*****************************************************************/

#include "sashURLConnection.h"
#include "sashURLConnectionHeaders.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "nsCRT.h"
#include "sashIExtension.h"
#include "sashCoreNet.h"
#include "SashNet.h"

NS_IMPL_ISUPPORTS2_CI(sashURLConnection, sashIURLConnection, sashIConstructor);


sashURLConnection::sashURLConnection()
{
	 NS_INIT_ISUPPORTS();
	 m_requestHeaders = NULL;
	 m_responseHeaders = NULL;

	 m_connectionType = GET;
	 m_status = STATUS_SUCCESS;
	 m_connected = false;
	 
	 m_aborted = false;
	 m_isAsync = false;

	 m_saveResponseToFile = false;

	 m_emptyResponseHeaders = true;
	 
	 m_response = "";

	 m_requestFile = "";
	 m_requestBody = "";

	 m_progressEventFrequency = 1;
	 m_progressBlock = 0;

	 m_errorMessage = "";
}

void sashURLConnection::reinit(){
	 // get rid of old headers, responses, etc.
	 SetStatus(STATUS_SUCCESS);
	 SetConnected(false);
	 m_aborted = false;
	 m_emptyResponseHeaders = true;
	 m_response = "";
	 m_progressBlock = 0;
	 m_errorMessage = "";
}

sashURLConnection::~sashURLConnection()
{
	 DEBUGMSG(net, "destroying URL connection\n");
	 pthread_mutex_destroy(&m_connectionLock);
	 NS_IF_RELEASE(m_requestHeaders);
	 NS_IF_RELEASE(m_responseHeaders);
	 if (m_dc) delete m_dc;
}

NS_IMETHODIMP
sashURLConnection::InitializeNewObject(sashIActionRuntime * actionRuntime, 
									   sashIExtension * ext, 
									   JSContext * cx, PRUint32 argc,
									   nsIVariant **argv, PRBool *ret)
{
 	 DEBUGMSG(net, "initializing new url connection object\n");
	 m_cx = cx;
	 m_ext = ext;
	 m_act = actionRuntime;
	 assert (ext);

	 *ret = true;

	 if (argc > 1) {
		  *ret = false;
	 } else {
		  if (argc == 1){
			   SetURL(VariantGetString(argv[0]).c_str());
		  }
		  pthread_mutex_init(&m_connectionLock, NULL);
		  m_dc = new URLConnectionDownloadCallback(this);
	 }
	 return NS_OK;
}

// call this from sashCore::ProcessEvent.
bool sashURLConnection::ProcessEvent(){
	 PRInt32 status;

	 if (m_connectionType == GET){
		  m_sdl->Process(&status);
		  while ((status == SashNet::NET_TRANSFERRING)
				 && (! m_aborted)){
			   DEBUGMSG(net, "more data to download\n");
			   m_sdl->Process(&status);
		  }
	 } else if (m_connectionType == POST){
		  m_sul->Process(&status);
		  while ((status == SashNet::NET_TRANSFERRING)
				 && (! m_aborted)){
			   DEBUGMSG(net, "more data to upload\n");
			   m_sul->Process(&status);
		  }
	 } 

	 switch(status){
	 case SashNet::NET_SETUP:
		  DEBUGMSG(net, "download setting up.\n");
		  break;

	 case SashNet::NET_TRANSFERRING:
		  DEBUGMSG(net, "download transferring.\n");
		  break;
	 case SashNet::NET_ABORTED:
	 case SashNet::NET_DONE:
	 case SashNet::NET_ERROR:
	 case SashNet::NET_ERROR_HOSTNAME_NOT_RESOLVED:
	 case SashNet::NET_ERROR_FILE_NOT_FOUND:
		  transferComplete(status);
		  break;

	 default:
		  DEBUGMSG(net, "Unknown SashNet constant\n");
		  break;
	 }

	 return true;
}

/* readonly attribute PRInt32 GET; */
NS_IMETHODIMP sashURLConnection::GetGET(PRInt32 *aGET)
{
	 *aGET = GET;
	 return NS_OK;
}

/* readonly attribute PRInt32 POST; */
NS_IMETHODIMP sashURLConnection::GetPOST(PRInt32 *aPOST)
{
	 *aPOST = POST;
	 return NS_OK;
}

/* readonly attribute PRInt32 STATUS_CANCEL; */
NS_IMETHODIMP sashURLConnection::GetSTATUS_CANCEL(PRInt32 *aSTATUS_CANCEL)
{
	 *aSTATUS_CANCEL = STATUS_CANCEL;
	 return NS_OK;
}

/* readonly attribute PRInt32 STATUS_FAIL; */
NS_IMETHODIMP sashURLConnection::GetSTATUS_FAIL(PRInt32 *aSTATUS_FAIL)
{
	 *aSTATUS_FAIL = STATUS_FAIL;
	 return NS_OK;
}

/* readonly attribute PRInt32 STATUS_NAME_NOT_RESOLVED; */
NS_IMETHODIMP sashURLConnection::GetSTATUS_NAME_NOT_RESOLVED(PRInt32 *aSTATUS_NAME_NOT_RESOLVED)
{
	 *aSTATUS_NAME_NOT_RESOLVED = STATUS_NAME_NOT_RESOLVED;
	 return NS_OK;

}
/* readonly attribute PRInt32 STATUS_PROGRESS; */
NS_IMETHODIMP sashURLConnection::GetSTATUS_PROGRESS(PRInt32 *aSTATUS_PROGRESS)
{
	 *aSTATUS_PROGRESS = STATUS_PROGRESS;
	 return NS_OK;
}

/* readonly attribute PRInt32 STATUS_SUCCESS; */
NS_IMETHODIMP sashURLConnection::GetSTATUS_SUCCESS(PRInt32 *aSTATUS_SUCCESS)
{
	 *aSTATUS_SUCCESS = STATUS_SUCCESS;
	 return NS_OK;
}

/* readonly attribute PRInt32 STATUS_TIMEOUT; */
NS_IMETHODIMP sashURLConnection::GetSTATUS_TIMEOUT(PRInt32 *aSTATUS_TIMEOUT)
{
	 *aSTATUS_TIMEOUT = STATUS_TIMEOUT;
	 return NS_OK;
}

/* attribute PRBool Async; */
NS_IMETHODIMP sashURLConnection::GetAsync(PRBool *aAsync)
{
	 *aAsync = m_isAsync;
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetAsync(PRBool aAsync)
{
	 m_isAsync = aAsync;
	 return NS_OK;
}

/* readonly attribute PRBool ConnectionActive; */
NS_IMETHODIMP sashURLConnection::GetConnectionActive(PRBool *aConnectionActive)
{
	 // active if there's a downloader that has been started.
	 lockConnection();
	 *aConnectionActive = m_connected;
	 unlockConnection();
	 return NS_OK;
}

bool sashURLConnection::GetConnected(bool lock){
	 bool retval;
	 if (lock) 	 lockConnection();
	 retval = m_connected;
	 if (lock) 	 unlockConnection();
	 return retval;
}

void sashURLConnection::SetConnected(bool value, bool lock){
	 if (lock) 	 lockConnection();
	 m_connected = value;
	 if (lock) 	 unlockConnection();
}

int sashURLConnection::GetStatus(bool lock){
	 int retval;
	 if (lock) 	 lockConnection();
	 retval = m_status;
	 if (lock) 	 unlockConnection();
	 return retval;
}

void sashURLConnection::SetStatus(int value, bool lock){
	 if (lock) 	 lockConnection();
	 m_status = (Status) value;
	 if (lock) 	 unlockConnection();
}

/* readonly attribute PRInt32 ConnectionType; */
NS_IMETHODIMP sashURLConnection::GetConnectionType(PRInt32 *aConnectionType)
{
	 *aConnectionType = m_connectionType;
	 return NS_OK;
}

NS_IMETHODIMP sashURLConnection::SetConnectionType(PRInt32 aConnectionType)
{
	 m_connectionType = (ConnectionType) aConnectionType;
	 return NS_OK;
}

/* readonly attribute string ErrorMessage; */
NS_IMETHODIMP sashURLConnection::GetErrorMessage(char * *aErrorMessage)
{
	 XP_COPY_STRING(m_errorMessage.c_str(), aErrorMessage);
	 return NS_OK;
}

void sashURLConnection::SetErrorMessage(const string &message){
	 m_errorMessage = message;
}

/* attribute PRInt32 ProgressEventFrequency; */
NS_IMETHODIMP sashURLConnection::GetProgressEventFrequency(PRInt32 *aProgressEventFrequency)
{
	 *aProgressEventFrequency = m_progressEventFrequency;
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetProgressEventFrequency(PRInt32 aProgressEventFrequency)
{
	 m_progressEventFrequency = aProgressEventFrequency;
	 return NS_OK;	 
}

/* attribute string Protocol; */
NS_IMETHODIMP sashURLConnection::GetProtocol(char * *aProtocol)
{
	 XP_COPY_STRING (m_protocol.c_str(), aProtocol);
	 return NS_OK;
}

/* attribute string RequestFile; */
NS_IMETHODIMP sashURLConnection::GetRequestFile(char * *file)
{
	 XP_COPY_STRING (m_requestFile.c_str(), file);
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetRequestFile(const char * file)
{
	 m_requestFile = file;
	 return NS_OK;
}


/* void SetRequestFile (in string key, in string value); */
NS_IMETHODIMP sashURLConnection::SetRequestFile(const char *key, const char *filename)
{
// 	 DEBUGMSG(net, "SetRequestFile()\n");
// 	 m_requestFiles[key] = filename;
// 	 return NS_OK;
	 return NS_ERROR_NOT_IMPLEMENTED;
}

/* void ClearRequestFiles (); */
NS_IMETHODIMP sashURLConnection::ClearRequestFiles()
{
// 	 m_requestFiles.clear();
// 	 return NS_OK;
	 return NS_ERROR_NOT_IMPLEMENTED;
}



/* void SetRequestParameter (in string key, in string value); */
NS_IMETHODIMP sashURLConnection::SetRequestParameter(const char *key, const char *value)
{
	 m_requestParameters[key] = value;
	 string ret = createParameterString();
	 return NS_OK;
}

/* void ClearRequestParameters (); */
NS_IMETHODIMP sashURLConnection::ClearRequestParameters()
{
	 m_requestParameters.clear();
	 return NS_OK;
}

string sashURLConnection::createParameterString(){
	 map<string, string>::const_iterator ai = m_requestParameters.begin(),
		  bi = m_requestParameters.end();
	 string parameters = "";
	 bool first = true;
	 while (ai != bi){
		  if (! first) parameters += '&';
		  else first = false;
		  parameters += (ai->first + "=" + ai->second);
		  ++ ai;
	 }
	 return parameters;
}

/* attribute nsIVariant RequestBody; */
NS_IMETHODIMP sashURLConnection::GetRequestBody(char **aRequestBody)
{
	 XP_COPY_STRING (m_requestBody.c_str(), aRequestBody);
	 return NS_OK;
}

NS_IMETHODIMP sashURLConnection::SetRequestBody(const char *aRequestBody)
{
	 m_requestBody = aRequestBody;
	 return NS_OK;
}

/* readonly attribute sashIURLConnectionHeaders RequestHeaders; */
NS_IMETHODIMP sashURLConnection::GetRequestHeaders(sashIURLConnectionHeaders * *aRequestHeaders)
{
	 nsresult retval;
	 if (m_requestHeaders == NULL){
		  retval = nsComponentManager::CreateInstance(
			   ksashURLConnectionHeadersCID, NULL, 
			   NS_GET_IID(sashIURLConnectionHeaders),
			   (void **) &m_requestHeaders);
		  if (! NS_SUCCEEDED(retval)){
			   DEBUGMSG(net, "CreateInstance of sashURLConnectionHeaders failed\n");
			   return retval;
		  }
		  m_requestHeaders->Initialize(m_cx);
	 }
	 NS_ADDREF(m_requestHeaders);
	 *aRequestHeaders = m_requestHeaders;
	 return NS_OK;
}

/* readonly attribute nsIVariant Response; */
NS_IMETHODIMP sashURLConnection::GetResponse(char **aResponse)
{
	 if ((m_connectionType == GET) && (m_saveResponseToFile)){
		  XP_COPY_STRING (m_filename.c_str(), aResponse);
	 } else {
		  XP_COPY_STRING (m_response.c_str(), aResponse);
	 }
	 return NS_OK;
}

/* attribute string ResponseAsString; */
NS_IMETHODIMP sashURLConnection::GetResponseAsString(char * *aResponseAsString)
{
	 XP_COPY_STRING (m_response.c_str(), aResponseAsString);
	 return NS_OK;
}

/* readonly attribute PRInt32 ResponseCode; */
NS_IMETHODIMP sashURLConnection::GetResponseCode(PRInt32 *aResponseCode)
{
	 if (m_connectionType == GET){
		  m_sdl->GetHTTPStatus(aResponseCode);
	 } else {
		  m_sul->GetHTTPStatus(aResponseCode);
	 }

	 return NS_OK;
}

/* readonly attribute sashIURLConnectionHeaders ResponseHeaders; */
NS_IMETHODIMP sashURLConnection::GetResponseHeaders(sashIURLConnectionHeaders * *aResponseHeaders)
{
	 nsresult retval;
	 if (m_responseHeaders == NULL){
		  retval = nsComponentManager::CreateInstance(
			   ksashURLConnectionHeadersCID, NULL, 
			   NS_GET_IID(sashIURLConnectionHeaders),
			   (void **) &m_responseHeaders);
		  assert(NS_SUCCEEDED(retval));
		  m_responseHeaders->Initialize(m_cx);
	 }
	 NS_ADDREF(m_responseHeaders);

	 // should only have to do this once. 
	 // every time a new 'execute' happens, the old headers should
	 // get wiped out. until then, they should stay valid.
	 sashURLConnectionHeaders *rHeaders 
		  = (sashURLConnectionHeaders *) (m_responseHeaders);

	 if (m_emptyResponseHeaders){
		  rHeaders->Clear();

		  unsigned int numHeaders = m_responseHNames.size();
		  for (unsigned int i=0; i < numHeaders; i++){
			   DEBUGMSG(net, "setting header: (%s, %s)\n", 
						(m_responseHNames[i]).c_str(), 
						(m_responseHValues[i]).c_str());
			   rHeaders->SetValue((m_responseHNames[i]).c_str(), 
								  (m_responseHValues[i]).c_str());
		  }
		  m_emptyResponseHeaders = false;
	 }

	 *aResponseHeaders = m_responseHeaders;
	 return NS_OK;
}

/* attribute PRInt32 Retries; */
NS_IMETHODIMP sashURLConnection::GetRetries(PRInt32 *aRetries)
{
	 *aRetries = m_retries;
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetRetries(PRInt32 aRetries)
{
	 m_retries = aRetries;
	 return NS_OK;
}

/* attribute PRBool SaveResponseToFile; */
NS_IMETHODIMP sashURLConnection::GetSaveResponseToFile(PRBool *aSaveResponseToFile)
{
	 *aSaveResponseToFile = m_saveResponseToFile;
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetSaveResponseToFile(PRBool aSaveResponseToFile)
{
	 m_saveResponseToFile = aSaveResponseToFile;
	 return NS_OK;
}

/* attribute string LocalFile; */
NS_IMETHODIMP sashURLConnection::GetSaveResponseTo(char * *file)
{
	 XP_COPY_STRING (m_filename.c_str(), file);
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetSaveResponseTo(const char * file)
{
	 m_filename = file;
	 return NS_OK;
}

/* attribute PRInt32 Status; */
NS_IMETHODIMP sashURLConnection::GetStatus(PRInt32 *aStatus)
{
	 *aStatus = GetStatus();
	 return NS_OK;
}

/* attribute PRInt32 TimeOut; */
NS_IMETHODIMP sashURLConnection::GetTimeOut(PRInt32 *aTimeOut)
{
	 *aTimeOut = m_timeout;
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetTimeOut(PRInt32 aTimeOut)
{
	 m_timeout = aTimeOut;
	 return NS_OK;
}

/* attribute string URL; */
NS_IMETHODIMP sashURLConnection::GetURL(char * *aURL)
{
	 XP_COPY_STRING (m_URL.c_str(), aURL);
	 return NS_OK;
}

NS_IMETHODIMP sashURLConnection::SetURL(const char * aURL)
{
	 m_URL = aURL;
	 int index = m_URL.find("://");
	 m_protocol = (index == -1) ? "" : m_URL.substr(0, index);
	 return NS_OK;
}

/* attribute string UserAgent; */
NS_IMETHODIMP sashURLConnection::GetUserAgent(char * *aUserAgent)
{
	 XP_COPY_STRING (m_userAgent.c_str(), aUserAgent);
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetUserAgent(const char * aUserAgent)
{
	 m_userAgent = aUserAgent;
	 return NS_OK;
}

/* attribute boolean ForceMultiPartPost; */
NS_IMETHODIMP sashURLConnection::GetForceMultiPartPost(PRBool *aForceMultiPartPost)
{
// 	 *aForceMultiPartPost = m_forceMultiPart;
// 	 return NS_OK;
	 return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP sashURLConnection::SetForceMultiPartPost(PRBool aForceMultiPartPost)
{
// 	 m_forceMultiPart = aForceMultiPartPost;
// 	 return NS_OK;
	 return NS_ERROR_NOT_IMPLEMENTED;
}


/* void Abort (); */
NS_IMETHODIMP sashURLConnection::Abort()
{

	 if (m_connectionType == GET)
		  m_sdl->Abort();
	 else 
		  m_sul->Abort();
	 m_aborted = true;
	 return NS_OK;
}

string sashURLConnection::createGetURL(){
	 string url = m_URL;
	 string params = createParameterString();
	 if (params != ""){
		  url += '?';
		  url += params;
	 }
	 return url;
}

/* void Execute (); */
NS_IMETHODIMP sashURLConnection::Execute()
{
	 nsresult rv;
	 if (! GetConnected()){
		  reinit();

		  m_emptyResponseHeaders = true;
		  SetConnected(true);
		  SetStatus(STATUS_PROGRESS);

		  if (m_connectionType == GET){
			   string url = createGetURL();
			   if (m_saveResponseToFile){
					if ((m_filename == "")
						|| (m_filename == m_tempfilename)){
						 char name[] = "sashXXXXXX";
						 mktemp(name);
						 m_tempfilename = name;
						 m_filename = m_tempfilename;
					}
					rv = NewSashFileDownloader(url.c_str(), m_filename.c_str(),
											   false, getter_AddRefs(m_sdl));
			   } else {
					rv = NewSashDownloader(url.c_str(),  
										   getter_AddRefs(m_sdl));
			   }
			   if (! NS_SUCCEEDED(rv)){
					DEBUGMSG(net, "Couldn't create new downloader\n");
					return rv;
			   }

			   fillInDLHeaders(m_requestHeaders);
			   m_sdl->SetStatusCallback(m_dc);
			   m_startTime = time(NULL);
			   m_sdl->StartTransfer();
			   PRInt32 status;
			   
			   if (! m_isAsync){
					m_sdl->ProcessUntilComplete(&status);
					transferComplete(status);
			   } else {
					((sashCoreNet *)m_ext)->addAsyncConnection(this);
			   }
		  } else {
			   int body, file, parameters;
			   body = (m_requestBody == "") ? 0 : 1;
			   file = (m_requestFile == "") ? 0 : 1;
			   parameters = (m_requestParameters.size() == 0) ? 0 : 1;

			   if (body + file + parameters > 1){
					OutputError("You are only allowed to set one of the following: RequestBody, RequestFile, or RequestParameters\n");
					m_status = STATUS_FAIL;
					return NS_OK;
			   }

			   if (m_requestFile != "") {
					DEBUGMSG(net, "POST: uploading file: %s", m_requestFile.c_str());
					rv = NewSashFileUploader(m_URL.c_str(), 
											 m_requestFile.c_str(), 
											 getter_AddRefs(m_sul));
			   } else if (m_requestParameters.size() > 0) {
					string params = createParameterString();
					DEBUGMSG(net, "POST: parameters: %s\n",
							 params.c_str());
					rv = NewSashDataUploader(m_URL.c_str(), params.size(),
											 (char *)params.c_str(),
											 getter_AddRefs(m_sul));
			   }else if (m_requestBody != ""){
					DEBUGMSG(net, "POST: RequestBody...\n");
					rv = NewSashDataUploader(m_URL.c_str(),  
										m_requestBody.size(),
										(char *)m_requestBody.c_str(),
										getter_AddRefs(m_sul));
			   } else {
					DEBUGMSG(net, "POST: no body to upload...\n");
					rv = NewSashDataUploader(m_URL.c_str(),
										0, NULL, getter_AddRefs(m_sul));
			   }
			   if (! NS_SUCCEEDED(rv)){
					DEBUGMSG(net, "Couldn't create data uploader\n");
					return rv;
			   }
				   
			   fillInDLHeaders(m_requestHeaders);
			   m_sul->SetStatusCallback(m_dc);
			   m_sul->StartTransfer();
			   PRInt32 status;

			   if (! m_isAsync){
					DEBUGMSG(net, "upload: processuntilcomplete\n");
					m_sul->ProcessUntilComplete(&status);
					transferComplete(status);
			   } else {
					((sashCoreNet *)m_ext)->addAsyncConnection(this);
			   }
		  }
		  return NS_OK;
	 } else {
		  OutputError("ERROR: Tried to execute two simultaneous transfers with a URLConnection object.\n");
		  m_status = STATUS_FAIL;
		  return NS_OK;
	 }
}

void sashURLConnection::transferComplete(PRInt32 status){
	 SetConnected(false);
	 bool success = false;

	 switch(status){
	 case SashNet::NET_ABORTED:
		  DEBUGMSG(net, "transfer aborted.\n");
		  SetStatus(STATUS_CANCEL);
		  callURLEvent (m_onAbort);
		  break;
	 case SashNet::NET_DONE:
		  DEBUGMSG(net, "transfer completed.\n");
		  success = true;
		  break;
	 case SashNet::NET_ERROR:
		  DEBUGMSG(net, "transfer error.\n");
		  SetErrorMessage("The transfer could not be completed successfully.");
		  SetStatus(STATUS_FAIL);
		  break;
	 case SashNet::NET_ERROR_HOSTNAME_NOT_RESOLVED:
		  DEBUGMSG(net, "Error: The hostname could not be resolved.\n");
		  SetErrorMessage("The hostname could not be resolved.");
		  SetStatus(STATUS_FAIL);
		  break;
	 case SashNet::NET_ERROR_FILE_NOT_FOUND:
		  DEBUGMSG(net, "Error: The file was not found.\n");
		  SetErrorMessage("The file was not found.");
		  SetStatus(STATUS_FAIL);
		  break;
	 default:
		  DEBUGMSG(net, "Unknown SashNet constant\n");
		  SetErrorMessage("Unknown error.");
		  SetStatus(STATUS_FAIL);
		  break;
	 }

	 if (success){
		  SetStatus(STATUS_SUCCESS);
		  DEBUGMSG(net, "saving response headers...\n");
		  getResponseNamesAndValues();
		  
		  DEBUGMSG(net, "saving response body...\n");
		  getResponseBody();
	 }
	 
	 DEBUGMSG(net, "calling complete callback...\n");
	 callURLEvent (m_onComplete);
	 DEBUGMSG(net, "releasing this object\n");
	 sashURLConnection* a = this;
	 DEBUGMSG(net, "Refcount is %d\n", mRefCnt);
	 NS_RELEASE(a);
}

/* attribute string OnAbort; */
NS_IMETHODIMP sashURLConnection::GetOnAbort(char * *aOnAbort)
{
	 XP_COPY_STRING (m_onAbort.c_str(), aOnAbort);
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetOnAbort(const char * aOnAbort)
{
	 m_onAbort = aOnAbort;
	 return NS_OK;
}

/* attribute string OnComplete; */
NS_IMETHODIMP sashURLConnection::GetOnComplete(char * *aOnComplete)
{
	 XP_COPY_STRING (m_onComplete.c_str(), aOnComplete);
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetOnComplete(const char * aOnComplete)
{
	 m_onComplete = aOnComplete;
	 return NS_OK;
}

/* attribute string OnProgress; */
NS_IMETHODIMP sashURLConnection::GetOnProgress(char * *aOnProgress)
{
	 XP_COPY_STRING (m_onProgress.c_str(), aOnProgress);
	 return NS_OK;
}
NS_IMETHODIMP sashURLConnection::SetOnProgress(const char * aOnProgress)
{
	 m_onProgress = aOnProgress;
	 return NS_OK;
}

/* attribute string OnStart; */
NS_IMETHODIMP sashURLConnection::GetOnStart(char * *aOnStart)
{
	 XP_COPY_STRING (m_onStart.c_str(), aOnStart);
	 return NS_OK;
}

NS_IMETHODIMP sashURLConnection::SetOnStart(const char * aOnStart)
{
	 m_onStart = aOnStart;
	 return NS_OK;
}

void sashURLConnection::callURLEvent (string callback){
	 vector <nsIVariant *> args;
	 DEBUGMSG(net, "about to call event %s\n",
			  callback.c_str());

	 if (callback != "") {
		  nsIVariant * urlVariant;

		  NewVariant(&urlVariant);
		  VariantSetInterface(urlVariant, 
							  (nsISupports *)(sashIURLConnection *)this);
		  args.push_back(urlVariant);
								   
		  CallEvent(m_act, m_cx, 
					callback.c_str(), args);
		  NS_IF_RELEASE(urlVariant);
	 }
}

void sashURLConnection::callURLEvent (string callback, PRUint32 p,
									  PRUint32 pmax, PRUint32 timeElapsed){
	 vector <nsIVariant *> args;
	 DEBUGMSG(net, "about to call event %s\n",
			  callback.c_str());

	 if (callback != "") {
		  nsIVariant * urlVariant;
		  nsIVariant * pVariant;
		  nsIVariant * pmaxVariant;
		  nsIVariant * timeVariant;

		  NewVariant(&urlVariant);
		  VariantSetInterface(urlVariant, 
							  (nsISupports *)(sashIURLConnection *)this);
		  args.push_back(urlVariant);

		  NewVariant(&pVariant);
		  VariantSetNumber(pVariant, p);
		  args.push_back(pVariant);

		  NewVariant(&pmaxVariant);
		  VariantSetNumber(pmaxVariant, pmax);
		  args.push_back(pmaxVariant);

		  NewVariant(&timeVariant);
		  VariantSetNumber(timeVariant, timeElapsed);
		  args.push_back(timeVariant);
								   
		  CallEvent(m_act, m_cx, 
					callback.c_str(), args);
	 }
}

void sashURLConnection::fillInDLHeaders (
	 sashIURLConnectionHeaders *headers)
{ 
	 if (headers){
		  sashURLConnectionHeaders *h = (sashURLConnectionHeaders *)headers;
		  // iterate through headers, add each one.
		  unsigned int count = h->m_headers.size();
		  DEBUGMSG(net, "filling in %d headers\n", 
				   count);
		  for (unsigned int i=0; i<count; i++){
			   DEBUGMSG(net, "added header (%s, %s)...\n", 
 						h->m_headers[i].first.c_str(),
 						h->m_headers[i].second.c_str());

			   if (m_connectionType == GET){
					m_sdl->SetRequestHeader(h->m_headers[i].first.c_str(),
											h->m_headers[i].second.c_str());
			   } else {
					m_sul->SetRequestHeader(h->m_headers[i].first.c_str(),
											h->m_headers[i].second.c_str());

			   }
		  }
	 }
}

void sashURLConnection::getResponseNamesAndValues ()
{
	 PRUint32 numHeaders;
	 char *name;
	 char *value;
	 if (m_connectionType == GET){
		  m_sdl->GetNumResponseHeaders(&numHeaders);
		  DEBUGMSG(net, "getting %d headers\n", numHeaders);
		  for (PRUint32 i=0; i<numHeaders; i++){
			   m_sdl->GetResponseHeaderByIndex(i, &name, &value);
			   m_responseHNames.push_back(name);
			   m_responseHValues.push_back(value);
		  }
	 } else {
		  m_sul->GetNumResponseHeaders(&numHeaders);
		  for (PRUint32 i=0; i<numHeaders; i++){
			   m_sul->GetResponseHeaderByIndex(i, &name, &value);
			   m_responseHNames.push_back(name);
			   m_responseHValues.push_back(value);
		  }
	 }
}

void sashURLConnection::getResponseBody() {
	 m_response = "";
	 if ((m_connectionType == GET) && (m_saveResponseToFile)){
		  m_response = m_filename;
		  DEBUGMSG(net, "GET data saved in file: %s\n", m_response.c_str());
		  return;
	 }

	 PRUint32 size;
	 char *data;
	 if (m_connectionType == GET){
		  m_sdl->GetResponseData(&size, &data);
	 } else {
		  m_sul->GetResponseData(&size, &data);
	 }
	 if (data) {
		  m_response = data;
		  nsMemory::Free(data);
	 }
}

NS_IMPL_ISUPPORTS1(URLConnectionDownloadCallback, sashINetCallback);

NS_IMETHODIMP URLConnectionDownloadCallback::UpdateProgress(PRUint32 p, 
															PRUint32 pmax) 
{
	 DEBUGMSG(net, "callbackupdateprogress: %d/%d\n",
			  p, pmax);
	 if (m_URLConn->m_progressEventFrequency > 0){
		  int elapsedTime = time(NULL) - m_URLConn->m_startTime;
		  if (elapsedTime > m_URLConn->m_progressEventFrequency * m_URLConn->m_progressBlock){
			   m_URLConn->callURLEvent (m_URLConn->m_onProgress, p, pmax,
										elapsedTime);
			   m_URLConn->m_progressBlock ++;
		  }
/*
		  if (p > 
			  m_URLConn->m_progressEventFrequency * 
			  m_URLConn->m_progressBlock){
			   m_URLConn->callURLEvent (m_URLConn->m_onProgress, p, pmax);
			   m_URLConn->m_progressBlock ++;
		  }
*/
	 }
	 return NS_OK;
}

NS_IMETHODIMP URLConnectionDownloadCallback::UpdateStatus(const char * status)
{
	 DEBUGMSG(net, "callbackupdatestatus: %s\n",
			  status);
	 return NS_OK;
}

NS_IMETHODIMP URLConnectionDownloadCallback::UpdateComplete() { 
	 DEBUGMSG(net, "callbackupdatecomplete\n");
	 return NS_OK;
}

NS_IMETHODIMP URLConnectionDownloadCallback::UpdateAbort() { 
	 DEBUGMSG(net, "callbackupdateabort\n");
	 return NS_OK;
}
