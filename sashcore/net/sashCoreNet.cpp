
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for sashNet

*****************************************************************/

#include "sashCoreNet.h"
#include "sashIActionRuntime.h"
#include "nsIVariant.h"
#include "sashIExtension.h"
#include "sashVariantUtils.h"
#include "extensiontools.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "secman.h"
#include "sashIGenericConstructor.h"
#include "sashURLConnection.h"

SASH_EXTENSION_IMPL_NO_NSGET(sashCoreNet, sashICoreNet, "Net");

sashCoreNet::sashCoreNet()
{
	 DEBUGMSG(net, "sashCoreNet constructor\n");
	 DEBUGMSG(net, "sashCoreNet pointer: %d\n", (int)this);
	 NS_INIT_ISUPPORTS();
	 m_URLConnectionConstructor = NULL;

}

sashCoreNet::~sashCoreNet()
{
	 NS_RELEASE (m_URLConnectionConstructor);
	 pthread_mutex_destroy(&m_connLock);
}

NS_IMETHODIMP sashCoreNet::Cleanup() {
	 return NS_OK;
}

NS_IMETHODIMP sashCoreNet::ProcessEvent() {
	 // keep track of every URLConnection.
	 // iterate through each one, call ProcessAsyncEvent for each one.
	 // if ProcessAsyncEvent returns false, then drop it from the list.
	 DEBUGMSG(net, "sashCoreNet::ProcessEvent...\n");
	 vector<sashURLConnection *>::iterator a;

	 // WWW: may need to fix this.
	 while (anyAsyncConnections()){
		  DEBUGMSG(net, "processing async connections\n");

		  lockConnList();
		  a = m_connections.begin();
		  while (a != m_connections.end()){
			   if ((*a)->ProcessEvent()){
					m_connections.erase(a);
			   } else {
					a++;
			   }
		  }
		  unlockConnList();
	 }
	 return NS_OK;
}

NS_IMETHODIMP sashCoreNet::Initialize(sashIActionRuntime * rt, 
								  const char * registration,
								  const char * webGuid,
								  JSContext * jcx,
								  JSObject * jsObj)
{
	 DEBUGMSG(net, "Initializing net\n");
	 pthread_mutex_init(&m_connLock, NULL);
	 m_cx = jcx;
	 m_rt = rt;
	 rt->GetSecurityManager(&m_secMan);
	 assert(m_rt);
	 NewSashConstructor(m_rt, this, SASHURLCONNECTION_CONTRACT_ID, 
						SASHIURLCONNECTION_IID_STR,
						&m_URLConnectionConstructor);
	 return NS_OK;
}

/* readonly attribute sashIGenericConstructor URLConnection; */
NS_IMETHODIMP sashCoreNet::GetURLConnection(sashIGenericConstructor * *aURLConnection)
{
	 DEBUGMSG(net, "getting url connection constructor\n");
	 AssertSecurity(m_secMan, GSS_DOWNLOAD, true);

	 assert (m_URLConnectionConstructor);
	 NS_ADDREF(m_URLConnectionConstructor);
	 *aURLConnection = m_URLConnectionConstructor;
	 return NS_OK;
}

/* attribute string HomeServer; */
NS_IMETHODIMP sashCoreNet::GetHomeServer(char * *aHomeServer)
{
// 	 XP_COPY_STRING (m_homeServer.c_str(), aHomeServer);
// 	 return NS_OK;
	 return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP sashCoreNet::SetHomeServer(const char * aHomeServer)
{
// 	 m_homeServer = aHomeServer;
// 	 return NS_OK;
	 return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute PRBool InternetConnectState; */
NS_IMETHODIMP sashCoreNet::GetInternetConnectState(PRBool *aInternetConnectState)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP sashCoreNet::SetInternetConnectState(PRBool aInternetConnectState)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* PRBool CheckConnection (in string Server, in PRInt32 nPort, in PRInt32 nTimeOut); */
NS_IMETHODIMP sashCoreNet::CheckConnection(const char *Server, PRInt32 nPort, PRInt32 nTimeOut, PRBool *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* PRBool ConnectToInternet (in string Url); */
NS_IMETHODIMP sashCoreNet::ConnectToInternet(const char *Url, PRBool *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute string OnConnectStateChange; */
NS_IMETHODIMP sashCoreNet::GetOnConnectStateChange(char * *aOnConnectStateChange)
{
// 	 XP_COPY_STRING (m_onConnectStateChange.c_str(), aOnConnectStateChange);
// 	 return NS_OK;
	 return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP sashCoreNet::SetOnConnectStateChange(const char * aOnConnectStateChange)
{
// 	 DEBUGMSG(net, "setting connect state change callback\n");
// 	 m_onConnectStateChange = aOnConnectStateChange;
// 	 return NS_OK;
	 return NS_ERROR_NOT_IMPLEMENTED;
}

/* [noscript] void setCoreExt (in sashIExtension sComm); */
NS_IMETHODIMP sashCoreNet::SetCoreExt(sashIExtension *ext)
{
	 DEBUGMSG(net, "setting sashcore ext pointer\n");
	 m_sashCore = ext;
	 return NS_OK;
}

void sashCoreNet::addAsyncConnection (sashURLConnection *conn){
	 DEBUGMSG(net, "adding async connection to list.\n");
	 lockConnList();
	 m_connections.push_back (conn);
	 unlockConnList();

//	 DEBUGMSG(net, "calling rt->ProcessEvent()\n");
	 assert (m_rt);
	 m_rt->ProcessEvent((sashIExtension *)this);
//	 DEBUGMSG(net, "called rt->ProcessEvent()\n");
}

bool sashCoreNet::anyAsyncConnections (){
	 lockConnList();
	 bool retval = m_connections.empty();
	 unlockConnList();
	 
	 return (! retval);
}
