
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

implementation file for sashURLConnectionHeaders

*****************************************************************/
#include "sashURLConnectionHeaders.h"
#include "sashVariantUtils.h"
#include "extensiontools.h"
#include "nsCRT.h"

NS_IMPL_ISUPPORTS2_CI(sashURLConnectionHeaders, sashIURLConnectionHeaders, 
				    sashIConstructor);

sashURLConnectionHeaders::sashURLConnectionHeaders()
{
  NS_INIT_ISUPPORTS();
}

sashURLConnectionHeaders::~sashURLConnectionHeaders()
{
}

/* [noscript] void Initialize (in nativeJSContext cx); */
NS_IMETHODIMP sashURLConnectionHeaders::Initialize(JSContext * cx)
{
	 m_cx = cx;
	 return NS_OK;
}

NS_IMETHODIMP
sashURLConnectionHeaders::InitializeNewObject(sashIActionRuntime * actionRuntime,
											  sashIExtension * ext, 
											  JSContext * cx, 
											  PRUint32 argc, nsIVariant **argv, 
											  PRBool *ret)
{
	 *ret = true;
	 return NS_OK;
}

/* readonly attribute PRUint32 Count; */
NS_IMETHODIMP sashURLConnectionHeaders::GetCount(PRUint32 *aCount)
{
	 *aCount = m_headers.size();
	 return NS_OK;
}

/* void Clear (); */
NS_IMETHODIMP sashURLConnectionHeaders::Clear()
{
	 m_headers.clear();
	 return NS_OK;
}

/* string GetValue (in string HeaderName); */
NS_IMETHODIMP sashURLConnectionHeaders::GetValue(const char *HeaderName, char **_retval)
{
	 unsigned int numItems = m_headers.size();
	 for (unsigned int i=0; i < numItems; i++){
		  if (m_headers[i].first == HeaderName){
			   XP_COPY_STRING (m_headers[i].second.c_str(), _retval);
			   return NS_OK;
		  }
	 }
	 XP_COPY_STRING("", _retval);
	 return NS_OK;
}

/* string Name (in PRUint32 Index); */
NS_IMETHODIMP sashURLConnectionHeaders::Name(PRUint32 Index, char **_retval)
{
	 XP_COPY_STRING (invalidIndex(Index) ?
					   "" : m_headers[Index].first.c_str(),
					   _retval);
	 return NS_OK;
}

/* void Remove (in PRUint32 Index); */
NS_IMETHODIMP sashURLConnectionHeaders::Remove(PRUint32 Index)
{
	 if (! invalidIndex(Index)){
		  headermap::iterator a = m_headers.begin();
		  headermap::iterator b = a + Index;
		  m_headers.erase(b);
	 }
	 return NS_OK;
}

/* void SetValue (in string Name, in string Value); */
NS_IMETHODIMP sashURLConnectionHeaders::SetValue(const char *Name, const char *Value)
{
	 // check to see whether it exists already.
	 unsigned int numItems = m_headers.size();
	 for (unsigned int i=0; i < numItems; i++){
		  if (m_headers[i].first == Name){
			   m_headers[i].second = Value;
			   DEBUGMSG(net, "URLConnectionHeaders: header set (%s, %s)\n",
						Name, Value);
			   return NS_OK;
		  }
	 }
	 
	 // if it's not already there, add a new header entry.
	 pair<string, string> hEntry;
	 hEntry.first = Name;
	 hEntry.second = Value;
	 m_headers.push_back (hEntry);
	 DEBUGMSG(net, "URLConnectionHeaders: new header set (%s, %s)\n",
			  Name, Value);


	 return NS_OK;
}

/* string Value (in PRUint32 Index); */
NS_IMETHODIMP sashURLConnectionHeaders::Value(PRUint32 Index, char **_retval)
{
	 XP_COPY_STRING (invalidIndex(Index) ?
					   "" : m_headers[Index].second.c_str(),
					   _retval);
	 return NS_OK;
}

bool sashURLConnectionHeaders::invalidIndex(unsigned int index){
	 return (index >= m_headers.size());
}
