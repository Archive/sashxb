
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Wing Yung

header file for sashURLConnection

*****************************************************************/

#ifndef SASHURLCONNECTION_H
#define SASHURLCONNECTION_H

#include "nsID.h"
#include "sashIURLConnection.h"
#include "sashIURLConnectionHeaders.h"
#include "sashIConstructor.h"
#include "sashICoreNet.h"
#include "sashINet.h"
#include <string>
#include <vector>
#include <map>

// (f73848d0-5665-45e1-9b4c-ab719f73d355)
#define SASHURLCONNECTION_CID {0xf73848d0, 0x5665, 0x45e1, {0x9b, 0x4c, 0xab, 0x71, 0x9f, 0x73, 0xd3, 0x55}}

NS_DEFINE_CID(ksashURLConnectionCID, SASHURLCONNECTION_CID);

#define SASHURLCONNECTION_CONTRACT_ID "@gnome.org/SashXB/URLConnection;1"

typedef enum {
  STATUS_CANCEL,
  STATUS_FAIL,
  STATUS_NAME_NOT_RESOLVED,
  STATUS_PROGRESS,
  STATUS_SUCCESS,
  STATUS_TIMEOUT
} Status;

typedef enum {
	 GET,
	 POST
} ConnectionType;

class sashURLConnection : public sashIURLConnection,
						  public sashIConstructor
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHICONSTRUCTOR
  NS_DECL_SASHIURLCONNECTION


  sashURLConnection();
  virtual ~sashURLConnection();


private:
  void reinit();
  void transferComplete(PRInt32 status);

  pthread_mutex_t m_connectionLock;
  void lockConnection(){
	   pthread_mutex_lock(&m_connectionLock);
  }
  void unlockConnection(){
	   pthread_mutex_unlock(&m_connectionLock);
  }

  JSContext *m_cx;
  sashIExtension *m_ext;

  ConnectionType m_connectionType;
  Status m_status;
  bool m_connected;

  bool GetConnected(bool lock=true);
  void SetConnected(bool value, bool lock=true);

  int GetStatus(bool lock=true);
  void SetStatus(int value, bool lock=true);

  bool ProcessEvent();

  bool m_aborted;
  bool m_isAsync;

  string m_errorMessage;
  void SetErrorMessage(const string &message);

  string m_Error;
  PRInt32 m_progressEventFrequency;
  int m_progressBlock;
  string m_protocol;

  sashIURLConnectionHeaders *m_requestHeaders;
  sashIURLConnectionHeaders *m_responseHeaders;
  void fillInDLHeaders (sashIURLConnectionHeaders *headers);

  void getResponseNamesAndValues();
  void getResponseBody();

  vector<string> m_responseHNames;
  vector<string> m_responseHValues;

  string m_requestBody;
  string m_requestFile;
  map<string, string> m_requestFiles;
  map<string, string> m_requestParameters;
  string createParameterString();
  string createGetURL();

  bool m_forceMultiPart;

  string m_response;
  int m_responseCode;
  int m_retries;
  int m_timeout;

  bool m_saveResponseToFile;

  string m_URL;
  string m_userAgent;

  string m_onAbort;
  string m_onComplete;
  string m_onProgress;
  string m_onStart;

  void callURLEvent (string callback);
  void callURLEvent (string callback, PRUint32 p,
					 PRUint32 pmax,
					 PRUint32 timeElapsed);

  bool m_emptyResponseHeaders;

  nsCOMPtr<sashIDownloader> m_sdl;
  nsCOMPtr<sashIUploader> m_sul;
  sashINetCallback *m_dc;

  string m_filename;
  string m_tempfilename;

  friend class sashCoreNet;
  friend class URLConnectionDownloadCallback;
  nsCOMPtr<sashIActionRuntime> m_act;
  int m_startTime;
};

class URLConnectionDownloadCallback : public sashINetCallback {
public:
	 URLConnectionDownloadCallback(sashURLConnection *urlconn)
		  { NS_INIT_ISUPPORTS(); m_URLConn = urlconn; }
	 virtual ~URLConnectionDownloadCallback() {}

	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHINETCALLBACK

private:
	 friend class sashURLConnection;
	 sashURLConnection *m_URLConn;
};


#endif


