/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

Sash.Core implementation class definition

*****************************************************************/

#ifndef SASHCORE_H
#define SASHCORE_H

#include "nsID.h"
#include "nsIFactory.h"
#include "channels/sashIChannels.h"
#include "cursor/sashICursor.h"
#include "sashIExtension.h"
#include "sashISecurityManager.h"
#include "ui/sashIUI.h"
#include "net/sashICoreNet.h"
#include "platform/sashIPlatform.h"
#include "sashICore.h"

#include <string>

#define SASHCORE_CID {0x70bcbe85, 0x0567, 0x4ffc, \
                     {0x87, 0x9e, 0xcf, 0x31, 0xd1, 0x5f, 0xc5, 0xb4}}
    
NS_DEFINE_CID(ksashCoreCID, SASHCORE_CID);

#define SASHCORE_CONTRACT_ID "@gnome.org/SashXB/sashCore;1"

class sashCore : public sashICore,
				 public sashIExtension
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHICORE
  
  sashCore();
  virtual ~sashCore();
 private:

  JSContext * m_cx;
  JSObject * m_obj;
  sashIActionRuntime * m_rt;
  string m_registration;
  string m_webguid;
  sashISecurityManager * m_secMan;
  sashIChannelCollection * m_chanCol;
  sashICursor * m_cur;
  sashIProcess * m_proc;
  sashIUI* m_ui;
  sashICoreNet* m_net;
  sashIPlatform* m_platform;
};

#endif
