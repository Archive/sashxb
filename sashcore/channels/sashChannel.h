/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

Header file for the channels extension

*****************************************************************/

#ifndef SASHCHANNEL_H
#define SASHCHANNEL_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIChannels.h"
#include "sashIConstructor.h"
#include "SecurityManager.h"

#include "channel.h"
#include <string>

#define SASHCHANNEL_CID {0x257f1718, 0x9ec9, 0x4808, \
                        {0x9f, 0x28, 0x28, 0x44, 0x47, 0xbc, 0xbc, 0x99}}
    
NS_DEFINE_CID(ksashChannelCID, SASHCHANNEL_CID);

#define SASHCHANNEL_CONTRACT_ID "@gnome.org/SashXB/Channel;1"

class sashChannel : public sashIChannel
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHICHANNEL
  
  sashChannel();
  virtual ~sashChannel();
protected:

  sashIChannelCollection * m_chanCollection;

  nsCOMPtr<sashIChannelEvent> m_memberLeftEvent;
  nsCOMPtr<sashIChannelEvent> m_messageEvent;
  nsCOMPtr<sashIChannelEvent> m_newMemberEvent;

  string m_name;
  bool m_isPublic;
};

inline sashIChannel * NewSashChannel(sashIChannelCollection * chanCol, 
									 bool isPublic, const char *  name) {
	 sashIChannel * chan;

	 if (NS_SUCCEEDED(nsComponentManager::CreateInstance(ksashChannelCID, NULL, 
														NS_GET_IID(sashIChannel), 
														(void **) &chan))) {
		  chan->Initialize(chanCol, isPublic, name);
		  return chan;
	 }

	 return NULL;
}

#endif
