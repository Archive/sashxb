/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

Header file for the channels extension

*****************************************************************/

#ifndef SASHCHANNELMESSAGE_H
#define SASHCHANNELMESSAGE_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIChannels.h"
#include "sashIConstructor.h"
#include "SecurityManager.h"

#include "channel.h"
#include <string>

#define SASHCHANNELMESSAGE_CID {0x403f705b, 0xce1e, 0x40aa, \
                               {0x8f, 0x95, 0x08, 0x9b, 0xc0, 0xfa, 0x57, 0xe2}}
    
NS_DEFINE_CID(ksashChannelMessageCID, SASHCHANNELMESSAGE_CID);

#define SASHCHANNELMESSAGE_CONTRACT_ID "@gnome.org/SashXB/ChannelMessage;1"

class sashChannelMessage : public sashIChannelMessage,
						   public sashIConstructor
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHICONSTRUCTOR
  NS_DECL_SASHICHANNELMESSAGE
  
  sashChannelMessage();
  virtual ~sashChannelMessage();
 protected:

  int m_ID;
  string m_text;
  nsCOMPtr<sashIActionRuntime> m_act;
};

inline sashIChannelMessage * NewSashChannelMessage() {
	 sashChannelMessage * msg;

	 if (NS_SUCCEEDED(nsComponentManager::CreateInstance(ksashChannelMessageCID, NULL, 
														NS_GET_IID(sashIChannelMessage), 
														(void **)&msg))) {
		  return msg;
	 }

	 return NULL;
}

#endif
