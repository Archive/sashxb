#include "ChannelInfo.h"
#include "debugmsg.h"

int ChannelIdent::compare(gconstpointer a, gconstpointer b) {
  ChannelIdent * chanA = (ChannelIdent *) a;
  ChannelIdent * chanB = (ChannelIdent *) b;

  if (chanA->pub)
	return (chanB->pub && (strcmp(chanA->name, chanB->name) == 0));
  else
	return (!chanB->pub && (chanA->webguid.Equals(chanB->webguid)) && 
			(strcmp(chanA->name, chanB->name) == 0));
}

guint ChannelIdent::hash(gconstpointer c) {
  ChannelIdent * chan = (ChannelIdent *) c;

  return (g_str_hash(chan->name) +
		  g_str_hash(chan->webguid.ToString())) *
	((chan->pub + 1) * 2);
}

int channel_find(ChannelInfo * channels, ChannelIdent & ident) {
  for (int i = 0; i < NUM_CHANNELS; i++) {
	if (channels[i].compare(ident))
	  return i;
  }
  return -1;
}

int channel_find_empty(ChannelInfo * channels) {
  for (int i = 0; i < NUM_CHANNELS; i++) {
	if (channels[i].numListeners == 0)
	  return i;
  }
  return -1;
}

int channel_find_or_create(ChannelInfo * channels, ChannelIdent & ident) {
  int i, empty = -1;

  // find the requested channel and keep track of empty indices
  for (i = 0; i < NUM_CHANNELS; i++) {
	if (empty == -1 && channels[i].numListeners == 0)
	  empty = i;
	if (channels[i].compare(ident))
	  return i;
  }

  // the channel was not found -- create it
  if (empty != -1) {
	DEBUGMSG(channels, "Populating channel index %d\n", empty);
	channels[empty].ident.Initialize(ident);
  }

  return empty;
}

// ----- Message Group functions -----
MessageGroup::MessageGroup(const ChannelMessage & msg) {
  const char * msgStr = msg.message.c_str();

  // Set up the message header
  header.info.type = msg.info.type;
  header.info.sashMessageID = msg.info.sashMessageID;
  header.info.ident.Initialize(msg.info.ident);
  
  header.numPackets = msg.message.size() / MAX_MESSAGE_SIZE;
  if (msg.message.size() % MAX_MESSAGE_SIZE != 0)
	header.numPackets++;

  DEBUGMSG(channels, "New message group -- %d packets\n", header.numPackets);

  // create the message packets
  ChannelMessageData packet;
  for (int i = 0; i < header.numPackets; i++) {
	packet.packetIndex = i;
	strncpy(packet.text, &msgStr[MAX_MESSAGE_SIZE * i], MAX_MESSAGE_SIZE);
	packet.text[MAX_MESSAGE_SIZE] = '\0';
	DEBUGMSG(channels, "Packet %d of %d gets text %s\n", i + 1, header.numPackets,
			 packet.text);
	packets.push_back(packet);
  }
}

MessageGroup::MessageGroup(const ChannelMessageHeader & msgHeader) {
  header = msgHeader;
}

ChannelMessage MessageGroup::toMessage() {
  ChannelMessage msg;

  assert(header.numPackets == (int)packets.size());

  msg.info.type = header.info.type;
  msg.info.sashMessageID = header.info.sashMessageID;
  msg.info.ident.Initialize(header.info.ident);

  for (int i = 0; i < header.numPackets; i++) {
	DEBUGMSG(channels, "Packet text is %s\n", packets[i].text);
	msg.message += string(packets[i].text);
  }

  return msg;
}
