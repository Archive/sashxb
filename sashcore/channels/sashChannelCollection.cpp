/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

Implementation of the channels extension

*****************************************************************/

#include "sashIActionRuntime.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "secman.h"

#include "sashChannelCollection.h"
#include "sashChannel.h"
#include "sashChannelMessage.h"

#include "extensiontools.h"

SASH_EXTENSION_IMPL_NO_NSGET(sashChannelCollection, sashIChannelCollection, "Channels");
//NS_IMPL_ISUPPORTS2_CI(sashChannelCollection, sashIChannelCollection, sashIExtension);
//classname, interfacename, sashIExtension); 
//GET_SASH_NAME_IMPL("Channels");

sashChannelCollection::sashChannelCollection()
{
  NS_INIT_ISUPPORTS();
  DEBUGMSG(channels, "Creating channels extension\n");

  m_channels = NULL;
  m_chanHash = g_hash_table_new(ChannelIdent::hash, ChannelIdent::compare);

}

sashChannelCollection::~sashChannelCollection()
{
	 if (m_channels)
		  delete m_channels;
	 NS_RELEASE(m_messageConstructor);
	 DEBUGMSG(channels, "Returning from sashChannelCollection destructor\n");
}

NS_IMETHODIMP sashChannelCollection::Cleanup() {
	 return NS_OK;
}

NS_IMETHODIMP sashChannelCollection::ProcessEvent() {
  vector<ChannelMessage> messages = m_channels->GetMessageQueue();
  nsCOMPtr<sashIChannelEvent> callback;
  sashIChannel * destChannel;

  // for each message received...
  for (unsigned int i = 0; i < messages.size(); i++) {

	destChannel = (sashIChannel *) g_hash_table_lookup(m_chanHash, &messages[i].info.ident);
	if (destChannel) {
	  switch (messages[i].info.type) {
	  case SASH_CHANNEL_MESSAGE_MEMBER_LEFT: {
		   destChannel->GetOnMemberLeft(getter_AddRefs(callback));
		   if (callback)
				callback->Presence(destChannel);
		break;
	  }
	  case SASH_CHANNEL_MESSAGE_MEMBER_NEW: {
		   destChannel->GetOnNewMember(getter_AddRefs(callback));
		   if (callback)
				callback->Presence(destChannel);
		break;
	  }
	  case SASH_CHANNEL_MESSAGE_SEND: {
		   destChannel->GetOnMessage(getter_AddRefs(callback));
		   
		   if (callback) {
				nsCOMPtr<sashIChannelMessage> chanMsg = getter_AddRefs(NewSashChannelMessage());
				chanMsg->SetID(messages[i].info.sashMessageID);
				chanMsg->SetText(messages[i].message.c_str());
				
				callback->Message(chanMsg, destChannel);
		   }
		   break;
	  }
	  default:
		break;
	  }
	}
	else {
	  DEBUGMSG(channels, "Unknown channel (%s)\n", messages[i].info.ident.name);
	}
  }

  return NS_OK;
}

NS_IMETHODIMP sashChannelCollection::Initialize(sashIActionRuntime * rt, 
												const char * registration,
												const char * webGuid,
												JSContext * jcx,
												JSObject * jsObj)												
{
  DEBUGMSG(channels, "Initializing channels\n");


  m_rt = rt;
  m_cx = jcx;
  
  NewSashConstructor(m_rt, this, SASHCHANNELMESSAGE_CONTRACT_ID, SASHICHANNELMESSAGE_IID_STR,
					 &m_messageConstructor);

  rt->GetSecurityManager(&m_secMan);
  XP_GET_STRING(rt->GetSashBinaryPath, m_sashBinPath);
  m_webGuid.Parse(webGuid);
  m_channels = new Channels(m_sashBinPath, m_rt, this);
  
  return NS_OK;
}

/* sashIGenericConstructor Message (); */
NS_IMETHODIMP sashChannelCollection::GetMessage(sashIGenericConstructor **_retval)
{
  DEBUGMSG(channels, "Returning message constructor\n");
  NS_ADDREF(m_messageConstructor);
  *_retval = m_messageConstructor;
  return NS_OK;
}

/* sashIChannel Add (in string name, in nsIVariant public); */
NS_IMETHODIMP sashChannelCollection::Add(const char *name, nsIVariant * isPublic, sashIChannel **_retval)
{
	 bool publicChan = VariantGetBoolean(isPublic);
	 ChannelIdent chan;

	 if (publicChan)
	   AssertSecurity(m_secMan, GSS_CHANNEL_USE, true);

	 if (strcmp(name, "") == 0) {
	   *_retval = NULL;
	   return NS_OK;
	 }	   

	 DEBUGMSG(channels, "Creating %s channel %s\n", publicChan ? 
			  "public" : "private", name);

	 chan.Initialize(publicChan, m_webGuid, name);
	 if (m_channels->Add(chan)) {
	   *_retval = NewSashChannel(this, publicChan, name);

	   ChannelIdent * hashEntry = new ChannelIdent();
	   hashEntry->Initialize(publicChan, m_webGuid, name);
	   g_hash_table_insert(m_chanHash, hashEntry, *_retval);

	   // send a presence notification to the channel
	   m_channels->Send(chan, SASH_CHANNEL_MESSAGE_MEMBER_NEW, 0, NULL);
	 }

	 return NS_OK;
}

/* sashIChannel Item (in nsIVariant index, in nsIVariant public); */
NS_IMETHODIMP sashChannelCollection::Item(nsIVariant *index, 
										  nsIVariant *isPublic,
										  sashIChannel **_retval)
{
  bool pub = VariantGetBoolean(isPublic, true);
  string name;
  
  *_retval = NULL;


  if (pub)
	AssertSecurity(m_secMan, GSS_CHANNEL_USE, true);


  // look for the name of the given channel index
  if (VariantIsNumber(index)) {
	vector<string> names = m_channels->EnumerateSubscribed(pub, m_webGuid);
	int chanIndex = (int)VariantGetNumber(index, -1);

	if (chanIndex >= 0 && chanIndex < (int)names.size()) {
	  name = names[chanIndex];
	}
  }
  else 
	name = VariantGetString(index, "");

  ChannelIdent ident;
  if (name != "") {
	ident.Initialize(pub, m_webGuid, name.c_str());
	*_retval = (sashIChannel *) g_hash_table_lookup(m_chanHash, &ident);
	if (*_retval)
	  NS_ADDREF(*_retval);
  }

  return NS_OK;
}

/* nsIVariant List (in nsIVariant public); */
NS_IMETHODIMP sashChannelCollection::List(nsIVariant * isPublic, nsIVariant **_retval)
{
  DEBUGMSG(channels, "Listing channels\n");

  bool pub = VariantGetBoolean(isPublic);

  if (pub)
	AssertSecurity(m_secMan, GSS_CHANNEL_USE, true);

  vector<string> names = m_channels->Enumerate(pub, m_webGuid);

  NewVariant(_retval);
  VariantSetArray(*_retval, names);

  return NS_OK;
}

/* PRBool Remove (in nsIVariant index, in nsIVariant public); */
NS_IMETHODIMP sashChannelCollection::Remove(nsIVariant *index, nsIVariant * isPublic, PRBool *_retval)
{
  sashIChannel * chan;
  ChannelIdent ident, * orig_key;
  bool pub;
  string name;
  *_retval = false;

  pub = VariantGetBoolean(isPublic, true);

  if (pub)
	AssertSecurity(m_secMan, GSS_CHANNEL_USE, true);

  // look for the name of the given channel index
  if (VariantIsNumber(index)) {
	vector<string> names = m_channels->EnumerateSubscribed(pub, m_webGuid);
	int chanIndex = (int)VariantGetNumber(index, -1);

	if (chanIndex >= 0 && chanIndex < (int)names.size()) {
	  name = names[chanIndex];
	}
  }
  else 
	name = VariantGetString(index, "");

  ident.Initialize(pub, m_webGuid, name.c_str());
  if (g_hash_table_lookup_extended(m_chanHash, &ident, (void **) &orig_key, 
								   (void **) &chan)) {
	g_hash_table_remove(m_chanHash, &ident);
	delete orig_key;
	*_retval = true;

	// send a presence notification to the channel
	m_channels->Send(ident, SASH_CHANNEL_MESSAGE_MEMBER_LEFT, 0, NULL);
  }

  return NS_OK;
}

NS_IMETHODIMP sashChannelCollection::SendMessage(PRBool isPublic, const char *name, 
												 PRInt32 ID, const char *text)
{
  ChannelIdent chan;

  if (isPublic)
	AssertSecurity(m_secMan, GSS_CHANNEL_USE, true);

  chan.Initialize(isPublic, m_webGuid, name);
  if (m_channels->Send(chan, SASH_CHANNEL_MESSAGE_SEND, ID, text))
	return NS_OK;
  else
	return NS_ERROR_FAILURE;
}

/* [noscript] PRInt32 MemberCount (in PRBool isPublic, in string name); */
NS_IMETHODIMP sashChannelCollection::MemberCount(PRBool isPublic, const char *name, PRInt32 *_retval)
{
  ChannelIdent chan;

  if (isPublic)
	AssertSecurity(m_secMan, GSS_CHANNEL_USE, true);

  chan.Initialize(isPublic, m_webGuid, name);
  *_retval = m_channels->MemberCount(chan);
  return NS_OK;
}
