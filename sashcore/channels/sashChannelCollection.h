/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

Header file for the channels extension

*****************************************************************/

#ifndef SASHCHANNELCOLLECTION_H
#define SASHCHANNELCOLLECTION_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIChannels.h"
#include "sashIExtension.h"
#include "SecurityManager.h"
#include "secman.h"

#include "channel.h"
#include <string>

class Channels;
class sashIGenericConstructor;

#define SASHCHANNELCOLLECTION_CID {0xa77e149a, 0x4d18, 0x45f0, \
                                  {0x9c, 0x91, 0x66, 0x3a, 0xf8, 0xca, 0x6b, 0x67}}
    
NS_DEFINE_CID(ksashChannelCollectionCID, SASHCHANNELCOLLECTION_CID);

#define SASHCHANNELCOLLECTION_CONTRACT_ID "@gnome.org/SashXB/ChannelCollection;1"

class sashChannelCollection : public sashIChannelCollection
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHICHANNELCOLLECTION
  
  sashChannelCollection();
  virtual ~sashChannelCollection();
 private:

  Channels * m_channels;
  string m_sashBinPath;

  GHashTable * m_chanHash;

  JSContext * m_cx;
  sashISecurityManager * m_secMan;
  sashIGenericConstructor * m_messageConstructor;
  sashIActionRuntime * m_rt;
  nsIID m_webGuid;
};

#endif
