#include "channel.h"
#include "debugmsg.h"
#include "sash_error.h"
#include "extensiontools.h"

void * channel_start(void * chan) {
	 Channels * channel = (Channels *) chan;

	 DEBUGMSG(channels, "Listening for messages\n");
	 
	 channel->channel_listen();
	 return NULL;
}

Channels::Channels(string binPath, sashIActionRuntime * rt, sashIChannelCollection * chanExt) {
  bool clearShm = false;

     m_rt = rt;
	 m_chanExt = chanExt;
	 threadExit = false;
	 m_msgHash = g_hash_table_new(g_str_hash, g_str_equal);

	 m_chanKey = ftok(binPath.c_str(), 'C') + 1;
	 DEBUGMSG(channels, "Binary path is %s\n", binPath.c_str());
	 DEBUGMSG(channels, "Channel key is %x\n", m_chanKey);

	 // Get the channel semaphore, creating if necessary
	 DEBUGMSG(channels, "Creating semaphore with permissions %d (%x hex)\n",
			  0666 | IPC_CREAT | IPC_EXCL, 0666| IPC_CREAT | IPC_EXCL);
	 m_semID = semget(m_chanKey, 1, 0666 | IPC_CREAT | IPC_EXCL);
	 if (m_semID == -1) {
		  DEBUGMSG(channels, "Semaphore for key %d already exists\n", m_chanKey);
		  m_semID = semget(m_chanKey, 1, 0666);
	 }
	 else {
		  // set the semaphore value to 1
	   if (semctl(m_semID, 0, SETVAL, 1) == -1) {
		 perror("Channel Error");
		 OutputError("Unable to set semaphore value!");
	   }
	 }

	 DEBUGMSG(channels, "Got semaphore %d\n", m_semID);
	 if (m_semID == -1) {
	      perror("Channel Error");
		  OutputError("Sash channels unable to get a semaphore!");
	 }
	 
	 // Get the channel shared memory, creating if necessary
	 DEBUGMSG(channels, "Attempting to allocate %d bytes of shared mem\n", CHANNEL_MEMORY_SIZE);
	 m_shmID = shmget(m_chanKey, CHANNEL_MEMORY_SIZE, 0666 | IPC_CREAT | IPC_EXCL);
	 if (m_shmID == -1) {
	   DEBUGMSG(channels, "Shared memory already exists\n");
	   m_shmID = shmget(m_chanKey, CHANNEL_MEMORY_SIZE, 0666);
	   if (m_shmID == -1)
		 OutputError("Sash channels unable to get shared memory block!");
	 } else {
	   clearShm = true;
	 }

	 m_channels = (ChannelInfo *) shmat(m_shmID, NULL, 0);
	 if ((int)m_channels == -1)
		  OutputError("Sash channels unable to attach to shared memory block!");

	 if (clearShm)
	   memset(m_channels, 0, CHANNEL_MEMORY_SIZE);

	 // Create the channel message queue
	 m_queue = msgget(IPC_PRIVATE, 0666 | IPC_CREAT);
	 if (m_queue == -1)
		  OutputError("Sash channels unable to create a message queue!");
	 DEBUGMSG(channels, "Program message queue ID = %d\n", m_queue);

	 pthread_mutex_init(&m_mutex, 0);

	 m_threadID = pthread_create(&msg_thread, 0, channel_start, this);
	 if (m_threadID == -1)
		  OutputError("Sash channels unable to create a message receiving thread!");	 
}

Channels::~Channels() {

  DEBUGMSG(channels, "Destroying channels\n");

  // tell the listener thread to stop executing
  pthread_mutex_lock(&m_mutex);
  {
	threadExit = true;
  }
  pthread_mutex_unlock(&m_mutex);

  // destroy the channel message queue
  msgctl(m_queue, IPC_RMID, NULL);

  pthread_join(m_threadID, NULL);

  DEBUGMSG(channels, "Returning from Channels destructor\n");
}

void Channels::sem_lock() {
	 struct sembuf sop;

	 sop.sem_num = 0;
	 sop.sem_op = -1;
	 sop.sem_flg = 0;

	 // lock the semaphore
	 if (semop(m_semID, &sop, 1) == -1)
		  OutputError("Unable to lock channel semaphore!");

	 DEBUGMSG(channels, "--- Semaphore locked ---\n");
}

void Channels::sem_unlock() {
	 struct sembuf sop;

	 sop.sem_num = 0;
	 sop.sem_op = 1;
	 sop.sem_flg = 0;

	 // unlock the semaphore
	 if (semop(m_semID, &sop, 1) == -1)
		  OutputError("Unable to unlock channel semaphore!");

	 DEBUGMSG(channels, "--- Semaphore unlocked ---\n");
}

void Channels::ProcessPacket(const ChannelMessagePacket & packet) {
  MessageGroup * group;
  ChannelMessage msg;
  bool messageCompleted = false;

  if (packet.packetType == CHANNEL_PACKET_HEADER) {
	if (packet.header.numPackets == 0) {
	  // no need to assemble a message
	  MessageGroup singleton(packet.header);
	  msg = singleton.toMessage();
	  messageCompleted = true;
	}
	else {
	  group = new MessageGroup(packet.header);
	  g_hash_table_insert(m_msgHash, g_strdup(packet.message_id.ToString()), group);
	}
  }
  else if (packet.packetType == CHANNEL_PACKET_DATA) {
	group = (MessageGroup *) g_hash_table_lookup(m_msgHash, packet.message_id.ToString());
	group->packets.push_back(packet.data);
	if ((int)group->packets.size() == group->header.numPackets) {

	  // the message is complete
	  msg = group->toMessage();

	  // remove the group from the message hash
	  char * key;
	  if (g_hash_table_lookup_extended(m_msgHash, packet.message_id.ToString(),
									   (void **) &key, (void **) &group) != TRUE) {
		printf("Channels are broken\n");
		return;
	  }
	  free(key);
	  delete group;

	  messageCompleted = true;
	}
  }

  if (messageCompleted) {
	pthread_mutex_lock(&m_mutex);
	{
	  m_msgQueue.push_back(msg);
	}
	pthread_mutex_unlock(&m_mutex);
	
	m_rt->ProcessEvent((sashIExtension *) m_chanExt);
  }
  
}

void Channels::channel_listen() {
	 ChannelMessagePacket packet;
	 bool keepRunning = true;

	 while (keepRunning) {

	   // get the next message from the queue 
	   DEBUGMSG(channels, "About to call msgrcv on channel %d\n", m_queue);
	   if (msgrcv(m_queue, &packet, sizeof(ChannelMessagePacket), 0, 0) == -1) {
		 if (errno == EINTR)
		   continue;
		 perror("Channel Error");
	   }
	   else {
		 ProcessPacket(packet);
	   }

	   // see if the thread should terminate
	   pthread_mutex_lock(&m_mutex);
	   {
		 if (threadExit)
		   keepRunning = false;
	   }
	   pthread_mutex_unlock(&m_mutex);

	 }
}

vector<ChannelMessage> Channels::GetMessageQueue() {
  vector<ChannelMessage> queue;

  pthread_mutex_lock(&m_mutex);
  {
	queue = m_msgQueue;
	m_msgQueue.clear();
  }
  pthread_mutex_unlock(&m_mutex);

  return queue;
}

bool Channels::Add(ChannelIdent & ident) {
	 bool success = false;

	 sem_lock(); 
	 {
		  int chan = channel_find_or_create(m_channels, ident);
		  if (chan >= 0) {
			   success = m_channels[chan].addID(m_queue);
		  }
	 }
	 sem_unlock();

	 return success;
}

bool Channels::Remove(ChannelIdent & ident) {
	 bool success = false;

	 sem_lock(); 
	 {
		  int chan = channel_find(m_channels, ident);
		  if (chan >= 0) {
			   success = m_channels[chan].removeID(m_queue);
		  }
	 }
	 sem_unlock();

	 return success;
}

bool Channels::SendMessageGroup(int queue_id, const MessageGroup & m) {
  ChannelMessagePacket packet;

  packet.ipc_type_field = 1;

  // generate a unique ID for this message
  string guid;
  XP_GET_STRING(m_rt->GenerateGUID, guid);
  packet.message_id.Parse(guid.c_str());

  // send the message header
  packet.packetType = CHANNEL_PACKET_HEADER;
  packet.header = m.header;
  if (!packet.Send(queue_id))
	return false; // failed to send the message

  // send the message data
  packet.packetType = CHANNEL_PACKET_DATA;
  for (int i = 0; i < (int)m.packets.size(); i++) {
	packet.data = m.packets[i];
	if (!packet.Send(queue_id))
	  return false; // failed to send the message
  }

  return true;
}

bool Channels::Send(ChannelIdent & ident, 
					int messageType, 
					int messageID, 
					const char * message) {
  DEBUGMSG(channels, "** Sending Message (%d, %s) on channel (%s, %s, %s)\n",
		   messageID, 
		   message, 
		   ident.pub ? "public" : "private", 
		   ident.webguid.ToString(),
		   ident.name);

	 bool success = false;
	 int chan, i;
	 vector<int> listeners;

	 // Get the list of listeners on this channel
	 sem_lock();
	 {
		  chan = channel_find(m_channels, ident);
		  if (chan >= 0) {
			for (i = 0; i < m_channels[chan].numListeners; i++) {
			  listeners.push_back(m_channels[chan].listeners[i]);
			}
		  }
	 }
	 sem_unlock();

	 if (chan == -1) {
	   DEBUGMSG(channels, "Unable to find channel (%s, %s)\n", 
				ident.pub ? "public" : "private", ident.name);
	 }
	 else {
	   
	   ChannelMessage msg;
	   
	   msg.info.type = messageType;
	   msg.info.sashMessageID = messageID;
	   msg.info.ident.Initialize(ident);
	   if (message)
		 msg.message = message;
	   else
		 msg.message = "";
	   
	   MessageGroup msgGroup(msg);
	   
	   // send the message to each listener on the channel
	   for (i = 0; i < (int)listeners.size(); i++) {
		 DEBUGMSG(channels, "sending message to channel %d\n", listeners[i]);
		 if (SendMessageGroup(listeners[i], msgGroup))
		   success = true;
	   }
	 }

	 return success;
}

vector<string> Channels::Enumerate(bool pub, nsIID & webguid) {
	 vector<string> names;

	 sem_lock();
	 {
	   for (int i = 0; i < NUM_CHANNELS; i++) {
		 if (pub == m_channels[i].ident.pub && m_channels[i].numListeners > 0) 
		   if (pub || (!pub && webguid.Equals(m_channels[i].ident.webguid))) {
			 DEBUGMSG(channels, "Matched channel index %d\n", i);
			 names.push_back(m_channels[i].ident.name);
		   }
	   }
	 }
	 sem_unlock();

	 return names;
}

vector<string> Channels::EnumerateSubscribed(bool pub, nsIID & webguid) {
	 vector<string> names;

	 sem_lock();
	 {
	   for (int i = 0; i < NUM_CHANNELS; i++) {
		 if (pub == m_channels[i].ident.pub && m_channels[i].numListeners > 0) 
		   if (pub || (!pub && webguid.Equals(m_channels[i].ident.webguid))) {
			 if (m_channels[i].checkID(m_queue)) {
			   DEBUGMSG(channels, "Matched channel index %d\n", i);
			   names.push_back(m_channels[i].ident.name);
			 }
		   }
	   }
	 }
	 sem_unlock();

	 return names;
}

int Channels::ChannelCount() {
	 int count = 0;

	 sem_lock();
	 {
		  for (int i = 0; i < NUM_CHANNELS; i++)
			   if (m_channels[i].numListeners > 0)
					count++;
	 }
	 sem_unlock();

	 return count;
}

int Channels::MemberCount(ChannelIdent & ident) {
  int members = 0, index;

  sem_lock();
  {
	index = channel_find(m_channels, ident);
	if (index >= 0)
	  members = m_channels[index].numListeners;
  }
  sem_unlock();

  return members;
}

