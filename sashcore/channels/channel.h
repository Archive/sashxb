#ifndef CHANNEL_H
#define CHANNEL_H

#include <vector>
#include <string>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/msg.h>

#include "ChannelInfo.h"
#include "sashIActionRuntime.h"

void * channel_start(void * chan);

class sashIChannelCollection;

class Channels {
protected:
     key_t m_chanKey; // The key used for all IPC operations
	 int m_semID;     // Semaphore ID
	 int m_shmID;     // Shared memory ID
	 int m_queue;     // Message queue ID
	 ChannelInfo * m_channels;  // pointer to the channels shared memory
	 pthread_t msg_thread;      // thread waiting for channel messages

	 vector<ChannelMessage> m_msgQueue; // messages received

	 sashIActionRuntime * m_rt;
	 sashIChannelCollection * m_chanExt;

	 pthread_mutex_t m_mutex;  // lock this before using variables common
	                           // to both threads
	 int m_threadID;
	 bool threadExit;      // set to true if the thread should exit

	 void sem_lock();    // lock the shared memory
	 void sem_unlock();  // unlock the shared memory

	 GHashTable * m_msgHash;

	 void channel_listen();

	 bool SendMessageGroup(int queue_id, const MessageGroup & m);
	 void ProcessPacket(const ChannelMessagePacket & packet);
	 static string GenerateGUID();

public:
	 Channels(string binPath, sashIActionRuntime * rt, sashIChannelCollection * chanExt);
	 ~Channels();

	 bool Add(ChannelIdent & ident);
	 bool Remove(ChannelIdent & ident);
	 bool Send(ChannelIdent & ident, int messageType, int messageID, const char * message);

	 vector<string> Enumerate(bool pub, nsIID & webguid);
	 vector<string> EnumerateSubscribed(bool pub, nsIID & webguid);

	 vector<ChannelMessage> GetMessageQueue();
	 int ChannelCount();  // # of active channels
	 int MemberCount(ChannelIdent & ident); // # of listeners on a channel

	 friend void * channel_start(void * chan);
};

#endif
