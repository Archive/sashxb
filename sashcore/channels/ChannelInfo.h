#ifndef CHANNEL_INFO_H
#define CHANNEL_INFO_H

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <glib.h>
#include <nsISupports.h>

#include <sys/msg.h>
#include <errno.h>

#include "debugmsg.h"

#define NUM_CHANNELS 64
#define CHANNEL_NAME_SIZE 40
#define CHANNEL_MAX_LISTENERS 50

#define CHANNEL_MEMORY_SIZE (NUM_CHANNELS * sizeof(ChannelInfo))
//#define MAX_MESSAGE_SIZE (1024 - sizeof(ChannelMessageInfo))
#define MAX_MESSAGE_SIZE (512)

#define SASH_CHANNEL_MESSAGE_ERROR 0
#define SASH_CHANNEL_MESSAGE_MEMBER_LEFT 1
#define SASH_CHANNEL_MESSAGE_MEMBER_NEW 2
#define SASH_CHANNEL_MESSAGE_SEND 3

// These structures will be placed directly in shared memory, so
//   make sure that they don't have a vtable or require constructors

// ChannelIdent: information to define a channel
struct ChannelIdent {
  bool pub;                       // public channel?
  nsIID webguid;                  // the weblication guid of a private channel
  char name[CHANNEL_NAME_SIZE];   // the name of the channel

  void Initialize(bool isPublic, nsIID & a_webguid, const char * a_name) {
	pub = isPublic;
	webguid = a_webguid;
	strncpy(name, a_name, CHANNEL_NAME_SIZE - 1);
  }

  // Initialize: ChannelIdent -> void. Effectively a copy constructor
  void Initialize(const ChannelIdent & ident) {
	pub = ident.pub;
	webguid = ident.webguid;
	strncpy(name, ident.name, CHANNEL_NAME_SIZE - 1);
  }

  static int compare(gconstpointer a, gconstpointer b);
  static guint hash(gconstpointer c);
};

struct ChannelInfo {
     ChannelIdent ident;
	 int numListeners;
	 int listeners[CHANNEL_MAX_LISTENERS];  // message queue IDs of listeners in this channel

	 // compare: does the given info match this channel?
	 bool compare(ChannelIdent & other) const {
		  if (ident.pub) 
			   return (other.pub && (strcmp(ident.name, other.name) == 0));
		  else
			   return (!other.pub && (ident.webguid.Equals(other.webguid)) && 
					   (strcmp(ident.name, other.name) == 0));
	 }

	 // addID: add a message queue ID to this channel
	 bool addID(int id) {
		  for (int i = 0; i < numListeners; i++) {
			   if (listeners[i] == id)
					return true;
		  }
		  if (numListeners == CHANNEL_MAX_LISTENERS) 
			   return false;
		  listeners[numListeners] = id;
		  numListeners++;
		  return true;
	 }

	 // removeID: remove a message queue ID from this channel
	 bool removeID(int id) {
		  for (int i = 0; i < numListeners; i++) {
			   if (listeners[i] == id) {
					listeners[i] = listeners[numListeners - 1];
					numListeners--;
					return true;
			   }
		  }
		  return false;
	 }

     // checkID: return true if the given message queue ID is a registered listener
     bool checkID(int id) {
	   for (int i = 0; i < numListeners; i++) {
		 if (listeners[i] == id) {
		   return true;
		 }
	   }
	   return false;
     }
};


// ---------- Message structures ---------------
struct ChannelMessageInfo {
  int type;
  int sashMessageID;

  ChannelIdent ident;
};

// ----- ChannelMessage: the fully composed message -----
struct ChannelMessage {
  ChannelMessageInfo info;
  string message;
};

// ----- Message Packets -----
enum ChannelPacketType {
  CHANNEL_PACKET_HEADER,
  CHANNEL_PACKET_DATA
};

struct ChannelMessageHeader {
  ChannelMessageInfo info;
  int numPackets;
};

struct ChannelMessageData {
     int packetIndex;
     char text[MAX_MESSAGE_SIZE + 1]; // leave an extra byte for the null terminator
};

struct ChannelMessagePacket {
  int ipc_type_field;
  nsID message_id;  // A unique identifier for this message group.
                    // Message packets that are part of the same message
                    // have the same message_id.

  ChannelPacketType packetType;
  union {
	ChannelMessageHeader header;
	ChannelMessageData data;
  };

  bool Send(int queue_id) {
	// send the packet

	while (msgsnd(queue_id, this, sizeof(ChannelMessagePacket), 0) == -1) {
	  DEBUGMSG(channels, "Unable to send packet -- %s\n", strerror(errno));
	  if (errno != EINTR)
		return false; // failed to send the message
	}
	return true;
  }
};

struct MessageGroup {
  ChannelMessageHeader header;
  vector<ChannelMessageData> packets;

  // MessageGroup constructor: create a message group from a message
  MessageGroup(const ChannelMessage & msg);

  // create a message group from a message header
  MessageGroup(const ChannelMessageHeader & msgHeader);

  // toMessage: combine the message packets into the final message
  ChannelMessage toMessage();
};

// ---------- Channel utilities ---------------

// channel_find: find the index of the channel matching the given info.
//               Returns -1 if the channel does not exist.
int channel_find(ChannelInfo * channels, ChannelIdent & ident);

// channel_find_empty: find the index of an unused channel.
//                     Returns -1 on failure.
int channel_find_empty(ChannelInfo * channels);

// channel_find_or_create: find the index of the channel matching the given info
//    if the channel does not exist, return the index of an empty channel.
//    Returns -1 on failure.
int channel_find_or_create(ChannelInfo * channels, ChannelIdent & ident);

#endif
