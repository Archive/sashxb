/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

Implementation of the channels extension

*****************************************************************/

#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "secman.h"

#include "sashChannelMessage.h"

NS_IMPL_ISUPPORTS2_CI(sashChannelMessage, sashIChannelMessage, sashIConstructor);

sashChannelMessage::sashChannelMessage() 
{
	 NS_INIT_ISUPPORTS();
	 DEBUGMSG(channels, "Creating new channel message\n");
}

sashChannelMessage::~sashChannelMessage()
{
}


NS_IMETHODIMP
sashChannelMessage::InitializeNewObject(sashIActionRuntime * act,
										sashIExtension * ext, JSContext * cx, 
										PRUint32 argc, nsIVariant **argv, PRBool *ret)
{
  DEBUGMSG(channels, "Initializing channel message\n");
  if (argc >= 1)
	m_ID = (int) VariantGetNumber(argv[0], 0);
  if (argc >= 2)
	m_text = VariantGetString(argv[1], "");

  m_act = act;
  *ret = true;
  return NS_OK;
}

/* attribute PRInt32 ID; */
NS_IMETHODIMP sashChannelMessage::GetID(PRInt32 *aID)
{
	 *aID = m_ID;
	 return NS_OK;
}

NS_IMETHODIMP sashChannelMessage::SetID(PRInt32 aID)
{
	 m_ID = aID;
	 return NS_OK;
}

/* attribute PRInt32 Text; */
NS_IMETHODIMP sashChannelMessage::GetText(char ** aText)
{
	 XP_COPY_STRING(m_text.c_str(), aText);
	 return NS_OK;
}

NS_IMETHODIMP sashChannelMessage::SetText(const char * aText)
{
	 m_text = aText;
	 return NS_OK;
}
