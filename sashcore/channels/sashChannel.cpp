/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

Implementation of the channel class

*****************************************************************/

#include "sashIActionRuntime.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "secman.h"

#include "sashChannel.h"

NS_IMPL_ISUPPORTS1_CI(sashChannel, sashIChannel);

sashChannel::sashChannel() 
{
  NS_INIT_ISUPPORTS();
}

NS_IMETHODIMP sashChannel::Initialize(sashIChannelCollection *chCol, 
									  PRBool isPublic, const char *name) {
  m_chanCollection = chCol;
  m_isPublic = isPublic;
  m_name = name;

  return NS_OK;
}

sashChannel::~sashChannel()
{
}

/* attribute string OnMemberLeft; */
NS_IMETHODIMP sashChannel::GetOnMemberLeft(sashIChannelEvent * *aOnMemberLeft)
{
	 *aOnMemberLeft = m_memberLeftEvent;
	 NS_ADDREF(*aOnMemberLeft);
	 return NS_OK;
}

NS_IMETHODIMP sashChannel::SetOnMemberLeft(sashIChannelEvent * aOnMemberLeft)
{
	 m_memberLeftEvent = aOnMemberLeft;
	 return NS_OK;
}

/* attribute string OnMessage; */
NS_IMETHODIMP sashChannel::GetOnMessage(sashIChannelEvent * *aOnMessage)
{
	 *aOnMessage = m_messageEvent;
	 NS_ADDREF(*aOnMessage);
	 return NS_OK;
}

NS_IMETHODIMP sashChannel::SetOnMessage(sashIChannelEvent * aOnMessage)
{
	 m_messageEvent = aOnMessage;
	 return NS_OK;
}

/* attribute string OnNewMember; */
NS_IMETHODIMP sashChannel::GetOnNewMember(sashIChannelEvent * *aOnNewMember)
{
	 *aOnNewMember = m_newMemberEvent;
	 NS_ADDREF(*aOnNewMember);
	 return NS_OK;
}

NS_IMETHODIMP sashChannel::SetOnNewMember(sashIChannelEvent * aOnNewMember)
{
	 m_newMemberEvent = aOnNewMember;
	 return NS_OK;
}

/* PRBool SendMessage (in sashIChannelMessage msg); */
NS_IMETHODIMP sashChannel::SendMessage(sashIChannelMessage *msg, PRBool *_retval)
{
	 int ID;
	 string text;

	 XP_GET_STRING(msg->GetText, text);
	 msg->GetID(&ID);

	 *_retval = NS_SUCCEEDED(m_chanCollection->SendMessage(m_isPublic, m_name.c_str(), ID, text.c_str()));

	 return NS_OK;
}

/* readonly attribute PRInt32 MemberCount; */
NS_IMETHODIMP sashChannel::GetMemberCount(PRInt32 *aMemberCount)
{
	 return m_chanCollection->MemberCount(m_isPublic, m_name.c_str(), aMemberCount);
}

/* readonly attribute string Name; */
NS_IMETHODIMP sashChannel::GetName(char * *aName)
{
	 XP_COPY_STRING(m_name.c_str(), aName);
	 return NS_OK;
}

/* readonly attribute PRBool Public; */
NS_IMETHODIMP sashChannel::GetPublic(PRBool *aPublic)
{
	 *aPublic = m_isPublic;
	 return NS_OK;
}
