/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Implementation of the platform class

*****************************************************************/

#include "sashIActionRuntime.h"
#include "extensiontools.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "SashPlatform.h"
#include <sys/utsname.h>
#include <stdlib.h>

NS_IMPL_ISUPPORTS1_CI(SashPlatform, sashIPlatform);

SashPlatform::SashPlatform() {
  NS_INIT_ISUPPORTS();
}

SashPlatform::~SashPlatform() { }

NS_IMETHODIMP SashPlatform::GetOS(char** val) {
	 struct utsname u;
	 if (uname(&u)) return NS_ERROR_UNEXPECTED;
	 XP_COPY_STRING(u.sysname, val);
	 return NS_OK;
}


NS_IMETHODIMP SashPlatform::GetShell(char** val) {
	 const char* a = getenv("SHELL");
	 if (a == NULL) {
		  a = "";
	 }
	 XP_COPY_STRING(a, val);
	 return NS_OK;
}

