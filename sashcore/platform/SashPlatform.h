/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Header file for the Core.Platform extension

*****************************************************************/

#ifndef SASHPLATFORM_H
#define SASHPLATFORM_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIPlatform.h"
#include "SecurityManager.h"
#define SASHPLATFORM_CID {0x70e0fc6f, 0x8672, 0x4ed4, {0x8b, 0xa7, 0xac, 0x1b, 0x37, 0xd9, 0x7c, 0x67}}
    
NS_DEFINE_CID(kSashPlatformCID, SASHPLATFORM_CID);

#define SASHPLATFORM_CONTRACT_ID "@gnome.org/SashXB/Platform;1"

class SashPlatform : public sashIPlatform
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIPLATFORM
  
  SashPlatform();
  virtual ~SashPlatform();

};

#endif
