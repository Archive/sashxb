/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Header file for the Core.Cursor extension

*****************************************************************/

#ifndef SASHCURSOR_H
#define SASHCURSOR_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashICursor.h"
#include "SecurityManager.h"

#define SASHCURSOR_CID {0xcc0f484c, 0x33f9, 0x40ed, {0x81, 0xdc, 0x61, 0x3d, 0xab, 0x24, 0x2a, 0x22}}
    
NS_DEFINE_CID(kSashCursorCID, SASHCURSOR_CID);

#define SASHCURSOR_CONTRACT_ID "@gnome.org/SashXB/Cursor;1"

class SashCursor : public sashICursor
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHICURSOR
  
  SashCursor();
  virtual ~SashCursor();

};

#endif
