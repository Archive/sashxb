/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Implementation of the cursor class

*****************************************************************/

#include "sashIActionRuntime.h"
#include "nsIVariant.h"
#include "sashVariantUtils.h"
#include <gtk/gtk.h>
#include "extensiontools.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "secman.h"
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "SashCursor.h"

NS_IMPL_ISUPPORTS1_CI(SashCursor, sashICursor);

SashCursor::SashCursor() {
  NS_INIT_ISUPPORTS();
}

SashCursor::~SashCursor() { }

// with help from xwarppointer
NS_IMETHODIMP SashCursor::SetPosition(PRInt16 x, PRInt16 y)
{
	 Display *display;
	 Window root;
	 display = XOpenDisplay(NULL);
	 assert(display);
	 root = DefaultRootWindow(display);

	 XWarpPointer(display, None, root, 0, 0, 0, 0, x, y);
	 XFlush(display);

	 return NS_OK;
}

NS_IMETHODIMP SashCursor::GetPosition(nsIVariant *specific_coord, nsIVariant** _retval)
{
	 gint x = -1, y = -1;
	 // get the position for a toplevel window -- what else can we do?
	 GList* l = gdk_window_get_toplevels();
	 if (l != NULL) {
		  GdkWindow* w = (GdkWindow*) l->data;
		  gdk_window_get_pointer(w, &x, &y, NULL);
	 }

	 // creating arrays of variants is really painful
	 if (NewVariant(_retval) == NS_OK) {
		  nsIVariant* v = *_retval;
		  if (VariantIsBoolean(specific_coord)) {
			   if (VariantGetBoolean(specific_coord)) {
					VariantSetNumber(v, x);
			   } else {
					VariantSetNumber(v, y);
			   }				 
		  } else {
			   vector <int> nums;
			   nums.push_back(x);
			   nums.push_back(y);
			   VariantSetArray(v, nums);
		  }
	 } else 
		  return NS_ERROR_FAILURE;
			   
	 return NS_OK;
}

NS_IMETHODIMP SashCursor::SetStandard(const SashCursorType s) {
	 GList* l = gdk_window_get_toplevels();
	 while (l != NULL) {
		  GdkWindow* w = (GdkWindow*) l->data;
		  GdkCursor* c = gdk_cursor_new((GdkCursorType) s);
		  gdk_window_set_cursor(w, c);
		  l = g_list_next(l);
	 }
	 return NS_OK;
}

/* readonly attribute string APPSTARTING; */
NS_IMETHODIMP SashCursor::GetAPPSTARTING(PRInt32 * aAPPSTARTING)
{
  *aAPPSTARTING = GDK_WATCH;
  return NS_OK;
}

/* readonly attribute string ARROW; */
NS_IMETHODIMP SashCursor::GetARROW(PRInt32 * aARROW)
{
  *aARROW = GDK_LEFT_PTR;
  return NS_OK;
}

/* readonly attribute string CROSS; */
NS_IMETHODIMP SashCursor::GetCROSS(PRInt32 * aCROSS)
{
  *aCROSS = GDK_CROSS;
  return NS_OK;
}

/* readonly attribute string DEFAULT; */
NS_IMETHODIMP SashCursor::GetDEFAULT(PRInt32 * aDEFAULT)
{
  *aDEFAULT = GDK_LEFT_PTR;
  return NS_OK;
}

/* readonly attribute string HELP; */
NS_IMETHODIMP SashCursor::GetHELP(PRInt32 * aHELP)
{
  *aHELP = GDK_QUESTION_ARROW;
  return NS_OK;
}

/* readonly attribute string IBEAM; */
NS_IMETHODIMP SashCursor::GetIBEAM(PRInt32 * aIBEAM)
{
  *aIBEAM = GDK_XTERM;
  return NS_OK;
}

/* readonly attribute string ICON; */
NS_IMETHODIMP SashCursor::GetICON(PRInt32 * aICON)
{
  *aICON = GDK_ARROW;
  return NS_OK;
}

/* readonly attribute string NO; */
NS_IMETHODIMP SashCursor::GetNO(PRInt32 * aNO)
{
  *aNO = GDK_PIRATE;
  return NS_OK;
}

/* readonly attribute string SIZEALL; */
NS_IMETHODIMP SashCursor::GetSIZEALL(PRInt32 * aSIZEALL)
{
  *aSIZEALL = GDK_FLEUR;
  return NS_OK;
}

/* readonly attribute string SIZENESW; */
NS_IMETHODIMP SashCursor::GetSIZENESW(PRInt32 * aSIZENESW)
{
  *aSIZENESW = GDK_SB_V_DOUBLE_ARROW;
  return NS_OK;
}

/* readonly attribute string SIZENS; */
NS_IMETHODIMP SashCursor::GetSIZENS(PRInt32 * aSIZENS)
{
  *aSIZENS = GDK_SB_V_DOUBLE_ARROW;
  return NS_OK;
}

/* readonly attribute string SIZENWSE; */
NS_IMETHODIMP SashCursor::GetSIZENWSE(PRInt32 * aSIZENWSE)
{
  *aSIZENWSE = GDK_SB_H_DOUBLE_ARROW;
  return NS_OK;
}

/* readonly attribute string SIZEWE; */
NS_IMETHODIMP SashCursor::GetSIZEWE(PRInt32 * aSIZEWE)
{
	 *aSIZEWE = GDK_SB_H_DOUBLE_ARROW;
	 return NS_OK;
}

/* readonly attribute string UPARROW; */
NS_IMETHODIMP SashCursor::GetUPARROW(PRInt32 * aUPARROW)
{
  *aUPARROW = GDK_CENTER_PTR;
  return NS_OK;
}

/* readonly attribute string WAIT; */
NS_IMETHODIMP SashCursor::GetWAIT(PRInt32 * aWAIT)
{
  *aWAIT = GDK_WATCH;
  return NS_OK;
}

/* readonly attribute string X_CURSOR; */
NS_IMETHODIMP SashCursor::GetX_CURSOR(PRInt32 * aX_CURSOR)
{
  *aX_CURSOR = GDK_X_CURSOR;
  return NS_OK;
}

/* readonly attribute string BASED_ARROW_DOWN; */
NS_IMETHODIMP SashCursor::GetBASED_ARROW_DOWN(PRInt32 * aBASED_ARROW_DOWN)
{
  *aBASED_ARROW_DOWN = GDK_BASED_ARROW_DOWN;
  return NS_OK;
}

/* readonly attribute string BASED_ARROW_UP; */
NS_IMETHODIMP SashCursor::GetBASED_ARROW_UP(PRInt32 * aBASED_ARROW_UP)
{
  *aBASED_ARROW_UP = GDK_BASED_ARROW_UP;
  return NS_OK;
}

/* readonly attribute string BOAT; */
NS_IMETHODIMP SashCursor::GetBOAT(PRInt32 * aBOAT)
{
  *aBOAT = GDK_BOAT;
  return NS_OK;
}

/* readonly attribute string BOGOSITY; */
NS_IMETHODIMP SashCursor::GetBOGOSITY(PRInt32 * aBOGOSITY)
{
  *aBOGOSITY = GDK_BOGOSITY;
  return NS_OK;
}

/* readonly attribute string BOTTOM_LEFT_CORNER; */
NS_IMETHODIMP SashCursor::GetBOTTOM_LEFT_CORNER(PRInt32 * aBOTTOM_LEFT_CORNER)
{
  *aBOTTOM_LEFT_CORNER = GDK_BOTTOM_LEFT_CORNER;
  return NS_OK;
}

/* readonly attribute string BOTTOM_RIGHT_CORNER; */
NS_IMETHODIMP SashCursor::GetBOTTOM_RIGHT_CORNER(PRInt32 * aBOTTOM_RIGHT_CORNER)
{
  *aBOTTOM_RIGHT_CORNER = GDK_BOTTOM_RIGHT_CORNER;
  return NS_OK;
}

/* readonly attribute string BOTTOM_SIDE; */
NS_IMETHODIMP SashCursor::GetBOTTOM_SIDE(PRInt32 * aBOTTOM_SIDE)
{
  *aBOTTOM_SIDE = GDK_BOTTOM_SIDE;
  return NS_OK;
}

/* readonly attribute string BOTTOM_TEE; */
NS_IMETHODIMP SashCursor::GetBOTTOM_TEE(PRInt32 * aBOTTOM_TEE)
{
  *aBOTTOM_TEE = GDK_BOTTOM_TEE;
  return NS_OK;
}

/* readonly attribute string BOX_SPIRAL; */
NS_IMETHODIMP SashCursor::GetBOX_SPIRAL(PRInt32 * aBOX_SPIRAL)
{
  *aBOX_SPIRAL = GDK_BOX_SPIRAL;
  return NS_OK;
}

/* readonly attribute string CENTER_PTR; */
NS_IMETHODIMP SashCursor::GetCENTER_PTR(PRInt32 * aCENTER_PTR)
{
  *aCENTER_PTR = GDK_CENTER_PTR;
  return NS_OK;
}

/* readonly attribute string CIRCLE; */
NS_IMETHODIMP SashCursor::GetCIRCLE(PRInt32 * aCIRCLE)
{
  *aCIRCLE = GDK_CIRCLE;
  return NS_OK;
}

/* readonly attribute string CLOCK; */
NS_IMETHODIMP SashCursor::GetCLOCK(PRInt32 * aCLOCK)
{
  *aCLOCK = GDK_CLOCK;
  return NS_OK;
}

/* readonly attribute string COFFEE_MUG; */
NS_IMETHODIMP SashCursor::GetCOFFEE_MUG(PRInt32 * aCOFFEE_MUG)
{
  *aCOFFEE_MUG = GDK_COFFEE_MUG;
  return NS_OK;
}

/* readonly attribute string CROSS_REVERSE; */
NS_IMETHODIMP SashCursor::GetCROSS_REVERSE(PRInt32 * aCROSS_REVERSE)
{
  *aCROSS_REVERSE = GDK_CROSS_REVERSE;
  return NS_OK;
}

/* readonly attribute string CROSSHAIR; */
NS_IMETHODIMP SashCursor::GetCROSSHAIR(PRInt32 * aCROSSHAIR)
{
  *aCROSSHAIR = GDK_CROSSHAIR;
  return NS_OK;
}

/* readonly attribute string DIAMOND_CROSS; */
NS_IMETHODIMP SashCursor::GetDIAMOND_CROSS(PRInt32 * aDIAMOND_CROSS)
{
  *aDIAMOND_CROSS = GDK_DIAMOND_CROSS;
  return NS_OK;
}

/* readonly attribute string DOT; */
NS_IMETHODIMP SashCursor::GetDOT(PRInt32 * aDOT)
{
  *aDOT = GDK_DOT;
  return NS_OK;
}

/* readonly attribute string DOTBOX; */
NS_IMETHODIMP SashCursor::GetDOTBOX(PRInt32 * aDOTBOX)
{
  *aDOTBOX = GDK_DOTBOX;
  return NS_OK;
}

/* readonly attribute string DOUBLE_ARROW; */
NS_IMETHODIMP SashCursor::GetDOUBLE_ARROW(PRInt32 * aDOUBLE_ARROW)
{
  *aDOUBLE_ARROW = GDK_DOUBLE_ARROW;
  return NS_OK;
}

/* readonly attribute string DRAFT_LARGE; */
NS_IMETHODIMP SashCursor::GetDRAFT_LARGE(PRInt32 * aDRAFT_LARGE)
{
  *aDRAFT_LARGE = GDK_DRAFT_LARGE;
  return NS_OK;
}

/* readonly attribute string DRAFT_SMALL; */
NS_IMETHODIMP SashCursor::GetDRAFT_SMALL(PRInt32 * aDRAFT_SMALL)
{
  *aDRAFT_SMALL = GDK_DRAFT_SMALL;
  return NS_OK;
}

/* readonly attribute string DRAPED_BOX; */
NS_IMETHODIMP SashCursor::GetDRAPED_BOX(PRInt32 * aDRAPED_BOX)
{
  *aDRAPED_BOX = GDK_DRAPED_BOX;
  return NS_OK;
}

/* readonly attribute string EXCHANGE; */
NS_IMETHODIMP SashCursor::GetEXCHANGE(PRInt32 * aEXCHANGE)
{
  *aEXCHANGE = GDK_EXCHANGE;
  return NS_OK;
}

/* readonly attribute string FLEUR; */
NS_IMETHODIMP SashCursor::GetFLEUR(PRInt32 * aFLEUR)
{
  *aFLEUR = GDK_FLEUR;
  return NS_OK;
}

/* readonly attribute string GOBBLER; */
NS_IMETHODIMP SashCursor::GetGOBBLER(PRInt32 * aGOBBLER)
{
  *aGOBBLER = GDK_GOBBLER;
  return NS_OK;
}

/* readonly attribute string GUMBY; */
NS_IMETHODIMP SashCursor::GetGUMBY(PRInt32 * aGUMBY)
{
  *aGUMBY = GDK_GUMBY;
  return NS_OK;
}

/* readonly attribute string HAND1; */
NS_IMETHODIMP SashCursor::GetHAND1(PRInt32 * aHAND1)
{
  *aHAND1 = GDK_HAND1;
  return NS_OK;
}

/* readonly attribute string HAND2; */
NS_IMETHODIMP SashCursor::GetHAND2(PRInt32 * aHAND2)
{
  *aHAND2 = GDK_HAND2;
  return NS_OK;
}

/* readonly attribute string HEART; */
NS_IMETHODIMP SashCursor::GetHEART(PRInt32 * aHEART)
{
  *aHEART = GDK_HEART;
  return NS_OK;
}

/* readonly attribute string IRON_CROSS; */
NS_IMETHODIMP SashCursor::GetIRON_CROSS(PRInt32 * aIRON_CROSS)
{
  *aIRON_CROSS = GDK_IRON_CROSS;
  return NS_OK;
}

/* readonly attribute string LEFT_PTR; */
NS_IMETHODIMP SashCursor::GetLEFT_PTR(PRInt32 * aLEFT_PTR)
{
  *aLEFT_PTR = GDK_LEFT_PTR;
  return NS_OK;
}

/* readonly attribute string LEFT_SIDE; */
NS_IMETHODIMP SashCursor::GetLEFT_SIDE(PRInt32 * aLEFT_SIDE)
{
  *aLEFT_SIDE = GDK_LEFT_SIDE;
  return NS_OK;
}

/* readonly attribute string LEFT_TEE; */
NS_IMETHODIMP SashCursor::GetLEFT_TEE(PRInt32 * aLEFT_TEE)
{
  *aLEFT_TEE = GDK_LEFT_TEE;
  return NS_OK;
}

/* readonly attribute string LEFTBUTTON; */
NS_IMETHODIMP SashCursor::GetLEFTBUTTON(PRInt32 * aLEFTBUTTON)
{
  *aLEFTBUTTON = GDK_LEFTBUTTON;
  return NS_OK;
}

/* readonly attribute string LL_ANGLE; */
NS_IMETHODIMP SashCursor::GetLL_ANGLE(PRInt32 * aLL_ANGLE)
{
  *aLL_ANGLE = GDK_LL_ANGLE;
  return NS_OK;
}

/* readonly attribute string LR_ANGLE; */
NS_IMETHODIMP SashCursor::GetLR_ANGLE(PRInt32 * aLR_ANGLE)
{
  *aLR_ANGLE = GDK_LR_ANGLE;
  return NS_OK;
}

/* readonly attribute string MAN; */
NS_IMETHODIMP SashCursor::GetMAN(PRInt32 * aMAN)
{
  *aMAN = GDK_MAN;
  return NS_OK;
}

/* readonly attribute string MIDDLEBUTTON; */
NS_IMETHODIMP SashCursor::GetMIDDLEBUTTON(PRInt32 * aMIDDLEBUTTON)
{
  *aMIDDLEBUTTON = GDK_MIDDLEBUTTON;
  return NS_OK;
}

/* readonly attribute string MOUSE; */
NS_IMETHODIMP SashCursor::GetMOUSE(PRInt32 * aMOUSE)
{
  *aMOUSE = GDK_MOUSE;
  return NS_OK;
}

/* readonly attribute string PENCIL; */
NS_IMETHODIMP SashCursor::GetPENCIL(PRInt32 * aPENCIL)
{
  *aPENCIL = GDK_PENCIL;
  return NS_OK;
}

/* readonly attribute string PIRATE; */
NS_IMETHODIMP SashCursor::GetPIRATE(PRInt32 * aPIRATE)
{
  *aPIRATE = GDK_PIRATE;
  return NS_OK;
}

/* readonly attribute string PLUS; */
NS_IMETHODIMP SashCursor::GetPLUS(PRInt32 * aPLUS)
{
  *aPLUS = GDK_PLUS;
  return NS_OK;
}

/* readonly attribute string QUESTION_ARROW; */
NS_IMETHODIMP SashCursor::GetQUESTION_ARROW(PRInt32 * aQUESTION_ARROW)
{
  *aQUESTION_ARROW = GDK_QUESTION_ARROW;
  return NS_OK;
}

/* readonly attribute string RIGHT_PTR; */
NS_IMETHODIMP SashCursor::GetRIGHT_PTR(PRInt32 * aRIGHT_PTR)
{
  *aRIGHT_PTR = GDK_RIGHT_PTR;
  return NS_OK;
}

/* readonly attribute string RIGHT_SIDE; */
NS_IMETHODIMP SashCursor::GetRIGHT_SIDE(PRInt32 * aRIGHT_SIDE)
{
  *aRIGHT_SIDE = GDK_RIGHT_SIDE;
  return NS_OK;
}

/* readonly attribute string RIGHT_TEE; */
NS_IMETHODIMP SashCursor::GetRIGHT_TEE(PRInt32 * aRIGHT_TEE)
{
  *aRIGHT_TEE = GDK_RIGHT_TEE;
  return NS_OK;
}

/* readonly attribute string RIGHTBUTTON; */
NS_IMETHODIMP SashCursor::GetRIGHTBUTTON(PRInt32 * aRIGHTBUTTON)
{
  *aRIGHTBUTTON = GDK_RIGHTBUTTON;
  return NS_OK;
}

/* readonly attribute string RTL_LOGO; */
NS_IMETHODIMP SashCursor::GetRTL_LOGO(PRInt32 * aRTL_LOGO)
{
  *aRTL_LOGO = GDK_RTL_LOGO;
  return NS_OK;
}

/* readonly attribute string SAILBOAT; */
NS_IMETHODIMP SashCursor::GetSAILBOAT(PRInt32 * aSAILBOAT)
{
  *aSAILBOAT = GDK_SAILBOAT;
  return NS_OK;
}

/* readonly attribute string SB_DOWN_ARROW; */
NS_IMETHODIMP SashCursor::GetSB_DOWN_ARROW(PRInt32 * aSB_DOWN_ARROW)
{
  *aSB_DOWN_ARROW = GDK_SB_DOWN_ARROW;
  return NS_OK;
}

/* readonly attribute string SB_H_DOUBLE_ARROW; */
NS_IMETHODIMP SashCursor::GetSB_H_DOUBLE_ARROW(PRInt32 * aSB_H_DOUBLE_ARROW)
{
  *aSB_H_DOUBLE_ARROW = GDK_SB_H_DOUBLE_ARROW;
  return NS_OK;
}

/* readonly attribute string SB_LEFT_ARROW; */
NS_IMETHODIMP SashCursor::GetSB_LEFT_ARROW(PRInt32 * aSB_LEFT_ARROW)
{
  *aSB_LEFT_ARROW = GDK_SB_LEFT_ARROW;
  return NS_OK;
}

/* readonly attribute string SB_RIGHT_ARROW; */
NS_IMETHODIMP SashCursor::GetSB_RIGHT_ARROW(PRInt32 * aSB_RIGHT_ARROW)
{
  *aSB_RIGHT_ARROW = GDK_SB_RIGHT_ARROW;
  return NS_OK;
}

/* readonly attribute string SB_UP_ARROW; */
NS_IMETHODIMP SashCursor::GetSB_UP_ARROW(PRInt32 * aSB_UP_ARROW)
{
  *aSB_UP_ARROW = GDK_SB_UP_ARROW;
  return NS_OK;
}

/* readonly attribute string SB_V_DOUBLE_ARROW; */
NS_IMETHODIMP SashCursor::GetSB_V_DOUBLE_ARROW(PRInt32 * aSB_V_DOUBLE_ARROW)
{
  *aSB_V_DOUBLE_ARROW = GDK_SB_V_DOUBLE_ARROW;
  return NS_OK;
}

/* readonly attribute string SHUTTLE; */
NS_IMETHODIMP SashCursor::GetSHUTTLE(PRInt32 * aSHUTTLE)
{
  *aSHUTTLE = GDK_SHUTTLE;
  return NS_OK;
}

/* readonly attribute string SIZING; */
NS_IMETHODIMP SashCursor::GetSIZING(PRInt32 * aSIZING)
{
  *aSIZING = GDK_SIZING;
  return NS_OK;
}

/* readonly attribute string SPIDER; */
NS_IMETHODIMP SashCursor::GetSPIDER(PRInt32 * aSPIDER)
{
  *aSPIDER = GDK_SPIDER;
  return NS_OK;
}

/* readonly attribute string SPRAYCAN; */
NS_IMETHODIMP SashCursor::GetSPRAYCAN(PRInt32 * aSPRAYCAN)
{
  *aSPRAYCAN = GDK_SPRAYCAN;
  return NS_OK;
}

/* readonly attribute string STAR; */
NS_IMETHODIMP SashCursor::GetSTAR(PRInt32 * aSTAR)
{
  *aSTAR = GDK_STAR;
  return NS_OK;
}

/* readonly attribute string TARGET; */
NS_IMETHODIMP SashCursor::GetTARGET(PRInt32 * aTARGET)
{
  *aTARGET = GDK_TARGET;
  return NS_OK;
}

/* readonly attribute string TCROSS; */
NS_IMETHODIMP SashCursor::GetTCROSS(PRInt32 * aTCROSS)
{
  *aTCROSS = GDK_TCROSS;
  return NS_OK;
}

/* readonly attribute string TOP_LEFT_ARROW; */
NS_IMETHODIMP SashCursor::GetTOP_LEFT_ARROW(PRInt32 * aTOP_LEFT_ARROW)
{
  *aTOP_LEFT_ARROW = GDK_TOP_LEFT_ARROW;
  return NS_OK;
}

/* readonly attribute string TOP_LEFT_CORNER; */
NS_IMETHODIMP SashCursor::GetTOP_LEFT_CORNER(PRInt32 * aTOP_LEFT_CORNER)
{
  *aTOP_LEFT_CORNER = GDK_TOP_LEFT_CORNER;
  return NS_OK;
}

/* readonly attribute string TOP_RIGHT_CORNER; */
NS_IMETHODIMP SashCursor::GetTOP_RIGHT_CORNER(PRInt32 * aTOP_RIGHT_CORNER)
{
  *aTOP_RIGHT_CORNER = GDK_TOP_RIGHT_CORNER;
  return NS_OK;
}

/* readonly attribute string TOP_SIDE; */
NS_IMETHODIMP SashCursor::GetTOP_SIDE(PRInt32 * aTOP_SIDE)
{
  *aTOP_SIDE = GDK_TOP_SIDE;
  return NS_OK;
}

/* readonly attribute string TOP_TEE; */
NS_IMETHODIMP SashCursor::GetTOP_TEE(PRInt32 * aTOP_TEE)
{
  *aTOP_TEE = GDK_TOP_TEE;
  return NS_OK;
}

/* readonly attribute string TREK; */
NS_IMETHODIMP SashCursor::GetTREK(PRInt32 * aTREK)
{
  *aTREK = GDK_TREK;
  return NS_OK;
}

/* readonly attribute string UL_ANGLE; */
NS_IMETHODIMP SashCursor::GetUL_ANGLE(PRInt32 * aUL_ANGLE)
{
  *aUL_ANGLE = GDK_UL_ANGLE;
  return NS_OK;
}

/* readonly attribute string UMBRELLA; */
NS_IMETHODIMP SashCursor::GetUMBRELLA(PRInt32 * aUMBRELLA)
{
  *aUMBRELLA = GDK_UMBRELLA;
  return NS_OK;
}

/* readonly attribute string UR_ANGLE; */
NS_IMETHODIMP SashCursor::GetUR_ANGLE(PRInt32 * aUR_ANGLE)
{
  *aUR_ANGLE = GDK_UR_ANGLE;
  return NS_OK;
}

/* readonly attribute string WATCH; */
NS_IMETHODIMP SashCursor::GetWATCH(PRInt32 * aWATCH)
{
  *aWATCH = GDK_WATCH;
  return NS_OK;
}

/* readonly attribute string XTERM; */
NS_IMETHODIMP SashCursor::GetXTERM(PRInt32 * aXTERM)
{
  *aXTERM = GDK_XTERM;
  return NS_OK;
}

