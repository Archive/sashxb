/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

Implementation of the processs extension

*****************************************************************/

#include "extensiontools.h"
#include "sashProcess.h"
#include "sashVariantUtils.h"

#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

NS_IMPL_ISUPPORTS1_CI(sashProcess, sashIProcess);

sashProcess::sashProcess()
{
  NS_INIT_ISUPPORTS();
}

sashProcess::~sashProcess()
{
}

/* PRInt32 Create (in string filename); */
NS_IMETHODIMP sashProcess::Create(nsIVariant * v, PRInt32 *_retval)
{
	 const char ** args = NULL;
	 vector<string> items;
	 string str;
	 if (VariantIsArray(v)) {
		  VariantGetArray(v, items);
		  if (items.size() > 0) {
			   args = new const char* [items.size() + 1];
			   for (unsigned int i = 0; i < items.size(); i++)
					args[i] = items[i].c_str();
			   args[items.size()] = NULL;
		  }
	 }
	 else {
		  args = new const char*[2];
		  str = VariantGetString(v);
		  args[0] = str.c_str();
		  args[1] = NULL;
	 }

	 if (args) {
		  int pid = fork();

		  if (pid == 0) {
			   execvp(args[0], (char **)args);
			   _exit(0);
		  }
		  else
			   *_retval = pid;
		  
		  delete [] args;
		  
		  return NS_OK;
	 }

	 return NS_ERROR_INVALID_ARG;
}

/* PRBool IsAlive (in PRInt32 pid); */
NS_IMETHODIMP sashProcess::IsAlive(PRInt32 pid, PRBool *_retval)
{
  if (pid > 0)
	*_retval = (kill(pid, 0) == 0);
  else
	*_retval = false;
  return NS_OK;
}

/* PRBool Terminate (in PRInt32 pid); */
NS_IMETHODIMP sashProcess::Terminate(PRInt32 pid, PRBool *_retval)
{
  if (pid > 0)
	*_retval = (kill(pid, SIGKILL) == 0);
  else
	*_retval = false;
  return NS_OK;
}
