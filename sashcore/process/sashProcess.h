/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

Header file for the channels extension

*****************************************************************/

#ifndef SASHPROCESS_H
#define SASHPROCESS_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIProcess.h"
#include "sashIExtension.h"
#include "SecurityManager.h"
#include "secman.h"

#include <string>

#define SASHPROCESS_CID {0x0f6ef661, 0x56d4, 0x448f, \
                        {0x9c, 0xe0, 0xd8, 0x69, 0x0e, 0x53, 0x0f, 0x5d}}
    
NS_DEFINE_CID(kSashProcessCID, SASHPROCESS_CID);

#define SASHPROCESS_CONTRACT_ID "@gnome.org/SashXB/Core/Process;1"

class sashProcess : public sashIProcess
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIPROCESS
  
  sashProcess();
  virtual ~sashProcess();
 private:

};

#endif
