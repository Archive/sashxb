/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Implementation of the SashUI class

*****************************************************************/

#include "sashIActionRuntime.h"
#include "nsIVariant.h"
#include "sashVariantUtils.h"
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnomeui/gnome-pixmap.h>
#include "extensiontools.h"
#include "debugmsg.h"
#include "nsMemory.h"
#include "secman.h"
#include "SashUI.h"
#include <gnome.h>
NS_IMPL_ISUPPORTS1_CI(SashUI, sashIUI);
enum {
	 IDCANCEL = 0,
	 IDABORT,
	 IDIGNORE,
	 IDNO,
	 IDOK,
	 IDRETRY,
	 IDYES
};

const int MB_ABORTRETRYIGNORE  = 1 << 0;
const int MB_DEFBUTTON1        = 1 << 1;
const int MB_DEFBUTTON2        = 1 << 2;
const int MB_DEFBUTTON3        = 1 << 3;
const int MB_DEFBUTTON4        = 1 << 4;
const int MB_ICONERROR         = 1 << 5;
const int MB_ICONINFORMATION   = 1 << 6;
const int MB_ICONWARNING       = 1 << 7;
const int MB_OK                = 1 << 8;
const int MB_OKCANCEL          = 1 << 9;
const int MB_RETRYCANCEL       = 1 << 10;
const int MB_SETFOREGROUND     = 1 << 11;
const int MB_SYSTEMMODAL       = 1 << 12;
const int MB_YESNO             = 1 << 13;
const int MB_YESNOCANCEL       = 1 << 14;
const int MB_TOPMOST           = 1 << 15;

// const int OFN_ALLOWMULTISELECT = 1 << 0;
// const int OFN_CREATEPROMPT     = 1 << 1;
// const int OFN_FILEMUSTEXIST    = 1 << 2;
// const int OFN_HIDEREADONLY     = 1 << 3;
// const int OFN_OVERWRITEPROMPT  = 1 << 4;
// const int OFN_PATHMUSTEXIST    = 1 << 5;
 
SashUI::SashUI() {
  NS_INIT_ISUPPORTS();
}

SashUI::~SashUI() { }

/* readonly attribute PRInt32 IDCANCEL; */
NS_IMETHODIMP SashUI::GetIDCANCEL(PRInt32 *aIDCANCEL)
{
    *aIDCANCEL = IDCANCEL;
	return NS_OK;
}

/* readonly attribute PRInt32 IDABORT; */
NS_IMETHODIMP SashUI::GetIDABORT(PRInt32 *aIDABORT)
{
	*aIDABORT = IDABORT;
	return NS_OK;
}

/* readonly attribute PRInt32 IDIGNORE; */
NS_IMETHODIMP SashUI::GetIDIGNORE(PRInt32 *aIDIGNORE)
{
    *aIDIGNORE = IDIGNORE;
	return NS_OK;
}

/* readonly attribute PRInt32 IDNO; */
NS_IMETHODIMP SashUI::GetIDNO(PRInt32 *aIDNO)
{
    *aIDNO = IDNO;
	return NS_OK;
}

/* readonly attribute PRInt32 IDOK; */
NS_IMETHODIMP SashUI::GetIDOK(PRInt32 *aIDOK)
{
    *aIDOK = IDOK;
	return NS_OK;
}

/* readonly attribute PRInt32 IDRETRY; */
NS_IMETHODIMP SashUI::GetIDRETRY(PRInt32 *aIDRETRY)
{
    *aIDRETRY = IDRETRY;
	return NS_OK;
}

/* readonly attribute PRInt32 IDYES; */
NS_IMETHODIMP SashUI::GetIDYES(PRInt32 *aIDYES)
{
    *aIDYES = IDYES;
	return NS_OK;
}

/* readonly attribute string MB_ABORTRETRYIGNORE; */
NS_IMETHODIMP SashUI::GetMB_ABORTRETRYIGNORE(PRInt32 * aMB_ABORTRETRYIGNORE)
{
  *aMB_ABORTRETRYIGNORE = MB_ABORTRETRYIGNORE;
  return NS_OK;
}

/* readonly attribute string MB_DEFBUTTON1; */
NS_IMETHODIMP SashUI::GetMB_DEFBUTTON1(PRInt32 * aMB_DEFBUTTON1)
{
  *aMB_DEFBUTTON1 = MB_DEFBUTTON1;
  return NS_OK;
}

/* readonly attribute string MB_DEFBUTTON2; */
NS_IMETHODIMP SashUI::GetMB_DEFBUTTON2(PRInt32 * aMB_DEFBUTTON2)
{
  *aMB_DEFBUTTON2 = MB_DEFBUTTON2;
  return NS_OK;
}

/* readonly attribute string MB_DEFBUTTON3; */
NS_IMETHODIMP SashUI::GetMB_DEFBUTTON3(PRInt32 * aMB_DEFBUTTON3)
{
  *aMB_DEFBUTTON3 = MB_DEFBUTTON3;
  return NS_OK;
}

/* readonly attribute string MB_DEFBUTTON4; */
NS_IMETHODIMP SashUI::GetMB_DEFBUTTON4(PRInt32 * aMB_DEFBUTTON4)
{
  *aMB_DEFBUTTON4 = MB_DEFBUTTON4;
  return NS_OK;
}

/* readonly attribute string MB_ICONERROR; */
NS_IMETHODIMP SashUI::GetMB_ICONERROR(PRInt32 * aMB_ICONERROR)
{
  *aMB_ICONERROR = MB_ICONERROR;
  return NS_OK;
}

/* readonly attribute string MB_ICONINFORMATION; */
NS_IMETHODIMP SashUI::GetMB_ICONINFORMATION(PRInt32 * aMB_ICONINFORMATION)
{
  *aMB_ICONINFORMATION = MB_ICONINFORMATION;
  return NS_OK;
}

/* readonly attribute string MB_ICONWARNING; */
NS_IMETHODIMP SashUI::GetMB_ICONWARNING(PRInt32 * aMB_ICONWARNING)
{
  *aMB_ICONWARNING = MB_ICONWARNING;
  return NS_OK;
}

/* readonly attribute string MB_OK; */
NS_IMETHODIMP SashUI::GetMB_OK(PRInt32 * aMB_OK)
{
  *aMB_OK = MB_OK;
  return NS_OK;
}

/* readonly attribute string MB_OKCANCEL; */
NS_IMETHODIMP SashUI::GetMB_OKCANCEL(PRInt32 * aMB_OKCANCEL)
{
  *aMB_OKCANCEL = MB_OKCANCEL;
  return NS_OK;
}

/* readonly attribute string MB_RETRYCANCEL; */
NS_IMETHODIMP SashUI::GetMB_RETRYCANCEL(PRInt32 * aMB_RETRYCANCEL)
{
  *aMB_RETRYCANCEL = MB_RETRYCANCEL;
  return NS_OK;
}

/* readonly attribute string MB_SETFOREGROUND; */
NS_IMETHODIMP SashUI::GetMB_SETFOREGROUND(PRInt32 * aMB_SETFOREGROUND)
{
  *aMB_SETFOREGROUND = MB_SETFOREGROUND;
  return NS_OK;
}

/* readonly attribute string MB_SYSTEMMODAL; */
NS_IMETHODIMP SashUI::GetMB_SYSTEMMODAL(PRInt32 * aMB_SYSTEMMODAL)
{
  *aMB_SYSTEMMODAL = MB_SYSTEMMODAL;
  return NS_OK;
}

/* readonly attribute string MB_YESNO; */
NS_IMETHODIMP SashUI::GetMB_YESNO(PRInt32 * aMB_YESNO)
{
  *aMB_YESNO = MB_YESNO;
  return NS_OK;
}

/* readonly attribute string MB_YESNOCANCEL; */
NS_IMETHODIMP SashUI::GetMB_YESNOCANCEL(PRInt32 * aMB_YESNOCANCEL)
{
  *aMB_YESNOCANCEL = MB_YESNOCANCEL;
  return NS_OK;
}

void SashUI::set_default(GtkWidget* d, PRInt32 flags, int max) {
	 int but = 0;
	 if (flags & MB_DEFBUTTON2) but = 1;
	 if (flags & MB_DEFBUTTON3 && max > 2) but = 2;
	 if (flags & MB_DEFBUTTON4 && max > 3) but = 3;
	 gnome_dialog_set_default(GNOME_DIALOG(d), but);
}


/* PRInt32 MessageBox (in string message, in string title, in PRInt32 type); */
NS_IMETHODIMP SashUI::MessageBox(const char *message, const char *title, PRInt32 type, PRInt32 *_retval)
{
	 // this function could not be more painful

	 // handle the requested icons first
	 char* c;
	 GtkWidget *pixmap, *hbox;
	 if (type & MB_ICONERROR) c = gnome_pixmap_file("gnome-error.png");
	 else if (type & MB_ICONINFORMATION) c = gnome_pixmap_file("gnome-info.png");
	 else if (type & MB_ICONWARNING) c = gnome_pixmap_file("gnome-warning.png");
	 else c = gnome_pixmap_file("gnome-default-dlg.png");
	 if (c == NULL) return NS_ERROR_NULL_POINTER;
	 pixmap = gnome_pixmap_new_from_file(c);
	 if (pixmap == NULL) return NS_ERROR_NULL_POINTER;

	 GtkWidget * label = gtk_label_new(message);
	
	 hbox = gtk_hbox_new(false, 0);
	 gtk_box_pack_start(GTK_BOX(hbox), pixmap, FALSE, TRUE, 0);
	 gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);
	 gtk_widget_show(hbox);
	 gtk_widget_show(label);
	 gtk_widget_show(pixmap);
	GtkWidget* d;
	if (type & MB_OK) {
		 d = gnome_dialog_new(title, GNOME_STOCK_BUTTON_OK, NULL);
		 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), hbox, TRUE, TRUE, 0);
		 gnome_dialog_run_and_close(GNOME_DIALOG(d));
		 *_retval = IDOK;
	} else if (type & MB_OKCANCEL) {
		 d = gnome_dialog_new(title, GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
		 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), hbox, TRUE, TRUE, 0);
		 set_default(d, type, 2);
		 int ret = gnome_dialog_run_and_close(GNOME_DIALOG(d));
		 *_retval = (ret == 0 ? IDOK : IDCANCEL);
	} else if (type & MB_RETRYCANCEL) {
		 d = gnome_dialog_new(title,_("Retry"), GNOME_STOCK_BUTTON_CANCEL, NULL);
		 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), hbox, TRUE, TRUE, 0);
		 set_default(d, type, 2);
		 int ret = gnome_dialog_run_and_close(GNOME_DIALOG(d));
		 *_retval = (ret == 0 ? IDRETRY : IDCANCEL);
	} else if (type & MB_YESNO) {
		 d = gnome_dialog_new(title, GNOME_STOCK_BUTTON_YES, GNOME_STOCK_BUTTON_NO, NULL);
		 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), hbox, TRUE, TRUE, 0);
		 set_default(d, type, 2);
		 int ret = gnome_dialog_run_and_close(GNOME_DIALOG(d));
		 *_retval = (ret == 0 ? IDYES : IDNO);
	} else if (type & MB_YESNOCANCEL) {
		 d = gnome_dialog_new(title, GNOME_STOCK_BUTTON_YES, GNOME_STOCK_BUTTON_NO, GNOME_STOCK_BUTTON_CANCEL, NULL);
		 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), hbox, TRUE, TRUE, 0);
		 set_default(d, type, 3);
		 int ret = gnome_dialog_run_and_close(GNOME_DIALOG(d));
		 *_retval = (ret == 0 ? IDYES : (ret == 1 ? IDNO : IDCANCEL));
	} else if (type & MB_ABORTRETRYIGNORE) {
		 d = gnome_dialog_new(title, _("Abort"), _("Retry"), _("Ignore"),  NULL);
		 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), hbox, TRUE, TRUE, 0);
		 set_default(d, type, 3);
		 int ret = gnome_dialog_run_and_close(GNOME_DIALOG(d));
		 *_retval = (ret == 0 ? IDABORT : (ret == 1 ? IDRETRY : IDIGNORE));
	}

    return NS_OK;
}

/* string PromptForValue (in string title, in string prompt, in string default_value); */
NS_IMETHODIMP SashUI::PromptForValue(const char *title, const char *prompt, const char *default_value, char **_retval)
{
	 GtkWidget* label = gtk_label_new(prompt);
	 GtkWidget* entry = gtk_entry_new();
	 gtk_widget_show(label);
	 gtk_widget_show(entry);
	 gtk_entry_set_text(GTK_ENTRY(entry), default_value);
	 GtkWidget* d = gnome_dialog_new(title, GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
	 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), label, TRUE, TRUE, 0);
	 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), entry, TRUE, TRUE, 0);
	 gnome_dialog_close_hides(GNOME_DIALOG(d), true);
	 if (gnome_dialog_run(GNOME_DIALOG(d)) == 1) {
		  gtk_widget_destroy(d);
		  XP_COPY_STRING("", _retval);
		  return NS_OK;
	 }
	 gchar* c = gtk_editable_get_chars(GTK_EDITABLE(entry), 0, -1);
	 string s(c);
	 if (c != NULL) g_free(c);
	 gtk_widget_destroy(d);
	 XP_COPY_STRING(s.c_str(), _retval);
	 return NS_OK;
}

/* nsIVariant PromptChoice (in string title, in string prompt, in nsIVariant choicelist, in nsIVariant default_value, in nsIVariant multiselect); */
NS_IMETHODIMP SashUI::PromptChoice(const char *title, const char *prompt, nsIVariant *choicelist, nsIVariant *default_value, nsIVariant *multiselect, nsIVariant **_retval)
{
	 bool ms;
	 GList* gl = NULL;
	 if (VariantIsArray(choicelist)) {
		  vector <string> choices;
		  VariantGetArray (choicelist, choices);

		  for (unsigned int i = 0 ; i < choices.size(); i++) {
			   string s = choices[i];
			   GtkWidget* list_item = gtk_list_item_new_with_label(s.c_str());
			   gtk_object_set_data(GTK_OBJECT(list_item), "offset", (gpointer) i);
			   gtk_widget_show(list_item);
			   gl = g_list_append(gl, list_item);
		  }
	 }
	 GtkWidget* label = gtk_label_new(prompt);
	 gtk_widget_show(label);
	 GtkWidget* list = gtk_list_new();
	 gtk_list_append_items(GTK_LIST(list), gl);
	 if ((ms = VariantGetBoolean(multiselect))) {
		  gtk_list_set_selection_mode(GTK_LIST(list), GTK_SELECTION_MULTIPLE);
	 } else {
		  gtk_list_set_selection_mode(GTK_LIST(list), GTK_SELECTION_SINGLE);
	 }
	 if (VariantIsNumber(default_value)) {
		  int off = (int) VariantGetNumber(default_value);
		  gtk_list_select_item(GTK_LIST(list), off);
	 }
	 gtk_widget_show(list);
	 GtkWidget* d = gnome_dialog_new(title, GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
	 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), label, TRUE, TRUE, 0);
	 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), list, TRUE, TRUE, 0);
	 gnome_dialog_close_hides(GNOME_DIALOG(d), true);
	 if (gnome_dialog_run(GNOME_DIALOG(d)) == 1) {
		  gtk_widget_destroy(d);
		  NewVariant(_retval);
		  VariantSetEmpty(*_retval);
		  return NS_OK;
	 }

	 NewVariant(_retval);
	 GList* dl = GTK_LIST(list)->selection;
	 if (dl == NULL) {
		  VariantSetEmpty(*_retval);
	 } else if (!ms) {
		  VariantSetNumber(*_retval, (int) gtk_object_get_data(GTK_OBJECT(dl->data), "offset"));
	 } else {

		  vector <int> nums;

		  while (dl) {
			   int i = (int) gtk_object_get_data(GTK_OBJECT(dl->data), "offset");
			   nums.push_back(i);
			   dl = dl->next;
		  }
		  VariantSetArray(*_retval, nums);
	 }
		  
	 gtk_widget_destroy(d);
	 return NS_OK;
	 
}
