/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Header file for the Core.UI extension

*****************************************************************/

#ifndef SASHUI_H
#define SASHUI_H

#include "nsID.h"
#include "nsIFactory.h"
#include "sashIUI.h"
#include "SecurityManager.h"
#include <gtk/gtk.h>
#define SASHUI_CID {0x83b715e1, 0xa50e, 0x42be, {0xbf, 0x8e, 0x08, 0x73, 0x70, 0xcd, 0xe8 ,0x0a}}

NS_DEFINE_CID(kSashUICID, SASHUI_CID);

#define SASHUI_CONTRACT_ID "@gnome.org/SashXB/UI;1"

class SashUI : public sashIUI
{
 public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIUI
  
  SashUI();
  virtual ~SashUI();
private:
  void set_default(GtkWidget* d, PRInt32 flags, int max);
};

#endif
