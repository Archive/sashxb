/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

   
 IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, AJ Shankar

Implementation of the core extension

*****************************************************************/

#include <unistd.h>
#include "sashIActionRuntime.h"
#include "extensiontools.h"
#include "sashVariantUtils.h"
#include "debugmsg.h"
#include "sashtools.h"
#include "sashCore.h"
#include "channels/sashChannelCollection.h"
#include "cursor/SashCursor.h"
#include "process/sashProcess.h"
#include "ui/SashUI.h"
#include "net/sashCoreNet.h"
#include "platform/SashPlatform.h"
#include "security.h"
#include "secman.h"
#include "FileSystem.h"
#include "sashINet.h"

SASH_EXTENSION_IMPL_NO_NSGET(sashCore, sashICore, "Core");

sashCore::sashCore() 
{
  NS_INIT_ISUPPORTS();
  DEBUGMSG(core, "Creating core extension\n");
  DEBUGMSG(core, "sashCore pointer: %d\n", (int)this);
  m_chanCol = NULL;
  m_cur = NULL;
  m_proc = NULL;
  m_ui = NULL;
  m_net = NULL;
  m_platform = NULL;
}

sashCore::~sashCore()
{
  DEBUGMSG(core, "Releasing ChannelCollection\n");
  NS_IF_RELEASE(m_chanCol);
  DEBUGMSG(core, "Releasing Cursor\n");
  NS_IF_RELEASE(m_cur);
  DEBUGMSG(core, "Releasing ui\n");
  NS_IF_RELEASE(m_ui);
 DEBUGMSG(core, "Releasing net\n");
 NS_IF_RELEASE(m_net);
  DEBUGMSG(core, "Releasing proc\n");
  NS_IF_RELEASE(m_proc);
  DEBUGMSG(core, "Releasing platform\n");
  NS_IF_RELEASE(m_platform);
  DEBUGMSG(core, "Returning from sashCore destructor\n");
  NS_IF_RELEASE(m_proc);
}

NS_IMETHODIMP 
sashCore::ProcessEvent() {
	 DEBUGMSG(net, "sashCore::ProcessEvent...\n");

	 if (m_net){
		  m_net->ProcessEvent();
	 }
	 return NS_OK;
}

NS_IMETHODIMP 
sashCore::Cleanup() {
  return NS_OK;
}

NS_IMETHODIMP
sashCore::Initialize(sashIActionRuntime *act, 
			   const char *registrationXml, 
			   const char *webGuid,
			   JSContext *cx, JSObject *obj)
{
  DEBUGMSG(core, "initializing core\n");


  // Save all of the initialization parameters
  m_rt = act;
  m_registration = registrationXml;
  m_webguid = webGuid;
  m_cx = cx;
  m_obj = obj;

  act->GetSecurityManager(&m_secMan);
  return NS_OK;
}

NS_IMETHODIMP sashCore::GetChannels(sashIChannelCollection ** chan) {
  nsresult retval;

  if (m_chanCol == NULL) {
	retval = nsComponentManager::CreateInstance(ksashChannelCollectionCID, NULL, 
												NS_GET_IID(sashIChannelCollection),
												(void **) &m_chanCol);
	assert(NS_SUCCEEDED(retval));
	m_chanCol->Initialize(m_rt, m_registration.c_str(), m_webguid.c_str(), m_cx, m_obj);
  }

  NS_ADDREF(m_chanCol);
  *chan = m_chanCol;
  return NS_OK;
}

NS_IMETHODIMP sashCore::GetProcess(sashIProcess ** proc) {
  nsresult retval;

  AssertSecurity(m_secMan, GSS_PROCESS_SPAWNING, true);

  if (m_proc == NULL) {
	retval = nsComponentManager::CreateInstance(kSashProcessCID, NULL, 
												NS_GET_IID(sashIProcess),
												(void **) &m_proc);
	assert(NS_SUCCEEDED(retval));
  }

  NS_ADDREF(m_proc);
  *proc = m_proc;
  return NS_OK;
}

NS_IMETHODIMP sashCore::GetUI(sashIUI ** ui) {
  nsresult retval;

  if (m_ui == NULL) {
	retval = nsComponentManager::CreateInstance(kSashUICID, NULL, 
												NS_GET_IID(sashIUI),
												(void **) &m_ui);
	assert(NS_SUCCEEDED(retval));
  }

  NS_ADDREF(m_ui);
  *ui = m_ui;
  return NS_OK;
}

NS_IMETHODIMP sashCore::GetNet(sashICoreNet ** net) {
  nsresult retval;

  if (m_net == NULL) {
	   DEBUGMSG(net, "creating instance of net\n");
	retval = nsComponentManager::CreateInstance(ksashCoreNetCID, NULL, 
												NS_GET_IID(sashICoreNet),
												(void **) &m_net);
	assert(NS_SUCCEEDED(retval));
	m_net->SetCoreExt((sashIExtension *)this);
	m_net->Initialize(m_rt, m_registration.c_str(), m_webguid.c_str(), m_cx, m_obj);
  }

  NS_ADDREF(m_net);
  *net = m_net;
  return NS_OK;
}


NS_IMETHODIMP sashCore::GetCursor(sashICursor ** cur) {
  nsresult retval;

  if (m_cur == NULL) {
	retval = nsComponentManager::CreateInstance(kSashCursorCID, NULL, 
												NS_GET_IID(sashICursor),
												(void **) &m_cur);
	assert(NS_SUCCEEDED(retval));
  }

  NS_ADDREF(m_cur);
  *cur = m_cur;
  return NS_OK;
}

NS_IMETHODIMP sashCore::GetPlatform(sashIPlatform ** plat) {
  nsresult retval;

  if (m_platform == NULL) {
	retval = nsComponentManager::CreateInstance(kSashPlatformCID, NULL, 
												NS_GET_IID(sashIPlatform),
												(void **) &m_platform);
	assert(NS_SUCCEEDED(retval));
  }

  NS_ADDREF(m_platform);
  *plat = m_platform;
  return NS_OK;
}

/* readonly attribute string WeblicationDataPath; */
NS_IMETHODIMP sashCore::GetWeblicationDataPath(char * *aWeblicationDataPath)
{
	 m_rt->GetDataDirectory(aWeblicationDataPath);
    return NS_OK;
}

/* boolean ActionIsInstalled (in string webguid, in string actguid); */
NS_IMETHODIMP sashCore::ActionIsInstalled(const char *webguid, const char *actguid, PRBool *_retval)
{
	 WeblicationIsInstalled(webguid, _retval);
	 return NS_OK;
}

/* void ExecAction (in string webguid, in string actguid); */
NS_IMETHODIMP sashCore::ExecAction(const char *webguid, const char *actguid)
{
	 AssertSecurity(m_secMan, GSS_ACTION_SPAWNING, true);
	 char* bin;
	 m_rt->GetSashBinaryPath(&bin);
    if (fork() == 0) {
      // check return value!
      DEBUGMSG(sashcore, "Spawning off weblication child process\n");
      DEBUGMSG(sashcore, "Command line: %s %s %s\n", 
	       bin, webguid, actguid);
      execl(bin, bin, ToUpper(webguid).c_str(), ToUpper(actguid).c_str(), NULL);
      _exit(0);
    }

    return NS_OK;
}

/* void ExecDefaultWebBrowser (in string url); */
NS_IMETHODIMP sashCore::ExecDefaultWebBrowser(const char *url)
{
	 AssertSecurity(m_secMan, GSS_RESTART_BROWSER, true);
	 char* datap;
	 m_rt->GetDataDirectory(&datap);

	 if (FileSystem::FileExists(datap + string("/mozilla"))) {
		  // check the path to see if "." is in there
		  string path = getenv("PATH");
		  if (path.find(".") != string::npos && 
			  OutputQuery("The Sash Runtime is trying to start mozilla."
						  " A copy of mozilla was found in the weblication's data directory."
						  " You appear have '.' in your path, so there is a chance"
						  " that the weblication's (potentially malicious) copy may"
						  " be executed instead of the system-wide copy. It is"
						  " strongly suggested that you abort this weblication."
						  " Do you want to abort?"))
			   _exit(0);
	 }

	 if (fork() == 0) {
		  execlp("mozilla", "mozilla", url, NULL);
		  _exit(0);
	 }
	 return NS_OK;
}

/* string GetNewGUID (); */
NS_IMETHODIMP sashCore::GetNewGUID(char **_retval)
{
	 return m_rt->GenerateGUID(_retval);
}

/* string GetSashVersion (); */
NS_IMETHODIMP sashCore::GetSashVersion(char **_retval)
{
	 XP_COPY_STRING(SASH_VERSION, _retval);
	 return NS_OK;
}

/* string PrintFile (in string file); */
NS_IMETHODIMP sashCore::PrintFile(const char *file, char **_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void Sleep (in PRInt32 ms); */
NS_IMETHODIMP sashCore::Sleep(PRInt32 ms)
{
	 usleep(ms*1000);
	 return NS_OK;
}

/* boolean WeblicationIsInstalled (in string guid); */
NS_IMETHODIMP sashCore::WeblicationIsInstalled(const char *guid, PRBool *_retval)
{
	 m_rt->WeblicationIsInstalled(guid, _retval);
	 return NS_OK;
}

NS_IMETHODIMP sashCore::Include(const char *filename, nsIVariant **_retval) {
	 char* data;
	 PRUint32 size;
	 bool from_ns = false;
	 if (URLHasProtocol(filename)) {
		  DEBUGMSG(sashcore, "downloading include file %s from web\n", filename);
		  AssertSecurity(m_secMan, GSS_DOWNLOAD, true);
		  nsCOMPtr<sashIDownloader> d;
		  NewSashDownloader(filename, getter_AddRefs(d));
		  d->StartTransfer();
		  PRInt32 status;
		  d->ProcessUntilComplete(&status);
		  if (status != sashINet::NET_DONE) {
			   DEBUGMSG(sashcore, "failed download\n");
			   NewVariant(_retval);
			   VariantSetEmpty(*_retval);
			   return NS_OK;
		  }
		  d->GetResponseData(&size, &data);
		  
		  from_ns = true;
		  
	 } else {
		  DEBUGMSG(sashcore, "getting include file %s locally\n", filename);
		  m_rt->AssertFSAccess(filename);
		  FILE* f = fopen(filename, "r");
		  if (! f) {
			   NewVariant(_retval);
			   VariantSetEmpty(*_retval);
			   return NS_OK;
		  }
		  // read in the whole file
		  fseek(f, 0, SEEK_END);
		  size = ftell(f);
		  fseek(f, 0, SEEK_SET);
 
		  data = new char[size];
		  fread(data, sizeof(char), size, f);
	 }

    jsval result;
	JSBool ok = JS_EvaluateScript(m_cx, JS_GetGlobalObject(m_cx),
								  data, size, filename, 0, &result);

	if (!ok) {
		 DEBUGMSG(sashcore, "eval-ing include file failed\n");
		 
		 NewVariant(_retval);
		 VariantSetEmpty(*_retval);
	} else {
		 m_rt->CreateVariantFromJSVal(m_cx, result, _retval);
	}
	if (from_ns) {
		 nsMemory::Free(data);
	} else {
		 delete[] data;
	}
	return NS_OK;

}
