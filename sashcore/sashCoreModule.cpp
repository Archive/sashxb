
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000,2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142

    USA

*****************************************************************

Contributor(s): John Corwin

module for the SashFileSystem
******************************************************************/

#include "sashCore.h"

#include "channels/sashChannel.h"
#include "channels/sashChannelCollection.h"
#include "channels/sashChannelMessage.h"
#include "cursor/SashCursor.h"
#include "process/sashProcess.h"
#include "ui/SashUI.h"
#include "net/sashCoreNet.h"
#include "net/sashURLConnection.h"
#include "net/sashURLConnectionHeaders.h"
#include "platform/SashPlatform.h"

#include "extensiontools.h"

NS_GENERIC_FACTORY_CONSTRUCTOR(sashCore);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashChannelCollection);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashChannel);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashChannelMessage);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashCursor);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashProcess);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashUI);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashCoreNet);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashURLConnection);
NS_GENERIC_FACTORY_CONSTRUCTOR(sashURLConnectionHeaders);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashPlatform);

NS_DECL_CLASSINFO(sashCore);
NS_DECL_CLASSINFO(sashChannelCollection);
NS_DECL_CLASSINFO(sashChannel);
NS_DECL_CLASSINFO(sashChannelMessage);
NS_DECL_CLASSINFO(SashCursor);
NS_DECL_CLASSINFO(sashProcess);
NS_DECL_CLASSINFO(SashUI);
NS_DECL_CLASSINFO(sashCoreNet);
NS_DECL_CLASSINFO(sashURLConnection);
NS_DECL_CLASSINFO(URLConnectionDownloadCallback);
NS_DECL_CLASSINFO(sashURLConnectionHeaders);
NS_DECL_CLASSINFO(SashPlatform);


static nsModuleComponentInfo components[] = {
	 MODULE_COMPONENT_ENTRY(sashCore, SASHCORE, "Sash Core extension"),
	 MODULE_COMPONENT_ENTRY(sashChannelCollection, SASHCHANNELCOLLECTION, 
							"Sash Channels"),
	 MODULE_COMPONENT_ENTRY(sashChannel, SASHCHANNEL, "Sash Channel Object"),
	 MODULE_COMPONENT_ENTRY(sashChannelMessage, SASHCHANNELMESSAGE, 
							"Sash Channel Message"),
	 MODULE_COMPONENT_ENTRY(SashCursor, SASHCURSOR, "Sash Cursor"),
	 MODULE_COMPONENT_ENTRY(sashProcess, SASHPROCESS, "Sash Process"),
	 MODULE_COMPONENT_ENTRY(SashUI, SASHUI, "Sash UI"),
	 MODULE_COMPONENT_ENTRY(sashCoreNet, SASHCORENET, "Sash Core Net"),
	 MODULE_COMPONENT_ENTRY(sashURLConnection, SASHURLCONNECTION, 
							"Sash URL Connection"),
	 MODULE_COMPONENT_ENTRY(sashURLConnectionHeaders, SASHURLCONNECTIONHEADERS,
							"Sash URL Connection Headers"),
	 MODULE_COMPONENT_ENTRY(SashPlatform, SASHPLATFORM, "Sash Platform")
};

NS_IMPL_NSGETMODULE(sashCore, components)
