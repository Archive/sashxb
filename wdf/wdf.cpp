
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

// AJ Shankar

#include "wdf.h"
#include "Registry.h"
#include "FileSystem.h"
#include "XMLDocument.h"
#include "XMLNode.h"
#include "sash_constants.h"
#include <iostream.h>
#include "debugmsg.h"
#include <string>
#include <vector>
#include <strstream>

using namespace std;

const string WDF_TRUE ("true");
const string WDF_FALSE ("false");

const string WDF_ID ("id");
const string WDF_EXTENSIONTYPE ("sashextension");

const string WDF_FILEID ("fileid");
const string WDF_FILEID_NAME ("name");
const string WDF_FILEID_LOCATION ("location");
const string WDF_FILES_FILE_PRECACHE ("precache");
const string WDF_FILES ("files");
const string WDF_FILES_FILE ("file");

const string WDF_UPDATE("update");
const string WDF_UPDATE_CHECK ("check");
const string WDF_UPDATE_NOTIFY ("notify");
const string WDF_UPDATE_FREQUENCY ("frequency");

const string WDF_VERSION ("version");
const string WDF_VERSION_SASH ("sash");
const string WDF_VERSION_MOZILLA ("mozilla");
const string WDF_VERSION_WEBLICATION ("weblication");
const string WDF_VERSION_MINIMUM ("minimumversion");
const string WDF_VERSION_RECOMMENDED ("recommendedversion");
const string WDF_VERSION_MAJOR ("major");
const string WDF_VERSION_MAJOR1 ("major1");
const string WDF_VERSION_MINOR ("minor");
const string WDF_VERSION_MAINTENANCE ("maintenance");

const string WDF_SECURITY ("security");
const string WDF_SECURITYEXTENSIONS ("securityextensions");
const string WDF_SECURITYINFOS ("securityinfos");
const string WDF_SECURITYINFO ("securityinfo");
const string WDF_SECURITYVALUES ("securityvalues");
const string WDF_SECURITYVALUE ("securityvalue");
const string WDF_SECURITYEXTENSIONID ("extensionid");
const string WDF_SECURITYINFO_TYPE ("type");
const string WDF_SECURITYINFO_NAME ("name");
const string WDF_SECURITYINFO_ID ("id");
const string WDF_SECURITY_GLOBAL ("Global");

const string WDF_PLATFORMS_LIST ("platforms");
const string WDF_PLATFORM ("platform");

const string WDF_TITLE ("title");
const string WDF_ABSTRACT ("abstract");
const string WDF_AUTHOR ("author");

const string WDF_EXPIRATION ("expiration");
const string WDF_EXPIRATION_DATE ("date");
const string WDF_EXPIRATION_TIME ("time");
const string WDF_EXPIRATION_DAYS ("days");

const string WDF_DEPENDENCIES ("dependencies");
const string WDF_DEPENDENCY ("weblication");
const string WDF_DEPENDENCY_TITLE ("title");
const string WDF_DEPENDENCY_CONTENT ("content");
const string WDF_DEPENDENCY_ID ("id");
const string WDF_DEPENDENCY_CONTENT_FILENAME ("filename");

const string WDF_INSTALL ("install");
const string WDF_INSTALL_SCREEN_TITLE_ATTR ("title");
const string WDF_INSTALL_SCREEN_TYPE ("type");
const string WDF_INSTALL_SCREEN_PAGE ("page");
const string WDF_INSTALL_SCREEN_IMAGE ("image");
const string WDF_INSTALL_SCREEN_TEXT ("text");
const string WDF_INSTALL_SCREEN_BACK_ATTR ("back");
const string WDF_INSTALL_SCREEN_NEXT_ATTR ("next");

const string WDF_ACTION_ACTION_ID ("id");
const string WDF_ACTION_NAME ("name");
const string WDF_ACTION_REGISTRATION ("registration");
const string WDF_ACTION_WEBLICATION_ID ("weblicationid");
const string WDF_ACTION_LIST ("packageextensions");

const string WDF_REGISTRY("regkeys");
const string WDF_REGISTRY_KEY("key");
const string WDF_REGISTRY_KEY_ID("id");
const string WDF_REGISTRY_VALUE("value");
const string WDF_REGISTRY_VALUE_DATA("data");
const string WDF_REGISTRY_VALUE_NAME("name");
const string WDF_REGISTRY_VALUE_TYPE("type");
const string WDF_REGISTRY_VALUES("values");

const string WDF_CHECK ("h@x0r");
const string WDF_CHECK_KEY ("wdf-check");

static const char *SashExtensionTypeString[] = {
  "weblication",
  "extension",
  "location",
};

static const char *InstallScreenTypeString[] = {
  "textpage",
  "license",
  "custom",
  "postsecurity",
  "failure",
  "security",
  "success",
  "script"
};

static const char *FileLocationTypeString[] = {
  "weblication",
  "cache",
  "weblicationpath",
  "sharedlocation",
  "server"
};

static const char *GlobalSecuritySettingString[] = {
  "registryaccess",
  "channeluse",
  "processspawning",
  "print",
  "clipboard",
  "register",
  "downloadsecurity",
  "execruntime",
  "fileio",
  "restartexplorer",
  "javapolicy",
  "rpc"
};


static const char *SecurityTypeString[] = {
  "boolean",
  "number",
  "string",
  "string-enumeration",
  "string-array"
};

// this is an ugly hack, mapping windows registry types to ours
// if you modify these values, please take care to verify that the 
// ConvertRegistry function still works
static const char *WindowsRegistryValueTypeString[] = {
	 "reg_sz",
	 "reg_dword"
};

string int2String(int i){
	 string ret = "";
	 if (i<1000){
		  char num[4];
		  snprintf(num, 3, "%d", i);
		  ret = num;
	 }
	 return ret;
}

typedef bool(*ConvFunc)(const XMLNode, const string&, Registry*);

using namespace std;


GHashTable* WDF::s_pHash = g_hash_table_new(g_str_hash, g_str_equal);


WDF::WDF() {
	 m_pRegistry = new Registry();
	 if (g_hash_table_size(s_pHash) < 1) {
		  DEBUGMSG(wdf, "Initializing function hash table... \n");

		  g_hash_table_insert(s_pHash, (void*)WDF_SECURITY.c_str(), (void*)ConvertSecurity);
		  g_hash_table_insert(s_pHash, (void*)WDF_PLATFORMS_LIST.c_str(), (void*)ConvertPlatforms);
		  g_hash_table_insert(s_pHash, (void*)WDF_DEPENDENCIES.c_str(), (void*)ConvertDependencies);
		  g_hash_table_insert(s_pHash, (void*)WDF_INSTALL.c_str(), (void*)ConvertInstallScreens);
		  g_hash_table_insert(s_pHash, (void*)WDF_ACTION_LIST.c_str(), (void*)ConvertActions);
		  g_hash_table_insert(s_pHash, (void*)WDF_FILES.c_str(), (void*)ConvertFiles);
		  g_hash_table_insert(s_pHash, (void*)WDF_REGISTRY.c_str(), (void*)ConvertRegistry);
	 }
};

WDF::~WDF() {
	 delete m_pRegistry;
}

bool WDF::OpenAmbiguousFile(const string& filename) { 
	 DEBUGMSG(wdf, "Detecting file %s\n", filename.c_str());
	 if (!FileSystem::FileExists(filename))
		  return false;

	 if (! LoadFromFile(filename)) {
		  if (! ConvertFromWDFDoc(filename))
			   return false;
	 }
	 return true;
}

bool WDF::LoadFromFile(const string& filename) {
	 if (m_pRegistry) delete m_pRegistry;
	 
	 m_pRegistry = new Registry();
	 if (!m_pRegistry->OpenFile(filename))
		  return false;
	 
	 if (!TestWDFCheck()) {
		  return false;
	 }
	 return true;
}

void WDF::SaveToFile(const string& filename) {
	 SetWDFCheck();
	 m_pRegistry->WriteToDisk(filename);
}

void WDF::Clear() {
  if (m_pRegistry) delete m_pRegistry;
  
  m_pRegistry = new Registry();
}

// -----------------------------------------------------

bool WDF::ConvertFromWDFDoc(const string& filename) {
	 DEBUGMSG(wdf, "Found; parsing XML\n");
	 XMLDocument d;
	 if (! d.loadFromFile(filename)) return false;

	 DEBUGMSG(wdf, "Verifying Windows WDF format\n");
	 XMLNode n = d.getRootNode();
	 if (n.getName() != "wdf") return false;

	 DEBUGMSG(wdf, "Okay; converting to WDF registry\n");
	 if (m_pRegistry) delete m_pRegistry;
	 m_pRegistry = new Registry();

	 // now just recursively go and dump stuff into the registry
	 return ConvertNode(n, "", m_pRegistry);
}

bool WDF::ConvertNode(const XMLNode d, const string& path, Registry* const r) {
	 // first, write out its attributes as values
	 // if it has text associated with it, write that as the default value
	 XMLString s = d.getText();
  
	 if (s != "") {
		  StaticSetValue(r, s, path);
	 }
	 vector<string> n = d.getAttributeNames();
	 vector<string>::iterator ai = n.begin(), bi = n.end();
	 while (ai != bi) {
		  StaticSetValue(r, d.getAttribute(*ai), path, *ai);
		  ++ai;
	 }
  
	 // then, call convert node recursively on its children
	 vector<XMLNode> xn = d.getChildNodes();
	 vector<XMLNode>::iterator xai = xn.begin(), xbi = xn.end();
  
	 while (xai != xbi) {
		  string np = xai->getName();
		  // make sure there's no special function to handle this node
		  void* gf = g_hash_table_lookup(s_pHash, ToLower(np).c_str());
		  if (gf) {
			   DEBUGMSG(wdf, "Calling special conversion function for %s\n", np.c_str());
			   ConvFunc func = (ConvFunc) gf;
			   if (! func(*xai, (path + "\\" + np), r)) return false;
		  } else {
			   if (! ConvertNode(*xai, path + "\\" + np, r)) return false;
		  } 
		  ++xai;
	 }
	 return true;
}

bool WDF::ConvertSecurity(const XMLNode n, const string& path, Registry* const r) {
	 // first do the global values
	 vector<string> g = n.getAttributeNames();
	 vector<string>::iterator aig = g.begin(), big = g.end();
	 while (aig != big) {
		  int idx = MatchString(*aig, GlobalSecuritySettingString, GSS_NUM_VALUES);
		  if (idx < 0) {
			   ++aig;
			   continue;
		  }
		  char* ch = g_strdup_printf("%d", idx);
		  string np = path + "\\" + WDF_SECURITY_GLOBAL + "\\" + ch;
		  g_free(ch);
		  StaticSetValue(r, n.getAttribute(*aig), np);
		  ++aig;
	 }

	 // then do all the extension-specific ones
	 vector<XMLNode> e = n.getFirstChildNode(WDF_SECURITYEXTENSIONS).getChildNodes(WDF_SECURITYINFOS);
	 vector<XMLNode>::iterator ai = e.begin(), bi = e.end();
	 // for each extension...
	 while (ai != bi) {
		  string eid = ai->getAttribute(WDF_SECURITYEXTENSIONID);
		  string np = path + "\\" + ToUpper(eid);
		  vector<XMLNode> si = ai->getChildNodes(WDF_SECURITYINFO);
		  vector<XMLNode>::iterator air = si.begin(), bir = si.end();
		  // for each extension setting...
		  while (air != bir) {
			   string p = np + "\\" + air->getAttribute(WDF_SECURITYINFO_ID);
			   string type = air->getAttribute(WDF_SECURITYINFO_TYPE);
			   if (type != "") StaticSetValue(r, type, p, WDF_SECURITYINFO_TYPE);
			   string descrip = air->getFirstChildNode(WDF_SECURITYINFO_NAME).getText();
			   if (descrip != "") StaticSetValue(r, descrip, p, WDF_SECURITYINFO_NAME);
			   vector<XMLNode> values = air->getFirstChildNode(WDF_SECURITYVALUES).getChildNodes(WDF_SECURITYVALUE);
			   if (values.size() == 1) {
					StaticSetValue(r, values[0].getText(), p);
			   } else {
					vector<string> svals;
					vector<XMLNode>::iterator aiv = values.begin(), biv = values.end();
					// for each extension setting value...
					while (aiv != biv) {
						 svals.push_back(aiv->getText());
						 ++aiv;
					}
					RegistryValue rv;
					rv.SetValueType(RVT_ARRAY);
					rv.m_Array = svals;
					r->SetValue(p, "", &rv);
			   }
			   ++air;
		  }
		  ++ai;
	 }
	 return true;
}

bool WDF::ConvertDependencies(const XMLNode n, const string& path, Registry* const r) {
	 vector<XMLNode> xn = n.getChildNodes(WDF_DEPENDENCY);
	 vector<XMLNode>::iterator ai = xn.begin(), bi = xn.end();

	 while (ai != bi) {
		  string id = ToUpper(ai->getAttribute(WDF_DEPENDENCY_ID));
		  r->CreateKey(path + "\\" + id);
		  vector<XMLNode> xni = ai->getChildNodes();
		  vector<XMLNode>::iterator aii = xni.begin(), bii = xni.end();
		  while (aii != bii) {
			   ConvertNode(*aii, path + "\\" + id + "\\" + aii->getName(), r);
			   ++aii;
		  }
		  ++ai;
	 }
	 return true;
}

bool WDF::ConvertPlatforms(const XMLNode n, const string& path, Registry* const r) {
	 RegistryValue rv;
	 rv.SetValueType(RVT_ARRAY);
	 vector<XMLNode> xn = n.getChildNodes();
	 vector<XMLNode>::iterator ai = xn.begin(), bi = xn.end();
	 while (ai != bi) {
		  string s = ai->getText();
		  if (s != "")
			   rv.m_Array.push_back(s);
		  ++ai;
	 }
	 r->SetValue(path, "", &rv);
	 return true;
}

bool WDF::ConvertInstallScreens(const XMLNode n, const string& path, Registry* const r) {
	 char id = 'A';
	 vector<XMLNode> a = n.getChildNodes();
	 vector<XMLNode>::iterator ai = a.begin(), bi = a.end();
	 while (ai != bi) {
		  string name = ai->getName();
		  if (MatchString(name, InstallScreenTypeString, IST_NUM_VALUES) != IST_UNDEFINED) {
			   string np = path + "\\" + id;
			   StaticSetValue(r, name, np, WDF_INSTALL_SCREEN_TYPE);
			   ConvertNode(*ai, np, r);  
			   id++;
		  }
		  ++ai;

	 }
	 return true;
}

bool WDF::ConvertActions(const XMLNode n, const string& path, Registry* const r) {
	 vector<XMLNode> a = n.getChildNodes();
	 vector<XMLNode>::iterator ai = a.begin(), bi = a.end();
	 while (ai != bi) {
		  string id = ToUpper(ai->getAttribute(WDF_ACTION_ACTION_ID));
		  string np = path + "\\" + id;

			if (ai->isElement()) {
				StaticSetValue(r, ToUpper(ai->getAttribute(WDF_ACTION_WEBLICATION_ID)), np, WDF_ACTION_WEBLICATION_ID);
				StaticSetValue(r, ai->getFirstChildNode(WDF_ACTION_NAME).getText(),
											 np, WDF_ACTION_NAME);
				StaticSetValue(r, ai->getFirstChildNode(WDF_ACTION_REGISTRATION).getXML(),
											 np, WDF_ACTION_REGISTRATION);
			}
		  ++ai;
	 }
	 return true;
}

bool WDF::ConvertFiles(const XMLNode n, const string& path, Registry* const r) {
	 vector<XMLNode> a = n.getChildNodes(WDF_FILES_FILE);
	 vector<XMLNode>::iterator ai = a.begin(), bi = a.end();
	 while (ai != bi) {
		  string name = ai->getFirstChildNode(WDF_FILEID).getAttribute(WDF_FILEID_NAME);
		  string np = path + "\\" + name;
		  StaticSetValue(r, ai->getFirstChildNode(WDF_FILEID).getAttribute(WDF_FILEID_LOCATION), np, WDF_FILEID_LOCATION);
		  StaticSetValue(r, ai->getAttribute(WDF_FILES_FILE_PRECACHE), np, WDF_FILES_FILE_PRECACHE);
		  ++ai;
	 }
	 return true;
}

bool WDF::ConvertRegistry(const XMLNode n, const string& path, Registry* const r) {
	 vector<XMLNode> blooga = n.getChildNodes(WDF_REGISTRY_KEY);
	 vector<XMLNode>::iterator ai = blooga.begin(), bi = blooga.end();
	 while (ai != bi) {
		  ConvertRegistryKey(*ai, path + "\\" + ai->getAttribute(WDF_REGISTRY_KEY_ID), r);
		  ++ai;
	 }
	 return true;
}

bool WDF::ConvertRegistryKey(const XMLNode n, const string& path, Registry* const r) {
	 // first, convert all values
	 XMLNode v = n.getFirstChildNode(WDF_REGISTRY_VALUES);
	 vector<XMLNode> vn = v.getChildNodes(WDF_REGISTRY_VALUE);
	 vector<XMLNode>::iterator ai = vn.begin(), bi = vn.end();
	 while (ai != bi) {
		  RegistryValue rv;
		  string name = ai->getAttribute(WDF_REGISTRY_VALUE_NAME);
		  // this is an ugly hack, mapping windows registry types to ours
		  // if you change these values, please verify that this function still works
		  rv.SetValueType((RegistryValueType)MatchString(ai->getAttribute(WDF_REGISTRY_VALUE_TYPE), 
														 WindowsRegistryValueTypeString, 3));
		  RegistryValueType rvt = rv.GetValueType();
		  if (rvt != RVT_INVALID) {
			   if (rv.GetValueType() == RVT_NUMBER) {
					rv.m_Number = strtod(ai->getAttribute(WDF_REGISTRY_VALUE_DATA).c_str(), NULL);
			   } else {
					string d = ai->getAttribute(WDF_REGISTRY_VALUE_DATA);
					if (d == "") {
						 vector<string> booga;
						 // get all the child data nodes (could this get any more annoying?)
						 vector<XMLNode> ooga = ai->getChildNodes(WDF_REGISTRY_VALUE_DATA);
						 vector<XMLNode>::iterator ai = ooga.begin(), bi = ooga.end();
						 while (ai != bi) {
							  booga.push_back(ai->getText());
							  ++ai;
						 }
						 if (booga.size() == 1) {
							  rv.m_String = booga[0];
						 } else if (booga.size() > 1) {
							  rv.SetValueType(RVT_ARRAY);
							  rv.m_Array = booga;
						 }
					} else {
						 rv.m_String = d;
					}
			   } 
			   r->SetValue(path, name, &rv);
		  }
		  ++ai;
	 }
	 if (n.hasChildNodes(WDF_REGISTRY)){
		  ConvertRegistry(n.getFirstChildNode(WDF_REGISTRY), path, r);
	 }
	 return true;
}

// -----------------------------------------------------

string WDF::GetID() const {
	 return ToUpper(GetValue("\\", WDF_ID));
}

void WDF::SetID(const string& i) {
	 SetValue(i, "\\", WDF_ID);
}

SashExtensionType WDF::GetExtensionType() const {
	 string s = GetValue("\\", WDF_EXTENSIONTYPE);
	 return (SashExtensionType) MatchString(s, SashExtensionTypeString, SET_NUM_VALUES);
}
	
void WDF::SetExtensionType(SashExtensionType s) {
	 assert(s >= 0 && s < SET_NUM_VALUES);
	 SetValue(SashExtensionTypeString[s], "\\", WDF_EXTENSIONTYPE);
}

vector<string> WDF::GetPlatforms() const {
	 RegistryValue rv;
	 m_pRegistry->QueryValue(WDF_PLATFORMS_LIST, "", &rv);
	 return rv.m_Array;
}

void WDF::SetPlatforms(const vector<string>& v) {
	 RegistryValue rv;
	 rv.SetValueType(RVT_ARRAY);
	 rv.m_Array = v;
	 m_pRegistry->SetValue(WDF_PLATFORMS_LIST, "", &rv);
}


WDFExpiration WDF::GetExpiration() const {
	 WDFExpiration w;
	 w.date = GetValue(WDF_EXPIRATION+"\\"+WDF_EXPIRATION_DATE);
	 w.time = GetValue(WDF_EXPIRATION+"\\"+WDF_EXPIRATION_TIME);
	 w.days = GetValue(WDF_EXPIRATION+"\\"+WDF_EXPIRATION_DAYS);
	 return w;

}
void WDF::SetExpiration(const WDFExpiration& w) {
	 SetValue(w.date, WDF_EXPIRATION+"\\"+WDF_EXPIRATION_DATE);
	 SetValue(w.time, WDF_EXPIRATION+"\\"+WDF_EXPIRATION_TIME);
	 SetValue(w.days, WDF_EXPIRATION+"\\"+WDF_EXPIRATION_DAYS);
}

WDFUpdate WDF::GetUpdate() const {
	 WDFUpdate w;
	 w.check = IsTrue(GetValue(WDF_UPDATE, WDF_UPDATE_CHECK));
	 w.notify = IsTrue(GetValue(WDF_UPDATE, WDF_UPDATE_NOTIFY));
	 w.frequency = GetValue(WDF_UPDATE + "\\" + WDF_UPDATE_FREQUENCY);
	 return w;
}

void WDF::SetUpdate(const WDFUpdate& u) {
	 SetValue(BoolToString(u.check), WDF_UPDATE, WDF_UPDATE_CHECK);
	 SetValue(BoolToString(u.notify), WDF_UPDATE, WDF_UPDATE_NOTIFY);
	 SetValue(u.frequency, WDF_UPDATE + "\\" + WDF_UPDATE_FREQUENCY);
}

WDFVersion WDF::GetVersion() const {
	 WDFVersion v;
	 v.weblication = GetOneVersion(WDF_VERSION + "\\" + WDF_VERSION_WEBLICATION);
	 v.sash.minimum = Version(GetValue(WDF_VERSION + "\\" + WDF_VERSION_SASH, WDF_VERSION_MINIMUM));
	 v.sash.recommended = Version(GetValue(WDF_VERSION + "\\" + WDF_VERSION_SASH, WDF_VERSION_RECOMMENDED));
	 v.mozilla.minimum = Version(GetValue(WDF_VERSION + "\\" + WDF_VERSION_MOZILLA, WDF_VERSION_MINIMUM));
	 v.mozilla.recommended = Version(GetValue(WDF_VERSION + "\\" + WDF_VERSION_MOZILLA, WDF_VERSION_RECOMMENDED));
	 return v;
}

void WDF::SetVersion(const WDFVersion& v) {
	 SetOneVersion(v.weblication, WDF_VERSION + "\\" + WDF_VERSION_WEBLICATION);
	 SetValue(v.sash.minimum.ToString(), WDF_VERSION + "\\" + WDF_VERSION_SASH, WDF_VERSION_MINIMUM);
	 SetValue(v.sash.recommended.ToString(), WDF_VERSION + "\\" + WDF_VERSION_SASH, WDF_VERSION_RECOMMENDED);
	 SetValue(v.mozilla.minimum.ToString(), WDF_VERSION + "\\" + WDF_VERSION_MOZILLA, WDF_VERSION_MINIMUM);
	 SetValue(v.mozilla.recommended.ToString(), WDF_VERSION + "\\" + WDF_VERSION_MOZILLA, WDF_VERSION_RECOMMENDED);
}

Version WDF::GetOneVersion(const string& key) const {
	 Version v;
	 RegistryValue rv;
	 m_pRegistry->QueryValue(key, WDF_VERSION_MAJOR, &rv);
	 v.major = (unsigned int) rv.m_Number;
	 m_pRegistry->QueryValue(key, WDF_VERSION_MAJOR1, &rv);
	 v.major1 = (unsigned int)  rv.m_Number;
	 m_pRegistry->QueryValue(key, WDF_VERSION_MINOR, &rv);
	 v.minor = (unsigned int) rv.m_Number;
	 m_pRegistry->QueryValue(key, WDF_VERSION_MAINTENANCE, &rv);
	 v.maintenance = (unsigned int) rv.m_Number;
	 return v;
}

void WDF::SetOneVersion(const Version& v, const string& key) {
	 RegistryValue rv;
	 rv.SetValueType(RVT_NUMBER);
	 rv.m_Number = v.major;
	 m_pRegistry->SetValue(key, WDF_VERSION_MAJOR, &rv);
	 rv.m_Number = v.major1;
	 m_pRegistry->SetValue(key, WDF_VERSION_MAJOR1, &rv);
	 rv.m_Number = v.minor;
	 m_pRegistry->SetValue(key, WDF_VERSION_MINOR, &rv);
	 rv.m_Number = v.maintenance;
	 m_pRegistry->SetValue(key, WDF_VERSION_MAINTENANCE, &rv);
}

string WDF::GetAuthor() const {
	 return GetValue(WDF_AUTHOR);
}

void WDF::SetAuthor(const string& s) {
	 SetValue(s, WDF_AUTHOR);
}

string WDF::GetTitle() const {
	 return GetValue(WDF_TITLE);
}

void WDF::SetTitle(const string& s) {
	 SetValue(s, WDF_TITLE);
}

string WDF::GetAbstract() const {
	 return GetValue(WDF_ABSTRACT);
}

void WDF::SetAbstract(const string& s) {
	SetValue(s, WDF_ABSTRACT);
}

vector<WDFDependency> WDF::GetDependencies() const {
	 vector<WDFDependency> d;
	 vector<string> s = m_pRegistry->EnumKeys(WDF_DEPENDENCIES);
	 vector<string>::iterator ai = s.begin(), bi = s.end();
	 while (ai != bi) {
		  WDFDependency i;
		  i.id = InPlaceToUpper(*ai);
		  i.filename = GetValue(WDF_DEPENDENCIES + "\\" + *ai + "\\" + WDF_DEPENDENCY_CONTENT,
								WDF_DEPENDENCY_CONTENT_FILENAME);
		  i.title = GetValue(WDF_DEPENDENCIES + "\\" + *ai + "\\" + WDF_DEPENDENCY_TITLE, "");
		  i.required_version = GetOneVersion(WDF_DEPENDENCIES + "\\" + *ai + "\\" + WDF_VERSION_WEBLICATION);
		  d.push_back(i);
		  ++ai;
	 }
	 return d;
}
	
void WDF::SetDependencies(vector<WDFDependency>& v) {
	 // clean up old dependencies
	 m_pRegistry->DeleteKey(WDF_DEPENDENCIES, true);
	 vector<WDFDependency>::iterator ai = v.begin(), bi = v.end();
	 while (ai != bi) {
		  AddDependency(*ai);
		  ++ai;
	 }
}

void WDF::AddDependency(const WDFDependency& w) {
	 SetValue(w.filename, WDF_DEPENDENCIES + "\\" + ToUpper(w.id) + "\\" + WDF_DEPENDENCY_CONTENT, 
			  WDF_DEPENDENCY_CONTENT_FILENAME);
	 SetValue(w.title, WDF_DEPENDENCIES + "\\" + ToUpper(w.id) + "\\" + WDF_DEPENDENCY_TITLE, "");
	 SetOneVersion(w.required_version, WDF_DEPENDENCIES + "\\" + ToUpper(w.id) + "\\" + WDF_VERSION_WEBLICATION);
}

bool WDF::RemoveDependency(const string& id) {
	 string up = ToUpper(id);
	 vector<string> s = m_pRegistry->EnumKeys(WDF_DEPENDENCIES);
	 vector<string>::iterator ai = s.begin(), bi = s.end();
	 while (ai != bi) {
		  if (InPlaceToUpper(*ai) == up) {
			   m_pRegistry->DeleteKey(WDF_DEPENDENCIES + "\\" + up, true);
			   return true;
		  }
		  ++ai;
	 }
	 return false;
}

vector<WDFInstallScreen> WDF::GetInstallScreens() const {
	 vector<WDFInstallScreen> d;
	 vector<string> s = m_pRegistry->EnumKeys(WDF_INSTALL);
	 vector<string>::iterator ai = s.begin(), bi = s.end();
	 while (ai != bi) {
		  WDFInstallScreen i;
		  string np = WDF_INSTALL + "\\" + *ai;
		  i.type = (InstallScreenType) MatchString(GetValue(np, WDF_INSTALL_SCREEN_TYPE),
							   InstallScreenTypeString, IST_NUM_VALUES);
		  i.title = GetValue(np, WDF_INSTALL_SCREEN_TITLE_ATTR);
		  i.next = IsTrue(GetValue(np, WDF_INSTALL_SCREEN_NEXT_ATTR));
		  i.back = IsTrue(GetValue(np, WDF_INSTALL_SCREEN_BACK_ATTR));
		  i.page.file = GetValue(np + "\\" + WDF_INSTALL_SCREEN_PAGE + "\\" + WDF_FILEID, WDF_FILEID_NAME);
		  i.page.location = (FileLocationType) MatchString(GetValue(np + "\\" + WDF_INSTALL_SCREEN_PAGE +
																	"\\" + WDF_FILEID, WDF_FILEID_LOCATION),
														   FileLocationTypeString, FLT_NUM_VALUES);

		  i.image.file = GetValue(np + "\\" + WDF_INSTALL_SCREEN_IMAGE + "\\" + WDF_FILEID, WDF_FILEID_NAME);
		  i.image.location = (FileLocationType) MatchString(GetValue(np + "\\" + WDF_INSTALL_SCREEN_IMAGE +
																	 "\\" + WDF_FILEID, WDF_FILEID_LOCATION),
															FileLocationTypeString, FLT_NUM_VALUES);

		  i.text.file = GetValue(np + "\\" + WDF_INSTALL_SCREEN_TEXT + "\\" + WDF_FILEID, WDF_FILEID_NAME);
		  i.text.location = (FileLocationType) MatchString(GetValue(np + "\\" + WDF_INSTALL_SCREEN_TEXT + 
																	"\\" + WDF_FILEID, WDF_FILEID_LOCATION),
														   FileLocationTypeString, FLT_NUM_VALUES);

		  d.push_back(i);
		  ++ai;
	 }
	 return d;
}
	
void WDF::SetInstallScreens(vector<WDFInstallScreen>& v) {
	 char name = 'A';
	 m_pRegistry->DeleteKey(WDF_INSTALL, true);
	 vector<WDFInstallScreen>::iterator ai = v.begin(), bi = v.end();
	 
	 while (ai != bi) {
		  string np = WDF_INSTALL + "\\" + name;
		  assert(ai->type >= 0 && ai->type < IST_NUM_VALUES);
		  SetValue(InstallScreenTypeString[ai->type], np, WDF_INSTALL_SCREEN_TYPE);
		  SetValue(ai->title, np, WDF_INSTALL_SCREEN_TITLE_ATTR);
		  SetValue(BoolToString(ai->next), np, WDF_INSTALL_SCREEN_NEXT_ATTR);
		  SetValue(BoolToString(ai->back), np, WDF_INSTALL_SCREEN_BACK_ATTR);
		  if (ai->page.file != "") {
			   assert(ai->page.location >= 0 && ai->page.location < FLT_NUM_VALUES);
			   SetValue(ai->page.file, np + "\\" + WDF_INSTALL_SCREEN_PAGE + "\\" + WDF_FILEID, WDF_FILEID_NAME);
			   SetValue(FileLocationTypeString[ai->page.location], 
						np + "\\" + WDF_INSTALL_SCREEN_PAGE + "\\" + WDF_FILEID, WDF_FILEID_LOCATION);
		  }
		  if (ai->image.file != "") {
			   assert(ai->page.location >= 0 && ai->page.location < FLT_NUM_VALUES);
			   SetValue(ai->image.file, np + "\\" + WDF_INSTALL_SCREEN_IMAGE + "\\" + WDF_FILEID, WDF_FILEID_NAME);
			   SetValue(FileLocationTypeString[ai->image.location],
						np + "\\" + WDF_INSTALL_SCREEN_IMAGE + "\\" + WDF_FILEID, WDF_FILEID_LOCATION);
		  }
		  if (ai->text.file != "") {
			   assert(ai->page.location >= 0 && ai->page.location < FLT_NUM_VALUES);
			   SetValue(ai->text.file, np + "\\" + WDF_INSTALL_SCREEN_TEXT + "\\" + WDF_FILEID, WDF_FILEID_NAME);
			   SetValue(FileLocationTypeString[ai->text.location], 
						np + "\\" + WDF_INSTALL_SCREEN_TEXT + "\\" + WDF_FILEID, WDF_FILEID_LOCATION);
		  }

		  ++ai;
		  name++;
	 }
}

WDFAction WDF::GetAction(const string& guid) const {
	 WDFAction i;
	 bool found = false;
	 string up = ToUpper(guid);
	 vector<string> keys = m_pRegistry->EnumKeys(WDF_ACTION_LIST);
	 vector<string>::iterator ai = keys.begin(), bi = keys.end();
	 while (ai != bi) {
		  if (InPlaceToUpper(*ai) == up) { found = true; break; }
		  ++ai;
	 }
	 if (!found) return i;
	 string np = WDF_ACTION_LIST + "\\" + guid;
	 i.id = up;
	 i.locationid = GetValue(np, WDF_ACTION_WEBLICATION_ID);
	 i.name = GetValue(np, WDF_ACTION_NAME);
	 i.registration = GetValue(np, WDF_ACTION_REGISTRATION);
	 return i;
}

vector<WDFAction> WDF::GetActions() const {
	 vector<WDFAction> v;
	 vector<string> s = m_pRegistry->EnumKeys(WDF_ACTION_LIST);
	 vector<string>::iterator ai = s.begin(), bi = s.end();
	 while (ai != bi) {
		  v.push_back(GetAction(*ai));
		  ++ai;
	 }
	 return v;
}

void WDF::SetActions(vector<WDFAction>& v) {
	 m_pRegistry->DeleteKey(WDF_ACTION_LIST, true);
	 vector<WDFAction>::iterator ai = v.begin(), bi = v.end();
	 while (ai != bi) {
		  AddAction(*ai);
		  ++ai;
	 }
}

void WDF::AddAction(const WDFAction& w) {
	 string np = WDF_ACTION_LIST + "\\" + ToUpper(w.id);
	 SetValue(ToUpper(w.locationid), np, WDF_ACTION_WEBLICATION_ID);
	 SetValue(w.name, np, WDF_ACTION_NAME);
	 string reg = w.registration;
	 // stupid xml
	 reg.erase(0, reg.find_first_not_of(" "));
	 if (reg.find("<registration>") != 0) {
		  reg = "<registration>" + reg + "</registration>";
	 }
	 SetValue(reg, np, WDF_ACTION_REGISTRATION);
}

bool WDF::RemoveAction(const string& id) {
	 string up = ToUpper(id);
	 vector<string> s = m_pRegistry->EnumKeys(WDF_ACTION_LIST);
	 vector<string>::iterator ai = s.begin(), bi = s.end();
	 while (ai != bi) {
		  if (InPlaceToUpper(*ai) == up) {
			   m_pRegistry->DeleteKey(WDF_ACTION_LIST + "\\" + up, true);
			   return true;
		  }
		  ++ai;
	 }
	 return false;
}

vector<WDFFile> WDF::GetFiles() const {
	 vector<WDFFile> w;
	 vector<string> s = m_pRegistry->EnumKeys(WDF_FILES);
	 vector<string>::iterator ai = s.begin(), bi = s.end();
	 while (ai != bi) {
		  WDFFile f;
		  f.filename = *ai;
		  f.precache = IsTrue(GetValue(WDF_FILES + "\\" + *ai, WDF_FILES_FILE_PRECACHE));
		  f.location = (FileLocationType) MatchString(GetValue(WDF_FILES + "\\" + *ai, WDF_FILEID_LOCATION),
													  FileLocationTypeString, FLT_NUM_VALUES);
		  w.push_back(f);
		  ++ai;
	 }	 
	 return w;
}

void WDF::SetFiles(vector<WDFFile>& v) {
	 m_pRegistry->DeleteKey(WDF_FILES, true);
	 vector<WDFFile>::iterator ai = v.begin(), bi = v.end();
	 while (ai != bi) {
		  AddFile(*ai);
		  ++ai;
	 }
}

// assumes filenames are unique! (may want to enforce this in WDE?)
void WDF::AddFile(const WDFFile& w) {
	 string np = WDF_FILES + "\\" + w.filename;
	 SetValue(BoolToString(w.precache), np, WDF_FILES_FILE_PRECACHE);
	 assert(w.location >= 0 && w.location < FLT_NUM_VALUES);
	 SetValue(FileLocationTypeString[w.location], np, WDF_FILEID_LOCATION);
}

bool WDF::RemoveFile(const string& filename) {
	 vector<string> a = m_pRegistry->EnumKeys(WDF_FILES);
	 vector<string>::iterator ai = a.begin(), bi = a.end();
	 while (ai != bi) {
		  if (*ai == filename) {
			   m_pRegistry->DeleteKey(WDF_FILES + "\\" + filename, true);
			   return true;
		  }
		  ++ai;
	 }
	 return false;
}

void WDF::GetSecurityItems(SecurityHash & sh) {
	 // if this is a location or extension, we want to add our values to the hash
	 if (GetExtensionType() != SET_WEBLICATION) {
		  string id = GetID();
		  vector<string> v = m_pRegistry->EnumKeys(WDF_SECURITY + "\\" + id);
		  vector<string>::iterator ai = v.begin(), bi = v.end();

		  while (ai != bi) {
			   string hashval = id + "\\" + *ai;
			   string path = WDF_SECURITY + "\\" + hashval;

			   // first, see if it's already there
			   SecurityItem val = sh.Lookup(hashval);
			   val.m_Parent = id;
			   val.m_ID = strtol(ai->c_str(), NULL, 10);
			   val.m_DescriptiveName = GetValue(path, WDF_SECURITYINFO_NAME);

			   SecurityType s = (SecurityType) MatchString(GetValue(path, WDF_SECURITYINFO_TYPE),
														   SecurityTypeString, ST_NUM_VALUES);

			   RegistryValue rv;
			   m_pRegistry->QueryValue(path, "", &rv);

			   switch (s) {
			   case ST_BOOL:
					val.m_Bool = IsTrue(rv.m_String);
					break;
			   case ST_NUMBER:
					val.m_Number = strtod(rv.m_String.c_str(), NULL);
					break;
			   case ST_STRING:
					val.m_String = rv.m_String;
					break;
			   case ST_STRING_ENUM:
					val.m_StringVals = rv.m_Array;
					break;
 			   case ST_STRING_ARRAY:
					val.m_StringVals = rv.m_Array;
					break;
			   default: ;
			   }

			   val.m_Type = s;
			   sh.Insert(hashval, val);
			   
			   ++ai;
		  }
		  if (v.size() > 0) {
			   // finally add the descriptive tag
			   string special = id + "\\" + SECURITY_KEY_DESCRIP;
			   // see if it's already there
			   SecurityItem si = sh.Lookup(special);
			   si.m_DescriptiveName = GetTitle();
			   si.m_Type = ST_BOOL; // hack
			   sh.Insert(special, si);
		  }
	 } else {
		  // just lookup and modify values
		  vector<string> v = m_pRegistry->EnumKeys(WDF_SECURITY);
		  vector<string>::iterator ai = v.begin(), bi = v.end();

		  while (ai != bi) {
			   string guid = *ai;
			   vector<string> items = m_pRegistry->EnumKeys(WDF_SECURITY + "\\" + guid);
			   vector<string>::iterator aii = items.begin(), bii = items.end();
			   while (aii != bii) {
					RegistryValue rv;
					m_pRegistry->QueryValue(WDF_SECURITY + "\\" + guid + "\\" + *aii, "", &rv);
					string hash_key = guid + "\\" + *aii;

					SecurityItem i = sh.Lookup(hash_key);
					if (i.m_Type != ST_INVALID) {
					  switch (i.m_Type) {
					  case ST_BOOL:
						i.m_Bool = IsTrue(rv.m_String);
						break;
					  case ST_NUMBER:
						i.m_Number = strtod(rv.m_String.c_str(), NULL);
						break;
					  case ST_STRING:
						i.m_String = rv.m_String;
						break;
					  case ST_STRING_ENUM:
						// match up the string with its enumed value
						for (unsigned int n = 0 ; n < i.m_StringVals.size() ; n++) {
						  if (i.m_StringVals[n] == rv.m_String) {
							i.m_StringIndex = n;
							break;
						  }
						}
						break;
					  case ST_STRING_ARRAY:
						i.m_StringVals = rv.m_Array;
						break;
					  default: ;
					  }
					  sh.Insert(hash_key, i);
					}
					++aii;
			   }
			   ++ai;
		  }
	 }
}

void WDF::SetSecurityItems(SecurityHash & sh) {
	 m_pRegistry->DeleteKey(WDF_SECURITY, true);
	 sec_hash internalsh = sh.GetHash();
	 sec_hash::iterator a = internalsh.begin(), b = internalsh.end();
	 while (a != b) {
		  SetSecurityItem(a->second);
		  ++a;
	 } 
}

void WDF::SetSecurityItem(SecurityItem & s) {
	 char* ch = g_strdup_printf("%d", s.m_ID);
	 string path = WDF_SECURITY + "\\" + s.m_Parent + "\\" + ch;
	 g_free(ch);

	 StaticSetValue(m_pRegistry, s.m_DescriptiveName, path, WDF_SECURITYINFO_NAME);
	 StaticSetValue(m_pRegistry, SecurityTypeString[s.m_Type], path, WDF_SECURITYINFO_TYPE);

	 // If it's an extension custom security setting and the type
	 // is a string enumeration, save the array.
	 if ((GetExtensionType() == SET_EXTENSION)  
		 && ((s.m_Type == ST_STRING_ENUM)
			 || (s.m_Type == ST_STRING_ARRAY))){
 		  StaticSetStrArray(m_pRegistry, s.m_StringVals, path, "");
	 }

	 // If it's a weblication, then save the value.
	 if (GetExtensionType() == SET_WEBLICATION) {
		  if (s.m_Type == ST_STRING_ARRAY) {
			   RegistryValue rv;
			   rv.SetValueType(RVT_ARRAY);
			   rv.m_Array = s.m_StringVals;
			   m_pRegistry->SetValue(path, "", &rv);
		  } else {
			   string v;
			   char* ch;
			   switch (s.m_Type) {
			   case ST_BOOL:
					v = BoolToString(s.m_Bool);
					break;
			   case ST_NUMBER:
					ch = g_strdup_printf("%f", s.m_Number);
					v = ch;
					g_free(ch);
					break;
			   case ST_STRING:
					v = s.m_String;
					break;
			   case ST_STRING_ENUM:
					v = s.m_StringVals[s.m_StringIndex];
					break;
			   case ST_STRING_ARRAY:
					StaticSetStrArray(m_pRegistry, 
									  s.m_StringVals,
									  path);
					break;
			   default: ;
			   }
			   
			   if (s.m_Type != ST_STRING_ARRAY)
					StaticSetValue(m_pRegistry, v, path);

		  }
	 }
}




// grafts the registry portion of the wdf into the specified registry
// at root level
void WDF::GetRegistry(Registry* r, const string wdf_path) {
	 vector<string> vals = m_pRegistry->EnumValues(WDF_REGISTRY + "\\" + wdf_path); 
	 vector<string>::iterator ai = vals.begin(), bi = vals.end();
	 while (ai != bi) {
		  RegistryValue v;
		  m_pRegistry->QueryValue(WDF_REGISTRY + "\\" + wdf_path, *ai, &v);
		  r->SetValue(wdf_path, *ai, &v);
		  ++ai;
	 }
	 
	 vector<string> keys = m_pRegistry->EnumKeys(WDF_REGISTRY + "\\" + wdf_path);
	 vector<string>::iterator air = keys.begin(), bir = keys.end();
	 while (air != bir) {
		  r->CreateKey(wdf_path + "\\" + *air);
		  GetRegistry(r, wdf_path + "\\" + *air);
		  ++air;
	 }
}

void WDF::SetRegistry(Registry* r, const string reg_path) {
	 if (reg_path == "") m_pRegistry->DeleteKey(WDF_REGISTRY, true);

	 vector<string> vals = r->EnumValues(reg_path); 
	 vector<string>::iterator ai = vals.begin(), bi = vals.end();
	 while (ai != bi) {
		  RegistryValue v;
		  r->QueryValue(reg_path, *ai, &v);
		  m_pRegistry->SetValue(WDF_REGISTRY + "\\" + reg_path, *ai, &v);
		  ++ai;
	 }
	 
	 vector<string> keys = r->EnumKeys(reg_path);
	 vector<string>::iterator air = keys.begin(), bir = keys.end();
	 while (air != bir) {
		  m_pRegistry->CreateKey(WDF_REGISTRY + "\\" + reg_path + "\\" + *air);
		  SetRegistry(r, reg_path + "\\" + *air);
		  ++air;
	 }
}

// -----------------------------------------------------

bool WDF::IsTrue(const string& s) {
	 return (ToLower(s) == WDF_TRUE);
}

string WDF::BoolToString(const bool b) {
	 return (b ? WDF_TRUE : WDF_FALSE);
}

int WDF::MatchString(const string& s, const char* ary[], int max) {
	 string a = ToLower(s);
	 for (int i = 0 ; i < max ; i++) {	
		  if (a == string(ary[i])) return i;
	 }
	 return -1;
}

const char* WDF::GetStringFromArray(int index, const char* ary[], int max){
	 if ((index < 0) || (index >= max)){
		  return "Invalid";
	 }
	 
	 return ary[index];
}

void WDF::SetWDFCheck() {
	 SetValue(WDF_CHECK, "", WDF_CHECK_KEY);
}

bool WDF::TestWDFCheck() const {
	 return (GetValue("", WDF_CHECK_KEY) == WDF_CHECK);
}

string WDF::GetValue(const string& key, const string& value_name) const {
	 RegistryValue rv;
	 m_pRegistry->QueryValue(key, value_name, &rv);
	 if (rv.GetValueType() == RVT_INVALID) return "";
	 assert(rv.GetValueType() == RVT_STRING);
	 return rv.m_String;
}

void WDF::SetValue(const string& val, const string& key, const string& value_name) {
	 StaticSetValue(m_pRegistry, val, key, value_name);
}

void WDF::StaticSetValue(Registry* r, const string& val, const string& key, const string& value_name) {
//	 DEBUGMSG(wdf, "Saving to %s:%s --> %s\n", key.c_str(), value_name.c_str(), val.c_str());

	 RegistryValue rv;
	 rv.SetValueType(RVT_STRING);
	 rv.m_String = val;
	 r->SetValue(key, value_name, &rv);
}

void WDF::StaticSetStrArray(Registry* r, const vector<string>& val, const string& key, const string& value_name) {
//	 DEBUGMSG(wdf, "Saving to %s:%s --> %s\n", key.c_str(), value_name.c_str(), val.c_str());

	 RegistryValue rv;
	 rv.SetValueType(RVT_ARRAY);
	 rv.m_Array = val;
	 r->SetValue(key, value_name, &rv);
}


