
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

// AJ Shankar

#ifndef _wdf_h_
#define _wdf_h_

#include <glib.h>
#include <stdio.h>
#include "Registry.h"
#include "XMLNode.h"
#include "SecurityHash.h"
#include "security.h"
#include "sash_version.h"

enum SashExtensionType {
  SET_WEBLICATION = 0,
  SET_EXTENSION,
  SET_LOCATION,
  SET_NUM_VALUES,
  SET_UNDEFINED = -1
};

enum InstallScreenType {
  IST_TEXT = 0,
  IST_LICENSE,
  IST_CUSTOM,
  IST_POSTSECURITY,
  IST_FAILURE,
  IST_SECURITY,
  IST_SUCCESS,
  IST_SCRIPT,
  IST_NUM_VALUES,
  IST_UNDEFINED = -1
};

enum FileLocationType {				
  FLT_WEBLICATION = 0,
  FLT_CACHE,
  FLT_WEBLICATIONPATH,
  FLT_SHAREDLOCATION,
  FLT_SERVER,
  FLT_NUM_VALUES,
  FLT_UNDEFINED = -1
};

struct WDFDependency { 
	 std::string id, filename, title;
	 Version required_version;
};
struct WDFExpiration { std::string date, time, days; };
struct WDFUpdate { bool check, notify; std::string frequency; };
struct WDFAction { std::string id, locationid, name, registration; };
struct WDFVersion {
	 Version weblication;
	 struct { Version minimum, recommended; } sash, mozilla;
};
struct WDFInstallScreen {
	 InstallScreenType type;
	 std::string title;
	 struct { std::string file; FileLocationType location; } page, image, text;
	 bool next, back;
};
struct WDFFile {
	 bool precache; 
	 FileLocationType location;
	 std::string filename;
};

class WDF {
public:
	 WDF();
	 ~WDF();

	 // attempts to parse the given file as a wdf registry,
	 // and failing that, a windows-compliant wdf doc
	 // (basically wrappers calls to LoadFromFile and ConvertFromWDFDoc)
	 // returns true on success
	 bool OpenAmbiguousFile(const std::string& filename);

	 // loads from a WDF registry -- returns true on success
	 bool LoadFromFile(const std::string& filename);

	 // saves to a WDF registry
	 void SaveToFile(const std::string& filename = "");

	 // reads in from a windows-compliant wdf doc, returns true on success
	 bool ConvertFromWDFDoc(const std::string& filename);
	 // writes out to a windows-compliant wdf doc
//	 void ConvertToWDFDoc(const std::string& filename);

	 void Clear();

	 std::string GetID() const;
	 void SetID(const std::string& i);

	 SashExtensionType GetExtensionType() const;
	 void SetExtensionType(SashExtensionType);
	 
	 std::vector<std::string> GetPlatforms() const;
	 void SetPlatforms(const std::vector<std::string>& v);

	 WDFExpiration GetExpiration() const;
	 void SetExpiration(const WDFExpiration& w);

	 WDFUpdate GetUpdate() const;
	 void SetUpdate(const WDFUpdate& u);

	 WDFVersion GetVersion() const;
	 void SetVersion(const WDFVersion& v);
	
	 std::string GetAuthor() const;
	 void SetAuthor(const std::string& s);

	 std::string GetTitle() const;
	 void SetTitle(const std::string& s);

	 std::string GetAbstract() const;
	 void SetAbstract(const std::string& s);

	 std::vector<WDFDependency> GetDependencies() const;
	 void SetDependencies(std::vector<WDFDependency>& v);
	 // will replace an existing dependency with the same id
 	 void AddDependency(const WDFDependency& w);
	 bool RemoveDependency(const std::string& id);

	 /// note that for install screens, order matters
	 std::vector<WDFInstallScreen> GetInstallScreens() const;
	 void SetInstallScreens(std::vector<WDFInstallScreen>& v);

	 std::vector<WDFAction> GetActions() const;
	 WDFAction GetAction(const std::string& guid) const;
	 void SetActions(std::vector<WDFAction>& v);
	 // will replace an existing action with the same id
 	 void AddAction(const WDFAction& w);
	 bool RemoveAction(const std::string& id);

	 // assumes filenames are unique! (may want to enforce this in WDE?)
	 std::vector<WDFFile> GetFiles() const;
	 void SetFiles(std::vector<WDFFile>& v);
	 // will replace an existing file with the same filename
 	 void AddFile(const WDFFile& w);
	 bool RemoveFile(const std::string& filename); // should be unique?

	 // if wdf is for a weblication, modifies existing values in the hash
	 // to reflect settings
	 // if it's a location or extension, adds its new values to the hash
	 void GetSecurityItems(SecurityHash & sh);
	 void SetSecurityItems(SecurityHash & sh);
	 void SetSecurityItem(SecurityItem & si);

	 // grafts the entire registry into the wdf
	 void GetRegistry(Registry* r, const std::string wdf_path = "");

	 // grafts the registry portion of the wdf into the specified registry
	 // at root level
	 // reg_path is the path in the registry to set from (leave as "" to set the
	 // whole thing)
	 void SetRegistry(Registry* r, const std::string reg_path = "");

	 static int MatchString(const std::string& s, const char* ary[], int max);
	 static const char* GetStringFromArray(int index, const char* ary[], int max);

private:
	 static GHashTable * s_pHash;
	 Registry* m_pRegistry;

	 static bool ConvertNode(const XMLNode d, const std::string& path, Registry* const r);
	 static bool ConvertFiles(const XMLNode n, const std::string& path, Registry* const r);
	 static bool ConvertActions(const XMLNode n, const std::string& path, Registry* const r);
	 static bool ConvertRegistry(const XMLNode n, const std::string& path, Registry* const r);
	 static bool ConvertSecurity(const XMLNode n, const std::string& path, Registry* const r);
	 static bool ConvertPlatforms(const XMLNode n, const std::string& path, Registry* const r);
	 static bool ConvertRegistryKey(const XMLNode n, const std::string& path, Registry* const r);
	 static bool ConvertDependencies(const XMLNode n, const std::string& path, Registry* const r);
	 static bool ConvertInstallScreens(const XMLNode n, const std::string& path, Registry* const r);

	 // simple wrapper for registry; assumes data is always a string
	 // if it's not (e.g., number, array), you have to interface with the registry yourself
	 std::string GetValue(const std::string& key, const std::string& value_name = "") const;
	 void SetValue(const std::string& val, const std::string& key, const std::string& value_name = "");
	 static void StaticSetValue(Registry* r, 
								const std::string& val, 
								const std::string& key, 
								const std::string& value_name = "");

	 // used for extension custom str enum settings
	 static void StaticSetStrArray(Registry* r, 
								   const std::vector<std::string>& val, 
								   const std::string& key, 
								   const std::string& value_name = "");

     // is this a real wdf registry?
	 void SetWDFCheck();
	 bool TestWDFCheck() const;

	 // generic method to set weblication version (currently used by Version and Dependency)
	 Version GetOneVersion(const std::string& key) const;
	 void SetOneVersion(const Version& v, const std::string& key);

	 // helper functions for dealing with string conversions
	 static bool IsTrue(const std::string& s);
	 static std::string BoolToString(const bool b);
};

#endif // _wdf_h_
