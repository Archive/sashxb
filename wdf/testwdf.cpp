
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Test program for the WDF

*****************************************************************/

//
// wdf test
//
//

#include <string>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include "sash_constants.h"
#include "sash_error.h"
#include "wdf.h"
#include "FileSystem.h"
#include "SecurityHash.h"

using namespace std;

const string author = "me";
const string abstract = "A test wdf";
const string title = "The title";
const string id = ToUpper("oogakablooga");
int errors = 0;

void print(gpointer key, gpointer val, gpointer me) {
	 char* c = (char*) key;
	 SecurityItem* i = (SecurityItem*) val;
	 cout << c << ": " << i->m_DescriptiveName << " --> ";
	 if (i->m_Type == ST_BOOL) {
		  cout << (i->m_Bool ? "true" : "false") << endl;
	 } else if (i->m_Type == ST_STRING_ENUM) {
		  cout << i->m_StringVals[i->m_StringIndex] << endl;
	 }
}

bool Check(string s, bool arg) {
	 cout << s << "\t";
	 if (arg) 
		  cout << "[SUCCESS]" << endl;
	 else {
		  errors++;
		  cout << "[FAILURE]" << endl;
	 }

}

int main(int argc, char* argv[]) {
	 argc--;
	 argv++;

	 if (argc != 1) {
		  OutputError("Usage: testwdf [WDF file]");
	 }

	 if (! FileSystem::FileExists(string("../security/") + GlobalSecurityFileName)) {
		  OutputError("Cannot find security.dat! Please run create_security_dat first.");
	 }

	 cout << "Creating new wdf... ";
	 WDF w;
	 cout << "[DONE]." << endl << "Adding properties... ";
	 w.SetID(id);
	 w.SetExtensionType(SET_EXTENSION);
	 vector<string> v;
	 v.push_back("Joonix");
	 v.push_back("Windoze");
	 w.SetPlatforms(v);
	 WDFExpiration e = {"December 1", "noon", "69"};
	 w.SetExpiration(e);
	 WDFUpdate u = {false, true, "daily"};
	 w.SetUpdate(u);
	 WDFVersion o = { string("1.2.3.4"), {string("5"), string("6")}, {string("7"), string("8")}};
	 w.SetVersion(o);
	 w.SetAuthor(author);
	 w.SetAbstract(abstract);
	 w.SetTitle(title);
	 vector<WDFDependency> wd;
	 WDFDependency wda = {"HELLO", "hi", "this is a title", string("1.2.3.4")};
	 WDFDependency wdb = {"GOODBYE", "cya", "this is a title 2", string("2.2.3.4")};
	 WDFDependency wdc = {"THIRD", "three", "this is a title 3", string("3.2.3.4")};
	 wd.push_back(wda);
	 wd.push_back(wdb);
	 wd.push_back(wdc);
	 w.SetDependencies(wd);
	 w.RemoveDependency("hello");
	 wd.erase(vector<WDFDependency>::iterator(wd.begin()));

	 vector<WDFFile> wf;
	 WDFFile wfa = {"false", FLT_WEBLICATION, "www.ibm.com"};
	 WDFFile wfb = {"true", FLT_SERVER, "www.oldmanmurray.com"};
	 WDFFile wfc = {"false", FLT_SHAREDLOCATION, "www.splag.net"};
	 WDFFile wfd = {"true", FLT_CACHE, "test.file"};
	 wf.push_back(wfa);
	 wf.push_back(wfc);
	 wf.push_back(wfd);
	 wf.push_back(wfb);
	 w.SetFiles(wf);
	 w.RemoveFile("www.oldmanmurray.com");
	 wf.pop_back();

	 vector<WDFAction> wa;
	 WDFAction waa = {"ID", "LOC1", "weblication", "<registration>www.ibm.com</registration>"};
	 WDFAction wab = {"ID2", "LOC2", "server", "<registration>www.oldmanmurray.com</registration>"};
	 wa.push_back(waa);
	 wa.push_back(wab);
	 w.SetActions(wa);
	 w.RemoveAction("id");
	 wa.erase(wa.begin());

	 WDFInstallScreen wisa = {IST_TEXT, "Title 1", {"page1.html", FLT_CACHE},
							  {"page1.html", FLT_CACHE}, {"page1.html", FLT_CACHE}, true, false};
	 WDFInstallScreen wisb = {IST_SECURITY, "Security", {"security.html", FLT_SHAREDLOCATION},
							  {"page1.html", FLT_CACHE}, {"page1.html", FLT_CACHE}, true, true};
	 WDFInstallScreen wisc = {IST_LICENSE, "License", {"license.html", FLT_WEBLICATIONPATH},
							  {"page1.html", FLT_CACHE}, {"page1.html", FLT_CACHE}, false, false};

	 vector<WDFInstallScreen> wisv; wisv.push_back(wisa); wisv.push_back(wisb); wisv.push_back(wisc);
	 w.SetInstallScreens(wisv);

	 cout << "[DONE]." << endl << "Saving generated wdf to [phil.dat]." << endl;
	 w.SaveToFile("phil.dat");

	 cout << "Loading wdf from [phil.dat]." << endl;
	 if (!w.LoadFromFile("phil.dat"))
		  OutputError("Error loading in WDF registry file!");

	 cout << "Verifying stored values..." << endl;
	 Check("Platforms:", v == w.GetPlatforms());

	 Check("ID:\t", id == w.GetID());

	 WDFExpiration p = w.GetExpiration();
	 Check("Expiration:", e.date == p.date && e.time == p.time && e.days == p.days);

	 WDFUpdate pl = w.GetUpdate();
	 Check("Update:\t", u.check == pl.check && u.notify == pl.notify && u.frequency == pl.frequency);

	 WDFVersion po = w.GetVersion();
	 Check("Version\t", o.weblication.major == po.weblication.major &&
		 o.weblication.major1 == po.weblication.major1 &&
		 o.weblication.minor == po.weblication.minor &&
		 o.weblication.maintenance == po.weblication.maintenance &&
		 o.sash.minimum == po.sash.minimum &&
		 o.sash.recommended == po.sash.recommended &&
		 o.mozilla.minimum == po.mozilla.minimum &&
		 o.mozilla.recommended == po.mozilla.recommended && 
		   o.weblication.ToString() == "1.2.3.4");

	 Check("Author:\t", author == w.GetAuthor());
	 Check("Abstract:", abstract == w.GetAbstract());
	 Check("Title:\t", title == w.GetTitle());

	 unsigned int c;
	 vector<WDFDependency> mmm = w.GetDependencies();
	 if (Check("Dependency num:", mmm.size() == wd.size())) {
		  for (c = 0 ; c < wd.size() ; c++) {
			   Check("   Dependency:", mmm[c].id == wd[c].id &&
					 mmm[c].filename == wd[c].filename &&
					 mmm[c].title == wd[c].title && 
					 mmm[c].required_version.major == wd[c].required_version.major && 
					 mmm[c].required_version.major1 == wd[c].required_version.major1 &&
					 mmm[c].required_version.minor == wd[c].required_version.minor &&
					 mmm[c].required_version.maintenance == wd[c].required_version.maintenance);
		  }
	 }

	 vector<WDFFile> nn = w.GetFiles();
	 if (Check("File check:", nn.size() == wf.size())) {
		  for (c = 0 ; c < wf.size() ; c++) {
			   Check("   File:", nn[c].precache == wf[c].precache &&
					 nn[c].filename == wf[c].filename &&
					 nn[c].location == wf[c].location);
		  }
	 }

	 vector<WDFAction> oo = w.GetActions();
	 if (Check("Action check:", oo.size() == wa.size())) {
		  for (c = 0 ; c < wa.size() ; c++) {
			   Check("   Action:", oo[c].id == wa[c].id &&
					 oo[c].locationid == wa[c].locationid &&
					 oo[c].name == wa[c].name &&
					 oo[c].registration == wa[c].registration);
		  }
	 }

	 cout << "[DONE]." << endl;
	 
	 cout << endl << "Reading in global security hash... ";
	 SecurityHash sh(string("../security/") + GlobalSecurityFileName);
	 cout << "[DONE]." << endl;

	 string name = argv[0];
	 cout << endl << "Parsing Windows wdf file: [" << name << "]" << endl;
	 w.ConvertFromWDFDoc(name);

	 Registry* r = new Registry();
	 w.GetRegistry(r);
	 Registry* r2 = new Registry();
	 RegistryValue rv, rv2;
	 rv.SetValueType(RVT_STRING);
	 rv.m_String = "test";
	 r2->SetValue("bob\\\\phil\\", "ooga", &rv);
	 w.SetRegistry(r2);
	 Registry* r3 = new Registry();
	 w.GetRegistry(r3);
	 r3->QueryValue("bob\\phil", "ooga", &rv2);
	 Check("\tChecking registry grafting...", rv.GetValueType() == rv2.GetValueType() && 
		   rv.m_String == rv2.m_String && 
		   rv.m_String == "test");
	 w.SetRegistry(r);

	 delete r; delete r2; delete r3;

	 w.GetSecurityItems(sh);
	 cout << "\tLooking up security value... ";
	 SecurityItem i = sh.Lookup(SECURITY_GLOBAL + "\\" + "1");
	 Check("", i.m_Type != ST_INVALID);

	 bool newb;
	 cout << "\tModifying security value and re-checking... ";
	 if (i.m_Type == ST_BOOL) {
		  newb = i.m_Bool = !i.m_Bool;
		  w.SetSecurityItem(i);
		  w.GetSecurityItems(sh);
		  SecurityItem l = sh.Lookup(SECURITY_GLOBAL + "\\" + "1");
		  if (l.m_Type != ST_INVALID || l.m_Bool != newb)
			   OutputError("[FAILED!]");
	 }
	 cout << "[SUCCESS]" << endl;
	 
	 cout << "Saving to WDF registry [bob.dat]... ";
	 w.SaveToFile("bob.dat");
	 cout << "[DONE]." << endl;
	 cout << endl << "Have NOT checked install screens. " << endl;

	 if (errors) {
		  cout << "FAILURE: "<< errors << " errors!" << endl << endl;
		  exit(1);
	 } 
	 cout << "Success!" << endl << endl;

	 exit(0);
}

