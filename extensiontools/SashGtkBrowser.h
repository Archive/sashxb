
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHGKTBROWSER_H
#define SASHGTKBROWSER_H

#include "sashIGtkBrowser.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <nsIComponentManager.h>
#include <prenv.h>
#include "gtkmozembed_internal.h"
#include "sashICacheManagerNotify.h"

struct _GtkWidget;
typedef struct _GtkWidget GtkWidget;

class nsIScriptContext;
class sashIActionRuntime;
class nsIDocShell;
class CacheManager;

#define SASHGTKBROWSER_CONTRACT_ID "@gnome.org/SashXB/extensiontools/SashGtkBrowser;1"
NS_DEFINE_CID(kSashGtkBrowserCID, SASHGTKBROWSER_CID);

class SashGtkBrowser : public sashIGtkBrowser
{
public:
	 
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHIGTKBROWSER

	 SashGtkBrowser();
	 virtual ~SashGtkBrowser();

 private:
  GtkWidget * m_embed;
  sashIActionRuntime * m_runtime;
  CacheManager *m_cacher;
  PRBool m_enableSashContext;

  void ConnectSashProtocolHandler(sashIActionRuntime *runtime);
  void SetProfilePath();
  NS_IMETHOD SetErrorHandler();
  NS_IMETHOD GetDocument(nsIDocument **doc);
  
};
 
#endif // SASHGTKBROWSER_H
