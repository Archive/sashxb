
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASHDOCUMENTOBSERVER_H
#define SASHDOCUMENTOBSERVER_H

#include "nsIDocumentObserver.h"

//CD09380E-A924-4CFF-82DD-9612D9AB5CF3
#define SASHDOCUMENTOBSERVER_CID {0xCD09380E, 0xA924, 0x4CFF, {0x82, 0xDD, 0x96, 0x12, 0xD9, 0xAB, 0x5C, 0xF3}}

#define SASHDOCUMENTOBSERVER_CONTRACT_ID "@gnome.org/SashXB/extensiontools/docobserver;1"

#define NOOP { return NS_OK; }

class sashDocumentObserver : public nsIDocumentObserver
{
 public:
  NS_DECL_ISUPPORTS;

  sashDocumentObserver();
  virtual ~sashDocumentObserver();

  NS_IMETHOD BeginUpdate(nsIDocument *aDocument);
  NS_IMETHOD EndUpdate(nsIDocument *aDocument);
  NS_IMETHOD BeginLoad(nsIDocument *aDocument);
  NS_IMETHOD EndLoad(nsIDocument *aDocument);
  NS_IMETHOD BeginReflow(nsIDocument *aDocument, nsIPresShell *aShell) NOOP
  NS_IMETHOD EndReflow(nsIDocument *aDocument, nsIPresShell *aShell) NOOP
  NS_IMETHOD ContentChanged(nsIDocument *aDocument, nsIContent *aContent,
			    nsISupports *aSubContent) NOOP
  NS_IMETHOD ContentStatesChanged(nsIDocument* aDocument,
                                  nsIContent* aContent1,
                                  nsIContent* aContent2) NOOP
  NS_IMETHOD AttributeChanged(nsIDocument *aDocument,
                              nsIContent*  aContent,
                              PRInt32      aNameSpaceID,
                              nsIAtom*     aAttribute,
                              PRInt32      aHint) NOOP
  NS_IMETHOD ContentAppended(nsIDocument *aDocument,
                             nsIContent* aContainer,
			     PRInt32     aNewIndexInContainer) NOOP
  
  NS_IMETHOD ContentInserted(nsIDocument *aDocument,
                             nsIContent* aContainer,
                             nsIContent* aChild,
                             PRInt32 aIndexInContainer) NOOP
  NS_IMETHOD ContentReplaced(nsIDocument *aDocument,
                             nsIContent* aContainer,
                             nsIContent* aOldChild,
                             nsIContent* aNewChild,
			     PRInt32 aIndexInContainer) NOOP
  NS_IMETHOD ContentRemoved(nsIDocument *aDocument,
			    nsIContent* aContainer,
                            nsIContent* aChild,
                            PRInt32 aIndexInContainer) NOOP
  NS_IMETHOD StyleSheetAdded(nsIDocument *aDocument,
                             nsIStyleSheet* aStyleSheet) NOOP
  NS_IMETHOD StyleSheetRemoved(nsIDocument *aDocument,
                               nsIStyleSheet* aStyleSheet) NOOP
  NS_IMETHOD StyleSheetDisabledStateChanged(nsIDocument *aDocument,
                                            nsIStyleSheet* aStyleSheet,
                                            PRBool aDisabled) NOOP
  NS_IMETHOD StyleRuleChanged(nsIDocument *aDocument,
                              nsIStyleSheet* aStyleSheet,
                              nsIStyleRule* aStyleRule,
                              PRInt32 aHint) NOOP 
  NS_IMETHOD StyleRuleAdded(nsIDocument *aDocument,
                            nsIStyleSheet* aStyleSheet,
                            nsIStyleRule* aStyleRule) NOOP
  NS_IMETHOD StyleRuleRemoved(nsIDocument *aDocument,
                              nsIStyleSheet* aStyleSheet,
                              nsIStyleRule* aStyleRule) NOOP
  NS_IMETHOD DocumentWillBeDestroyed(nsIDocument *aDocument) NOOP
};

#endif // SASHDOCUMENTOBSERVER_H
