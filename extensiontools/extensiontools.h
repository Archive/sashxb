
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef EXTENSIONTOOLS_H
#define EXTENSIONTOOLS_H

#include <string>
#include <vector>
#include <glib.h>
#include "debugmsg.h"
#include <prenv.h>
#include <nsIFactory.h>
#include <nsIGenericFactory.h>
#include <nsIServiceManager.h>
#include <nsComponentManagerUtils.h>
#include <nsIScriptContext.h>
#include <plstr.h>
#include <math.h>
#include "nsCRT.h"
#include "sash_constants.h"
#include "xpcomtools.h"
#include "jsapi.h"
#include "nsIVariant.h"
#include <uuid/uuid.h>
#include "sashIActionRuntime.h"

extern "C" {
	 int SashRegisterSashRuntime(vector<string> componentDirs);
	 void GetVariantArray(nsIVariant * v, nsIVariant *** items, PRUint32 * count);
}

#define SASH_IMPL_ISUPPORTS_INHERITED_CI1(Class, Super, i1)               \
    SASH_IMPL_QUERY_INTERFACE_INHERITED1(Class, Super, i1)                \
    NS_IMPL_ADDREF_INHERITED(Class, Super)                                \
    NS_IMPL_RELEASE_INHERITED(Class, Super)                               \
    NS_IMPL_CI_INTERFACE_GETTER1(Class, i1)

#define SASH_IMPL_QUERY_INTERFACE_INHERITED1(Class, Super, i1)                \
  NS_IMPL_QUERY_HEAD(Class)                                                   \
  NS_IMPL_QUERY_BODY(i1)                                                      \
  NS_IMPL_QUERY_CLASSINFO(Class)                                              \
  NS_IMPL_QUERY_TAIL_INHERITING(Super)                                        \

#define SASH_IMPL_ISUPPORTS_INHERITED_CI2(Class, Super, i1, i2)               \
    SASH_IMPL_QUERY_INTERFACE_INHERITED2(Class, Super, i1, i2)                \
    NS_IMPL_ADDREF_INHERITED(Class, Super)                                    \
    NS_IMPL_RELEASE_INHERITED(Class, Super)                                   \
    NS_IMPL_CI_INTERFACE_GETTER2(Class, i1, i2)

#define SASH_IMPL_QUERY_INTERFACE_INHERITED2(Class, Super, i1, i2)            \
  NS_IMPL_QUERY_HEAD(Class)                                                   \
  NS_IMPL_QUERY_BODY(i1)                                                      \
  NS_IMPL_QUERY_BODY(i2)                                                      \
  NS_IMPL_QUERY_CLASSINFO(Class)                                              \
  NS_IMPL_QUERY_TAIL_INHERITING(Super)                                        \

#define GET_SASH_NAME_IMPL(classname, sashname) \
NS_IMETHODIMP classname::GetSashName(char **ret ) { XP_COPY_STRING(sashname, ret); return NS_OK; }

#define SASH_LOCATION_IMPL(classname, interfacename, capsname, sashname, description) \
NS_IMPL_ISUPPORTS3_CI(classname, interfacename, sashIExtension, sashILocation); \
GET_SASH_NAME_IMPL(classname, sashname) \
MODULE_COMPONENT_INFO_STANDALONE(classname, capsname, description)

#define SASH_EXTENSION_IMPL(classname, interfacename, capsname, sashname, description) \
NS_IMPL_ISUPPORTS2_CI(classname, interfacename, sashIExtension); \
GET_SASH_NAME_IMPL(classname, sashname) \
MODULE_COMPONENT_INFO_STANDALONE(classname, capsname, description)

#define SASH_LOCATION_IMPL_NO_NSGET(classname, interfacename, sashname) \
NS_IMPL_ISUPPORTS3_CI(classname, interfacename, sashIExtension, sashILocation); \
GET_SASH_NAME_IMPL(classname, sashname)

#define SASH_EXTENSION_IMPL_NO_NSGET(classname, interfacename, sashname) \
NS_IMPL_ISUPPORTS2_CI(classname, interfacename, sashIExtension); \
GET_SASH_NAME_IMPL(classname, sashname)


#define MODULE_COMPONENT_INFO(classname) \
NS_GENERIC_FACTORY_CONSTRUCTOR(classname); \
static nsModuleComponentInfo classname ## _components[] = \
{ \
{ \
description, \
capsname ## _CID,\
capsname ## _CONTRACT_ID, \
classname ## Constructor, \
   NULL, \
 NULL,\
   NULL,\
   NS_CI_INTERFACE_GETTER_NAME(classname),\
   NULL,\
   &NS_CLASSINFO_NAME(classname),\
   0\
   } \
}; \
NS_IMPL_NSGETMODULE(classname, classname ## _components)

#define MODULE_COMPONENT_INFO_STANDALONE(classname, capsname, description) \
NS_DECL_CLASSINFO(classname); \
NS_GENERIC_FACTORY_CONSTRUCTOR(classname); \
static nsModuleComponentInfo classname ## _components[] = \
{ \
{ \
description, \
capsname ## _CID,\
capsname ## _CONTRACT_ID, \
classname ## Constructor, \
   NULL, \
 NULL,\
   NULL,\
   NS_CI_INTERFACE_GETTER_NAME(classname),\
   NULL,\
   &NS_CLASSINFO_NAME(classname),\
   0\
   } \
}; \
NS_IMPL_NSGETMODULE(classname, classname ## _components)

#define MODULE_COMPONENT_ENTRY(classname, capsname, description) \
{ \
description, \
capsname ## _CID,\
capsname ## _CONTRACT_ID, \
classname ## Constructor, \
   NULL, \
 NULL,\
   NULL,\
   NS_CI_INTERFACE_GETTER_NAME(classname),\
   NULL,\
   &NS_CLASSINFO_NAME(classname),\
   0\
   } 

inline void CallEvent(sashIActionRuntime * act, JSContext * cx, const string & event, 
					  std::vector<nsIVariant *> & args, nsIVariant ** result = NULL)
{
	 nsIVariant ** argv = NULL;
	 if (args.size() > 0) {
		  argv = new nsIVariant * [args.size()];
		  for (unsigned int i = 0; i < args.size(); i++)
			   argv[i] = args[i];
	 }
	 act->CallEvent(event.c_str(), cx, args.size(), argv, result);
	 if (argv)
		  delete [] argv;
}

inline void CallEvent(sashIActionRuntime * act, nsIScriptContext * nscx,
					  const string & event, std::vector<nsIVariant *> & args,
					  nsIVariant ** result = NULL) 
{
	 JSContext * jcx = (JSContext *) nscx->GetNativeContext();
	 CallEvent(act, jcx, event, args, result);
}

inline nsIVariant * CallEventWithResult(sashIActionRuntime * act, JSContext * cx, 
										const string & event, 
										std::vector<nsIVariant *> & args)
{
	 nsIVariant * result;
	 CallEvent(act, cx, event, args, &result);
	 return result;
}

inline nsIVariant * CallEventWithResult(sashIActionRuntime * act, nsIScriptContext * nscx, 
										const string event, std::vector<nsIVariant *> & args) 
{
	 nsIVariant * result;
	 CallEvent(act, nscx, event, args, &result);
	 return result;
}

#endif // EXTENSIONTOOLS_H
