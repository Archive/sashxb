
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "nsIXPConnect.h"
#include "nsIScriptError.h"
#include "nsIServiceManager.h"
#include "nsIComponentManager.h"
#include "nsIJSContextStack.h"
#include "nsIJSRuntimeService.h"
#include "nsMemory.h"
#include "nsIXPCSecurityManager.h"
#include "nsICategoryManager.h"
#include "nsIGenericFactory.h"
#include "jsapi.h"

#include "sashJSEmbed.h"
#include "debugmsg.h"

NS_IMPL_ISUPPORTS1(sashJSEmbed, sashIJSEmbed);

static FILE *gOutFile = stdout;
static FILE *gErrFile = stderr;
JS_STATIC_DLL_CALLBACK(JSBool) Print(JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval);
JS_STATIC_DLL_CALLBACK(void) defaultErrorReporter(JSContext *cx, const char *message, JSErrorReport *report);

static JSFunctionSpec glob_functions[] = {
    {"print",           Print,          0},
    {"printsys",        Print,          0},
    {0}
};

static JSClass global_class = {
    "global", 0,
    JS_PropertyStub,  JS_PropertyStub,  JS_PropertyStub,  JS_PropertyStub,
    JS_EnumerateStub, JS_ResolveStub,   JS_ConvertStub,   JS_FinalizeStub
};


sashJSEmbed::sashJSEmbed() : m_jsRT(NULL), m_jsContext(NULL), m_jsGlob(NULL),
                             m_jsErrCallback(defaultErrorReporter), m_secMan(NULL)
{
	 NS_INIT_ISUPPORTS();
}

sashJSEmbed::~sashJSEmbed()
{
}

NS_IMETHODIMP sashJSEmbed::SetOutput(FILE *out)
{
	gOutFile = out;
	return NS_OK;
}

void sashJSEmbed::SetErrorHandler(JSErrorReporter errHandler)
{
	m_jsErrCallback = errHandler;
}

NS_IMETHODIMP sashJSEmbed::SetSecurityMode(PRInt32 mode)
{
	if(!m_secMan) 
		 return NS_ERROR_FAILURE;
	
	m_secMan->SetMode((sashJSEmbedSecurityManager::Mode) mode);
	return NS_OK;
}

NS_IMETHODIMP sashJSEmbed::StartJSInterpreter()
{
	nsresult rv;

	// get the JSRuntime from the runtime svc
	nsCOMPtr<nsIJSRuntimeService> rtsvc(do_GetService("@mozilla.org/js/xpc/RuntimeService;1", &rv));
	if(NS_FAILED(rv) || NS_FAILED(rtsvc->GetRuntime(&m_jsRT)) || !m_jsRT)
		return NS_ERROR_FAILURE; //DIE("FAILED to get a JSRuntime");


	//setup js context
	m_jsContext = JS_NewContext(m_jsRT, 8192);
	if(!m_jsContext)
		return NS_ERROR_FAILURE; //DIE("FAILED to create a JSContext");


	//setup error reporting
	JS_SetErrorReporter(m_jsContext, m_jsErrCallback);


	//get xpconnect
	nsCOMPtr<nsIXPConnect> xpc(do_GetService(nsIXPConnect::GetCID(), &rv));
	if(!xpc)
		return NS_ERROR_FAILURE; //DIE("FAILED to get xpconnect service\n");


	nsCOMPtr<nsIJSContextStack> cxstack(do_GetService("@mozilla.org/js/xpc/ContextStack;1", &rv));
	if(NS_FAILED(rv))
		return NS_ERROR_FAILURE; //DIE("FAILED to get the nsThreadJSContextStack service!\n");
	if(NS_FAILED(cxstack->Push(m_jsContext)))
		return NS_ERROR_FAILURE; //DIE("FAILED to get push the current jscontext on the nsThreadJSContextStack service!\n");


	//setup the global object
	m_jsGlob = JS_NewObject(m_jsContext, &global_class, NULL, NULL);
	if (!m_jsGlob)
		return NS_ERROR_FAILURE; //DIE("FAILED to create global object");
	if (!JS_InitStandardClasses(m_jsContext, m_jsGlob))
		return NS_ERROR_FAILURE; //DIE("FAILED to init standard classes");
	if (!JS_DefineFunctions(m_jsContext, m_jsGlob, glob_functions))
		return NS_ERROR_FAILURE; //DIE("FAILED to define global functions");
	if (NS_FAILED(xpc->InitClasses(m_jsContext, m_jsGlob)))
		return NS_ERROR_FAILURE; //DIE("FAILED to init xpconnect classes");


	//setup the security manager
	m_secMan = new sashJSEmbedSecurityManager();
	if(!m_secMan) return NS_ERROR_FAILURE;
	if(!NS_SUCCEEDED(xpc->SetSecurityManagerForJSContext(m_jsContext, m_secMan, nsIXPCSecurityManager::HOOK_ALL)))
		return NS_ERROR_FAILURE;

	// set the branch callback for GC
	JS_SetBranchCallback(m_jsContext, sashJSEmbed::BranchCB);

	return NS_OK;
}

JSBool sashJSEmbed::BranchCB(JSContext * cx, JSScript * script) {
	 static int count = 0;
	 if ((count & 0xFFF) == 0) {
		  DEBUGMSG(js, "GC\n");
		  JS_GC(cx);
	 }
	 else if ((count & 0xF) == 0) {
		  DEBUGMSG(js, "Maybe GC\n");
		  JS_MaybeGC(cx);
	 }
	 count++;
	 return true;
}

NS_IMETHODIMP sashJSEmbed::ShutdownJSInterpreter()
{
	JS_ClearScope(m_jsContext, m_jsGlob);
	JS_GC(m_jsContext);
	JS_DestroyContext(m_jsContext);
	return NS_OK;
}

NS_IMETHODIMP sashJSEmbed::EvalScript(const char * script)
{
  jsval rval;

  int status = JS_EvaluateScript(m_jsContext, m_jsGlob, script, 
			    strlen(script), "SashScript", 0, &rval);

  if (status == JS_TRUE)
	   return NS_OK;
  else
	   return NS_ERROR_FAILURE;
}

NS_IMETHODIMP sashJSEmbed::EvalScriptFile(const char *fName)
{
	JSScript *script;
	jsval result;
	JSBool ok;
	int status = NS_ERROR_FAILURE;

	script = JS_CompileFile(m_jsContext, m_jsGlob, fName);
	if(script) {
	  ok = JS_ExecuteScript(m_jsContext, m_jsGlob, script, &result);
	  if(ok) {
		JS_DestroyScript(m_jsContext, script);
		status = NS_OK;
	  }
	}
	return status;
}

NS_IMETHODIMP sashJSEmbed::GetScriptContext(JSContext ** cx)
{
	 *cx = m_jsContext;
	 return NS_OK;
}

JS_STATIC_DLL_CALLBACK(JSBool) Print(JSContext *cx, JSObject *obj, uintN argc, jsval *argv, jsval *rval)
{
    uintN i, n;
    JSString *str;

    for (i = n = 0; i < argc; i++) {
        str = JS_ValueToString(cx, argv[i]);
        if (!str)
            return JS_FALSE;
        fprintf(gOutFile, "%s%s", i ? " " : "", JS_GetStringBytes(str));
    }
    n++;
    if (n)
        fputc('\n', gOutFile);
    return JS_TRUE;
}

JS_STATIC_DLL_CALLBACK(void) defaultErrorReporter(JSContext *cx, const char *message, JSErrorReport *report)
{
	fprintf(gErrFile, "Javascript error on line %d: %s\n", report->lineno, message);
}

