
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "sashDocumentObserver.h"
#include "debugmsg.h"

NS_IMPL_ISUPPORTS1(sashDocumentObserver, nsIDocumentObserver);

sashDocumentObserver::sashDocumentObserver()
{
  DEBUGMSG(browser, "Creating document observer\n");
}

sashDocumentObserver::~sashDocumentObserver()
{
  DEBUGMSG(browser, "Destroying document observer\n");
}

NS_IMETHODIMP
sashDocumentObserver::BeginUpdate(nsIDocument *doc)
{
  DEBUGMSG(browser, "BeginUpdate\n");
  return NS_OK;
}

NS_IMETHODIMP
sashDocumentObserver::EndUpdate(nsIDocument *doc)
{
  DEBUGMSG(browser, "EndUpdate\n");
  return NS_OK;
}

NS_IMETHODIMP
sashDocumentObserver::BeginLoad(nsIDocument *doc)
{
  DEBUGMSG(browser, "BeginLoad\n");
  return NS_OK;
}

NS_IMETHODIMP
sashDocumentObserver::EndLoad(nsIDocument *doc)
{
  DEBUGMSG(browser, "EndLoad\n");
  return NS_OK;
}
