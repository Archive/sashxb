
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef XPCOM_TOOLS_H
#define XPCOM_TOOLS_H

#include <nsMemory.h>
#include "plstr.h"
#include <string>

/**
   Return a string in an XPCOM-safe way. 
*/
inline char* XP_STRDUP(const char* s) {
	 return (char *) nsMemory::Clone(s, sizeof(char) * (PL_strlen(s) + 1));
}

inline char* XP_STRDUP(const string& s) {
	 return (char *) nsMemory::Clone(s.c_str(), sizeof(char) * (s.length() + 1));
}

inline void XP_COPY_STRING(const char* s, char** val) {
	 *val = XP_STRDUP(s);
}

inline void  XP_COPY_STRING(const string& s, char** val) {
     *val = XP_STRDUP(s);
}

/*
 For setting myString from a char* returned by a method
 eg) XP_GET_STRING(myChild->GetName, myName)
*/
#define XP_GET_STRING(dummy, myString) \
{\
  char *temp = NULL;    \
  dummy(&temp);         \
  if (temp)             \
    myString = temp;    \
  else                  \
    myString = "";      \
  nsMemory::Free(temp); \
}


#endif
