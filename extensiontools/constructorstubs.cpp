
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

string ext_prefix;

string string_upper(string s) {
  transform (s.begin(), s.end(), s.begin(), toupper);
  return s;
}

string cut_here(string s) {
  return "--8<--------[ "+ s+ " ]---8<-------------------------";
}

void generateDotHText(ostream &ostr, vector<string> &props) {
  string prop_construct;
  ostr << endl << cut_here("for sash"+ ext_prefix + ".h") << endl;
  ostr << "\tprivate:" << endl;
  for (unsigned int i= 0; i< props.size(); ++i)
    ostr << "\t\tsashIGenericConstructor *m_p" << props[i] << "Constructor;" << endl;
}

void generateDotCPPText(ostream &ostr, vector<string> &props) {
  string prop_construct;
  ostr << endl << cut_here("for sash"+ ext_prefix + ".cpp") << endl;
  ostr << "NS_IMPL_ISUPPORTS2_CI(sash" << ext_prefix << ", sashI" << ext_prefix << ", sashIExtension);" << endl;
  ostr << "SASH_COMMON_IMPL(sash" << ext_prefix << ", sashI" << ext_prefix;
  ostr << ", ksash" << ext_prefix << "CID, SASH" << string_upper(ext_prefix) << ", SASHI";
  ostr << string_upper(ext_prefix) << ", \"" << ext_prefix << "\", \"description here\");" << endl;
  ostr << "sash" << ext_prefix << "::sash" << ext_prefix << "() {" << endl;
  ostr << "\tNS_INIT_ISUPPORTS();" << endl;
  for (unsigned int i= 0; i< props.size(); ++i) {
    ostr << "/* Constructor initialization for property " << props[i] << " */" << endl;
    ostr << "\tnsComponentManager::CreateInstance(ksashGenericConstructorCID, NULL, ";
    ostr << "NS_GET_IID(sashIGenericConstructor), (void **) &m_p" << props[i] << "Constructor);" << endl;
    ostr << "\tm_p" << props[i] << "Constructor->AddRef();" << endl;
    ostr << "\tm_p" << props[i] << "Constructor->SetConstructorProperties(SASH";
    prop_construct= string_upper(ext_prefix + props[i]);
    ostr << prop_construct << "_CONTRACT_ID, SASHI" << prop_construct << "_IID_STR);" << endl;
  }
  ostr << "}" << endl << endl;
  
  ostr << "sash" << ext_prefix << "::~sash" << ext_prefix << "() {" << endl;
  for (unsigned int i= 0; i< props.size(); ++i) {
    ostr << "\tm_p" << props[i] << "Constructor->Release();" << endl;
  }
  ostr << "}" << endl << endl;
  
  for (unsigned int i= 0; i< props.size(); ++i) {
    ostr << "/* readonly atribute sashIGenericConstructor " << props[i] << "; */" << endl;
    ostr << "sash" << ext_prefix << "::Get" << props[i] << "(sashIGenericConstructor **a" << props[i] << ") {" << endl;
    ostr << "\t*a" << props[i] << "= m_p" << props[i] << "Constructor;" << endl;
    ostr << "\treturn NS_OK;" << endl;
    ostr << "}" << endl << endl;
  }
  
  props.push_back("");
  for (unsigned int i= 0; i< props.size(); ++i) {
    ostr << "NS_DECL_CLASSINFO(sash" << ext_prefix << props[i] << ");" << endl;
    ostr << "NS_GENERIC_FACTORY_CONSTRUCTOR(sash" << ext_prefix << props[i] << ");" << endl;
    ostr << "#define _" << string_upper(props[i]) << "_ENTRY MODULE_COMPONENT_ENTRY(sash";
    ostr << ext_prefix << props[i] << ", SASH" << string_upper(ext_prefix + props[i]) << ", \"\"" << endl;
  }
  
  ostr << "static nsModuleComponentInfo sash" << ext_prefix << "_components[]= {" << endl;
  for (unsigned int i= 0; i< props.size(); ++i) {
    ostr << "\t_" << string_upper(props[i]) << "_ENTRY";
    if (i< props.size()- 1) ostr << ", " << endl;
  }
  ostr << "};" << endl << "NS_IMPL_NSGETMODULE(sash" << ext_prefix << ", sash" << ext_prefix << "_components);" << endl;
}

int main(void) {
  vector<string> props;
  string tmp;
  cin >> ext_prefix;
  while (!cin.fail()) {
    cin >> tmp;
    cin.ipfx(0);
    props.push_back(tmp);
  }
  generateDotHText(cout, props);
  generateDotCPPText(cout, props);
  return 0;
}

