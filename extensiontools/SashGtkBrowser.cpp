
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include <nsNetUtil.h>
#include <nsIIOService.h>

#include "CacheManager.h"
#include "SashGtkBrowser.h"
#include "sashIActionRuntime.h"
#include "debugmsg.h"
#include "InstallationManager.h"
#include "RuntimeTools.h"
#include <nsIWebBrowser.h>
#include <nsIPresShell.h>
#include <nsIInterfaceRequestor.h>
#include <nsIScriptGlobalObject.h>
#include <nsIComponentManager.h>
#include "gtkmozembed_internal.h"

NS_IMPL_ISUPPORTS1(SashGtkBrowser, sashIGtkBrowser);

static void
new_window_cb(GtkMozEmbed *embed, GtkMozEmbed **newEmbed, 
	      guint chromemask, SashGtkBrowser *browser)
{
  browser->CreateNewWindow(chromemask, newEmbed);
}

static gboolean
delete_window_cb(GtkWidget *window)
{
  gtk_widget_destroy(window);
  return true;
}

static void
destroy_brows_cb(GtkMozEmbed *embed, GtkWidget *parent)
{
  printf("Brows destroy\n");
  gtk_widget_destroy(parent);
}

SashGtkBrowser::SashGtkBrowser()
{
	 NS_INIT_ISUPPORTS();
	 m_runtime = NULL;
	 RuntimeTools::RegisterSashGtkBrowser(this);
}

SashGtkBrowser::~SashGtkBrowser()
{
  gtk_object_unref(GTK_OBJECT(m_embed));
  RuntimeTools::UnregisterSashGtkBrowser(this);
}

NS_IMETHODIMP SashGtkBrowser::Initialize(sashIActionRuntime * runtime)
{
	 if (m_runtime != NULL)
		  return NS_ERROR_UNEXPECTED;
	 m_runtime = runtime;
	 RuntimeTools::SetSashGlobalNameSpaceActionRuntime(runtime);
  
	 {
		  // I hope this should be here. It notifies the sash protocol handler
		  // of what our current cache manager is
		  //ConnectSashProtocolHandler(m_runtime);
	 }
	 m_embed = gtk_moz_embed_new();
	 gtk_object_ref(GTK_OBJECT(m_embed));
	 
	 gtk_signal_connect(GTK_OBJECT(m_embed), "new_window",
						GTK_SIGNAL_FUNC(new_window_cb), this);
	 SetProfilePath();
	 return NS_OK;
}

NS_IMETHODIMP SashGtkBrowser::GetWidget(GtkWidget ** widget)
{
	 *widget = m_embed;
	 return NS_OK;
}

void
SashGtkBrowser::SetProfilePath()
{
  DEBUGMSG(browser, "Setting prof path\n");
  string path = InstallationManager::StaticGetSashInstallDirectory();
  char *full = g_strdup(path.c_str());
  gtk_moz_embed_set_profile_path(full,"runtime_profile");
  g_free(full);
}

NS_IMETHODIMP
SashGtkBrowser::GetDocShell(nsIDocShell **docShell)
{
  assert(docShell != NULL);

  nsCOMPtr<nsIWebBrowser> web;
  gtk_moz_embed_get_nsIWebBrowser(GTK_MOZ_EMBED(m_embed), 
				  getter_AddRefs(web));
  assert(web != NULL);
  nsCOMPtr<nsIDocShell> tempShell(do_GetInterface(web));
  *docShell = tempShell;
  assert(*docShell != NULL);
  return NS_OK;
}

NS_IMETHODIMP
SashGtkBrowser::GetDocument(nsIDocument **doc)
{
  nsresult rv;
  nsIDocShell* docShell;
  GetDocShell(&docShell);

  nsCOMPtr<nsIPresShell> pres;
  rv = docShell->GetPresShell(getter_AddRefs(pres));
  if (NS_FAILED(rv))
    return rv;
  if (pres == NULL) {
//     too risky
//     nsCOMPtr<nsIPresShell> oldpres;
//	   rv = docShell->GetEldestPresShell(getter_AddRefs(oldpres));
	   *doc = NULL;
  } else {
	   rv = pres->GetDocument(doc);
	   if (NS_FAILED(rv))
			return rv;
	   assert(*doc != NULL);
  }
  return NS_OK;
}

JS_STATIC_DLL_CALLBACK(void)
ErrorReporter(JSContext *cx, const char *message, JSErrorReport *report)
{
  fprintf(stderr, "JS Error: %s\n", message);
}

NS_IMETHODIMP
SashGtkBrowser::SetErrorHandler()
{
	 nsIScriptContext* scx;
  GetScriptContext(&scx);
  JSContext *jcx = (JSContext *) scx->GetNativeContext();
  JS_SetErrorReporter(jcx, ErrorReporter); 
  return NS_OK;
}

NS_IMETHODIMP
SashGtkBrowser::GetScriptContext(nsIScriptContext **cx)
{ 
  nsresult rv;

  nsIDocument* doc;
  rv = GetDocument(&doc);

  DEBUGMSG(browser, "document is %p\n", doc);
  if (rv != NS_OK)
    return rv;

  if (doc != NULL) {
	   nsCOMPtr<nsIScriptGlobalObject> script;
	   rv = doc->GetScriptGlobalObject(getter_AddRefs(script));
	   if (NS_FAILED(rv))
			return rv;
	   assert(script != NULL);
	   
	   rv = script->GetContext(cx);
	   DEBUGMSG(browser, "script is %p\n", doc);
  } else {
	   // context might be null during page transitions; can't do anything about this
	   *cx = NULL;
  }

  DEBUGMSG(browser, "context is %p\n", *cx);

  return rv;
}

NS_IMETHODIMP SashGtkBrowser::LoadURL(const char * url)
{
  gtk_moz_embed_load_url(GTK_MOZ_EMBED(m_embed), url);
  return NS_OK;
}

NS_IMETHODIMP SashGtkBrowser::CreateNewWindow(guint chromemask, GtkMozEmbed ** _retval)
{
  GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  sashIGtkBrowser *brows;
  NewSashGtkBrowser(&brows, m_runtime);

  GtkWidget * b_widget;
  brows->GetWidget(&b_widget);

  gtk_signal_connect(GTK_OBJECT(b_widget), "destroy_browser",
		     GTK_SIGNAL_FUNC(destroy_brows_cb), window);
  gtk_signal_connect(GTK_OBJECT(window), "delete",
		     GTK_SIGNAL_FUNC(delete_window_cb), NULL);

  gtk_container_add(GTK_CONTAINER(window), b_widget);
  gtk_widget_show_all(window);
  *_retval =  GTK_MOZ_EMBED(b_widget);
  return NS_OK;
  // XXX: Figure out how to delete the SashGtkBrowser; refcounting? delete signal?
}

// This sets up the sash protocol handler and the cache manager
void SashGtkBrowser::ConnectSashProtocolHandler(sashIActionRuntime *runtime) {
  nsresult rv;
  nsISupports *handler;
  sashICacheManagerNotify *cacheman;
  nsCOMPtr<nsIIOService> ioService(do_GetIOService(&rv));
  if (NS_FAILED(rv)) {
    cerr << "cannot instantiate ioService" << endl;
    return;
  }
  // Unlike sashFakeHandler, here we want our own protocol handler to be returned
  rv= ioService->GetProtocolHandler("http", (nsIProtocolHandler **) &handler);
  if (!handler) {
    cerr << "Http protocol not available" << endl;
    return;
  }
  rv= handler->QueryInterface(NS_GET_IID(sashICacheManagerNotify), (void **) &cacheman);
  if (!cacheman) {
    cerr << "Have no idea what went wrong " << hex << rv << endl;
    return;
  }
  char *tmp;
  std::string fufu;
  runtime->GetDirectory(&tmp);
  fufu= tmp;
  fufu+= "/cache";
  cerr << "Caching directory is " << fufu << endl;
  cacheman->SetCacheManager(new CacheManager(fufu, true));
  NS_RELEASE(cacheman);
  NS_RELEASE(handler);
}

NS_IMETHODIMP SashGtkBrowser::SetEnableSashContext(PRBool enable) {
	 m_enableSashContext = enable;
	 return NS_OK;
}

NS_IMETHODIMP SashGtkBrowser::GetEnableSashContext(PRBool * enable) {
	 *enable = m_enableSashContext;
	 return NS_OK;
}

/*
NS_GENERIC_FACTORY_CONSTRUCTOR(SashGtkBrowser)

  static nsModuleComponentInfo components[] = {
    { "SashGtkBrowser component",
      SASHGTKBROWSER_CID,
      SASHGTKBROWSER_CONTRACT_ID,
      SashGtkBrowserConstructor,
      NULL,
      NULL
    }
  };

NS_IMPL_NSGETMODULE("SashGtkBrowserModule", components)
*/
