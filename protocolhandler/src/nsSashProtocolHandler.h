
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu

Replaces Mozilla's HTTP Protocol handler with the Sash Protocol handler

*****************************************************************/

#ifndef NS_SASH_HTTP_PROTOCOL_HANDLER_H_DEF
#define NS_SASH_HTTP_PROTOCOL_HANDLER_H_DEF

#include "nsIProtocolHandler.h"
#include "nsCOMPtr.h"

#include "nsIHTTPProtocolHandler.h"
#include "nsIHTTPChannel.h"

//#include "nsIFileProtocolHandler.h"
//#include "../../../netwerk/protocol/file/src/nsIFileChannel.h"
#include "nsIChannel.h"


//class nsIHTTPProtocolHandler;
//class nsIHTTPChannel;

//#define SASH_PORT 80

#define NS_SASHPROTOCOLHANDLER_CID \
{\
	0x4A635CD4, 0x3202, 0x4077, {0x89, 0xE8, 0x66, 0x30, 0x4E, 0x08, 0x9A, 0x4E} \
}

class nsSashProtocolHttpHandler : public nsIProtocolHandler
{
public:
     NS_DECL_ISUPPORTS
     NS_DECL_NSIPROTOCOLHANDLER
	  
     nsSashProtocolHttpHandler();
     virtual ~nsSashProtocolHttpHandler();

     // Define a Create method to be used with a factory:
     static NS_METHOD Create(nsISupports* aOuter, const nsIID& aIID, void* *aResult);

private:
     //nsIHTTPProtocolHandler* m_pMozillaHttpProtoHandler;
     //	nsIHTTPChannel*         m_pMozillaHttpChannel;
     nsCOMPtr<nsIHTTPProtocolHandler> m_pMozillaHttpProtoHandler;
     //nsCOMPtr<nsIHTTPChannel>         m_pMozillaHttpChannel;
     nsCOMPtr<nsIChannel>             m_pMozillaHttpChannel;

     nsCOMPtr<nsIHTTPChannel>         m_pOrigHttpChannel;

     nsCOMPtr<nsIProtocolHandler>     m_FileHandler;
     nsCOMPtr<nsIFileChannel>         m_FileChannel;
};

#endif //#ifndef NS_SASH_HTTP_PROTOCOL_HANDLER_H_DEF
