
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu

Replaces HTTP channel to override default caching functionality

*****************************************************************/


#ifndef NS_SASH_CHANNEL_H_DEF
#define NS_SASH_CHANNEL_H_DEF

//#include "nsIHTTPChannel.h"

#include "nsIChannel.h"
#include "nsIStreamListener.h"
#include "nsILoadGroup.h"
#include "nsIFile.h"


//class nsIHTTPChannel;
//class nsIFileChannel;

class nsSashChannel : public nsIChannel
{
public:
     nsSashChannel(nsIChannel& aHTTPChannel, nsIChannel& aFileChannel);
                   //nsIHTTPChannel& aChannel,//nsHTTPChannel& aChannel,
		   //nsIFileChannel& aFileChannel);
     virtual ~nsSashChannel();

     NS_DECL_ISUPPORTS
     NS_DECL_NSIREQUEST
     NS_DECL_NSICHANNEL

private:
     //nsHTTPChannel&  m_HTTPChannel;
     
     //nsIHTTPChannel& m_HTTPChannel;
     //nsIFileChannel& m_FileChannel;
     nsIChannel& m_HTTPChannel;
     nsIChannel& m_FileChannel;

     nsIChannel&     m_Channel;

     nsCOMPtr<nsIStreamListener> mResponseDataListener;
     nsCOMPtr<nsILoadGroup>      mLoadGroup;

     nsCOMPtr<nsIChannel>        mFileTransport;
     nsCOMPtr<nsIFile>           mSpec;
};

#endif //#ifndef NS_SASH_CHANNEL_H_DEF
