
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu

Replaces Mozilla's HTTP Protocol handler with the Sash Protocol handler

*****************************************************************/

#include "nspr.h"
//#include "nsSashHttpChannel.h"
#include "nsSashProtocolHandler.h"
#include "nsIURL.h"
#include "nsCRT.h"
#include "nsIComponentManager.h"
#include "nsIServiceManager.h"
#include "nsIInterfaceRequestor.h"
#include "nsIProgressEventSink.h"
#include "nsIHTTPChannel.h"
#include "nsIHTTPProtocolHandler.h"
//#include "../../../netwerk/protocol/http/src/nsHTTPHandler.h"
//#include "nsHTTPChannel.h"
#include "nsIIOService.h"

//#include "nsSashProtocolChannel.h"
#include "nsSashChannel.h"

#include <iostream>
#include <cassert>

static NS_DEFINE_CID(kSimpleURICID, NS_SIMPLEURI_CID);

//--------------------------- Constructor destructor ------------------------
nsSashProtocolHttpHandler::nsSashProtocolHttpHandler()
{
     std::cout << "nsSashProtocolHttpHandler()" << std::endl;

     NS_INIT_REFCNT();
     /* Create mozilla handler here. Sandesh */

     nsresult rv;

     // Ask service manager for something that implements nsIIOService.
     //  io_service is a nsCOMPtr so it will handle service release.
     nsCOMPtr<nsIIOService> io_service(do_GetIOService(&rv));
     if (NS_FAILED(rv)) {
	  NS_ASSERTION( NS_SUCCEEDED(rv), 
			"Service manager cannot return an nsIIOService interface");
     }

     // Ask nsIIOService for http protocol handler
     nsCOMPtr<nsIProtocolHandler> handler;
     rv = io_service->GetProtocolHandler("http", getter_AddRefs(handler));

     // Store http handler
     m_pMozillaHttpProtoHandler = do_QueryInterface(handler, &rv);
     if (NS_FAILED(rv)) {
	  NS_ASSERTION( NS_SUCCEEDED(rv),
			"nsIProtocolHandler could query for nsIHTTPHandler interface");
     }

     // Store file handler
     nsCOMPtr<nsIProtocolHandler> file_handler;
     rv = io_service->GetProtocolHandler("file", getter_AddRefs(file_handler));
     m_FileHandler = do_QueryInterface(file_handler, &rv);
     if (NS_FAILED(rv)) {
	  NS_ASSERTION( NS_SUCCEEDED(rv),
			"nsIProtocolHandler could query for nsIFileHandler interface");
     }

     //XPCOM_NEW
     //m_pMozillaHttpChannel = nsnull;
}

nsSashProtocolHttpHandler::~nsSashProtocolHttpHandler()
{
     std::cout << "~nsSashProtocolHttpHandler() called" << std::endl;

     // Don't release mozilla handlers -- nsCOMPtr handles it. awu
}

//--------------------------- nsISupports interface ------------------------
NS_IMPL_ISUPPORTS(nsSashProtocolHttpHandler, NS_GET_IID(nsIProtocolHandler));

//------------------------- To support factory ------------------------------
NS_METHOD nsSashProtocolHttpHandler::Create(nsISupports* aOuter, const nsIID& aIID, void* *aResult)
{
     std::cout << "nsSashProtocolHttpHandler::Create() called" << std::endl;

     nsSashProtocolHttpHandler* pProtocolHandler = new nsSashProtocolHttpHandler();
     if (pProtocolHandler == nsnull)
	  return NS_ERROR_OUT_OF_MEMORY;
     NS_ADDREF(pProtocolHandler);
     nsresult rv = pProtocolHandler->QueryInterface(aIID, aResult);
     NS_RELEASE(pProtocolHandler);
     return rv;
}
    
//------------------------------- nsIProtocolHandler -------------------------
NS_IMETHODIMP nsSashProtocolHttpHandler::GetScheme(char* *result)
{
     std::cout << "nsSashProtocolHttpHandler::GetScheme() called" << std::endl;

     //*result = nsCRT::strdup("http");
     *result = nsCRT::strdup("sash");
     if (!*result)
	  return NS_ERROR_OUT_OF_MEMORY;
     return NS_OK;
}

NS_IMETHODIMP nsSashProtocolHttpHandler::GetDefaultPort(PRInt32 *result)
{
     std::cout << "nsSashProtocolHttpHandler::GetDefaultPort()" << std::endl;

     m_pMozillaHttpProtoHandler->GetDefaultPort(result);
     return NS_OK;
}

NS_IMETHODIMP nsSashProtocolHttpHandler::NewURI(const char *aSpec, nsIURI *aBaseURI, nsIURI **result)
{
     std::cout << "nsSashProtocolHttpHandler::NewURI() BEGIN" << std::endl;

     std::cout << "\taSpec=" << aSpec << std::endl;
     
     // char* uri_spec;
//      aBaseURI->GetSpec(&uri_spec);
//      std::cout << "\taBaseURI->GetSpec() returns " << uri_spec << std::endl;
     
     nsresult rv;
     //Instead of handling in nsSashHttpChannel, use existing Protocol handler for this
     //Sandesh
     NS_ASSERTION(m_pMozillaHttpProtoHandler,
		  "Invalid Mozilla HTTP Protocol Handler!");

     //rv = m_pMozillaHttpProtoHandler->NewURI(aSpec, aBaseURI, result);
     rv = m_FileHandler->NewURI(aSpec, aBaseURI, result);

     char* tmp_scheme;
     (*result)->GetScheme(&tmp_scheme);
     //std::cout << "\tm_pMozillaHttpProtoHandler created NewURI w/ scheme=" << tmp_scheme << std::endl;
     std::cout << "\tm_FileHandler created NewURI w/ scheme=" << tmp_scheme << std::endl;

     std::cout << "nsSashProtocolHttpHandler::NewURI() END\n" << std::endl;

     return rv;
}

NS_IMETHODIMP nsSashProtocolHttpHandler::NewChannel(nsIURI* url, nsIChannel* *result)
{
     std::cout << "nsSashProtocolHttpHandler::NewChannel() BEGIN" << std::endl;

     nsIURI* http_url;
     url->Clone(&http_url);

     char* old_spec;
     http_url->GetSpec(&old_spec);
     std::cout << "\tPassed url spec=" << old_spec << std::endl;

     http_url->SetScheme("http");
     std::cout << "\tSetting url scheme to http instead." << std::endl;

     //http_url->SetSpec("http://chameleon/index.html");

     // If we don't change scheme, we get 
     //  "ASSERTION: Non-HTTP request coming to HTTP Handler!!!"

     char* tmp_spec;
     http_url->GetSpec(&tmp_spec);
     std::cout << "\tNow url spec=" << tmp_spec << std::endl;

     nsresult rv;
     //Instead of creating nsSashHttpChannel, create existing mozilla channel
     //Sandesh
     nsIHTTPChannel* i_http_channel;
     // Ask HTTP Protocol handler to return nsIHTTPChannel*
     rv = m_pMozillaHttpProtoHandler->NewChannel(http_url, 
						 (nsIChannel**)(&i_http_channel));
     m_pMozillaHttpChannel = i_http_channel;

     // test time check -- set spec back to normal
     url->SetSpec(old_spec);

     url->SetScheme("file");
     std::cout << "\tSetting url scheme to file instead." << std::endl;

     url->GetSpec(&tmp_spec);
     std::cout << "\tNow url spec=" << tmp_spec << std::endl;     

     std::cout << "\tCreating file channel from file protocol handler" << std::endl;

     // Ask file protocol handler to return nsIFileChannel*
     nsIFileChannel* i_file_channel;
     rv = m_FileHandler->NewChannel(url,
				    (nsIChannel**)(&i_file_channel));
     m_FileChannel = i_file_channel;


     NS_ASSERTION( NS_SUCCEEDED(rv),
		   "Mozilla HTTP Protocol Handler could not return channel");

     // Cast from nsIHTTPChannel* to nsHTTPChannel*
//     nsHTTPChannel* http_channel = (nsHTTPChannel*)(i_http_channel);
     // nsIFileChannel* to nsFileChannel*
     //nsFileChannel* file_channel = (nsFileChannel*)(i_file_channel);

     // store orig http_channel?
//     m_pOrigHttpChannel = http_channel;
     
     // return channel in result

//      nsSashProtocolChannel* pSashChannel = new nsSashProtocolChannel(*http_channel); 
//      m_pMozillaHttpChannel = pSashChannel;
//      *result = pSashChannel;

     std::cout << "\twrapping nsHTTPChannel..." << std::endl;

     // wrap (contain) nsHTTPChannel with a nsSashChannel
     nsSashChannel* pWrappedSashChannel = new nsSashChannel(*i_http_channel,//*http_channel,
							    *i_file_channel);
     // store copy of channel, so that it is AddRef'd
     m_pMozillaHttpChannel = pWrappedSashChannel;
     *result = pWrappedSashChannel;

     //*result = http_channel;
     

     std::cout << "nsSashProtocolHttpHandler::NewChannel() END\n" << std::endl;

     return rv;
}
