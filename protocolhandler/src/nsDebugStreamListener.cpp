
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu

Intercepts HTTP streams to override default caching functionality

*****************************************************************/



#include "nsDebugStreamListener.h"
#include <iostream>
#include <cassert>
#include "nsIInputStream.h"
#include "nsIFileStreams.h"
#include "nsILocalFile.h"

/* Implementation file */
NS_IMPL_ISUPPORTS2(nsDebugStreamListener,
		   nsIStreamListener, nsIStreamObserver);


nsDebugStreamListener::nsDebugStreamListener(nsIStreamListener* apISL,
					     nsIChannel*        apIChannel,
					     const char*        aDebugPrefix)
     : m_pISL(apISL), 
       m_FileChannel(apIChannel), m_DebugPrefix(aDebugPrefix)
{ 
     NS_INIT_ISUPPORTS();
}

nsDebugStreamListener::~nsDebugStreamListener()
{
}

#include "nsIComponentManager.h"

/* void onDataAvailable (in nsIChannel channel, in nsISupports ctxt, 
   in nsIInputStream inStr, in unsigned long sourceOffset, in unsigned long count); */
NS_IMETHODIMP nsDebugStreamListener::OnDataAvailable(nsIChannel *channel, nsISupports *ctxt, 
						     nsIInputStream *inStr, 
						     PRUint32 sourceOffset, PRUint32 count)
{
     std::cout << m_DebugPrefix << " -- nsDebugStreamListener::OnDataAvailable()" << std::endl;

     PRUint32 bytes_avail; 
     inStr->Available(&bytes_avail);
     std::cout << m_DebugPrefix << " -- " << bytes_avail << " bytes available." << std::endl;

     //create file output stream
     nsresult rv;
     nsIFileOutputStream* fos = nsnull;
     rv = nsComponentManager::CreateInstance(NS_LOCALFILEOUTPUTSTREAM_PROGID,
					     nsnull, 
					     NS_GET_IID(nsIFileOutputStream),
					     (void**)&fos);

     //create local file
     nsILocalFile* file = nsnull;
     rv = nsComponentManager::CreateInstance(NS_LOCAL_FILE_PROGID,
					     nsnull, 
					     NS_GET_IID(nsILocalFile),
					     (void**)&file);
     //mSpec = file;
     char* path = "/home/awu/sash_out.txt";
     rv = file->InitWithPath(path);

     fos->Init(file, PR_CREATE_FILE | PR_RDWR, PR_IRUSR | PR_IWUSR);

//#ifdef 0
     if (bytes_avail > 0) {
	  char* buf = new char[bytes_avail];
	  PRUint32 bytes_read;
	  inStr->Read(buf, bytes_avail, &bytes_read);

// 	  std::cout << "buf[] = [\n" << std::endl;
// 	  for (int i=0; i < (int)bytes_read; i++) {
// 	       std::cout << buf[i];
// 	  }
// 	  std::cout << std::endl;

	  std::cout << "\n]\nWriting to " << path << " ..." << std::endl;
	  PRUint32 write_count;
	  fos->Write(buf, bytes_read, &write_count);

	  std::cout << write_count << " bytes written." << std::endl;

	  delete[] buf;
     }
// #endif //#ifdef 0

     //create file input stream
     nsIFileInputStream* fis = nsnull;
     rv = nsComponentManager::CreateInstance(NS_LOCALFILEINPUTSTREAM_PROGID,
					     nsnull, 
					     NS_GET_IID(nsIFileInputStream),
					     (void**)&fis);
     fis->Init(file, PR_RDONLY, PR_IRUSR | PR_IWUSR);

     return m_pISL->OnDataAvailable(channel/*m_FileChannel*/, ctxt, /*inStr*/fis, 
				    sourceOffset, count);
}

/* void onStartRequest (in nsIChannel channel, in nsISupports ctxt); */
NS_IMETHODIMP nsDebugStreamListener::OnStartRequest(nsIChannel* channel, nsISupports* ctxt)
{
     std::cout << m_DebugPrefix << " -- nsDebugStreamListener::OnStartRequest()" << std::endl;

     return m_pISL->OnStartRequest(channel, ctxt);
}

/* void onStopRequest (in nsIChannel channel, in nsISupports ctxt, 
   in nsresult status, in wstring errorMsg); */
NS_IMETHODIMP nsDebugStreamListener::OnStopRequest(nsIChannel *channel, 
						   nsISupports *ctxt, 
						   nsresult status, 
						   const PRUnichar *errorMsg)
{
     std::cout << m_DebugPrefix << " -- nsDebugStreamListener::OnStopRequest()" << std::endl;

     return m_pISL->OnStopRequest(channel, ctxt, status, errorMsg);
}


/* End of implementation class template. */

