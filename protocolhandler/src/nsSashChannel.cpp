
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu

Replaces HTTP channel to override default caching functionality

*****************************************************************/


#include "nsSashChannel.h"
//#include "../../../netwerk/protocol/http/src/nsHTTPChannel.h"

// for nsHTTPAtoms (checking last modified)
//#include "../../../netwerk/protocol/http/src/nsHTTPAtoms.h"
//#include "../../../netwerk/protocol/http/src/nsHTTPResponse.h"

//#include "nsIHTTPChannel.h"
//#include "nsIFileChannel.h"
#include "nsIChannel.h"

#include "nsIURI.h"
//#include "nsSashProtocolChannel.h"
#include <iostream>

//debug help
void PrintChannelSpec(nsIChannel& aChannel) {
     nsIURI* uri;
     aChannel.GetURI(&uri);
     char* spec;
     uri->GetSpec(&spec);

     std::cout << spec << std::flush;
}

NS_IMPL_THREADSAFE_ISUPPORTS2(nsSashChannel,
			      nsIChannel,
			      nsIRequest);

nsSashChannel::nsSashChannel(nsIChannel& aHTTPChannel,
			     nsIChannel& aFileChannel)
                             //nsIHTTPChannel& aHTTPChannel,//nsHTTPChannel& aHTTPChannel,
                             //nsIFileChannel& aFileChannel) 
     : m_HTTPChannel(aHTTPChannel),
       m_FileChannel(aFileChannel),
       m_Channel(aHTTPChannel)
{
     NS_INIT_ISUPPORTS();
     /* member initializers and constructor code */

     std::cout << "nsSashChannel ctor -- m_HTTPChannel spec=";
     PrintChannelSpec(m_HTTPChannel);

     std::cout << "\nnsSashChannel ctor -- m_FileChannel spec=";
     PrintChannelSpec(m_FileChannel);
     std::cout << std::endl;
}

nsSashChannel::~nsSashChannel()
{
     /* destructor code */
}

////////////////////////////////////////////////////////////////////////////////
// nsIRequest methods:

NS_IMETHODIMP
nsSashChannel::IsPending(PRBool *result)
{
     std::cout << "nsSashChannel::IsPending()" << std::endl;
     return m_Channel.IsPending(result);
}

NS_IMETHODIMP
nsSashChannel::GetStatus(nsresult *status)
{
     std::cout << "nsSashChannel::GetStatus()" << std::endl;
    return m_Channel.GetStatus(status);
}

NS_IMETHODIMP
nsSashChannel::Cancel(nsresult status)
{
     std::cout << "nsSashChannel::Cancel()" << std::endl;
    return m_Channel.Cancel(status);
}

NS_IMETHODIMP
nsSashChannel::Suspend(void)
{
     std::cout << "nsSashChannel::Suspend()" << std::endl;
     return m_Channel.Suspend();
}

NS_IMETHODIMP
nsSashChannel::Resume(void)
{
     std::cout << "nsSashChannel::Resume()" << std::endl;
     return m_Channel.Resume();
}

////////////////////////////////////////////////////////////////////////////////
// nsIChannel methods:

NS_IMETHODIMP
nsSashChannel::GetOriginalURI(nsIURI* *o_URL)
{
     std::cout << "nsSashChannel::GetOriginalURI()" << std::endl;
     return m_Channel.GetOriginalURI(o_URL);
}

NS_IMETHODIMP
nsSashChannel::SetOriginalURI(nsIURI* o_URL)
{
     std::cout << "nsSashChannel::SetOriginalURI()" << std::endl;

     char* spec;
     o_URL->GetSpec(&spec);
     std::cout << "\toriginal URI = " << spec << std::endl;

     return m_Channel.SetOriginalURI(o_URL);
}

NS_IMETHODIMP
nsSashChannel::GetURI(nsIURI* *o_URL)
{
     nsresult rv;
     std::cout << "nsSashChannel::GetURI()" << std::endl;
     rv = m_Channel.GetURI(o_URL);

     char* spec;
     (*o_URL)->GetSpec(&spec);
     std::cout << "\tspec = " << spec << std::endl;

     return rv;
}

NS_IMETHODIMP
nsSashChannel::SetURI(nsIURI* o_URL)
{
     std::cout << "nsSashChannel::SetURI()" << std::endl;
     return m_Channel.SetURI(o_URL);
}

NS_IMETHODIMP
nsSashChannel::OpenInputStream(nsIInputStream **o_Stream)
{
     std::cout << "nsSashChannel::OpenInputStream()" << std::endl;
     return m_Channel.OpenInputStream(o_Stream);
}

NS_IMETHODIMP
nsSashChannel::OpenOutputStream(nsIOutputStream **_retval)
{
     std::cout << "nsSashChannel::OpenOutputStream()" << std::endl;
     return m_Channel.OpenOutputStream(_retval);
}


#include "nsIFileTransportService.h"
static NS_DEFINE_CID(kFileTransportServiceCID, NS_FILETRANSPORTSERVICE_CID);
#include "nsDirectoryService.h"
#include "nsIComponentManager.h"

#include "nsDebugStreamListener.h"


NS_IMETHODIMP
nsSashChannel::AsyncRead(nsIStreamListener *aListener, nsISupports *aContext)
{
     std::cout << "nsSashChannel::AsyncRead() BEGIN" << std::endl;
     // Add to loadGroup?

     //nsSashProtocolChannel::SetResponseDataListener(m_HTTPChannel, (nsIStreamListener*)0);
      nsDebugStreamListener* pISL = new nsDebugStreamListener(aListener,
							      &m_FileChannel,
							      "@@@ nsSashChannel's HTTP listener:");

     if (aListener) {
	  //std::cout << "\tCreating mResponseDataListener = new nsHTTPFinalListener(...);" << std::endl;
	  //mResponseDataListener = new nsHTTPFinalListener ( &m_HTTPChannel, listener, aContext );
//(this, listener, aContext);
     } else {
	  //std::cout << "\tNot creating mResponseDataListener" << std::endl;
	  //mResponseDataListener = listener;
     }

     //nsHTTPResponseListener* cache_listener;
     //cache_listener = new nsHTTPCacheListener(&m_HTTPChannel,//this, 
     //				      m_HTTPChannel.mHandler);
     //NS_ADDREF(cache_listener);
     //cache_listener->SetListener(mResponseDataListener);


     // use http channel to check time stamp
//      nsresult http_status;
//      m_HTTPChannel.GetStatus(&http_status);
//      if (http_status == NS_OK) {
// 	  std::cout << "\thttp_status == NS_OK" << std::endl; 
//      } else {
// 	  std::cout << "\thttp_status not NS_OK!" << std::endl; 
//      }

     std::cout << "\tm_HTTPChannel's spec=";
     PrintChannelSpec(m_HTTPChannel);
     std::cout << std::endl;

//     std::cout << "\tm_HTTPChannel.mResponse = " << m_HTTPChannel.mResponse << std::endl;
     std::cout << "\tm_HTTPChannel.AsyncRead()" << std::endl;
      nsresult rv_http = m_HTTPChannel.AsyncRead(
	   //aListener,
	   //(pISL->m_pISL), 
	   pISL,
	   aContext);

     if (rv_http != NS_OK) {
	  std::cout << "\tm_HTTPChannel.AsyncRead() does not return NS_OK!" << std::endl;
     }

   //   m_HTTPChannel.GetStatus(&http_status);
//      if (http_status == NS_OK) {
// 	  std::cout << "\thttp_status == NS_OK" << std::endl; 
//      } else {
// 	  std::cout << "\thttp_status not NS_OK!" << std::endl; 
//      }


     nsresult rv;
     // Retrieve the value of the 'Last-Modified:' header, if present
//      PRTime lastModified;
//      PRBool lastModifiedHeaderIsPresent;

//      std::cout << "\ttrying to ParseDateHeader()" << std::endl;
//      std::cout << "\tm_HTTPChannel.mResponse = " << m_HTTPChannel.mResponse << std::endl;
//      rv = m_HTTPChannel.mResponse->ParseDateHeader(nsHTTPAtoms::Last_Modified, &lastModified,
//  						   &lastModifiedHeaderIsPresent);
//      std::cout << "\tfinished ParseDateHeader()" << std::endl;

//      std::cout << "m_HTTPChannel.mResponse: lastModified=" << lastModified << std::endl;

//      nsDebugStreamListener* pISL_file = new nsDebugStreamListener(*aListener,
// 								  "nsSashChannel's FILE listener:");

     //m_FileChannel.AsyncRead((nsIStreamListener*)listener, (nsISupports*)aContext);
//      nsresult rv_file = m_FileChannel.AsyncRead(
//           aListener,
// 	  //pISL_file,
// 	  aContext);

//      if (rv_file != NS_OK) {
// 	  std::cout << "\tm_FileChannel.AsyncRead() does not return NS_OK!" << std::endl;
//      } 

     std::cout << "nsSashChannel::AsyncRead() END" << std::endl;
     return rv_http;//file;

/*************************************************************************/
     

     //mCacheTransport->AsyncRead(cache_listener, aContext);
    
     //mResponseContext = aContext; // ?
     // create file transport service
     //nsresult rv;
     nsCOMPtr<nsIFileTransportService> fts = 
       do_GetService(NS_FILETRANSPORTSERVICE_CONTRACTID, &rv);
     if(NS_FAILED(rv)) return rv;

     std::cout << "\tAsking file transport service to create a transport" << std::endl;

     //NS_WITH_SERVICE(nsIProperties, directoryService, NS_DIRECTORY_SERVICE_PROGID, &rv);
     //rv = directoryService->Get("system.OS_CurrentProcessDirectory", NS_GET_IID(nsIFile),
     //			getter_AddRefs(mSpec));

     nsILocalFile* file = nsnull;
     rv = nsComponentManager::CreateInstance(NS_LOCAL_FILE_PROGID,
					     nsnull, 
					     NS_GET_IID(nsILocalFile),
					     (void**)&file);
     mSpec = file;
     rv = file->InitWithPath("/tmp/sash.txt");

     PRBool exists;
     file->Exists(&exists);
     if (exists == PR_TRUE) {
	  std::cout << "\tFile exists" << std::endl;
     } else {
	  std::cout << "\tFile does not exist!" << std::endl;
     }

     char* file_name;
     mSpec->GetLeafName(&file_name);
     std::cout << "\tmSpec->GetLeafName() returns " << file_name << std::endl;

     //mSpec; //do stuff
     rv = fts->CreateTransport(mSpec, PR_RDONLY, PR_IRUSR | PR_IWUSR,
			       getter_AddRefs(mFileTransport));
     if (NS_FAILED(rv)) return rv;

     nsIURI* http_uri;
     this->GetURI(&http_uri);
     char* http_spec;
     http_uri->GetSpec(&http_spec);

     std::cout << "\tCalling mFileTranport->SetURI() w/ param spec = " << http_spec << std::endl;
     mFileTransport->SetURI(http_uri);

     nsIURI* URI;
     std::cout << "\tCalling mFileTransport->GetURI()" << std::endl;
     mFileTransport->GetURI(&URI);
     std::cout << "\tCalling URI->GetSpec()" << std::endl;
     char* spec;
     URI->GetSpec(&spec);
     std::cout << "\tmFileTransport->GetURI ... GetSpec() returns " << spec << std::endl;

     std::cout << "\tAsking file transport to AsyncRead()" << std::endl;

     //nsCOMPtr<nsIStreamListener> tempListener(this); //?
     rv = mFileTransport->AsyncRead(/*tempListener*/aListener, aContext);

     std::cout << "nsSashChannel::AsyncRead() END" << std::endl;
    
     //NS_RELEASE(cache_listener);
     //return m_HTTPChannel.AsyncRead(listener, aContext);
     return NS_OK;
}

NS_IMETHODIMP
nsSashChannel::AsyncWrite(nsIInputStream *fromStream,
                          nsIStreamObserver *observer,
                          nsISupports *ctxt)
{
     std::cout << "nsSashChannel::AsyncWrite()" << std::endl;
     return m_Channel.AsyncWrite(fromStream,
				     observer,
				     ctxt);
}

NS_IMETHODIMP
nsSashChannel::GetLoadAttributes(PRUint32 *aLoadAttributes)
{
     std::cout << "nsSashChannel::GetLoadAttributes()" << std::endl;
     return m_Channel.GetLoadAttributes(aLoadAttributes);
}

NS_IMETHODIMP
nsSashChannel::SetLoadAttributes(PRUint32 aLoadAttributes)
{
     std::cout << "nsSashChannel::SetLoadAttributes()" << std::endl;
     return m_Channel.SetLoadAttributes(aLoadAttributes);
}

NS_IMETHODIMP
nsSashChannel::GetContentType(char * *aContentType)
{
     std::cout << "nsSashChannel::GetContentType()" << std::endl;
     return m_Channel.GetContentType(aContentType);
}

NS_IMETHODIMP
nsSashChannel::SetContentType(const char *aContentType)
{
     std::cout << "nsSashChannel::SetContentType()" << std::endl;
     return m_Channel.SetContentType(aContentType);
}


NS_IMETHODIMP
nsSashChannel::GetContentLength(PRInt32 *aContentLength)
{
     std::cout << "nsSashChannel::GetContentLength()" << std::endl;
     return m_Channel.GetContentLength(aContentLength);
}


NS_IMETHODIMP
nsSashChannel::SetContentLength(PRInt32 aContentLength)
{
     std::cout << "nsSashChannel::SetContentLength()" << std::endl;
     return m_Channel.SetContentLength(aContentLength);
}

NS_IMETHODIMP
nsSashChannel::GetTransferOffset(PRUint32 *aTransferOffset)
{
     std::cout << "nsSashChannel::GetTransferOffset()" << std::endl;
     return m_Channel.GetTransferOffset(aTransferOffset);
}

NS_IMETHODIMP
nsSashChannel::SetTransferOffset(PRUint32 aTransferOffset)
{
     std::cout << "nsSashChannel::SetTransferOffset()" << std::endl;
     return m_Channel.SetTransferOffset(aTransferOffset);
}

NS_IMETHODIMP
nsSashChannel::GetTransferCount(PRInt32 *aTransferCount)
{
     std::cout << "nsSashChannel::GetTransferCount()" << std::endl;
     return m_Channel.GetTransferCount(aTransferCount);
}

NS_IMETHODIMP
nsSashChannel::SetTransferCount(PRInt32 aTransferCount)
{
     std::cout << "nsSashChannel::SetTransferCount()" << std::endl;
     return m_Channel.SetTransferCount(aTransferCount);
}

NS_IMETHODIMP 
nsSashChannel::GetSecurityInfo(nsISupports * *aSecurityInfo)
{
     std::cout << "nsSashChannel::GetSecurityInfo()" << std::endl;
     return m_Channel.GetSecurityInfo(aSecurityInfo);
}

NS_IMETHODIMP
nsSashChannel::GetBufferSegmentSize(PRUint32 *aBufferSegmentSize)
{
     std::cout << "nsSashChannel::GetBufferSegmentSize()" << std::endl;
     return m_Channel.GetBufferSegmentSize(aBufferSegmentSize);
}

NS_IMETHODIMP
nsSashChannel::SetBufferSegmentSize(PRUint32 aBufferSegmentSize)
{
     std::cout << "nsSashChannel::SetBufferSegmentSize()" << std::endl;
     return m_Channel.SetBufferSegmentSize(aBufferSegmentSize);
}

NS_IMETHODIMP
nsSashChannel::GetBufferMaxSize(PRUint32 *aBufferMaxSize)
{
     std::cout << "nsSashChannel::GetBufferMaxSize()" << std::endl;
     return m_Channel.GetBufferMaxSize(aBufferMaxSize);
}

NS_IMETHODIMP
nsSashChannel::SetBufferMaxSize(PRUint32 aBufferMaxSize)
{
     std::cout << "nsSashChannel::SetBufferMaxSize()" << std::endl;
     return m_Channel.SetBufferMaxSize(aBufferMaxSize);
}

NS_IMETHODIMP
nsSashChannel::GetLocalFile(nsIFile* *file)
{
     std::cout << "nsSashChannel::GetLocalFile()" << std::endl;
     return m_Channel.GetLocalFile(file);
}

NS_IMETHODIMP
nsSashChannel::GetPipeliningAllowed(PRBool *aPipeliningAllowed)
{
     std::cout << "nsSashChannel::GetPipeliningAllowed()" << std::endl;
     return m_Channel.GetPipeliningAllowed(aPipeliningAllowed);
}
 
NS_IMETHODIMP
nsSashChannel::SetPipeliningAllowed(PRBool aPipeliningAllowed)
{
     std::cout << "nsSashChannel::SetPipeliningAllowed()" << std::endl;
     return m_Channel.SetPipeliningAllowed(aPipeliningAllowed);
}

NS_IMETHODIMP
nsSashChannel::GetLoadGroup(nsILoadGroup * *aLoadGroup)
{
     // return local copy?

     std::cout << "nsSashChannel::GetLoadGroup()" << std::endl;
     return m_Channel.GetLoadGroup(aLoadGroup);
}

NS_IMETHODIMP
nsSashChannel::SetLoadGroup(nsILoadGroup *aGroup)
{
     mLoadGroup = aGroup; // store copy here

     std::cout << "nsSashChannel::SetLoadGroup(nsILoadGroup* aGroup = " << aGroup << ")" << std::endl;
     return m_Channel.SetLoadGroup(aGroup);
}

NS_IMETHODIMP
nsSashChannel::GetOwner(nsISupports * *aOwner)
{
     std::cout << "nsSashChannel::GetOwner()" << std::endl;
     return m_Channel.GetOwner(aOwner);
}

NS_IMETHODIMP
nsSashChannel::SetOwner(nsISupports * aOwner)
{
     std::cout << "nsSashChannel::SetOwner()" << std::endl;
     return m_Channel.SetOwner(aOwner);
}

NS_IMETHODIMP
nsSashChannel::GetNotificationCallbacks(
     nsIInterfaceRequestor* *aNotificationCallbacks)
{
     std::cout << "nsSashChannel::GetNotificationCallbacks()" << std::endl;
     return m_Channel.GetNotificationCallbacks(aNotificationCallbacks);
}

NS_IMETHODIMP
nsSashChannel::SetNotificationCallbacks(nsIInterfaceRequestor* 
					aNotificationCallbacks)
{
      std::cout << "nsSashChannel::SetNotificationCallbacks(nsIInterfaceRequestor* aNotificationCallbacks="
 	       << aNotificationCallbacks << ")" << std::endl;
      nsresult rv = m_Channel.SetNotificationCallbacks(aNotificationCallbacks);
      //nsresult rv = m_HTTPChannel.SetNotificationCallbacks(aNotificationCallbacks);
      if (rv == NS_OK) {
	   std::cout << "nsSashChannel::SetNotificationCallbacks() returned NS_OK" << std::endl;
      } else {
	   std::cout << "nsSashChannel::SetNotificationCallbacks() did not return NS_OK!" << std::endl;
      }

      return rv;
}


////////////////////////////////////////////////////////////////////////////////
// nsIHTTPChannel methods

// NS_IMETHODIMP nsSashChannel::GetRequestHeader(nsIAtom *headerAtom, char **_retval)
// {
//      std::cout << "nsSashChannel::GetRequestHeader()" << std::endl;
//      return m_Channel.GetRequestHeader(headerAtom, _retval);
// }

// NS_IMETHODIMP nsSashChannel::SetRequestHeader(nsIAtom *headerAtom, const char *value)
// {
//      std::cout << "nsSashChannel::SetRequestHeader()" << std::endl;
//      return m_Channel.SetRequestHeader(headerAtom, value);
// }

// NS_IMETHODIMP nsSashChannel::GetRequestHeaderEnumerator(nsISimpleEnumerator **_retval)
// {
//      std::cout << "nsSashChannel::GetRequestHeaderEnumerator()" << std::endl;
//      return m_Channel.GetRequestHeaderEnumerator(_retval);
// }

// NS_IMETHODIMP nsSashChannel::SetRequestMethod(nsIAtom *methodAtom)
// {
//      std::cout << "nsSashChannel::SetRequestMethod()" << std::endl;
//      return m_Channel.SetRequestMethod(methodAtom);
// }

// NS_IMETHODIMP nsSashChannel::GetRequestMethod(nsIAtom **methodAtom)
// {
//      std::cout << "nsSashChannel::GetRequestMethod()" << std::endl;
//      return m_Channel.GetRequestMethod(methodAtom);
// }

// NS_IMETHODIMP nsSashChannel::GetResponseHeader(nsIAtom *headerAtom, char **_retval)
// {
//      std::cout << "nsSashChannel::GetResponseHeader()" << std::endl;
//      return m_Channel.GetResponseHeader(headerAtom, _retval);
// }

// NS_IMETHODIMP nsSashChannel::SetResponseHeader(nsIAtom *headerAtom, const char *headerValue)
// {
//      std::cout << "nsSashChannel::SetResponseHeader()" << std::endl;
//      return m_Channel.SetResponseHeader(headerAtom, headerValue);
// }

// NS_IMETHODIMP nsSashChannel::GetResponseHeaderEnumerator(nsISimpleEnumerator **_retval)
// {
//      std::cout << "nsSashChannel::GetResponseHeaderEnumerator()" << std::endl;
//      return m_Channel.GetResponseHeaderEnumerator(_retval);
// }

// NS_IMETHODIMP nsSashChannel::GetUploadStream(nsIInputStream * *aUploadStream)
// {
//      std::cout << "nsSashChannel::GetUploadStream()" << std::endl;
//      return m_Channel.GetUploadStream(aUploadStream);
// }
// NS_IMETHODIMP nsSashChannel::SetUploadStream(nsIInputStream * aUploadStream)
// {
//      std::cout << "nsSashChannel::SetUploadStream()" << std::endl;
//      return m_Channel.SetUploadStream(aUploadStream);
// }

// NS_IMETHODIMP nsSashChannel::SetReferrer(nsIURI *referrer, PRUint32 referrerLevel)
// {
//      std::cout << "nsSashChannel::SetReferrer()" << std::endl;
//      return m_Channel.SetReferrer(referrer, referrerLevel);
// }

// NS_IMETHODIMP nsSashChannel::GetReferrer(nsIURI * *aReferrer)
// {
//      std::cout << "nsSashChannel::GetReferrer()" << std::endl;
//      return m_Channel.GetReferrer(aReferrer);
// }

// NS_IMETHODIMP nsSashChannel::GetResponseStatus(PRUint32 *aResponseStatus)
// {
//      std::cout << "nsSashChannel::GetResponseStatus()" << std::endl;
//      return m_Channel.GetResponseStatus(aResponseStatus);
// }

// NS_IMETHODIMP nsSashChannel::GetResponseString(char * *aResponseString)
// {
//      std::cout << "nsSashChannel::GetResponseString()" << std::endl;
//      return m_Channel.GetResponseString(aResponseString);
// }

// NS_IMETHODIMP nsSashChannel::GetEventSink(nsIHTTPEventSink * *aEventSink)
// {
//      std::cout << "nsSashChannel::GetEventSink()" << std::endl;
//      return m_Channel.GetEventSink(aEventSink);
// }

// NS_IMETHODIMP nsSashChannel::GetResponseDataListener(nsIStreamListener * *aResponseDataListener)
// {
//      std::cout << "nsSashChannel::GetResponseDataListener()" << std::endl;
//      return m_Channel.GetResponseDataListener(aResponseDataListener);
// }

// NS_IMETHODIMP nsSashChannel::GetCharset(char * *aCharset)
// {
//      std::cout << "nsSashChannel::GetCharset()" << std::endl;
//      return m_Channel.GetCharset(aCharset);
// }

// NS_IMETHODIMP nsSashChannel::GetAuthTriedWithPrehost(PRBool *aAuthTriedWithPrehost)
// {
//      std::cout << "nsSashChannel::GetAuthTriedWithPrehost()" << std::endl;
//      return m_Channel.GetAuthTriedWithPrehost(aAuthTriedWithPrehost);
// }
// NS_IMETHODIMP nsSashChannel::SetAuthTriedWithPrehost(PRBool aAuthTriedWithPrehost)
// {
//      std::cout << "nsSashChannel::SetAuthTriedWithPrehost()" << std::endl;
//      return m_Channel.SetAuthTriedWithPrehost(aAuthTriedWithPrehost);
// }

// NS_IMETHODIMP nsSashChannel::GetApplyConversion(PRBool *aApplyConversion)
// {
//      std::cout << "nsSashChannel::GetApplyConversion()" << std::endl;
//      return m_Channel.GetApplyConversion(aApplyConversion);
// }
// NS_IMETHODIMP nsSashChannel::SetApplyConversion(PRBool aApplyConversion)
// {
//      std::cout << "nsSashChannel::SetApplyConversion()" << std::endl;
//      return m_Channel.SetApplyConversion(aApplyConversion);
// }

// NS_IMETHODIMP nsSashChannel::GetUsingProxy(PRBool *aUsingProxy)
// {
//      std::cout << "nsSashChannel::GetUsingProxy()" << std::endl;
//      return m_Channel.GetUsingProxy(aUsingProxy);
// }

// NS_IMETHODIMP nsSashChannel::GetUsingTransparentProxy(PRBool *aUsingTransparentProxy)
// {
//      std::cout << "nsSashChannel::GetUsingTransparentProxy()" << std::endl;
//      return m_Channel.GetUsingTransparentProxy(aUsingTransparentProxy);
// }

// NS_IMETHODIMP nsSashChannel::GetPrompter(nsIPrompt * *aPrompter)
// {
//      std::cout << "nsSashChannel::GetPrompter()" << std::endl;
//      return m_Channel.GetPrompter(aPrompter);
// }

// NS_IMETHODIMP nsSashChannel::GetProxyRequestURI(char * *aProxyRequestURI)
// {
//      std::cout << "nsSashChannel::GetProxyRequestURI()" << std::endl;
//      return m_Channel.GetProxyRequestURI(aProxyRequestURI);
// }
// NS_IMETHODIMP nsSashChannel::SetProxyRequestURI(const char * aProxyRequestURI)
// {
//      std::cout << "nsSashChannel::SetProxyRequestURI()" << std::endl;
//      return m_Channel.SetProxyRequestURI(aProxyRequestURI);
// }



////////////////////////////////////////////////////////////////////////////////
// nsIInterfaceRequestor method
// NS_IMETHODIMP
// nsSashChannel::GetInterface(const nsIID &anIID, void **aResult ) {
//      std::cout << "nsSashChannel::GetInterface()" << std::endl;
//      return m_Channel.GetInterface(anIID, aResult);
// }


////////////////////////////////////////////////////////////////////////////////
// nsIProgressEventSink methods
// NS_IMETHODIMP
// nsSashChannel::OnStatus(nsIChannel *aChannel,
//                         nsISupports *aContext,
//                         const PRUnichar *aMsg) {
//      std::cout << "nsSashChannel::OnStatus()" << std::endl;
//      return m_Channel.OnStatus(aChannel, aChannel, aMsg);
// }

// NS_IMETHODIMP
// nsSashChannel::OnProgress(nsIChannel* aChannel, nsISupports* aContext,
// 			  PRUint32 aProgress, PRUint32 aProgressMax) {
//      std::cout << "nsSashChannel::OnProgress()" << std::endl;
//      return m_Channel.OnProgress(aChannel, aContext, aProgress, aProgressMax);
// }


////////////////////////////////////////////////////////////////////////////////
// nsIProxy methods
// NS_IMETHODIMP
// nsSashChannel::GetProxyHost(char* *o_ProxyHost)
// {
//      std::cout << "nsSashChannel::GetProxyHost()" << std::endl;
//      return m_Channel.GetProxyHost(o_ProxyHost);
// }

// NS_IMETHODIMP
// nsSashChannel::SetProxyHost(const char* i_ProxyHost) 
// {
//      std::cout << "nsSashChannel::SetProxyHost()" << std::endl;
//      return m_Channel.SetProxyHost(i_ProxyHost);
// }

// NS_IMETHODIMP
// nsSashChannel::GetProxyPort(PRInt32 *aPort)
// {
//      std::cout << "nsSashChannel::GetProxyPort()" << std::endl;
//      return m_Channel.GetProxyPort(aPort);
// }

// NS_IMETHODIMP
// nsSashChannel::SetProxyPort(PRInt32 i_ProxyPort) 
// {
//      std::cout << "nsSashChannel::SetProxyPort()" << std::endl;
//      return m_Channel.SetProxyPort(i_ProxyPort);
// }

// NS_IMETHODIMP nsSashChannel::GetProxyType(char * *o_ProxyType)
// {
//      std::cout << "nsSashChannel::GetProxyType()" << std::endl;
//      return m_Channel.GetProxyType(o_ProxyType);
// }

// NS_IMETHODIMP nsSashChannel::SetProxyType(const char * i_ProxyType)
// {
//      std::cout << "nsSashChannel::SetProxyType()" << std::endl;
//      return m_Channel.SetProxyType(i_ProxyType);
// }


////////////////////////////////////////////////////////////////////////////////
// nsIStreamAsFile methods
// NS_IMETHODIMP 
// nsSashChannel::GetFile(nsIFile * *aFile)
// {
//      std::cout << "nsSashChannel::GetFile()" << std::endl;
//      return m_Channel.GetFile(aFile);
// }

// NS_IMETHODIMP 
// nsSashChannel::AddObserver(nsIStreamAsFileObserver *aObserver) 
// {
//      std::cout << "nsSashChannel::AddObserver()" << std::endl;
//      return m_Channel.AddObserver(aObserver);
// }

// NS_IMETHODIMP 
// nsSashChannel::RemoveObserver(nsIStreamAsFileObserver *aObserver)
// {
//      std::cout << "nsSashChannel::RemoveObserver()" << std::endl;
//      return m_Channel.RemoveObserver(aObserver);
// }
