
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu

Intercepts HTTP streams to override default caching functionality

*****************************************************************/



#ifndef NS_SASH_DEBUG_STREAM_LISTENER_H
#define NS_SASH_DEBUG_STREAM_LISTENER_H

#include "nsIStreamListener.h"
#include "nsCOMPtr.h"
#include "nsIChannel.h"

/* Header file */
class nsDebugStreamListener : public nsIStreamListener
{
public:
     NS_DECL_ISUPPORTS
     NS_DECL_NSISTREAMOBSERVER
     NS_DECL_NSISTREAMLISTENER

     nsDebugStreamListener(nsIStreamListener* aISL,
			   nsIChannel*        aChannel,
			   const char* aDebugPrefix);
     virtual ~nsDebugStreamListener();
  
private:
     nsCOMPtr<nsIStreamListener> m_pISL;
     nsCOMPtr<nsIChannel>        m_FileChannel;
     const char*                 m_DebugPrefix;
};

#endif //#ifndef NS_SASH_DEBUG_STREAM_LISTENER_H
