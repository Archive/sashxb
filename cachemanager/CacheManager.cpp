/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar, Stefan Atev

Provides trasparent and controlled caching of web files for Sash

*****************************************************************/

// Sash Cache Manager
// AJ Shankar July 2000

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "debugmsg.h"
#include "Registry.h"
#include "CacheManager.h"
#include "sashINet.h"

using namespace std;

static string ConvertAlphaNumeric(const string &s) {
  string rv= "";
  for (unsigned int i= 0; i< s.length(); ++i)
    switch (s[i]) {
      case ':':
      case '/':
      case '?':
      case '@':
      case '^':
        rv+= "_";
        break;
      default:
        rv+= s[i];
    }
  return rv;
}

CacheManager::CacheManager(const string& CacheDir, bool online = true) : 
	 m_CacheDir(CacheDir),
	 m_Online(online)	 
{
  DEBUGMSG(cachemanager, "Cache: Loading in cache registry...\n");

  // try opening the registry
  m_pRegistry = new Registry();
  if (!m_pRegistry->OpenFile(m_CacheDir + "/" + CacheRegistryFile)) {
	   DEBUGMSG(cachemanager, "Cache: [FAILED]\n");
	   OutputError("Error opening cache registry!");
  }

  DEBUGMSG(cachemanager, "Cache: [SUCCEEDED]\n");
  
  // for generating unique filenames
  srand((unsigned int) time(NULL));
}

CacheManager::~CacheManager() {
  delete m_pRegistry;
}

// adds a file to the cache, but does not retrieve it
bool CacheManager::Add(const string& URL) {

  DEBUGMSG(cachemanager, "Cache: Adding URL %s to cache registry...\n", URL.c_str());

  // first, get a unique local filename
  string localname = GenerateUniqueName()+ ConvertAlphaNumeric(URL);
  
  m_pRegistry->CreateKey(URL);
  
  RegistryValue v, bv;
  v.SetValueType(RVT_STRING);
  bv.SetValueType(RVT_NUMBER);
  v.m_String = localname;
  bv.m_Number= 5.0;
  m_pRegistry->SetValue(URL, "localname", &v);
  m_pRegistry->SetValue(URL, "skip_cache", &bv);
  
  m_pRegistry->WriteToDisk();
  
  DEBUGMSG(cachemanager, "Cache: [SUCCEEDED]\n");
  return true;
}

// deletes a file from the cache and registry
bool CacheManager::Delete(const string& URL) {
  RegistryValue v;
  m_pRegistry->QueryValue(URL, "localname", &v);
  
  //	 if (v->GetValueType() != RVT_STRING)
  //		  OutputError("Cache registry is corrupted!");
  
  DEBUGMSG(cachemanager, "Cache: Deleting URL %s (cache location %s) from cache\n",
	   URL.c_str(), v.m_String.c_str());
  
  RemoveEntry(URL);
  DEBUGMSG(cachemanager, "Cache: Removing %s\n", (m_CacheDir + "/" + v.m_String).c_str());
  remove((m_CacheDir + "/" + v.m_String).c_str());
  
  DEBUGMSG(cachemanager, "Cache: [SUCCEEDED]\n");
  return true;
}

// returns the cached local filename corresponding to the URL
string CacheManager::GetLocalFile(const string& URL) {
  RegistryValue rv;
  string localname;
  m_pRegistry->QueryValue(URL, "localname", &rv);
  if (rv.GetValueType() == RVT_STRING)
    localname = rv.m_String;
  
  return m_CacheDir + "/" + localname;
}

// adds a file to the cache registry and retrieves it
bool CacheManager::Populate(const string& URL) {

  Add(URL);
  
  // now fetch the page and save it in the temporary directory
  Refresh(URL);
  return true;
}

// refreshes the specified URL
void CacheManager::Refresh(const string& URL, const string & updated = "") {
  string fname = GetLocalFile(URL);
  
  DEBUGMSG(cachemanager, "Cache: Refreshing URL %s (cache location %s)\n",
	   URL.c_str(), fname.c_str());
  
  // retrieve URL and store in fname
  nsCOMPtr<sashIDownloader> downloader;
  NewSashFileDownloader(URL.c_str(), fname.c_str(), false, getter_AddRefs(downloader));
  // XXX set downloader If-Modified-Since request header to updated
  downloader->StartTransfer();
  PRInt32 status;
  downloader->ProcessUntilComplete(&status);

  //TODO: note failure if it happened
  
  // note that file has been updated locally
  RegistryValue rv;
  rv.SetValueType(RVT_STRING);
  // TODO get last modified header
  m_pRegistry->SetValue(URL, "updated", &rv);
  
  rv.SetValueType(RVT_NUMBER);
  rv.m_Number = 0;
  m_pRegistry->SetValue(URL, "skip_cache", &rv);
  
  // to avoid corruption, synchronous writes to disk
  m_pRegistry->WriteToDisk();
  
  // keep track of the file's size
  
  RegistryValue rv2;
  rv2.SetValueType(RVT_NUMBER);
  rv2.m_Number = GetFileSize(fname);
  m_pRegistry->SetValue(URL, "size", &rv2);
  
  DEBUGMSG(cachemanager, "Cache: [SUCCEEDED]\n");
}

// refreshes every item in the cache
void CacheManager::UpdateAll() {
	 vector<string> files = GetURLsOfCacheFiles();
	 vector<string>::iterator bi, ei;

	 bi = files.begin();
	 ei = files.end();

	 while (bi != ei) {
		  Refresh(*bi);
		  ++bi;
	 }
}

// returns a vector of all the URLs currently in the cache
vector<string> CacheManager::GetURLsOfCacheFiles() {
	 return m_pRegistry->EnumKeys("\\");
}

// returns the size of the specified file
off_t
CacheManager::GetFileSize(const string & fname)
{
  off_t ret;
  int fd = open(fname.c_str(), O_RDONLY);
  if (fd == -1)
    DEBUGMSG(cachemanager, "Cache: open of %s failed\n", fname.c_str());
  struct stat statbuf;
  if (fstat(fd, &statbuf) == -1) {
    DEBUGMSG(cachemanager, "Cache: fstat failed\n");
    ret = -1;
  } else {
    ret = statbuf.st_size;
  }
  close(fd);
  return ret;
}

// removes entry from the cache registry
void CacheManager::RemoveEntry(const string& URL) {
  DEBUGMSG(cachemanager, "Cache: Deleting URL %s from cache registry...\n",
	   URL.c_str());
  
  m_pRegistry->DeleteValue(URL, "localname");
  m_pRegistry->DeleteValue(URL, "updated");
  m_pRegistry->DeleteValue(URL, "accessed");
  m_pRegistry->DeleteValue(URL, "size");
  m_pRegistry->DeleteValue(URL, "skip_cache");
  
  m_pRegistry->DeleteKey(URL);
  
  // to avoid corruption, synchronous writes to disk
  m_pRegistry->WriteToDisk();
  
  DEBUGMSG(cachemanager, "Cache: [SUCCEEDED]\n");
}


// generates a unique filename
string CacheManager::GenerateUniqueName() {
  char name[64];
  snprintf(name, 64, "%d%d", (int) time(NULL), rand());
  return string(name);
}

void CacheManager::GetFile(const string& URL) {
  RegistryValue rv;
  
  DEBUGMSG(cachemanager, "Cache: Getting URL %s\n", URL.c_str());
	 
  // see whether the file is in the registry already
  m_pRegistry->QueryValue(URL, "updated", &rv);
  if (rv.GetValueType() != RVT_NUMBER) {
    if (m_Online)
      // not in the registry, so retrieve the file
      Populate(URL);
    else {
      DEBUGMSG(cachemanager, "Cache: [FAILED]\n");

      OutputMessage("Attempting to retrieve file while offline!");
      return;
    }

  } else {
    string updated = rv.m_String;
    // if we're offline, only use cached copy
    if (m_Online) {
      Refresh(URL, updated);
    }
  }
  
  // note that file has been updated locally
  rv.SetValueType(RVT_NUMBER);
  rv.m_Number = (float) time(NULL);
  m_pRegistry->SetValue(URL, "accessed", &rv);
  
  // to avoid corruption, synchronous writes to disk
  m_pRegistry->WriteToDisk();
  

  DEBUGMSG(cachemanager, "Cache: [SUCCEEDED]\n");

  // return the file		  
	 
}

CacheItem CacheManager::GetInfo(const std::string& URL) {
  CacheItem ci;
  
  RegistryValue rv;
  m_pRegistry->QueryValue(URL, "localname", &rv);
  if (rv.GetValueType() != RVT_STRING)
    return ci;
  
  ci.localname = rv.m_String;
  
  m_pRegistry->QueryValue(URL, "updated", &rv);
  if (rv.GetValueType() == RVT_STRING)
    ci.updated = rv.m_String;
  
  m_pRegistry->QueryValue(URL, "accessed", &rv);
  if (rv.GetValueType() == RVT_NUMBER)
    ci.accessed = (time_t) rv.m_Number;
  
  m_pRegistry->QueryValue(URL, "size", &rv);
  if (rv.GetValueType() == RVT_NUMBER)
    ci.size = (time_t) rv.m_Number;
  
  m_pRegistry->QueryValue(URL, "skip_cache", &rv);
  if (rv.GetValueType() == RVT_NUMBER)
    ci.skip_cache = ((int) rv.m_Number)!= 0;
  
  ci.URL = URL;
  
  return ci;
}
