
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Provides trasparent and controlled caching of web files for Sash

*****************************************************************/

#ifndef _CACHEMANAGER_H_
#define _CACHEMANAGER_H_

// Sash Cache Manager
// AJ Shankar July 2000


#include "Registry.h"
#include <string>
#include <fcntl.h>

const std::string CacheRegistryFile = "cache.dat";

struct CacheItem {
  CacheItem() : updated(""), accessed(0), size(0), skip_cache(true) {}
  std::string URL;
  std::string updated;
  time_t accessed;
  std::string localname;
  time_t size;
  bool skip_cache;
};

// cache registry looks like
// \ --
//    |- [URL]
//         |-updated (last retrieved from web)
//         |-accessed (last accessed locally)
//         |-localname (name of local file)
//         |-size (size in bytes of file)
//
// updated is stored as the string the HTTP server gave as 
// the last-modified header, so this can be resent in the if-modified-since
// header


class CacheManager {

public:
	 CacheManager(const std::string& CacheDir, bool online);
	 ~CacheManager();

	 // adds a file to the cache, but does not retrieve it
	 bool Add(const std::string& URL);

	 // deletes a file from the cache and the cache registry
	 bool Delete(const std::string& URL);

	 // returns the cached local filename corresponding to the URL
	 std::string GetLocalFile(const std::string& URL);

	 // adds a file to the cache and retrieves it
	 bool Populate(const std::string& URL);

	 // refreshes the specified URL
	 // (i.e. refetches a file and updates timestamp in the registry)
	 void Refresh(const std::string& URL, const string & updated = "");

	 // refreshes every item in the cache
	 void UpdateAll();

	 // returns a vector of all the URLs currently in the cache
	 std::vector<std::string> GetURLsOfCacheFiles();

	 // gets all the information about a given cache item
	 // sash scripts should *not* be allowed to access this!
	 // if item is not in cache registry, URL in struct will be ""
	 CacheItem GetInfo(const std::string& URL);

#ifdef _TESTING
	 void PubGetFile(const std::string& URL) { GetFile(URL); }
#endif

private:
	 // cache directory
	 std::string m_CacheDir;

	 // are we on or off-line?
	 bool m_Online;

	 // cache registry
	 Registry* m_pRegistry;

	 // removes an entry from the cache registry
	 void RemoveEntry(const std::string& URL);

	 // generates a unique filename
	 std::string GenerateUniqueName();

	 // retrieves a file (updated from the web if necessary)
	 void GetFile(const std::string& URL);

	 off_t GetFileSize(const std::string & fname);
};

#endif // _CACHEMANAGER_H_
