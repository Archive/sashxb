
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

A test of the CacheManager system

*****************************************************************/

//
// Cache manager test
//
//

#include <string>
#include <iostream>
#include <stdlib.h>
#include "sash_constants.h"
#include "sash_error.h"
#include "CacheManager.h"

using namespace std;

int main(int argc, char* argv[]) {
  return 0;
#if 0
  CacheManager cache(string(getenv("HOME")) + (string("/.sash/testapp/") + string(CACHEDIR)), true /* assume we are online */);
  cout << "Testing Populate() " << endl;
  cache.Populate("http://www.news.com/");

  cout << endl << "Testing Add() " << endl;
  cache.Add("http://www.duke.edu");

  cout << endl << "Testing GetLocalFile()" << endl;	 
  cout << "bob: ";
  if (cache.GetLocalFile("bob") != "") {
    cout << "[FAILED]" << endl;
    exit(1);
  }

  cout << "http://www.duke.edu" << 
    cache.GetLocalFile("http://www.duke.edu") << endl;

  cout << endl << "Testing UpdateAll()" << endl;
  cache.UpdateAll();

  cout << endl << "Testing GetFile()" << endl;
  //cache.PubGetFile("http://www.news.com/");

  cout << endl << "Testing GetInfo()" << endl;
  CacheItem c = cache.GetInfo("http://www.news.com/");
  if (c.URL == "") {
    cout << "[FAILED]" << endl;
    exit(1);
  }

  cout << "news.com: " << endl;
  cout << "\tURL: " << c.URL << endl;
  cout << "\tlocalname: " << c.localname << endl; 
  cout << "\tupdated: " << c.updated << endl; 
  cout << "\taccessed: " << c.accessed << endl;
  cout << "\tsize: " << c.size << endl;

  cout << endl << "Testing Delete()" << endl;
  cache.Delete("http://www.news.com/");

  cout << "news.com: " << cache.GetLocalFile("http://www.news.com/") << endl;
  if (cache.GetLocalFile("http://www.news.com/") != "") {
    cout << "[FAILED]" << endl;
    exit(1);
  }
  cout << endl;

  cache.Delete("http://www.duke.edu");
  cache.Delete("http://www.news.com");
  exit(0);
#endif
}
