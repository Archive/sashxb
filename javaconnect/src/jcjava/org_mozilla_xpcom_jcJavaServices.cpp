
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "org_mozilla_xpcom_jcJavaServices.h"
#include <jni.h>
#include "nsIComponentManager.h"
#include "prtypes.h"
#include "prmem.h"
#include "nsXPCOM.h"
#include "jcFactory.h"
#include "jcServices.h"
#include "jcUtils.h"
#include "jcConvert.h"
#include "nsIServiceManager.h"
#include "nsIInterfaceInfoManager.h"
#include "nsIInterfaceInfo.h"
#include "nsILocalFile.h"
#include "nsReadableUtils.h"
#include "nsEmbedAPI.h"
#include "jcXPCall.h"
#include "jcError.h"

JNIEXPORT void JNICALL
Java_org_mozilla_xpcom_jcJavaServices_nativeReleaseInterface(JNIEnv *env, jclass cls, jlong addr) {
	if (addr== 0)
		return;
	// NOT SAFE
	reinterpret_cast<nsISupports *>(addr)->Release();
}

#define DO_ID_PARSE_FORM_JSTRING(idstr, idvar, err) { \
	char *CStr; \
	int siz; \
	nsresult res= J2X_ConvertString(env, nsXPTType::T_CHAR_STR, idstr, siz, &CStr); \
	if (NS_FAILED(res)) { \
		JC_ThrowException(env, res); \
		return err; \
	} \
	if (!idvar.Parse(CStr)) { \
		PR_FREEIF(CStr); \
		JC_ThrowException(env, JC_ERROR_CANT_PARSE_ID); \
		return err; \
	} \
	PR_FREEIF(CStr); \
}

JNIEXPORT jlong JNICALL
Java_org_mozilla_xpcom_jcJavaServices_hasInterface(JNIEnv *env, jclass cls, jlong addr, jstring iidstr) {
	nsIID iid;
	nsresult res;
	
	if (addr== 0) // null has no interfaces
		return 0;

	DO_ID_PARSE_FORM_JSTRING(iidstr, iid, 0);

	nsISupports *test= reinterpret_cast<nsISupports *>(addr);
	nsISupports *result;
	res= test->QueryInterface(iid, (void **) &result);
	if (NS_FAILED(res)) {
		// silent failure, we have no interface
		return 0;
	}
	return reinterpret_cast<jlong>(result);
}

JNIEXPORT jboolean JNICALL
Java_org_mozilla_xpcom_jcJavaServices_hasInterfaceInCI(JNIEnv *env, jclass cls, jlong addr, jstring iidstr) {
	nsIID iid;
	nsresult res;
	
	if (addr== 0) // null has no interfaces
		return JNI_FALSE;

	DO_ID_PARSE_FORM_JSTRING(iidstr, iid, JNI_FALSE);

	nsISupports *test= reinterpret_cast<nsISupports *>(addr);
	nsIClassInfo *classInfo;
	res= test->QueryInterface(NS_GET_IID(nsIClassInfo), (void **) &classInfo);
	if (NS_FAILED(res)) // no class info
		return JNI_FALSE;
	nsIID **iids;
	PRUint32 count;
	jboolean hasIt= JNI_FALSE;
	res= classInfo->GetInterfaces(&count, &iids);
	NS_RELEASE(classInfo); // before error check
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return JNI_FALSE;
	}
	// must go through all to delete them, so do not jump
	// out of the loop when you find the iid
	for (PRUint32 i= 0; i< count; ++i) {
		if (iids[i]->Equals(iid))
			hasIt= JNI_TRUE;
		delete iids[i];
	}
	PR_FREEIF(iids);
	return hasIt;
}

JNIEXPORT jboolean JNICALL Java_org_mozilla_xpcom_jcJavaServices_isSameObject(JNIEnv *env, jclass cls, jlong a1, jlong a2) {
	nsresult res;
	jboolean result= JNI_FALSE;
	
	nsISupports *obj1= reinterpret_cast<nsISupports *>(a1);
	nsISupports *obj2= reinterpret_cast<nsISupports *>(a2);
	if (!obj1 || !obj2) // at least one is null
		return (obj1== obj2); // only true if both are null
	nsISupports *obj1ISupports;
	res= obj1->QueryInterface(NS_GET_IID(nsISupports), (void **) &obj1ISupports);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return JNI_FALSE;
	}
	nsISupports *obj2ISupports;
	res= obj2->QueryInterface(NS_GET_IID(nsISupports), (void **) &obj2ISupports);
	if (NS_FAILED(res)) {
		NS_RELEASE(obj1ISupports);
		JC_ThrowException(env, res);
		return JNI_FALSE;
	}
	if (obj1ISupports== obj2ISupports)
		result= JNI_TRUE;
	NS_RELEASE(obj1ISupports);
	NS_RELEASE(obj2ISupports);
	return result;
}

JNIEXPORT jobject JNICALL
Java_org_mozilla_xpcom_jcJavaServices_isAlreadyWrapped(JNIEnv *env, jclass cls, jlong addr, jstring iidstr) {
	nsIID iid;
	nsresult res;

	DO_ID_PARSE_FORM_JSTRING(iidstr, iid, 0);

    nsCOMPtr<jcIServices> jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
    if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
    	return 0;
    }
    jobject result;
    res= jcs->GetNativeObjectWrapper(env, reinterpret_cast<nsISupports *>(addr), iid, &result);
    if (NS_FAILED(res)) {
    	// silent failure, do not throw exception
    	return 0;
    }
    return result;
}

JNIEXPORT jstring JNICALL
Java_org_mozilla_xpcom_jcJavaServices_getInterfaceNameFromIID(JNIEnv *env, jclass cls, jstring iidstr) {
	nsresult res;
	nsIID iid;

	DO_ID_PARSE_FORM_JSTRING(iidstr, iid, 0);

	char *interfaceName= JC_GetInterfaceName(iid);
	if (!interfaceName) {
		JC_ThrowException(env, JC_ERROR_CANT_GET_INTERFACE_NAME);
		return 0;
	}
	jstring result;
	res= X2J_ConvertString(env, nsXPTType::T_CHAR_STR, interfaceName, 0, result);
	PR_FREEIF(interfaceName); // before error check
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	return result;
}


JNIEXPORT jboolean JNICALL
Java_org_mozilla_xpcom_jcJavaServices_initXPCOMnative(JNIEnv *, jclass) {
	nsILocalFile *binDir;
	// TO DO: Remove the hard-coded path!!!!!!!
	NS_NewLocalFile(NS_LITERAL_STRING("/home/atevstef/.sash/mozilla-bin"), PR_FALSE, &binDir);
	nsresult res= NS_InitEmbedding(binDir, 0);
	return NS_SUCCEEDED(res);
}

JNIEXPORT jstring JNICALL
Java_org_mozilla_xpcom_jcJavaServices_isJavaImplemented(JNIEnv *env, jclass cls, jstring cidstr) {
	nsIComponentManager *compMgr;
	nsCID cid;
	nsresult res;

	DO_ID_PARSE_FORM_JSTRING(cidstr, cid, 0);

	res= NS_GetComponentManager(&compMgr);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	nsISupports *test;
	res= compMgr->GetClassObject(cid, NS_GET_IID(nsISupports), (void **) &test);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	// QI for JCFactory on the class object
	// That means we have a genuine Java Class and need not wrap
	// it for java use
	jcIFactory *jcfact;
	res= test->QueryInterface(NS_GET_IID(jcIFactory), (void **) &jcfact);
	NS_RELEASE(test);
	if (NS_FAILED(res)) {
		jstring result;
		// XXX check if null string ptr is OK
		res= X2J_ConvertString(
			env, nsXPTType::T_CHAR_STR,
			NS_STATIC_CAST(void *, ""),
			0, result
		);
		if (NS_FAILED(res)) {
			JC_ThrowException(env, res);
			return 0;
		}
		return result; // empty string
	}
	jcClassData *classData= 0;
	res= jcfact->GetClassData(&classData);
	NS_RELEASE(jcfact);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	jstring result;
	res= X2J_ConvertString(
		env, nsXPTType::T_CHAR_STR,
		NS_STATIC_CAST(void *, classData->getClassName()),
		0, result
	);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	return result;
}

JNIEXPORT jlong JNICALL
Java_org_mozilla_xpcom_jcJavaServices_createInstanceAndReturnAddress(JNIEnv *env, jclass cls, jstring cidstr, jstring iidstr) {
	nsCID cid;
	nsIID iid;
	nsresult res;
	
	DO_ID_PARSE_FORM_JSTRING(iidstr, iid, 0);
	DO_ID_PARSE_FORM_JSTRING(cidstr, cid, 0);

	nsIComponentManager *compMgr;
	res= NS_GetComponentManager(&compMgr);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	nsISupports *test;
	res= compMgr->CreateInstance(cid, 0, iid, (void **) &test);
	NS_RELEASE(compMgr);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	return reinterpret_cast<jlong>(test);
}

JNIEXPORT jlong JNICALL
Java_org_mozilla_xpcom_jcJavaServices_getServiceAndReturnAddress(JNIEnv *env, jclass cls, jstring cidstr, jstring iidstr) {
	nsCID cid;
	nsIID iid;
	nsresult res;
	
	DO_ID_PARSE_FORM_JSTRING(iidstr, iid, 0);
	DO_ID_PARSE_FORM_JSTRING(cidstr, cid, 0);

	nsIServiceManager *servMgr;
	res= NS_GetServiceManager(&servMgr);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	nsISupports *test;
	res= servMgr->GetService(cid, iid, (void **) &test);
	NS_RELEASE(servMgr);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	return reinterpret_cast<jlong>(test);
}

JNIEXPORT jlong JNICALL
Java_org_mozilla_xpcom_jcJavaServices_getSpecialComponentManager(JNIEnv *env, jclass cls) {
	nsIComponentManager *compMgr;
	nsresult res= NS_GetComponentManager(&compMgr);

	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	return reinterpret_cast<jlong>(compMgr);
}	

JNIEXPORT jlong JNICALL
Java_org_mozilla_xpcom_jcJavaServices_getSpecialServiceManager(JNIEnv *env, jclass cls) {
	nsIServiceManager *servMgr;
	nsresult res= NS_GetServiceManager(&servMgr);

	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	return reinterpret_cast<jlong>(servMgr);
}

JNIEXPORT jstring JNICALL
Java_org_mozilla_xpcom_jcJavaServices_toClassID(JNIEnv *env, jclass cls, jstring contractid) {
	nsIComponentManager *compMgr;
	nsIComponentManagerObsolete *obsMgr;
	nsCID cid;
	nsresult res;
	
	res= NS_GetComponentManager(&compMgr);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	res= compMgr->QueryInterface(NS_GET_IID(nsIComponentManagerObsolete), (void **) &obsMgr);
	NS_RELEASE(compMgr); // before error check
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	char *contractIDCStr;
	int siz;
	res= J2X_ConvertString(env, nsXPTType::T_CHAR_STR, contractid, siz, &contractIDCStr);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	res= obsMgr->ContractIDToClassID(contractIDCStr, &cid);
	PR_FREEIF(contractIDCStr); // before error check
	NS_RELEASE(obsMgr); // ditto
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	char *cidCStr;
	cidCStr= cid.ToString();
	jstring result;
	res= X2J_ConvertString(env, nsXPTType::T_CHAR_STR, cidCStr, 0, result);
	PR_FREEIF(cidCStr); // before error check
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	return result;
}

JNIEXPORT jstring JNICALL
Java_org_mozilla_xpcom_jcJavaServices_toContractID(JNIEnv *env, jclass cls, jstring cidstr) {
	nsIComponentManager *compMgr;
	nsIComponentManagerObsolete *obsMgr;
	nsCID cid;
	nsresult res;
	
	DO_ID_PARSE_FORM_JSTRING(cidstr, cid, 0);
	
	res= NS_GetComponentManager(&compMgr);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	res= compMgr->QueryInterface(NS_GET_IID(nsIComponentManagerObsolete), (void **) &obsMgr);
	NS_RELEASE(compMgr); // before error check
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	char *dummyClassName= 0;
	char *contractIDCStr= 0;
	res= obsMgr->CLSIDToContractID(cid, &dummyClassName, &contractIDCStr);
	PR_FREEIF(dummyClassName); // before error check
	NS_RELEASE(obsMgr); // ditto
	if (NS_FAILED(res)) {
		PR_FREEIF(contractIDCStr);
		JC_ThrowException(env, res);
		return 0;
	}
	jstring result;
	res= X2J_ConvertString(env, nsXPTType::T_CHAR_STR, contractIDCStr, 0, result);
	PR_FREEIF(contractIDCStr);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	return result;
}

JNIEXPORT jobjectArray JNICALL
Java_org_mozilla_xpcom_jcJavaServices_getAllInterfacesForObject(JNIEnv *env, jclass cls, jlong addr) {
	nsIClassInfo *objCI;
	nsISupports *obj;
	nsIID **iids;
	nsresult res;
	
	if (addr== 0) // null begets null
		return 0;
	
	obj= reinterpret_cast<nsISupports *>(addr);
	res= obj->QueryInterface(NS_GET_IID(nsIClassInfo), (void **) &objCI);
	if (NS_FAILED(res)) {
		// silent failure, no exception
		return 0;
	}
	PRUint32 cnt;
	res= objCI->GetInterfaces(&cnt, &iids);
	NS_RELEASE(objCI); // before error check
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}

	jclass strcls= env->FindClass("java/lang/String");
	if (!strcls) {
		env->ExceptionClear();
		JC_ThrowException(env, res);
		return 0;
	}
	jobjectArray arr= env->NewObjectArray(cnt, strcls, 0);
	if (!arr) {
		env->ExceptionClear();
		JC_ThrowException(env, res);
		return 0;
	}
	for (PRUint32 i= 0; i< cnt; ++i) {
		char *interfaceName= JC_GetInterfaceName(*(iids[i]));
		if (!interfaceName) {
			env->DeleteLocalRef(arr);
			JC_ThrowException(env, JC_ERROR_CANT_GET_INTERFACE_NAME);
			return 0;
		}
		jstring str;
		res= X2J_ConvertString(env, nsXPTType::T_CHAR_STR, interfaceName, 0, str);
		PR_FREEIF(interfaceName); // before error check
		if (NS_FAILED(res)) {
			env->DeleteLocalRef(arr);
			JC_ThrowException(env, res);
			return 0;
		}
		env->SetObjectArrayElement(arr, i, str);
		env->DeleteLocalRef(str); // before error check
		if (env->ExceptionCheck()) {
			env->ExceptionClear();
			env->DeleteLocalRef(arr);
			JC_ThrowException(env, JC_ERROR_CANT_SET_OBJECT_ARRAY_ELEMENT);
			return 0;
		}
	}
	return arr;
}

JNIEXPORT void JNICALL
Java_org_mozilla_xpcom_jcJavaServices_addInstanceToNativeTable(JNIEnv *env, jclass cls, jlong addr, jobject obj, jstring iidstr) {
	nsresult res;
	nsIID iid;

	DO_ID_PARSE_FORM_JSTRING(iidstr, iid, );

    nsCOMPtr<jcIServices> jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
    if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
    	return;
    }
	jobject gobj;
	gobj= env->NewWeakGlobalRef(obj);
	if (!gobj) {
		JC_ThrowException(env, JC_ERROR_CANT_REFERENCE);
		return;
	}
	// gobj will be dereffed if it was already in
    res= jcs->AddNativeObject(env, reinterpret_cast<nsISupports * >(addr), gobj, iid);
    if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
    	return;
    }
}

JNIEXPORT void JNICALL
Java_org_mozilla_xpcom_jcJavaServices_removeInstanceFromNativeTable(JNIEnv *env, jclass cls, jlong addr, jobject obj, jstring iidstr) {
	nsresult res;
	nsIID iid;

	DO_ID_PARSE_FORM_JSTRING(iidstr, iid, );
    nsCOMPtr<jcIServices> jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
    if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
    	return;
    }
    // object will be dereffed
    jcs->RemoveNativeObject(env, reinterpret_cast<nsISupports * >(addr), iid);
}

JNIEXPORT jobject JNICALL
Java_org_mozilla_xpcom_jcJavaServices_invokeMethod(
	JNIEnv *env,
	jclass cls,
	jlong addr,
	jstring intName,
	jstring metName,
	jobjectArray args) {

	nsresult res;
	
	if (addr== 0) {
		JC_ThrowException(env, "Attempt to invoke method on null pointer");
		return 0;
	}

    nsCOMPtr<jcIServices> jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
    if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
    	return 0;
	}

	char *interfaceName;
	int siz;
	res= J2X_ConvertString(env, nsXPTType::T_CHAR_STR, intName, siz, &interfaceName);
    if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
    	return 0;
	}
		
	char *methodName;
	res= J2X_ConvertString(env, nsXPTType::T_CHAR_STR, metName, siz, &methodName);
	if (NS_FAILED(res)) {
		PR_FREEIF(interfaceName);
		JC_ThrowException(env, res);
		return 0;
	}

	if (PL_strncmp("org.mozilla.xpcom.", interfaceName, 18)== 0) {
		char *tmp= PL_strdup(interfaceName+ 18);
		PR_FREEIF(interfaceName);
		interfaceName= tmp;
	}
	
	PRUint16 methodIndex;
	
	nsIInterfaceInfoManager* mgr= 0;
	mgr= XPTI_GetInterfaceInfoManager();
	if (!mgr) {
		PR_FREEIF(interfaceName);
		PR_FREEIF(methodName);
		JC_ThrowException(env, JC_ERROR_CANT_GET_INTERFACE_MANAGER);
		return 0;
	}
	nsIInterfaceInfo *interfaceInfo;

	res= mgr->GetInfoForName(interfaceName, &interfaceInfo);
	PR_FREEIF(interfaceName); // before error check
	if (NS_FAILED(res)) {
		NS_RELEASE(mgr);
		PR_FREEIF(methodName);
		JC_ThrowException(env, res);
		return 0;
	}

	const nsXPTMethodInfo *methodInfo;
	
	res= interfaceInfo->GetMethodInfoForName(methodName, &methodIndex, &methodInfo);
	if (NS_FAILED(res)) {
		*methodName= *methodName ^ 0x20;
		res= interfaceInfo->GetMethodInfoForName(methodName, &methodIndex, &methodInfo);
		if (NS_SUCCEEDED(res)) {
		}
		else
			*methodName= *methodName ^ 0x20;
	}
	if (NS_FAILED(res)) {
		if (PL_strncmp("get", methodName, 3)== 0) {
			char *tmp= PL_strdup(methodName+ 3);
			PR_FREEIF(methodName);
			methodName= tmp;
			res= interfaceInfo->GetMethodInfoForName(methodName, &methodIndex, &methodInfo);
			if (NS_SUCCEEDED(res)) {
			}
			else {
				if (*methodName>= 'A' && *methodName<= 'Z') {
					*methodName= *methodName- 'A'+ 'a';
					res= interfaceInfo->GetMethodInfoForName(methodName, &methodIndex, &methodInfo);
					if (NS_SUCCEEDED(res)) {
					}
				}
			}
		}
	}
	if (NS_FAILED(res)) {
		if (PL_strncmp("set", methodName, 3)== 0) {
			char *tmp= PL_strdup(methodName+ 3);
			PR_FREEIF(methodName);
			methodName= tmp;
			res= interfaceInfo->GetMethodInfoForName(methodName, &methodIndex, &methodInfo);
			if (NS_SUCCEEDED(res)) {
				++methodIndex;
				// bump methodIndex up
				res= interfaceInfo->GetMethodInfo(methodIndex, &methodInfo);
			}
			else {
				if (*methodName>= 'A' && *methodName<= 'Z') {
					*methodName= *methodName- 'A'+ 'a';
					res= interfaceInfo->GetMethodInfoForName(methodName, &methodIndex, &methodInfo);
					if (NS_SUCCEEDED(res)) {
						++methodIndex;
						// bump methodIndex up
						res= interfaceInfo->GetMethodInfo(methodIndex, &methodInfo);
					}
				}
			}
		}
	}
	if (NS_FAILED(res)) { // it is final, we could not find it
		NS_RELEASE(mgr);
		NS_RELEASE(interfaceInfo);
		PR_FREEIF(methodName);
		JC_ThrowException(env, res);
		return 0;
	}

	jobject result= 0;
	jobjectArray globalizedArgs;
	
	// so that it can be accessed from other threads
	globalizedArgs= (jobjectArray) env->NewGlobalRef(args);
	if (!globalizedArgs) {
		NS_RELEASE(mgr);
		NS_RELEASE(interfaceInfo);
		PR_FREEIF(methodName);
		JC_ThrowException(env, JC_ERROR_CANT_REFERENCE);
		return 0;
	}
	
	nsISupports *obj= reinterpret_cast<nsISupports *>(addr);
	// unfortunately the JNIEnv would be invalidated in another
	// thred, keep that in mind
	jcNativeCallInfo *callInfo= new jcNativeCallInfo(
		methodName,
		interfaceInfo,
		methodInfo,
		methodIndex,
		env,
		globalizedArgs,
		obj,
		&result
	);
	res= jcs->PerformNativeCall(callInfo);
	env->DeleteGlobalRef(globalizedArgs);
	NS_RELEASE(mgr);
	NS_RELEASE(interfaceInfo);
	PR_FREEIF(methodName);
	if (NS_FAILED(res)) {
		JC_ThrowException(env, res);
		return 0;
	}
	return result;
}
