#!/bin/sh
# arg 1 = xpidl
# ardg 2 = SashXB top_srcdir (relative path)
# argh 3 = Mozilla idl dir (absolute path)
# arg 4 = jar
if test ! -d import ; then mkdir import; fi
if test ! -e import/dummy ; then
    touch import/dummy
    echo This may take some time
    echo Importing interface definitions
    cd import
    ln -s -f ../$2/include/*.idl .
    ln -s -f $3/*.idl .
    cp ../fakedidls.idl .
    echo Generating Java interfaces for IDLs
    for ff in *.idl; do ../$1 -m jc -I ../$2/include -I $3 $ff ; done
    rm nsISupports.*
    cd ..
    echo Compiling interfaces
    javac -classpath . -d import import/*.java
    cd import
    $4 cf ../imported.jar org/mozilla/xpcom/*.class
    cd ..
    echo Done, cleaning up
fi
##for ff in import/*.java; do 
##	if test -s $ff; then rm org/mozilla/xpcom/`basename $ff .java`.class ; fi ;
##done
##rm -rf import
