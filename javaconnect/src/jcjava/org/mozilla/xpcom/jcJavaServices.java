
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.mozilla.xpcom;

import java.lang.reflect.*;

public class jcJavaServices {
	private static jcAnnotatingClassLoader jcLoader= null;
	// force class loading
	private static nsISupports dummy;
	
	static {
		System.loadLibrary("jcjavaservices");
		getJCClassLoader();
		if (System.getSecurityManager() instanceof SashSecurityManager)
			SashDynamicPermission.initSecurity();
	}
	
	public static ClassLoader getJCClassLoader() {
		if (jcLoader== null)
			jcLoader= new jcAnnotatingClassLoader(ClassLoader.getSystemClassLoader());
		return jcLoader;
	}

	static class jcException extends Exception {
	}
	
	protected static native long hasInterface(long addr, String iid);
	protected static native boolean hasInterfaceInCI(long addr, String iid);
	protected static native boolean isSameObject(long addr1, long addr2);
	protected static native Object isAlreadyWrapped(long addr, String iidstr);
	protected static native String getInterfaceNameFromIID(String iid);
	protected static native void nativeReleaseInterface(long addr);

	protected static boolean IsXPCOMOn= false;

	// Initialize XPCOM if it hasn't been initialized already. A prerequisite
	// For calling any other jcJavaServices

	public static void notifyXPCOMRunning() {
		IsXPCOMOn= true;
	}

	public static boolean initXPCOM() {
		if (IsXPCOMOn) return true;
		IsXPCOMOn= initXPCOMnative();
		return IsXPCOMOn;
	}

	protected static native boolean initXPCOMnative();
	
	// Check if a CID corresponds to a COM object implemented in Java
	// Return "" on failure, or class name on success
	protected static native String isJavaImplemented(String classID);

	// Return the address of a newly instantiated XPCOM object
	protected static native long createInstanceAndReturnAddress(String classID, String iid);
	
	// Return the address of the service
	protected static native long getServiceAndReturnAddress(String classID, String iid);
	
	// Return the address of the component manager service
	protected static native long getSpecialComponentManager();
	
	// Return the address of the service manager
	protected static native long getSpecialServiceManager();
	
	// Convert a contract id to class id
	protected static native String toClassID(String contractID);
	
	// Convert a class id to contract id
	protected static native String toContractID(String classID);
	
	// Return an array of interface names implemented by an XPCOM object
	protected static native String[] getAllInterfacesForObject(long addressOfISupports);

	// Add the object to the wrapped native table
	protected static native void addInstanceToNativeTable(long addressOfISupports, Object obj, String iidstr);
	
	// Add the object to the wrapped native table
	protected static native void removeInstanceFromNativeTable(long addressOfISupports, Object obj, String iidstr);
	
	// Invoke a method on a XPCOM object
	protected static native Object invokeMethod(long address, String intName, String metName, Object[] args);
	
	public static Object wrapXPCOMInstance(long address, IID instIID) throws ClassNotFoundException {
		// Check if it's not wrapped already first
		Object result= isAlreadyWrapped(address, instIID.getIDStr());
		
		if (result!= null) {
			return result;
		}
		// WARNING !!! DO NOT USE jcLoader here. You don't need to anyway!
		try {
			if (!instIID.equals(nsISupports.IID) && !hasInterfaceInCI(address, instIID.getIDStr())) {
				long newAddr= hasInterface(address, instIID.getIDStr());
				if (newAddr== 0) return null;
				String intName= getInterfaceNameFromIID(instIID.getIDStr());
				
				Class interfaces[]= new Class[2];
				// Nasty race due to class initialization
				interfaces[1]= Class.forName("org.mozilla.xpcom."+ intName);
				interfaces[0]= Class.forName("org.mozilla.xpcom.nsISupports");
				
				Object px;
				if (jcLoader!= null)
					px= Proxy.newProxyInstance(
						jcLoader,
						interfaces,
						new jcWrappedNative(newAddr, false, instIID)
					);
				else
					px= Proxy.newProxyInstance(
						interfaces[0].getClassLoader(),
						interfaces,
						new jcWrappedNative(newAddr, false, instIID)
					);
				addInstanceToNativeTable(newAddr, px, instIID.getIDStr());
				return px;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new ClassNotFoundException("org.mozilla.xpcom.nsISupports");
		}
		String[] allInterfaces= getAllInterfacesForObject(address);
		Class[] interfaces;
		if (allInterfaces== null) // no CI as it is
			interfaces= new Class[1]; // just nsISupports
		else
			interfaces= new Class[allInterfaces.length+ 1];
		interfaces[0]= Class.forName("org.mozilla.xpcom.nsISupports");
		// carefull with using allInterfaces.length - the object may be null !!!!
		for (int i= 1; i< interfaces.length; ++i)
			interfaces[i]= Class.forName("org.mozilla.xpcom."+ allInterfaces[i- 1]);
		if (jcLoader!= null)
			result= Proxy.newProxyInstance(
				jcLoader,
				interfaces,
				new jcWrappedNative(address, (allInterfaces!= null), instIID)
			);
		else
			result= Proxy.newProxyInstance(
				interfaces[0].getClassLoader(),
				interfaces,
				new jcWrappedNative(address, (allInterfaces!= null), instIID)
			);
		addInstanceToNativeTable(address, result, instIID.getIDStr());
		return result;
	}
	
	public static Object createXPCOMInstance(CID classID, IID instIID) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		String className= isJavaImplemented(classID.getIDStr());
		Object result;
		if (className== null || className.equals("")) { // it's native
			long addr= createInstanceAndReturnAddress(classID.getIDStr(), instIID.getIDStr());
			result= wrapXPCOMInstance(addr, instIID);
		}
		else {
			// it is a Java interface, no need to jump through hoops to
			// instantiate it
			className= className.replace('/', '.');
			result= jcLoader.findClass(className).newInstance();
		}
		return result;
	}

	public static Object getXPCOMService(CID classID, IID instIID) throws ClassNotFoundException {
		String className= isJavaImplemented(classID.getIDStr());
		if (className== null || className.equals("")) { // it's native
			long addr= getServiceAndReturnAddress(classID.getIDStr(), instIID.getIDStr());
			return wrapXPCOMInstance(addr, instIID);
		}
		// it is a Java interface, it can't really be a service
		// right now
		return null;
	}

	public static nsIComponentManager getNativeComponentManager() throws ClassNotFoundException {
		long addr= getSpecialComponentManager();
		return (nsIComponentManager) wrapXPCOMInstance(addr, nsIComponentManager.IID);
	}

	public static nsIServiceManager getNativeServiceManager() throws ClassNotFoundException {
		long addr= getSpecialServiceManager();
		return (nsIServiceManager) wrapXPCOMInstance(addr, nsIServiceManager.IID);
	}
	
	public static void decrementRefCountForProxy(Object pxy) {
		long addr= ((jcWrappedNative) Proxy.getInvocationHandler(pxy)).addr;
		nativeReleaseInterface(addr);
	}
}
