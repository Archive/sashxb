
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.mozilla.xpcom;

import java.security.*;

public class SashSecurityManager extends SecurityManager {
	public SashSecurityManager() {
	}
	
	public void checkPermission(Permission perm) {
		try {
			super.checkPermission(perm);
		} catch (SecurityException ex) {
		}
	}
	public void checkPermission(Permission perm, Object context) {
		try {
			super.checkPermission(perm, context);
		} catch (SecurityException ex) {
		}
	}
	public void checkAccpet(String host, int port) {
		try {
			super.checkAccept(host, port);
		} catch (Exception ex) {
		}
	}
	public void checkConnect(String host, int port) {
		try {
			super.checkConnect(host, port, AccessController.getContext());
		} catch (Exception ex) {
		}
	}
	public void checkConnect(String host, int port, Object context) {
		try {
			super.checkConnect(host, port, context);
		} catch (Exception ex) {
		}
	}
	public Object getSecurityContext() {
		ProtectionDomain doms[]= new ProtectionDomain[1];
		doms[0]= jcAnnotatingClassLoader.getProtectionDomain();
		return new AccessControlContext(doms);
	}
}
