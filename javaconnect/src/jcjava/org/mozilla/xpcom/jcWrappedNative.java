
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.mozilla.xpcom;

import java.lang.reflect.*;

class jcWrappedNative implements InvocationHandler {
		
	protected long addr= 0; // null
	protected IID instantiationIID;
	protected boolean hasClassInfo;
		
	public jcWrappedNative(long address, boolean hasCI, IID instIID) {
		addr= address;
		hasClassInfo= hasCI;
		instantiationIID= instIID;
	}
		
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		String mn= method.getName();
		long thisAddr= ((jcWrappedNative) Proxy.getInvocationHandler(proxy)).addr;
		IID thisIID= ((jcWrappedNative) Proxy.getInvocationHandler(proxy)).instantiationIID;
			
		if (mn.equals("toString")) {
			return "[JavaConnect wrapped "+ thisIID.getIDStr()+ "@"+ Long.toHexString(thisAddr)+ "]";
		}
			
		if (mn.equals("equals")) {
			return new Boolean(
				Proxy.isProxyClass(args[0].getClass()) &&
				Proxy.getInvocationHandler(args[0]) instanceof jcWrappedNative &&
				jcJavaServices.isSameObject(thisAddr, ((jcWrappedNative) Proxy.getInvocationHandler(args[0])).addr)
			);
		}
			
		if (mn.equals("hashCode")) {
			return new Integer((int) thisAddr); // this is almost certainly unique
		}
			
		if (mn.equals("finalize")) {
			System.out.println("FINALIZE called form invocation handler invokeMethod");
			jcJavaServices.removeInstanceFromNativeTable(thisAddr, proxy, thisIID.getIDStr());
		}
			
		if (mn.equals("queryInterface")) {
			if (thisIID.equals(args[0])) {
				return proxy;
			}
			if (hasClassInfo &&
				jcJavaServices.hasInterfaceInCI(thisAddr, ((IID) args[0]).getIDStr())) {
				return proxy;
			}

			long newAddr= jcJavaServices.hasInterface(thisAddr, ((IID) args[0]).getIDStr());
				
			// we can just treat the interface as a new object
			if (newAddr!= 0) {
				Object result= jcJavaServices.wrapXPCOMInstance(newAddr, (IID) args[0]);
				// wrapXPCOM will add one Ref to result, but hasInterface already
				// reffed the object. This removes the duplicate
				jcJavaServices.decrementRefCountForProxy(result);
				return result;
			}
			return null;
		}
			
		Object result= jcJavaServices.invokeMethod(
			thisAddr,
			method.getDeclaringClass().getName(),
			method.getName(),
			args
		);
		return result;
	}
		
	public void finalize() {
		System.out.println("FINALIZING");
//		jcJavaServices.removeInstanceFromNativeTable(addr, this, "");
	}

}
