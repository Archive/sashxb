
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.mozilla.xpcom;

import java.util.Enumeration;

public class jcComponentManager {
	
	/*
		Minimal component management functionality
	*/
	static class WrappedXPCOMEnumeration implements Enumeration {
		
//		private nsISimpleEnumerator simpEnum;
		private nsISimpleEnumerator simpEnum;
		
		// has to use Obsolete component manager
		// Wrap around nsISimpleEnumerator
		public WrappedXPCOMEnumeration(nsISimpleEnumerator in) {
			simpEnum= in;
		}
		
		synchronized public boolean hasMoreElements() {
			if (simpEnum!= null)
				return simpEnum.hasMoreElements();
			return false;
		}

		synchronized public Object nextElement() {
			if (simpEnum!= null) {
				nsISupportsString el= (nsISupportsString)
					simpEnum.getNext().queryInterface(nsISupportsString.IID);
				return el.getData();
			}
			return null;
		}
	}

	synchronized static public Enumeration components() {
		nsISimpleEnumerator toWrap= null;
		try {
			nsIComponentManagerObsolete obsMan= (nsIComponentManagerObsolete)
				jcJavaServices.getNativeComponentManager().queryInterface(nsIComponentManagerObsolete.IID);
			if (obsMan!= null)
				toWrap= (nsISimpleEnumerator) obsMan.enumerateContractIDs();
		} catch (Exception ex) {
		}
		return new WrappedXPCOMEnumeration(toWrap);
	}
	
	synchronized static public CID getClassID(String contractID) {
		String cidStr= jcJavaServices.toClassID(contractID);
		if (cidStr== null || cidStr.length()== 0)
			return null;
		return new CID(cidStr);
	}
	
	// This metod is more convenient for classes that are known to
	// implement nsIClassInfo - all declared interfaces will be
	// automatically exposed. For methods without ClassInfo, you
	// just get nsISupports exposed - but you can still QI
	synchronized static public nsISupports createInstance(String contractID) {
		String cidStr= jcJavaServices.toClassID(contractID);
		
		if (cidStr== null || cidStr.length()== 0)
			return null;
		CID serviceCID= new CID(cidStr);
		Object res;
		try {
			res= jcJavaServices.createXPCOMInstance(serviceCID, nsISupports.IID);
		} catch (Exception ex) {
			res= null;
		}
		if (res!= null)
			return (nsISupports) res;
		return null;
	}
	
	synchronized static public nsISupports createInstance(String contractID, IID requestedIID) {
		String cidStr= jcJavaServices.toClassID(contractID);
		if (cidStr== null || cidStr.length()== 0)
			return null;
		CID serviceCID= new CID(cidStr);
		Object res;
		try {
			res= jcJavaServices.createXPCOMInstance(serviceCID, requestedIID);
		} catch (Exception ex) {
			res= null;
		}
		if (res!= null)
			return (nsISupports) res;
		return null;
	}

	// with given iface
	synchronized static public nsISupports getService(String contractID, IID serviceIID) {
		String cidStr= jcJavaServices.toClassID(contractID);
		if (cidStr== null || cidStr.length()== 0)
			return null;
		CID serviceCID= new CID(cidStr);
		Object res;
		try {
			res= jcJavaServices.getXPCOMService(serviceCID, serviceIID);
		} catch (Exception ex) {
			res= null;
		}
		if (res!= null)
			return (nsISupports) res;
		return null;
	}
}
