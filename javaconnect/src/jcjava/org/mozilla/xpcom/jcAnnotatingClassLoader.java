
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.mozilla.xpcom;

import java.security.*;
import java.util.*;
import java.util.jar.*;
import java.util.zip.*;
import java.io.*;
import java.net.URL;

/*
THIS CLASS IS USED BY JAVA_CONNECT TO LOAD CLASSES IN ITS OWN DOMAIN
List your JAR files in javaconnect.jar.path (':' separated list)
Note that if the files are in the CLASSPATH they will be loaded into the system
domain
*/

public class jcAnnotatingClassLoader extends ClassLoader {
	private static ProtectionDomain jcPD= null;
	private static PermissionCollection perms= null;
	private static Hashtable loadedClasses= new Hashtable();
	static {
		try {
			SashDynamicPermission sashperm= new SashDynamicPermission("Luna");
			perms= sashperm.newPermissionCollection();
			perms.add(sashperm);
			perms.setReadOnly();
			jcPD= new ProtectionDomain(new CodeSource(new URL("http://www.sashxb.org"), new java.security.cert.Certificate[0]), perms);
		} catch (Exception ex) {
			ex.printStackTrace();
			// ignore for now
		}
	}
	public static ProtectionDomain getProtectionDomain() {
		return jcPD;
	}
	public jcAnnotatingClassLoader(ClassLoader dad) {
		super (dad);
	}
	public Class findClass(String name) throws ClassNotFoundException {
		Class result;
		result= (Class) loadedClasses.get(name.replace('/', '.'));
		if (result!= null)
			return result;
		try {
			result= Class.forName(name.replace('/', '.'));
			if (result!= null) {
				return result;
			}
		} catch (Exception ex) {
		}
		// using the system class loader
		String jcExtraJars= System.getProperty("javaconnect.jar.path");
		while (jcExtraJars.length()> 0) {
			int col= jcExtraJars.indexOf(':');
			String jarname;
			if (col== 0) {
				jcExtraJars= jcExtraJars.substring(1);
				continue;
			}
			if (col< 0) {
				jarname= jcExtraJars;
				jcExtraJars= "";
			}
			else { // col >= 1 here
				jarname= jcExtraJars.substring(0, col);
				jcExtraJars= jcExtraJars.substring(col+ 1);
			}
			if (jarname.length()== 0)
				continue;
			byte[] b= tryLoadFromJar(jarname, name.replace('.', '/')+ ".class");
			if (b!= null) {
				// Check if sashSecurityManager is actually running
				if (System.getSecurityManager().getClass().getName().equals("org.mozilla.xpcom.SashSecurityManager"))
					result= defineClass(name.replace('/', '.'), b, 0, b.length, jcPD);
				else // treat as system trusted code, subject to normal policy
					result= defineClass(name.replace('/', '.'), b, 0, b.length, null);
				Class ints[]= result.getInterfaces();
				loadedClasses.put(name.replace('/', '.'), result);
				return result;
			}
		}
		throw new ClassNotFoundException(name);
	}
	
	private byte[] tryLoadFromJar(String jarname, String name) {
		byte b[]= null;
		try {
			JarFile f= new JarFile(jarname);
			ZipEntry cf= f.getEntry(name);
			if (cf== null)
				return null;
			InputStream is= f.getInputStream(cf);
			int bytes= (int) cf.getSize();
			b= new byte[bytes];
			int read= 0;
			while (read< bytes) {
				int nread= is.read(b, read, bytes- read);
				read+= nread;
			}
			is.close();
		} catch (Exception ex) {
			b= null;
		}
		return b;
	}
}
