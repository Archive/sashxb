
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
package org.mozilla.xpcom;

import java.security.*;
import java.util.*;

public class SashDynamicPermission extends Permission {
	
	private static sashISecurityManager secMan= null;
	
	public static void initSecurity() {
		try {
			secMan= (sashISecurityManager)
				jcComponentManager.getService("@gnome.org/SashXB/SecurityManager;1", sashISecurityManager.IID);
		} catch (Exception ex) {
		}
	}
	
	class SashDynamicPermissionCollection extends PermissionCollection {
		private Hashtable permissions;
		private boolean isRO;
		public SashDynamicPermissionCollection() {
			permissions= new Hashtable(4); // only expect one permission
			isRO= false;
		}
		public Enumeration elements() {
			return permissions.elements();
		}
		// ALL permissions must imply perm for success
		// THIS is NOT how other permissions work!
		// Anyhow, we only expect to have one SashDynamicPermission here
		public boolean implies(Permission perm) {
			boolean result= true;
			Enumeration perms= elements();
			// empty == IMPLIES it!
			while (perms.hasMoreElements() && result)
				result= result && ((Permission) perms.nextElement()).implies(perm);
			return result;
		}
		public boolean isReadOnly() {
			return isRO;
		}
		public void setReadOnly() {
			isRO= true;
		}
		public void add(Permission perm) {
			if (!isRO)
				permissions.put(perm, perm);
		}
	}

	public SashDynamicPermission(String name) {
		super(name);
	}
	
	public boolean equals(Object obj) {
		return (obj instanceof SashDynamicPermission) &&
			getName().equals(((SashDynamicPermission) obj).getName()) &&
			getActions().equals(((SashDynamicPermission) obj).getActions());
	}
	public String getActions() {
		return "";
	}
	public int hashCode() {
		return (getName()+ getActions()).hashCode();
	}
	synchronized public boolean implies(Permission permission) {
		if (secMan== null)
			return true; // same here
		try {
//			if (permission instanceof java.io.FilePermission) {
//				System.err.println("check SashXB permission for filesystem access.");
//				// 8 == FILE SYSTEM
//				System.err.println("Permission is: "+ secMan.queryNumOrEnum(8));
//			}
			if (permission instanceof java.net.SocketPermission) {
				// 6 == NET
				boolean netOK= secMan.queryBool(6);
				if (!netOK) {
					secMan.securityViolationBool("Global", 6, true);
				}
			}
			int javaPow= (int) secMan.queryNumOrEnum(10);
			if (javaPow< 2) {
				secMan.securityViolationNumOrEnum("Global", 10, 2);
			}
		} catch (Exception ex) {
		}
/*
 GSS_REGISTRY_ACCESS = 0,
  GSS_CHANNEL_USE,
  GSS_PROCESS_SPAWNING,
  GSS_PRINTING,
  GSS_CLIPBOARD,
  GSS_REGISTER,
  GSS_DOWNLOAD,
  GSS_ACTION_SPAWNING,
  GSS_FILESYSTEM_ACCESS,
  GSS_RESTART_BROWSER,
  GSS_JAVA,
  GSS_RPC,
  GSS_NUM_VALUES,		
*/
		// 10 == Java power
		return true;
	}
	public PermissionCollection newPermissionCollection() {
		return new SashDynamicPermissionCollection();
	}
}
