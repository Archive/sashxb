import org.mozilla.xpcom.*;

public class TestXPCOM2 {

	public static void main(String args[]) {
		try {
			System.out.println("Initializing XPCOM");
			jcJavaServices.initXPCOM();
			// this is the native variant
			jcIMarshallerTest tt= (jcIMarshallerTest)
				jcJavaServices.createXPCOMInstance(
					new CID("{319ea598-1e91-42d8-bd1f-6e3db5aa1779}"),
					jcIMarshallerTest.IID
				);
			System.out.println("tt= "+ tt);
			// this is the java-implemented variant
			jcIMarshallerTest jj= (jcIMarshallerTest)
				jcJavaServices.createXPCOMInstance(
					new CID("{581f7341-e777-4f0a-861b-194d68afc7c4}"),
					jcIMarshallerTest.IID
				);
			System.out.println("jj= "+ jj);
			System.out.println(
				"Starting a comprehensive component test\n\n"+
				"First test is a C++ XPCOM object calling methods on a\n"+
				"Java implemented XPCOM object that was passed to it\n"+
				"as a prameter from Java.\n\n"
			);
			tt.testBuddy(jj);
			System.out.println(
				"\n\nSecond test is a Java XPCOM object calling methods on a\n"+
				"C++ implemented XPCOM object that was passed to it\n"+
				"as a prameter from Java.\n\n"
			);
			jj.testBuddy(tt);
/*
			System.out.println(
				"\n\nThird test is a C++ XPCOM object calling methods on a\n"+
				"C++ implemented XPCOM object that was passed to it\n"+
				"as a prameter from Java.\n\n"
			);
			tt.testBuddy(tt);
			System.out.println(
				"\n\nFourth test is a Java XPCOM object calling methods on a\n"+
				"Java implemented XPCOM object that was passed to it\n"+
				"as a prameter from Java.\n\n"
			);
			jj.testBuddy(jj);
*/
		} catch (Exception ex) {
			System.out.println("Got exception: "+ ex+ ex.getMessage());
		}
	}
}
