import org.mozilla.xpcom.*;

public class TestXPCOM1 {

	public static void main(String args[]) {
		try {
			System.out.println("Initializing XPCOM");
			jcJavaServices.initXPCOM();
			System.out.println("Creating a local file");
			nsILocalFile lf= (nsILocalFile)
				jcJavaServices.createXPCOMInstance(
					new CID("{2e23e220-60be-11d3-8c4a-000064657374}"),
					nsILocalFile.IID
				);
			System.out.println("lf= "+ lf);
			System.out.println("About to call a method on local file object");
			lf.initWithPath("/home/atevstef/test.txt");
			System.out.println("Unbelievable");
			lf.setFileSize(193);
			System.out.println("This would be magic: "+ lf.getFileSize());
			System.out.println("disk space: "+ lf.getDiskSpaceAvailable());
			System.out.println("target: "+ lf.getTarget());
			System.out.println("path: "+ lf.getPath());
			System.out.println("exists: "+ lf.exists());
			lf= null;
		} catch (Exception ex) {
			System.out.println("Got exception: "+ ex);
		}
	}
}
