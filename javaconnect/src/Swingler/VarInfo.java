/**
 * @author atevstef
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class VarInfo {

	private Class typeClass;
	private String origName;
	private String idlName;
	private int arrayDims;
	public boolean isPrim;
	public boolean isArr;
	
	public String wrapperIDL(boolean isRetval, String paramName) {
		if (arrayDims== 1 && !isRetval && origName.equals("java.lang.String")) {
			// Needs special care since arrays of Mozilla strings are not supported
			// substitute with wstring
			return "[array, size_is("+ paramName+ "_length)] in "+
				"wstring"+ " "+ paramName+ ", in unsigned long "+ paramName+ "_length";
		}
		if (arrayDims== 1 && !isRetval) {
			return "[array, size_is("+ paramName+ "_length)] in "+
				idlName+ " "+ paramName+ ", in unsigned long "+ paramName+ "_length";
		}
		if (arrayDims> 0)
			if (isRetval)
				return "nsIVariant";
			else
				return "in nsIVariant "+ paramName;
		if (isRetval) return idlName;
		return "in "+ idlName+ " "+ paramName;
	}
	
	public String wrapperImpl(boolean isTyped, boolean isCast, String paramName) {
		String result= "";
		String type= "";
		if (paramName!= null) result= paramName;
		if (isTyped && paramName!= null) result= " "+ result;
		if (!isPrim && isArr && isTyped) return "Object"+ result;
		if (!isPrim && isCast)
			return "(("+ idlName+ "Impl) "+ result+ ")";
		if (isTyped) {
			if (isPrim)
				type= origName;
			else
				type= idlName;
			for (int i= 0; i< arrayDims; ++i)
				type+= "[]";
		}
		
		return type+ result;
	}
	
	public boolean equals(Object o) {
		return o!= null && o instanceof VarInfo && typeClass.equals(((VarInfo) o).typeClass);
	}
	
	public String getPackage() {
		return typeClass.getPackage().getName();
	}
	
	VarInfo(Class c) {
		idlName= TypeNameManager.getTypeName(c);
		isArr= false;
		if (c.isArray()) {
			isArr= true;
			isPrim= TypeNameManager.isPrimitive(c.getComponentType());
			typeClass= c.getComponentType();
		}
		else {
			isPrim= TypeNameManager.isPrimitive(c);
			typeClass= c;
		}
		arrayDims= 0;
		while (c.isArray()) {
			++arrayDims;
			c= c.getSuperclass();
		}
		origName= typeClass.getName();
	}

}
