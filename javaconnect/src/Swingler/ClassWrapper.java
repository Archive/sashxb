/**
 * @author atevstef
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */

import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import java.io.*;
import org.eclipse.swt.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.*;

public class ClassWrapper {
	private Class mainClass;
	private String mainClassName;
	private Hashtable methods;
	private Hashtable exceptions;
	private Hashtable includedTypes;
	private Hashtable packages;
	public static final int x= 10;
	
	ClassWrapper(Class c) {
		mainClass= c;
		mainClassName= TypeNameManager.convertClassName(c.getName());
		methods= new Hashtable();
		exceptions= new Hashtable();
		includedTypes= new Hashtable();
		packages= new Hashtable();
		String rt= c.getPackage().getName();
		packages.put(rt, rt);
		fillMethods(c);
	}
		
	private void fillMethods(Class c) {
		int modifiers= c.getModifiers();
		if (!Modifier.isPublic(modifiers)) {
			System.err.println("Cannot wrap class "+ c.getName());
			return;
		}
		
//		Method methodsArr[]= c.getDeclaredMethods();
		Method methodsArr[]= c.getMethods();
		for (int i= 0; i< methodsArr.length; ++i) {
			modifiers= methodsArr[i].getModifiers();
			if (!Modifier.isPublic(modifiers)) continue;
			MethodInfo mi= new MethodInfo(methodsArr[i]);
			MethodInfo conf= (MethodInfo) methods.get(mi.origName);
			if (conf!= null) {
				/* overload conflict */
				mi.next= conf;
				mi.overConflicts= conf.overConflicts+ 1;
				methods.remove(mi.origName);
			}
			methods.put(mi.origName, mi);
		}
	}
	
	private void clearProblemMethods() {
		Enumeration m= methods.elements();
		while (m.hasMoreElements()) {
			MethodInfo prev= null;
			MethodInfo mi= ((MethodInfo) m.nextElement());
			while (mi!= null) {
				String rt= mi.wrapperIDL();
				if (mi.isOverloadOfSuper(mainClass)) {
//					System.err.println("Method implemented in superclass: "+ mi.origName);
				}
				if (rt== null) {
					System.err.println("Cannot wrap method "+ mi.origName+ mi.overConflicts);
					if (prev== null) {
						methods.remove(mi.origName);
						mi= mi.next;
						if (mi!= null) methods.put(mi.origName, mi);
					}
					else {
						mi= mi.next;
						prev.next= mi;
					}
				}
				else {
					prev= mi;
					mi= mi.next;
				}
			}
		}
	}
	
	private void resolveOverloadConflicts() {
		Enumeration m= methods.elements();
		Hashtable newMeth= new Hashtable();
		Vector protoNames= new Vector();
		Vector overNames= new Vector();
		Vector include= new Vector();
		while (m.hasMoreElements()) {
			MethodInfo mi= ((MethodInfo) m.nextElement());
			if (mi.next== null) {
				String overNameCached= PropertyCache.getOver(mainClassName, mi.wrapperIDL());
				if (overNameCached== null)
					overNameCached= mi.origName;
				boolean includeCached= PropertyCache.getInclude(mainClassName, mi.wrapperIDL());
				protoNames.add(mi.wrapperIDL());
				overNames.add(overNameCached);
				include.add(new Boolean(includeCached));
				continue;
			}
			MethodInfo trav= mi;
			int i= 1;
			while (trav!= null) {
				String rt= trav.wrapperIDL();
				String overNameCached= PropertyCache.getOver(mainClassName, rt);
				if (overNameCached== null)
					overNameCached= trav.origName+ i;
				boolean includeCached= PropertyCache.getInclude(mainClassName, rt);
				protoNames.add(rt);
				overNames.add(overNameCached);
				include.add(new Boolean(includeCached));
				trav= trav.next;
				++i;
			}
		}
		OverloadingUI ui= new OverloadingUI(protoNames, overNames, include, mainClassName);
//		ui.show();
		int cnt= 0;
		m= methods.elements();
		while (m.hasMoreElements()) {
			MethodInfo mi= ((MethodInfo) m.nextElement());
			MethodInfo trav= mi;
			while (trav!= null) {
				MethodInfo next= trav.next;
				String rt= trav.wrapperIDL();
				if (((Boolean) include.elementAt(cnt)).booleanValue()) {
					trav.overName= (String) overNames.elementAt(cnt);
					trav.next= null;
					newMeth.put(trav.overName, trav);
					PropertyCache.setInclude(mainClassName, rt, true);
				}
				else
					PropertyCache.setInclude(mainClassName, rt, false);
				PropertyCache.setOver(mainClassName, rt, (String) overNames.elementAt(cnt));
				trav= next;
				++cnt;	
			}
		}
		methods= newMeth;
		m= methods.elements();
		while (m.hasMoreElements()) {
			MethodInfo mi= ((MethodInfo) m.nextElement());
			mi.addTypes(includedTypes);
			mi.addPackages(packages);
		}
	}
	
	private String constAttributes() {
		String result= "";
		Field[] fields= mainClass.getDeclaredFields();
		for (int i= 0; i< fields.length; ++i) {
			int modifiers= fields[i].getModifiers();
			if (Modifier.isPublic(modifiers) &&
				Modifier.isFinal(modifiers) &&
				Modifier.isStatic(modifiers) &&
				fields[i].getType().isPrimitive()) {
				try {		
					String ctype= fields[i].getType().getName();
					if (ctype.equals("short"))
						result+= "\tconst short "+ fields[i].getName()+
						"= "+ fields[i].getShort(mainClass)+ ";\n";
					if (ctype.equals("char"))
						result+= "\tconst short "+ fields[i].getName()+
						"= "+ (short) fields[i].getChar(mainClass)+
						"; /* converted char (unicode) */\n";
					if (ctype.equals("byte"))
						result+= "\tconst short "+ fields[i].getName()+
						"= "+ (short) fields[i].getByte(mainClass)+
						"; /* converted byte */\n";
					if (ctype.equals("int"))
						result+= "\tconst long "+ fields[i].getName()+ "= "+ fields[i].getInt(mainClass)+ ";\n";
				} catch (Exception ex) {
					/* We don't really care; Exception is bad news */
				}
			}
		}
		return result;
	}
	
	private String detectAttributes() {
		return "";
	}
	
	public String wrapperIDL() {
		clearProblemMethods();
		resolveOverloadConflicts();

		String result=
			"/* DO NOT EDIT THIS FILE - IT WAS AUTOGENERATED BY Swingler\n"+
			"   FROM the Java class "+ mainClass.getName()+ "\n"+
			"   The corresponding implementation is in "+ mainClassName+ "Impl.java\n"+
			"*/\n\n";
			
		/* just in case we picked ourselves up on the road */
		includedTypes.remove(mainClassName);
		result+= "#include \"nsISupports.idl\"\n";
		result+= "#include \"nsIVariant.idl\"\n";

		Class interfaces[]= mainClass.getInterfaces();
		for (int i= 0; i< interfaces.length; ++i) {
			String rt= TypeNameManager.getTypeName(interfaces[i]);
			result+= "#include \""+ rt+ ".idl\"\n";
		}
		Class sup= mainClass.getSuperclass();
		String suprt= TypeNameManager.getTypeName(sup);
		if (suprt!= null && !suprt.equals("nsIVariant"))
			result+= "#include \""+ suprt+ ".idl\"\n";

		Enumeration ii= includedTypes.elements();
		while (ii.hasMoreElements()) 
			result+= "interface "+ (String) ii.nextElement()+ ";\n";
		result+= "\n";	
		String uuid= PropertyCache.getUUID(mainClassName);
		if (uuid== null) {
			uuid= TypeNameManager.generateUUID();
			PropertyCache.setUUID(mainClassName, uuid);
		}
		result+= "[scriptable, uuid("+ uuid+ ")]\ninterface ";
		result+= TypeNameManager.getTypeName(mainClass);
		if (suprt== null || suprt.equals("nsIVariant"))
			result+= ": nsISupports";
		else
			result+= ": "+ suprt;

		result+= " {\n";

		Enumeration m= methods.elements();
		while (m.hasMoreElements()) {
			MethodInfo mi= ((MethodInfo) m.nextElement());
			mi.addExceptions(exceptions);
		}

		m= exceptions.elements();
		while (m.hasMoreElements()) {
			String rt= ((String) m.nextElement());
			result+= "\texception "+ rt+ " {};\n";
		}

		result+= constAttributes();
		result+= detectAttributes();

		m= methods.elements();
		while (m.hasMoreElements()) {
			MethodInfo mi= ((MethodInfo) m.nextElement());
			String rt= mi.wrapperIDL();
			if (mi.isOverloadOfSuper(mainClass)) {
//				System.err.println("No need for method "+ mi.overName+ " in interface "+ mainClassName);
				rt= null;
			}
			if (rt!= null)
				result+= "\t"+ rt+ mi.wrapperIDLExceptions()+ ";\n";
//			else
//				System.err.println("No idl equivalent for method "+ mi.overName);
		}
		result+= "};\n";
		return result;
	}

	public String wrapperImpl() {
		String result=
			"/* DO NOT EDIT THIS FILE - IT WAS AUTOGENERATED BY Swingler\n"+
			"   FROM the Java class "+ mainClass.getName()+ "\n"+
			"   The corresponding IDL is in "+ mainClassName+ ".idl\n"+
			"*/\n\n";
			
		result+= "package org.mozilla.xpcom;\n\n";
		
		Enumeration ii= packages.elements();
		while (ii.hasMoreElements()) {
			String rt= (String) ii.nextElement();
			result+= "import "+ rt+ ".*;\n";
		}
		result+= "\n";
			
		result+= "public class "+ mainClassName+ "Impl";
		result+= " implements "+ mainClassName;
		Class interfaces[]= mainClass.getInterfaces();
		for (int i= 0; i< interfaces.length; ++i)
			result+= ", "+ TypeNameManager.getTypeName(interfaces[i]);
		result+= " {\n";
		result+= "\n\t/* Underlying implementation */\n";
		result+= "\tpublic "+ mainClass.getName()+ " __holder;\n\n";
		result+= "\t"+ mainClassName+ "Impl() {\n"+
			"\t\t__holder= null;\n"+
			"\t}\n\n";
		result+= "\t"+ mainClassName+ "Impl("+ mainClass.getName()+ " __in) {\n"+
			"\t\t__holder= __in;\n"+
			"\t}\n\n";
		result+= "\tpublic Object queryInterface(IID ignored) {\n";
		result+= "\t\t/* this would never be called */\n";
		result+= "\t\treturn this;\n";
		result+= "\t}\n\n";
		Enumeration m= methods.elements();
		while (m.hasMoreElements()) {
			MethodInfo mi= ((MethodInfo) m.nextElement());
			result+= mi.wrapperImpl();
		}
		result+= "}\n";
		return result;
	}
	
	public void workYourMagic() {
		try {
			System.err.println("\nWrapping "+ mainClassName+ "\n");
			BufferedWriter bf= new BufferedWriter(new FileWriter(mainClassName+ ".idl"));
			bf.write(wrapperIDL());
			bf.close();
			bf= new BufferedWriter(new FileWriter(mainClassName+ "Impl.java"));
			bf.write(wrapperImpl());
			bf.close();
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			System.err.println(ex.getClass().getName()+ ":"+ ex.getMessage());
		}
	}
	
	public static boolean recursive= false;
	
	public static void main(String args[]) {
		if (args.length< 1) {
			System.err.println("Usage: ClassWrapper [-r] classname(s)");
			return;
		}
		if (args[0].equals("-r") || args[0].equals("-R"))
			recursive= true;
		int i= 0;
		if (recursive)
			++i; // skip the -r
		if (i>= args.length) { // only -r provided
			System.err.println("Usage: ClassWrapper [-r] classname(s)");
			return;
		}
		PropertyCache.load();
		for (;i< args.length; ++i) {
			try {
				Class cls= Class.forName(args[i]);
				ClassWrapper cw= new ClassWrapper(cls);
				cw.workYourMagic();
			} catch (Exception ex) {
				System.err.println("Error processing class "+ args[i]);
				ex.printStackTrace(System.err);
			}
		}
		PropertyCache.save();
	}					
}
