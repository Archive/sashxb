/**
 * @author atevstef
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */

import java.util.*;
import org.doomdark.uuid.*;

public class TypeNameManager {
	
	private static Hashtable typeMap;
	private static Hashtable primitivesMap;
	private static UUIDGenerator uuidgen;
	
	public static String convertClassName(String name) {
		String result= "";
		boolean capitalize= false;
		for (int i= name.length()- 1; i>=0; --i) {
			if (name.charAt(i)!= '.') {
				result= name.charAt(i)+ result;
				continue;
			}
			if (!capitalize) {
				result= "I"+ result;
				capitalize= true;
			}
			else {
				result= result.substring(0, 1).toUpperCase()+
					result.substring(1);
			}
		}
		if (!capitalize)
			return "jcI"+ result;
		return result;
	}
	
	public static String javaToIDLException(String s) {
		return "jc_"+ s.replace('.', '_');
	}
	
	public static String getTypeName(Class c) {
		if (c== null) {
			return null;
		}
		String name= c.getName();
		String mappedName= (String) primitivesMap.get(name);
		if (mappedName!= null) {
			return mappedName;
		}
		mappedName= (String) typeMap.get(name);
		if (mappedName!= null) {
			return mappedName;
		}
		if (c.isArray()) {
			mappedName= getTypeName(c.getComponentType());
			typeMap.put(name, mappedName);
		}			
		else {
			mappedName= convertClassName(name);
			typeMap.put(name, mappedName);
			if (ClassWrapper.recursive) {
				ClassWrapper cw= new ClassWrapper(c);
				cw.workYourMagic();
			}
		}
		return mappedName;
	}

	public static boolean isPrimitive(Class c) {
		String name= c.getName();
		String mappedName= (String) primitivesMap.get(name);
		return mappedName!= null;
	}
	
	private static void fillType(Class c, String s) {
		primitivesMap.put(c.getName(), s);
	}
	
	public static String generateUUID() {
		return uuidgen.generateRandomBasedUUID().toString();
	}

	static {
		typeMap= new Hashtable();
		primitivesMap= new Hashtable();
		fillType(Object.class, "nsIVariant");
		fillType(void.class, "void");
		fillType(byte.class, "octet");
		fillType(short.class, "short");
		fillType(char.class, "wchar");
		fillType(boolean.class, "boolean");
		fillType(int.class, "long");
		fillType(long.class, "long long");
		fillType(float.class, "float");
		fillType(double.class, "double");
		fillType(String.class, "AString");
		fillType(Byte.class, "nsIVariant");
		fillType(Short.class, "nsIVariant");
		fillType(Character.class, "nsIVariant");
		fillType(Boolean.class, "nsIVariant");
		fillType(Integer.class, "nsIVariant");
		fillType(Long.class, "nsIVariant");
		fillType(Float.class, "nsIVariant");
		fillType(Double.class, "nsIVariant");
		uuidgen= UUIDGenerator.getInstance();
	}

}
