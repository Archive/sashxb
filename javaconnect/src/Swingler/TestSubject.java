/**
 * @author atevstef
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class TestSubject {

	public static final int x= 10;
	public static final char z= 'S';
	public static final double t= 3.5;

	public void test(int x) throws Exception {
	}
	
	public void test(long x) {
	}
	
	public TestSubject ping(TestSubject x) {
		return null;
	}
	

}

