/**
 * @author atevstef
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */

import java.lang.reflect.*;
import java.util.*;

public class MethodInfo {
	
	private VarInfo returnType;
	private VarInfo[] paramTypes;
	private Class[] exceptions;
	private Method saveMeth;
	public String origName;
	public String overName;
	public int overConflicts;
	public MethodInfo next;
	
	public boolean seemsGetter() {
		return paramTypes.length== 0 && origName.length()> 3 && origName.startsWith("get");
	}
	
	public String setterBuddyName() {
		if (!seemsGetter()) return null;
		return "set"+ origName.substring(3);
	}
	
	public boolean matchesSetter(MethodInfo s) {
		if (!seemsGetter()) return false;
		return returnType.equals(s.paramTypes[0]) && setterBuddyName().equals(s.origName);
	}
	
	public String wrapperIDL() {
		String rt= returnType.wrapperIDL(true, null);
		if (rt== null) return null;
		String result= rt+ " "+	overName+ "(";
		if (paramTypes.length> 0) {
			rt= paramTypes[0].wrapperIDL(false, "p_0");
			if (rt== null) return null;
			result+= rt;
			for (int i= 1; i< paramTypes.length; ++i) {
				rt= paramTypes[i].wrapperIDL(false, "p_"+ i);
				if (rt== null) return null;
				result+= ", "+ rt;
			}
		}
		result+= ")";
		return result;
	}
	
	public void addExceptions(Hashtable ht) {
		for (int i= 0; i< exceptions.length; ++i)
			if (!ht.containsKey(exceptions[i].getName()))
				ht.put(exceptions[i].getName(), TypeNameManager.javaToIDLException(exceptions[i].getName()));
	}
	
	public void addTypes(Hashtable ht) {
		if (!returnType.isPrim) {
			String rt= returnType.wrapperIDL(true, null);
			if (!ht.containsKey(rt))
				ht.put(rt, rt);
		}	
		for (int i= 0; i< paramTypes.length; ++i) {
			if (paramTypes[i].isPrim)
				continue;
			String rt= paramTypes[i].wrapperIDL(true, null);
			if (!ht.containsKey(rt))
				ht.put(rt, rt);
		}
	}
	
	public void addPackages(Hashtable ht) {
		if (!returnType.isPrim) {
			String rt= returnType.getPackage();
			if (!ht.containsKey(rt))
				ht.put(rt, rt);
		}	
		for (int i= 0; i< paramTypes.length; ++i) {
			if (paramTypes[i].isPrim)
				continue;
			String rt= paramTypes[i].getPackage();
			if (!ht.containsKey(rt))
				ht.put(rt, rt);
		}
	}
	
	public String wrapperIDLExceptions() {
		String result= "";
		if (exceptions.length> 0) {
			result+= " raises(";
			result+= TypeNameManager.javaToIDLException(exceptions[0].getName());
			// all but first
			for (int i= 1; i< exceptions.length; ++i)
				result+= ", "+ TypeNameManager.javaToIDLException(exceptions[i].getName());
			result+= ")";
		}
		return result;
	}
	
	public String wrapperImpl() {
		String result= "";
		if (returnType.isArr)
			result= "\tpublic Object ";
		else
			result= "\tpublic "+ returnType.wrapperImpl(true, false, null)+ " ";
		result+= overName+ "(";
		if (paramTypes.length> 0) {
			result+= paramTypes[0].wrapperImpl(true, false, "p_0");
			for (int i= 1; i< paramTypes.length; ++i)
				result+= ", "+ paramTypes[i].wrapperImpl(true, false, "p_"+ i);
		}
		result+= ")"+ wrapperImplExceptions()+ " {\n";
		if (returnType.wrapperIDL(true, null).equals("void"))
			result+= "\t\t";
		else
			result+= "\t\treturn ";
		if (!returnType.isPrim && !returnType.isArr) result+= "("+
			returnType.wrapperImpl(true, false, null)+ ") new "+
			returnType.wrapperImpl(true, false, null)+ "Impl(";
		result+= "__holder."+ origName+ "(";
		if (paramTypes.length> 0) {
			if (paramTypes[0].isPrim)
				result+= paramTypes[0].wrapperImpl(false, false, "p_0");
			else
				result+= paramTypes[0].wrapperImpl(false, true, "p_0")+ ".__holder";
			for (int i= 1; i< paramTypes.length; ++i)
				if (paramTypes[i].isPrim)
					result+= ", "+ paramTypes[i].wrapperImpl(false, false, "p_"+ i);
				else
					result+= ", "+ paramTypes[i].wrapperImpl(false, true, "p_"+ i)+ ".__holder";
		}
		result+= ")";
		if (!returnType.isPrim) result+= ")";
		result+= ";\n\t}\n\n";
		return result;
	}

	public String wrapperImplExceptions() {
		String result= "";
		if (exceptions.length> 0) {
			result+= " throws ";
			result+= exceptions[0].getName();
			// all but first
			for (int i= 1; i< exceptions.length; ++i)
				result+= ", "+ exceptions[i].getName();
		}
		return result;
	}

	public boolean isOverloadOfSuper(Class c) {
		try {
			Class sc= c.getSuperclass();
			if (sc.getMethod(origName, saveMeth.getParameterTypes())!= null)
				return true;
			Class[] interfaces= c.getInterfaces();
			for (int i= 0; i< interfaces.length; ++i)
				if (interfaces[i].getMethod(origName, saveMeth.getParameterTypes())!= null)
					return true;
		} catch (Exception ex) {}
		return false;
	}	
	
	MethodInfo(Method m) {
		saveMeth= m;
		exceptions= m.getExceptionTypes();
		origName= m.getName();
		overName= origName;
		overConflicts= 1;
		returnType= new VarInfo(m.getReturnType());
		Class params[]= m.getParameterTypes();
		paramTypes= new VarInfo[params.length];
		for (int i= 0; i< params.length; ++i)
			paramTypes[i]= new VarInfo(params[i]);
		next= null;
	}
		
}
