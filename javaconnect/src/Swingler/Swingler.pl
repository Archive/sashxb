#!/usr/bin/perl -W

################################################################
#    Sash for Linux
#    The Sash Runtime for Linux
#
#   Copyright (C) 2000 IBM Corporation
#
#    This library is free software; you can redistribute it and/or
#    modify it under the terms of the GNU Lesser General Public
#    License as published by the Free Software Foundation; either
#    version 2.1 of the License, or (at your option) any later version.
#
#    This library is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with this library; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
#    Contact:
#    sashxb@sashxb.org
#
#    IBM Advanced Internet Technology Group
#    c/o Lotus Development Corporation
#    One Rogers Street
#    Cambridge, MA 02142
#    USA
#
################################################################
#
#Contributor(s): Stefan Atev
#
################################################################

use Data::Dumper;

#global variable, set to 1 if -r (--recursive) option was set
$recurseThroughTypes= 0;
#global variable, set to 1 if -i (--idl) option was set
$writeInterfaceDefinitions= 0;
#global variable, set to 1 if -j (--java) option was set
$writeInterfaceImplementations= 0;
#global variable, set to 1 if -H (--hidden) option was set
$showHiddenEntries= 0;

##############################################################################
#
# General routines
#
##############################################################################

sub delimit {
	my $list= shift;
	my $proc= shift;
	my $result= "";
	for (my $i= 0; $i < @{$list} ; ++$i) {
		$result.= $proc ? $proc->($list->[$i], $i) : $list->[$i];
		$result.= ", " unless ($i+ 1 == @{$list});
	}
	return $result;
}

sub canonicizeMethod {
	my $method= shift;
	return "$method->{RTYPE} $method->{NAME}(" . delimit($method->{PTYPES}) . ")";
}

sub findMethod {
	my $class= shift;
	my $method= shift;
	my $methodList= $class->{METHODS}->{$method->{NAME}};
	my $target= canonicizeMethod($method);
	foreach my $possibleMatch (@{$methodList}) {
		if (canonicizeMethod($possibleMatch) eq $target) {
			return $possibleMatch;
		}
	}
	return;
}

sub findMethodByCannon {
	my $class= shift;
	my $methodCannon= shift;
	my $mname= $methodCannon;
	$mname=~ s/\S+\s([^\(]+).*/$1/;
	my $methodList= $class->{METHODS}->{$mname};
	foreach my $possibleMatch (@{$methodList}) {
		if (canonicizeMethod($possibleMatch) eq $methodCannon) {
			return $possibleMatch;
		}
	}
	return;
}

sub loadCustomWrapConfig {
	my $substitutions;
	if (open(my $cache, "Swingler.wrap.cfg")) {
		while (my $line= <$cache>) {
			chomp($line);
			$line=~ s/\s//g;
			next if $line=~ /^\#/;
			if ($line=~ /\+\+\+SUBST/) {
				chomp(my $src= <$cache>);
				chomp(my $dst= <$cache>);
				push(@{$substitutions}, { SRC => $src, DST => $dst} );
				next;
			}
		}
		close($cache);
	}
	else {
		print "Creating default config: Swingler.wrap.cfg\n";
		open(my $cache, "> Swingler.wrap.cfg") or die("Bad things");
		print $cache "# Global exclude/modify file for type conversions\n# Entries are of the format:\n" .
			"# The following three lines show a substitution using Perl rules\n" .
			"#   +++SUBST\n" .
			"#   (javax?)\\.(Swing|awt)\\.(\\S+)\n" .
			"#   com\\.flip\\.\$2\\.\$1\\.\$3\n";
		close($cache);
	}
	return $substitutions;
}

$replacePatterns= loadCustomWrapConfig;

sub loadRenameConfig {
	my $class= shift;
	if (open(my $cache, "$class->{NAME}.rename.cfg")) {
		while (my $line= <$cache>) {
			chomp($line);
			next if $line=~ /^\#/;
			$line=~ s/\s\s+/ /g;
			findMethodByCannon($class, $1)->{NEWNAME}= $2 if $line=~ /(.+)\s\=\>\s(.+)/;
		}
		close($cache);
	}
	else {
		print "Creating default config: $class->{NAME}.rename.cfg\n";
		open(my $cache, "> $class->{NAME}.rename.cfg") or die("Bad things");
		while (my ($methodName, $methodList)= each(%{$class->{METHODS}})) {
			if (@{$methodList}> 1) {
				for (my $i= 0; $i< @{$methodList}; ++$i) {
					next if $methodList->[$i]->{NEWNAME};
					$methodList->[$i]->{NEWNAME}= "$methodName$i";
					print $cache canonicizeMethod($methodList->[$i]), " => $methodName$i\n";
				}
			}
			else {
				$methodList->[0]->{NEWNAME}= $methodName unless $methodList->[0]->{NEWNAME};
				print $cache canonicizeMethod($methodList->[0]), " => $methodName\n";
			}
		}
		close($cache);
	}
}

sub loadIncludeConfig {
	my $class= shift;
	my $result;
	if (open(my $cache, "$class->{NAME}.include.cfg")) {
		while (my $line= <$cache>) {
			chomp($line);
			next if $line=~ /^\#/;
			$line=~ s/\s\s+/ /g;
			findMethodByCannon($class, $2)->{WRAP}= ($1 eq "+") if $line=~ /\s*(\+|\-)\s+(.+)/;
		}
		close($cache);
	}
	else {
		print "Creating default config: $class->{NAME}.include.cfg\n";
		open(my $cache, "> $class->{NAME}.include.cfg") or die("Bad things");
		while (my ($methodName, $methodList)= each(%{$class->{METHODS}})) {
			foreach $method (@{$methodList}) {
				my $cname= canonicizeMethod($method);
				print $cache ($method->{WRAP} ? "+" : "-"), " $cname\n";
			}
		}
		close($cache);
	}
}

sub loadGUIDConfig {
	my $class= shift;
	my $result;
	if (open(my $cache, "$class->{NAME}.uuid.cfg")) {
		while (my $line= <$cache>) {
			chomp($line);
			$line=~ s/\s//g;
			if ($line=~ /(\S+)/) {
				$result= $1;
			}
		}
		close($cache);
	}
	else {
		print "Creating default config: $class->{NAME}.uuid.cfg\n";
		open(my $cache, "> $class->{NAME}.uuid.cfg") or die("Bad things");
		my $guid= `uuidgen` or die("Cannot generate GUID");
		chomp($guid);
		print $cache "$guid\n";
		$result= $guid;
		close($cache);
	}
	return $result;
}

#forward declaration
sub parseJavaType;
# forward
sub wrapVariableImpl;

%holderTable= (
	"byte" => "Byte",
	"short" => "Short",
	"char" => "Character",
	"boolean" => "Boolean",
	"int" => "Integer",
	"long" => "Long",
	"float" => "Float",
	"double" => "Double",
);
sub javaHolder {
	my $typ= shift;
	my $holder= $holderTable{$typ};
	return $holder ? "java.lang.$holder" : $typ;
}

sub javaHolderValue {
	my $typ= shift;
	my $var= shift;
	my $holder= $holderTable{$typ};
	return $holder ? "(($holder) $var).${typ}Value()" : "($typ) $var"
}

sub javaconnectHolderValue {
	my $typ= shift;
	my $var= shift;
	my $impltyp= wrapVariableImpl($typ);
	return $var if $primitiveTypes{scrubArrayDims($typ)};
	return $typ=~ /\[\]/ ? "a$var" : "($typ) ((${impltyp}Impl) $var).__holder";
}

sub scrubArrayDims {
	my $result= shift;
	$result=~ s/\[\]//g;
	return $result;
}

sub wrapExceptionIDL {
	my $ex= shift;
	$ex=~ s/\./_/g;
	return "jc_" . $ex;
}

##############################################################################
#
# Variable related routines
#
##############################################################################
%primitiveTypes= (
	"java.lang.Object" => "nsIVariant",
	"void" => "void",
	"byte" => "octet",
	"short" => "short",
	"char" => "wchar",
	"boolean" => "boolean",
	"int" => "long",
	"long" => "long long",
	"float" => "float",
	"double" => "double",
	"java.lang.String" => "AString",
	"java.lang.Byte" => "nsIVariant",
	"java.lang.Short" => "nsIVariant",
	"java.lang.Character" => "nsIVariant",
	"java.lang.Boolean" => "nsIVariant",
	"java.lang.Integer" => "nsIVariant",
	"java.lang.Long" => "nsIVariant",
	"java.lang.Float" => "nsIVariant",
	"java.lang.Double" => "nsIVariant",
);

sub parseVariable {
	my $variable= scrubArrayDims(shift);
	return unless $variable;
	return $variable if $primitiveTypes{$variable};
	$variable=~ s/$_->{SRC}/$_->{DST}/ foreach @{$replacePatterns};
	return $variable if $primitiveTypes{$variable};
	return if $variable eq "NONE";
	parseJavaType($variable) unless $convertedTypes{$variable};
	return if $convertedTypes{$variable} eq "IGNORETYPE";
	return $variable;
}

sub wrapVariableIDL {
	my $variable= shift;
	my $name= shift;
	my $arrayDims= 0;
	while ($variable=~ s/(\[\])//) { ++$arrayDims; }
	my $tvar= parseVariable($variable);
	$variable= $tvar if $tvar;
	my $varIDLName= $primitiveTypes{$variable};
	$varIDLName= $variable unless $varIDLName;
	$varIDLName=~ s/\.(\w+)\z/I$1/;
	$varIDLName=~ s/\.\l(\w+)/\u$1/g;
	if ($arrayDims== 1 && $name) {
		return "[array, size_is(${name}_length)] in " .
			(($variable eq "java.lang.String") ? "wstring" : "$varIDLName") .
			" $name, in unsigned long ${name}_length";
	}
	return $arrayDims ?
		($name ? "in nsIVariant $name" : "nsIVariant") :
		($name ? "in $varIDLName $name" : $varIDLName);
}

sub wrapVariableImpl {
	my $variable= shift;
	my $name= shift;
	my $arrayDims= 0;
	my $arrayTrail= "";
	while ($variable=~ s/(\[\])//) { ++$arrayDims; $arrayTrail.= "[]"; }
	my $tvar= parseVariable($variable);
	$variable= $tvar if $tvar;
	$varImplName= $variable;
	if (!$primitiveTypes{$variable}) {
		$varImplName=~ s/\.(\w+)\z/I$1/;
		$varImplName=~ s/\.\l(\w+)/\u$1/g;
	}
	return $arrayDims ?
		($name ? "$varImplName$arrayTrail $name" : "java.lang.Object") :
		($name ? "$varImplName $name" : $varImplName);
}

##############################################################################
#
# Constant related routines
#
##############################################################################
$constFieldPattern=
	'^\s*' .                              # start
	'public\sstatic\sfinal\s' .           # must be public static final
	'(\S+)\s' .                           # type name
	'(\S+)' .                             # field name
	';';                                  # done

sub parseConstant {
	my $line= shift;
	my $class= shift;
	return unless $line=~ $constFieldPattern;
	return {
		NAME => $2,
		RTYPE => $1,
		WRAP => parseVariable($1),
		CLASS => $class,
	};
}

sub wrapConstantIDL {
	my $constant= shift;
	my $result= "readonly attribute " . wrapVariableIDL($constant->{RTYPE}) . " $constant->{NAME};";
	$result= $showHiddenEntries ? "/*SKIPPED* $result */" : "" unless $constant->{WRAP};
	return $result;
}

sub wrapConstantImpl {
	my $constant= shift;
	my $class= shift;
	my $offset= shift;
	my $result= "${offset}public $constant->{RTYPE} get\u$constant->{NAME}() {\n";
	$result.= "${offset}\treturn $class->{NAME}.$constant->{NAME};\n";
	$result.= "${offset}}\n";
	$result= $showHiddenEntries ? "${offset}/*SKIPPED*\n$result\n${offset}*/" : "" unless $constant->{WRAP};
	return $result;
}

##############################################################################
#
# Method related routines
#
##############################################################################
$methodPattern=
	'\s*' .                               # start
	'public\s' .                          # must be public
	'(static\s)?' .                       # may be static, we care
	'(' .                                 # discard other modifiers
		'|native\s' .
		'|abstract\s' .
		'|final\s' .
		'|synchronized\s' .
	')*' .
	'(\S+)\s' .                           # type name
	'(\S+)\(' .                           # method name
	'([^\)]*)\)' .                        # comma separated param type list
	'((\[\])*)' .                         # return type array dimensions
	'(\s*throws\s([^\)]*))?' .            # throws clause
	';';                                  # done

sub parseMethod {
	my $line= shift;
	my $class= shift;
	return unless $line=~ $methodPattern;
	my $ptypes= $5;
	my $excepts= $9 ? $9 : "";
	my $method= {
		NAME => $4,
		ISSTATIC => $1,
		RTYPE => $7 ? ($3 . $7) : $3,
		WRAP => 1,
		CLASS => $class,
	};
	$method->{NAME}.= '_' if $method->{NAME}=~ /^(toString|clone|finalize|equals|hashCode)\z/;
	$ptypes=~ s/\s//g;
	$excepts=~ s/\s//g;
	$method->{PTYPES}= [split(/,/, $ptypes)];
	$method->{EXCEPTIONS}= [split(/,/, $excepts)];
	$method->{WRAP}= 0 unless parseVariable($method->{RTYPE});
	foreach $typ (@{$method->{PTYPES}}) {
		$method->{WRAP}= 0 unless parseVariable($typ);
	}
	return $method;
}

sub wrapMethodIDL {
	my $method= shift;
	my $cname= canonicizeMethod($method);
	$cname=~ s/\s//g;
	my $class= shift;
	my $returnType= wrapVariableIDL($method->{RTYPE});
	my $paramFilter= sub {
		return wrapVariableIDL(shift, "p_" . shift);
	};
	my $paramTypes= delimit($method->{PTYPES}, $paramFilter);
	my $exceptions= delimit($method->{EXCEPTIONS}, \&wrapExceptionIDL);
	my $result= "$returnType $method->{NEWNAME}($paramTypes)";
	$result.= " raises($exceptions)" if $exceptions;
	$result.= ";";
	$result= $showHiddenEntries ? "/*SKIPPED* $result */" : "" unless $method->{WRAP};
	return $result;
}

sub wrapMethodCall {
	my $dims= shift;
	my $typ= shift;
	my $offset= shift;
	my $resAcc= shift;
	if (!$resAcc) {
		$resAcc= "";
	}
	my $result= "";
	if ($dims) {
		my $dimTyp= "";
		for (my $i= 1; $i< $dims; ++$i) {
			$dimTyp.= "[]";
		}
		$result.= "${offset}\t${typ}${dimTyp} nres${dims}\[\]= new ${typ}\[res${resAcc}.length];\n";
		$result.= "${offset}\tfor (int i${dims}= 0; i${dims}< nres${dims}\.length; ++i${dims}) {\n";
		$result.= wrapMethodCall($dims- 1, $typ, "${offset}\t", "${resAcc}\[i${dims}\]");
		$result.= "${offset}\t}\n";
	}
	else {
		$result.= "${offset}\tnres1[i1]= new ${typ}(res${resAcc});\n";
	}
}

sub wrapIncomingArrayRec {
	my $fdims= shift;
	my $addon= shift;
	my $dims= shift;
	my $typ= shift;
	my $idx= shift;
	my $offset= shift;
	my $resAcc= shift;
	my $inp= wrapVariableIDL($typ) . "Impl";
	for (my $i= 0; $i< $fdims; ++$i) {
		$inp.= "[]";
	}
	$inp= "((${inp}) p_${idx})";
	if (!$resAcc) {
		$resAcc= "";
	}
	my $result= "";
	if ($dims) {
		my $dimTyp= "";
		for (my $i= 1; $i< $dims; ++$i) {
			$dimTyp.= "[]";
		}
		$result.= "${offset}$typ${dimTyp} ap_${idx}${addon}\[]= new ${typ}\[$inp${resAcc}.length];\n";
		$result.= "${offset}for (int i${dims}= 0; i${dims}< ap_${idx}${addon}\.length; ++i${dims}) {\n";
		$result.= wrapIncomingArrayRec($fdims, (($dims!= 1) ? "_l$addon" : $addon), $dims- 1, $typ, $idx, "${offset}\t", "${resAcc}\[i${dims}\]");
		$result.= "${offset}}\n";
	}
	else {
		$result.= "${offset}ap_${idx}${addon}\[i1]= ($inp${resAcc}).__holder;\n";
	}
	return $result;
}

sub wrapIncomingArray {
	my $typ= shift;
	my $idx= shift;
	my $offset= shift;
	my $arrayDims= 0;
	my $result= "";
	while ($typ=~ s/\[\]//) { ++$arrayDims; }
	return "" unless $arrayDims;
	return "" if $primitiveTypes{$typ};
	$result.= wrapIncomingArrayRec($arrayDims, "", $arrayDims, $typ, $idx, $offset);
	return $result;
}

sub wrapMethodImpl {
	my $method= shift;
	my $class= shift;
	my $offset= shift;
	my $returnType= wrapVariableImpl($method->{RTYPE});
	
	my $paramTypesFilter= sub {
		return wrapVariableImpl(shift, "p_" . shift);
	};
	my $paramTypes= delimit($method->{PTYPES}, $paramTypesFilter);
	
	my $parametersPreFilter= sub {
		return wrapIncomingArray(shift, shift, "\t\t");
	};
	my $preParameters= delimit($method->{PTYPES}, $parametersPreFilter);
	$preParameters=~ s/\,\s*//g;
	my $parametersFilter= sub {
		return javaconnectHolderValue(shift, "p_" . shift);
	};
	my $parameters= delimit($method->{PTYPES}, $parametersFilter);
	my $exceptions= delimit($method->{EXCEPTIONS});

	my $result= "${offset}public ";
	$result.= "$returnType $method->{NEWNAME}($paramTypes)";
	$result.= " throws $exceptions" if $exceptions;
	$result.= " {\n";
	$result.= $preParameters;
	my $mname= $method->{NAME};
	$mname=~ s/^(.*)_\z/$1/ if $mname=~ /^(toString|equals|hashCode|finalize|clone)_\z/;
	my $call= $method->{ISSTATIC} ? "$class->{NAME}.$mname" : "__holder.$mname";
	if ($returnType eq "void") {
		$result.= "${offset}\t$call($parameters);";
	}
	elsif ($primitiveTypes{scrubArrayDims($method->{RTYPE})}) {
		$result.= "${offset}\treturn $call($parameters);";
	}
	else {
		if ($method->{RTYPE}=~ /\[\]/) {
			my $arrayDims= 0;
			my $typ= $method->{RTYPE};
			while ($typ=~ s/\[\]//) { ++$arrayDims; }
			$result.= "${offset}\t$method->{RTYPE} res= $call($parameters);\n";
			$result.= wrapMethodCall($arrayDims, wrapVariableIDL($typ) . "Impl", $offset);
			$result.= "${offset}\treturn nres$arrayDims;\n";
		}
		else {
			$result.= "${offset}\treturn new " . wrapVariableIDL($method->{RTYPE}) . "Impl($call($parameters))";
		}
	}
	$result.= ";\n${offset}}\n";
	$result= $showHiddenEntries ? "/*SKIPPED* $result */" : "" unless $method->{WRAP};
	return $result;
}

##############################################################################
#
# Constructor routines
#
##############################################################################

sub parseConstructor {
	my $line= shift;
	my $class= shift;
	my $className= $class->{NAME};
	my $constructorPattern=
		'\s*' .                               # start
		'public\s' .                          # must be public
		"$className" . '\(' .                 # class name == constructor name
		'([^\)]*)\)' .                        # comma separated param type list
		'(\s*throws\s([^\)]*))?' .            # throws clause
		';';                                  # done
	if ($line !~ $constructorPattern) { return; };
	my $ptypes= $1 ? $1 : "";
	my $excepts= $3 ? $3 : "";
	my $constructor= {
		WRAP => 1,
		CLASS => $class,
	};
	$ptypes=~ s/\s//g;
	$excepts=~ s/\s//g;
	$constructor->{PTYPES}= [split(/,/, $ptypes)];
	$constructor->{EXCEPTIONS}= [split(/,/, $excepts)];
	foreach $typ (@{$constructor->{PTYPES}}) {
		$constructor->{WRAP}= 0 unless parseVariable($typ);
	}
	return $constructor;
}

sub wrapConstructorImpl {
	my $constructor= shift;
	my $class= shift;
	my $offset= shift;
	my $parametersFilter= sub {
		return javaHolderValue(shift, "argv[" . (shift) . "]");
	};
	my $parameters= delimit($constructor->{PTYPES}, $parametersFilter);
	my $result= "${offset}if (argv.length== " . (scalar @{$constructor->{PTYPES}}) ;
	my $pcnt= 0;
	foreach $param (@{$constructor->{PTYPES}}) {
		my $archtype= javaHolder($param);
		$result.= " && (argv[$pcnt] instanceof $archtype)";
		++$pcnt;
	}
	$result.= ") {\n${offset}\t__holder= new $class->{NAME}($parameters);\n";
	$result.= "${offset}\treturn true;\n";
	$result.= "${offset}}\n";
	$result= $showHiddenEntries ? "${offset}/*SKIPPED*\n$result\n${offset}*/" : "" unless $constructor->{WRAP};
	return $result;
}
##############################################################################
#
# Class/Interface routines
#
##############################################################################
$classPattern=
	'\s*' .                               # start
	'public\s' .                          # must be public
	'(' .                                 # discard other modifiers
		'|native\s' .
		'|abstract\s' .
		'|final\s' .
		'|static\s' .
		'|synchronized\s' .
	')*' .
	'class\s' .                           # classese, please
	'(\S*)\s' .                           # class name
	'(extends\s(\S+))?' .                 # extends something ?
	'(implements\s([^\{]+))?';            # implements 

$interfacePattern=
	'\s*' .                               # start
	'public\s' .                          # must be public
	'(' .                                 # discard other modifiers
		'|native\s' .
		'|abstract\s' .
		'|final\s' .
		'|synchronized\s' .
	')*' .
	'interface\s' .                       # interface, please
	'(\S*)\s' .                           # interface name
	'(extends\s([^\{\/]+))?';             # extends list 

sub parseClass {
	my $line= shift;
	return unless $line;
	my $JAVAPSRC= shift;
	my $class;
	if ($line=~ $classPattern) {
		$class= {
			TYPE => 'class',
			NAME => $2,
		};
		my $impls= $5 ? $5 : "";
		$impls=~ s/\s//g;
		$class->{IMPLEMENTS}= [split(/,/, $impls)];
		$class->{EXTENDS}= $4;
	}
	elsif ($line=~ $interfacePattern) {
		$class= {
			TYPE => 'interface',
			NAME => $2,
		};
		my $impls= $4 ? $4 : "";
		$impls=~ s/\s//g;
		$class->{EXTENDS}= $impls;
	}
	else {
		return;
	}
	$class->{ABSTRACT}= 1 if $line=~ /public.*abstract.*class/;
	my $parentList= [$class->{EXTENDS}, $class->{IMPLEMENTS} ? @{$class->{IMPLEMENTS}} : ()];
	parseVariable($_) foreach @{$parentList};
	my $methods;
	my $constructors;
	my $constants;
	while (my $line= <$JAVAPSRC>) {
		chomp($line);
		my $methodRef= parseMethod($line, $class);
		if ($methodRef) {
			push(@{$methods->{$methodRef->{NAME}}}, $methodRef);
			next;
		}
		my $constructorRef= parseConstructor($line, $class);
		if ($constructorRef) {
			push(@{$constructors}, $constructorRef);
			next;
		}
		my $constantRef= parseConstant($line, $class);
		if ($constantRef) {
			$constants->{$constantRef->{NAME}}= $constantRef;
			next;
		}
	}
	$class->{METHODS}= $methods;
	$class->{CONSTRUCTORS}= $constructors;
	$class->{CONSTANTS}= $constants;
	return $class;
}

sub pullMethods {
	my $class= shift;
	my $methods= shift;
	my $filterProc= shift;
	while (my ($methodName, $methodList)= each(%{$class->{METHODS}})) {
		foreach my $method (@{$methodList}) {
			if (!$filterProc || $filterProc->($method)) {
				push(@{$methods->{$method->{NAME}}}, $method);
			}
		}
	}
	return $methods;
}

sub listClassMethods {
	my $class= shift;
	my $result;
	while (my ($methodName, $methodList)= each(%{$class->{METHODS}})) {
		foreach my $method (@{$methodList}) {
			if (!$filterProc || $filterProc->($method)) {
				push(@{$result}, $method);
			}
		}
	}
	return $result;
}

sub processClass {
	my $class= shift;
	my $newMethods;
	
	if (!$class || $class->{PROCESSED}) {
		return;
	}
	print "Processing $class->{NAME}\n";

	my $parentList= [$class->{EXTENDS}, $class->{IMPLEMENTS} ? @{$class->{IMPLEMENTS}} : () ];
	processClass($convertedTypes{$_}) foreach @{$parentList};
	$newMethods= pullMethods($convertedTypes{$_}, $newMethods) foreach @{$parentList};

	$filterOriginal= sub {
		my $method= shift;
		my $parentList= [$method->{CLASS}->{EXTENDS}, $method->{CLASS}->{IMPLEMENTS} ? @{$method->{CLASS}->{IMPLEMENTS}} : () ];
		foreach my $parentName (@{$parentList}) {
			return 0 if findMethod($convertedTypes{$parentName}, $method);
		}
		return 1;
	};
	$newMethods= pullMethods($class, $newMethods, $filterOriginal);

	$class->{PROCESSED}= 1;
	$class->{METHODS}= $newMethods;

	loadRenameConfig($class);
	loadIncludeConfig($class);
	$class->{GUID}= loadGUIDConfig($class);
}

sub wrapClassIDL {
	my $class= shift;
	if (not $class) { return; }
	my $iname= wrapVariableIDL($class->{NAME});
	my $superCls= $class->{EXTENDS};
	if (not $superCls && parseVariable($superCls)) {
		$superCls= undef;
	}
	my $result=
			"/* DO NOT EDIT THIS FILE - IT WAS AUTOGENERATED BY Swingler\n" .
			"   FROM the Java class $class->{NAME}\n" .
			"   The corresponding implementation is in ${iname}Impl.java\n" .
			"*/\n\n";
	
	## Handle include list
	my $includes= { "nsISupports" => 1, "nsIVariant" => 1 };
	if ($superCls && parseVariable($superCls)) {
		$includes->{wrapVariableIDL($superCls)}= 1;
	}
	foreach $typ (@{$class->{IMPLEMENTS}}) {
		if (parseVariable($typ)) {
			$includes->{wrapVariableIDL($typ)}= 1;
		}
	}
	while (my ($typ, $dummy)= each(%{$includes})) {
		if ($dummy) {
			$result.= "#include \"$typ.idl\"\n";
		}
	}
	$result.= "\n";

	## Handle refrenced types
	my $interfaces;
	while (my ($constantName, $constantRef)= each(%{$class->{CONSTANTS}})) {
		next unless $constantRef->{WRAP};
		if ($primitiveTypes{scrubArrayDims($constantRef->{RTYPE})}) {
			next;
		}
		print "$constantRef->{RTYPE}\n";
		$interfaces->{wrapVariableIDL($constantRef->{RTYPE})}= 1;
	}
	my $exceptions;
	while (my ($methodName, $methodList)= each(%{$class->{METHODS}})) {
		foreach $method (@{$methodList}) {
			next unless $method->{WRAP};
			
			if (not $primitiveTypes{scrubArrayDims($method->{RTYPE})}) {
				$interfaces->{wrapVariableIDL($method->{RTYPE})}= 1;
			}
			foreach $typ (@{$method->{PTYPES}}) {
				if (not $primitiveTypes{scrubArrayDims($typ)}) {
					$interfaces->{wrapVariableIDL(scrubArrayDims($typ))}= 1;
				}
			}
			foreach $exc (@{$method->{EXCEPTIONS}}) {
				$exceptions->{wrapExceptionIDL($exc)}= 1;
			}
		}
	}
	$interfaces->{"nsIVariant"}= 0;
	while (my ($typ, $dummy)= each(%{$interfaces})) {
		if ($dummy) {
			$result.= "interface $typ;\n";
		}
	}
	$result.= "\n";
	
	$result.= "[scriptable, uuid($class->{GUID})]\n" .
		"interface $iname: ";
	if ($superCls && wrapVariableIDL($superCls) ne "nsIVariant") {
		$result.= wrapVariableIDL($superCls);
	}
	else {
		$result.= "nsISupports";
	}
	$result.= " {\n\n";
	
	## Handle exceptions declarations
	while (my ($exc, $dummy)= each(%{$exceptions})) {
		if ($dummy) {
			$result.= "\texception $exc {};\n";
		}
	}
	$result.= "\n";
	
	## Handle constant declarations
	while (my ($constantName, $constantRef)= each(%{$class->{CONSTANTS}})) {
		if ($constantRef->{CLASS}== $class) {
			my $wrap= wrapConstantIDL($constantRef);
			$result.= "\t$wrap\n" unless not $wrap;
		}
	}
	$result.= "\n";
	
	## Handle method declarations
	while (my ($methodName, $methodList)= each(%{$class->{METHODS}})) {
		foreach $method (@{$methodList}) {
			## MUST skip inherited methods in IDL, but NOT in implementation
			if ($method->{CLASS}== $class) {
				my $wrap= wrapMethodIDL($method, $class);
				$result.= "\t$wrap\n" unless not $wrap;
			}
		}
	}
	$result.= "};\n";
	if ($writeInterfaceDefinitions) {
		open(my $outidl, "> ${iname}.idl") or die("Thousand horrible deaths");
		print "Creating ${iname}.idl\n";
		print $outidl $result;
		close($outidl);
	}
}

sub wrapClassImpl {
	my $class= shift;
	if (not $class) { return; }
	my $iname= wrapVariableIDL($class->{NAME});
	my $superCls= $class->{EXTENDS};
	if (not $superCls && parseVariable($superCls)) {
		$superCls= undef;
	}
	my $result=
			"/* DO NOT EDIT THIS FILE - IT WAS AUTOGENERATED BY Swingler\n" .
			"   FROM the Java class $class->{NAME}\n" .
			"   The corresponding interface is in ${iname}.idl\n" .
			"*/\n\n";
	
	$result.= "package org.mozilla.xpcom;\n\n";

	## Handle imports	
	my $interfaces;
	while (my ($constantName, $constantRef)= each(%{$class->{CONSTANTS}})) {
		if (not $constantRef->{WRAP}) {
			next;
		}
		if ($primitiveTypes{scrubArrayDims($constantRef->{RTYPE})}) {
			next;
		}
		$interfaces->{scrubArrayDims($constantRef->{RTYPE})}= 1;
	}
	while (my ($methodName, $methodList)= each(%{$class->{METHODS}})) {
		foreach $method (@{$methodList}) {
			if (not $method->{WRAP}) {
				next;
			}
			if (not $primitiveTypes{scrubArrayDims($method->{RTYPE})}) {
				$interfaces->{scrubArrayDims($method->{RTYPE})}= 1;
			}
			foreach $typ (@{$method->{PTYPES}}) {
				if (not $primitiveTypes{scrubArrayDims($typ)}) {
					$interfaces->{scrubArrayDims($typ)}= 1;
				}
			}
		}
	}
	$interfaces->{"nsIVariant"}= 0;
	while (my ($typ, $dummy)= each(%{$interfaces})) {
		if ($dummy) {
			$result.= "import $typ;\n";
		}
	}
	$result.= "\n";
	
	$result.= "public class ${iname}Impl implements $iname";
	$result.= ", sashIScriptableConstructor";
	foreach $impls (@{$class->{IMPLEMENTS}}) {
		$result.= ", $impls";
	}
	$result.= " {\n";

	## Boilerplate
	$result.= "\n\t/* Underlying implementation */\n" .
		"\tpublic $class->{NAME} __holder;\n\n" .
		"\t${iname}Impl() {\n" .
		"\t\t__holder= null;\n" .
		"\t}\n\n" .
		"\t${iname}Impl($class->{NAME} __in) {\n" .
		"\t\t__holder= __in;\n" .
		"\t}\n\n" .
		"\tpublic Object queryInterface(IID ignored) {\n" .
		"\t\t/* this would never be called */\n" .
		"\t\treturn this;\n" .
		"\t}\n\n";
	
	## Do the consturctors
	$result.= "\tpublic boolean initializeNewObject(sashIActionRuntime actionRuntime, sashIExtension2 parentExtension, int contextID, Object[] argv) {\n";
	$result.= "\t\ttry {\n";
	if (not $class->{ABSTRACT}) {
		foreach $constructor (@{$class->{CONSTRUCTORS}}) {
			$result.= wrapConstructorImpl($constructor, $class, "\t\t\t");
		}
	}
	$result.= "\t\t} catch (Exception ex) {\n";
	$result.= "\t\t\treturn false;\n";
	$result.= "\t\t}\n";
	$result.= "\t\treturn false;\n";
	$result.= "\t}\n\n";
	## Handle constant declarations
	while (my ($constantName, $constantRef)= each(%{$class->{CONSTANTS}})) {
		my $wrap= wrapConstantImpl($constantRef, $class, "\t");
		$result.= "$wrap\n" unless not $wrap;
	}
	$result.= "\n";
	
	## Handle method declarations
	while (my ($methodName, $methodList)= each(%{$class->{METHODS}})) {
		foreach $method (@{$methodList}) {
			my $wrap= wrapMethodImpl($method, $class, "\t");
			$result.= "$wrap\n" unless not $wrap;
		}
	}
	$result.= "};\n";
	if ($writeInterfaceImplementations) {
		open(my $outidl, "> ${iname}Impl.java") or die("Thousand horrible deaths");
		print "Creating ${iname}Impl.java\n";
		print $outidl $result;
		close($outidl);
	}
}

##############################################################################
#
# Main
#
##############################################################################

sub parseJavaType {
	my $jtyp= shift;
	if ($convertedTypes{$jtyp}) {
		return;
	}
	$convertedTypes{$jtyp}= "sentinel";
	print "Parsing $jtyp\n";
	open(my $JAVAPSRC, "javap -public $jtyp |") or die("BUST");
	<$JAVAPSRC> foreach (1 .. 8); # skip legalese header

	my $line= <$JAVAPSRC>;
	chomp($line);
	# fix space promblem with strange characters, i.e. in java.awt.RenderingHints.Key
	$line=~ s/\.\ /./g;
	my $tclass= parseClass($line, $JAVAPSRC);
	close($JAVAPSRC);
	# skip non-public classes
	$convertedTypes{$jtyp}= $tclass ? $tclass : "IGNORETYPE";
	return $convertedTypes{$jtyp};
}

foreach $arg (@ARGV) {
	if ($arg=~ /-(r|-recursive)/) {
		$recurseThroughTypes= 1;
	}
	elsif ($arg=~ /-(i|-idl)/) {
		$writeInterfaceDefinitions= 1;
	}
	elsif ($arg=~ /-(j|-java)/) {
		$writeInterfaceImplementations= 1;
	}
	elsif ($arg=~ /-(H|-hidden)/) {
		$showHiddenEntries= 1;
	}
	else {
		push(@classesToWrap, $arg);
	}
}
if (@classesToWrap < 1) {
	print STDERR "Usage: Swingler [-r] [-i] [-j] [-H] classes...\n";
}
else {
	parseJavaType($_) foreach @classesToWrap;
	foreach $cls (values(%convertedTypes)) {
		next if $cls eq "IGNORETYPE";
		processClass($cls);
		wrapClassIDL($cls);
		wrapClassImpl($cls);
	}
}
