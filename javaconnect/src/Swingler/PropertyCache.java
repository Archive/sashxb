/**
 * @author atevstef
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */

import java.util.*;
import java.io.*;

public class PropertyCache {
	/* a map of class/interface name to iid */
	private static Hashtable iids;
	/* a map of clasname+methodproto to self - include or not? */
	/* list non-includes */
	private static Hashtable wrap;
	/* a map of clasname+methodproto to new name */
	private static Hashtable overNames;
	
	static {
		iids= new Hashtable();
		wrap= new Hashtable();
		overNames= new Hashtable();
	}
	
	PropertyCache() {
	}
	
	private static void loadTable(String fname, Hashtable ht, boolean hasLine2) {
		System.err.println("Loading properties cache: "+ fname);
		try {
			BufferedReader bf= new BufferedReader(new FileReader(fname));
			String line1= bf.readLine();
			String line2;
			while (line1!= null && !line1.equals("")) {
				if (hasLine2) {
					line2= bf.readLine();
					ht.put(line1, line2);
				}
				else
					ht.put(line1, line1);
				line1= bf.readLine();
			}
			System.err.println("DONE");
			bf.close();
		} catch (Exception ex) {
			/* we don't care about exceptions */
		}
	}
	
	public static void load() {
		loadTable("swingler.iids.cache", iids, true);
		loadTable("swingler.wrap.cache", wrap, false);
		loadTable("swingler.over.cache", overNames, true);
	}

	private static void saveTable(String fname, Hashtable ht, boolean hasLine2) {
		try {
			System.err.println("Saving properties cache: "+ fname);
			BufferedWriter bf= new BufferedWriter(new FileWriter(fname));
			Enumeration i= ht.keys();
			while (i.hasMoreElements()) {
				String l1= (String) i.nextElement();
				bf.write(l1);
				bf.newLine();
				String l2;
				if (hasLine2) {
					l2= (String) ht.get(l1);
					bf.write(l2);
					bf.newLine();
				}
			}
			bf.close();
		} catch (Exception ex) {
			/* like I care */
		}
	}
	
	public static void save() {
		saveTable("swingler.iids.cache", iids, true);
		saveTable("swingler.wrap.cache", wrap, false);
		saveTable("swingler.over.cache", overNames, true);
	}
	
	public static String getOver(String cn, String mproto) {
		return (String) overNames.get(cn+ "::"+ mproto);
	}

	public static String getUUID(String cn) {
		return (String) iids.get(cn);
	}

	public static boolean getInclude(String cn, String mproto) {
		return wrap.get(cn+ "::"+ mproto)== null;
	}
	
	public static void setOver(String cn, String mproto, String val) {
		overNames.put(cn+ "::"+ mproto, val);
	}

	public static void setInclude(String cn, String mproto, boolean value) {
		if (value)
			wrap.remove(cn+ "::"+ mproto);
		else
			wrap.put(cn+ "::"+ mproto, cn+ "::"+ mproto);
	}

	public static void setUUID(String cn, String uuid) {
		iids.put(cn, uuid);
	}

	

}

