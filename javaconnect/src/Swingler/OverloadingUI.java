/**
 * @author atevstef
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */

import javax.swing.*;
import javax.swing.table.*;
import java.util.*;

public class OverloadingUI {

	private JTable table;
	private JDialog dialog;
	private Vector protoCol;
	private Vector namesCol;
	private Vector includeCol;
	private String className;
	
	OverloadingUI(Vector proto, Vector names, Vector include, String tname) {
		protoCol= proto;
		className= tname;
		namesCol= names;
		if (include== null) {
			includeCol= new Vector(proto.size());
			for (int i= 0; i< proto.size(); ++i)
				includeCol.add(new Boolean(true));
		}
		else
			includeCol= include;
			
		TableModel model= new AbstractTableModel() {
    		public String getColumnName(int col) { 
        		if (col== 0) return "Prototype";
        		if (col== 1) return "New Name"; 
        		if (col== 2) return "Include?";
        		return "";
    		}
    		public int getRowCount() {
    			return protoCol.size();
    		}
    		public int getColumnCount() {
    			return 3;
    		}
    		public Object getValueAt(int row, int col) {
    			if (col== 0) return protoCol.elementAt(row);
        		if (col== 1) return namesCol.elementAt(row);
        		if (col== 2) return includeCol.elementAt(row);
        		return null;
    		}
    		public boolean isCellEditable(int row, int col) {
    			return col> 0;
    		}
    		public void setValueAt(Object value, int row, int col) {
    			if (col== 1)
        			namesCol.setElementAt(value, row);
        		if (col== 2)
        			includeCol.setElementAt(value, row);
				fireTableCellUpdated(row, col);
    		}
    		public Class getColumnClass(int col) {
    			if (col== 2) return Boolean.class;
    			return String.class;
    		}
		};
		table= new JTable(model);
	}

	public void show() {
		System.err.println("About to show UI for class: "+ className);
		dialog= new JDialog();
		dialog.setTitle(className);
		table.getColumnModel().getColumn(0).setPreferredWidth(300);
		table.getColumnModel().getColumn(1).setPreferredWidth(150);
		table.getColumnModel().getColumn(2).setPreferredWidth(100);
		System.err.println("Up to JScrollPane creation: "+ className);
		dialog.getContentPane().add(new JScrollPane(table));
		dialog.setSize(600, 400);
		dialog.setModal(true);
		dialog.show();
	}
}
