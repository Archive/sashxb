
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "jcMarshallerTestImplC.h"
#include "nsIVariant.h"
#include "nsIFile.h"
#include "nsString.h"
#include "nsReadableUtils.h"
#include "prmem.h"
#include "plstr.h"
#include <iostream>
#include <iomanip>

NS_IMPL_ISUPPORTS1(jcMarshallerTestImplC, jcIMarshallerTest)

jcMarshallerTestImplC::jcMarshallerTestImplC() {
  NS_INIT_ISUPPORTS();
}

jcMarshallerTestImplC::~jcMarshallerTestImplC() {
}

#define RESCHECK(xx) \
	res= xx; \
	if (NS_FAILED(res)) \
		cerr << "\tError " << hex << res << dec << endl;

#define PRIMITIVETEST(namestr, procname, vartype, val1) \
	{ \
		vartype tret, tout, bret, bout; \
		cerr << "\tTest " << namestr << endl; \
		RESCHECK(##procname##(val1, &tout, &tret)); \
		RESCHECK(buddy->##procname##(val1, &bout, &bret)); \
		if (tout!= bout || tret!= bret) \
			cerr << "\t\tFAILURE" << endl; \
		else \
			cerr << "\t\tok" << endl; \
	}

#define MOZSTRINGTEST(namestr, procname, vartype, valstr) \
	{ \
		cerr << "\tTest " << namestr << endl; \
		vartype in, tout, tret, bout, bret; \
		in= NS_LITERAL_STRING(valstr); \
		RESCHECK(##procname##(in, tout, tret)); \
		if (!in.Equals(tout) || !in.Equals(tret) || !tout.Equals(tret)) \
			cerr << "\t\tC Test Broken" << endl; \
		RESCHECK(buddy->##procname##(in, bout, bret)); \
		if (!tret.Equals(bret) || !tout.Equals(bout)) \
			cerr << "\t\tFAILURE" << endl; \
		else \
			cerr << "\t\tok" << endl; \
	}

#define MOZCSTRINGTEST(namestr, procname, vartype, valstr) \
	{ \
		cerr << "\tTest " << namestr << endl; \
		vartype in, tout, tret, bout, bret; \
		in= NS_LITERAL_CSTRING(valstr); \
		RESCHECK(##procname##(in, tout, tret)); \
		if (!in.Equals(tout) || !in.Equals(tret) || !tout.Equals(tret)) \
			cerr << "\t\tC Test Broken" << endl; \
		RESCHECK(buddy->##procname##(in, bout, bret)); \
		if (!tret.Equals(bret) || !tout.Equals(bout)) \
			cerr << "\t\tFAILURE" << endl; \
		else \
			cerr << "\t\tok" << endl; \
	}

/* void testBuddy (in jcIMarshallerTest buddy); */
NS_IMETHODIMP jcMarshallerTestImplC::TestBuddy(jcIMarshallerTest *buddy) {
	cerr << endl << "JavaConnect Marshaller Test, C-Side Client" << endl << endl;
	cerr << "Comparing results with component " << buddy << endl << endl;
	nsresult res;
	
	PRIMITIVETEST("Boolean", TestBoolean, PRBool, PR_FALSE);
	PRIMITIVETEST("Boolean", TestBoolean, PRBool, PR_TRUE);
	PRIMITIVETEST("Byte", TestByte, PRUint8, 1);
	PRIMITIVETEST("Byte", TestByte, PRUint8, 255);
	PRIMITIVETEST("Short", TestShort, PRInt16, -1000);
	PRIMITIVETEST("Short", TestShort, PRInt16, 1000);
	PRIMITIVETEST("Char", TestChar, PRUnichar, 0);
	PRIMITIVETEST("Char", TestChar, PRUnichar, 0xffff);
	PRIMITIVETEST("Int", TestInt, PRInt32, -1000000);
	PRIMITIVETEST("Int", TestInt, PRInt32, 1000000);
	PRIMITIVETEST("Long", TestLong, PRInt64, -10000000000LL);
	PRIMITIVETEST("Long", TestLong, PRInt64, 10000000000LL);
	
	MOZSTRINGTEST("nsXPIDLString", TestAString, nsXPIDLString, "Test Whamma Lamma");
	MOZSTRINGTEST("nsXPIDLString", TestAString, nsXPIDLString, "developerWorks(tm)");
	MOZSTRINGTEST("nsAutoString", TestAString, nsAutoString, "Enter the Linux Zone");
	MOZSTRINGTEST("nsAutoString", TestAString, nsAutoString, "Speed-start your Linux app");
	MOZSTRINGTEST("nsString", TestAString, nsString, "Linux Software Evaluation Kit");
	MOZSTRINGTEST("nsString", TestAString, nsString, "This CD set contains:");
	
	MOZSTRINGTEST("DOM:nsXPIDLString", TestDOMString, nsXPIDLString, "Test Whamma Lamma");
	MOZSTRINGTEST("DOM:nsXPIDLString", TestDOMString, nsXPIDLString, "developerWorks(tm)");
	MOZSTRINGTEST("DOM:nsAutoString", TestDOMString, nsAutoString, "Enter the Linux Zone");
	MOZSTRINGTEST("DOM:nsAutoString", TestDOMString, nsAutoString, "Speed-start your Linux app");
	MOZSTRINGTEST("DOM:nsString", TestDOMString, nsString, "Linux Software Evaluation Kit");
	MOZSTRINGTEST("DOM:nsString", TestDOMString, nsString, "This CD set contains:");
	
	MOZCSTRINGTEST("nsXPIDLCString", TestCString, nsXPIDLCString, "Test Whamma Lamma");
	MOZCSTRINGTEST("nsXPIDLCString", TestCString, nsXPIDLCString, "developerWorks(tm)");
	MOZCSTRINGTEST("nsCAutoString", TestCString, nsCAutoString, "Enter the Linux Zone");
	MOZCSTRINGTEST("nsCAutoString", TestCString, nsCAutoString, "Speed-start your Linux app");
	MOZCSTRINGTEST("nsCString", TestCString, nsCString, "Linux Software Evaluation Kit");
	MOZCSTRINGTEST("nsCString", TestCString, nsCString, "This CD set contains:");
	
	MOZCSTRINGTEST("UTF:nsXPIDLCString", TestUTF8String, nsXPIDLCString, "Test Whamma Lamma");
	MOZCSTRINGTEST("UTF:nsXPIDLCString", TestUTF8String, nsXPIDLCString, "developerWorks(tm)");
	MOZCSTRINGTEST("UTF:nsCAutoString", TestUTF8String, nsCAutoString, "Enter the Linux Zone");
	MOZCSTRINGTEST("UTF:nsCAutoString", TestUTF8String, nsCAutoString, "Speed-start your Linux app");
	MOZCSTRINGTEST("UTF:nsCString", TestUTF8String, nsCString, "Linux Software Evaluation Kit");
	MOZCSTRINGTEST("UTF:nsCString", TestUTF8String, nsCString, "This CD set contains:");
	
	{
		cerr << "\tTest char *" << endl;
		char *in= 0, *tout= 0, *tret= 0, *bout= 0, *bret= 0;
		in= PL_strdup("Testing the stringie, hmm shweet");
		RESCHECK(TestCharStr(in, &tout, &tret));
		RESCHECK(buddy->TestCharStr(in, &bout, &bret));
		if (PL_strcmp(tout, bout)!= 0 || PL_strcmp(tret, bret)!= 0)
			cerr << "\t\tFAILURE" << endl;
		else
			cerr << "\t\tok" << endl;
		PR_FREEIF(in);
		PR_FREEIF(tout);
		PR_FREEIF(tret);
		PR_FREEIF(bout);
		PR_FREEIF(bret);
	}

	{
		cerr << "\tTest char * with size" << endl;
		char *in= 0, *tout= 0, *tret= 0, *bout= 0, *bret= 0;
		PRUint32 inl, toutl, boutl;
		char *tmp= PL_strdup("Testing the stringie, hmm shweet, and size, too");
		inl= PL_strlen(tmp);
		in= (char *) PR_MALLOC(sizeof(char)* inl);
		memcpy(in, tmp, sizeof(char)* inl);
		PR_FREEIF(tmp);
		
		RESCHECK(TestCharStrWithSize(in, inl, &tout, &toutl, &tret));
		RESCHECK(buddy->TestCharStrWithSize(in, inl, &bout, &boutl, &bret));
		if (
			inl!= boutl || inl!= toutl ||
			PL_strncmp(tout, bout, boutl)!= 0 || PL_strcmp(tret, bret)!= 0
			)
			cerr << "\t\tFAILURE" << endl;
		else
			cerr << "\t\tok" << endl;
		PR_FREEIF(in);
		PR_FREEIF(tout);
		PR_FREEIF(tret);
		PR_FREEIF(bout);
		PR_FREEIF(bret);
	}

	{
		cerr << "\tTest PRUnichar *" << endl;
		PRUnichar *in= 0, *tout= 0, *tret= 0, *bout= 0, *bret= 0;
		in= ToNewUnicode(NS_LITERAL_STRING("UNICODE ATTACK"));
		RESCHECK(TestWCharStr(in, &tout, &tret));
		RESCHECK(buddy->TestWCharStr(in, &bout, &bret));
		if (!nsDependentString(tout).Equals(bout) ||
			!nsDependentString(tret).Equals(bret))
			cerr << "\t\tFAILURE" << endl;
		else
			cerr << "\t\tok" << endl;
		PR_FREEIF(in);
		PR_FREEIF(tout);
		PR_FREEIF(tret);
		PR_FREEIF(bout);
		PR_FREEIF(bret);
	}

	{
		cerr << "\tTest PRUnichar * with size" << endl;
		PRUnichar *in= 0, *tout= 0, *tret= 0, *bout= 0, *bret= 0;
		PRUint32 inl, toutl, boutl;
		PRUnichar *tmp= ToNewUnicode(NS_LITERAL_STRING("A sizeable UNICODE"));
		inl= nsDependentString(tmp).Length();
		in= (PRUnichar *) PR_MALLOC(sizeof(PRUnichar)* inl);
		memcpy(in, tmp, sizeof(PRUnichar)* inl);
		PR_FREEIF(tmp);
		
		RESCHECK(TestWCharStrWithSize(in, inl, &tout, &toutl, &tret));
		RESCHECK(buddy->TestWCharStrWithSize(in, inl, &bout, &boutl, &bret));
		if (
			inl!= boutl || inl!= toutl ||
			!nsDependentString(tout, toutl).Equals(nsDependentString(bout, boutl)) ||
			!nsDependentString(tret).Equals(bret)
			)
			cerr << "\t\tFAILURE" << endl;
		else
			cerr << "\t\tok" << endl;
		PR_FREEIF(in);
		PR_FREEIF(tout);
		PR_FREEIF(tret);
		PR_FREEIF(bout);
		PR_FREEIF(bret);
	}

	{
		cerr << "\tTest IIDRef" << endl;
		nsIID *in= 0, *tout= 0, *tret= 0, *bout= 0, *bret= 0;
		in= new nsIID(NS_GET_IID(nsISupports));
		
		RESCHECK(TestIIDRef(*in, &tout, &tret));
		RESCHECK(buddy->TestIIDRef(*in, &bout, &bret));
		if (
			!tout->Equals(*bout) || !tret->Equals(*bret) ||
			!in->Equals(*bout)
			)
			cerr << "\t\tFAILURE" << endl;
		else
			cerr << "\t\tok" << endl;

		delete in;
		delete tout;
		delete bout;
		delete tret;
		delete bret;

		cerr << "\tTest IIDPtr" << endl;
		in= new nsIID(NS_GET_IID(jcIMarshallerTest));
		
		RESCHECK(TestIIDPtr(in, &tout, &tret));
		RESCHECK(buddy->TestIIDPtr(in, &bout, &bret));
		if (
			!tout->Equals(*bout) || !tret->Equals(*bret) ||
			!in->Equals(*bout)
			)
			cerr << "\t\tFAILURE" << endl;
		else
			cerr << "\t\tok" << endl;

		delete in;
		delete tout;
		delete bout;
		delete tret;
		delete bret;
	}
	
	{
		cerr << "\tTest primitive array (int)" << endl;
		int src[4]= {800822, 790506, 830520, 820214};
		int *tout= 0, *bout= 0;
		PRUint32 inl= 4, toutl, boutl;
		
		RESCHECK(TestIntArray(src, inl, &tout, &toutl));
		RESCHECK(buddy->TestIntArray(src, inl, &bout, &boutl));
		
		bool worked= (toutl== inl && inl== boutl && bout!= 0);
		for (PRUint32 i= 0; worked && i< inl; ++i)
			if (tout[i]!= bout[i] || bout[i]!= src[i])
				worked= false;
		if (worked)
			cerr << "\t\tok" << endl;
		else
			cerr << "\t\tFAILURE" << endl;

		PR_FREEIF(tout);
		PR_FREEIF(bout);
	}

	{
		cerr << "\tTest char * array" << endl;
		const char* src[4]= {"Hello", "Sweet", "Java", "Bridge"};
		char **tout= 0, **bout= 0;
		PRUint32 inl= 4, toutl, boutl;
		
		RESCHECK(TestCharStrArray(src, inl, &tout, &toutl));
		RESCHECK(buddy->TestCharStrArray(src, inl, &bout, &boutl));
		
		bool worked= (toutl== inl && inl== boutl && bout!= 0);
		for (PRUint32 i= 0; worked && i< inl; ++i)
			if (PL_strcmp(tout[i], bout[i])!= 0 || PL_strcmp(bout[i], src[i])!= 0)
				worked= false;
		if (worked)
			cerr << "\t\tok" << endl;
		else
			cerr << "\t\tFAILURE" << endl;

		for (PRUint32 i= 0; i< inl; ++i) {
			PR_FREEIF(tout[i]);
			PR_FREEIF(bout[i]);
		}
		PR_FREEIF(tout);
		PR_FREEIF(bout);
	}
	{
		cerr << "\tTest PRUnichar * array" << endl;
		PRUnichar* src[4];
		src[0]= ToNewUnicode(NS_LITERAL_STRING("Iskam znachi, iskam"));
		src[1]= ToNewUnicode(NS_LITERAL_STRING("Da si piina edna"));
		src[2]= ToNewUnicode(NS_LITERAL_STRING("Mnogo, Mnogo"));
		src[3]= ToNewUnicode(NS_LITERAL_STRING("Ledena bira"));
		PRUnichar **tout= 0, **bout= 0;
		PRUint32 inl= 4, toutl, boutl;
		
		RESCHECK(TestWCharStrArray((const PRUnichar **) src, inl, &tout, &toutl));
		RESCHECK(buddy->TestWCharStrArray((const PRUnichar **) src, inl, &bout, &boutl));
		
		bool worked= (toutl== inl && inl== boutl && bout!= 0);
		for (PRUint32 i= 0; worked && i< inl; ++i)
			if (!nsDependentString(tout[i]).Equals(bout[i]) ||
				!nsDependentString(bout[i]).Equals(src[i]))
				worked= false;
		if (worked)
			cerr << "\t\tok" << endl;
		else
			cerr << "\t\tFAILURE" << endl;

		for (PRUint32 i= 0; i< inl; ++i) {
			PR_FREEIF(src[i]);
			PR_FREEIF(tout[i]);
			PR_FREEIF(bout[i]);
		}
		PR_FREEIF(tout);
		PR_FREEIF(bout);
	}
	{
		cerr << "\tTest IIDPtr array" << endl;
		nsIID* src[4];
		src[0]= new nsIID(NS_GET_IID(nsISupports));
		src[1]= new nsIID(NS_GET_IID(nsIVariant));
		src[2]= new nsIID(NS_GET_IID(nsIFile));
		src[3]= new nsIID(NS_GET_IID(jcIMarshallerTest));
		nsIID **tout= 0, **bout= 0;
		PRUint32 inl= 4, toutl, boutl;
		
		RESCHECK(TestIIDPtrArray((const nsIID **) src, inl, &tout, &toutl));
		RESCHECK(buddy->TestIIDPtrArray((const nsIID **) src, inl, &bout, &boutl));
		
		bool worked= (toutl== inl && inl== boutl && bout!= 0);
		for (PRUint32 i= 0; worked && i< inl; ++i)
			if (!tout[i]->Equals(*bout[i]) ||
				!bout[i]->Equals(*src[i]))
				worked= false;
		if (worked)
			cerr << "\t\tok" << endl;
		else
			cerr << "\t\tFAILURE" << endl;

		for (PRUint32 i= 0; i< inl; ++i) {
			delete src[i];
			delete tout[i];
			delete bout[i];
		}
		PR_FREEIF(tout);
		PR_FREEIF(bout);
	}
	
	// Test to see whther we are leaking refrences on conversion
	{
		cerr << "\tTest ISupports" << endl;
		nsISupports *in= 0, *tout= 0, *tret= 0, *bout= 0, *bret= 0;
		
		QueryInterface(NS_GET_IID(nsISupports), (void **) &in);
		
		in->AddRef();
		cerr << "\t\tBefore ref count: " << in->Release() << endl;

		for (int i= 0; i< 10; ++i)
		{

			RESCHECK(TestISupports(in, &tout, &tret));
			RESCHECK(buddy->TestISupports(in, &bout, &bret));
			if (tout!= bout || tret!= bret || bout!= in)
				cerr << "\t\tFAILURE" << endl;
			else
				cerr << "\t\tok" << endl;
		
			NS_RELEASE(tout);
			NS_RELEASE(bout);
			NS_RELEASE(tret);
			NS_RELEASE(bret);
		}
		in->AddRef();
		cerr << "\t\tAfter ref count: " << in->Release() << endl;
		NS_RELEASE(in);
	}
	{
		cerr << "\tTest ISupports with IID" << endl;
		nsISupports *in= 0, *tout= 0, *tret= 0, *bout= 0, *bret= 0;
		nsIID *iidin, *tiidout, *biidout;
		iidin= new nsIID(NS_GET_IID(jcIMarshallerTest));
		
		QueryInterface(NS_GET_IID(jcIMarshallerTest), (void **) &in);
		
		in->AddRef();
		cerr << "\t\tBefore ref count: " << in->Release() << endl;

		for (int i= 0; i< 10; ++i)
		{

			RESCHECK(TestISupportsWithIID(in, iidin, &tout, &tiidout, &tret));
			RESCHECK(buddy->TestISupportsWithIID(in, iidin, &bout, &biidout, &bret));
			if (tout!= bout || tret!= bret || bout!= in ||
				!iidin->Equals(*tiidout) || !iidin->Equals(*biidout))
				cerr << "\t\tFAILURE" << endl;
			else
				cerr << "\t\tok" << endl;
		
			NS_RELEASE(tout);
			NS_RELEASE(bout);
			NS_RELEASE(tret);
			NS_RELEASE(bret);
			delete tiidout;
			delete biidout;
		}
		in->AddRef();
		cerr << "\t\tAfter ref count: " << in->Release() << endl;
		NS_RELEASE(in);
		delete iidin;
	}
	{
		cerr << "\tTest ISupports array " << endl;
		nsISupports **in= 0, **tout= 0, **bout= 0;
		PRUint32 inl, boutl, toutl;
		
		inl= 50;
		
		in= (nsISupports **) PR_MALLOC(sizeof(nsISupports *)* inl);
		
		for (PRUint32 i= 0; i< inl; ++i)
			QueryInterface(NS_GET_IID(nsISupports), (void **) in+ i);
		
		in[0]->AddRef();
		cerr << "\t\tBefore ref count: " << in[0]->Release() << endl;

		for (int testcnt= 0; testcnt< 10; ++testcnt)
		{

			RESCHECK(TestISupportsArray(in, inl, &tout, &toutl));
			RESCHECK(buddy->TestISupportsArray(in, inl, &bout, &boutl));
			
			bool worked= (inl== toutl && inl== boutl);
			for (PRUint32 i= 0; worked && i< inl; ++i)
				if (in[i]!= bout[i] || in[i]!= tout[i])
					worked= false;
			if (!worked)
				cerr << "\t\tFAILURE" << endl;
			else
				cerr << "\t\tok" << endl;
		
			for (PRUint32 i= 0; i< inl; ++i) {
				NS_RELEASE(tout[i]);
				NS_RELEASE(bout[i]);
			}
		}
		in[0]->AddRef();
		cerr << "\t\tAfter ref count: " << in[0]->Release() << endl;
		for (PRUint32 i= 0; i< inl; ++i)
			NS_RELEASE(in[i]);
	}
	{
		cerr << "\tTest Variant" << endl;

		nsCOMPtr<nsIVariant> in= 0, tout= 0, tret= 0, bout= 0, bret= 0;
		
		in= do_CreateInstance("@mozilla.org/variant;1");
		nsCOMPtr<nsIWritableVariant> inw= do_QueryInterface(in);
		
		inw->SetWritable(PR_TRUE);
		
		cerr << "\t\tdouble as variant" << endl;
		inw->SetAsDouble(3.1415);

		RESCHECK(TestIVariant(in, getter_AddRefs(tout), getter_AddRefs(tret)));
		RESCHECK(buddy->TestIVariant(in, getter_AddRefs(bout), getter_AddRefs(bret)));
		{
			double toval, boval, trval, brval;
			tout->GetAsDouble(&toval);
			tret->GetAsDouble(&trval);
			bout->GetAsDouble(&boval);
			bret->GetAsDouble(&brval);
			if (toval!= boval || trval!= brval || boval!= (double) 3.1415)
				cerr << "\t\tFAILURE" << endl;
			else
				cerr << "\t\tok" << endl;
		}
			
		
		cerr << "\t\tstring as variant" << endl;
		inw->SetAsString("Did I make it through?");

		RESCHECK(TestIVariant(in, getter_AddRefs(tout), getter_AddRefs(tret)));
		RESCHECK(buddy->TestIVariant(in, getter_AddRefs(bout), getter_AddRefs(bret)));
		{
			char *toval, *boval, *trval, *brval;
			tout->GetAsString(&toval);
			tret->GetAsString(&trval);
			bout->GetAsString(&boval);
			bret->GetAsString(&brval);
			if (PL_strcmp(toval, boval)!= 0 || PL_strcmp(trval, brval)!= 0
				|| PL_strcmp(boval, "Did I make it through?")!= 0)
				cerr << "\t\tFAILURE" << endl;
			else
				cerr << "\t\tok" << endl;
		}
	}
/*
TestIVariant(nsIVariant *pin, nsIVariant **pout, nsIVariant **_retval) {
*/

	return NS_OK;	
}

/* boolean testBoolean (in boolean pin, out boolean pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestBoolean(PRBool pin, PRBool *pout, PRBool *_retval) {
	*pout= pin;
	*_retval= pin;
    return NS_OK;
}

/* octet testByte (in octet pin, out octet pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestByte(PRUint8 pin, PRUint8 *pout, PRUint8 *_retval) {
	*pout= pin;
	*_retval= pin;
    return NS_OK;
}

/* short testShort (in short pin, out short pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestShort(PRInt16 pin, PRInt16 *pout, PRInt16 *_retval) {
	*pout= pin;
	*_retval= pin;
    return NS_OK;
}

/* wchar testChar (in wchar pin, out wchar pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestChar(PRUnichar pin, PRUnichar *pout, PRUnichar *_retval) {
	*pout= pin;
	*_retval= pin;
    return NS_OK;
}

/* long testInt (in long pin, out long pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestInt(PRInt32 pin, PRInt32 *pout, PRInt32 *_retval) {
	*pout= pin;
	*_retval= pin;
    return NS_OK;
}

/* long long testLong (in long long pin, out long long pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestLong(PRInt64 pin, PRInt64 *pout, PRInt64 *_retval) {
	*pout= pin;
	*_retval= pin;
    return NS_OK;
}

/* string testCharStr (in string pin, out string pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestCharStr(const char *pin, char **pout, char **_retval) {
	*pout= PL_strdup(pin);
	*_retval= PL_strdup(pin);
    return NS_OK;
}

/* wstring testWCharStr (in wstring pin, out wstring pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestWCharStr(const PRUnichar *pin, PRUnichar **pout, PRUnichar **_retval) {
	nsString temp;
	temp= nsDependentString(pin);
	*pout= ToNewUnicode(temp);
	*_retval= ToNewUnicode(temp);
	return NS_OK;
}

/* AString testAString (in AString pin, out AString pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestAString(const nsAString & pin, nsAString & pout, nsAString & _retval) {
	pout= pin;
	_retval= pin;
    return NS_OK;
}

/* DOMString testDOMString (in DOMString pin, out DOMString pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestDOMString(const nsAString & pin, nsAString & pout, nsAString & _retval) {
	pout= pin;
	_retval= pin;
    return NS_OK;
}

/* ACString testCString (in ACString pin, out ACString pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestCString(const nsACString & pin, nsACString & pout, nsACString & _retval) {
	pout= pin;
	_retval= pin;
    return NS_OK;
}

/* AUTF8String testUTF8String (in AUTF8String pin, out AUTF8String pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestUTF8String(const nsACString & pin, nsACString & pout, nsACString & _retval) {
	pout= pin;
	_retval= pin;
    return NS_OK;
}

/* string testCharStrWithSize ([size_is (inl)] in string pin, in unsigned long inl, [size_is (outl)] out string pout, out unsigned long outl); */
NS_IMETHODIMP jcMarshallerTestImplC::TestCharStrWithSize(const char *pin, PRUint32 inl, char **pout, PRUint32 *outl, char **_retval) {
	*pout= (char *) PR_MALLOC(inl* sizeof(char));
	memcpy(*pout, pin, inl* sizeof(char));
	*outl= inl;
	*_retval= (char *) PR_MALLOC((inl+ 1)* sizeof(char));
	memcpy(*_retval, pin, inl* sizeof(char));
	(*_retval)[inl]= 0;
    return NS_OK;
}

/* wstring testWCharStrWithSize ([size_is (inl)] in wstring pin, in unsigned long inl, [size_is (outl)] out wstring pout, out unsigned long outl); */
NS_IMETHODIMP jcMarshallerTestImplC::TestWCharStrWithSize(const PRUnichar *pin, PRUint32 inl, PRUnichar **pout, PRUint32 *outl, PRUnichar **_retval) {
	*pout= (PRUnichar *) PR_MALLOC(inl* sizeof(PRUnichar));
	memcpy(*pout, pin, inl* sizeof(PRUnichar));
	*outl= inl;
	*_retval= (PRUnichar *) PR_MALLOC((inl+ 1)* sizeof(PRUnichar));
	memcpy(*_retval, pin, inl* sizeof(PRUnichar));
	(*_retval)[inl]= 0;
    return NS_OK;
}

/* nsIIDRef testIIDPtr (in nsIIDRef pin, out nsIIDPtr pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestIIDRef(const nsIID & pin, nsIID **pout, nsIID **_retval) {
	*pout= new nsIID(pin);
	*_retval= new nsIID(pin);
    return NS_OK;
}

/* nsIIDPtr testIIDPtr (in nsIIDPtr pin, out nsIIDPtr pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestIIDPtr(const nsIID * pin, nsIID * *pout, nsIID * *_retval) {
	*pout= new nsIID(*pin);
	*_retval= new nsIID(*pin);
    return NS_OK;
}

/* void testIntArray ([array, size_is (inl)] in long pin, in unsigned long inl, [array, size_is (outl)] out long pout, out unsigned long outl); */
NS_IMETHODIMP jcMarshallerTestImplC::TestIntArray(PRInt32 *pin, PRUint32 inl, PRInt32 **pout, PRUint32 *outl) {
	*pout= (PRInt32 *) PR_MALLOC(inl* sizeof(PRInt32));
	memcpy(*pout, pin, inl* sizeof(PRInt32));
	*outl= inl;
    return NS_OK;
}

/* void testCharStrArray ([array, size_is (inl)] in string pin, in unsigned long inl, [array, size_is (outl)] out string pout, out unsigned long outl); */
NS_IMETHODIMP jcMarshallerTestImplC::TestCharStrArray(const char **pin, PRUint32 inl, char ***pout, PRUint32 *outl) {
	*pout= (char **) PR_MALLOC(inl* sizeof(char *));
	for (PRUint32 i= 0; i< inl; ++i)
		(*pout)[i]= PL_strdup(pin[i]);
	*outl= inl;
	return NS_OK;
}

/* void testWCharStrArray ([array, size_is (inl)] in wstring pin, in unsigned long inl, [array, size_is (outl)] out wstring pout, out unsigned long outl); */
NS_IMETHODIMP jcMarshallerTestImplC::TestWCharStrArray(const PRUnichar **pin, PRUint32 inl, PRUnichar ***pout, PRUint32 *outl) {
	*pout= (PRUnichar **) PR_MALLOC(inl* sizeof(PRUnichar *));
	for (PRUint32 i= 0; i< inl; ++i)
		(*pout)[i]= ToNewUnicode(nsDependentString(pin[i]));
	*outl= inl;
	return NS_OK;
}

/* void testIIDPtrArray ([array, size_is (inl)] in nsIIDPtr pin, in unsigned long inl, [array, size_is (outl)] out nsIIDPtr pout, out unsigned long outl); */
NS_IMETHODIMP jcMarshallerTestImplC::TestIIDPtrArray(const nsIID * *pin, PRUint32 inl, nsIID * **pout, PRUint32 *outl) {
	*pout= (nsIID **) PR_MALLOC(inl* sizeof(nsIID *));
	for (PRUint32 i= 0; i< inl; ++i)
		(*pout)[i]= new nsIID(*pin[i]);
	*outl= inl;
    return NS_OK;
}

/* nsISupports testISupports (in nsISupports pin, out nsISupports pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestISupports(nsISupports *pin, nsISupports **pout, nsISupports **_retval) {
	*pout= pin;
	NS_IF_ADDREF(*pout);
	*_retval= pin;
	NS_IF_ADDREF(*_retval);
	return NS_OK;
}

/* nsISupports testISupportsWithIID ([iid_is (iniid)] in nsISupports pin, in nsIIDPtr iniid, [iid_is (outiid)] out nsISupports pout, out nsIIDPtr outiid); */
NS_IMETHODIMP jcMarshallerTestImplC::TestISupportsWithIID(nsISupports *pin, const nsIID * iniid, nsISupports **pout, nsIID * *outiid, nsISupports **_retval) {
	*outiid= new nsIID(*iniid);
	*pout= pin;
	NS_IF_ADDREF(*pout);
	return pin->QueryInterface(*iniid, (void **) _retval);
}

/* void testISupportsArray ([array, size_is (inl)] in nsISupports pin, in unsigned long inl, [array, size_is (outl)] out nsISupports pout, out unsigned long outl); */
NS_IMETHODIMP jcMarshallerTestImplC::TestISupportsArray(nsISupports **pin, PRUint32 inl, nsISupports ***pout, PRUint32 *outl) {
	*pout= (nsISupports **) PR_MALLOC(inl* sizeof(nsISupports *));
	for (PRUint32 i= 0; i< inl; ++i) {
		(*pout)[i]= pin[i];
		NS_IF_ADDREF((*pout)[i]);
	}
	*outl= inl;
    return NS_OK;
}

/* nsIVariant testIVariant (in nsIVariant pin, out nsIVariant pout); */
NS_IMETHODIMP jcMarshallerTestImplC::TestIVariant(nsIVariant *pin, nsIVariant **pout, nsIVariant **_retval) {
	*pout= pin;
	NS_IF_ADDREF(*pout);
	*_retval= pin;
	NS_IF_ADDREF(*_retval);
	return NS_OK;
}
