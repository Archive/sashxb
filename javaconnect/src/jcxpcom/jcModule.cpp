/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * The contents of this file are subject to the Mozilla Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Sun Microsystems,
 * Inc. Portions created by Sun are
 * Copyright (C) 1999 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * Contributor(s):
 * Stefan Atev <atevstef@luther.edu> - IBM Internet Technology Group
 * Igor Kushnirskiy <idk@eng.sun.com>
 */

#include <fstream.h>
#include "nsCRT.h"
#include "nsIFile.h"
#include "nsIAllocator.h"
#include "nsAString.h"
#include "nsSharableString.h"
#include "nsCOMPtr.h"
#include "jcModule.h"
#include "jcFactory.h"
#include "jcServices.h"
#include "nsIComponentRegistrar.h"
#include "nsReadableUtils.h"
#include "plstr.h"
#include "prmem.h"

NS_IMPL_ISUPPORTS1(jcModule, nsIModule);

jcModule::jcModule() : mLocation(0) {
    NS_INIT_ISUPPORTS();    
}

jcModule::~jcModule() {
	PR_FREEIF(mLocation);
}

NS_IMETHODIMP jcModule::GetClassObject(
	nsIComponentManager *aCompMgr,
	const nsCID &aClass,
	const nsIID &aIID,
	void * *result) {
		
    if (!result)
    	return NS_ERROR_NULL_POINTER;
	return aCompMgr->GetClassObject(aClass, aIID, result);
}

NS_IMETHODIMP jcModule::RegisterSelf(
	nsIComponentManager *aCompMgr,
	nsIFile *_location,
	const char *registryLocation,
	const char *componentType) {
	
    nsSharableString str;
    _location->GetPath(str);
    PR_FREEIF(mLocation);
    mLocation= ToNewCString(str);
    
    // Get the directory of the module - and add to classpath
    nsresult res;
    nsCOMPtr<nsIComponentRegistrar> registrar(do_QueryInterface(aCompMgr, &res));
    if (NS_FAILED(res))
    	return res;

	// Obtain module CID, contractID, description (possibly from .jar file)
	// and then rock and roll further
    // nsCID cid;
    
	// ATEV: DO NOT FORGET ERROR CHECKING AND JAR FILE SUPPORT

	ifstream in(mLocation);
	char className[1024], cidSTR[1024], contractID[1024], moduleDescription[1024];

	// First line describes the module
	in.getline(moduleDescription, 1024);
	
	// Second line is an integer - the number of exported classes
	// (records)
	int classCnt= 0;
	in >> classCnt;
	// essentially ISFX
	in.getline(className, 1024);

	for (int i= 0; i< classCnt; ++i) {
		// Class name (First line of record)
		in.getline(className, 1024);
		// Then ClassID (Second line of record)
		in.getline(cidSTR, 1024);
		nsCID classCID;
		classCID.Parse((const char *) cidSTR);
		// Then contractID (Third line of record)
		in.getline(contractID, 1024);
		
    	nsIFactory *jcf= 0;
    	nsresult res= aCompMgr->CreateInstance(
    		kjcFactoryCID,
    		0,
    		NS_GET_IID(nsIFactory),
    		(void **) &jcf
    	);
    	
    	if (NS_FAILED(res))
    		return res;

    	nsCOMPtr<jcIFactory> jcif= do_QueryInterface(jcf, &res);
    	if (NS_FAILED(res)) {
    		NS_RELEASE(jcf);
    		return res;
    	}

    	jcClassData *classData= new jcClassData(
    		className,
    		contractID,
    		classCID
    	);
    	
	    nsCOMPtr<jcIServices> jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
	    // Do not forget to remove yourself from the Runtime Wrapper Table and delete
	    // the global ref to mobj
	    if (NS_SUCCEEDED(res))
	    	jcs->AddClassData(classData);
	    jcs= 0;
    		
    	res= jcif->SetClassData(classData);
    	if (NS_FAILED(res)) {
    		NS_RELEASE(jcf);
    		jcif= 0;
    		return res;
    	}
    	jcif= 0;

    	res= registrar->RegisterFactory(
    		classCID,
    		className,
    		contractID,
    		jcf
    	);
    	
    	if (NS_FAILED(res)) {
    		NS_RELEASE(jcf);
    		return res;
    	}
    }
    return NS_OK;
}

NS_IMETHODIMP jcModule::UnregisterSelf(
	nsIComponentManager *aCompMgr,
	nsIFile *_location,
	const char *registryLocation) {
	
	// ATEV: Add unregister code
	// Apparently BlackConnect was not doing anything here, but intended to
    return NS_OK;
}

NS_IMETHODIMP jcModule::CanUnload(nsIComponentManager *aCompMgr, PRBool *_retval) {

	// ATEV: Can we always unload? What are the implications of that?
    if (!_retval)
    	return NS_ERROR_NULL_POINTER;
    *_retval= PR_TRUE;
    return NS_OK;
}
