#ifndef _JC_MODULE_H
#define _JC_MODULE_H

/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * The contents of this file are subject to the Mozilla Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Sun Microsystems,
 * Inc. Portions created by Sun are
 * Copyright (C) 1999 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * Contributor(s):
 * Stefan Atev <atevstef@luther.edu> - IBM Internet Technology Group
 * Igor Kushnirskiy <idk@eng.sun.com>
 */

#include "nsIModule.h"

// c999e21d-a890-4a15-a280-a64ad4bc391b
#define JCMODULE_CID \
	{0xc999e21d, 0xa890, 0x4a15, {0xa2, 0x80, 0xa6, 0x4a, 0xd4, 0xbc, 0x39, 0x1b}}

NS_DEFINE_CID(kjcModuleCID, JCMODULE_CID);

#define JCMODULE_CONTRACT_ID "@mozilla.org/javaconnect/module;1"

class jcModule: public nsIModule {
	public:
    	NS_DECL_ISUPPORTS
    	NS_DECL_NSIMODULE

    	jcModule();
    	virtual ~jcModule();
    
	protected:
    	char *mLocation;
    	
};
#endif
