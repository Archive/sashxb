#ifndef _JC_MARSHALLER_TEST_IMPLC_H
#define _JC_MARSHALLER_TEST_IMPLC_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "jcIMarshallerTest.h"

// 319ea598-1e91-42d8-bd1f-6e3db5aa1779
#define JCMARSHALLERTESTIMPLC_CID \
	{0x319ea598, 0x1e91, 0x42d8, {0xbd, 0x1f, 0x6e, 0x3d, 0xb5, 0xaa, 0x17, 0x79}}

NS_DEFINE_CID(kjcMarshallerTestImplCCID, JCMARSHALLERTESTIMPLC_CID);

#define JCMARSHALLERTESTIMPLC_CONTRACT_ID "@mozilla.org/javaconnect/marshallerTestC;1"

class jcMarshallerTestImplC: public jcIMarshallerTest {

	public:

		NS_DECL_ISUPPORTS
		NS_DECL_JCIMARSHALLERTEST

		jcMarshallerTestImplC();
		virtual ~jcMarshallerTestImplC();
};

#endif
