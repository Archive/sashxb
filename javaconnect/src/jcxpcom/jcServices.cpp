
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "jcServices.h"
#include "jcWrappedJavaObject.h"
#include "nsComponentManagerUtils.h"
#include "prmem.h"
#include "prthread.h"
#include "plevent.h"
#include "nsIEventQueueService.h"
#include "jcXPCall.h"
#include "jcConvert.h"
#include "jcError.h"

#include <vector>
#include <string>

NS_IMPL_THREADSAFE_ISUPPORTS1(jcServices, jcIServices)

jcServices::jcServices() {
	NS_INIT_ISUPPORTS();

	mWrappers= new jcWrappersTable();
	mNatives= new jcNativesTable();
	mClassData= new jcClassDataTable();
	mClasses= new jcClassTable();
}

jcServices::~jcServices() {
	if (mWrappers)
		delete mWrappers;
	mWrappers= 0;
	if (mNatives)
		delete mNatives;
	mNatives= 0;
	if (mClassData)
		delete mClassData;
	mClassData= 0;
	if (mClasses)
		delete mClasses;
	mClassData= 0;
}

NS_IMETHODIMP jcServices::CreateWrappedJavaObject(JNIEnv * env, jcClassData *cd, const nsIID &iid, nsISupports **_retval) {
	bool detach= false; // so that supplied env does not detach
	nsresult res= JC_ERROR_CANT_WRAP_OBJECT;

	if (!env) // get your own env
		detach= jcGetEnv(&env);
	if (!env)
		return JC_ERROR_CANT_GET_JNI_ENVIRONMENT;
	
	jobject obj= JC_NewObject(env, cd->getClassName());
	if (!obj) {
		env->ExceptionDescribe();
		jcDropEnv(env, detach);
		return JC_ERROR_CANT_CREATE_OBJECT;
	};
	res= WrapJavaObject(env, obj, iid, _retval);

	// obj has been globally referenced by WrapJavaObject
	env->DeleteLocalRef(obj);
	jcDropEnv(env, detach);
	return res;
}

NS_IMETHODIMP jcServices::WrapJavaObject(JNIEnv *env, jobject obj, const nsIID &iid, nsISupports **_retval) {
	if (!_retval)
		return NS_ERROR_NULL_POINTER;

    jcWrappedJavaObject *proxy= 0;
    
    proxy= (jcWrappedJavaObject *) mWrappers->Lookup(obj);
    if (proxy) {
    	nsresult res= proxy->QueryInterface(iid, (void **) _retval);
    	return res;
    }

	bool detach= false;
	if (!env)
		detach= jcGetEnv(&env);
	if (!env)
		return JC_ERROR_CANT_GET_JNI_ENVIRONMENT;

	char *className= 0;
	GetJavaObjectClassName(env, obj, &className);
	if (!className)
		return JC_ERROR_CANT_GET_CLASS_NAME;

	jcClassData *cd= (jcClassData *) mClassData->Lookup(className);
	PR_FREEIF(className);

	if (!cd)
		return JC_ERROR_CANT_GET_CLASS_DATA;

	proxy= new jcWrappedJavaObject(cd);
    if (!proxy)
    	return JC_ERROR_CANT_CREATE_OBJECT_WRAPPER;
		
	jobject gobj= env->NewGlobalRef(obj);
	if (!gobj) {
		env->ExceptionClear();
		jcDropEnv(env, detach);
		delete proxy;
		return JC_ERROR_CANT_REFERENCE;
	}
	proxy->mobj= gobj;
	mWrappers->Add(gobj, proxy);
	nsresult res= proxy->QueryInterface(iid, (void **) _retval);

	if (NS_FAILED(res)) {
		env->DeleteGlobalRef(gobj);
		delete proxy;
	}

	jcDropEnv(env, detach);
	return res;
}

NS_IMETHODIMP jcServices::RemoveJavaObject(JNIEnv *env, jobject obj) {
	mWrappers->Remove(obj);
	bool detach= false;
	if (!env)
		detach= jcGetEnv(&env);
	if (!env)
		return JC_ERROR_CANT_GET_JNI_ENVIRONMENT;
	env->DeleteGlobalRef(obj);
	jcDropEnv(env, detach);
    return NS_OK;
}

NS_IMETHODIMP jcServices::GetClassInfoForClassName(JNIEnv *env, const char *className, PRUint32 *count, nsIID * **array) {

	if (!array)
		return NS_ERROR_NULL_POINTER;
	*array= 0;

	vector<string> iidList;
	iidList= jcGetInterfaceIDsForClass(env, className);

	*count= iidList.size();
	if (*count== 0)
		return NS_OK;

	nsIID **result= 0;
	result= (nsIID **) PR_MALLOC(sizeof(nsIID *)* (*count));
	
	for (unsigned int j= 0; j< *count; ++j) {
		result[j]= new nsIID();
		result[j]->Parse(iidList[j].c_str());
	}
	*array= result;
    return NS_OK;
}

NS_IMETHODIMP jcServices::GetJavaObjectClassName(JNIEnv *env, jobject obj, char **_retval) {
	if (!_retval)
		return NS_ERROR_NULL_POINTER;

	bool detach= false;
	if (!env)
		detach= jcGetEnv(&env);
	if (!env)
		return JC_ERROR_CANT_GET_JNI_ENVIRONMENT;
	
	jclass objClass= env->GetObjectClass(obj);
	if (!objClass) {
		env->ExceptionClear();
		jcDropEnv(env, detach);
		return JC_ERROR_CANT_FIND_CLASS;
	}
	
//	jclass classClass= env->FindClass("java/lang/Class");
	jclass classClass= JC_FindClass(env, "java/lang/Class");
	if (!classClass) {
		env->ExceptionClear();
		env->DeleteLocalRef(objClass);
		jcDropEnv(env, detach);
		return JC_ERROR_CANT_FIND_CLASS;
	}
	
	jmethodID getNameMID= env->GetMethodID(classClass, "getName", "()Ljava/lang/String;");
	env->DeleteLocalRef(classClass); // before error check
	if (!getNameMID) {
		env->ExceptionClear();
		env->DeleteLocalRef(objClass);
		jcDropEnv(env, detach);
		return JC_ERROR_CANT_FIND_METHOD;
	}
	
	jstring classNameJStr= (jstring) env->CallObjectMethod(objClass, getNameMID);
	env->DeleteLocalRef(objClass); // before error check
	if (!classNameJStr) {
		env->ExceptionClear();
		jcDropEnv(env, detach);
		return JC_ERROR_CALL_FAILED;
	}
	
	char *classNameCStr;
	int siz;
	nsresult res= J2X_ConvertString(env, nsXPTType::T_CHAR_STR, classNameJStr, siz, &classNameCStr);
	if (NS_FAILED(res)) {
		env->ExceptionClear();
		jcDropEnv(env, detach);
		return res;
	}
	string className= classNameCStr;
	PR_FREEIF(classNameCStr);
	
	className= MapDotToSlash(className);
	*_retval= PL_strdup(className.c_str());
	jcDropEnv(env, detach);
	return NS_OK;
}

NS_IMETHODIMP jcServices::WrapNativeObject(JNIEnv *env, nsISupports *obj, const nsIID &iid, jobject *_retval) {
	if (!_retval)
		return NS_ERROR_NULL_POINTER;
	if (!obj) { // null
		*_retval= 0;
		return NS_OK;
	}

	bool detach= false;
	if (!env)
		detach= jcGetEnv(&env);
	if (!env)
		return JC_ERROR_CANT_GET_JNI_ENVIRONMENT;
	
	jobject iidObj;
	nsresult res= X2J_ConvertIIDObject(env, iid, iidObj);
	if (NS_FAILED(res)) {
		jcDropEnv(env, detach);
		return res;
	}

//	jclass servicesClass= env->FindClass("org/mozilla/xpcom/jcJavaServices");
	jclass servicesClass= JC_FindClass(env, "org/mozilla/xpcom/jcJavaServices");
	if (!servicesClass) {
		env->ExceptionClear();
		env->DeleteLocalRef(iidObj);
		jcDropEnv(env, detach);
		return JC_ERROR_CANT_FIND_CLASS;
	}
	jmethodID wrapMID= env->GetStaticMethodID(servicesClass, "wrapXPCOMInstance", "(JLorg/mozilla/xpcom/IID;)Ljava/lang/Object;");
	if (!wrapMID) {
		env->ExceptionClear();
		env->DeleteLocalRef(servicesClass);
		env->DeleteLocalRef(iidObj);
		jcDropEnv(env, detach);
		return JC_ERROR_CANT_FIND_METHOD;
	}
	jobject jobj= env->CallStaticObjectMethod(servicesClass, wrapMID, reinterpret_cast<jlong>(obj), iidObj);
	env->DeleteLocalRef(servicesClass); //before error check
	env->DeleteLocalRef(iidObj);
	if (!jobj) {
		env->ExceptionDescribe();
		env->ExceptionClear();
		jcDropEnv(env, detach);
		return JC_ERROR_CALL_FAILED;
	}
	// after a successful call the object should be already in natives table
	env->DeleteLocalRef(jobj);
	
	// wrapXPCOMInstance should already have added the wrapped object
	// we are essentially completing a roundtrip though the JVM and back
	GetNativeObjectWrapper(env, obj, iid, _retval);

	jcDropEnv(env, detach);
	return NS_OK;
}

NS_IMETHODIMP jcServices::AddNativeObject(JNIEnv *env, nsISupports *obj, jobject jobj, const nsIID &iid) {
	jcNativeProxySet *pxys;
	pxys= (jcNativeProxySet *) mNatives->Lookup(obj);
	char *iidStr= iid.ToString();
	
	if (!env)
		return NS_ERROR_NULL_POINTER;
	
	if (!pxys) {
		// deallocated when last element is removed or the table dies
		pxys= new jcNativeProxySet();
		mNatives->Add(obj, pxys);
	}
	if (!pxys->safeThread) {
		pxys->safeThread= PR_GetCurrentThread();
	}
	if (pxys->objForIIDWithAdd(iidStr, jobj)) {
		env->DeleteWeakGlobalRef(jobj);
	}
	PR_FREEIF(iidStr);
	return NS_OK;
}

NS_IMETHODIMP jcServices::RemoveNativeObject(JNIEnv *env, nsISupports *obj, const nsIID &iid) {
	if (!env)
		return NS_ERROR_NULL_POINTER;

	jcNativeProxySet *pxys;
	pxys= (jcNativeProxySet *) mNatives->Lookup(obj);
	if (!pxys) // silent failure
		return NS_OK;
	char *iidStr= iid.ToString();
	jobject deadProxy= pxys->objForIIDWithRemove(iidStr);
	PR_FREEIF(iidStr);
	env->DeleteWeakGlobalRef(deadProxy);
	if (pxys->isEmpty()) {
		NS_IF_RELEASE(pxys->eventQueue);
		delete pxys;
		mNatives->Remove(obj);
	}
	NS_IF_RELEASE(obj);
	return NS_OK;
}

NS_IMETHODIMP jcServices::GetNativeObjectWrapper(JNIEnv *env, nsISupports *obj, const nsIID &iid, jobject *_retval) {
	if (!_retval || !env)
		return NS_ERROR_NULL_POINTER;
	jcNativeProxySet *pxys;
	pxys= (jcNativeProxySet *) mNatives->Lookup(obj);
	if (!pxys)
		return JC_ERROR_CANT_FIND_IN_SERVICES_TABLE;

	char *iidStr= iid.ToString();
	jobject result= pxys->objForIID(iidStr);
	PR_FREEIF(iidStr);

	// local ref in out frame
	*_retval= env->NewLocalRef(result);
	if (!(*_retval)) {
		env->ExceptionClear();
		return JC_ERROR_CANT_REFERENCE;
	}
    return NS_OK;
}

NS_IMETHODIMP jcServices::AddClassData(jcClassData * cd) {
	if (!cd)
		return NS_ERROR_NULL_POINTER;
	mClassData->Add(cd->getClassName(), cd);
    return NS_OK;
}

NS_IMETHODIMP jcServices::GetClassData(const char *className, jcClassData * *_retval) {
	if (!_retval)
		return NS_ERROR_NULL_POINTER;
	*_retval= (jcClassData *) mClassData->Lookup(className);
    return NS_OK;
}

static nsresult PerformInfoCall(jcNativeCallInfo *info);
static void* XPCallOnPost(PLEvent *event);
static void PR_CALLBACK XPCallOnDestroy(PLEvent *event);

NS_IMETHODIMP jcServices::PerformNativeCall(jcNativeCallInfo *info) {
	nsresult res;
	if (!info)
		return NS_ERROR_NULL_POINTER;

	jcNativeProxySet *pxys;
	pxys= (jcNativeProxySet *) mNatives->Lookup(info->m_obj);
	if (!pxys)
		return JC_ERROR_CANT_FIND_IN_SERVICES_TABLE;

	if (PR_GetCurrentThread()== pxys->safeThread) {
		res= PerformInfoCall(info);
		delete info;
		return res;
	}
	
    nsCOMPtr<nsIEventQueueService> eqs= do_GetService("@mozilla.org/event-queue-service;1");
    if (!eqs)
		return JC_ERROR_CANT_GET_EVENT_QUEUE_SERVICE;
	if (!pxys->eventQueue) {
		res= eqs->GetThreadEventQueue(pxys->safeThread, &pxys->eventQueue);
		if (NS_FAILED(res)) {
			cerr << "Cannot get thread queue for " << pxys->safeThread << endl;
			return res;
		}
	}
	// This signals to perform info call to get the JNIEnv for
	// the thread it runs in
	info->m_env= 0;

	PLEvent *event= PR_NEW(PLEvent);
	PL_InitEvent(event, info, XPCallOnPost, XPCallOnDestroy);
	void *resFromPost;
    pxys->eventQueue->PostSynchronousEvent(event, &resFromPost);
	res= reinterpret_cast<nsresult>(resFromPost);
	delete info;
	return res;
}

static nsresult PerformInfoCall(jcNativeCallInfo *info) {
	bool detach= false;
	JNIEnv *env= info->m_env;
	if (!env)
		detach= jcGetEnv(&env);
	if (!env)
		return JC_ERROR_CANT_GET_JNI_ENVIRONMENT;
	XPCall *call= new XPCall(info->m_methodName, info->m_interfaceInfo, info->m_methodInfo, info->m_methodIndex);
	nsresult res= call->Init(env, info->m_args);
	if (NS_FAILED(res)) {
		delete call;
		return res;
	}
	res= call->PerformCall(env, info->m_obj, *(info->m_result));
	delete call;
	jcDropEnv(env, detach);
	return res;
}

static void* XPCallOnPost(PLEvent *event) {
	jcNativeCallInfo *info;
	info= (jcNativeCallInfo *) PL_GetEventOwner(event);
	nsresult res= PerformInfoCall(info);
	return reinterpret_cast<void *>(res);
}

static void  PR_CALLBACK XPCallOnDestroy(PLEvent *event) {
}

/* JavaClass FindGlobalClass (in string classname); */
NS_IMETHODIMP jcServices::FindGlobalClass(const char *classname, jclass *_retval) {
	if (!_retval)
		return NS_ERROR_NULL_POINTER;
	*_retval= (jclass) mClasses->Lookup(classname);
    return NS_OK;
}

/* void CacheGlobalClass (in string classname, in JavaClass cls); */
NS_IMETHODIMP jcServices::CacheGlobalClass(const char *classname, jclass cls) {
	mClasses->Add(classname, cls);
    return NS_OK;
}
