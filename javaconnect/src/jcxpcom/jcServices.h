#ifndef _JCSERVICES_H
#define _JCSERVICES_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "jcUtils.h"
#include "jcTables.h"
#include "jcIServices.h"

class jcServices: public jcIServices {

	public:
		NS_DECL_ISUPPORTS
		NS_DECL_JCISERVICES

		jcServices();
		virtual ~jcServices();
		
	private:
	
		/* In the future, map class names to the information required
		by GetInterfaces */
//		jcInterfacesInfoTable *mClassNameToIIDInfo;
		
		/* Map jobjects to their respective wrappers */
		jcWrappersTable *mWrappers;
		jcNativesTable *mNatives;
		jcClassDataTable *mClassData;
		jcClassTable *mClasses;
};

#endif
