#ifndef _JCFACTORY_H
#define _JCFACTORY_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "nsIFactory.h"
#include "jcIFactory.h"

// d06e7800-26ce-40d3-9604-c63ef444f763
#define JCFACTORY_CID \
	{0xd06e7800, 0x26ce, 0x40d3, {0x96, 0x04, 0xc6, 0x3e, 0xf4, 0x44, 0xf7, 0x63}}

NS_DEFINE_CID(kjcFactoryCID, JCFACTORY_CID);

#define JCFACTORY_CONTRACT_ID "@mozilla.org/javaconnect/factory;1"

class jcFactory: public nsIFactory, public jcIFactory {
	public:
    	NS_DECL_ISUPPORTS
    	NS_DECL_NSIFACTORY
    	NS_DECL_JCIFACTORY
    
    	jcFactory();
    	virtual ~jcFactory();
    
	private:
		friend class jcWrappedJavaObject;
		jcClassData *mClassData;

};

#endif
