/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 * The Initial Developer of this code under the MPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1999 Netscape Communications Corporation.  All Rights
 * Reserved.
 *
 * Contributors:
 *   Mike Shaver <shaver@zeroknowledge.com>
 *   John Bandhauer <jband@netscape.com>
 *   IBM Corp.
 *   Robert Ginda <rginda@netscape.com>
 *   Stefan Atev <atevstef@luther.edu> - IBM Internet Technology Group
 */

#include "nsCOMPtr.h"
#include "nsICategoryManager.h"
#include "nsIComponentManager.h"
#include "nsIGenericFactory.h"
#include "nsILocalFile.h"
#include "nsIModule.h"
#include "nsIServiceManager.h"
#include "nsISupports.h"
#include "nsIXPConnect.h"
#include "nsCRT.h"
#include "nsMemory.h"
#include "nsIRegistry.h"
#include "nsXPIDLString.h"
#include "nsIXPCScriptable.h"

#include "nsIScriptError.h"
#include "nsIConsoleService.h"

#include "jcComponentLoader.h"
#include "jcModule.h"
#include "jcFactory.h"
#include "jcServices.h"
//#include "jcMarshallerTestImplC.h"
#include "prenv.h"
#include "prmem.h"
#include "plstr.h"

const char jcComponentLoaderContractID[]= "@mozilla.org/javaconnect/loader;1";
const char jcComponentTypeName[]= "text/javaconnect";

// Don't forget to change the length if you change the extension!
const char jcComponentNameSuffix[]= ".jcm";
const int jcComponentNameSuffixLen= 4;

const char JCfileSizeValueName[]= "FileSize";
const char JClastModValueName[]= "LastModTimeStamp";
const char JCxpcomKeyName[]= "software/mozilla/XPCOM/components";

NS_IMPL_THREADSAFE_ISUPPORTS1(jcComponentLoader, nsIComponentLoader);

jcComponentLoader::jcComponentLoader() :
	mCompMgr(nsnull),
	mModules(0),
	mXPCOMKey(0),
	mInitialized(PR_FALSE),
	mDeferredComponents(0) {

    NS_INIT_ISUPPORTS();
    NS_NewISupportsArray(&mDeferredComponents);
    classPathEnv= 0;
    jcJarPathEnv= 0;
}

jcComponentLoader::~jcComponentLoader() {
    mInitialized= PR_FALSE;
    if (mModules) delete mModules;
    NS_IF_RELEASE(mDeferredComponents);
    mCompMgr= 0;
	PR_FREEIF(classPathEnv);
	PR_FREEIF(jcJarPathEnv);
}

NS_IMETHODIMP
jcComponentLoader::GetFactory(
	const nsIID &aCID,
	const char *aLocation,
	const char *aType,
	nsIFactory **_retval) {

    if (!_retval)
    	return NS_ERROR_NULL_POINTER;

    nsIModule *module= ModuleForLocation(aLocation, 0);
    if (!module)
    	return NS_ERROR_FACTORY_NOT_LOADED;

    nsresult res= module->GetClassObject(
    	mCompMgr, aCID,
		NS_GET_IID(nsIFactory),
		(void **)_retval
	);
    return res;
}

NS_IMETHODIMP
jcComponentLoader::Init(nsIComponentManager *aCompMgr, nsISupports *aReg) {
	
    mCompMgr= aCompMgr;
    nsresult res;

    mRegistry= do_QueryInterface(aReg, &res);
    if (NS_SUCCEEDED(res)) {
        res= mRegistry->GetSubtree(nsIRegistry::Common, JCxpcomKeyName, &mXPCOMKey);
        if (NS_FAILED(res))
        	mRegistry= 0; // silently ignore all registry ops
    }
    mModules= new jcModulesTable(mCompMgr);
    if (!mModules)
    	return NS_ERROR_OUT_OF_MEMORY;

	mInitialized= PR_TRUE;

	char *t_classPathEnv= PR_GetEnv("CLASSPATH");
	string cpath= "";
	if (t_classPathEnv)
		cpath= t_classPathEnv;
	char *homedir= PR_GetEnv("HOME");
	if (homedir && *homedir)
		cpath= string(homedir)+ "/.sash/components/javaconnect.jar:"+
			string(homedir)+ "/.sash/components/sashspecific.jar:"+
			string(homedir)+ "/.sash/components/imported.jar:"+
			cpath;
	PR_FREEIF(homedir);
	cpath= string("CLASSPATH=")+ cpath;
	t_classPathEnv= PL_strdup(cpath.c_str());
	PR_SetEnv(t_classPathEnv);
	PR_FREEIF(classPathEnv);
	classPathEnv= t_classPathEnv;

//------------------------------------
#if 0
	char *t_jcJarPathEnv= PR_GetEnv("JAVACONNECTJARPATH");
	string jpath= "";
	if (t_jcJarPathEnv)
		jpath= t_jcJarPathEnv;
	homedir= PR_GetEnv("HOME");
	if (homedir && *homedir)
		jpath= string(homedir)+ "/.sash/components/imported.jar:"+ jpath;
	PR_FREEIF(homedir);
	jpath= string("JAVACONNECTJARPATH=")+ jpath;
	t_jcJarPathEnv= PL_strdup(jpath.c_str());
	PR_SetEnv(t_jcJarPathEnv);
	PR_FREEIF(jcJarPathEnv);
	jcJarPathEnv= t_jcJarPathEnv;
#endif
    return NS_OK;
}

NS_IMETHODIMP
jcComponentLoader::AutoRegisterComponents(PRInt32 when, nsIFile *aDirectory) {
    return RegisterComponentsInDir(when, aDirectory);
}

nsresult
jcComponentLoader::RegisterComponentsInDir(PRInt32 when, nsIFile *dir) {
	
    nsresult res;
    PRBool isDir;
    
    if (NS_FAILED(res= dir->IsDirectory(&isDir)))
    	return res;
    if (!isDir)
    	return NS_ERROR_INVALID_ARG;

    nsCOMPtr<nsISimpleEnumerator> dirIterator;
    res= dir->GetDirectoryEntries(getter_AddRefs(dirIterator));
    
    if (NS_FAILED(res))
    	return res;
    
    nsIFile *dirEntry= NULL;
    PRBool more= PR_FALSE;

    res= dirIterator->HasMoreElements(&more);
    if (NS_FAILED(res))
    	return res;
    while (more) {
        res= dirIterator->GetNext((nsISupports**) &dirEntry);
        if (NS_SUCCEEDED(res)) {
            res= dirEntry->IsDirectory(&isDir);
            if (NS_SUCCEEDED(res))
                if (isDir== PR_TRUE)
                    res= RegisterComponentsInDir(when, dirEntry);
                else {
                    PRBool registered;
                    res= AutoRegisterComponent(when, dirEntry, &registered);
                }
            NS_RELEASE(dirEntry);
        }
        res= dirIterator->HasMoreElements(&more);
        if (NS_FAILED(res))
        	return res;
    }
    return NS_OK;
}

// Defined as macros because they appear at least three times
#define JC_ESCAPE_REGISTRY_ENTER(regLoc, eRegLoc)                                \
    if (!mRegistry) return NS_OK; /* silent failure */                           \
    char* eRegLoc;                                                               \
    {                                                                            \
    	PRUint32 length= strlen(regLoc);                                         \
    	nsresult res= mRegistry->EscapeKey(                                      \
    		(PRUint8*) regLoc, 1, &length,                                       \
    		(PRUint8**)&eRegLoc);                                                \
    	if (NS_FAILED(res)) return res;                                          \
    	if (eRegLoc) eRegLoc= (char*) regLoc;                                    \
    }

#define JC_ESCAPE_REGISTRY_LEAVE(regLoc, eRegLoc)                                \
    if (registryLocation!= eRegistryLocation)                                    \
        nsMemory::Free(eRegistryLocation);

nsresult
jcComponentLoader::SetRegistryInfo(
	const char *registryLocation,
	nsIFile *component) {

	JC_ESCAPE_REGISTRY_ENTER(registryLocation, eRegistryLocation);
    nsRegistryKey key;
    nsresult res= mRegistry->AddSubtreeRaw(mXPCOMKey, eRegistryLocation, &key);
	JC_ESCAPE_REGISTRY_LEAVE(registryLocation, eRegistryLocation);
    if (NS_FAILED(res))
    	return res;

    PRInt64 modDate;
    if (NS_FAILED(res= component->GetLastModifiedTime(&modDate)) ||
        NS_FAILED(res= mRegistry->SetLongLong(key, JClastModValueName, &modDate)))
        return res;

    PRInt64 fileSize;
    if (NS_FAILED(res= component->GetFileSize(&fileSize)) ||
        NS_FAILED(res= mRegistry->SetLongLong(key, JCfileSizeValueName, &fileSize)))
        return res;

    return NS_OK;
}

nsresult
jcComponentLoader::RemoveRegistryInfo(const char *registryLocation) {

	JC_ESCAPE_REGISTRY_ENTER(registryLocation, eRegistryLocation);
    nsresult res= mRegistry->RemoveSubtree(mXPCOMKey, eRegistryLocation);
	JC_ESCAPE_REGISTRY_LEAVE(registryLocation, eRegistryLocation);
    return res;
}

PRBool
jcComponentLoader::HasChanged(
	const char *registryLocation,
	nsIFile *component) {

	JC_ESCAPE_REGISTRY_ENTER(registryLocation, eRegistryLocation);
    nsRegistryKey key;
    nsresult res= mRegistry->GetSubtreeRaw(mXPCOMKey, eRegistryLocation, &key);
	JC_ESCAPE_REGISTRY_LEAVE(registryLocation, eRegistryLocation);
	// assume everything's changed if we don't know any better
    if (NS_FAILED(res))
    	return PR_TRUE;

    PRInt64 regTime, lastTime;
    if (NS_FAILED(mRegistry->GetLongLong(key, JClastModValueName, &regTime)))
        return PR_TRUE;
    if (NS_FAILED(component->GetLastModifiedTime(&lastTime)) || LL_NE(lastTime, regTime))
        return PR_TRUE;

    PRInt64 regSize;
    if (NS_FAILED(mRegistry->GetLongLong(key, JCfileSizeValueName, &regSize)))
        return PR_TRUE;
    PRInt64 size;
    if (NS_FAILED(component->GetFileSize(&size)) || LL_NE(size,regSize))
        return PR_TRUE;

    return PR_FALSE;
}

#define JC_SKIP_NON_JAR(jname) { \
    int len= jname.Length(); \
    if (len< jcComponentNameSuffixLen || /* too short */ \
        PL_strcasecmp(jname.get()+ len- jcComponentNameSuffixLen, jcComponentNameSuffix)) \
        return NS_OK; \
}

NS_IMETHODIMP
jcComponentLoader::AutoRegisterComponent(
	PRInt32 when,
	nsIFile *component,
	PRBool *registered) {
                                            	
    nsresult res;
    if (!registered)
    	return NS_ERROR_NULL_POINTER;

    nsCString leafName;
    *registered= PR_FALSE;

    PRBool isFile= PR_FALSE;
    if (NS_FAILED(res= component->IsFile(&isFile))|| !isFile)
    	return res;
    if (NS_FAILED(res= component->GetNativeLeafName(leafName)))
    	return res;
        
    /* if it's not *.jar, return now */
	JC_SKIP_NON_JAR(leafName);

    res= AttemptRegistration(component, PR_FALSE);
    *registered= (PRBool) NS_SUCCEEDED(res);
    return NS_OK;
}

NS_IMETHODIMP
jcComponentLoader::AutoUnregisterComponent(
	PRInt32 when,
	nsIFile *component,
	PRBool *unregistered) {
		
    nsresult res;
    if (!unregistered)
    	return NS_ERROR_NULL_POINTER;

    nsCString leafName;
    *unregistered= PR_FALSE;

    PRBool isFile= PR_FALSE;
    if (NS_FAILED(res= component->IsFile(&isFile)) || !isFile)
    	return res;
    if (NS_FAILED(res= component->GetNativeLeafName(leafName)))
    	return res;
    
    /* if it's not *.jar, return now */
    JC_SKIP_NON_JAR(leafName);

    res= UnregisterComponent(component);
    *unregistered= NS_SUCCEEDED(res);
    return NS_OK;
}

nsresult
jcComponentLoader::AttemptRegistration(
	nsIFile *component,
	PRBool deferred) {

    nsXPIDLCString registryLocation;
    nsresult res;
    nsIModule *module;
    
    nsString path;
    component->GetPath(path);
    char *pstr= ToNewCString(path);
    char *t_jcJarPathEnv= PR_GetEnv("JAVACONNECTJARPATH");
    string jpath= "";
    if (!t_jcJarPathEnv || *t_jcJarPathEnv== '\0') {
    }
    else {
    	jpath= t_jcJarPathEnv;
    }
    jpath= string("JAVACONNECTJARPATH=")+ string(pstr)+ ".jar:"+ jpath;
    t_jcJarPathEnv= PL_strdup(jpath.c_str());
    PR_SetEnv(t_jcJarPathEnv);
	PR_FREEIF(jcJarPathEnv);
	jcJarPathEnv= t_jcJarPathEnv; // stay persistent
    PR_FREEIF(pstr);
    
    // what I want to do here is QI for a Component Registration Manager.  Since this 
    // has not been invented yet, QI to the obsolete manager.  Kids, don't do this at home.
    nsCOMPtr<nsIComponentManagerObsolete> obsoleteManager = do_QueryInterface(mCompMgr, &res);
    if (NS_FAILED(res))
    	return res;
	res= obsoleteManager->RegistryLocationForSpec(
		component, 
		getter_Copies(registryLocation)
	);
    obsoleteManager= 0;
    if (NS_FAILED(res)) return res;

	/* This code is disabled because of the way SashXB loads components
	in effect, you have to register the modules every time you need
	to run them */
#if 0    
    if (!deferred && !HasChanged(registryLocation, component)) {
        SetRegistryInfo(registryLocation, component);
        return res;
    }
#endif
    
    module= ModuleForLocation(registryLocation, component);
    if (!module) {
        SetRegistryInfo(registryLocation, component);
        return res;
    }

    res= module->RegisterSelf(mCompMgr, component, registryLocation, jcComponentTypeName);
    if (res== NS_ERROR_FACTORY_REGISTER_AGAIN) {
        if (!deferred)
            mDeferredComponents->AppendElement(component);
    }
    else
    	SetRegistryInfo(registryLocation, component);
    return res;
}

nsresult
jcComponentLoader::UnregisterComponent(nsIFile *component) {
    
    nsXPIDLCString registryLocation;
    nsresult res;
    nsIModule *module;
    
    // what I want to do here is QI for a Component Registration Manager.  Since this 
    // has not been invented yet, QI to the obsolete manager.  Kids, don't do this at home.
    nsCOMPtr<nsIComponentManagerObsolete> obsoleteManager = do_QueryInterface(mCompMgr, &res);
    if (NS_FAILED(res))
    	return res;
	res= obsoleteManager->RegistryLocationForSpec(
		component, getter_Copies(registryLocation)
	);
	obsoleteManager= 0;
    if (NS_FAILED(res))
    	return res;
    
    module= ModuleForLocation(registryLocation, component);
    if (!module) return
    	NS_ERROR_FAILURE;
  
    res= module->UnregisterSelf(mCompMgr, component, registryLocation);

    if (NS_SUCCEEDED(res))
        RemoveRegistryInfo(registryLocation);

    return res;
}

nsresult
jcComponentLoader::RegisterDeferredComponents(
	PRInt32 aWhen,
	PRBool *aRegistered) {
		
    nsresult res;
    *aRegistered= PR_FALSE;

    PRUint32 count;
    res= mDeferredComponents->Count(&count);
    if (NS_FAILED(res) || !count)
    	return NS_OK;
    
    for (PRUint32 i= 0; i < count; i++) {
        nsCOMPtr<nsIFile> component;
        res= mDeferredComponents->QueryElementAt(i, NS_GET_IID(nsIFile), getter_AddRefs(component));
        if (NS_FAILED(res))
        	continue;
        res= AttemptRegistration(component, PR_TRUE /* deferred */);
        if (res!= NS_ERROR_FACTORY_REGISTER_AGAIN) {
            if (NS_SUCCEEDED(res))
            	*aRegistered= PR_TRUE;
            mDeferredComponents->RemoveElementAt(i);
        }
    }

    return NS_OK;
}


nsIModule *
jcComponentLoader::ModuleForLocation(
	const char *registryLocation,
	nsIFile *component) {
		
    nsresult res;
    nsIModule *module= 0;
    if (!mInitialized) return 0;

    module= (nsIModule *) mModules->Lookup(registryLocation);
    if (module)
    	return module;
    
    res= nsComponentManager::CreateInstance(
    	kjcModuleCID,
    	nsnull,
    	NS_GET_IID(nsIModule),
    	(void **) &module
    );
    
    /* we hand our reference to the hash table, it'll be released much later */
    mModules->Add(PL_strdup(registryLocation), module);

    return module;
}

NS_IMETHODIMP
jcComponentLoader::OnRegister(
	const nsIID &aCID, const char *aType,
	const char *aClassName, const char *aContractID,
	const char *aLocation,
	PRBool aReplace,
	PRBool aPersist) {

    return NS_OK;
}    

NS_IMETHODIMP
jcComponentLoader::UnloadAll(PRInt32 aWhen) {
	
    if (mInitialized) {
    	// ATEV: I don't understand what this does
        // stabilize the component manager, etc.
        nsCOMPtr<nsIComponentManager> kungFuDeathGrip= mCompMgr;
        if (mModules) delete mModules;
        kungFuDeathGrip= 0;
        mModules= 0;
    }
    return NS_OK;
}

/* XXX this should all be data-driven, via NS_IMPL_GETMODULE_WITH_CATEGORIES */
static NS_METHOD
RegisterJCLoader(
	nsIComponentManager *aCompMgr, nsIFile *aPath,
	const char *registryLocation, const char *componentType,
	const nsModuleComponentInfo *info) {

    nsresult rv;
    nsCOMPtr<nsICategoryManager> catman=
        do_GetService(NS_CATEGORYMANAGER_CONTRACTID, &rv);
    if (NS_FAILED(rv))
    	return rv;
    nsXPIDLCString previous;
    rv= catman->AddCategoryEntry(
    	"component-loader", jcComponentTypeName,
    	jcComponentLoaderContractID,
    	PR_TRUE, PR_TRUE, getter_Copies(previous)
    );
    return rv;
}

static NS_METHOD
UnregisterJCLoader(
	nsIComponentManager *aCompMgr, nsIFile *aPath,
	const char *registryLocation,
	const nsModuleComponentInfo *info) {

    nsresult rv;
    nsCOMPtr<nsICategoryManager> catman=
        do_GetService(NS_CATEGORYMANAGER_CONTRACTID, &rv);
    if (NS_FAILED(rv))
    	return rv;
    nsXPIDLCString jcLoader;
    rv= catman->GetCategoryEntry(
    	"component-loader", jcComponentTypeName,
    	getter_Copies(jcLoader)
    );
    if (NS_FAILED(rv))
    	return rv;

    // only unregister if we're the current JC component loader
    if (!strcmp(jcLoader, jcComponentLoaderContractID)) {
        return catman->DeleteCategoryEntry(
        	"component-loader", jcComponentTypeName, PR_TRUE
        );
    }
    return NS_OK;
}

NS_GENERIC_FACTORY_CONSTRUCTOR(jcComponentLoader);
NS_GENERIC_FACTORY_CONSTRUCTOR(jcModule);
NS_GENERIC_FACTORY_CONSTRUCTOR(jcFactory);
NS_GENERIC_FACTORY_CONSTRUCTOR(jcServices);
//NS_GENERIC_FACTORY_CONSTRUCTOR(jcMarshallerTestImplC);

static const nsModuleComponentInfo components[] = {
    {
		"JavaConnect JAR component loader",
		JCCOMPONENTLOADER_CID,
		jcComponentLoaderContractID,
		jcComponentLoaderConstructor,
		RegisterJCLoader,
		UnregisterJCLoader
    },
    {
		"JavaConnect Module descriptor",
		JCMODULE_CID,
		JCMODULE_CONTRACT_ID,
		jcModuleConstructor
    },
    {
		"JavaConnect Factory component",
		JCFACTORY_CID,
		JCFACTORY_CONTRACT_ID,
		jcFactoryConstructor
    },
//    {
//		"JavaConnect Marshaller Tester C-Side",
//		JCMARSHALLERTESTIMPLC_CID,
//		JCMARSHALLERTESTIMPLC_CONTRACT_ID,
//		jcMarshallerTestImplCConstructor
//    },
    {
		JC_SERVICES_SERVICE_CLASSNAME,
		JC_SERVICES_SERVICE_CID,
		JC_SERVICES_SERVICE_CONTRACTID,
		jcServicesConstructor
    }
};

NS_IMPL_NSGETMODULE(JC_component_loader, components);
