/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 * The Initial Developer of this code under the MPL is Netscape
 * Communications Corporation.  Portions created by Netscape are
 * Copyright (C) 1999 Netscape Communications Corporation.  All Rights
 * Reserved.
 *
 * Contributors:
 * Stefan Atev <atevstef@luther.edu> - IBM Internet Technology Group
 */

#include "nsIComponentLoader.h"
#include "nsIRegistry.h"
#include "nsISupports.h"
#include "nsIModule.h"
#include "nsSupportsArray.h"
#include "nsIFileSpec.h"
#include "nsIFile.h"
#include "jcTables.h"

extern const char jcComponentLoaderContractID[];
extern const char jcComponentTypeName[];

// 252ac5a2-82b7-481c-9df9-3be3e25a7c56
#define JCCOMPONENTLOADER_CID \
	{0x252ac5a2, 0x82b7, 0x481c, {0x9d, 0xf9, 0x3b, 0xe3, 0xe2, 0x5a, 0x7c, 0x56}}
	
class jcComponentLoader: public nsIComponentLoader {

	public:
		NS_DECL_ISUPPORTS
		NS_DECL_NSICOMPONENTLOADER

		jcComponentLoader();
		virtual ~jcComponentLoader();

	protected:

		nsresult AttemptRegistration(nsIFile *component, PRBool deferred);
		nsresult UnregisterComponent(nsIFile *component);
		nsresult RegisterComponentsInDir(PRInt32 when, nsIFile *dir);
		nsIModule *ModuleForLocation(const char *aLocation, nsIFile *component);
		PRBool HasChanged(const char *registryLocation, nsIFile *component);
		nsresult SetRegistryInfo(const char *registryLocation, nsIFile *component);
		nsresult RemoveRegistryInfo(const char *registryLocation);

		/* ATEV: I'll leave it as a weak ref, though I don't see why
		   it got this way in the first place
		*/
		nsIComponentManager* mCompMgr; // weak ref, should make it strong?
		nsCOMPtr<nsIRegistry> mRegistry;
		jcModulesTable *mModules;
		nsRegistryKey mXPCOMKey;

		PRBool mInitialized;
		/* Switched to a nsISupportsArray * from nsSupportsArray, because
			1. It is the XPCOM way
			2. Count was perpetually 8, apparently static XPCOM objects
			do not always play well
		*/
		nsISupportsArray *mDeferredComponents;
		
		// free in destructor
		char *classPathEnv;
		char *jcJarPathEnv;
};
