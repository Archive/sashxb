
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "prmem.h"
#include "nsCRT.h"
#include "nsCOMPtr.h"
#include "nsIServiceManager.h"
#include "nsComponentManagerUtils.h"
#include "jcFactory.h"
#include "jcWrappedJavaObject.h"
#include "jcServices.h"
#include <iostream>

NS_IMPL_THREADSAFE_ISUPPORTS2(jcFactory, nsIFactory, jcIFactory)

jcFactory::jcFactory() {
	mClassData= 0;
    NS_INIT_ISUPPORTS();
}

jcFactory::~jcFactory() {
}

NS_IMETHODIMP jcFactory::CreateInstance(nsISupports *aOuter, const nsIID & iid, void * *result) {
    nsresult res;
    
    if (aOuter)
    	return NS_ERROR_NO_AGGREGATION;
    if (!result)
    	return NS_ERROR_NULL_POINTER;
    *result= 0;

    nsISupports *proxy= 0;

    nsCOMPtr<jcIServices> jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
    if (NS_FAILED(res))
    	return res;
    res= jcs->CreateWrappedJavaObject(0, mClassData, iid, &proxy);
    jcs= 0;
    if (NS_FAILED(res))
    	return res;

    *result= proxy;
    return NS_OK;
}

NS_IMETHODIMP jcFactory::LockFactory(PRBool lock) {
    return NS_ERROR_NOT_IMPLEMENTED;
}

NS_IMETHODIMP jcFactory::SetClassData(jcClassData * cd) {
	mClassData= cd;
    return NS_OK;
}

NS_IMETHODIMP jcFactory::GetClassData(jcClassData * *_retval) {
	if (!_retval)
		return NS_ERROR_NULL_POINTER;
	*_retval= mClassData;
    return NS_OK;
}
