/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "jcClassData.h"
#include "prmem.h"
#include "plstr.h"

jcClassData::jcClassData(
	const char *cname,
	const char *contractid,
	const nsCID &cid)
{
	className= PL_strdup(cname);
	contractID= PL_strdup(contractid);
	classID= cid;
	interfaces= 0;
	interfacesCount= 0;
}

jcClassData::~jcClassData()
{
	PR_FREEIF(className);
	PR_FREEIF(contractID);
	for (unsigned int i= 0; i< interfacesCount; ++i)
		PR_FREEIF(interfaces[i]);
	PR_FREEIF(interfaces);	
}
		
const char *jcClassData::getClassName() const {
	return className;
}

const char *jcClassData::getContractID() const {
	return contractID;
}

nsCID jcClassData::getClassID() const {
	return classID;
}
		
void jcClassData::setInterfaces(
	const nsIID **ifaces,
	const unsigned int count)
{
	for (unsigned int i= 0; i< interfacesCount; ++i)
		PR_FREEIF(interfaces[i]);
	PR_FREEIF(interfaces);
	interfacesCount= 0;
	interfaces= 0;
	if (!ifaces || !count)
		return;
	interfacesCount= count;
	interfaces= NS_STATIC_CAST(nsIID **, PR_MALLOC(sizeof(nsIID *)* count));
	for (unsigned int i= 0; i< count; ++i) {
		interfaces[i]= NS_STATIC_CAST(nsIID *, PR_MALLOC(sizeof(nsIID)));
		*interfaces[i]= *ifaces[i];
	}
}

nsIID **jcClassData::getInterfaces() const
{
	nsIID **result;
	result= NS_STATIC_CAST(nsIID **, PR_MALLOC(sizeof(nsIID *)* interfacesCount));
	for (unsigned int i= 0; i< interfacesCount; ++i) {
		result[i]= NS_STATIC_CAST(nsIID *, PR_MALLOC(sizeof(nsIID)));
		*result[i]= *interfaces[i];
	}
	return result;
}
		
unsigned int jcClassData::getInterfacesCount() const
{
	return interfacesCount;
}

bool jcClassData::hasInterface(const nsIID &iid) const
{
	for (unsigned int i= 0; i< interfacesCount; ++i)
		if (interfaces[i]->Equals(iid))
			return true;
	return false;
}

bool jcClassData::hasInterfaces() const
{
	return interfaces ? true : false;
}
