
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "jcConvert.h"
#include "nsReadableUtils.h"
#include "plstr.h"
#include "prmem.h"
#include "nsIVariant.h"
#include "jcError.h"

/**
	Stefan Atev <atvstef@luther.edu>
	
	There are two types of conversions here - Java-to-XPCOM conversions and
	XPCOM-to-Java ones. All Java-to-XPCOM routines start with J2X_Convert,
	while the XPCOM-to-Java ones start with X2J_Convert.
	The few Java-Java routines are marked by the prefix J2J_.
	
	You may find some error checking that seems redundant - just leave it
	be, there is absolutely no need to optimize these portions of the code
	as of right now. I decided against using goto to simulate try/catch for
	the JNI code, because that's what I did in the previous version and it was
	ridiculously hard to modify the code or to keep track of what exactly
	needed to be released on exit.
	
	env->PushLocalFrame/PopLocalFrame is gone on purpose - while it seemingly
	simplified the code, it meant that some results being returned were invalid
	since their frame was popped.
	
	A lot of the routines have multiple exit points - beats gotos and since
	now all procedures return nsresults, it is easy to spot a return NS_***
	
*/

/********************************************************/
/********************************************************/

nsresult X2J_ConvertPrimitive(
	JNIEnv *env, nsXPTType pt,
	void *src, jvalue &dst) {
		
	if (!env || !src)
		return NS_ERROR_NULL_POINTER;

	switch (pt.TagPart()) {
		case nsXPTType::T_I8:
		case nsXPTType::T_U8:
		case nsXPTType::T_CHAR:
			dst.b= *((jbyte *) src);
			break;
		case nsXPTType::T_I16:
			dst.s= *((jshort *) src);
			break;
		case nsXPTType::T_I32:
		case nsXPTType::T_U32:
			dst.i= *((jint *) src);
			break;
		case nsXPTType::T_I64:
		case nsXPTType::T_U64:
			dst.j= *((jlong *) src);
			break;
		case nsXPTType::T_U16:
		case nsXPTType::T_WCHAR:
			dst.c= *((jchar *) src);
			break;
		case nsXPTType::T_FLOAT:
			dst.f= *((float *) src);
			break;
		case nsXPTType::T_DOUBLE:
			dst.d= *((double *) src);
			break;
		case nsXPTType::T_BOOL:
			// special case
			if (*((PRBool *) src))
				dst.z= JNI_TRUE;
			else
				dst.z= JNI_FALSE;
			break;
		default:
			return JC_ERROR_CANT_HANDLE_TYPE;
	}
	return NS_OK;
}

nsresult J2X_ConvertPrimitve(
	JNIEnv *env, nsXPTType pt,
	jvalue src, void *dst) {
		
	if (!env || !dst)
		return NS_ERROR_NULL_POINTER;
	
	switch (pt.TagPart()) {
		case nsXPTType::T_I8:
		case nsXPTType::T_U8:
		case nsXPTType::T_CHAR:
			*((PRUint8 *) dst)= src.b;
			break;
		case nsXPTType::T_I16:
			*((PRInt16 *) dst)= src.s;
			break;
		case nsXPTType::T_I32:
		case nsXPTType::T_U32:
			*((PRUint32 *) dst)= src.i;
			break;
		case nsXPTType::T_I64:
		case nsXPTType::T_U64:
			*((PRUint64 *) dst)= src.j;
			break;
		case nsXPTType::T_U16:
		case nsXPTType::T_WCHAR:
			*((PRUint16 *) dst)= src.c;
			break;
		case nsXPTType::T_FLOAT:
			*((float *) dst)= src.f;
			break;
		case nsXPTType::T_DOUBLE:
			*((double *) dst)= src.d;
			break;
		case nsXPTType::T_BOOL:
			*((PRBool *) dst)= src.z;
			break;
		default:
			return JC_ERROR_CANT_HANDLE_TYPE;
	}
	return NS_OK;
}

nsresult X2J_ConvertString(
	JNIEnv *env, nsXPTType pt,
	void *src, int size,
	jstring &dst) {
	
	PRUint32 length= size;
	PRUnichar *ustr= 0;
	
	if (!env || !src)
		return NS_ERROR_NULL_POINTER;
	
	switch (pt.TagPart()) {
		case nsXPTType::T_ASTRING:
		case nsXPTType::T_DOMSTRING: {
			// src is pointer to nsAString
			length= ((nsAString *) src)->Length();
			dst= env->NewString(PromiseFlatString(*((nsAString *) src)).get(), (jsize) length);
			break;
		}
		case nsXPTType::T_CSTRING: {
			// src is pointer to nsACString
			length= ((nsACString *) src)->Length();
			ustr= ToNewUnicode(*((nsACString *) src));
			dst= env->NewString((jchar *) ustr, (jsize) length);
			PR_FREEIF(ustr);
			break;
		}
		case nsXPTType::T_UTF8STRING: {
			length= ((nsACString *) src)->Length();
			dst= env->NewString(NS_ConvertUTF8toUCS2(*((nsACString *) src)).get(), (jsize) length);
			break;
		}
		case nsXPTType::T_CHAR_STR: {
			length= PL_strlen((char *) src);
			ustr= ToNewUnicode(nsDependentCString((char *) src));
			dst= env->NewString((jchar *) ustr, (jsize) length);
			PR_FREEIF(ustr);
			break;
		}
		case nsXPTType::T_WCHAR_STR: {
			length= nsDependentString((PRUnichar *) src).Length();
			dst= env->NewString(nsDependentString((PRUnichar *) src).get(), (jsize) length);
			break;
		}
		case nsXPTType::T_PSTRING_SIZE_IS: {
			if (size< 0)
				return JC_ERROR_BAD_LENGTH;
			ustr= ToNewUnicode(nsDependentCString((char *) src, length));
			dst= env->NewString((jchar *) ustr, (jsize) length);
			PR_FREEIF(ustr);
			break;
		}
		case nsXPTType::T_PWSTRING_SIZE_IS: {
			if (size< 0)
				return JC_ERROR_BAD_LENGTH;
			ustr= ToNewUnicode(nsDependentString((PRUnichar *) src, length));
			dst= env->NewString((jchar *) ustr, (jsize) length);
			PR_FREEIF(ustr);
			break;
		}
		default: {
			return JC_ERROR_CANT_HANDLE_TYPE;
		}
	}
	if (!dst) {
		// Failed to convert string
		env->ExceptionClear();
		return JC_ERROR_CANT_CONVERT;
	}
	return NS_OK;	
}

nsresult J2X_ConvertString(
	JNIEnv *env, nsXPTType pt,
	jstring src, int &size,
	void *dst) {
		
	jsize length;
	jchar *ustr= 0;

	if (!env || !src || !dst)
		return NS_ERROR_NULL_POINTER;
	
	length= env->GetStringLength(src);
	if (env->ExceptionCheck()) {
		env->ExceptionClear();
		return JC_ERROR_CANT_GET_LENTGH;
	}
	// don't care if it's a copy
	ustr= (jchar *) env->GetStringChars(src, 0);
	if (!ustr) {
		env->ExceptionClear();
		return JC_ERROR_CANT_GET_STRING_CHARS;
	}
	
	switch (pt.TagPart()) {
		case nsXPTType::T_DOMSTRING:
		case nsXPTType::T_ASTRING:
			*((nsAString *) dst)= nsDependentString(ustr, length);
			break;
		case nsXPTType::T_CSTRING:
			CopyUCS2toASCII(nsDependentString(ustr, length), *((nsACString *) dst));
			break;
		case nsXPTType::T_UTF8STRING:
			*((nsACString *) dst)= NS_ConvertUCS2toUTF8((PRUnichar *) ustr, length);
			break;
		case nsXPTType::T_CHAR_STR:
			*((char **) dst)= ToNewCString(nsDependentString(ustr, length));
			break;
		case nsXPTType::T_WCHAR_STR:
			*((PRUnichar **) dst)= ToNewUnicode(nsDependentString(ustr, length));
			break;
		case nsXPTType::T_PSTRING_SIZE_IS: {
			char *temp= ToNewCString(nsDependentString(ustr, length));
			if (!temp) {
				env->ReleaseStringChars(src, ustr);
				return JC_ERROR_CANT_CONVERT;
			}
			*((char **) dst)= (char *) PR_MALLOC(length* sizeof(char));
			if (!(*((char **) dst))) {
				env->ReleaseStringChars(src, ustr);
				return JC_ERROR_CANT_CONVERT;
			}
			memcpy(*((char **) dst), temp, length* sizeof(char));
			PR_FREEIF(temp);
			break;
		}
		case nsXPTType::T_PWSTRING_SIZE_IS:
			*((PRUnichar **) dst)= (PRUnichar *) PR_MALLOC(length* sizeof(PRUnichar));
			if (!(*((PRUnichar **) dst))) {
				env->ReleaseStringChars(src, ustr);
				return JC_ERROR_CANT_CONVERT;
			}
			memcpy(*((PRUnichar **) dst), ustr, length* sizeof(PRUnichar));
			break;
		default:
			env->ReleaseStringChars(src, ustr);
			return JC_ERROR_CANT_HANDLE_TYPE;
	}
	env->ReleaseStringChars(src, ustr);
	size= (int) length;
	return NS_OK;
}

/********************************************************/

nsresult X2J_ConvertIIDObject(
	JNIEnv *env, const nsIID &iid, jobject &dst) {
	char *iidCstr= iid.ToString();
	if (!iidCstr)
		return JC_ERROR_CANT_PARSE_ID;
	jstring iidJstr;
	nsresult res= X2J_ConvertString(
		env, nsXPTType(nsXPTType::T_CHAR_STR), iidCstr, 0, iidJstr
	);
	PR_FREEIF(iidCstr);
	if (NS_FAILED(res))
		return res;
//	jclass cls= env->FindClass("org/mozilla/xpcom/IID");
	jclass cls= JC_FindClass(env, "org/mozilla/xpcom/IID");
	if (!cls) {
		env->ExceptionClear();
		env->DeleteLocalRef(iidJstr);
		return JC_ERROR_CANT_FIND_CLASS;
	}
	jmethodID mid= env->GetMethodID(cls, "<init>", "(Ljava/lang/String;)V");
	if (!mid) {
		env->ExceptionClear();
		env->DeleteLocalRef(iidJstr);
		env->DeleteLocalRef(cls);
		return JC_ERROR_CANT_FIND_METHOD;
	}
	dst= env->NewObject(cls, mid, iidJstr);
	env->DeleteLocalRef(cls); // before error check
	env->DeleteLocalRef(iidJstr);
	if (!dst) {
		env->ExceptionClear();
		return JC_ERROR_CANT_CREATE_OBJECT;
	}
	return NS_OK;
}

nsresult J2X_ConvertIIDObject(
	JNIEnv *env, jobject src, nsIID &iid) {
	
	jclass cls;
	jmethodID mid;
//	cls= env->FindClass("org/mozilla/xpcom/IID");
	cls= JC_FindClass(env, "org/mozilla/xpcom/IID");
	if (!cls) {
		env->ExceptionClear();
		return JC_ERROR_CANT_FIND_CLASS;
	}
	mid= env->GetMethodID(cls, "getIDStr", "()Ljava/lang/String;");
	env->DeleteLocalRef(cls); // before error check
	if (!mid) {
		env->ExceptionClear();
		return JC_ERROR_CANT_FIND_METHOD;
	}
	jstring iidJstr= (jstring) env->CallObjectMethod(src, mid);
	if (!iidJstr) {
		env->ExceptionClear();
		return JC_ERROR_CALL_FAILED;
	}
	char *iidStr;
	int siz;
	nsresult res= J2X_ConvertString(env, nsXPTType::T_CHAR_STR, iidJstr, siz, &iidStr);
	env->DeleteLocalRef(iidJstr); // before error check
	if (NS_FAILED(res))
		return res;
	res= iid.Parse(iidStr);
	PR_FREEIF(iidStr);
	return res;
}

// save us some typing
#define X2J_CONVERT_ARRAY_PRIMITIVE_CASE(XTYP, JTyp, jtyp) \
case nsXPTType::T_##XTYP: \
	dst= env->New##JTyp##Array(size); \
	if (!dst) { \
		env->ExceptionClear(); \
		return JC_ERROR_CANT_CREATE_ARRAY; \
	} \
	env->Set##JTyp##ArrayRegion( \
		(jtyp##Array) dst, 0, size, ((jtyp *) src) \
	); \
	if (env->ExceptionCheck()) { \
		env->DeleteLocalRef(dst); \
		env->ExceptionClear(); \
		return JC_ERROR_CANT_SET_ARRAY_ELEMENTS; \
	} \
	break;

// as a macro to avoid sync problems
#define X2J_CONVERT_ARRAY_OBJECT_CASE_IMPL(convert_el_WithRes) \
for (int i= 0; i< size; ++i) { \
	jobject el; \
	nsresult res= convert_el_WithRes; \
	if (NS_FAILED(res)) { \
		env->DeleteLocalRef(dst); \
		return res; \
	} \
	env->SetObjectArrayElement((jobjectArray) dst, i, el); \
	if (env->ExceptionCheck()) { \
		env->DeleteLocalRef(dst); \
		return JC_ERROR_CANT_SET_OBJECT_ARRAY_ELEMENT; \
	} \
	env->DeleteLocalRef(el); \
} \
break;

nsresult X2J_ConvertArray(
	JNIEnv *env, nsXPTType pt,
	void *src, int size, nsIID &iid,
	jarray &dst) {
		
	if (!env || (!src && size!= 0) || size< 0)
		return NS_ERROR_NULL_POINTER;
		
	/* allocate the object array if needed */
	jclass cls= 0;
	switch (pt.TagPart()) {
		// I can't imagine how T_P*STRING_SIZE_IS is handled in arrays
		case nsXPTType::T_DOMSTRING:
		case nsXPTType::T_ASTRING:
		case nsXPTType::T_CSTRING:
		case nsXPTType::T_CHAR_STR:
		case nsXPTType::T_WCHAR_STR:
		case nsXPTType::T_UTF8STRING: {
//			cls= env->FindClass("java/lang/String");
			cls= JC_FindClass(env, "java/lang/String");
			if (!cls) {
				env->ExceptionClear();
				return JC_ERROR_CANT_FIND_CLASS;
			}
			break;
		}
		case nsXPTType::T_INTERFACE:
		case nsXPTType::T_INTERFACE_IS: {
			char *memberClassNamePtr= 0;
			if (iid.Equals(NS_GET_IID(nsIVariant)) || iid.Equals(NS_GET_IID(nsIWritableVariant)))
				memberClassNamePtr= PL_strdup("java/lang/Object");
			else			
				memberClassNamePtr= JC_GetInterfaceName(iid);
			if (!memberClassNamePtr)
				return JC_ERROR_CANT_GET_INTERFACE_NAME;
			string memberClassNameStr;
			if (iid.Equals(NS_GET_IID(nsIVariant)) || iid.Equals(NS_GET_IID(nsIWritableVariant)))
				memberClassNameStr= memberClassNamePtr;
			else
				memberClassNameStr= string("org/mozilla/xpcom/")+ memberClassNamePtr;
			PR_FREEIF(memberClassNamePtr);
//			cls= env->FindClass(memberClassNameStr.c_str());
			cls= JC_FindClass(env, memberClassNameStr.c_str());
			if (!cls) {
				env->ExceptionDescribe();
				env->ExceptionClear();
				return JC_ERROR_CANT_FIND_CLASS;
			}
			break;
		}
		case nsXPTType::T_IID: {
//			cls= env->FindClass("org/mozilla/xpcom/IID");
			cls= JC_FindClass(env, "org/mozilla/xpcom/IID");
			if (!cls) {
				env->ExceptionClear();
				return JC_ERROR_CANT_FIND_CLASS;
			}
			break;
		}
	}
	if (cls) {
		dst= env->NewObjectArray(size, cls, 0);
		env->DeleteLocalRef(cls); // before possible return
		if (!dst) {
			env->ExceptionClear();
			return JC_ERROR_CANT_CREATE_OBJECT_ARRAY;
		}
	}

	// process the array
	switch (pt.TagPart()) {

		// special case since PRBool is 32 bit
		case nsXPTType::T_BOOL: {
			dst= env->NewBooleanArray(size);
			if (!dst) {
				env->ExceptionClear();
				return JC_ERROR_CANT_CREATE_ARRAY;
			}
			jboolean *tmp= (jboolean *) PR_MALLOC(sizeof(jboolean)* size);
			if (!tmp) {
				env->DeleteLocalRef(dst);
				return JC_ERROR_NO_MEMORY;
			}
			for (int i= 0; i< size; ++i)
				tmp[i]= ( *(((PRBool *) src)+ i) ? JNI_TRUE : JNI_FALSE);
			env->SetBooleanArrayRegion(
				(jbooleanArray) dst, 0, size, tmp
			);
			PR_FREEIF(tmp); // before error check
			if (env->ExceptionCheck()) {
				env->DeleteLocalRef(dst);
				env->ExceptionClear();
				return JC_ERROR_CANT_SET_ARRAY_ELEMENTS;
			}
			break;
		}
		
		// Primitive cases are different since JNI has special support for them
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(I8, Byte, jbyte);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(I16, Short, jshort);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(I32, Int, jint);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(I64, Long, jlong);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(U8, Byte, jbyte);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(U16, Char, jchar);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(U32, Int, jint);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(U64, Long, jlong);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(FLOAT, Float, jfloat);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(DOUBLE, Double, jdouble);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(CHAR, Byte, jbyte);
		X2J_CONVERT_ARRAY_PRIMITIVE_CASE(WCHAR, Char, jchar);

		case nsXPTType::T_DOMSTRING:
		case nsXPTType::T_ASTRING:
		case nsXPTType::T_CSTRING:
		case nsXPTType::T_UTF8STRING:
		case nsXPTType::T_CHAR_STR:
		case nsXPTType::T_WCHAR_STR:
			X2J_CONVERT_ARRAY_OBJECT_CASE_IMPL(
				X2J_ConvertString(env, pt, *(((void **) src)+ i), 0, (jstring &) el)
			);
		case nsXPTType::T_INTERFACE:
		case nsXPTType::T_INTERFACE_IS:
			X2J_CONVERT_ARRAY_OBJECT_CASE_IMPL(
				X2J_ConvertObject(env, pt, *(((nsISupports **) src)+ i), iid, el)
			);
		case nsXPTType::T_IID:
			X2J_CONVERT_ARRAY_OBJECT_CASE_IMPL(
				X2J_ConvertIIDObject(env, (**(((nsIID **) src)+ i)), el)
			);
		default:
			return JC_ERROR_CANT_HANDLE_TYPE;
	}
	return NS_OK;
}
#undef X2J_CONVERT_ARRAY_PRIMITIVE_CASE
#undef X2J_CONVERT_ARRAY_OBJECT_CASE_IMPL

#define J2X_CONVERT_ARRAY_PRIMITIVE_CASE(XTYP, JTyp, jtyp) \
case nsXPTType::T_##XTYP: { \
	jtyp *result= (jtyp *) PR_MALLOC(sizeof(jtyp)* size); \
	if (!result) \
		return JC_ERROR_NO_MEMORY; \
	env->Get##JTyp##ArrayRegion((jtyp##Array) src, 0, size, result); \
	if (env->ExceptionCheck()) { \
		env->ExceptionClear(); \
		return JC_ERROR_CANT_GET_ARRAY_ELEMENTS; \
	} \
	*dst= result; \
	return NS_OK; \
}

nsresult J2X_ConvertArray(
	JNIEnv *env, nsXPTType pt,
	jarray src, int &size, nsIID &iid, 
	void **dst) {
		
	if (!env || !dst)
		return NS_ERROR_NULL_POINTER;

	size= env->GetArrayLength(src);
	if (env->ExceptionCheck()) {
		env->ExceptionClear();
		return JC_ERROR_CANT_GET_LENTGH;
	}
	if (size== 0) { // empty array in c== 0 ??
		*dst= 0;
		return NS_OK;
	}

	switch (pt.TagPart()) {
		// J2X_CONVERT_ARRAY_PRIMITIVE_CASE(BOOL, Boolean, jboolean);
		// Special case since PRBool is 32 bit
		case nsXPTType::T_BOOL: {
			jboolean *bools;
			bools= (jboolean *) PR_MALLOC(sizeof(jboolean)* size);
			if (!bools)
				return JC_ERROR_NO_MEMORY;
			env->GetBooleanArrayRegion((jbooleanArray) src, 0, size, bools);
			if (env->ExceptionCheck()) {
				env->ExceptionClear();
				return JC_ERROR_CANT_GET_ARRAY_ELEMENTS;
			}
			PRBool *result;
			result= (PRBool *) PR_MALLOC(sizeof(PRBool)* size);
			if (!result)
				return JC_ERROR_NO_MEMORY;
			for (int i= 0; i< size; ++i)
				result[i]= bools[i];
			PR_FREEIF(bools);
			*dst= result;
			return NS_OK;
		}

		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(I8, Byte, jbyte);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(I16, Short, jshort);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(I32, Int, jint);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(I64, Long, jlong);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(U8, Byte, jbyte);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(U16, Char, jchar);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(U32, Int, jint);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(U64, Long, jlong);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(FLOAT, Float, jfloat);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(DOUBLE, Double, jdouble);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(CHAR, Byte, jbyte);
		J2X_CONVERT_ARRAY_PRIMITIVE_CASE(WCHAR, Char, jchar);

		case nsXPTType::T_DOMSTRING:
		case nsXPTType::T_ASTRING:
		case nsXPTType::T_CSTRING:
		case nsXPTType::T_UTF8STRING:
			cerr << "I think these types cannot be in arrays" << endl;
			break;
		case nsXPTType::T_CHAR_STR:
		case nsXPTType::T_WCHAR_STR: {
			void *result= PR_MALLOC(size* sizeof(void *));
			for (int i= 0; i< size; ++i) {
				jobject obj= env->GetObjectArrayElement((jobjectArray) src, i);
				// null strings are not OK
				if (!obj) {
					env->ExceptionClear();
					PR_FREEIF(result);
					return JC_ERROR_CANT_GET_OBJECT_ARRAY_ELEMENT;
				}
				int length;
				nsresult res= J2X_ConvertString(env, pt, (jstring) obj, length, (((void **) result)+ i));
				env->DeleteLocalRef(obj);
				if (NS_FAILED(res)) {
					for (int j= 0; j< i; ++j)
						PR_FREEIF( *(((void **) result)+ i) ) ;
					PR_FREEIF(result);
					return res;
				}
			}
			*dst= result;
			return NS_OK;
		}
		case nsXPTType::T_INTERFACE:
		case nsXPTType::T_INTERFACE_IS: {
			void *result= PR_MALLOC(size* sizeof(void *));
			for (int i= 0; i< size; ++i) {
				// Slower than necessary, but helps since some functions
				// like J2X_ConvertVariant depend on that being null
				*(((void **) result)+ i)= 0;
				jobject obj= env->GetObjectArrayElement((jobjectArray) src, i);
				// may be null, so don't check for obj== null
				if (env->ExceptionCheck()) {
					env->ExceptionClear();
					PR_FREEIF(result);
					return JC_ERROR_CANT_GET_OBJECT_ARRAY_ELEMENT;
				}
				nsresult res= J2X_ConvertObject(env, pt, obj, iid, (((nsISupports **) result)+ i));
				env->DeleteLocalRef(obj);
				if (NS_FAILED(res)) {
					for (int j= 0; j< i; ++j)
						NS_IF_RELEASE(*(((nsISupports **) result)+ i));
					PR_FREEIF(result);
					return res;
				}
			}
			*dst= result;
			return NS_OK;
		}
		case nsXPTType::T_IID: {
			void *result= PR_MALLOC(size* sizeof(void *));
			for (int i= 0; i< size; ++i) {
				jobject obj= env->GetObjectArrayElement((jobjectArray) src, i);
				if (!obj) {
					env->ExceptionClear();
					PR_FREEIF(result);
					return JC_ERROR_CANT_GET_OBJECT_ARRAY_ELEMENT;
				}
				*(((nsIID **) result) + i)= new nsIID();
				nsresult res= J2X_ConvertIIDObject(env, obj, **(((nsIID **) result)+ i));
				env->DeleteLocalRef(obj);
				if (NS_FAILED(res)) {
					for (int j= 0; j< i; ++j)
						delete *(((nsIID **) result)+ i);
					PR_FREEIF(result);
					return res;
				}
			}
			*dst= result;
			return NS_OK;
		}
	}
	return JC_ERROR_CANT_CONVERT;
}
#undef J2X_CONVERT_ARRAY_PRIMITIVE_CASE

nsresult X2J_ConvertObject(
	JNIEnv *env, nsXPTType pt,
	void *src, nsIID &iid,
	jobject &dst) {
		
	nsCOMPtr<jcIServices> jcs= 0;
	nsresult res= JC_ERROR_CANT_CONVERT;
	nsISupports *ifacePtr= (nsISupports *) src, *testIface= 0;
	
	if (!env)
		return NS_ERROR_NULL_POINTER;
	
	if (!src) { // null??
		dst= 0;
		return NS_OK;
	}

	if (iid.Equals(NS_GET_IID(nsIVariant)) || iid.Equals(NS_GET_IID(nsIWritableVariant))) {
		return X2J_ConvertVariant(env, src, dst);
	}

	/*
		Check if the interface pointer points to something we have wrapped
		ourselves.
		These two checks are totally NOT XPCOM, QI will return magic
		values here. No valid object that is not a WrappedJavaObject
		or WrappedJavaInterface will ever implement the fake
		IWrappedJavaObject and IWrappedJavaInterface interfaces.
	*/
	res= ifacePtr->QueryInterface(kjcWrappedJavaObjectIID, (void **) &testIface);
	if (reinterpret_cast<unsigned int>(testIface)== JC_MAJIC_WRAPPER_IDENTIFIER &&
		reinterpret_cast<unsigned int>(res)== JC_MAJIC_FAILURE_RESPONSE) {

		dst= ((jcWrappedJavaObject *) (nsIClassInfo *) ifacePtr)->mobj;
		// consumer expects a local ref in its own frame
		dst= env->NewLocalRef(dst);
		if (!dst) {
			env->ExceptionClear();
			return JC_ERROR_CANT_REFERENCE;
		}
		return NS_OK;
	}
	if (reinterpret_cast<unsigned int>(testIface)== JC_MAJIC_TEAROFF_IDENTIFIER &&
		reinterpret_cast<unsigned int>(res)== JC_MAJIC_FAILURE_RESPONSE) {

		dst= ((jcWrappedJavaObject *) (((jcWrappedJavaInterface *) ifacePtr)->parent))->mobj;
		// consumer expects a local ref in its own frame
		dst= env->NewLocalRef(dst);
		if (!dst) {
			env->ExceptionClear();
			return JC_ERROR_CANT_REFERENCE;
		}
		return NS_OK;
	}
			
	// We need to wrap it up for use in java!
	res= ifacePtr->QueryInterface(iid, (void **) &testIface);
	if (NS_FAILED(res))
		return res;
	jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
	if (NS_FAILED(res)) {
		NS_RELEASE(testIface);
		return res;
	}
	res= jcs->WrapNativeObject(env, testIface, iid, &dst);
	NS_RELEASE(testIface);
	jcs= 0;
	if (NS_FAILED(res))
		return res;

	// consumer expects a local ref in its own frame
	dst= env->NewLocalRef(dst);
	if (!dst) {
		env->ExceptionClear();
		return JC_ERROR_CANT_REFERENCE;
	}
	return NS_OK;
}

nsresult J2X_ConvertObject(
	JNIEnv *env, nsXPTType pt,
	jobject src, nsIID &iid,
	void *dst) {
	
	if (!env || !dst)
		return NS_ERROR_NULL_POINTER;
	
	nsCOMPtr<jcIServices> jcs= 0;
	nsresult res;
	switch (pt.TagPart()) {
		case nsXPTType::T_INTERFACE:
		case nsXPTType::T_INTERFACE_IS:
			if (iid.Equals(NS_GET_IID(nsIVariant)) || iid.Equals(NS_GET_IID(nsIWritableVariant))) {
				if (!(*((nsIWritableVariant **) dst))) {
					nsCOMPtr<nsIWritableVariant> retvar= do_CreateInstance("@mozilla.org/variant;1");
					retvar->QueryInterface(iid, (void **) dst);
					retvar= 0;
				}
				return J2X_ConvertVariant(env, src, *((nsIWritableVariant **) dst));
			}
			if (!src) {
				*((nsISupports **) dst)= 0;
				return NS_OK;
			}
			/*
				This is a heavy JNI section, be careful; I should have it
				implemented in Java, but this is a more straightforward way
				to do it and may be easier in the future when I start worrying
				about threads. The ugly goto is here again because there's too
				many possible errors to make
			*/
			{
				env->PushLocalFrame(16);
				
				jclass proxyCls, objCls, jcCls;
				jmethodID isProxyMid, getHandlerMid;
				jboolean ans;
				jlong addr;
				jobject invHandler;
				jfieldID addrFieldId;
				
				objCls= env->GetObjectClass(src);
				if (!objCls) goto detectDoubleWrapAbort;
//				proxyCls= env->FindClass("java/lang/reflect/Proxy");
				proxyCls= JC_FindClass(env, "java/lang/reflect/Proxy");
				if (!proxyCls) goto detectDoubleWrapAbort;
				isProxyMid= env->GetStaticMethodID(proxyCls, "isProxyClass", "(Ljava/lang/Class;)Z");
				env->ExceptionDescribe();
				if (!isProxyMid) goto detectDoubleWrapAbort;
				ans= env->CallStaticBooleanMethod(proxyCls, isProxyMid, objCls);
				if (env->ExceptionCheck()) goto detectDoubleWrapAbort;
				// if it's not a proxy class it definately is
				// not a wrapped native
				if (ans!= JNI_TRUE) goto detectDoubleWrapNegative;
				// ok, so it is a proxy, get the invocation handler
				getHandlerMid= env->GetStaticMethodID(proxyCls, "getInvocationHandler", "(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;");
				if (!getHandlerMid) goto detectDoubleWrapAbort;
				invHandler= env->CallStaticObjectMethod(proxyCls, getHandlerMid, src);
				if (!invHandler) goto detectDoubleWrapAbort;
//				jcCls= env->FindClass("org/mozilla/xpcom/jcWrappedNative");
				jcCls= JC_FindClass(env, "org/mozilla/xpcom/jcWrappedNative");
				if (!jcCls) goto detectDoubleWrapAbort;
				// check if the handler is a jcWrappedNative
				ans= env->IsInstanceOf(invHandler, jcCls);
				if (env->ExceptionCheck()) goto detectDoubleWrapAbort;
				// if it is not, we got a negative				
				if (ans!= JNI_TRUE) goto detectDoubleWrapNegative;
				// ok, so now we know that we have a wrapped XPCOM
				// that made the roundtrip from Java
				// from now on errors are fatal for the method and should
				// not jump to the abort label
				addrFieldId= env->GetFieldID(jcCls, "addr", "J");
				if (!addrFieldId) {
					env->ExceptionClear();
					env->PopLocalFrame(0);
					return JC_ERROR_CANT_FIND_FIELD;
				}
				addr= env->GetLongField(invHandler, addrFieldId);
				if (env->ExceptionCheck()) {
					env->ExceptionClear();
					env->PopLocalFrame(0);
					return JC_ERROR_CANT_GET_FIELD;
				}
				// finally we are successful, addr is the memory address
				// of an XPCOM object
				*((nsISupports **) dst)= reinterpret_cast<nsISupports *>(addr);
				// QUESTION: should I reference the result here?
				NS_IF_ADDREF(*((nsISupports **) dst));
				env->PopLocalFrame(0);
				return NS_OK;
				
detectDoubleWrapAbort:
				env->ExceptionClear();
detectDoubleWrapNegative:
				env->PopLocalFrame(0);
			}
			jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
			if (NS_FAILED(res))
				return res;
			res= jcs->WrapJavaObject(env, src, iid, (nsISupports **) (dst));
			if (NS_FAILED(res))
				return res;
			return NS_OK;
	}
	// not an interface
	return JC_ERROR_CANT_HANDLE_TYPE;
}

//#define WORDPREFIX(c1, c2) ((c1 << 8)+ c2)
// The above will mathc the first two characters exactly
//
#define WORDPREFIX(c1, c2) (int) ((((c1 << 1)- c2- 11)) % 29)
nsresult J2X_ConvertVariant(
	JNIEnv *env, jobject src, nsIWritableVariant *dst) {
		
	nsIID iid;
	if (!src) {
		dst->SetAsEmpty();
		return NS_OK;
	}
	
	jclass objcls= env->GetObjectClass(src);
	if (!objcls) {
		env->ExceptionClear();
		return JC_ERROR_CANT_FIND_CLASS;
	}
//	jclass clscls= env->FindClass("java/lang/Class");
	jclass clscls= JC_FindClass(env, "java/lang/Class");
	if (!clscls) {
		env->ExceptionClear();
		env->DeleteLocalRef(objcls);
		return JC_ERROR_CANT_FIND_CLASS;
	}
	jmethodID getNameMID= env->GetMethodID(clscls, "getName", "()Ljava/lang/String;");
	if (!getNameMID) {
		env->ExceptionClear();
		env->DeleteLocalRef(objcls);
		env->DeleteLocalRef(clscls);
		return JC_ERROR_CANT_FIND_METHOD;
	}
	jstring className= (jstring) env->CallObjectMethod(objcls, getNameMID);
	env->DeleteLocalRef(clscls);
	env->DeleteLocalRef(objcls);
	if (!className) {
		env->ExceptionClear();
		return JC_ERROR_CALL_FAILED;
	}
	int siz;
	char *classNameCStr;
	nsresult res= J2X_ConvertString(env, nsXPTType::T_CHAR_STR, className, siz, &classNameCStr);
	if (NS_FAILED(res))
		return res;
	string cn= classNameCStr;
	PR_FREEIF(classNameCStr);
	// java.lang.XX*
	if (cn.length()> 12 && cn.substr(0, 10)== "java.lang.") {
		cn.erase(0, 10);
		if (cn== "String") {
			PRUnichar *str;
			int siz;
			res= J2X_ConvertString(env, nsXPTType::T_WCHAR_STR, (jstring) src, siz, &str);
			if (NS_FAILED(res))
				return res;
			res= dst->SetAsWString(str);
			PR_FREEIF(str);
			return res;
		}
		else {
			if (cn.length()> 9) // Character== 9 is the longest
				return JC_ERROR_CANT_HANDLE_TYPE;
			if (cn.find(".")!= string::npos) // A dot means sub-package
				return JC_ERROR_CANT_HANDLE_TYPE;
			nsXPTType typ;
			// I am hashing here to avoid many string comparisons
			// The hash is "perfect" for the types that are currently in
			// java.lang (as of JDK 1.3.1 and 1.4
			// Hopefully the compiler will use a jump table here
			switch (WORDPREFIX(cn[0], cn[1])) {
				case WORDPREFIX('B', 'y'): // Byte
					typ= nsXPTType::T_U8;
					break;
				case WORDPREFIX('B', 'o'): // Boolean
					typ= nsXPTType::T_BOOL;
					break;
				case WORDPREFIX('C', 'h'): // Character
					typ= nsXPTType::T_WCHAR;
					break;
				case WORDPREFIX('D', 'o'): // Double
					typ= nsXPTType::T_DOUBLE;
					break;
				case WORDPREFIX('F', 'l'): // Float
					typ= nsXPTType::T_FLOAT;
					break;
				case WORDPREFIX('I', 'n'): // Integer
					typ= nsXPTType::T_I32;
					break;
				case WORDPREFIX('L', 'o'): // Long
					typ= nsXPTType::T_I64;
					break;
				case WORDPREFIX('S', 'h'): // Short
					typ= nsXPTType::T_I16;
					break;
#if 0
				// Enable when testing with different hashes
				case WORDPREFIX('O', 'b'): // Object
				case WORDPREFIX('M', 'a'): // Math
				case WORDPREFIX('N', 'u'): // Number
				case WORDPREFIX('C', 'l'): // Class
				case WORDPREFIX('C', 'o'): // Compiler
				case WORDPREFIX('P', 'a'): // Package
				case WORDPREFIX('P', 'r'): // Process
				case WORDPREFIX('R', 'u'): // Runtime
				case WORDPREFIX('S', 'y'): // System
				case WORDPREFIX('T', 'h'): // Thread, Throwable
#endif
				default:
					return NS_OK;
			}
			// now we have a type tag and trying to convert what
			// we believe to be a primitive type
			jvalue jv;
			// Use this to save some typing. Nothing comes free...
			nsresult res= J2J_UnWrapPrimitive(env, typ, src, jv);
			if (NS_FAILED(res))
				return res;
			// and now we can actually set the value acording to
			// typ
			switch (typ.TagPart()) {
				case nsXPTType::T_U8:
					dst->SetAsUint8(jv.b);
					break;
				case nsXPTType::T_BOOL:
					dst->SetAsBool(jv.z ? PR_TRUE : PR_FALSE);
					break;
				case nsXPTType::T_WCHAR:
					dst->SetAsWChar(jv.c);
					break;
				case nsXPTType::T_DOUBLE:
					dst->SetAsDouble(jv.d);
					break;
				case nsXPTType::T_FLOAT:
					dst->SetAsFloat(jv.f);
					break;
				case nsXPTType::T_I32:
					dst->SetAsInt32(jv.i);
					break;
				case nsXPTType::T_I64:
					dst->SetAsInt64(jv.j);
					break;
				case nsXPTType::T_I16:
					dst->SetAsInt16(jv.s);
					break;
			}
			return NS_OK;
		}
	}
	if (cn.length()>= 2 && cn[0]== '[') {
		nsXPTType typ;
		switch (cn[1]) {
			case 'b':
			case 'B':
				typ= nsXPTType::T_U8;
				break;
			case 'c':
			case 'C':
				typ= nsXPTType::T_WCHAR;
				break;
			case 'z':
			case 'Z':
				typ= nsXPTType::T_BOOL;
				break;
			case 's':
			case 'S':
				typ= nsXPTType::T_I16;
				break;
			case 'i':
			case 'I':
				typ= nsXPTType::T_I32;
				break;
			case 'j':
			case 'J':
				typ= nsXPTType::T_I64;
				break;
			case 'd':
			case 'D':
				typ= nsXPTType::T_DOUBLE;
				break;
			case 'f':
			case 'F':
				typ= nsXPTType::T_FLOAT;
				break;
			case 'l':
			case 'L':
				typ= nsXPTType::T_INTERFACE;
				iid= NS_GET_IID(nsISupports);
				break;
			case '[':
				typ= nsXPTType::T_INTERFACE;
				iid= NS_GET_IID(nsIVariant);
				break;
		}
		int siz;
		void *array;
		res= J2X_ConvertArray(env, typ, (jarray) src, siz, iid, &array);
		if (NS_FAILED(res))
			return res;
		if (siz== 0)
			res= dst->SetAsEmptyArray();
		else
			res= dst->SetAsArray(typ.TagPart(), &iid, (PRUint32) siz, array);
		if (typ.TagPart()== nsXPTType::T_INTERFACE)
			for (int i= 0; i< siz; ++i)
				NS_IF_RELEASE(((nsISupports **) array)[i]);
		PR_FREEIF(array);
		return res;
	}
	iid= NS_GET_IID(nsISupports);
	void *tmp;
	res= J2X_ConvertObject(env, nsXPTType::T_INTERFACE, src, iid, &tmp);
	if (NS_FAILED(res))
		return res;
	dst->SetAsInterface(iid, (nsISupports *) tmp);
	nsISupports *cnt= (nsISupports *) tmp;
	NS_IF_RELEASE(cnt);
	return res;
}
#undef WORDPREFIX

#define X2J_CONVERT_VARIANT_PRIMITIVE_CASE(VTYP, dotJ, varN, UTYPE) \
case nsIDataType::VTYPE_##VTYP: { \
	jvalue val; \
	nsresult res= var->GetAs##varN((UTYPE *) & dotJ ); \
	if (NS_FAILED(res)) \
		return res; \
	res= J2J_WrapPrimitive(env, nsXPTType(pt), val, dst); \
	return res; \
}

nsresult X2J_ConvertVariant(
	JNIEnv *env, void *src, jobject &dst) {
	
	PRUint16 pt;
	jclass cls;
	jmethodID mid;

	if (!env || !src)
		return NS_ERROR_NULL_POINTER;
	nsCOMPtr<nsIVariant> var= do_QueryInterface((nsISupports *) src);
	if (!var)
		return NS_ERROR_NO_INTERFACE;
	
	nsresult res= var->GetDataType(&pt);
	if (NS_FAILED(res))
		return res;

	switch (pt) {

		// special case for PRBool
		case nsIDataType::VTYPE_BOOL: {
			jvalue val;
			PRBool tmp;
			nsresult res= var->GetAsBool(&tmp);
			if (NS_FAILED(res))
				return res;
			val.z= tmp ? JNI_TRUE : JNI_FALSE;
			res= J2J_WrapPrimitive(env, nsXPTType(pt), val, dst);
			return res;
		}

		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(INT8, val.b, Int8, PRUint8);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(INT16, val.s, Int16, PRInt16);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(INT32, val.i, Int32, PRInt32);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(INT64, val.j, Int64, PRInt64);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(UINT8, val.b, Uint8, PRUint8);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(UINT16, val.c, Uint16, PRUint16);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(UINT32, val.i, Uint32, PRUint32);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(UINT64, val.j, Uint64, PRUint64);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(FLOAT, val.f, Float, float);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(DOUBLE, val.d, Double, double);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(CHAR, val.b, Char, char);
		X2J_CONVERT_VARIANT_PRIMITIVE_CASE(WCHAR, val.c, WChar, PRUnichar);
		
		case nsIDataType::VTYPE_ASTRING: {
			nsString val;
			nsresult res= var->GetAsAString(val);
			if (NS_FAILED(res))
				return res;
			return X2J_ConvertString(env, nsXPTType(pt), &val, 0, (jstring &) dst);
		}
		case nsIDataType::VTYPE_DOMSTRING: {
			nsString val;
			nsresult res= var->GetAsDOMString(val);
			if (NS_FAILED(res))
				return res;
			return X2J_ConvertString(env, nsXPTType(pt), &val, 0, (jstring &) dst);
		}
		case nsIDataType::VTYPE_CSTRING: {
			nsCString val;
			nsresult res= var->GetAsACString(val);
			if (NS_FAILED(res))
				return res;
			return X2J_ConvertString(env, nsXPTType(pt), &val, 0, (jstring &) dst);
		}
		case nsIDataType::VTYPE_UTF8STRING: {
			nsCString val;
			nsresult res= var->GetAsAUTF8String(val);
			if (NS_FAILED(res))
				return res;
			return X2J_ConvertString(env, nsXPTType(pt), &val, 0, (jstring &) dst);
		}
		case nsIDataType::VTYPE_CHAR_STR: {
			char *val;
			nsresult res= var->GetAsString(&val);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertString(env, nsXPTType(pt), val, 0, (jstring &) dst);
			PR_FREEIF(val);
			return res;
		}
		case nsIDataType::VTYPE_WCHAR_STR: {
			PRUnichar *val;
			nsresult res= var->GetAsWString(&val);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertString(env, nsXPTType(pt), val, 0, (jstring &) dst);
			PR_FREEIF(val);
			return res;
		}
		case nsIDataType::VTYPE_STRING_SIZE_IS: {
			char *val;
			PRUint32 size;
			nsresult res= var->GetAsStringWithSize(&size, &val);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertString(env, nsXPTType(pt), val, size, (jstring &) dst);
			PR_FREEIF(val);
			return res;
		}
		case nsIDataType::VTYPE_WSTRING_SIZE_IS: {
			PRUnichar *val;
			PRUint32 size;
			nsresult res= var->GetAsWStringWithSize(&size, &val);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertString(env, nsXPTType(pt), val, size, (jstring &) dst);
			PR_FREEIF(val);
			return res;
		}
		
		case nsIDataType::VTYPE_EMPTY:
		case nsIDataType::VTYPE_VOID:
			dst= 0;
			return NS_OK;

		case nsIDataType::VTYPE_ID: {
			nsID id;
			res= var->GetAsID(&id);
			if (NS_FAILED(res)) 
				return res;
			jstring idString= 0;
			char *idstr= 0;
			idstr= id.ToString();
			nsresult res= X2J_ConvertString(env, nsXPTType::T_CHAR_STR, idstr, 0, idString);
			PR_FREEIF(idstr); // before error check
			if (NS_FAILED(res))
				return res;
//			cls= env->FindClass("org/mozilla/xpcom/nsIDBase");
			cls= JC_FindClass(env, "org/mozilla/xpcom/nsIDBase");
			if (!cls) {
				env->DeleteLocalRef(idString);
				env->ExceptionClear();
				return JC_ERROR_CANT_FIND_CLASS;
			}
			mid= env->GetMethodID(cls, "<init>", "(Ljava/lang/String;)V");
			if (!mid) {
				env->DeleteLocalRef(idString);
				env->DeleteLocalRef(cls);
				env->ExceptionClear();
				return JC_ERROR_CANT_FIND_METHOD;
			}
			dst= env->NewObject(cls, mid, idString);
			env->DeleteLocalRef(idString); // before erro check
			env->DeleteLocalRef(cls);
			if (!dst) {
				env->ExceptionClear();
				return JC_ERROR_CANT_CREATE_OBJECT;
			}
			return NS_OK;
		}

		case nsIDataType::VTYPE_INTERFACE: 
		case nsIDataType::VTYPE_INTERFACE_IS: {
			nsISupports *iface= 0;
			nsIID *iid= 0;
			nsresult res= var->GetAsInterface(&iid, (void **) &iface);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertObject(env, nsXPTType(pt), iface, *iid, dst);
			NS_IF_RELEASE(iface); // before return
			PR_FREEIF(iid);
			return res;
		}

		case nsIDataType::VTYPE_EMPTY_ARRAY: {
//			cls= env->FindClass("java/lang/Object");
			cls= JC_FindClass(env, "java/lang/Object");
			if (!cls) {
				env->ExceptionClear();
				return JC_ERROR_CANT_FIND_CLASS;
			}
			dst= env->NewObjectArray(0, cls, 0);
			env->DeleteLocalRef(cls); // before error check
			if (!dst) {
				env->ExceptionClear();
				return JC_ERROR_CANT_CREATE_OBJECT_ARRAY;
			}
			return NS_OK;
		}

		case nsIDataType::VTYPE_ARRAY: {
			PRUint16 typ;
			nsIID iid;
			PRUint32 cnt;
			void *array= 0;
			nsresult res= var->GetAsArray(&typ, &iid, &cnt, &array);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertArray(env, nsXPTType(typ), array, (int) cnt, iid, (jarray &) dst);
			// Must deref and deallocate
//			cerr << "jcConvert2::ConvertVariant::Deallocation glory" << endl;
			if (typ== nsIDataType::VTYPE_INTERFACE || typ== nsIDataType::VTYPE_INTERFACE_IS)
				for (unsigned int i= 0; i< cnt; ++i)
					NS_IF_RELEASE(*(((nsISupports **) array)+ i));
			if (typ== nsIDataType::VTYPE_CHAR_STR || typ== nsIDataType::VTYPE_STRING_SIZE_IS)
				for (unsigned int i= 0; i< cnt; ++i)
					PR_FREEIF(*(((char **) array)+ i));
			if (typ== nsIDataType::VTYPE_WCHAR_STR || typ== nsIDataType::VTYPE_WSTRING_SIZE_IS)
				for (unsigned int i= 0; i< cnt; ++i)
					PR_FREEIF(*(((PRUnichar **) array)+ i));
			PR_FREEIF(array);
//			cerr << "jcConvert2::ConvertVariant::Deallocation glory done" << endl;
			return res;
		}
	}
	// should not be able to get here at all
	return JC_ERROR_CANT_CONVERT;
}

nsresult J2J_WrapPrimitive(
	JNIEnv *env, nsXPTType pt,
	jvalue v, jobject &dst) {
	
	if (!env)
		return NS_ERROR_NULL_POINTER;
	
	string cl, mn;
	
	switch (pt.TagPart()) {
		case nsXPTType::T_I8:
		case nsXPTType::T_U8:
		case nsXPTType::T_CHAR:
			cl= "java/lang/Byte";
			mn= "(B)V";
			break;
		case nsXPTType::T_I16:
			cl= "java/lang/Short";
			mn= "(S)V";
			break;
		case nsXPTType::T_I32:
		case nsXPTType::T_U32:
			cl= "java/lang/Integer";
			mn= "(I)V";
			break;
		case nsXPTType::T_I64:
		case nsXPTType::T_U64:
			cl= "java/lang/Long";
			mn= "(J)V";
			break;
		case nsXPTType::T_U16:
		case nsXPTType::T_WCHAR:
			cl= "java/lang/Character";
			mn= "(C)V";
			break;
		case nsXPTType::T_FLOAT:
			cl= "java/lang/Float";
			mn= "(F)V";
			break;
		case nsXPTType::T_DOUBLE:
			cl= "java/lang/Double";
			mn= "(D)V";
			break;
		case nsXPTType::T_BOOL:
			cl= "java/lang/Boolean";
			mn= "(Z)V";
			break;
		default:
			dst= v.l;
			return NS_OK;
	}
//	jclass cls= env->FindClass(cl.c_str());
	jclass cls= JC_FindClass(env, cl.c_str());
	if (!cls) {
		env->ExceptionClear();
		return JC_ERROR_CANT_FIND_CLASS;
	}
	jmethodID mid= env->GetMethodID(cls, "<init>", mn.c_str());
	if (!mid) {
		env->ExceptionClear();
		env->DeleteLocalRef(cls);
		return JC_ERROR_CANT_FIND_METHOD;
	}
	dst= env->NewObjectA(cls, mid, &v); // &v is the "array" of args
	env->DeleteLocalRef(cls); // before erro check
	if (!dst) {
		env->ExceptionClear();
		return JC_ERROR_CANT_CREATE_OBJECT;
	}
	return NS_OK;
}

nsresult J2J_UnWrapPrimitive(
	JNIEnv *env, nsXPTType pt, 
	jobject src, jvalue &dst) {

	if (!env)
		return NS_ERROR_NULL_POINTER;
	
	string cl, mn, ms;

	// get relevant information	
	switch (pt.TagPart()) {
		case nsXPTType::T_I8:
		case nsXPTType::T_U8:
		case nsXPTType::T_CHAR:
			cl= "java/lang/Byte";
			mn= "()B";
			ms= "byteValue";
			break;
		case nsXPTType::T_I16:
			cl= "java/lang/Short";
			mn= "()S";
			ms= "shortValue";
			break;
		case nsXPTType::T_I32:
		case nsXPTType::T_U32:
			cl= "java/lang/Integer";
			mn= "()I";
			ms= "intValue";
			break;
		case nsXPTType::T_I64:
		case nsXPTType::T_U64:
			cl= "java/lang/Long";
			mn= "()J";
			ms= "longValue";
			break;
		case nsXPTType::T_U16:
		case nsXPTType::T_WCHAR:
			cl= "java/lang/Character";
			mn= "()C";
			ms= "charValue";
			break;
		case nsXPTType::T_FLOAT:
			cl= "java/lang/Float";
			mn= "()F";
			ms= "floatValue";
			break;
		case nsXPTType::T_DOUBLE:
			cl= "java/lang/Double";
			mn= "()D";
			ms= "doubleValue";
			break;
		case nsXPTType::T_BOOL:
			cl= "java/lang/Boolean";
			mn= "()Z";
			ms= "booleanValue";
			break;
		default:
			dst.l= src;
			return NS_OK;
	}
//	jclass cls= env->FindClass(cl.c_str());
	jclass cls= JC_FindClass(env, cl.c_str());
	if (!cls) {
		env->ExceptionClear();
		return JC_ERROR_CANT_FIND_CLASS;
	}
	jmethodID mid= env->GetMethodID(cls, ms.c_str(), mn.c_str());
	env->DeleteLocalRef(cls); // before error check
	if (!mid) {
		env->ExceptionClear();
		return JC_ERROR_CANT_FIND_METHOD;
	}
	// Unfortunately JNI does not have a CallJNIValueMethod...
	switch (pt.TagPart()) {
		case nsXPTType::T_I8:
		case nsXPTType::T_U8:
		case nsXPTType::T_CHAR:
			dst.b= env->CallByteMethod(src, mid);
			break;
		case nsXPTType::T_I16:
			dst.s= env->CallShortMethod(src, mid);
			break;
		case nsXPTType::T_I32:
		case nsXPTType::T_U32:
			dst.i= env->CallIntMethod(src, mid);
			break;
		case nsXPTType::T_I64:
		case nsXPTType::T_U64:
			dst.j= env->CallLongMethod(src, mid);
			break;
		case nsXPTType::T_U16:
		case nsXPTType::T_WCHAR:
			dst.c= env->CallCharMethod(src, mid);
			break;
		case nsXPTType::T_FLOAT:
			dst.f= env->CallFloatMethod(src, mid);
			break;
		case nsXPTType::T_DOUBLE:
			dst.d= env->CallDoubleMethod(src, mid);
			break;
		case nsXPTType::T_BOOL:
			dst.z= env->CallBooleanMethod(src, mid);
			break;
		default:
			// Whaa? we should not be able to get here
			//return JC_ERROR_YOUR_COMPILER_IS_BROKEN;
			break;
	}
	if (env->ExceptionCheck()) {
		env->ExceptionClear();
		return JC_ERROR_CALL_FAILED;
	}
	return NS_OK;
}

#define J2J_ARRAY_DOWN_PRIMITIVE_CASE(XTYP, JTyp, jtyp, dd) \
case nsXPTType::T_##XTYP: \
	env->Get##JTyp##ArrayRegion((jtyp##Array) src, 0, 1, &dd); \
	if (env->ExceptionCheck()) { \
		env->ExceptionClear(); \
		return JC_ERROR_CANT_GET_ARRAY_ELEMENTS; \
	} \
	return NS_OK;
	
nsresult J2J_ArrayDown(
	JNIEnv *env, nsXPTType pt,
	jarray src, jvalue &dst) {
	
	switch (pt.TagPart()) {
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(I8, Byte, jbyte, dst.b);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(U8, Byte, jbyte, dst.b);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(CHAR, Byte, jbyte, dst.b);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(I16, Short, jshort, dst.s);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(U16, Char, jchar, dst.c);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(WCHAR, Char, jchar, dst.c);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(I32, Int, jint, dst.i);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(U32, Int, jint, dst.i);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(I64, Long, jlong, dst.j);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(U64, Long, jlong, dst.j);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(FLOAT, Float, jfloat, dst.f);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(DOUBLE, Double, jdouble, dst.d);
		J2J_ARRAY_DOWN_PRIMITIVE_CASE(BOOL, Boolean, jboolean, dst.z);
		default:
			dst.l= env->GetObjectArrayElement((jobjectArray) src, 0);
			if (!dst.l) {
				env->ExceptionClear();
				return JC_ERROR_CANT_GET_OBJECT_ARRAY_ELEMENT;
			}
	}
	return NS_OK;
}
#undef J2J_ARRAY_DOWN_PRIMITIVE_CASE

#define J2J_ARRAY_UP_PRIMITIVE_CASE(XTYP, JTyp, jtyp, vsrc) \
case nsXPTType::T_##XTYP: \
	env->Set##JTyp##ArrayRegion( \
		(jtyp##Array) dst, 0, 1, &vsrc \
	); \
	if (env->ExceptionCheck()) { \
		env->ExceptionClear(); \
		return JC_ERROR_CANT_SET_ARRAY_ELEMENTS; \
	} \
	return NS_OK;
	
nsresult J2J_ArrayUp(
	JNIEnv *env, nsXPTType pt,
	jvalue src, jarray dst) {
	
	switch (pt.TagPart()) {
		J2J_ARRAY_UP_PRIMITIVE_CASE(I8, Byte, jbyte, src.b);
		J2J_ARRAY_UP_PRIMITIVE_CASE(I16, Short, jshort, src.s);
		J2J_ARRAY_UP_PRIMITIVE_CASE(I32, Int, jint, src.i);
		J2J_ARRAY_UP_PRIMITIVE_CASE(I64, Long, jlong, src.j);
		J2J_ARRAY_UP_PRIMITIVE_CASE(U8, Byte, jbyte, src.b);
		J2J_ARRAY_UP_PRIMITIVE_CASE(U16, Char, jchar, src.c);
		J2J_ARRAY_UP_PRIMITIVE_CASE(U32, Int, jint, src.i);
		J2J_ARRAY_UP_PRIMITIVE_CASE(U64, Long, jlong, src.j);
		J2J_ARRAY_UP_PRIMITIVE_CASE(FLOAT, Float, jfloat, src.f);
		J2J_ARRAY_UP_PRIMITIVE_CASE(DOUBLE, Double, jdouble, src.d);
		J2J_ARRAY_UP_PRIMITIVE_CASE(CHAR, Byte, jbyte, src.b);
		J2J_ARRAY_UP_PRIMITIVE_CASE(WCHAR, Char, jchar, src.c);
		// Bool is a normal case here
		J2J_ARRAY_UP_PRIMITIVE_CASE(BOOL, Boolean, jboolean, src.z);
		
		// Object case is the same for all of the rest
		default:
			env->SetObjectArrayElement((jobjectArray) dst, 0, src.l);
			if (env->ExceptionCheck()) {
				env->ExceptionClear();
				return JC_ERROR_CANT_SET_ARRAY_ELEMENTS;
			}
	}
	return NS_OK;
}
#undef J2J_ARRAY_UP_PRIMITIVE_CASE
