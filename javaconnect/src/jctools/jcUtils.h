#ifndef _JCUTILS_H
#define _JCUTILS_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "nsCOMPtr.h"
#include <jni.h>
#include <vector>
#include <string>

/**
	Returns the interface name for a given IID
*/
char *JC_GetInterfaceName(nsIID &iid);

/**
	Modify the method name to follow the Java naming convention. Optionally adds
	a "get"/"set" prefix.
	<p>For example, given the method name TryMe, the result will be one of "tryMe",
	"setTryMe" or "getTryMe". The same effect will happen for "tryMe".
	@param mn A reference to the string to fix
	@param isGetter If true, a "get" prefix gets added
	@param isSetter If true, a "set" prefix gets added. Mixing with isGetter is not defined
*/
void JC_FixMethodName(string &mn, bool isGetter, bool isSetter);

/**
	Checks whether a JVM is running and loads it if necessary.
	@return True if a JVM is running or has been successfully loaded. False if
	there was a problem - this is most likely a fatal error that should not be
	ignored.
*/
bool jcEnsureRunningJVM();

/**
	Get a JNI environment object valid for the currently executing thread. The returned
	argument must be passed to jcDropEnv on release.
	<p> A typical usage is as folows:
	<code><pre>
	JNIEnv *env;
	bool detach= jcGetEnv(&env); // Do not forget &
	if (!env) // error situation
	... // use the env
	jcDropEnv(env, detach); // release the environment BEFORE returning
	</pre></code>
	@param out A pointer to a destination JNIEnv *
	@return True if there was need to attach to the current thread, false otherwise
	@see jcDropEnv
*/		
bool jcGetEnv(JNIEnv **out);

/**
	Used in conjunction with jcGetEnv - detaches the given env from the current
	thread if necessary. Eventhough this function is almost always a no-op, you
	should call it in pair with jcGetEnv every time since there are no guarantees
	of how different VMs attach and detach to the current thread.
	@param in The environment to release
	@param detach If false this function is a no-op
*/
void jcDropEnv(JNIEnv *in, bool detach);

/**
	Lists all XPCOM-related interfaces implemented by a given Java class
	@param env A JNI environment object, may be null
	@param className The fully qualified slash-delimited name of a Java class
	@return A vector of strings listing all interface IDs for the supported interfaces
*/
vector<string> jcGetInterfaceIDsForClass(JNIEnv *env, const string &className);

string MapDotToSlash(const string &s);
string MapSlashToDot(const string &s);

/**
	Instantiate a new Java object and initialize with the default constructor.
	Equivalent to env->NewObject(), but will try to load the class through the
	jcAnnotatingClassLoader first.
	@param env A JNI environment object. Must not be null
	@param classname A fully qualified slash-delimited class name
	@return A locally referenced newly constructed jobject
*/
jobject JC_NewObject(JNIEnv *env, const char *classname);

/**
	Loads a Java class using the jcAnnotatingClassLoader if available. Has the
	same effect as env->FindClass, but is guaranteed to cache the result properly
	and will pin the classes in memory.
	@param env A JNI environment object. Must not be null
	@param classname A fully qualified slash-delimited class name
	@return A locally referenced jclass structure representing the class
*/
jclass JC_FindClass(JNIEnv *env, const char *classname);

#endif
