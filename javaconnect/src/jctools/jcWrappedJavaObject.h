/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * The contents of this file are subject to the Mozilla Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is Sun Microsystems,
 * Inc. Portions created by Sun are
 * Copyright (C) 1999 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * Contributor(s):
 * Igor Kushnirskiy <idk@eng.sun.com>
 * Stefan Atev <atevstef@luther.edu> - IBM Internet Technology Group
 */

#ifndef _JCWRAPPEDJAVAOBJECT_H
#define _JCWRAPPEDJAVAOBJECT_H

#include "xptcall.h"
#include "jcTables.h"
#include "nsISupports.h"
#include "nsIInterfaceInfo.h"
#include "nsIClassInfo.h"
#include "jcUtils.h"
#include "jcFactory.h"

#define JCIWRAPPEDJAVAOBJECT_IID \
  {0x53911a5e, 0xf37a, 0x4036, \
    { 0xae, 0xe4, 0x28, 0x90, 0xf8, 0x99, 0xf0, 0x3b }}
    
NS_DEFINE_IID(kjcWrappedJavaObjectIID, JCIWRAPPEDJAVAOBJECT_IID);

#define JC_MAJIC_WRAPPER_IDENTIFIER 0x1337d00d
#define JC_MAJIC_FAILURE_RESPONSE 0x573f4110

class jcWrappedJavaObject :
	public nsXPTCStubBase,
	public nsIClassInfo {
		
	public:
    	NS_DECL_ISUPPORTS
    	NS_DECL_NSICLASSINFO

    	jcWrappedJavaObject(jcClassData *cd);
    	virtual ~jcWrappedJavaObject();
    
		// Stub methods - implement and forget
    	NS_IMETHOD GetInterfaceInfo(nsIInterfaceInfo** info);
    	// call this method and return result
    	NS_IMETHOD CallMethod(
    		PRUint16 methodIndex,
    		const nsXPTMethodInfo* info,
			nsXPTCMiniVariant* params
		);

		// although it's public (so I can keep my sanity), you should
		// not treat it as such. Look into jcMarshalTools.cpp (X2JInterface)
		// to see how to detect whether an XPCOM object is a jcWrappedJavaObject
		jobject mobj;
		
	private:
		friend class jcServices;
		friend class jcFactory;
		friend class jcWrappedJavaInterface;
		
		NS_IMETHOD RemoveWrappedJavaInterface(const nsIID & iid);
		
		NS_IMETHOD ActualCallMethodByIndex(
			const nsIID & iid,
			PRInt16 methodIndex,
			const nsXPTMethodInfo *info,
			nsXPTCMiniVariant * params
		);
		
		// just a reference
		jcClassData *mClassData;
    	nsIInterfaceInfo *mInterfaceInfo;
    	
    	/* Map iids to jcWrappedJavaInterfaces */
    	jcTearOffTable *mInterfaces;
    	
};

#endif
