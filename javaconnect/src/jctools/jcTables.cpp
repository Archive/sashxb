
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "jcTables.h"
#include "jcUtils.h"
#include "prmem.h"
#include <iostream>
#include "nsAutoLock.h"

jcTable::jcTable() : ht(0) {
	mLock= PR_NewLock();
}

jcTable::~jcTable() {
	if (ht) PL_HashTableDestroy(ht);
	ht= 0;
	PR_DestroyLock(mLock);
}

void *jcTable::Lookup(const void *key) {
	nsAutoLock lock(mLock);
	return PL_HashTableLookup(ht, key);
}

PLHashEntry *jcTable::Add(const void *key, void *value) {
	nsAutoLock lock(mLock);
	return PL_HashTableAdd(ht, key, value);
}

bool jcTable::Remove(const void *key) {
	nsAutoLock lock(mLock);
	return (PL_HashTableRemove(ht, key)== PR_TRUE) ? true : false;
}
		
static PRIntn PR_CALLBACK
UnloadAndReleaseModules(PLHashEntry *he, PRIntn i, void *arg) {
    nsIModule *module= NS_STATIC_CAST(nsIModule *, he->value);
    nsIComponentManager *mgr= NS_STATIC_CAST(nsIComponentManager *, arg);
    PRBool canUnload;
    
    if (NS_SUCCEEDED(module->CanUnload(mgr, &canUnload)) && canUnload) {
        NS_RELEASE(module);
	    PR_FREEIF((void *) he->key);
        return HT_ENUMERATE_REMOVE;
    }
    return HT_ENUMERATE_NEXT;
}

jcModulesTable::jcModulesTable(nsIComponentManager *cm) {
	mCompMgr= do_QueryInterface(cm);
	ht= PL_NewHashTable(
    	16, PL_HashString, PL_CompareStrings,
        PL_CompareValues, 0, 0
    );
}

jcModulesTable::~jcModulesTable() {
	PL_HashTableEnumerateEntries(
		ht, UnloadAndReleaseModules, mCompMgr
	);
}

static PRIntn PR_CALLBACK
FreeClassData(PLHashEntry *he, PRIntn i, void *arg)
{
    delete NS_STATIC_CAST(jcClassData *, he->value);
    return HT_ENUMERATE_REMOVE;
}

jcClassDataTable::jcClassDataTable() {
	ht= PL_NewHashTable(
    	256, PL_HashString, PL_CompareStrings,
        PL_CompareValues, 0, 0
    );
}

jcClassDataTable::~jcClassDataTable() {
	PL_HashTableEnumerateEntries(
		ht, FreeClassData, 0
	);
}

/*
	"Hash" function for jobjects
*/
PR_EXTERN(PLHashNumber) PL_HashJObject(const void *key) {
	JNIEnv *env= 0;
	bool detach= jcGetEnv(&env);
	if (!env) return ((PLHashNumber ) key);
//	jclass cls= env->FindClass("java/lang/Object");
	// Use so-called "identityHash"
	jclass cls= JC_FindClass(env, "java/lang/Object");
	if (!cls) {
		jcDropEnv(env, detach);
		return ((PLHashNumber ) key);
	}
	jmethodID mid= env->GetMethodID(cls, "hashCode", "()I");
	if (!mid) {
		env->DeleteLocalRef(cls);
		jcDropEnv(env, detach);
		return ((PLHashNumber ) key);
	}
	int rv= env->CallNonvirtualIntMethod((jobject) key, cls, mid);
	if (env->ExceptionCheck()) env->ExceptionClear();
	env->DeleteLocalRef(cls);
	jcDropEnv(env, detach);
	return rv;
}

PR_EXTERN(PRIntn) PL_CompareJObject(const void *v1, const void *v2) {
	JNIEnv *env= 0;
	bool detach= jcGetEnv(&env);
	if (!env) return -1;
	jboolean rv= env->IsSameObject((jobject) v1, (jobject) v2);
	if (env->ExceptionCheck()) env->ExceptionClear();
	jcDropEnv(env, detach);
	return rv;
}

static PRIntn PR_CALLBACK
ReleaseWrappers(PLHashEntry *he, PRIntn i, void *arg) {
    return HT_ENUMERATE_REMOVE;
}

jcWrappersTable::jcWrappersTable() {
	ht= PL_NewHashTable(
		256, PL_HashJObject,
		PL_CompareJObject, PL_CompareValues, 0, 0
	);
}

jcWrappersTable::~jcWrappersTable() {
	PL_HashTableEnumerateEntries(ht, ReleaseWrappers, 0);
}

PR_EXTERN(PLHashNumber) PL_HashIID(const void *key) {
	return (PLHashNumber) (((nsIID *) key)->m0);
}

PR_EXTERN(PRIntn) PL_CompareIIDs(const void *v1, const void *v2) {
	return ((nsIID *) v1)->Equals(*( (nsIID *) v2));
}

static PRIntn PR_CALLBACK
ReleaseTearOffs(PLHashEntry *he, PRIntn i, void *arg) {
	delete ((nsIID *) he->key);
    return HT_ENUMERATE_REMOVE;
}

jcTearOffTable::jcTearOffTable() {
    ht= PL_NewHashTable(
    	16, PL_HashIID, PL_CompareIIDs,
    	PL_CompareValues, 0, 0
    );
}

jcTearOffTable::~jcTearOffTable() {
	PL_HashTableEnumerateEntries(ht, ReleaseTearOffs, 0);
}

PLHashEntry *jcTearOffTable::Add(const void *key, void *value) {
	nsAutoLock lock(mLock);
	nsIID *keyCopy= new nsIID(*((nsIID *) key));
	return PL_HashTableAdd(ht, keyCopy, value);
}

bool jcTearOffTable::Remove(const void *key) {
	nsAutoLock lock(mLock);
	PLHashNumber keyHash= PL_HashIID(key);
	PLHashEntry **hep= PL_HashTableRawLookup(ht, keyHash, key);
	PLHashEntry *he= *hep;
	if (!he) return false;
	nsIID *deathRow= (nsIID *) he->key;
	bool result= (PL_HashTableRemove(ht, key)== PR_TRUE) ? true : false;
	delete deathRow;
	return result;
}

/*
	"Hash" function for ISupports
*/
PR_EXTERN(PLHashNumber) PL_HashISupports(const void *key) {
	return ((PLHashNumber ) key);
}

PR_EXTERN(PRIntn) PL_CompareAlwaysEqual(const void *key1, const void *key2) {
	// always say things are equal
	return true;
}

static PRIntn PR_CALLBACK
ReleaseNatives(PLHashEntry *he, PRIntn i, void *arg) {

    nsISupports *val= (nsISupports *) he->key;
    NS_IF_RELEASE(val);
    jobject *obj= (jobject *) he->value;
    ((JNIEnv *) arg)->DeleteWeakGlobalRef(*obj);
    return HT_ENUMERATE_REMOVE;
}

jobject jcNativeProxySet::popFirstObj() {
	jobject res;
	if (proxies.empty())
		return 0;
	res= proxies.front().first;
	proxies.pop_front();
	return res;
}
		
jobject jcNativeProxySet::objForIID(const char *iidStr) {
	list<pair<jobject, string> >::iterator i;
	for (i= proxies.begin(); i!= proxies.end(); ++i)
		if (i->second== iidStr)
			return i->first;
	return 0;
}

// if it returns null, the object was newly added		
jobject jcNativeProxySet::objForIIDWithAdd(const char *iidStr, jobject pxy) {
	list<pair<jobject, string> >::iterator i;
	for (i= proxies.begin(); i!= proxies.end(); ++i)
		if (i->second== iidStr)
			return i->first;
	// push at front to make potential second lokup faster
	// this is NOT MostRecentlyUsed queue! This trick just
	// helps in the more common case that we ask for an interface
	// call a method or two and then release it
	proxies.push_front(pair<jobject, string>(pxy, iidStr));
	return 0;
}
		
// returns the object so that you can deref it, etc.
jobject jcNativeProxySet::objForIIDWithRemove(const char *iidStr) {
	jobject res;
	list<pair<jobject, string> >::iterator i;
	for (i= proxies.begin(); i!= proxies.end(); ++i)
		if (i->second== iidStr) {
			res= i->first;
			proxies.erase(i);
			return res;
		}
	return 0;
}

jcNativesTable::jcNativesTable() {
	ht= PL_NewHashTable(
		256, PL_HashISupports,
		PL_CompareValues, PL_CompareAlwaysEqual, 0, 0
	);
}

jcNativesTable::~jcNativesTable() {
    JNIEnv *env;
    bool detach= jcGetEnv(&env);
	PL_HashTableEnumerateEntries(ht, ReleaseNatives, env);
    jcDropEnv(env, detach);
}

static PRIntn PR_CALLBACK
GlobalDeref(PLHashEntry *he, PRIntn i, void *arg) {

    ((JNIEnv *) arg)->DeleteWeakGlobalRef((jobject) he->value);
    return HT_ENUMERATE_REMOVE;
}

jcClassTable::jcClassTable() {
	ht= PL_NewHashTable(
		256, PL_HashString,
		PL_CompareStrings, PL_CompareValues, 0, 0
	);
}

jcClassTable::~jcClassTable() {
    JNIEnv *env;
    bool detach= jcGetEnv(&env);
	PL_HashTableEnumerateEntries(ht, GlobalDeref, env);
    jcDropEnv(env, detach);
}
