#ifndef _JC_TABLES_H
#define _JC_TABLES_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "plhash.h"
#include "nsIModule.h"
#include "nsIComponentManager.h"
#include "nsCRT.h"
#include "nsID.h"
#include <list>
#include <pair.h>
#include <string>
#include "jcUtils.h"
#include "prlock.h"
#include "nsIInterfaceInfo.h"
#include "xptinfo.h"
#include "prthread.h"
#include "nsIEventQueue.h"
#include "nsIEventQueueService.h"
#include "jcClassData.h"

/**
	A wrapper around PL_HashTable provided by the Mozilla portable runtime. Methods
	on the table are thread-safe.
*/
class jcTable {

	public:
	
		jcTable();
		virtual ~jcTable();
		
		virtual void *Lookup(const void *key);
		virtual PLHashEntry *Add(const void *key, void *value);
		virtual bool Remove(const void *key);
		
	protected:
	
		PLHashTable *ht;
		// Tables should be thread safe
		PRLock *mLock;
		
};

/**
	Used by the component loader to cache module objects
*/
class jcModulesTable: public jcTable {

	public:

		jcModulesTable(nsIComponentManager *cm);
		virtual ~jcModulesTable();
		
	private:
		nsCOMPtr<nsIComponentManager> mCompMgr;
		
};

/**
	Used to map class names to a class data structure
*/
class jcClassDataTable: public jcTable {

	public:
		jcClassDataTable();
		virtual ~jcClassDataTable();
};

/**
	Maps jobjects to jcWrappedJavaObjects
*/
class jcWrappersTable: public jcTable {

	public:

		jcWrappersTable();
		virtual ~jcWrappersTable();
		
};

/**
	All information needed for one call into an XPCOM object. This is used by jcServices
	when an method calls happens from a thread different than the native "safe thread".
	See jcjava/org_mozilla_xpcom_jcJavaservices.cpp for details
*/
class jcNativeCallInfo {
	public:
		char *m_methodName;
		nsIInterfaceInfo *m_interfaceInfo;
		const nsXPTMethodInfo *m_methodInfo;
		PRUint16 m_methodIndex;
		JNIEnv *m_env;
		jobjectArray m_args;
		nsISupports *m_obj;
		jobject *m_result;
		
		jcNativeCallInfo(
			char *methodName,
			nsIInterfaceInfo *interfaceInfo,
			const nsXPTMethodInfo *methodInfo,
			PRUint16 methodIndex,
			JNIEnv *env,
			jobjectArray args,
			nsISupports *obj,
			jobject *result
		) :
			m_methodName(methodName),
			m_interfaceInfo(interfaceInfo),
			m_methodInfo(methodInfo),
			m_methodIndex(methodIndex),
			m_env(env),
			m_args(args),
			m_obj(obj),
			m_result(result) {};
};

/**
	Holds a list of Java proxies for native objects and their corresponding
	interface IDs. This is a complication that arises when some of an object's
	interfaces are not known at the time a proxy is created. Happens with most
	XPCOM objects since they do not provide nsIClassInfo. All SashXB objects
	provide ClassInfo, so there would be only one element in the list.
*/
class jcNativeProxySet {
	
	public:
		
		// I'm using a list since it has constant amortized inserts and
		// deletes and I don't need constant time random access
		list<pair<jobject, string> > proxies;
		
		// Marks the thread from which it is safe to call the object
		// Might eventually be a list in case we notice that it is
		// safe to call some object from more than one thread
		PRThread *safeThread;
		
		nsIEventQueue *eventQueue;

		jcNativeProxySet() : safeThread(0), eventQueue(0) {
		}
	
		~jcNativeProxySet() {
		}
		
		bool isEmpty() {
			return proxies.empty();
		}

		jobject popFirstObj();
		jobject objForIID(const char *iidStr);
		jobject objForIIDWithAdd(const char *iidStr, jobject pxy);
		jobject objForIIDWithRemove(const char *iidStr);
		
		
};

/**
	The table that maps an XPCOM object to the jcNativesProxySet for it
*/
class jcNativesTable: public jcTable {

	public:

		jcNativesTable();
		virtual ~jcNativesTable();
		
};

/**
	Maps class names to globally referenced jclass JNI structures. Used by
	JC_FindClass() in jctools/jcUtils.cpp
*/
class jcClassTable: public jcTable {

	public:

		jcClassTable();
		virtual ~jcClassTable();
		
};

/**
	Maps an IID to a jcWrappedJavaInterface object. Each jcWrappedJavaObject
	contains one of these.
*/
class jcTearOffTable: public jcTable {

	public:

		jcTearOffTable();
		virtual ~jcTearOffTable();

		virtual PLHashEntry *Add(const void *key, void *value);
		virtual bool Remove(const void *key);
		
};

#endif
