
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "jcUtils.h"
#include "nsIInterfaceInfoManager.h"
#include "xptinfo.h"
#include "prmem.h"
#include "prenv.h"
#include "jcConvert.h"

static JavaVM *jvm= 0;

bool jcGetEnv(JNIEnv **out) {
	if (!jcEnsureRunningJVM()) {
		*out= 0;
		return false;
	}
	JNIEnv *result= 0;
	jint rv= jvm->GetEnv((void **) &result, JNI_VERSION_1_2);
	if (rv!= JNI_OK) {
		*out= 0;
		return false;
	}
	if (result) {
		*out= result;
		return false;
	}
	cerr << "Attaching JVM to current thread" << endl;
	rv= jvm->AttachCurrentThread((void **) &result, NULL);
	if (rv!= JNI_OK) {
		*out= 0;
		return false;
	}
	*out= result;
	return true;
}

void jcDropEnv(JNIEnv *in, bool detach) {
	if (detach) {
		cerr << "Detaching from thread" << endl;
		jvm->DetachCurrentThread();
	}
}

bool jcEnsureRunningJVM() {
	jint res;
	jsize rv;
	JavaVM *vms[2];
	res= JNI_GetCreatedJavaVMs((JavaVM **) vms, 2, &rv);
	// res< 0 if there was an error
	if (res< 0) return false;
	// rv= number of jvms
	if (rv> 0) {
		// This is important, I was happily crashing if a VM already
		// existed
		if (!jvm) {
			jvm= vms[0]; // get first jvm
		}
		return true;
	}
	// Now we need to instantiate a VM
	JavaVM *vm;
	JavaVMInitArgs vm_args;
	JNIEnv *env;
	JavaVMOption options[3];
	
	const char *classpath= PR_GetEnv("CLASSPATH");
	string classStr= "";
	if (classpath) classStr= classpath;
//	PR_FREEIF(classpath);
	cerr << "Starting JVM with CLASSPATH=" << classStr << endl;
	classStr= "-Djava.class.path="+ classStr;

	const char *jcjarpath= PR_GetEnv("JAVACONNECTJARPATH");
	string jcjarStr= "";
	if (jcjarpath) jcjarStr= jcjarpath;
//	PR_FREEIF(jcjarpath);
	cerr << "Starting JVM with javaconnect.jar.path=" << jcjarStr << endl;
	jcjarStr= "-Djavaconnect.jar.path="+ jcjarStr;

	options[0].optionString= (char *) classStr.c_str();
	options[1].optionString= (char *) jcjarStr.c_str();
//	options[1].optionString= ""; // "-verbose:jni";
	options[2].optionString= "-Djdbc.drivers=org.gjt.mm.mysql.Driver";
	vm_args.version= 0x00010002;
	vm_args.options= options;
	vm_args.nOptions= 3;
	vm_args.ignoreUnrecognized= JNI_TRUE;
	cerr << "Starting JVM..." << endl;
	res= JNI_CreateJavaVM(&vm, (void **) &env, &vm_args);
	if (res< 0) {
		cerr << "FAILED" << endl;
		jvm= 0;
		return false;
	}
	cerr << "[JVM loaded at " << vm << "]" << endl;
	jvm= vm;
#ifdef SASH_JAVA_EXTRAS
	// all failures are silent here
	jclass cls= env->FindClass("org/mozilla/xpcom/SashSecurityManager");
	if (!cls) {
		cerr << "Sash security manager class not found" << endl;
		env->ExceptionClear();
		return true;
	}
	jmethodID mid= env->GetMethodID(cls, "<init>", "()V");
	if (!mid) {
		cerr << "Sash security manager has no default constructor" << endl;
		env->ExceptionClear();
		env->DeleteLocalRef(cls);
		return true;
	}
	jobject secMan= env->NewObject(cls, mid);
	env->DeleteLocalRef(cls); // before error check
	if (!secMan) {
		cerr << "Sash security manager cannot be instantiated" << endl;
		env->ExceptionClear();
		return true;
	}
	cls= env->FindClass("java/lang/System");
	if (!cls) {
		cerr << "Cannot access java.lang.System??? Pigs will fly" << endl;
		env->ExceptionClear();
		env->DeleteLocalRef(secMan);
		return true;
	}
	mid= env->GetStaticMethodID(cls, "setSecurityManager", "(Ljava/lang/SecurityManager;)V");
	if (!mid) {
		cerr << "Cannot access setSecurityManager method" << endl;
		env->ExceptionDescribe();
		env->ExceptionClear();
		env->DeleteLocalRef(secMan);
		env->DeleteLocalRef(cls);
		return true;
	}
	env->CallStaticVoidMethod(cls, mid, secMan);
	env->DeleteLocalRef(secMan); // before erro check
	env->DeleteLocalRef(cls); // ditto
	if (env->ExceptionCheck()) {
		env->ExceptionClear();
		return true;
	}
#endif
	return true;
}
		
vector<string> jcGetInterfaceIDsForClass(JNIEnv *env, const string &className) {
	vector<string> result;
	
	bool detach= false;
	if (!env)
		detach= jcGetEnv(&env);
	if (!env)
		return result; // this is really bad case, no JVM is available

	jclass cls= 0, clsP= 0, clsI= 0;
	jmethodID mid, midP;
	jfieldID fidI;
	jobjectArray array;
	jsize arrayLen;
	
	// no need for heroic manual deref here
	env->PushLocalFrame(16);

//	cls= env->FindClass(className.c_str());
	cls= JC_FindClass(env, className.c_str());
	if (!cls) goto abort;
	
//	clsP= env->FindClass("java/lang/Class");
	clsP= JC_FindClass(env, "java/lang/Class");
	if (!clsP) goto abort;

//	clsI= env->FindClass("org/mozilla/xpcom/nsIDBase");
	clsI= JC_FindClass(env, "org/mozilla/xpcom/nsIDBase");
	if (!clsI) goto abort;

	mid= env->GetMethodID(clsP, "getInterfaces", "()[Ljava/lang/Class;");
	if (!mid) goto abort;
	
	midP= env->GetMethodID(clsP, "getName", "()Ljava/lang/String;");
	if (!midP) goto abort;
	
	fidI= env->GetFieldID(clsI, "id", "Ljava/lang/String;");
	if (!fidI) goto abort;
	
	array= (jobjectArray) env->CallObjectMethod(cls, mid);
	if (!array) goto abort;

	arrayLen= env->GetArrayLength(array);
	if (env->ExceptionCheck()) goto abort;

	for (int i= 0; i< arrayLen; ++i) {
		jclass interface= (jclass) env->GetObjectArrayElement(array, i);
		if (!interface) goto abort;
		jfieldID fid= env->GetStaticFieldID(interface, "IID", "Lorg/mozilla/xpcom/IID;");
		if (!fid) { // interface is not XPCOM interface
			if (env->ExceptionCheck())
				env->ExceptionClear();
			env->DeleteLocalRef(interface);
		}
		else {
			jobject IIDObj= env->GetStaticObjectField(interface, fid);
			env->DeleteLocalRef(interface); // before error check
			if (!IIDObj) goto abort;
			
			jstring IIDStr= (jstring) env->GetObjectField(IIDObj, fidI);
			env->DeleteLocalRef(IIDObj); // before error check
			if (!IIDStr) goto abort;
			
			char *iidStr= (char *) env->GetStringUTFChars(IIDStr, 0);
			if (!iidStr) {
				env->DeleteLocalRef(IIDStr);
				goto abort;
			}
			
			string iidFinal= iidStr;
			env->ReleaseStringUTFChars(IIDStr, iidStr);
			env->DeleteLocalRef(IIDStr);
			
			if (iidFinal[0]!= '{')
				iidFinal= "{"+ iidFinal;
			if (iidFinal[iidFinal.length()- 1]!= '}')
				iidFinal+= "}";

			result.push_back(iidFinal);
		}
	}
	
abort:
	if (env->ExceptionCheck())
		env->ExceptionDescribe(); // must be before PopLocalFrame
	env->PopLocalFrame(0);
	jcDropEnv(env, detach);
	return result;
}

string MapDotToSlash(const string &s) {
	string result= s;
	for (unsigned int i= 0; i< result.length(); ++i)
		if (result[i]== '.') result[i]= '/';
	return result;
}

string MapSlashToDot(const string &s) {
	string result= s;
	for (unsigned int i= 0; i< result.length(); ++i)
		if (result[i]== '/') result[i]= '.';
	return result;
}

// caller frees
char *JC_GetInterfaceName(nsIID &iid) {
	char *strName= 0;
	nsIInterfaceInfoManager* mgr= 0;
	mgr= XPTI_GetInterfaceInfoManager();
	if (!mgr)
		return strName;
	mgr->GetNameForIID(&iid, &strName);
	NS_RELEASE(mgr);
	return strName;
}

void JC_FixMethodName(string &mn, bool isGetter, bool isSetter) {
	if (isGetter) {
		if (mn.length()> 0 && mn[0]>= 'a' && mn[0]<= 'z')
			mn[0]= mn[0]- 'a'+ 'A';
		mn= "get"+ mn;
		return;
	}
	if (isSetter) {
		if (mn.length()> 0 && mn[0]>= 'a' && mn[0]<= 'z')
			mn[0]= mn[0]- 'a'+ 'A';
		mn= "set"+ mn;
		return;
	}
	if (mn.length()> 0 && mn[0]>= 'A' && mn[0]<= 'Z')
		mn[0]+= 'a'- 'A';
	return;
}

// Create a new instance of the class specified by ClassName,
// going through our own ClassLoader
jobject JC_NewObject(JNIEnv *env, const char *classname) {
	jclass objclass= JC_FindClass(env, classname);
	if (!objclass) {
		// exception must be handled by caller
		return 0;
	}
	// Obtain default constructor for objclass
	jmethodID mid= env->GetMethodID(objclass, "<init>", "()V");
	if (!mid) {
		env->DeleteLocalRef(objclass);
		// exception must be handled by caller
		return 0;
	};
	// Create the new object
	jobject obj= env->NewObject(objclass, mid);
	env->DeleteLocalRef(objclass); // before error check
	// exception must be handled by caller
	return obj;
}

jclass JC_FindClass(JNIEnv *env, const char *classname) {
	nsresult res;
	bool cache= false;
	nsCOMPtr<jcIServices> jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
	if (NS_SUCCEEDED(res)) {
		jclass cached;
		res= jcs->FindGlobalClass(classname, &cached);
		if (NS_SUCCEEDED(res) && cached)
			return (jclass) env->NewLocalRef(cached); // must be local ref in caller's frame
		cache= true;
	}
	// load java services
	jclass servcls= env->FindClass("org/mozilla/xpcom/jcJavaServices");
	if (!servcls) {
		// exception must be handled by caller
		return 0;
	};
	// classLoader= jcJavaServices.getJCClassLoader()
	jmethodID getjcloader= env->GetStaticMethodID(servcls, "getJCClassLoader", "()Ljava/lang/ClassLoader;");
	if (!getjcloader) {
		env->DeleteLocalRef(servcls);
		// exception must be handled by caller
		return 0;
	};
	jobject classLoader= env->CallStaticObjectMethod(servcls, getjcloader);
	env->DeleteLocalRef(servcls); // before error check
	if (!classLoader) {
		// exception must be handled by caller
		return 0;
	};
	// objclass= classLoader.findClass(classname)
	jclass classLoaderClass= env->FindClass("java/lang/ClassLoader");
	if (!classLoaderClass) {
		env->DeleteLocalRef(classLoader);
		// exception must be handled by caller
		return 0;
	};
	jmethodID findClass= env->GetMethodID(
		classLoaderClass,
		"findClass",
		"(Ljava/lang/String;)Ljava/lang/Class;"
	);
	if (!findClass) {
		env->DeleteLocalRef(classLoader);
		env->DeleteLocalRef(classLoaderClass);
		// exception must be handled by caller
		return 0;
	};
	jstring classNameJStr;
	res= X2J_ConvertString(env, nsXPTType::T_CHAR_STR, (void *) classname, 0, classNameJStr);
	if (NS_FAILED(res)) {
		env->DeleteLocalRef(classLoader);
		env->DeleteLocalRef(classLoaderClass);
		return 0;
	}
	jobject objclass= env->CallObjectMethod(classLoader, findClass, classNameJStr);
	env->DeleteLocalRef(classLoader); // before error check
	env->DeleteLocalRef(classLoaderClass);
	env->DeleteLocalRef(classNameJStr);
	if (cache) {
		jclass globcls= (jclass) env->NewGlobalRef(objclass);
		res= jcs->CacheGlobalClass(classname, globcls);
	}
	return (jclass) objclass;
}
