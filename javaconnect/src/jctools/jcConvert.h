#ifndef _JC_CONVERT_2_H
#define _JC_CONVERT_2_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "jcUtils.h"
#include "nsString.h"
#include "xptinfo.h"
#include "jcServices.h"
#include "jcWrappedJavaObject.h"
#include "jcWrappedJavaInterface.h"
#include "nsIServiceManager.h"
#include "nsIVariant.h"

/**
	Convert an XPCOM primitive value to a Java value.
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param src A pointer to a primitive value (i.e. int *, float *)
	@param dst A reference to a jvalue union
	@return A standart XPCOM error code or one of the codes in jcError.h
	@see "jctools/jcJavaCall.cpp::PreCallIn"
*/
nsresult X2J_ConvertPrimitive(
	JNIEnv *env, nsXPTType pt,
	void *src, jvalue &dst);

/**
	Convert a Java value to an XPCOM primitive.
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param src A jvalue union
	@param dst A pointer to a primitive type (i.e. int *, float *)
	@return A standart XPCOM error code or one of the codes in jcError.h
*/
nsresult J2X_ConvertPrimitve(
	JNIEnv *env, nsXPTType pt,
	jvalue src, void *dst);

/**
	Convert an XPCOM or C String to a Java string. Single-byte characters and UTF8
	will be promoted (or demoted) to 16-bit Unicode.
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param src A pointer to a nsAString descendant (for Mozilla strings) or
	a C-string value (char *). See example
	@param size The length of the string to convert. Only meaningful (and required)
	when converting T_PSTRING_SIZE_IS and T_PWSTRING_SIZE_IS.
	@param dst A reference to a jstring structure
	@return A standart XPCOM error code or one of the codes in jcError.h
	<p>Example:
	<code><pre>
	nsString x= NS_LITERAL_STRING("Hello World");
	jstring jx;
	X2J_ConvertString(env, nsXPTType::T_ASTRING, &x, 0, jx); //  note & for x
	char *y= PL_strdup("Hello World");
	jstring jy;
	X2J_ConvertString(env, nsXPTType::T_CHAR_STR, y, 0, jy); //  note NO & for y
	PR_FREEIF(y);
	</pre></code>
*/
nsresult X2J_ConvertString(
	JNIEnv *env, nsXPTType pt,
	void *src, int size,
	jstring &dst);
	
/**
	Convert a Java string to an XPCOM or C string. Possible loss of accuracy
	is to be expected when converting to string types that cannot hold 16-bit
	Unicode values.
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param src A jstring value
	@param size A reference to an integer that will hold the length of the converted
	string after successful invocation.
	@param dst A pointer to a nsAstring descendant or a pointer to a character array.
	Pointers to nsAStrings must be valid, pointers to character arrays will be allocated
	by the method. See example
	@return A standart XPCOM error code or one of the codes in jcError.h
	<p>Example: jstr is a jstring
	<code><pre>
	nsString x;
	int size;
	J2X_ConvertString(env, nsXPTType::T_ASTRING, jstr, size, &x); //  note & for x
	char *y= 0; // will be allocated by conversion routine
	J2X_ConvertString(env, nsXPTType::T_CHAR_STR, jstr, size, &y); //  note & for y too
	</pre></code>
*/
nsresult J2X_ConvertString(
	JNIEnv *env, nsXPTType pt,
	jstring src, int &size,
	void *dst);
	
/**
	Convert an xpcom IID object to the correcponding org.mozilla.xpcom.IID object
	@param env A JNI environment object, cannot be null
	@param iid The nsIID to convert
	@param dst A reference to a jobject that will hold the result
	@return A standart XPCOM error code or one of the codes in jcError.h
	<p>Example:
	<code><pre>
	jobject ISupportsIID;
	X2J_ConvertIIDObject(env, NS_GET_IID(nsISupports), ISupportsIID);
	</pre></code>
*/
nsresult X2J_ConvertIIDObject(
	JNIEnv *env, const nsIID &iid, jobject &dst);

/**
	Convert a org.mozilla.xpcom.IID object to the correcponding xpcom IID object
	@param env A JNI environment object, cannot be null
	@param src A jobject representing a org.mozilla.xpcom.nsIDBase descendant
	@param dst A reference to a nsIID object
	@return A standart XPCOM error code or one of the codes in jcError.h
	<p>Example: jiid is a jobject holding an IID
	<code><pre>
	nsIID someId;
	J2X_ConvertIIDObject(env, jiid, someId);
	if (someId.Equals(NS_GET_IID(nsISupports))) ... // you get the idea
	</pre></code>
*/
nsresult J2X_ConvertIIDObject(
	JNIEnv *env, jobject src, nsIID &iid);

/**
	Convert an XPCOM array to a Java array. Note that even though nsAString descendants
	cannot be in an XPCOM array, you can still convert nsAString * with this function
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h describing the type of the array ELEMENTS
	@param src The array to convert (i.e. nsString *, char ** /C strings/, int *,
	nsISupports *)
	@param size The number of elements in the array
	@param iid An IID used when converting arrays of interface pointers (T_INTERFACE,
	T_INTERFACE_IS)
	@param dst A reference to a jarray structure
	@return A standart XPCOM error code or one of the codes in jcError.h
	<p>Example:
	<code><pre>
	jarray result;
	nsString x[2]; // x is like nsString *
	nsIID dummyIID;
	x[0]= NS_LITERAL_STRING("Hi");
	x[1]= NS_LITERAL_STRING("There");
	X2J_ConvertArray(env, nsXPTTYpe::T_ASTRING, x, 2, dummyIID, result);
	...
	jarray result;
	char *x[2]; // x is like char **
	nsIID dummyIID;
	x[0]= PL_strdup("Hi");
	x[1]= PL_strdup("There");
	X2J_ConvertArray(env, nsXPTTYpe::T_CHAR_STR, x, 2, dummyIID, result);
	...
	jarray result;
	PRInt16 x[2]; // x is like int *
	nsIID dummyIID;
	x[0]= 3;
	x[1]= 4;
	X2J_ConvertArray(env, nsXPTTYpe::T_I16, x, 2, dummyIID, result);
	...
	jarray result;
	nsIVariant x[2]; // x is like nsIVariant *
	x[0]= ...
	x[1]= ...
	X2J_ConvertArray(env, nsXPTTYpe::T_INTERFACE, x, 2, NS_GET_IID(nsIVariant), result);
	</pre></code>
*/
nsresult X2J_ConvertArray(
	JNIEnv *env, nsXPTType pt,
	void *src, int size, nsIID &iid, 
	jarray &dst);

/**
	Convert a Java array to an XPCOM array. Not that you CANNOT use this function
	to convert to nsAString *. This is different than X2J_ConvertArray
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h describing the type of the array ELEMENTS
	@param src The jarray to convert
	@param size A reference to an integer that will hold the number of elements in
	the array after successful invocation
	@param iid An IID used when converting arrays of interface pointers (T_INTERFACE,
	T_INTERFACE_IS). Note that this is NOT a returned value, you need to specify it
	in advance
	@param dst A pointer to an array (i.e. int **, char *** /C strings/). This array
	will be allocated by the function
	@return A standart XPCOM error code or one of the codes in jcError.h
*/
nsresult J2X_ConvertArray(
	JNIEnv *env, nsXPTType pt,
	jarray src, int &size, nsIID &iid, 
	void **dst);

/**
	Convert a java object to an XPCOM nsISupports object
	iid must be meaningful
	Variant objects will be redirected to J2X_ConvertVariant
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param src The jobject to convert
	@param iid The IID of the object - not a return value. Note that if iid is
	for nsIVariant or nsIWritableVariant the call will be redirected to J2X_ConvertVariant
	@param dst A pointer to an interface pointer. See example
	@return A standart XPCOM error code or one of the codes in jcError.h
	<p>Example: jobj is a Java object
	<code><pre>
	nsISupports *x;
	J2X_ConvertObject(env, nsXPTType::T_INTERFACE, jobj, NS_GET_IID(nsILocalFile), &x); // note & for x
	</pre></code>
*/
nsresult J2X_ConvertObject(
	JNIEnv *env, nsXPTType pt,
	jobject src, nsIID &iid,
	void *dst);

/**
	Convert an XPCOM object to a Java object
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param src An interface pointer
	@param iid The IID of the object - not a return value. Note that if iid is
	for nsIVariant or nsIWritableVariant the call will be redirected to X2J_ConvertVariant
	@param dst A reference to a jobject
	@return A standart XPCOM error code or one of the codes in jcError.h
*/
nsresult X2J_ConvertObject(
	JNIEnv *env, nsXPTType pt,
	void *src, nsIID &iid,
	jobject &dst);

/**
	Convert a generic Java object to an XPCOM variant
	@param env A JNI environment object, cannot be null
	@param src A jobject to convert - Use object holders for primitive types (i.e.
	java/lang/Integer will create a variant holding an PRInt16)
	@param dst PRE-ALLOCATED Writable variant object. It may be easier to just call
	X2J_ConvertObject, which will correctly forward the request to this method
	@return A standart XPCOM error code or one of the codes in jcError.h
*/
nsresult J2X_ConvertVariant(
	JNIEnv *env, jobject src, nsIWritableVariant *dst
);

/**
	Convert an XPCOM variant to a generic Java object
	@param env A JNI environment object, cannot be null
	@param src A variant
	@param dst a reference to a jobject. Primitive types will be converted to
	Java holder objects (i.e. a variant containing PRBool will be converted to
	java/lang/Boolean)
	@return A standart XPCOM error code or one of the codes in jcError.h
	<p><em>Note:</em> Arrays in Java are objects - you can cast Object to char[] for
	example. When consuming variants in Java-land, always check the type with
	instanceof.
*/
nsresult X2J_ConvertVariant(
	JNIEnv *env, void *src, jobject &dst);
	
/**
	Wrap a primitive java type in a Java holder object. See example.
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param v A jvalue
	@param dst A reference to a jobject
	@return A standart XPCOM error code or one of the codes in jcError.h
	<code><pre>
	jvalue val;
	val.f= 3.14; // float value
	jobject floatObj;
	// equivalent to Float floatObj= new Float(3.14) in Java
	J2J_WrapPrimitive(env, nsXPTType::T_FLOAT, val, floatObj);
	</pre></code>
*/
nsresult J2J_WrapPrimitive(
	JNIEnv *env, nsXPTType pt, 
	jvalue v, jobject &dst);

/**
	Get the underlying primitive value from a java holder object
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param src A jobject
	@param dst A reference to a jvalue
	@return A standart XPCOM error code or one of the codes in jcError.h
	<p>Example: floatObj is a java/lang/Float object
	<code><pre>
	jvalue res;
	// equivalent to float res_f= floatObj.floatValue() in Java
	J2J_UnWrapPrimitive(env, nsXPTType::T_FLOAT, floatObj, res);
	float res_f= res.f;
	</pre></code>
*/
nsresult J2J_UnWrapPrimitive(
	JNIEnv *env, nsXPTType pt, 
	jobject src, jvalue &dst);
	
/**
	The equivalent of the Java code:
	<code><pre>
		type[] src;
		type dst= src[0];
	</pre></code>
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param src A jarray
	@param dst A reference to a jvalue
	@return A standart XPCOM error code or one of the codes in jcError.h
*/
nsresult J2J_ArrayDown(
	JNIEnv *env, nsXPTType pt,
	jarray src, jvalue &dst);

/**
	The equivalent of:
	<code><pre>
		type src; // any Java type
		type[] dst= new type[1];
		dst[0]= src;
	</pre></code>
	The resulting array will have length 1
	@param env A JNI environment object, cannot be null
	@param pt A type constant from xptinfo.h
	@param src A jvalue
	@param dst A reference to a jarray
	@return A standart XPCOM error code or one of the codes in jcError.h
*/
nsresult J2J_ArrayUp(
	JNIEnv *env, nsXPTType pt,
	jvalue src, jarray dst);

#endif
