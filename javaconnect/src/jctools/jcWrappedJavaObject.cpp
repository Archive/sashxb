
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "prmem.h"
#include "plstr.h"
#include "nsIInterfaceInfoManager.h"

#include "nsCOMPtr.h"
#include "nsIServiceManager.h"
#include "jcJavaCall.h"
#include "nsIProgrammingLanguage.h"

#include "jcWrappedJavaObject.h"
#include "jcWrappedJavaInterface.h"
#include "jcServices.h"
#include "jcError.h"

jcWrappedJavaObject::jcWrappedJavaObject(jcClassData *cd):
	mClassData(cd),
	mInterfaceInfo(0)
{
    NS_INIT_ISUPPORTS();
    mInterfaces= new jcTearOffTable();
}

jcWrappedJavaObject::~jcWrappedJavaObject()
{
	nsresult res;
    nsCOMPtr<jcIServices> jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
    // Do not forget to remove yourself from the Runtime Wrapper Table and delete
    // the global ref to mobj
    if (NS_SUCCEEDED(res))
    	jcs->RemoveJavaObject(0, mobj);
    jcs= 0;
    NS_IF_RELEASE(mInterfaceInfo);
    if (mInterfaces)
    	delete mInterfaces;
}
 
NS_IMETHODIMP jcWrappedJavaObject::GetInterfaceInfo(nsIInterfaceInfo** info)
{
    // Act as a transparent nsISupports here
    if (!info)
    	return NS_ERROR_NULL_POINTER;
    // cache mInterfaceInfo
    if (!mInterfaceInfo) {
        nsIInterfaceInfoManager* iimgr;
        if ((iimgr= XPTI_GetInterfaceInfoManager())) {
        	nsIID suppiid= NS_GET_IID(nsISupports);
        	nsresult res= iimgr->GetInfoForIID(&suppiid, &mInterfaceInfo);
            NS_RELEASE(iimgr); // before error check
            if (NS_FAILED(res))
                return res;
        }
        else
            return JC_ERROR_CANT_GET_INTERFACE_MANAGER;
    }
    NS_ADDREF(mInterfaceInfo);
    *info= mInterfaceInfo;
    return NS_OK;
}

NS_IMETHODIMP jcWrappedJavaObject::CallMethod(
	PRUint16 methodIndex,
	const nsXPTMethodInfo* info,
	nsXPTCMiniVariant* params)
{
	nsresult result= JC_ERROR_NO_SUCH_METHOD;
	nsIID qiid;
	switch (methodIndex) {
		case 0: // QI
			//....
			qiid= *(NS_STATIC_CAST(nsIID *, params[0].val.p));
			result= QueryInterface(qiid, &params[1].val.p);
			break;
		case 1: // AddRef
			result= AddRef();
			//....
			break;
		case 2: // Release
			//....
			result= Release();
			break;
		default:
			// we are nsISupports for everything else
			break;
	}
    return result;
}

nsrefcnt jcWrappedJavaObject::AddRef(void)
{ 
    nsrefcnt cnt= NS_STATIC_CAST(
    	nsrefcnt,
    	PR_AtomicIncrement(NS_STATIC_CAST(PRInt32 *, &mRefCnt))
    );
    return cnt;
}

nsrefcnt jcWrappedJavaObject::Release(void)
{
    nsrefcnt cnt= NS_STATIC_CAST(
    	nsrefcnt,
    	PR_AtomicDecrement(NS_STATIC_CAST(PRInt32 *, &mRefCnt))
    );
    // Immediate disappearance
    if (cnt== 0)
    	delete this;
    return cnt;
}

NS_IMETHODIMP jcWrappedJavaObject::QueryInterface(REFNSIID aIID, void** aInstancePtr)
{
	if (!aInstancePtr)
		return NS_ERROR_NULL_POINTER;
	*aInstancePtr= 0;

	// Unfortunately NS_IMPL_ISUPPORTS does not work when
	// we want to dynamically expose interfaces :(
    if (aIID.Equals(NS_GET_IID(nsISupports))) {
    	this->AddRef();
    	*aInstancePtr= NS_ISUPPORTS_CAST(nsIClassInfo *, this);
        return NS_OK;
    }
    if (aIID.Equals(NS_GET_IID(nsIClassInfo))) {
    	this->AddRef();
        *aInstancePtr= NS_STATIC_CAST(nsIClassInfo *, NS_STATIC_CAST(jcWrappedJavaObject *, this));
        return NS_OK;
    }
    if (aIID.Equals(kjcWrappedJavaObjectIID)) {
    	*aInstancePtr= NS_STATIC_CAST(void *, JC_MAJIC_WRAPPER_IDENTIFIER);
        return NS_STATIC_CAST(nsresult, JC_MAJIC_FAILURE_RESPONSE);
    }
	jcWrappedJavaInterface *interfaceStub=
		NS_STATIC_CAST(jcWrappedJavaInterface *, mInterfaces->Lookup(&aIID));
	if (interfaceStub) {
		NS_ADDREF(interfaceStub);
		*aInstancePtr= interfaceStub;
    	return NS_OK;
    }
    if (!mClassData->hasInterfaces()) {
    	nsIID **temp;
    	PRUint32 tempCnt;
    	// Force loading of interface info in mClassData
    	// Will cause some unnecessary mallocing/freeing,
    	// but is simpler overall. The procedure only
    	// happens once per class
    	GetInterfaces(&tempCnt, &temp);
    	for (unsigned int i= 0; i< tempCnt; ++i)
    		PR_FREEIF(temp[i]);
    	PR_FREEIF(temp);
    }
	if (!mClassData->hasInterface(aIID)) {
		return NS_NOINTERFACE;
	}
		
	interfaceStub= new jcWrappedJavaInterface();
	if (!interfaceStub)
		return JC_ERROR_CANT_CREATE_INTERFACE_TEAROFF;
	interfaceStub->parent= NS_STATIC_CAST(jcWrappedJavaObject *, this);
	interfaceStub->iid= aIID;
	NS_ADDREF(interfaceStub);
	mInterfaces->Add(&aIID, interfaceStub);
	*aInstancePtr= interfaceStub;
    return NS_OK;
}

/* void getInterfaces (out PRUint32 count, [array, size_is (count), retval] out nsIIDPtr array); */
NS_IMETHODIMP jcWrappedJavaObject::GetInterfaces(PRUint32 *count, nsIID * **array)
{
	nsresult res= NS_OK;
	if (!count || !array)
		return NS_ERROR_NULL_POINTER;
	// cache interfaces for this class
	if (!mClassData->hasInterfaces()) {
	    nsCOMPtr<jcIServices> jcs= do_GetService(JC_SERVICES_SERVICE_CONTRACTID, &res);
	    if (NS_FAILED(res))
	    	return res;
	    // This is an expensive operation that
	    // involves lots of introspection and even
	    // more JNI callbacks in and out.
	    res= jcs->GetClassInfoForClassName(
	    	0, mClassData->getClassName(), count, array
	    );
	    if (NS_SUCCEEDED(res))
	    	mClassData->setInterfaces(
	    		NS_STATIC_CAST(const nsIID **, *array),
	    		*count
	    	);
	}
	else {
		*array= mClassData->getInterfaces();
		*count= mClassData->getInterfacesCount();
	}
    return res;
}

/* nsISupports getHelperForLanguage (in PRUint32 language); */
NS_IMETHODIMP jcWrappedJavaObject::GetHelperForLanguage(PRUint32 language, nsISupports **_retval)
{
	if (!_retval)
		return NS_ERROR_NULL_POINTER;
	*_retval= 0;
    return NS_OK;
}

/* readonly attribute string contractID; */
NS_IMETHODIMP jcWrappedJavaObject::GetContractID(char * *aContractID)
{
	if (!aContractID)
		return NS_ERROR_NULL_POINTER;
	*aContractID= PL_strdup(mClassData->getContractID());	
    return NS_OK;
}

/* readonly attribute string classDescription; */
NS_IMETHODIMP jcWrappedJavaObject::GetClassDescription(char * *aClassDescription)
{
	if (!aClassDescription)
		return NS_ERROR_NULL_POINTER;
	*aClassDescription= 0;
    return NS_OK;
}

/* readonly attribute nsCIDPtr classID; */
NS_IMETHODIMP jcWrappedJavaObject::GetClassID(nsCID * *aClassID)
{
	if (!aClassID)
		return NS_ERROR_NULL_POINTER;
	*aClassID= NS_STATIC_CAST(nsCID *, PR_MALLOC(sizeof(nsCID)));
	**aClassID= mClassData->getClassID();
    return NS_OK;
}

/* readonly attribute PRUint32 implementationLanguage; */
NS_IMETHODIMP jcWrappedJavaObject::GetImplementationLanguage(PRUint32 *aImplementationLanguage)
{
	*aImplementationLanguage= nsIProgrammingLanguage::JAVA;
    return NS_OK;
}

/* readonly attribute PRUint32 flags; */
NS_IMETHODIMP jcWrappedJavaObject::GetFlags(PRUint32 *aFlags)
{
	if (aFlags)
		return NS_ERROR_NULL_POINTER;
	*aFlags= nsIClassInfo::THREADSAFE;

//    const PRUint32 SINGLETON            = 1 << 0;
//    const PRUint32 THREADSAFE           = 1 << 1;
//    const PRUint32 MAIN_THREAD_ONLY     = 1 << 2;
//    const PRUint32 DOM_OBJECT           = 1 << 3;
//    const PRUint32 PLUGIN_OBJECT        = 1 << 4;
//    const PRUint32 EAGER_CLASSINFO      = 1 << 5;
    
    return NS_OK;
}

/* [notxpcom] readonly attribute nsCID classIDNoAlloc; */
NS_IMETHODIMP jcWrappedJavaObject::GetClassIDNoAlloc(nsCID *aClassIDNoAlloc)
{
	*aClassIDNoAlloc= mClassData->getClassID();
    return NS_OK;
}

NS_IMETHODIMP jcWrappedJavaObject::RemoveWrappedJavaInterface(const nsIID & iid)
{
	if (mInterfaces->Remove(&iid))
		return NS_OK;
	return JC_ERROR_CANT_REMOVE_FORM_SERVICES_TABLE;
}

NS_IMETHODIMP jcWrappedJavaObject::ActualCallMethodByIndex(
	const nsIID & iid,
	PRInt16 methodIndex,
	const nsXPTMethodInfo *info,
	nsXPTCMiniVariant * params)
{
	JavaCall *jcCall= new JavaCall(methodIndex, info, params, iid);
	if (!jcCall)
		return JC_ERROR_CANT_CREATE_JAVA_CALL;
		
	JNIEnv *env;
	bool detach= jcGetEnv(&env);
	
	// Set up a local environment with enough capacity for all params
	int reserve= info->GetParamCount();
	// This should be more than enough, actaully only out params need this
	// better safe than sorry
	reserve= 16+ reserve* 2;
	if (env->PushLocalFrame(reserve)!= 0) {
		env->ExceptionClear();
		delete jcCall;
		jcDropEnv(env, detach);
		return JC_ERROR_CANT_CREATE_LOCAL_ENVIRONMENT;
	}

	// scope of the local JNI frame

	nsresult res= jcCall->Init(env);
	if (NS_FAILED(res)) {
		delete jcCall;
		env->PopLocalFrame(0);
		jcDropEnv(env, detach);
		return res;
	}
	res= jcCall->PerformCall(env, mobj);

	// scope of the local JNI frame

	delete jcCall; // before error check
	env->PopLocalFrame(0);
	jcDropEnv(env, detach); // ditto
	return res;
}
