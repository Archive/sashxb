
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "prmem.h"
#include "nsIInterfaceInfoManager.h"

#include "nsCOMPtr.h"
#include "nsIServiceManager.h"
#include "nsIProgrammingLanguage.h"

#include "jcWrappedJavaObject.h"
#include "jcWrappedJavaInterface.h"
#include "jcServices.h"
#include "jcError.h"

jcWrappedJavaInterface::jcWrappedJavaInterface() :  parent(0), mInterfaceInfo(0) {
    NS_INIT_ISUPPORTS();
}

jcWrappedJavaInterface::~jcWrappedJavaInterface() {
	if (parent)
		((jcWrappedJavaObject *) parent)->RemoveWrappedJavaInterface(iid);
	
    NS_IF_RELEASE(mInterfaceInfo);
}
 
NS_IMETHODIMP jcWrappedJavaInterface::GetInterfaceInfo(nsIInterfaceInfo** info) {

    if (!info)
    	return NS_ERROR_NULL_POINTER;
    if (!mInterfaceInfo) {
        nsIInterfaceInfoManager* iimgr;
        if ((iimgr= XPTI_GetInterfaceInfoManager())) {
        	nsresult res= iimgr->GetInfoForIID(&iid, &mInterfaceInfo);
            if (NS_FAILED(res))
                return res;
            NS_RELEASE(iimgr);
        }
        else
            return JC_ERROR_CANT_GET_INTERFACE_MANAGER;
    }
    NS_ADDREF(mInterfaceInfo);
    *info= mInterfaceInfo;
    return NS_OK;
}

NS_IMETHODIMP jcWrappedJavaInterface::CallMethod(
	PRUint16 methodIndex,
	const nsXPTMethodInfo* info,
	nsXPTCMiniVariant* params) {
		
	nsresult result= JC_ERROR_NO_SUCH_METHOD;
	nsIID qiid;
	switch (methodIndex) {
		case 0: // QI
			//....
			qiid= *((nsIID *) params[0].val.p);
			result= QueryInterface(qiid, &params[1].val.p);
			break;
		case 1: // AddRef
			result= AddRef();
			//....
			break;
		case 2: // Release
			//....
			result= Release();
			break;
		default:
			result= parent->ActualCallMethodByIndex(
				iid,
				methodIndex,
				info,
				params
			);
			break;
	}
    return result;
}

nsrefcnt jcWrappedJavaInterface::AddRef(void) { 
    nsrefcnt cnt= NS_STATIC_CAST(
    	nsrefcnt,
    	PR_AtomicIncrement(NS_STATIC_CAST(PRInt32*, &mRefCnt))
    );
    parent->AddRef();
    return cnt;
}

nsrefcnt jcWrappedJavaInterface::Release(void) {
    nsrefcnt cnt= NS_STATIC_CAST(
    	nsrefcnt,
    	PR_AtomicDecrement(NS_STATIC_CAST(PRInt32*, &mRefCnt))
    );
    jcWrappedJavaObject *parentSave= parent;
    if (cnt== 0)
    	delete this;
    parentSave->Release();
    return cnt;
}

NS_IMETHODIMP jcWrappedJavaInterface::QueryInterface(REFNSIID aIID, void** aInstancePtr) {
	if (!aInstancePtr)
		return NS_ERROR_NULL_POINTER;
	*aInstancePtr= 0;
	if (aIID.Equals(iid)) {
		this->AddRef();
		*aInstancePtr= (nsXPTCStubBase *) this;
		return NS_OK;
	}
	if (aIID.Equals(NS_GET_IID(nsISupports))) {
		parent->AddRef();
		*aInstancePtr= parent;
		return NS_OK;
	}
    if (aIID.Equals(kjcWrappedJavaObjectIID)) {
    	*aInstancePtr= (void *) JC_MAJIC_TEAROFF_IDENTIFIER;
        return (nsresult) JC_MAJIC_FAILURE_RESPONSE;
    }
	return parent->QueryInterface(aIID, aInstancePtr);
}
