
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "jcXPCall.h"
#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsIServiceManager.h"
#include "nsIInterfaceInfoManager.h"
#include "prmem.h"
#include "prthread.h"
#include "nsString.h"
#include "nsReadableUtils.h"
#include "jcUtils.h"
#include "jcWrappedJavaObject.h"
#include "jcWrappedJavaInterface.h"
#include "jcServices.h"
#include "jcConvert.h"
#include "xptcall.h"
#include "jcError.h"

#include <string>
#include <iostream>

nsresult XPCall::GetSizeHintIndex(
	const nsXPTParamInfo pi,
	int dimension, int &index ) {
		
    PRUint8 result;
    nsresult res= interfaceInfo->GetSizeIsArgNumberForParam(
    	methodIndex, &pi, dimension, &result
    );
    if (NS_FAILED(res))
    	return res;
    index= result;
	return NS_OK;
}
	
nsresult XPCall::GetSizeHintValue(
	const nsXPTParamInfo pi, int dimension,
	int &value) {
		
	int idx;
	nsresult res= GetSizeHintIndex(pi, dimension, idx);
	if (NS_FAILED(res))
		return res;
	value= params[idx].val.u32;
	return NS_OK;
}

nsresult XPCall::SetSizeHintValue(
	const nsXPTParamInfo pi, int dimension,
	int value) {
		
	int idx;
	nsresult res= GetSizeHintIndex(pi, dimension, idx);
	if (NS_FAILED(res))
		return res;
	params[idx].val.u32= value;
	return NS_OK;
}

nsresult XPCall::GetIIDHintIndex(const nsXPTParamInfo pi, int &index) {
    PRUint8 result;
    nsresult res= interfaceInfo->GetInterfaceIsArgNumberForParam(
    	methodIndex, &pi, &result
    );
    if (NS_FAILED(res))
    	return res;
    index= result;
	return NS_OK;
}

nsresult XPCall::GetIIDHintValue(const nsXPTParamInfo pi, nsIID &iid) {
	int idx;
	nsresult res= GetIIDHintIndex(pi, idx);
	if (NS_FAILED(res))
		return res;
	iid= *((nsIID *) params[idx].val.p);
	return NS_OK;
}

// returns false on failure
nsresult XPCall::PreCallIn(
	JNIEnv *env, nsXPTCVariant *out,
	const nsXPTParamInfo pi) {
		
	if (!pi.IsIn() || pi.IsDipper())
		return NS_OK;
		
	nsresult res;
	int length;
	
	nsIID iid;

	jobject arg= env->GetObjectArrayElement((jobjectArray) jargs, argCnt);
	// the object is allowed to be null, so do not check for !arg
	if (env->ExceptionCheck()) {
		env->ExceptionClear();
		return JC_ERROR_CANT_GET_OBJECT_ARRAY_ELEMENT;
	}

	switch (pi.GetType().TagPart()) {
		case nsXPTType::T_I64:
		case nsXPTType::T_U64:
		case nsXPTType::T_I8:
		case nsXPTType::T_U8:
		case nsXPTType::T_CHAR:
		case nsXPTType::T_I16:
		case nsXPTType::T_I32:
		case nsXPTType::T_U32:
		case nsXPTType::T_U16:
		case nsXPTType::T_WCHAR:
		case nsXPTType::T_FLOAT:
		case nsXPTType::T_DOUBLE:
		case nsXPTType::T_BOOL: {
			jvalue val;
			res= J2J_UnWrapPrimitive(env, pi.GetType(), arg, val);
			env->DeleteLocalRef(arg); // before error check
			if (NS_FAILED(res))
				return res;
			res= J2X_ConvertPrimitve(env, pi.GetType(), val, &(out->val));
			return res;
		}
		case nsXPTType::T_DOMSTRING:
		case nsXPTType::T_ASTRING: {
			if (!out->val.p)
				out->val.p= new nsString();
			out->flags|= nsXPTCVariant::VAL_IS_DOMSTR;
			res= J2X_ConvertString(
				env, pi.GetType(), 
				(jstring) arg, length, out->val.p
			);
			env->DeleteLocalRef(arg); // before error check
			return res;
		}
		case nsXPTType::T_UTF8STRING:
		case nsXPTType::T_CSTRING: {
			if (!out->val.p)
				out->val.p= new nsCString();
			out->flags|= nsXPTCVariant::VAL_IS_CSTR;
			res= J2X_ConvertString(
				env, pi.GetType(), 
				(jstring) arg, length, out->val.p
			);
			env->DeleteLocalRef(arg); // before error check
			return res;
		}
		case nsXPTType::T_PSTRING_SIZE_IS:
		case nsXPTType::T_PWSTRING_SIZE_IS: {
			res= J2X_ConvertString(
				env, pi.GetType(), 
				(jstring) arg, length, &(out->val.p)
			);
			env->DeleteLocalRef(arg); // before error check
			res= SetSizeHintValue(pi, 0, length);
			return res;
		}	
		case nsXPTType::T_CHAR_STR:
		case nsXPTType::T_WCHAR_STR: {
			res= J2X_ConvertString(
				env, pi.GetType(), 
				(jstring) arg, length, &(out->val.p)
			);
			env->DeleteLocalRef(arg); // before error check
			return res;
		}
		case nsXPTType::T_INTERFACE_IS:
			res= GetIIDHintValue(pi, iid);
			if (NS_FAILED(res)) {
	        	env->DeleteLocalRef(arg);
				return res;
			}
			// fall through on success
		case nsXPTType::T_INTERFACE: {
			out->flags|= nsXPTCVariant::VAL_IS_IFACE;
			if (pi.GetType().TagPart()== nsXPTType::T_INTERFACE)
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
			if (NS_FAILED(res)) {
				env->DeleteLocalRef(arg);
				return res;
			}
			res= J2X_ConvertObject(env, pi.GetType(), arg, iid, &(out->val.p));
			env->DeleteLocalRef(arg); // before error check
			return res;
		}
        case nsXPTType::T_IID: {
			out->flags|= nsXPTCVariant::VAL_IS_ALLOCD;
			out->val.p= new nsIID();
        	res= J2X_ConvertIIDObject(env, arg, *((nsIID *) out->val.p));
        	env->DeleteLocalRef(arg); // before error check
			return res;
        }
        case nsXPTType::T_ARRAY: {
			out->flags|= nsXPTCVariant::VAL_IS_ARRAY;
        	int length;
        	nsXPTType elTyp;
        	nsIID iid;
			res= interfaceInfo->GetTypeForParam(methodIndex, &pi, 1, &elTyp);
			if (NS_FAILED(res)) {
	        	env->DeleteLocalRef(arg);
				return res;
			}
			if (elTyp.TagPart()== nsXPTType::T_INTERFACE) {
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
				if (NS_FAILED(res)) {
		        	env->DeleteLocalRef(arg);
					return res;
				}
			}
			if (elTyp.TagPart()== nsXPTType::T_INTERFACE_IS) {
				res= GetIIDHintValue(pi, iid);
				if (NS_FAILED(res)) {
		        	env->DeleteLocalRef(arg);
					return res;
				}
			}
        	res= J2X_ConvertArray(env, elTyp, (jarray) arg, length, iid, (void **) &(out->val.p));
        	env->DeleteLocalRef(arg); // before error check
			if (NS_FAILED(res))
				return res;
			res= SetSizeHintValue(pi, 0, length);
			return res;
        }
        default:
        	cerr << "XPCall::PreCallIn hit an unsupported type " << (int) pi.GetType().TagPart() << endl;
        	env->DeleteLocalRef(arg);
	}
	// can only slide here from the default case (i.e. T_VOID)
	return JC_ERROR_CANT_HANDLE_IN_VALUE;
}

nsresult XPCall::PreCallOut(
	JNIEnv *env, nsXPTCVariant *out,
	const nsXPTParamInfo pi) {
		
	if (!pi.IsOut() && !pi.IsDipper())
		return NS_OK;
		
	switch (pi.GetType().TagPart()) {
		case nsXPTType::T_I64:
		case nsXPTType::T_U64:
		case nsXPTType::T_BOOL:
		case nsXPTType::T_I8:
		case nsXPTType::T_U8:
		case nsXPTType::T_CHAR:
		case nsXPTType::T_I16:
		case nsXPTType::T_U16:
		case nsXPTType::T_WCHAR:
		case nsXPTType::T_I32:
		case nsXPTType::T_U32:
		case nsXPTType::T_FLOAT:
		case nsXPTType::T_DOUBLE:
		case nsXPTType::T_IID:
		case nsXPTType::T_CHAR_STR:
		case nsXPTType::T_WCHAR_STR:
			out->flags|= nsXPTCVariant::PTR_IS_DATA;
			out->ptr= &(out->val);
			break;
		case nsXPTType::T_ASTRING:
		case nsXPTType::T_DOMSTRING:
			out->flags|= nsXPTCVariant::VAL_IS_DOMSTR;
			out->val.p= new nsString();
			break;
		case nsXPTType::T_CSTRING:
			out->flags|= nsXPTCVariant::VAL_IS_CSTR;
			out->val.p= new nsCString();
			break;
		case nsXPTType::T_UTF8STRING:
			out->flags|= nsXPTCVariant::VAL_IS_UTF8STR;
			out->val.p= new nsCString();
			break;
		case nsXPTType::T_INTERFACE:
		case nsXPTType::T_INTERFACE_IS: {
			out->flags|= nsXPTCVariant::VAL_IS_IFACE;
			out->ptr= &(out->val);
			out->flags|= nsXPTCVariant::PTR_IS_DATA;
			break;
		}
		case nsXPTType::T_ARRAY:
			out->flags|= nsXPTCVariant::VAL_IS_ARRAY;
		case nsXPTType::T_PSTRING_SIZE_IS:
		case nsXPTType::T_PWSTRING_SIZE_IS: {
			out->flags|= nsXPTCVariant::PTR_IS_DATA;
			out->ptr= &(out->val);
			int idx;
			nsresult res= GetSizeHintIndex(pi, 0, idx);
			if (NS_FAILED(res))
				return res;
			// prepare the hint for an out
			params[idx].ptr= &(params[idx].val);
			params[idx].flags|= nsXPTCVariant::PTR_IS_DATA;
			break;
		}
		default:
			cerr << "XPCall PreCallOut hit an unsupported type " << endl;
			return JC_ERROR_CANT_HANDLE_OUT_VALUE;
	}
	return NS_OK;	
}

nsresult XPCall::PostCallOut(
	JNIEnv *env, nsXPTCVariant *in,
	const nsXPTParamInfo pi, jobject &result,
	bool dimensionUp) {
	
	result= 0;
	nsIID iid;
	
	if (!pi.IsOut() && !pi.IsDipper())
		return NS_OK;
		
	nsresult res;
	jvalue val;

	switch (pi.GetType().TagPart()) {
		case nsXPTType::T_I8:
		case nsXPTType::T_U8:
		case nsXPTType::T_CHAR:
		case nsXPTType::T_I16:
		case nsXPTType::T_I32:
		case nsXPTType::T_U32:
		case nsXPTType::T_I64:
		case nsXPTType::T_U64:
		case nsXPTType::T_U16:
		case nsXPTType::T_WCHAR:
		case nsXPTType::T_FLOAT:
		case nsXPTType::T_DOUBLE:
		case nsXPTType::T_BOOL: {
			res= X2J_ConvertPrimitive(env, pi.GetType(), in->ptr, val);
			if (NS_FAILED(res))
				return res;
			if (!dimensionUp) { // only wrap the return value
				res= J2J_WrapPrimitive(env, pi.GetType(), val, result);
				if (NS_FAILED(res))
					return res;
			}
			break;
		}

		case nsXPTType::T_VOID:
			return NS_OK;

		case nsXPTType::T_DOMSTRING:
		case nsXPTType::T_ASTRING:
		case nsXPTType::T_UTF8STRING:
		case nsXPTType::T_CSTRING:
		case nsXPTType::T_CHAR_STR:
		case nsXPTType::T_WCHAR_STR: {
			res= X2J_ConvertString(env, pi.GetType(), in->val.p, 0, (jstring &) result);
			if (NS_FAILED(res))
				return res;
			if (dimensionUp)
				val.l= env->NewLocalRef(result);
			break;
		}
		case nsXPTType::T_PSTRING_SIZE_IS:
		case nsXPTType::T_PWSTRING_SIZE_IS: {
			int length;
			res= GetSizeHintValue(pi, 0, length);
			res= X2J_ConvertString(env, pi.GetType(), in->val.p, length, (jstring &) result);
			if (NS_FAILED(res))
				return res;
			if (dimensionUp)
				val.l= env->NewLocalRef(result);
			break;
		}	
		case nsXPTType::T_IID: {
			res= X2J_ConvertIIDObject(env, *((nsIID *) in->val.p), result);
			if (NS_FAILED(res))
				return res;
			if (dimensionUp)
				val.l= env->NewLocalRef(result);
			break;
		}
		case nsXPTType::T_INTERFACE_IS:
			res= GetIIDHintValue(pi, iid);
			if (NS_FAILED(res))
				return res;
			// fall through
		case nsXPTType::T_INTERFACE: {
			if (pi.GetType().TagPart()== nsXPTType::T_INTERFACE)
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertObject(env, pi.GetType(), ((nsISupports *) in->val.p), iid, result);
			if (NS_FAILED(res))
				return res;
			if (dimensionUp)
				val.l= env->NewLocalRef(result);
			break;
		}
		case nsXPTType::T_ARRAY: {
			nsXPTType elementType;
			res= interfaceInfo->GetTypeForParam(methodIndex, &pi, 1, &elementType);
			if (NS_FAILED(res))
				return res;
			if (elementType.TagPart()== nsXPTType::T_INTERFACE) {
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
				if (NS_FAILED(res))
					return res;
			}
			if (elementType.TagPart()== nsXPTType::T_INTERFACE_IS) {
				res= GetIIDHintValue(pi, iid);
				if (NS_FAILED(res))
					return res;
			}
			int size;
			res= GetSizeHintValue(pi, 0, size);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertArray(env, elementType, in->val.p, size, iid, (jarray) result);
			if (NS_FAILED(res))
				return res;
			if (dimensionUp)
				val.l= env->NewLocalRef(result);
			break;
		}
		default:
			cerr << "XPCall PostCallOut hit an usupported type" << endl;
	}
	if (dimensionUp) {
		jobject dst= env->GetObjectArrayElement((jobjectArray) jargs, argCnt);
		if (!dst) {
			env->ExceptionClear();
			env->DeleteLocalRef(result);
			return JC_ERROR_CANT_GET_OBJECT_ARRAY_ELEMENT;
		}
		res= J2J_ArrayUp(env, pi.GetType(), val, (jarray) dst);
		env->DeleteLocalRef(result);
		return res;
	}
	return NS_OK;
}

nsresult XPCall::PerformCall(
	JNIEnv *env, nsISupports *cobj,
	jobject &retval) {
		
	retval= 0;
	nsresult res= XPTC_InvokeByIndex(cobj, methodIndex, paramCount, params);

	if (NS_FAILED(res))
		return res;

	argCnt= 0;
	for (int i= 0; i< paramCount; ++i) {
		if (ignoreHints.find(i)!= ignoreHints.end())
			continue;

		const nsXPTParamInfo paramInfo= methodInfo->GetParam(i);

		jobject obj= 0;
		res= PostCallOut(env, params+ i, paramInfo, obj, i!= retValIndex);
		if (NS_FAILED(res))
			return res;
		if (i== retValIndex)
			retval= obj;
		++argCnt;
	}
	return NS_OK;
}
		
nsresult XPCall::Init(JNIEnv *env, jarray args) {

	nsresult res;
	
	paramCount= (int) methodInfo->GetParamCount();
	argCnt= 0;
	jargs= args;
	params= (nsXPTCVariant *) PR_MALLOC(sizeof(nsXPTCVariant)* paramCount);
	for (int i= 0; i< paramCount; ++i) {
		params[i]= nsXPTCVariant();
		params[i].ClearFlags();
		params[i].val.p= 0;
	}
	// ignore size hints, their values should be set/allocated
	// by the values that depend on them
	for (int i= 0; i< paramCount; ++i) {

		const nsXPTParamInfo paramInfo= methodInfo->GetParam(i);

		params[i].type= paramInfo.GetType();
		
		switch (paramInfo.GetType().TagPart()) {
			case nsXPTType::T_PSTRING_SIZE_IS:
			case nsXPTType::T_PWSTRING_SIZE_IS:
			case nsXPTType::T_ARRAY: {
				int idx;
				res= GetSizeHintIndex(paramInfo, 0, idx);
				if (NS_FAILED(res))
					return res;
				ignoreHints.insert(idx);
			}
		}
	}
	
	for (int i= 0; i< paramCount; ++i) {
		if (ignoreHints.find(i)!= ignoreHints.end())
			continue;

		const nsXPTParamInfo paramInfo= methodInfo->GetParam(i);

		if (paramInfo.IsRetval())
			retValIndex= i;
		res= PreCallOut(env, params+ i, paramInfo); 
		if (NS_FAILED(res))
			return res;
		res= PreCallIn(env, params+ i, paramInfo);
		if (NS_FAILED(res))
			return res;
		++argCnt;
	}
	return NS_OK;
}

XPCall::XPCall(
	const char *methodName,
	const nsIInterfaceInfo *inInf,
	const nsXPTMethodInfo *methodInf,
	PRUint16 mi) {
	
	methodInfo= methodInf;
	methodIndex= mi;
	interfaceInfo= const_cast<nsIInterfaceInfo *>(inInf);
	params= 0;
	retValIndex= -1; // no ret val
}
	
XPCall::~XPCall() {
}
