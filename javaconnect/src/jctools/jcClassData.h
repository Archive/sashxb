#ifndef _JC_CLASSDATA_H
#define _JC_CLASSDATA_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "nsID.h"

/**
	Contains information about one Java class exposed to XPCOM. Needed to map
	a class name to a contract id or a class id (used by the nsIClassInfo
	implementation in jcWrappedjavaObject)
*/
class jcClassData {
	public:
		jcClassData(
			const char *cname,
			const char *contractid,
			const nsCID &cid
		);
		
		~jcClassData();
		
		const char *getClassName() const;
		
		const char *getContractID() const;
		
		nsCID getClassID() const;
		
		// makes own copy
		void setInterfaces(
			const nsIID **ifaces,
			const unsigned int count
		);

		// caller must free
		nsIID **getInterfaces() const;

		unsigned int getInterfacesCount() const;

		bool hasInterface(const nsIID &iid) const;

		bool hasInterfaces() const;

	private:
		char *className;
		char *contractID;
		nsCID classID;
		nsIID **interfaces;
		unsigned int interfacesCount;
};

#endif
