#ifndef _JC_XP_CALL_H
#define _JC_XP_CALL_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "xptcall.h"
#include "xptinfo.h"
#include "jcUtils.h"
#include <vector>
#include <set>

class XPCall {
	
	public:
	
		XPCall(
			const char *methodName,
			const nsIInterfaceInfo *interfaceInf,
			const nsXPTMethodInfo *methodInf,
			PRUint16 mi
		);
		
		~XPCall();

		nsresult Init(JNIEnv *env, jarray args);
		nsresult PerformCall(JNIEnv *env, nsISupports *obj, jobject &retval);
		
	private:

		nsresult GetSizeHintIndex(
			const nsXPTParamInfo pi,
			int dimension, int &index
		);
		nsresult XPCall::GetSizeHintValue(
			const nsXPTParamInfo pi, int dimension,
			int &value
		);
		nsresult SetSizeHintValue(
			const nsXPTParamInfo pi,
			int dimension, int value
		);
		nsresult GetIIDHintIndex(
			const nsXPTParamInfo pi,
			int &index
		);	
		nsresult GetIIDHintValue(
			const nsXPTParamInfo pi,
			nsIID &iid
		);
		
		nsresult PreCallIn(
			JNIEnv *env, nsXPTCVariant *out,
			const nsXPTParamInfo pi
		);
		nsresult PreCallOut(
			JNIEnv *env, nsXPTCVariant *out,
			const nsXPTParamInfo pi
		);
		nsresult PostCallOut(
			JNIEnv *env, nsXPTCVariant *in,
			const nsXPTParamInfo pi, jobject &result,
			bool dimensionUp
		);

		int paramCount; // param count
		int retValIndex; // index of return value
		
		nsXPTCVariant *params;
		const nsXPTMethodInfo *methodInfo;
		nsIInterfaceInfo *interfaceInfo;
		int argCnt;
		jarray jargs;
		set<int> ignoreHints;
		
		PRUint16 methodIndex;
};

#endif
