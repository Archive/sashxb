
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "jcError.h"
#include "jcUtils.h"

#define JC_ERROR_COUNT 40
static const char * JCErrorMessages[]= {
	"Generic error", // 0
	"Bad size specified", // 1
	"JNI call failed", // 2
	"Cannot deduce type signature", // 3
	"Cannot convert", // 4
	"Cannot instatiate interface tearoff", // 5
	"Cannot create Java call object", // 6
	"Cannot create a locat JNI environment", // 7
	"Cannot create object array", // 8
	"Cannot create object", // 9
	"Cannot create array", // 10
	"Cannot find class", // 11
	"Cannot find method id", // 12
	"Cannot find field id", // 13
	"Cannot get interface information manager", // 14
	"Cannot get interface name from IID", // 15
	"Cannot access object array element", // 16
	"Cannot access primitive array elements", // 17
	"Cannot access object field", // 18
	"Cannot get object length", // 19
	"Cannot get string characters", // 20
	"Cannot handle type", // 21
	"Cannot marshall in value", // 22
	"Cannot marshall out value", // 23
	"Cannot parse ID", // 24
	"Cannot prform call", // 25
	"Cannot create a reference to java object", // 26
	"Cannot remove object from object table", // 27
	"Cannot set object array element", // 28
	"Cannot set primitive array elements", // 29
	"Not enough memory", // 30
	"No such method", // 31
	"Unknown Java signatur", // 32
	"Your compiler is broken", // 33
	"Cannot wrap object", // 34
	"Cannot obtain JNI environment", // 35
	"Cannot resolve class name", // 36
	"Cannot get class data", // 37
	"Cannot create object wrapper", // 38
	"Cannot find in services table", // 39
	"Cannot get event queue service", // 40
	"END OF JAVACONNECT ERRORS", // JC_ERROR_COUNT + 1
	"Null pointer" // JC_ERROR_COUNT + 2
};

const char *GetErrorMessage(nsresult res) {
	int code= NS_ERROR_GET_CODE(res);
	int module= NS_ERROR_GET_MODULE(res);
	if (module== NS_ERROR_MODULE_GENERAL && code> 0 && code <= JC_ERROR_COUNT)
		return JCErrorMessages[code];
	if (res== NS_ERROR_FAILURE)
		return JCErrorMessages[0];
	if (res== NS_ERROR_NULL_POINTER)
		return JCErrorMessages[JC_ERROR_COUNT+ 2];
	return JCErrorMessages[0];
}

void JC_ThrowException(JNIEnv *env, const char *msg) {
	if (!env)
		return;
//	jclass cls= env->FindClass("org/mozilla/xpcom/XPCOMException");
	jclass cls= JC_FindClass(env, "org/mozilla/xpcom/XPCOMException");
	if (!cls) {
		env->ExceptionClear();
		return;
	}
	jint result= env->ThrowNew(cls, msg);
	env->DeleteLocalRef(cls);
	if (result< 0)
		env->ExceptionClear();
}

void JC_ThrowException(JNIEnv *env, nsresult res) {
	JC_ThrowException(env, GetErrorMessage(res));
}
