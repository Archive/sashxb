#ifndef JC_ERROR_H
#define JC_ERROR_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "nsError.h"
#include "jcUtils.h"

#define JC_GENERATE_ERROR(code) \
	NS_ERROR_GENERATE_FAILURE(NS_ERROR_MODULE_GENERAL, code)

#define JC_ERROR_BAD_LENGTH JC_GENERATE_ERROR(1)
#define JC_ERROR_CALL_FAILED JC_GENERATE_ERROR(2)
#define JC_ERROR_CANT_CONVERT_SIGNATURE JC_GENERATE_ERROR(3)
#define JC_ERROR_CANT_CONVERT JC_GENERATE_ERROR(4)
#define JC_ERROR_CANT_CREATE_INTERFACE_TEAROFF JC_GENERATE_ERROR(5)
#define JC_ERROR_CANT_CREATE_JAVA_CALL JC_GENERATE_ERROR(6)
#define JC_ERROR_CANT_CREATE_LOCAL_ENVIRONMENT JC_GENERATE_ERROR(7)
#define JC_ERROR_CANT_CREATE_OBJECT_ARRAY JC_GENERATE_ERROR(8)
#define JC_ERROR_CANT_CREATE_OBJECT JC_GENERATE_ERROR(9)
#define JC_ERROR_CANT_CREATE_ARRAY JC_GENERATE_ERROR(10)
#define JC_ERROR_CANT_FIND_CLASS JC_GENERATE_ERROR(11)
#define JC_ERROR_CANT_FIND_METHOD JC_GENERATE_ERROR(12)
#define JC_ERROR_CANT_FIND_FIELD JC_GENERATE_ERROR(13)
#define JC_ERROR_CANT_GET_INTERFACE_MANAGER JC_GENERATE_ERROR(14)
#define JC_ERROR_CANT_GET_INTERFACE_NAME JC_GENERATE_ERROR(15)
#define JC_ERROR_CANT_GET_OBJECT_ARRAY_ELEMENT JC_GENERATE_ERROR(16)
#define JC_ERROR_CANT_GET_ARRAY_ELEMENTS JC_GENERATE_ERROR(17)
#define JC_ERROR_CANT_GET_FIELD JC_GENERATE_ERROR(18)
#define JC_ERROR_CANT_GET_LENTGH JC_GENERATE_ERROR(19)
#define JC_ERROR_CANT_GET_STRING_CHARS JC_GENERATE_ERROR(20)
#define JC_ERROR_CANT_HANDLE_TYPE JC_GENERATE_ERROR(21)
#define JC_ERROR_CANT_HANDLE_IN_VALUE JC_GENERATE_ERROR(22)
#define JC_ERROR_CANT_HANDLE_OUT_VALUE JC_GENERATE_ERROR(23)
#define JC_ERROR_CANT_PARSE_ID JC_GENERATE_ERROR(24)
#define JC_ERROR_CANT_PERFORM_CALL JC_GENERATE_ERROR(25)
#define JC_ERROR_CANT_REFERENCE JC_GENERATE_ERROR(26)
#define JC_ERROR_CANT_REMOVE_FORM_SERVICES_TABLE JC_GENERATE_ERROR(27)
#define JC_ERROR_CANT_SET_OBJECT_ARRAY_ELEMENT JC_GENERATE_ERROR(28)
#define JC_ERROR_CANT_SET_ARRAY_ELEMENTS JC_GENERATE_ERROR(29)
#define JC_ERROR_NO_MEMORY JC_GENERATE_ERROR(30)
#define JC_ERROR_NO_SUCH_METHOD JC_GENERATE_ERROR(31)
#define JC_ERROR_UNKNOWN_SIGNATURE JC_GENERATE_ERROR(32)
#define JC_ERROR_YOUR_COMPILER_IS_BROKEN JC_GENERATE_ERROR(33)

#define JC_ERROR_CANT_WRAP_OBJECT JC_GENERATE_ERROR(34)
#define JC_ERROR_CANT_GET_JNI_ENVIRONMENT JC_GENERATE_ERROR(35)
#define JC_ERROR_CANT_GET_CLASS_NAME JC_GENERATE_ERROR(36)
#define JC_ERROR_CANT_GET_CLASS_DATA JC_GENERATE_ERROR(37)
#define JC_ERROR_CANT_CREATE_OBJECT_WRAPPER JC_GENERATE_ERROR(38)
#define JC_ERROR_CANT_FIND_IN_SERVICES_TABLE JC_GENERATE_ERROR(39)
#define JC_ERROR_CANT_GET_EVENT_QUEUE_SERVICE JC_GENERATE_ERROR(40)



const char *GetErrorMessage(nsresult res);
void JC_ThrowException(JNIEnv *env, const char *msg);
void JC_ThrowException(JNIEnv *env, nsresult res);

#endif
