
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/
#include "jcJavaCall.h"
#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"
#include "nsIServiceManager.h"
#include "nsIInterfaceInfoManager.h"
#include "prmem.h"
#include "prthread.h"
#include "nsString.h"
#include "nsReadableUtils.h"
#include "jcUtils.h"
#include "jcWrappedJavaObject.h"
#include "jcWrappedJavaInterface.h"
#include "jcServices.h"
#include "jcConvert.h"
#include "jcError.h"
#include <string>
#include <iostream>

nsresult JavaCall::GetSizeHintIndex(
	const nsXPTParamInfo pi,
	int dimension, int &index ) {
		
    PRUint8 result;
    nsresult res= interfaceInfo->GetSizeIsArgNumberForParam(
    	methodIndex, &pi, dimension, &result
    );
    if (NS_FAILED(res))
    	return res;
    index= result;
	return NS_OK;
}
	
nsresult JavaCall::GetSizeHintValue(
	const nsXPTParamInfo pi, int dimension,
	int &value) {
		
	int idx;
	nsresult res= GetSizeHintIndex(pi, dimension, idx);
	if (NS_FAILED(res))
		return res;
	value= params[idx].val.u32;
	return NS_OK;
}

nsresult JavaCall::SetSizeHintValue(
	const nsXPTParamInfo pi, int dimension,
	int value) {
		
	int idx;
	nsresult res= GetSizeHintIndex(pi, dimension, idx);
	if (NS_FAILED(res))
		return res;
	*((PRUint32 *) params[idx].val.p)= value;
	return NS_OK;
}

nsresult JavaCall::GetIIDHintIndex(const nsXPTParamInfo pi, int &index) {
    PRUint8 result;
    nsresult res= interfaceInfo->GetInterfaceIsArgNumberForParam(
    	methodIndex, &pi, &result
    );
    if (NS_FAILED(res))
    	return res;
    index= result;
	return NS_OK;
}
	
nsresult JavaCall::GetIIDHintValue(const nsXPTParamInfo pi, nsIID &iid) {
	int idx;
	nsresult res= GetIIDHintIndex(pi, idx);
	if (NS_FAILED(res))
		return res;
	iid= *((nsIID *) params[idx].val.p);
	return NS_OK;
}
	
nsresult JavaCall::PreCallIn(
	JNIEnv *env, nsXPTCMiniVariant *in,
	const nsXPTParamInfo pi) {
		
	if (!env || pi.IsOut() || pi.IsRetval() || pi.IsDipper())
		return NS_OK;

	jvalue arg;
	string sig;
	nsIID iid;
	int length;
	
	nsresult res= GetSignature(in, pi, 0, sig);
	if (NS_FAILED(res))
		return res;
	
	switch (pi.GetType().TagPart()) {
		case nsXPTType::T_I8:
		case nsXPTType::T_I16:
		case nsXPTType::T_I32:
		case nsXPTType::T_I64:
		case nsXPTType::T_U8:
		case nsXPTType::T_U16:
		case nsXPTType::T_U32:
		case nsXPTType::T_U64:
		case nsXPTType::T_FLOAT:
		case nsXPTType::T_DOUBLE:
		case nsXPTType::T_BOOL:
		case nsXPTType::T_CHAR:
		case nsXPTType::T_WCHAR: {
			res= X2J_ConvertPrimitive(env, pi.GetType(), &(in->val), arg);
			if (NS_FAILED(res)) {
				return res;
			}
			break;
		}
		case nsXPTType::T_PSTRING_SIZE_IS:
		case nsXPTType::T_PWSTRING_SIZE_IS: {
			res= GetSizeHintValue(pi, 0, length);
			if (NS_FAILED(res))
				return res;
			int idx;
			res= GetSizeHintIndex(pi, 0, idx);
			if (NS_FAILED(res))
				return res;
			ignoreHints.insert(idx);
			// fall through to regular string case
		}
		case nsXPTType::T_CSTRING:
		case nsXPTType::T_CHAR_STR:
		case nsXPTType::T_WCHAR_STR:
		case nsXPTType::T_UTF8STRING:
		case nsXPTType::T_DOMSTRING:
		case nsXPTType::T_ASTRING:
			res= X2J_ConvertString(env, pi.GetType(), in->val.p, length, (jstring) arg.l);
			if (NS_FAILED(res))
				return res;
			break;
		case nsXPTType::T_INTERFACE_IS:
			res= GetIIDHintValue(pi, iid);
			if (NS_FAILED(res))
				return res;
			// fall through
		case nsXPTType::T_INTERFACE: {
			if (pi.GetType().TagPart()== nsXPTType::T_INTERFACE)
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertObject(env, pi.GetType(), in->val.p, iid, arg.l);
			if (NS_FAILED(res))
				return res;
			break;
		}
		case nsXPTType::T_IID: {
			res= X2J_ConvertIIDObject(env, *((nsIID *) in->val.p), arg.l);
			if (NS_FAILED(res))
				return res;
			break;
		}
		case nsXPTType::T_ARRAY: {
			// Dimension must be 1 here, but 0 for the size_is part
			nsXPTType elementType;
			res= interfaceInfo->GetTypeForParam(methodIndex, &pi, 1, &elementType);
			if (NS_FAILED(res))
				return res;
			if (elementType.TagPart()== nsXPTType::T_INTERFACE) {
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
				if (NS_FAILED(res))
					return res;
			}
			if (elementType.TagPart()== nsXPTType::T_INTERFACE_IS) {
				res= GetIIDHintValue(pi, iid);
				if (NS_FAILED(res))
					return res;
			}
			// remember to ignore size hints Java-side
			int ignoreIdx;
			res= GetSizeHintIndex(pi, 0, ignoreIdx);
			if (NS_FAILED(res))
				return res;
			ignoreHints.insert(ignoreIdx);
			int size;
			res= GetSizeHintValue(pi, 0, size);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertArray(env, elementType, in->val.p, size, iid, (jarray) arg.l);
			if (NS_FAILED(res))
				return res;
			break;
		}
		default:
			return JC_ERROR_CANT_HANDLE_IN_VALUE;
	}
	jargs.push_back(arg);
	jsig.push_back(sig);
	return NS_OK;
}

nsresult JavaCall::PreCallOut(
	JNIEnv *env, nsXPTCMiniVariant *in,
	const nsXPTParamInfo pi) {
		
	if (!pi.IsOut() && !pi.IsRetval() && !pi.IsDipper())
		return NS_OK;
		
	jvalue arg;
	string sig;

	switch (pi.GetType().TagPart()) {
		case nsXPTType::T_PSTRING_SIZE_IS:
		case nsXPTType::T_PWSTRING_SIZE_IS:
		case nsXPTType::T_ARRAY: {
			int idx;
			nsresult res= GetSizeHintIndex(pi, 0, idx);
			if (NS_FAILED(res))
				return res;
			ignoreHints.insert(idx);
		}
	}
	
	nsresult res= GetSignature(in, pi, 0, sig);
	if (NS_FAILED(res))
		return res;
	if (sig[0]== 'L' || sig[0]== '[') { // dealing with objects
		// array of arrays is array of objects
		string className= "java/lang/Object";
		// skip L and ;
		if (sig[0]== 'L')
			className= sig.substr(1, sig.length()- 2);
//		jclass cls= env->FindClass(className.c_str());
		jclass cls= JC_FindClass(env, className.c_str());
		if (!cls) {
			env->ExceptionClear();
			return JC_ERROR_CANT_FIND_CLASS;
		}
		arg.l= env->NewObjectArray(1, cls, 0);
		env->DeleteLocalRef(cls); // before error check
		if (!arg.l) {
			env->ExceptionClear();
			return JC_ERROR_CANT_CREATE_OBJECT_ARRAY;
		}
	}
	else { // boring case
		switch (sig[0]) {
			case 'Z':
				arg.l= (jobject) env->NewBooleanArray(1);
				break;
			case 'B':
				arg.l= (jobject) env->NewByteArray(1);
				break;
			case 'C':
				arg.l= (jobject) env->NewCharArray(1);
				break;
			case 'S':
				arg.l= (jobject) env->NewShortArray(1);
				break;
			case 'I':
				arg.l= (jobject) env->NewIntArray(1);
				break;
			case 'J':
				arg.l= (jobject) env->NewLongArray(1);
				break;
			case 'F':
				arg.l= (jobject) env->NewFloatArray(1);
				break;
			case 'D':
				arg.l= (jobject) env->NewDoubleArray(1);
				break;
			default:
				return JC_ERROR_UNKNOWN_SIGNATURE;
		}
		if (!arg.l) {
			env->ExceptionClear();
			return JC_ERROR_CANT_CREATE_ARRAY;
		}
	}

	// outs need an extra array dimension
	sig= "["+ sig;
	jargs.push_back(arg);
	jsig.push_back(sig);
	return NS_OK;
}

nsresult JavaCall::PostCallIn(
	JNIEnv *env, nsXPTCMiniVariant *in,
	const nsXPTParamInfo pi) {
		
	if (!env || pi.IsOut() || pi.IsRetval() || pi.IsDipper())
		return NS_OK;

	switch (pi.GetType().TagPart()) {
#if 0
		case nsXPTType::T_ARRAY: {
			// Dimension must be 1 here, but 0 for the size_is part
			nsXPTType elementType;
			res= interfaceInfo->GetTypeForParam(methodIndex, &pi, 1, &elementType);
			if (NS_FAILED(res))
				return res;
			if (elementType.TagPart()== nsXPTType::T_INTERFACE) {
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
				if (NS_FAILED(res))
					return res;
			}
			if (elementType.TagPart()== nsXPTType::T_INTERFACE_IS) {
				res= GetIIDHintValue(pi, iid);
				if (NS_FAILED(res))
					return res;
			}
			// remember to ignore size hints Java-side
			int ignoreIdx;
			res= GetSizeHintIndex(pi, 0, ignoreIdx);
			if (NS_FAILED(res))
				return res;
			ignoreHints.insert(ignoreIdx);
			int size;
			res= GetSizeHintValue(pi, 0, size);
			if (NS_FAILED(res))
				return res;
			res= X2J_ConvertArray(env, elementType, in->val.p, size, iid, (jarray) arg.l);
			if (NS_FAILED(res))
				return res;
			break;
		}
#endif
	}
	return NS_OK;
}

nsresult JavaCall::PostCallOut(
	JNIEnv *env, nsXPTCMiniVariant *out,
	const nsXPTParamInfo pi, jvalue val,
	bool dimensionDown) {
		
	if (!pi.IsOut() && !pi.IsRetval() && !pi.IsDipper())
		return NS_OK;

	nsresult res;
	nsIID iid;
	jvalue jv;

	jv= val;
	if (dimensionDown) {
		res= J2J_ArrayDown(env, pi.GetType(), (jarray) val.l, jv);
		if (NS_FAILED(res))
			return res;
	}
	
	switch (pi.GetType().TagPart()) {
		case nsXPTType::T_I8:
		case nsXPTType::T_U8:
		case nsXPTType::T_CHAR:
		case nsXPTType::T_I16:
		case nsXPTType::T_I32:
		case nsXPTType::T_U32:
		case nsXPTType::T_I64:
		case nsXPTType::T_U64:
		case nsXPTType::T_U16:
		case nsXPTType::T_WCHAR:
		case nsXPTType::T_FLOAT:
		case nsXPTType::T_DOUBLE:
		case nsXPTType::T_BOOL: {
			res= J2X_ConvertPrimitve(env, pi.GetType(), jv, out->val.p);
			if (NS_FAILED(res))
				return res;
			return NS_OK;
		}
		case nsXPTType::T_DOMSTRING:
		case nsXPTType::T_ASTRING:
		case nsXPTType::T_CSTRING:
		case nsXPTType::T_UTF8STRING:
		case nsXPTType::T_CHAR_STR:
		case nsXPTType::T_PSTRING_SIZE_IS:
		case nsXPTType::T_WCHAR_STR:
		case nsXPTType::T_PWSTRING_SIZE_IS: {
			int length;
			res= J2X_ConvertString(
				env, pi.GetType(), 
				(jstring) jv.l, length, out->val.p
			);
			if (dimensionDown)
				env->DeleteLocalRef(jv.l); // before error check
			if (NS_FAILED(res))
				return res;
			if (pi.GetType().TagPart()== nsXPTType::T_PSTRING_SIZE_IS ||
				pi.GetType().TagPart()== nsXPTType::T_PWSTRING_SIZE_IS)
				res= SetSizeHintValue(pi, 0, length);
			return res;
		}
		case nsXPTType::T_INTERFACE_IS:
			iid= NS_GET_IID(nsISupports);
			res= NS_OK;
		case nsXPTType::T_INTERFACE: {
			if (pi.GetType().TagPart()== nsXPTType::T_INTERFACE)
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
			if (NS_FAILED(res)) {
				if (dimensionDown)
					env->DeleteLocalRef(jv.l);
				return res;
			}
			res= J2X_ConvertObject(env, pi.GetType(), jv.l, iid, out->val.p);
			if (dimensionDown)
				env->DeleteLocalRef(jv.l); // before error check
			if (NS_FAILED(res))
				return res;
			return NS_OK;
		}

        case nsXPTType::T_IID: {
        	nsIID tmp;
        	res= J2X_ConvertIIDObject(env, jv.l, tmp);
        	if (dimensionDown)
	        	env->DeleteLocalRef(jv.l);
			if (NS_FAILED(res))
				return res;
			*((nsIID **) out->val.p)= new nsIID(tmp);
			if (NS_FAILED(res))
				return res;
			return NS_OK;
        }
        case nsXPTType::T_ARRAY: {
        	int length;
        	nsXPTType elTyp;
        	nsIID iid;
			res= interfaceInfo->GetTypeForParam(methodIndex, &pi, 1, &elTyp);
			if (NS_FAILED(res)) {
				if (dimensionDown)
					env->DeleteLocalRef(jv.l);
				return res;
			}
			if (elTyp.TagPart()== nsXPTType::T_INTERFACE) {
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
				if (NS_FAILED(res)) {
					if (dimensionDown)
						env->DeleteLocalRef(jv.l);
					return res;
				}
			}
			if (elTyp.TagPart()== nsXPTType::T_INTERFACE_IS) {
				res= GetIIDHintValue(pi, iid);
				if (NS_FAILED(res)) {
					if (dimensionDown)
						env->DeleteLocalRef(jv.l);
					return res;
				}
			}
        	res= J2X_ConvertArray(env, elTyp, (jarray) jv.l, length, iid, (void **) out->val.p);
        	if (dimensionDown)
	        	env->DeleteLocalRef(jv.l); // before error check
			if (NS_FAILED(res))
				return res;
			res= SetSizeHintValue(pi, 0, length);
			return res;
        }
        default:
        	if (dimensionDown)
	        	env->DeleteLocalRef(jv.l);
	}
	return JC_ERROR_CANT_HANDLE_OUT_VALUE;
}

nsresult JavaCall::PerformCall(JNIEnv *env, jobject obj) {
	nsresult res;

	// check consistency of args
	if ( (retValIndex== -1 && int(jargs.size())!= paramCount)
		|| (retValIndex!= -1 && int(jargs.size())!= paramCount -1))
		return JC_ERROR_CANT_PERFORM_CALL;
		
	// Generate method type signature
	string typeSig= "(";
	for (int i= 0; i< int(jargs.size()); ++i)
		if (ignoreHints.find(i)== ignoreHints.end())
			typeSig+= jsig[i];
	typeSig+= ")"+ callSig;

	// Obtain method name
	const char *methodName= methodInfo->GetName();
	string metName= methodName;
	
	// OOOPS! Handle prefix for getters and setters
	JC_FixMethodName(metName, methodInfo->IsGetter(), methodInfo->IsSetter());
	
	jvalue *args= new jvalue[jargs.size()- ignoreHints.size()];

	if (!args)
		return JC_ERROR_CANT_PERFORM_CALL;

	for (int i= 0, j= 0; i< int(jargs.size()); ++i)
		if (ignoreHints.find(i)== ignoreHints.end())
			args[j++]= jargs[i];
		
	jmethodID mid;
	jclass cls;
	jvalue result;

	cls= env->GetObjectClass(obj);
	if (!cls) {
		env->ExceptionCheck();
		return JC_ERROR_CANT_FIND_CLASS;
	}

	mid= env->GetMethodID(cls, metName.c_str(), typeSig.c_str());
	env->DeleteLocalRef(cls); // before error check
	if (!mid) {
		env->ExceptionClear();
		return JC_ERROR_CANT_FIND_METHOD;
	}

	switch (callType) {
		case CallVoid:
			env->CallVoidMethodA(obj, mid, args);
			break;
		case CallObject:
			result.l= env->CallObjectMethodA(obj, mid, args);
			break;
		case CallBoolean:
			result.z= env->CallBooleanMethodA(obj, mid, args);
			break;
		case CallByte:
			result.b= env->CallByteMethodA(obj, mid, args);
			break;
		case CallChar:
			result.c= env->CallCharMethodA(obj, mid, args);
			break;
		case CallShort:
			result.s= env->CallShortMethodA(obj, mid, args);
			break;
		case CallInt:
			result.i= env->CallIntMethodA(obj, mid, args);
			break;
		case CallLong:
			result.j= env->CallLongMethodA(obj, mid, args);
			break;
		case CallFloat:
			result.f= env->CallFloatMethodA(obj, mid, args);
			break;
		case CallDouble:
			result.d= env->CallDoubleMethodA(obj, mid, args);
			break;
	}
	delete args;
	if (env->ExceptionCheck()) {
		env->ExceptionDescribe();
		env->ExceptionClear();
		return JC_ERROR_CANT_PERFORM_CALL;
	}
	
	// Take care of return value
	jargs.push_back(result);
	
	// Take care of simple out variables
	for (int i= 0; i< paramCount; ++i) {
		if (ignoreHints.find(i)!= ignoreHints.end())
			continue; // skip the index
		const nsXPTParamInfo pi= methodInfo->GetParam(i);
		res= PostCallIn(env, params+ i, pi);
		if (NS_FAILED(res))
			return res;
		res= PostCallOut(env, params+ i, pi, jargs[i], i!= retValIndex);
		if (NS_FAILED(res))
			return res;
	}
	return NS_OK;
}
		
nsresult JavaCall::Init(JNIEnv *env) {
	
	nsIInterfaceInfoManager* mgr= XPTI_GetInterfaceInfoManager();
	if (!mgr)
		return JC_ERROR_CANT_GET_INTERFACE_MANAGER;
	
	nsIID rootiid(callIID);
	nsresult res= mgr->GetInfoForIID(&rootiid, &interfaceInfo);
	NS_RELEASE(mgr);
	if (NS_FAILED(res))
		return res;
	NS_ADDREF(interfaceInfo);
	
	paramCount= int(methodInfo->GetParamCount());
	for (int i= 0; i< paramCount; ++i) {
		const nsXPTParamInfo paramInfo= methodInfo->GetParam(i);

		if (paramInfo.IsRetval()) {
			retValIndex= i;
			res= GetSignature(params+ i, paramInfo, 0, callSig);
			if (NS_FAILED(res))
				return res;
			// can't really fail
			GetCallType(callSig, callType);
			continue;
		}
		res= PreCallIn(env, params+ i, paramInfo);
		if (NS_FAILED(res))
			return res;
		res= PreCallOut(env, params+ i, paramInfo);
		if (NS_FAILED(res))
			return res;
	}
	return NS_OK;
}

nsresult JavaCall::GetSignature(
	nsXPTCMiniVariant *in, const nsXPTParamInfo pi,
	const nsXPTType *opt, string &result) {
		
	nsresult res;
	nsIID iid;
	uint8 tagPart;
	if (!opt)
		tagPart= pi.GetType().TagPart();
	else
		tagPart= opt->TagPart();
	switch (tagPart) {
		case nsXPTType::T_I8:
			result= "B";
			return NS_OK;
		case nsXPTType::T_I16:
			result= "S";
			return NS_OK;
		case nsXPTType::T_I32:
			result= "I";
			return NS_OK;
		case nsXPTType::T_I64:
			result= "J";
			return NS_OK;
		case nsXPTType::T_U8:
			result= "B";
			return NS_OK;
		case nsXPTType::T_U16:
			result= "C";
			return NS_OK;
		case nsXPTType::T_U32:
			result= "I";
			return NS_OK;
		case nsXPTType::T_U64:
			result= "J";
			return NS_OK;
		case nsXPTType::T_FLOAT:
			result= "F";
			return NS_OK;
		case nsXPTType::T_DOUBLE:
			result= "D";
			return NS_OK;
		case nsXPTType::T_BOOL:
			result= "Z";
			return NS_OK;
		case nsXPTType::T_CHAR:
			result= "B";
			return NS_OK;
		case nsXPTType::T_WCHAR:
			result= "C";
			return NS_OK;
		case nsXPTType::T_VOID:
			result= "V";
			return NS_OK;
		case nsXPTType::T_IID:
			result= "Lorg/mozilla/xpcom/IID;";
			return NS_OK;
		case nsXPTType::T_DOMSTRING:
		case nsXPTType::T_CHAR_STR:
		case nsXPTType::T_WCHAR_STR:
		case nsXPTType::T_PSTRING_SIZE_IS:
		case nsXPTType::T_PWSTRING_SIZE_IS:
		case nsXPTType::T_UTF8STRING:
		case nsXPTType::T_CSTRING:
		case nsXPTType::T_ASTRING:
			result= "Ljava/lang/String;";
			return NS_OK;
		case nsXPTType::T_ARRAY: {
			nsXPTType typ;
			res= interfaceInfo->GetTypeForParam(methodIndex, &pi, 1, &typ);
			if (NS_FAILED(res))
				return res;
			res= GetSignature(in, pi, &typ, result);
			if (NS_FAILED(res))
				return res;
			result= "["+ result;
			return NS_OK;
		}	
		case nsXPTType::T_INTERFACE_IS:
			// as far as the signature is concerned we do not want
			// to use anything but nsISupports
			result= "Lorg/mozilla/xpcom/nsISupports;";
			return NS_OK;
		case nsXPTType::T_INTERFACE: {
			if (tagPart== nsXPTType::T_INTERFACE)
				res= interfaceInfo->GetIIDForParamNoAlloc(methodIndex, &pi, &iid);
			if (NS_FAILED(res))
				return res;

			// HANDLE THE CASE WITH nsIVariant
			if (iid.Equals(NS_GET_IID(nsIVariant))) {
				result= "Ljava/lang/Object;";
				return NS_OK;
			}

			char *intName= JC_GetInterfaceName(iid);
			if (!intName)
				return JC_ERROR_CANT_GET_INTERFACE_NAME;
			result= string("Lorg/mozilla/xpcom/")+ intName+ ";";
			PR_FREEIF(intName);
			return NS_OK;
		}
	}
	return JC_ERROR_CANT_CONVERT_SIGNATURE;
}

nsresult JavaCall::GetCallType(const string &sig, JNICallType &typ) {
	switch (sig[0]) {
		case 'Z':
			typ= CallBoolean;
			break;
		case 'B':
			typ= CallByte;
			break;
		case 'S':
			typ= CallShort;
			break;
		case 'C':
			typ= CallChar;
			break;
		case 'I':
			typ= CallInt;
			break;
		case 'J':
			typ= CallLong;
			break;
		case 'F':
			typ= CallFloat;
			break;
		case 'D':
			typ= CallDouble;
			break;
		case 'V':
			typ= CallVoid;
			break;
		default:
			typ= CallObject;
	}
	return NS_OK;
}

JavaCall::JavaCall(
	PRUint16 mi,
	const nsXPTMethodInfo *info,
	nsXPTCMiniVariant *in,
	const nsIID &iid) {
		
	methodIndex= mi;
	methodInfo= info;
	interfaceInfo= 0;
	params= in;
	retValIndex= -1; // no ret val
	callType= CallVoid;
	callSig= "V"; // void by default
	jargs.clear();
	jsig.clear();
	ignoreHints.clear();
	callIID= iid;
}
	
JavaCall::~JavaCall() {
	NS_IF_RELEASE(interfaceInfo);
}
