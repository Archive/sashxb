#ifndef _JC_JAVA_CALL_H
#define _JC_JAVA_CALL_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "xptcall.h"
#include "xptinfo.h"
#include "jcUtils.h"
#include <vector>
#include <set>

typedef enum {
	CallVoid,
	CallObject,
	CallBoolean,
	CallByte,
	CallChar,
	CallShort,
	CallInt,
	CallLong,
	CallFloat,
	CallDouble
} JNICallType;

const unsigned char jvalNone= 0;
const unsigned char jvalIsOut= 1<< 0;
const unsigned char jvalDelRef= 1<< 1;

class JavaCall {
	
	public:
	
		JavaCall(
			PRUint16 mi,
			const nsXPTMethodInfo *info,
			nsXPTCMiniVariant *in,
			const nsIID &iid
		);
		
		~JavaCall();

		nsresult Init(JNIEnv *env);
		nsresult PerformCall(JNIEnv *env, jobject obj);
		
	private:

		nsresult GetSignature(
			nsXPTCMiniVariant *in, const nsXPTParamInfo pi,
			const nsXPTType *opt, string &sig
		);
		
		nsresult GetCallType(const string &sig, JNICallType &typ);
		
		nsresult GetSizeHintIndex(
			const nsXPTParamInfo pi,
			int dimension, int &index
		);
		nsresult GetSizeHintValue(
			const nsXPTParamInfo pi,
			int dimension, int &value
		);
		nsresult SetSizeHintValue(
			const nsXPTParamInfo pi,
			int dimension, int value
		);
		nsresult GetIIDHintIndex(
			const nsXPTParamInfo pi,
			int &index
		);	
		nsresult GetIIDHintValue(
			const nsXPTParamInfo pi,
			nsIID &iid
		);
		nsresult PreCallIn(
			JNIEnv *env, nsXPTCMiniVariant *in,
			const nsXPTParamInfo pi
		);
		nsresult PreCallOut(
			JNIEnv *env, nsXPTCMiniVariant *in,
			const nsXPTParamInfo pi
		);
		nsresult PostCallIn(
			JNIEnv *env, nsXPTCMiniVariant *in,
			const nsXPTParamInfo pi
		);
		nsresult PostCallOut(
			JNIEnv *env, nsXPTCMiniVariant *out,
			const nsXPTParamInfo pi, jvalue jv,
			bool dimensionDown
		);

		// process arguments that are actually passed to JNI
		// (All except the retval marked one)
		vector<jvalue> jargs;
		vector<string> jsig; // params signature for GetMethodID
		set<int> ignoreHints;
		
		int paramCount; // param count
		int retValIndex; // index of return value
		JNICallType callType; // call type
		string callSig; // return type signature
		
		nsXPTCMiniVariant *params;
		const nsXPTMethodInfo *methodInfo;
		nsIInterfaceInfo *interfaceInfo;
		
		nsIID callIID;
		PRUint16 methodIndex;
};

#endif
