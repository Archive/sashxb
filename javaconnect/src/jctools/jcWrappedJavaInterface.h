#ifndef _JCWRAPPEDJAVAINTERFACE_H
#define _JCWRAPPEDJAVAINTERFACE_H

/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Stefan Atev

*****************************************************************/

#include "xptcall.h"
#include "nsISupports.h"
#include "nsIInterfaceInfo.h"
#include "nsIClassInfo.h"
#include "jcWrappedJavaObject.h"

#define JCIWRAPPEDJAVAINTERFACE_IID \
  {0x53911a5e, 0xf37a, 0x4036, \
    { 0xae, 0xe4, 0x28, 0x90, 0xf8, 0x99, 0xf0, 0x3b }}
    
NS_DEFINE_IID(kjcWrappedJavaInterfaceIID, JCIWRAPPEDJAVAINTERFACE_IID);

#define JC_MAJIC_TEAROFF_IDENTIFIER 0x29731228

class jcWrappedJavaInterface :
	public nsXPTCStubBase {
		
	public:
    	NS_DECL_ISUPPORTS

    	jcWrappedJavaInterface();
    	virtual ~jcWrappedJavaInterface();
    
		// Stub methods - implement and forget
    	NS_IMETHOD GetInterfaceInfo(nsIInterfaceInfo** info);
    	// call this method and return result
    	NS_IMETHOD CallMethod(
    		PRUint16 methodIndex,
    		const nsXPTMethodInfo* info,
			nsXPTCMiniVariant* params
		);
		
		// This should be private. But, since I am the sole
		// abuser of this class, I might as well make my life
		// easy.
    	jcWrappedJavaObject *parent;

	private:
		friend jcWrappedJavaObject;
    	nsIID iid;
    	nsIInterfaceInfo *mInterfaceInfo;
    	
};
#endif
