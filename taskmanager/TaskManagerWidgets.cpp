// Autogenerated by gen_widgets.ml
#include "TaskManagerWidgets.h"
#include <assert.h>

Widgets::Widgets() {
	MainWindow = NULL;
	m_RWList = NULL;
	m_IWList = NULL;
	m_EList = NULL;
	m_LList = NULL;
	UseLocatorsCheckbox = NULL;
	LocatorAddressEntry = NULL;
	AddLocatorButton = NULL;
	LocatorList = NULL;
	LocatorAddressLabel = NULL;
	DeleteLocatorButton = NULL;
	FallBackCheckbox = NULL;
	ProxyAddress = NULL;
	ProxyPort = NULL;
	ProxyAddressLabel = NULL;
	ProxyPortLabel = NULL;
	CloseButton = NULL;
	NoProxyRadio = NULL;
	UseProxyRadio = NULL;
	AutodetectProxyRadio = NULL;
}

void Widgets::Initialize(GladeXML * UI) {
	MainWindow = glade_xml_get_widget(UI, "MainWindow");
	assert(MainWindow);
	m_RWList = glade_xml_get_widget(UI, "m_RWList");
	assert(m_RWList);
	m_IWList = glade_xml_get_widget(UI, "m_IWList");
	assert(m_IWList);
	m_EList = glade_xml_get_widget(UI, "m_EList");
	assert(m_EList);
	m_LList = glade_xml_get_widget(UI, "m_LList");
	assert(m_LList);
	UseLocatorsCheckbox = glade_xml_get_widget(UI, "UseLocatorsCheckbox");
	assert(UseLocatorsCheckbox);
	LocatorAddressEntry = glade_xml_get_widget(UI, "LocatorAddressEntry");
	assert(LocatorAddressEntry);
	AddLocatorButton = glade_xml_get_widget(UI, "AddLocatorButton");
	assert(AddLocatorButton);
	LocatorList = glade_xml_get_widget(UI, "LocatorList");
	assert(LocatorList);
	LocatorAddressLabel = glade_xml_get_widget(UI, "LocatorAddressLabel");
	assert(LocatorAddressLabel);
	DeleteLocatorButton = glade_xml_get_widget(UI, "DeleteLocatorButton");
	assert(DeleteLocatorButton);
	FallBackCheckbox = glade_xml_get_widget(UI, "FallBackCheckbox");
	assert(FallBackCheckbox);
	ProxyAddress = glade_xml_get_widget(UI, "ProxyAddress");
	assert(ProxyAddress);
	ProxyPort = glade_xml_get_widget(UI, "ProxyPort");
	assert(ProxyPort);
	ProxyAddressLabel = glade_xml_get_widget(UI, "ProxyAddressLabel");
	assert(ProxyAddressLabel);
	ProxyPortLabel = glade_xml_get_widget(UI, "ProxyPortLabel");
	assert(ProxyPortLabel);
	CloseButton = glade_xml_get_widget(UI, "CloseButton");
	assert(CloseButton);
	NoProxyRadio = glade_xml_get_widget(UI, "NoProxyRadio");
	assert(NoProxyRadio);
	UseProxyRadio = glade_xml_get_widget(UI, "UseProxyRadio");
	assert(UseProxyRadio);
	AutodetectProxyRadio = glade_xml_get_widget(UI, "AutodetectProxyRadio");
	assert(AutodetectProxyRadio);
}

