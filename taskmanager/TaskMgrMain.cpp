
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Ari Heitner, AJ Shankar

wrapper that gets the task manager and panel app running

*****************************************************************/

#include <gtk/gtk.h>
#include <applet-widget.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <popt.h>
#include "TaskMgrApplet.h"
#include "TaskMgr.h"
#include "RuntimeTools.h"
#include "extensiontools.h"


extern TaskManager* g_pTaskManager;
bool no_panel = false;

static const struct poptOption options[] = {
	 {"no-panel", 'n', POPT_ARG_NONE, &no_panel, 0, N_("Run in a normal window")},
	 {NULL, '\0', 0, NULL, 0}
};

void child_detected (int a, siginfo_t * t, void* v) {
	 DEBUGMSG(taskmgr, "Child detected, PID %d  \n", t->si_pid);
	 
	 waitpid(t->si_pid, NULL, WNOHANG);
}

int main(int argc, char **argv) {
	 poptContext c;
	 if (no_panel) {
		  gnome_init_with_popt_table("sash-task-manager", SASH_VERSION, 
									 argc, argv, options, 0, &c);
	 } else {
		  applet_widget_init("sash-task-manager", SASH_VERSION, 
							 argc, argv, (poptOption*) options, 0, &c);
	 }
	 glade_gnome_init();

	 struct sigaction act;
	 memset(&act, 0, sizeof(struct sigaction));
	 act.sa_sigaction = child_detected;
	 sigfillset(&(act.sa_mask));
	 
	 act.sa_flags = SA_NOCLDSTOP | SA_SIGINFO;
	 act.sa_restorer = NULL;
	 
	 sigaction(SIGCHLD, &act, NULL);
	 
	 vector<string> components;
	 SashRegisterSashRuntime(components);
	 SashErrorPushMode(SASH_ERROR_GNOME);

	 g_pTaskManager = new TaskManager();
	 if (no_panel) {
		  g_pTaskManager->Initialize(NULL);
		  g_pTaskManager->DisplayMe();
		  gtk_main();
	 } else {
		  cout << "The SashXB Task Manager is now active in the GNOME panel." << endl;
		  cout << "To run it as a normal application, use the --no-panel flag." << endl;
		  TaskManagerApplet* tma = new TaskManagerApplet();
		  g_pTaskManager->Initialize(tma);

		  applet_widget_gtk_main();
	 }	 
	 return(0);
}
