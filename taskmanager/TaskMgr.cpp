/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Ari Heitner, AJ Shankar

Sash Task Manager: uninstall, run, kill, etc. of Sash components

*****************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <gnome.h>
#include <applet-widget.h>
#include <signal.h>
#include <errno.h>
#include <glib.h>
#include <vector>
#include <algorithm>
#include <string>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <signal.h>
#include "sash_constants.h"
#include "TaskMgrApplet.h"
#include "TaskMgr.h"
#include "wdf.h"
#include "WeblicationRegister.h"
#include "InstallationManager.h"
#include "FileSystem.h"
#include "debugmsg.h"
#include "Installer.h"
#include "SashSecurityDialog.h"
#include "nsIPref.h"
#include "nsMemory.h"
#include "nsIServiceManager.h"

using namespace std;

vector<WeblicationRegister*> active;
TaskManager* g_pTaskManager = NULL;

TaskManager::TaskManager() : m_pTMA(NULL) {

}

TaskManager::~TaskManager() {
  SashErrorSetWindow(NULL);
}

void TaskManager::Initialize(TaskManagerApplet* tma) {
  string glade_file = GetSashShareDirectory() + "/task-manager.glade";
  
  GladeXML* UI = glade_xml_new(glade_file.c_str(), NULL);
  if (! UI) {
	printf("Unable to open task-manager.glade! Aborting.\n");
	exit(-1);
  }

  SetupTCPServer();  

  glade_xml_signal_autoconnect(UI);
  Widgets::Initialize(UI);

  SashErrorSetWindow(MainWindow);

  Fill();
  
  m_InstallationManager.GetLocatorServices(
	   LocatorData.use_locators,
	   LocatorData.locators,
	   LocatorData.fall_back);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(UseLocatorsCheckbox), 
							   LocatorData.use_locators);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(FallBackCheckbox), 
							   LocatorData.fall_back);

  for (unsigned int i=0 ; i< LocatorData.locators.size() ; i++) {
    char *row[1];
    row[0] = (char*) LocatorData.locators[i].c_str();
    gtk_clist_append(GTK_CLIST(LocatorList), row);
  }

  nsCOMPtr<nsIPref> p = do_GetService(NS_PREF_CONTRACTID);
  int i = 0;  
  if (p) {
	   p->GetIntPref("network.proxy.type", &i);
	   switch(i) {
	   case 0:
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(NoProxyRadio), true);
			break;
	   case 1:
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(UseProxyRadio), true);
			break;
	   case 2:
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(AutodetectProxyRadio), true);
			break;
	   default:
			;
	   }
	   p->GetIntPref("network.proxy.http_port", &i);
	   char num[10];
	   snprintf(num, 10, "%d", i);
	   gtk_entry_set_text(GTK_ENTRY(ProxyPort), num);
	   char* c;
	   p->GetCharPref("network.proxy.http", &c);
	   gtk_entry_set_text(GTK_ENTRY(ProxyAddress), c);
	   nsMemory::Free(c);
  }
  if (tma) {
	   m_pTMA = tma;
	   gtk_widget_show(GTK_WIDGET(CloseButton));
  }

}

void TaskManager::SetupTCPServer() {
  sock = socket(AF_INET, SOCK_STREAM, 0);
  int addrLen = sizeof(address);
  int on = 1;
  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)); 
  memset((void*)&address, 0, addrLen);
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = htonl(INADDR_ANY);
  address.sin_port = htons(DEFAULT_PORT);
  
  if ( bind(sock, (sockaddr*)&address, addrLen) ) {
    DEBUGMSG(taskmgr, "ERROR: couldn't bind to TCP port %d\n", 
	     ntohs(address.sin_port));

    OutputError("The SashXB Task Manager could not bind to its "
		"TCP port;\nis another Task Manager already running?");
  }

  DEBUGMSG(taskmgr, "Task manager active on port %d\n", 
	   ntohs(address.sin_port));
  
  if ( listen(sock, 5) ) {
    OutputError("The Sash Weblication Task Manager could not listen on "
		"its TCP port;\nis another Task Manager already running?");
  }

  // every two seconds, make sure all running weblications are still running
  m_ServeID = gtk_timeout_add(5, AsyncServe, (gpointer)this);
  m_CheckID = gtk_timeout_add(1000, AsyncCheckPids, (gpointer)this);
}

void TaskManager::DisplayMe() {
	 gtk_widget_show(GTK_WIDGET(MainWindow));
	 gdk_window_raise(MainWindow->window);
}

bool sort_seis_by_name (const SashExtensionItem& a, const SashExtensionItem& b) {
	 return (strcasecmp(a.name.c_str(), b.name.c_str()) < 0);
}

void TaskManager::Fill() {
  gtk_clist_clear(GTK_CLIST(m_RWList));
  for (unsigned int i=0; i<active.size(); i++) {
    char *row[3];
    row[0] = (char*) (active[i]->act_name).c_str();
    row[1] = (char*) (active[i]->webl_name).c_str();
    row[2] = (char*) (active[i]->pid).c_str();
    gtk_clist_prepend(GTK_CLIST(m_RWList), row);
  }
  
  m_WeblicationGuids.clear();
  gtk_clist_clear(GTK_CLIST(m_IWList));
  // can't get these by reference since we need to sort them
  vector<SashExtensionItem> installed = m_InstallationManager.GetInfoAllInstalled(SET_WEBLICATION);
  sort(installed.begin(), installed.end(), ptr_fun(sort_seis_by_name));
  for (unsigned int i=0; i<installed.size(); i++) {
    char *row[2];
    row[0] = (char*) installed[i].name.c_str();
    row[1] = (char*) installed[i].version.c_str();
    gtk_clist_append(GTK_CLIST(m_IWList), row);
	m_WeblicationGuids.push_back(installed[i].guid);
  }

  m_LocationGuids.clear();
  gtk_clist_clear(GTK_CLIST(m_LList));   
  vector<SashExtensionItem> installed_locs = m_InstallationManager.GetInfoAllInstalled(SET_LOCATION);
  sort(installed_locs.begin(), installed_locs.end(), ptr_fun(sort_seis_by_name));
  for (unsigned int i=0; i< installed_locs.size(); i++) {
    char *row[2];
    row[0] = (char*) installed_locs[i].name.c_str();
    row[1] = (char*) installed_locs[i].version.c_str();
    gtk_clist_append(GTK_CLIST(m_LList), row);
	m_LocationGuids.push_back(installed_locs[i].guid);
  }

  m_ExtensionGuids.clear();
  gtk_clist_clear(GTK_CLIST(m_EList));
  vector<SashExtensionItem> installed_exts = m_InstallationManager.GetInfoAllInstalled(SET_EXTENSION);
  sort(installed_exts.begin(), installed_exts.end(), ptr_fun(sort_seis_by_name));
  for (unsigned int i=0; i< installed_exts.size(); i++) {
    char *row[2];
    row[0] = (char*) installed_exts[i].name.c_str();
    row[1] = (char*) installed_exts[i].version.c_str();
    gtk_clist_append(GTK_CLIST(m_EList), row);
	m_ExtensionGuids.push_back(installed_exts[i].guid);
  }
}

void TaskManager::UpdateRunning() {
	 // total hack
  int i = active.size() - 1;

  DEBUGMSG(taskmgr, "Adding new running weblication %s\n", 
	   active[i]->webl_name.c_str());

  char *row[3];
  row[0] = (char*) (active[i]->act_name).c_str();
  row[1] = (char*) (active[i]->webl_name).c_str();
  row[2] = (char*) (active[i]->pid).c_str();
  gtk_clist_prepend(GTK_CLIST(m_RWList), row);
}

void TaskManager::RemoveRunning(const string& name, const string& pid) {
  int i = 0;
  gchar* c;
  // iterate over each element in the clist
  while (gtk_clist_get_text(GTK_CLIST(m_RWList), i, 2, &c)) {
    if (pid == c) {
      // we've found the column to remove
      gtk_clist_remove(GTK_CLIST(m_RWList), i);
      break;
    }
    i++;
  }
}

void ExitEverything(GtkButton *b, void* nothing) {
	 TaskManager* tm = g_pTaskManager;
	 shutdown(tm->sock, 2);
	 gtk_timeout_remove(tm->m_CheckID);
	 gtk_timeout_remove(tm->m_ServeID);
	 
	 HideMe(b, nothing);
	 TaskManagerApplet* tma = tm->m_pTMA;
	 if (tma) {
		  TaskManagerApplet::ExitApplet(NULL, tma);
	 } else {
		  gtk_main_quit();
	 }
}

int killPid;

void KillWeblication(GtkButton *b, void* d) {
  TaskManager* tm = g_pTaskManager;
	 
  if ( !GTK_CLIST(tm->m_RWList)->selection ) {
    OutputMessage("Please select an action first!");
    return;
  }
  int row = (int)g_list_nth_data(GTK_CLIST(tm->m_RWList)->selection, 0);

  DEBUGMSG(taskmgr, "Selected row appears to be %d\tPreparing to terminate\n",
	   row);

  char *pid;
  char *name, *webname;
  gtk_clist_get_text(GTK_CLIST(tm->m_RWList), row, 0, &name);
  gtk_clist_get_text(GTK_CLIST(tm->m_RWList), row, 1, &webname);
  gtk_clist_get_text(GTK_CLIST(tm->m_RWList), row, 2, &pid);
  
  killPid = atoi(pid);
  DEBUGMSG(taskmgr, "Action pid %s %d\n", pid, killPid);

  if (! OutputQuery("Are you sure you want to kill the running action\n"
					"'%s' from the weblication '%s'?", name, webname))
    return;
 
  if ( kill(killPid, SIGTERM) ) {
    if (errno == ESRCH)
      OutputMessage("That weblication does not appear to be running now!"
					"\n(Have you killed it yourself?)");
  } 
  AsyncCheckPids(NULL);

  gtk_clist_remove(GTK_CLIST(tm->m_RWList), row);
	   
  signal(SIGALRM, TerminateWeblication);
  alarm(1);
  DEBUGMSG(taskmgr, "Done killing\n");
}

void TerminateWeblication(int sig) {
  if ( kill(killPid, SIGKILL) ) {
    if (errno == ESRCH) {
      //      g_print("ERROR: process %d already killed\n",killPid);
    } else{ 
		 OutputMessage("Error: Unable to kill weblication!");
    }
  }
  killPid = -1;
}


void UninstallWeblication(GtkButton *b, void* d) {
  char *name;
  int row;
  TaskManager* tm = g_pTaskManager;

  if ( !GTK_CLIST(tm->m_IWList)->selection ) {
    // nothing is selected
    OutputMessage("Please select a weblication first!");

    return;
  }

  row = (int)g_list_nth_data(GTK_CLIST(tm->m_IWList)->selection, 0);
  string& guid = tm->m_WeblicationGuids[row];

  DEBUGMSG(taskmgr, "Selected row appears to be %d\n", row);

  gtk_clist_get_text(GTK_CLIST(tm->m_IWList), row, 0, &name);

  // now make sure the weblication isn't running
  vector<WeblicationRegister*>::iterator bi = active.begin(), ei = active.end();
  while (bi != ei) {
    if ((*bi)->webl_guid == guid) {
      OutputMessage("That weblication is still running!\nPlease exit it before attempting to uninstall.");
      return;
    }
    ++bi;
  }

  // now see if any weblications depend on the location
  vector<string> depends;
  DEBUGMSG(taskmgr, "Checking dependencies for %s\n", name);

  GetDependencies(guid, depends);

  string query = "Are you sure you want to uninstall the weblication '%s'?";
  if (depends.size() > 0) {
    query += "\nThe following SashXB components depend on it:\n";
    vector<string>::iterator bi = depends.begin(), ei = depends.end();
    while (bi != ei) {
      query += *bi + "\n";
      ++bi;
    }
  }

  if (! OutputQuery(query.c_str(), name))
    return;

  DEBUGMSG(taskmgr, "Uninstalling weblication with guid %s\n", guid.c_str());

  tm->m_InstallationManager.Uninstall(guid);

  OutputMessage("Weblication '%s' uninstalled!", name);

  gtk_clist_remove(GTK_CLIST(tm->m_IWList), row);
  tm->m_WeblicationGuids.erase(tm->m_WeblicationGuids.begin() + row);
}

void UninstallLocation (GtkButton *b, void* d) {
	 char *name;
  int row;
  TaskManager* tm = g_pTaskManager;
	 
  if ( !GTK_CLIST(tm->m_LList)->selection ) {
    // nothing is selected
    OutputMessage("Please select a location first!");

    return;
  }

  row = (int)g_list_nth_data(GTK_CLIST(tm->m_LList)->selection, 0);
  string& guid = tm->m_LocationGuids[row];
  DEBUGMSG(taskmgr, "Selected row appears to be %d\n", row);

  gtk_clist_get_text(GTK_CLIST(tm->m_LList), row, 0, &name);

  // now see if any weblications depend on the location
  vector<string> depends;
  DEBUGMSG(taskmgr, "Checking dependencies for %s\n", name);

  GetDependencies(guid, depends);

  string query = "Are you sure you want to uninstall the location '%s'?";
  if (depends.size() > 0) {
    query += "\nThe following SashXB weblications depend on it:\n";
    vector<string>::iterator bi = depends.begin(), ei = depends.end();
    while (bi != ei) {
      query += *bi + "\n";
      ++bi;
    }
  }

  if (! OutputQuery(query.c_str(), name))
    return;

  DEBUGMSG(taskmgr, "Uninstalling location with guid %s\n", guid.c_str());

  tm->m_InstallationManager.Uninstall(guid);

  OutputMessage("Location '%s' uninstalled!", name);

  gtk_clist_remove(GTK_CLIST(tm->m_LList), row);
  tm->m_LocationGuids.erase(tm->m_LocationGuids.begin() + row);

}

void UninstallExtension(GtkButton *b, void* d) {
	 char *name;
  int row;
  TaskManager* tm = g_pTaskManager;
	 
  if ( !GTK_CLIST(tm->m_EList)->selection ) {
    // nothing is selected
    OutputMessage("Please select an extension first!");
    return;
  }

  row = (int)g_list_nth_data(GTK_CLIST(tm->m_EList)->selection, 0);
  string& guid = tm->m_ExtensionGuids[row];

  DEBUGMSG(taskmgr, "Selected row appears to be %d\n", row);

  gtk_clist_get_text(GTK_CLIST(tm->m_EList), row, 0, &name);

  string query;
  
  bool is_default = false;

  for (const char** a = DEFAULT_EXTENSIONS ; *a != NULL ; a++) {
	   if (strcmp(guid.c_str(), *a) == 0) { is_default = true; break; }
  }

  if (is_default) {
	   query = "Are you sure you want to uninstall the '%s' extension? This will almost certainly "
			"damage all of your weblications.";
  } else {
	   // now see if any sash components depend on the extension
	   vector<string> depends;
	   DEBUGMSG(taskmgr, "Checking dependencies for %s\n", name);
	   
	   GetDependencies(guid, depends);
	   
	   query = "Are you sure you want to uninstall the extension '%s'?";
	   if (depends.size() > 0) {
			query += "\nThe following SashXB components depend on it:\n";
			vector<string>::iterator bi = depends.begin(), ei = depends.end();
			while (bi != ei) {
				 query += *bi + "\n";
				 ++bi;
			}
	   }
  }

  if (! OutputQuery(query.c_str(), name))
	   return;
	   
  DEBUGMSG(taskmgr, "Uninstalling extension with guid %s\n", guid.c_str());

  tm->m_InstallationManager.Uninstall(guid);

  OutputMessage("Extension '%s' uninstalled!", name);

  gtk_clist_remove(GTK_CLIST(tm->m_EList), row);
  tm->m_ExtensionGuids.erase(tm->m_ExtensionGuids.begin() + row);

}

void Update(const char* guid, const char* name) {
  // only want to install if it's a newer version
  Installer::s_InstallOverPrevious = 0;
  Installer i(guid);
 
  InstallResult r = i.Install();
  switch (r) {
  case IR_SUCCEEDED:
	   OutputMessage("Update succeeded!");
	   break;
  case IR_NOT_NEWER_VERSION:
	   OutputMessage("There is no update for '%s' available at this time.", name);
	   break;
  default:
	   OutputMessage("The installation failed: %s", i.m_Error.c_str());
  }

}

void UpdateWeblication(GtkButton *b, void* d) {
  char *name;
  int row;
  TaskManager* tm = g_pTaskManager;
	 
  if ( !GTK_CLIST(tm->m_IWList)->selection ) {
    // nothing is selected
    OutputMessage("Please select a weblication first!");
    return;
  }

  row = (int)g_list_nth_data(GTK_CLIST(tm->m_IWList)->selection, 0);
  DEBUGMSG(taskmgr, "Selected row appears to be %d\n", row);
  string& guid = tm->m_WeblicationGuids[row];

  gtk_clist_get_text(GTK_CLIST(tm->m_IWList), row, 0, &name);

  Update(guid.c_str(), name);
}


void UpdateLocation(GtkButton *b, void* d) {
  char *name;
  int row;
  TaskManager* tm = g_pTaskManager;
	 
  if ( !GTK_CLIST(tm->m_LList)->selection ) {
    // nothing is selected
    OutputMessage("Please select a location first!");
    return;
  }

  row = (int)g_list_nth_data(GTK_CLIST(tm->m_LList)->selection, 0);
  string& guid = tm->m_LocationGuids[row];
  DEBUGMSG(taskmgr, "Selected row appears to be %d\n", row);

  gtk_clist_get_text(GTK_CLIST(tm->m_LList), row, 0, &name);

  Update(guid.c_str(), name);
}
 
void UpdateExtension(GtkButton *b, void* d) {
  char *name;
  int row;
  TaskManager* tm = g_pTaskManager;
	 
  if ( !GTK_CLIST(tm->m_EList)->selection ) {
    // nothing is selected
    OutputMessage("Please select an extension first!");
    return;
  }

  row = (int)g_list_nth_data(GTK_CLIST(tm->m_EList)->selection, 0);
  string& guid = tm->m_ExtensionGuids[row];
  DEBUGMSG(taskmgr, "Selected row appears to be %d\n", row);

  gtk_clist_get_text(GTK_CLIST(tm->m_EList), row, 0, &name);

  Update(guid.c_str(), name);
}

void GetDependencies(const string& dependee_guid, vector<string>& depends) {
	 strinthash h;
	 vector<SashExtensionItem> sashitems = 
		  g_pTaskManager->m_InstallationManager.GetInfoAllInstalled(SET_UNDEFINED);
	 int size = (int) sashitems.size();
	 int mine = -1, i, j, k;
	 bool *adj_mat = new bool[size*size];
	 // construct reverse dependency graph
	 for (i = 0 ; i < size ; i++) {
		  const SashExtensionItem& s = sashitems[i];
		  if (mine == -1 && s.guid == dependee_guid)
			   mine = i;
		  h[s.guid] = i;
		  for (j = 0 ; j < size ; j++) {
			   adj_mat[size*i + j] = false;
		  }
		 }
	 for (i = 0 ; i < size ; i++) {
		  const SashExtensionItem& s = sashitems[i];
		  int this_loc = h[s.guid];
		  vector<string>::const_iterator abi = s.dependencies.begin(), aei = s.dependencies.end();
		  while (abi != aei) {
			   int loc = h[*abi];
			   adj_mat[size*loc + this_loc] = true;
			   ++abi;
		  }
	 }
	 // crunch
	 bool changed = true;
	 while (changed) {
		  changed = false;
		  for (i = 0 ; i < size ; i++) {
			   for (j = 0 ; j < size ; j++) {
					if (i == j) continue;
					if (adj_mat[size*i + j]) {
						 for (k = 0 ; k < size ; k++) {
							  if (adj_mat[size*j + k] && ! adj_mat[size*i + k]) {
								   adj_mat[size*i + k] = true;
								   changed = true;
							  }
						 }
					}
			   }
		  }
	 }
	 for (i = 0 ; i < size ; i++) {
		  if (adj_mat[size*mine + i]) {
			   depends.push_back(sashitems[i].name);
		  }
	 }
	 delete[] adj_mat;
}

void EditSecurity(GtkButton *b, void* d) {
	 TaskManager* tm = g_pTaskManager;
	 if ( !GTK_CLIST(tm->m_IWList)->selection ) {
		  // nothing is selected
		  OutputMessage("Please select a weblication first!");

		  return;
	 }
	 int row = (int)g_list_nth_data(GTK_CLIST(tm->m_IWList)->selection, 0);
	 string& guid = tm->m_WeblicationGuids[row];

	 DEBUGMSG(taskmgr, "Selected row appears to be %d\n", row);

	 SashSecurityDialog SecurityDialog;
	 string secfile = WeblicationDirectory(tm->m_InstallationManager.GetSashInstallDirectory(), guid) + "/" +
					   SecurityFileName(guid);
	 SecurityHash security(secfile);
	 if (SecurityDialog.Run(security, GTK_WINDOW(tm->MainWindow)))
		  security.Save(secfile);
}

void RunAction(GtkButton *but, void* da) {
	 char *name;
	 int row;
	 TaskManager* tm = g_pTaskManager;
	 
	 if ( !GTK_CLIST(tm->m_IWList)->selection ) {
		  // nothing is selected
		  OutputMessage("Please select a weblication first!");
		  return;
	 }

	 row = (int)g_list_nth_data(GTK_CLIST(tm->m_IWList)->selection, 0);

	 gtk_clist_get_text(GTK_CLIST(tm->m_IWList), row, 0, &name);
	 string& guid = tm->m_WeblicationGuids[row];

	 SashExtensionItem sei = tm->m_InstallationManager.GetInfo(guid);

	 // give the user a list of actions to execute
	 GList* gl = NULL;

	 vector<SashExtensionItem::Action>::const_iterator a = sei.actions.begin(), b = sei.actions.end();
	 while (a != b) {
		  GtkWidget* list_item = gtk_list_item_new_with_label(a->name.c_str());
		  // we can take the pointer because the glist isn't sticking around
		  gtk_object_set_data(GTK_OBJECT(list_item), "guid", (gpointer) &(a->id));
		  gtk_widget_show(list_item);
		  gl = g_list_append(gl, list_item);
		  ++a;
	 }
  
	 GtkWidget* label = gtk_label_new("Please select the action(s) to run:");
	 gtk_widget_show(label);
	 GtkWidget* list = gtk_list_new();
	 gtk_list_append_items(GTK_LIST(list), gl);
	 gtk_list_set_selection_mode(GTK_LIST(list), GTK_SELECTION_MULTIPLE);
	 gtk_widget_show(list);
	 GtkWidget* d = gnome_dialog_new(("Run an action from '"+string(name)+"'").c_str(), 
									 GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
	 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), label, TRUE, TRUE, 0);
	 gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(d)->vbox), list, TRUE, TRUE, 0);
	 gnome_dialog_close_hides(GNOME_DIALOG(d), true);
	 if (gnome_dialog_run(GNOME_DIALOG(d)) == 1) {
		  gtk_widget_destroy(d);
		  return;
	 }

	 GList* dl = GTK_LIST(list)->selection;
	 // if the user selected everything, just invoke the runtime
	 // as if running the entire weblication
	 if (g_list_length(dl) == sei.actions.size()) {
		  DEBUGMSG(taskmgr, "All actions selected; attempting to run weblication %s\n", name);
		  ExecRuntime(guid);
	 } else {
		  while (dl) {
			   string* s = (string*) gtk_object_get_data(GTK_OBJECT(dl->data), "guid");
			   DEBUGMSG(taskmgr, "Attempting to run weblication %s, action %s\n", name, s->c_str());
			   ExecRuntime(guid, *s);
			   dl = dl->next;
		  }
	 }
	 gtk_widget_destroy(d);

}

void RunWeblication(GtkButton *b, void* d) {
	 char *name;
	 int row;
	 TaskManager* tm = g_pTaskManager;
	 
	 if ( !GTK_CLIST(tm->m_IWList)->selection ) {
		  // nothing is selected
		  OutputMessage("Please select a weblication first!");
		  return;
	 }

	 row = (int)g_list_nth_data(GTK_CLIST(tm->m_IWList)->selection, 0);
	 string& guid = tm->m_WeblicationGuids[row];

	 gtk_clist_get_text(GTK_CLIST(tm->m_IWList), row, 0, &name);

	 DEBUGMSG(taskmgr, "Attempting to run weblication %s\n", name);
	 ExecRuntime(guid);
}

void ExecRuntime(string webguid, string actionguid) {
	 string sash_runtime = GetSashBinaryPath();
	 if (! FileSystem::FileExists(sash_runtime)) {
		  OutputMessage("Cannot locate the Sash runtime! Please consider reinstalling Sash.");
	 } else {
		  if (fork() == 0) {
			   // check return value!
			   DEBUGMSG(taskmgr, "Spawning off weblication child process\n");
			   DEBUGMSG(taskmgr, "Command line: %s %s %s\n", 
						sash_runtime.c_str(), InPlaceToUpper(webguid).c_str(), 
						InPlaceToUpper(actionguid).c_str());
			   if (actionguid == "")
					execl(sash_runtime.c_str(), sash_runtime.c_str(), 
						  InPlaceToUpper(webguid).c_str(), NULL);
			   else {
					execl(sash_runtime.c_str(), sash_runtime.c_str(), 
						  InPlaceToUpper(webguid).c_str(), InPlaceToUpper(actionguid).c_str(), NULL);
			   }
			   _exit(0);
		  }
	 }
}

void ChildDead(int pid) {
	 DEBUGMSG(taskmgr, "Child  *************\n");
	 waitpid(pid, NULL, WNOHANG);
}

int AsyncServe(gpointer d) {
	 TaskManager* tm = g_pTaskManager;
	 fd_set sockSet;
	 FD_ZERO(&sockSet);
	 FD_SET(tm->sock, &sockSet);
	 timeval tv;
	 tv.tv_sec = 0;
	 tv.tv_usec = 0;
	 while (1) {
		  int ret = select(tm->sock+1, &sockSet, NULL, NULL, &tv);
		  if (ret < 0) {
			   OutputError("ERROR: %s on select", ret);
		  } else if (ret == 0) {
			   break;
		  }
    
		  if ( FD_ISSET(tm->sock, &sockSet) ) {
			   DEBUGMSG(taskmgr, "An action connected\n");

			   sockaddr_in clientAddr;
			   unsigned addrLen;
			   int slave = accept(tm->sock, (sockaddr*)&clientAddr, &addrLen);
			   static char buf[512];
			   bzero(buf, 512);
			   int r = read(slave, buf, 512);

			   DEBUGMSG(taskmgr, "read %d bytes: %s\n", r, buf);
			   close(slave);
      
			   string p(buf);
			   WeblicationRegister* w = new WeblicationRegister();
			   if (! w->parseRegistrationString(p)) {
					delete w;
			   } else {
					if (w->webl_name == "installer-refresh") {
						 DEBUGMSG(taskmgr, "New installation detected; refreshing\n");
						 tm->m_InstallationManager.ReloadGlobalRegistry();
						 tm->Fill();
						 delete w;
					} else {
						 active.push_back(w);
						 tm->UpdateRunning();
						 DEBUGMSG(taskmgr, "Added new running weblication %s\n",
								  w->webl_name.c_str());
					}
			   }
		  }

		  FD_ZERO(&sockSet);
		  FD_SET(tm->sock, &sockSet);
	 }
  
	 return 1;
}

bool weblication_valid_pred(WeblicationRegister* w) {
	 // see if the program is alive
	 if (kill((pid_t) atoi(w->pid.c_str()), 0)) {
		  
		  DEBUGMSG(taskmgr, "Weblication %s is no longer running\n",
				   w->webl_name.c_str());
		  g_pTaskManager->RemoveRunning(w->webl_name, w->pid);
		  
		  delete w;
		  return true;
	 }
	 return false;
}

int AsyncCheckPids(gpointer g) {
	 // run through each of the running weblications, and make sure they are 
	 // still running
	 active.erase(remove_if(active.begin(), active.end(), ptr_fun(weblication_valid_pred)), active.end());
	 return 1;
}

void HideMe(GtkButton* b, void* d) {
	 gtk_widget_hide(GTK_WIDGET(g_pTaskManager->MainWindow));
}

void LocatorChecked(GtkToggleButton* b, gpointer data) {
	 TaskManager* tm = g_pTaskManager;
	 bool is_on = gtk_toggle_button_get_active(b);
	 if (is_on) {
		  gtk_widget_set_sensitive(tm->LocatorAddressLabel, true);
		  gtk_widget_set_sensitive(tm->LocatorAddressEntry, true);
		  gtk_widget_set_sensitive(tm->AddLocatorButton, true);
		  gtk_widget_set_sensitive(tm->DeleteLocatorButton, true);
		  gtk_widget_set_sensitive(tm->LocatorList, true);
		  gtk_widget_set_sensitive(tm->FallBackCheckbox, true);
	 } else {
		  gtk_widget_set_sensitive(tm->LocatorAddressLabel, false);
		  gtk_widget_set_sensitive(tm->LocatorAddressEntry, false);
		  gtk_widget_set_sensitive(tm->AddLocatorButton, false);
		  gtk_widget_set_sensitive(tm->DeleteLocatorButton, false);
		  gtk_widget_set_sensitive(tm->LocatorList, false);
		  gtk_widget_set_sensitive(tm->FallBackCheckbox, false);
	 }
	 tm->LocatorData.use_locators = is_on;
	 SaveLocatorData();
}

void AddLocator(GtkButton* b, gpointer blah) {
	 TaskManager* tm = g_pTaskManager;
	 string s = gtk_entry_get_text(GTK_ENTRY(tm->LocatorAddressEntry));
	 if (! URLHasProtocol(s)) {
		  s = "http://" + s;
	 }
	 tm->LocatorData.locators.push_back(s);
	 char* data[1];
	 data[0] = (char*) s.c_str();
	 gtk_clist_append(GTK_CLIST(tm->LocatorList), data);
	 SaveLocatorData();
}

void DeleteLocator(GtkButton* b, gpointer blah) {
	 TaskManager* tm = g_pTaskManager;
	 if ( !GTK_CLIST(tm->LocatorList)->selection ) {
		  // nothing is selected
		  OutputMessage("Please select an item first!");
		  return;
	 }
	 
	 int row = (int)g_list_nth_data(GTK_CLIST(tm->LocatorList)->selection, 0);
	 DEBUGMSG(taskmgr, "Selected row appears to be %d\n", row);
	 
	 gtk_clist_remove(GTK_CLIST(tm->LocatorList), row);
	 tm->LocatorData.locators.erase(tm->LocatorData.locators.begin() + row);
	 SaveLocatorData();
}

void FallBackChecked(GtkToggleButton* b, gpointer data) {
	 TaskManager* tm = g_pTaskManager;
	 tm->LocatorData.fall_back = gtk_toggle_button_get_active(b);
	 SaveLocatorData();
}

void SaveLocatorData() {
	 TaskManager* tm = g_pTaskManager;
	 tm->m_InstallationManager.SetLocatorServices(
		  tm->LocatorData.use_locators,
		  tm->LocatorData.locators,
		  tm->LocatorData.fall_back);
}

void ChangeProxy(GtkToggleButton* b, gpointer data) {
	 TaskManager* tm = g_pTaskManager;
	 bool on = gtk_toggle_button_get_active(b);
	 int val = atoi((char*)data);
	 if (val == 1) {
		  if (on) {
			   gtk_widget_set_sensitive(tm->ProxyAddress, true);
			   gtk_widget_set_sensitive(tm->ProxyAddressLabel, true);
			   gtk_widget_set_sensitive(tm->ProxyPort, true);
			   gtk_widget_set_sensitive(tm->ProxyPortLabel, true);

		  } else {
			   gtk_widget_set_sensitive(tm->ProxyAddress, false);
			   gtk_widget_set_sensitive(tm->ProxyAddressLabel, false);
			   gtk_widget_set_sensitive(tm->ProxyPort, false);
			   gtk_widget_set_sensitive(tm->ProxyPortLabel, false);
		  }
	 }
	 
	 if (on) {
		  nsCOMPtr<nsIPref> p = do_GetService(NS_PREF_CONTRACTID);
		  if (p) {
			   p->SetIntPref("network.proxy.type", val);
			   p->SavePrefFile(NULL);
		  }
	 }
}

void ProxyAddressChanged(GtkEntry* b, gpointer data) {
	 TaskManager* tm = g_pTaskManager;
	 const char* c = gtk_entry_get_text(GTK_ENTRY(tm->ProxyAddress));
	 nsCOMPtr<nsIPref> p = do_GetService(NS_PREF_CONTRACTID);
	 if (p) {
		  p->SetCharPref("network.proxy.http", c);
		  p->SavePrefFile(NULL);
	 }
}

void ProxyPortChanged(GtkEntry* b, gpointer data) {
	 TaskManager* tm = g_pTaskManager;
	 const char* c = gtk_entry_get_text(GTK_ENTRY(tm->ProxyPort));
	 nsCOMPtr<nsIPref> p = do_GetService(NS_PREF_CONTRACTID);
	 if (p) {
		  p->SetIntPref("network.proxy.http_port", atoi(c));
		  p->SavePrefFile(NULL);
	 }
}

gboolean CloseOrExit(GtkWidget* b, GdkEvent* e, gpointer data) {
	 TaskManager* tm = g_pTaskManager;
	 if (tm->m_pTMA) {
		  HideMe(NULL, data);
		  return true;
	 } else {
		  ExitEverything(NULL, data);
		  return false;
	 }
}
