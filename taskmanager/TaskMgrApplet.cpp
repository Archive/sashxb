
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Ari Heitner, AJ Shankar

Panel app which invokes the task manager on click

*****************************************************************/

#include <string.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <gnome.h>
#include <applet-widget.h>
#include <vector>
#include <algorithm>
#include <string>
#include "sash_constants.h"
#include "TaskMgrApplet.h"
#include "TaskMgr.h"
#include "carlos_large.xpm"
#include "carlos_medium.xpm"
#include "carlos_small.xpm"
#include "debugmsg.h"

using namespace std;
extern TaskManager* g_pTaskManager;

TaskManagerApplet::TaskManagerApplet() : m_picwidget(NULL) {
  m_applet = applet_widget_new("TaskManagerApplet");
  gtk_widget_realize(m_applet);
  applet_widget_set_tooltip(APPLET_WIDGET(m_applet), "SashXB Task Manager");
  
  m_button = gtk_button_new();

  applet_widget_add(APPLET_WIDGET(m_applet), m_button);
  
  gtk_button_set_relief(GTK_BUTTON(m_button), GTK_RELIEF_NONE);

  SignalConnect();
  
  gtk_widget_show_all(m_applet);

}

void TaskManagerApplet::SignalConnect() {
  applet_widget_register_stock_callback(APPLET_WIDGET(m_applet), 
										"Exit", 
										GNOME_STOCK_MENU_EXIT,
										_("Exit Sash Task Manager"), 
										&ExitApplet, 
										this);
#ifdef HAVE_PANEL_PIXEL_SIZE
  gtk_signal_connect(GTK_OBJECT(m_applet),
					 "change_pixel_size",
					 GTK_SIGNAL_FUNC(ResizeButton),
					 this);
#endif

  gtk_signal_connect(GTK_OBJECT(m_applet), 
                     "delete_event",
                     GTK_SIGNAL_FUNC(ExitApplet),
                     this);
  gtk_signal_connect(GTK_OBJECT(m_applet),
                     "destroy",
                     GTK_SIGNAL_FUNC(ExitApplet),
                     this);
                     
  gtk_signal_connect(GTK_OBJECT(m_button),
                     "clicked",
                     GTK_SIGNAL_FUNC(LaunchTaskManager),
                     this);                     
}

#ifdef HAVE_PANEL_PIXEL_SIZE
void TaskManagerApplet::ResizeButton(AppletWidget* aw, int height, void* gp) {
  DEBUGMSG(taskmgrapplet, "Resizing button for new panel size %d...\n", 
	   height);
  TaskManagerApplet* tma = (TaskManagerApplet*) gp;
  GdkBitmap *mask;
  GdkPixmap *pm;
  static char** pic;

  if (height > 36) { 
    pic = carlos_large_xpm;
  } else if (height > 24) {
    pic = carlos_medium_xpm;
  } else {
    pic = carlos_small_xpm;
  }
  pm = gdk_pixmap_colormap_create_from_xpm_d(NULL, gtk_widget_get_default_colormap(), 
					     &mask, NULL, (gchar**) pic);
  if (tma->m_picwidget) 
    gtk_container_remove(GTK_CONTAINER(tma->m_button), tma->m_picwidget);

  tma->m_picwidget = gtk_pixmap_new(pm, mask);
  
  gtk_container_add(GTK_CONTAINER(tma->m_button), tma->m_picwidget);
 
  gtk_widget_show_all(tma->m_applet);

};
#endif

void TaskManagerApplet::LaunchTaskManager(GtkButton *b, TaskManagerApplet *tma) {
  DEBUGMSG(taskmgrapplet, "In launchMgr()...");

  if (g_pTaskManager)
	   g_pTaskManager->DisplayMe();
}

void TaskManagerApplet::ExitApplet(AppletWidget* aw, void* data) {
  TaskManagerApplet* tma = (TaskManagerApplet *) data;
  if (tma->m_applet)
	   applet_widget_remove(APPLET_WIDGET(tma->m_applet));
  applet_widget_gtk_main_quit();
}

