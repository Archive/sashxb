
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Ari Heitner, AJ Shankar

Sash Task Manager: uninstall, run, kill, etc. of Sash components

*****************************************************************/

#ifndef __TASKMGR_H__
#define __TASKMGR_H__

//  All weblications register themselves with the TaskManager on startup (they
//  start it if it isn't running yet). It lives in the panel, and launches
//  a useful control center to manage running and installed weblications
//  and extensions

#include <vector>
#include <string>
#include "InstallationManager.h"
#include "TaskManagerWidgets.h"
#include "TaskMgrApplet.h"

struct TaskManager : public Widgets {
	 TaskManager();
	 ~TaskManager();
	 void DisplayMe();
	 void Fill();
	 void UpdateRunning();
	 void RemoveRunning(const std::string& name, const std::string& guid);
	 // pass an applet through if there is one
	 void Initialize(TaskManagerApplet* tma);

	 InstallationManager m_InstallationManager;
	 TaskManagerApplet* m_pTMA;
	 sockaddr_in address;
	 int sock;
	 void SetupTCPServer();
	 int m_CheckID;
	 int m_ServeID;

	 vector<string> m_WeblicationGuids, m_ExtensionGuids, m_LocationGuids;

	 struct {
		  bool use_locators, fall_back;
		  std::vector<std::string> locators;
	 } LocatorData;
};

extern "C" {
int AsyncServe(gpointer d);
int AsyncCheckPids(gpointer g);
void HideMe(GtkButton *b, void* d);
void ExitEverything(GtkButton *b, void *d);
void KillWeblication(GtkButton *b, void* d);
void TerminateWeblication(int sig);
void RunWeblication(GtkButton *b, void* d);
void RunAction(GtkButton *b, void* d);
void ExecRuntime(std::string webguid, std::string actionguid = "");
void UninstallWeblication(GtkButton *b, void* d);
void UninstallLocation(GtkButton *b, void* d);
void UninstallExtension(GtkButton *b, void* d);
void Update(const char* guid, const char* name);
void UpdateWeblication(GtkButton *b, void* d);
void UpdateLocation(GtkButton *b, void* d);
void UpdateExtension(GtkButton *b, void* d);
void GetDependencies(const std::string& dependee_guid, 
							std::vector<std::string>& depends);
void EditSecurity(GtkButton *b, void* d);
void ChildDead(int);
void LocatorChecked(GtkToggleButton* b, gpointer data);
void AddLocator(GtkButton* b, gpointer blah);
void DeleteLocator(GtkButton* b, gpointer blah);
void FallBackChecked(GtkToggleButton* b, gpointer data);
void SaveLocatorData();
void ChangeProxy(GtkToggleButton* b, gpointer data);
void ProxyAddressChanged(GtkEntry* b, gpointer data);
void ProxyPortChanged(GtkEntry* b, gpointer data);
gboolean CloseOrExit(GtkWidget* b, GdkEvent* e, gpointer data);
}

#endif // #define __TASKMGR_H__ 
