
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include <stdio.h>
#include <nsCOMPtr.h>
#include <nsIURI.h>
#include <nsNetCID.h>
#include <nsIServiceManager.h>
#include <nsIEventQueueService.h>
#include <nsNetUtil.h>
#include "downloader.h"
#include <assert.h>
#include <gtk/gtk.h>
#include <debugmsg.h>
#include <nsEmbedAPI.h>

static NS_DEFINE_CID(kEventQueueServiceCID, NS_EVENTQUEUESERVICE_CID);
PRBool gKeepRunning = PR_TRUE;

class MyCallback : public DownloadCallback
{

public:
  MyCallback() {}
  
  virtual void UpdateProgress(guint32 progress, guint32 progressMax) {
    //printf("%u/%u %f\n", progress, progressMax, 1.0 * progress / progressMax);
  }
  virtual void UpdateStatus(const string & msg) {
    printf("%s\n", msg.c_str());
  }
};

void testDownload(const string & url, const string & filename)
{
  Downloader d(url, filename, new MyCallback());
  d.Download("Thu, 29 Mar 2001 17:53:01 GMT");
}

#define URL "http://orion/poweredby.png"

int main(int argc, char **argv)
{
  if (argc != 3) {
    fprintf(stderr, "Usage: %s url filename\n", argv[0]);
    return 1;
  }
    
  NS_InitEmbedding(nsnull, nsnull);
	testDownload(argv[1], argv[2]);
  DEBUGMSG(downloader, "Returning from main() in testdownload.cpp\n");
  return 0;
}
