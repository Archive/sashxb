#include "SashNet.h"
#include "nsIInputStream.h"
#include "nsIServiceManager.h"
#include "nsIInterfaceRequestor.h"
#include "nsIURI.h"
#include "nsNetUtil.h"
#include "nsIByteArrayInputStream.h"
#include "nsIUploadChannel.h"
#include "nsIFileTransportService.h"
#include "nsISocketTransportService.h"

#include <unistd.h>

#include <algorithm>
#include <functional>

#include "sash_error.h"
#include "xpcomtools.h"
#include "debugmsg.h"
#include "extensiontools.h"

#define BUFSIZE 4096

NS_IMPL_ISUPPORTS5_CI(SashNet, sashINet, nsIStreamListener, nsIHttpHeaderVisitor, 
				   nsIInterfaceRequestor,  nsIProgressEventSink);

SashNet::SashNet() : 
	 m_requestType("GET"),
	 m_file(-1), 
	 m_status(sashINet::NET_SETUP),
	 m_HTTPStatus(-1)
{
	 NS_INIT_ISUPPORTS();

	 nsresult rv;

	 nsCOMPtr<nsIEventQueueService> eqs = 
		  do_GetService(NS_EVENTQUEUESERVICE_CONTRACTID, &rv);
	 if (!NS_SUCCEEDED(rv))
		  OutputError("Could not get Mozilla's event queue service!");
	 
	 rv = eqs->CreateMonitoredThreadEventQueue();
	 if (!NS_SUCCEEDED(rv))
		  OutputError("Could not create monitored thread event queue!");
	 
	 rv = eqs->GetThreadEventQueue(NS_CURRENT_THREAD, getter_AddRefs(m_eventq));
	 if (!NS_SUCCEEDED(rv))
		  OutputError("Could not get current event queue!\n");
}

SashNet::~SashNet() {
	 DEBUGMSG(sashnet, "Destroying SashNet object\n");
}

bool SashNet::IsSuccessful(int httpStatus) {
	 return (httpStatus / 100 == 2) || (httpStatus == 301) || (httpStatus == 302);
}

// ----------------- Header management --------------------
bool pair_find(pair<string, string> v, const char * a) {
	 return (strcasecmp(v.first.c_str(), a) == 0);	 
}

void SashNet::SetHeader(HeaderMap & set, const char * header, const char * value) {
	 HeaderMap::iterator pos = find_if(set.begin(), set.end(), bind2nd(ptr_fun(pair_find), header));
	 if (pos != set.end())
		  set.erase(pos);
	 set.push_back(make_pair(header, value));
}

string SashNet::GetHeader(const HeaderMap & set, const char * header) {
	 HeaderMap::const_iterator pos = find_if(set.begin(), set.end(), bind2nd(ptr_fun(pair_find), header));
	 if (pos != set.end())
		  return pos->second;
	 else
		  return "";
}

nsresult SashNet::GetHeaderByIndex(const HeaderMap & set, PRUint32 i, char ** header, char ** value) {
	 if (i < set.size()) {
		  XP_COPY_STRING(set[i].first, header);
		  XP_COPY_STRING(set[i].second, value);
		  return NS_OK;
	 }
	 return NS_ERROR_INVALID_ARG;
}

NS_IMETHODIMP SashNet::SetRequestHeader(const char * header, const char * value) {
	 SetHeader(m_RequestHeaders, header, value);
	 return NS_OK;
}

NS_IMETHODIMP SashNet::GetRequestHeader(const char * header, char ** value) {
	 string res = GetHeader(m_RequestHeaders, header);
	 XP_COPY_STRING(res, value);
	 return NS_OK;
}

NS_IMETHODIMP SashNet::GetNumRequestHeaders(PRUint32 * n) {
	 *n = m_RequestHeaders.size();
	 return NS_OK;
}

NS_IMETHODIMP SashNet::GetRequestHeaderByIndex(PRUint32 i, char ** header, char ** value) {
	 return GetHeaderByIndex(m_RequestHeaders, i, header, value);
}

NS_IMETHODIMP SashNet::SetResponseHeader(const char * header, const char * value) {
	 SetHeader(m_ResponseHeaders, header, value);
	 return NS_OK;
}

NS_IMETHODIMP SashNet::GetResponseHeader(const char * header, char ** value) {
	 string res = GetHeader(m_ResponseHeaders, header);
	 XP_COPY_STRING(res, value);
	 return NS_OK;
}

NS_IMETHODIMP SashNet::GetNumResponseHeaders(PRUint32 * n) {
	 *n = m_ResponseHeaders.size();
	 return NS_OK;
}

NS_IMETHODIMP SashNet::GetResponseHeaderByIndex(PRUint32 i, char ** header, char ** value) {
	 return GetHeaderByIndex(m_ResponseHeaders, i, header, value);
}

// -------------------- nsIStreamListener Callbacks ------------------------
NS_IMETHODIMP SashNet::SetStatusCallback(sashINetCallback * cb) {
	 m_cb = cb;
	 return NS_OK;
}

NS_IMETHODIMP SashNet::VisitHeader(const nsACString & header, const nsACString & value) {
	 const nsAFlatCString & h = PromiseFlatCString(header);
	 const nsAFlatCString & v = PromiseFlatCString(value);
	 return SetResponseHeader(h.get(), v.get());
}

NS_IMETHODIMP
SashNet::OnDataAvailable(nsIRequest *req, nsISupports *ctxt,
						 nsIInputStream *stream,
						 PRUint32 offset, PRUint32 count)
{
  char buf[BUFSIZE];
  nsresult rv;
  PRUint32 bytesRead = 0;

  if (m_status == NET_ABORTED)
	   return NS_OK;
  
  DEBUGMSG(sashnet, "SashNet::OnDataAvailable\n");

  while (count) {
    PRUint32 amount = PR_MIN(count, BUFSIZE);
    
    rv = stream->Read(buf, amount, &bytesRead);
    if (NS_FAILED(rv)) {
      DEBUGMSG(sashnet, "stream->Read fail\n");
      return rv;
    }

	DEBUGMSG(sashnet, "read %d bytes\n", bytesRead);

    if (m_file >= 0)
		 fs.WriteToFile(buf, bytesRead, m_file);

	if (m_keepData)
		 for (unsigned int i = 0; i < bytesRead; i++) {
			  m_data.push_back(buf[i]);
		 }

	count -= bytesRead;
  }
  return NS_OK;
}

NS_IMETHODIMP SashNet::OnStartRequest(nsIRequest * req, nsISupports * ctxt) {
	 DEBUGMSG(sashnet, "OnStartRequest\n");
	 return NS_OK;
}

NS_IMETHODIMP SashNet::OnStopRequest(nsIRequest * req, nsISupports * ctxt, nsresult status) {
	 DEBUGMSG(sashnet, "OnStopRequest\n");
	 // request finished
	 m_httpChan->GetResponseStatus((PRUint32*)&m_HTTPStatus);
	 DEBUGMSG(sashnet, "OnStopRequest -- HTTP status = %d\n", m_HTTPStatus);
	 // get response headers
	 DEBUGMSG(sashnet, "Response headers:\n");
	 nsCOMPtr<nsIHttpHeaderVisitor> visitor = do_QueryInterface((nsIHttpHeaderVisitor*)this);
	 m_httpChan->VisitResponseHeaders(visitor);

	 // If the status signals some sort of error, make sure to catch it.
	 if (m_HTTPStatus == -1)
		  m_status = NET_ERROR_HOSTNAME_NOT_RESOLVED;
	 else if (m_HTTPStatus == 404)
		  m_status = NET_ERROR_FILE_NOT_FOUND;

	 if (m_status != NET_ABORTED 
		 && m_status != NET_ERROR 
		 && m_status != NET_ERROR_HOSTNAME_NOT_RESOLVED
		 && m_status != NET_ERROR_FILE_NOT_FOUND
		 && IsSuccessful(m_HTTPStatus)) 
     {
		  SetFinished();
	 }
	 return NS_OK;
}
// -------------------- 

NS_IMETHODIMP
SashNet::GetInterface(const nsIID &iid, void **result)
{
  return QueryInterface(iid, result);
}

NS_IMETHODIMP
SashNet::OnStatus(nsIRequest *req, nsISupports *ctx,
			 nsresult status, const PRUnichar *statusText)
{
	 if (m_cb)
		  m_cb->UpdateStatus(GetStatusMsg(status, statusText).c_str());
	 return NS_OK;
}

// Return a string version of these unicode dealies
// TODO: Speed this up
static string UnicharToString(const PRUnichar *str)
{
  static const int limit = 200;
  string ret;
  int read = 0;

  while (*str != 0 && read < limit) {
    ret += *(char *)str;
    str++;
    read++;
  }
  return ret;
}

// Return a text discription of the status message
string
SashNet::GetStatusMsg(nsresult status, const PRUnichar *statusText)
{
  string res;

  switch (status) {
  case NS_NET_STATUS_READ_FROM:
    res += "Reading from ";
    break;
  case NS_NET_STATUS_WROTE_TO:
    res += "Wrote to ";
    break;
  case NS_NET_STATUS_RESOLVING_HOST:
    res += "Resolving host ";
    break;
  case NS_NET_STATUS_CONNECTED_TO:
    res += "Connected to ";
    break;
  case NS_NET_STATUS_SENDING_TO:
    res += "Sending to ";
    break;
  case NS_NET_STATUS_RECEIVING_FROM:
    res += "Receiving from ";
    break;
  case NS_NET_STATUS_CONNECTING_TO:
    res += "Connecting to ";
    break;
  default:
    return "Unknown status message";
  }
  res += UnicharToString(statusText);
  return res;
}

string SashNet::statusString(int status){
	 switch (status){

	 case NET_SETUP:
		  return "NET_SETUP";
		  break;
	 case NET_TRANSFERRING:
		  return "NET_TRANSFERRING";
		  break;
	 case NET_DONE:
		  return "NET_DONE";
		  break;
	 case NET_ABORTED:
		  return "NET_ABORTED";
		  break;
	 case NET_ERROR:
		  return "NET_ERROR";
		  break;
	 case NET_ERROR_FILE_NOT_FOUND:
		  return "NET_ERROR_FILE_NOT_FOUND";
		  break;
	 case NET_ERROR_HOSTNAME_NOT_RESOLVED:
		  return "NET_ERROR_HOSTNAME_NOT_RESOLVED";
		  break;
	 }
	 return "ERROR: Unknown NET constant";
}

NS_IMETHODIMP
SashNet::OnProgress(nsIRequest *req, nsISupports *ctx,
			   PRUint32 progress, PRUint32 progressMax)
{
	 DEBUGMSG(sashnet, "progress: %d of %d\n", progress, progressMax);
	 if (m_cb) {
		  m_cb->UpdateProgress(progress, progressMax);
	 }
	 return NS_OK;
}

// -------------------- Core SashNet methods ------------------------
void SashNet::SetFinished() {
	 DEBUGMSG(sashnet, "Download finished!\n");
	 m_status = NET_DONE;
	 if (m_cb)
		  m_cb->UpdateComplete();
}

NS_IMETHODIMP SashNet::GetHTTPStatus(PRInt32 * status) {
	 *status = m_HTTPStatus;
	 return NS_OK;
}

NS_IMETHODIMP SashNet::GetResponseData(PRUint32 * size, char ** data) {
	 *size = m_data.size();
	 if (m_data.size() > 0) {
		  *data = (char *) nsMemory::Alloc(m_data.size()+1);
		  for (unsigned int i = 0; i < m_data.size(); i++) {
			   (*data)[i] = m_data[i];
		  }
		  (*data)[*size] = '\0';
	 }
	 else {
		  *data = NULL;
	 }
	 return NS_OK;
}

void SashNet::Setup() {
	 m_HTTPStatus = -1;
	 m_data.clear();
	 if (m_file != -1) {
		  fs.CloseFile(m_file);
		  m_file = -1;
	 }
}

NS_IMETHODIMP SashNet::StartTransfer() {
	 DEBUGMSG(sashnet, "StartTransfer\n");
	 string proto = m_url.substr(0, 7);
	 string name = m_url.substr(7, m_url.size() - 7);
	 DEBUGMSG(sashnet, "Protocol is %s, File is %s\n", proto.c_str(), name.c_str());
	 m_startTime = time(NULL);
	 if (proto == "file://")
		  StartFileTransfer(name, m_filename);
	 else if (proto == "http://")
		  StartHTTPTransfer();
	 return NS_OK;
}

NS_IMETHODIMP SashNet::Process(PRInt32 * status) {
	 if ((m_status != NET_ABORTED) 
		 && (m_status != NET_ERROR)
		 && (m_status != NET_ERROR_FILE_NOT_FOUND)
		 && (m_status != NET_ERROR_HOSTNAME_NOT_RESOLVED)){
		  m_eventq->ProcessPendingEvents();
	 }

	 if (m_status == NET_DONE){
		  DEBUGMSG(sashnet, "Transfer complete\n");
		  if (m_file >= 0) {
			   fs.CloseFile(m_file);
			   m_file = -1;
		  }
		  if (m_HTTPStatus == 301) {
			   char * redirect;
			   GetResponseHeader("Location", &redirect);
			   DEBUGMSG(sashnet, "Location: %s\n", redirect);
		  }
		  if (!IsSuccessful(m_HTTPStatus)) {
			   if (m_HTTPStatus == -1){
					m_status = NET_ERROR_HOSTNAME_NOT_RESOLVED;
			   } else if (m_HTTPStatus == 404){
					m_status = NET_ERROR_FILE_NOT_FOUND;
			   } else {
					m_status = NET_ERROR;
			   }
		  }
	 }
	 *status = m_status;
	 return NS_OK;
}

NS_IMETHODIMP SashNet::ProcessUntilComplete(PRInt32 * status) {
	 DEBUGMSG(sashnet, "ProcessUntilComplete\n");
	 do {
		  Process(status);
		  usleep(1);
	 }
	 while (m_status == NET_TRANSFERRING);
	 return NS_OK;
}

NS_IMETHODIMP SashNet::Abort() {
	 DEBUGMSG(sashnet, "Transfer aborted!\n");
	 m_status = NET_ABORTED;
	 if (m_cb)
		  m_cb->UpdateAbort();
	 if (m_file >= 0) {
		  fs.CloseFile(m_file);
		  m_file = -1;
	 }
	 return m_httpChan->Cancel(NS_BINDING_ABORTED);
}

void SashNet::StartFileTransfer(const string & source, const string & dest) {
	 m_status = NET_ERROR;
	 if (m_keepData) {
		  // Load the file into memory
		  int size = fs.FileSize(source);
		  if (size > 0) {
			   int SourceFile = fs.OpenFile(source, true);
			   if (SourceFile >= 0) {
					if (size == 0) {
						 fs.CloseFile(SourceFile);
					}
					else {
						 char * buf = new char[size];
						 if (fs.ReadFromFile(buf, size, SourceFile) == size) {
							  fs.CloseFile(SourceFile);
							  for (int i = 0; i < size; i++)
								   m_data.push_back(buf[i]);
							  if (m_filename != "") {
								   // Also copy the file (already in memory)
								   int DestFile = fs.OpenFile(m_filename, false);
								   if (DestFile >= 0) {
									   if (fs.WriteToFile(buf, size, DestFile) == size)
											SetFinished();
										fs.CloseFile(DestFile);
								   }
							  }
							  else {
								   // Don't copy the file
								   SetFinished();
							  }
						 }
						 delete [] buf;
					}
			   }
		  }
	 }
	 else if (m_filename != "") {
		  // Just copy the file
		  fs.CopyFile(source, m_filename);
		  SetFinished();
	 }
	 else {
		  // do nothing!
		  SetFinished();
	 }
}

void SashNet::StartHTTPTransfer() {
	 DEBUGMSG(sashnet, "StartHTTPTransfer\n");
	 nsresult rv;
	 m_status = NET_ERROR;

	 nsCOMPtr<nsIInterfaceRequestor> notifier = do_QueryInterface((nsIInterfaceRequestor *)this);
	 if (! notifier){
		  DEBUGMSG(sashnet, "null notifier.\n");
		  return;
	 }

	 DEBUGMSG(sashnet, "creating URI object\n");
	 nsCOMPtr<nsIURI> uri;
	 rv = NS_NewURI(getter_AddRefs(uri), m_url.c_str());
	 if (!NS_SUCCEEDED(rv)) {
		  DEBUGMSG(sashnet, "Unable to create URI object\n");
		  return;
	 }

	 DEBUGMSG(sashnet, "creating channel object\n");
	 nsCOMPtr<nsIChannel> chan;
	 rv = NS_NewChannel(getter_AddRefs(chan), uri, nsnull, nsnull, notifier);
	 if (!NS_SUCCEEDED(rv)) {
		  DEBUGMSG(sashnet, "Unable to create channel object\n");
		  return;
	 }

	 DEBUGMSG(sashnet, "looking for HTTP channel\n");
	 m_httpChan = do_QueryInterface(chan);
	 if (!m_httpChan) {
		  DEBUGMSG(sashnet, "Not HTTP\n");
		  return;
	 }

	 bool is_zip = (m_url.substr(m_url.length() - 3, 3) == ".gz" ||
					m_url.substr(m_url.length() - 4, 4) == ".tgz");

	 if (is_zip) {
		  if (! NS_SUCCEEDED(m_httpChan->SetApplyConversion(false))){
			   DEBUGMSG(sashnet, "Couldn't set apply conversion.\n");
			   return;
		  }
	 }
	 
	 if (! NS_SUCCEEDED(m_httpChan->SetRedirectionLimit(5))){
		  DEBUGMSG(sashnet, "Couldn't set redirection limit.\n");
		  return;
	 }

	 DEBUGMSG(sashnet, "Setting Request headers (%d headers)\n",
			  m_RequestHeaders.size());
	 for (unsigned int i = 0; i < m_RequestHeaders.size(); i++){
		  m_httpChan->SetRequestHeader(
			   nsDependentCString(m_RequestHeaders[i].first.c_str(), 
								  m_RequestHeaders[i].first.size()),
			   nsDependentCString(m_RequestHeaders[i].second.c_str(), 
								  m_RequestHeaders[i].second.size()));
	 }
	 m_ResponseHeaders.clear();
  
	 if (m_filename != "") {
		  m_file = fs.OpenFile(m_filename, false);
		  if (m_file < 0)
			   DEBUGMSG(sashnet, "Unable to open file %s for writing\n", m_filename.c_str());
	 }

	 DEBUGMSG(sashnet, "Ready to transfer!\n");
	 m_status = NET_TRANSFERRING;
}

// --------------------- SashDownloader --------------------------
SASH_IMPL_ISUPPORTS_INHERITED_CI1(SashDownloader, SashNet, sashIDownloader);

SashDownloader::~SashDownloader() { }

NS_IMETHODIMP SashDownloader::InitializeDownload(const char * url) {
	 Setup();
	 m_url = url;
	 m_keepData = true;
	 return NS_OK;
}

NS_IMETHODIMP SashDownloader::InitializeDownloadToFile(const char * url, const char * filename, 
													   PRBool KeepData) 
{
	 Setup();
	 m_url = url;
	 m_keepData = KeepData;
	 m_filename = filename;
	 return NS_OK;
}

void SashDownloader::StartHTTPTransfer() {
	 DEBUGMSG(sashnet, "SashDownloader::StartHTTPTransfer");
	 SashNet::StartHTTPTransfer();
	 if (m_status == NET_TRANSFERRING){

		  if (! NS_SUCCEEDED(m_httpChan->AsyncOpen((nsIStreamListener*)this, nsnull))){
			   DEBUGMSG(sashnet, "HTTP Channel AsyncOpen failed\n");
		  }
	 }
}
// --------------------- SashUploader --------------------------
SASH_IMPL_ISUPPORTS_INHERITED_CI1(SashUploader, SashNet, sashIUploader);

SashUploader::SashUploader() {
	 m_requestType = "POST";
}

/* void InitializeUploadFromBuffer (in PRUint32 size, [array, size_is (size)] in char data); */
NS_IMETHODIMP SashUploader::InitializeUploadFromBuffer(const char * url, PRUint32 size, char *data) {
	 Setup();
	 m_url = url;
	 nsCOMPtr<nsIByteArrayInputStream> bytes;
	 nsresult rv = NS_NewByteArrayInputStream(getter_AddRefs(bytes), 
											  data, size);
	 if (!NS_SUCCEEDED(rv))
		  return rv;

	 m_inputStream = do_QueryInterface(bytes);

	 return NS_OK;
}

// This is an unfortunate hack. It seems that the Mozilla uploader
// does not add "\r\n" between the headers and the request data for POSTs, so
// we add it in ourselves.
// (The "\r\n" signifies the end of the headers and the beginning of 
// the data.)
NS_IMETHODIMP SashUploader::InitializeUploadFromBufferWithPrepend(const char *url, 
														 PRUint32 size,
														 char *data){
	 DEBUGMSG(sashnet, "SashUploader::InitializeUploadFromBufferWithPrepend\n");
	 char *newdata = new char[size + 2];
	 if (newdata){
		  newdata[0] = '\r';
		  newdata[1] = '\n';
		  memcpy(&(newdata[2]), data, size);
	 }

	 string cl = "Content-Length";
	 char *tempdata = new char[20];
	 sprintf(tempdata, "%d", size);
	 ((SashNet *)this)->SetRequestHeader(cl.c_str(), tempdata);
	 return (InitializeUploadFromBuffer(url, size+2, newdata));
}


// This is an unfortunate hack. It seems that the Mozilla uploader
// does not add "\r\n" between the headers and the request data for POSTs, so
// we add it in ourselves. Since there is no simple way to add the "\r\n"
// before the file, we read the entire file into a buffer and upload
// the buffer with a "\r\n" prepended.
// (The "\r\n" signifies the end of the headers and the beginning of 
// the data.)
/* void InitializeUploadFromFile (in string FileName); */
NS_IMETHODIMP SashUploader::InitializeUploadFromFile(const char * url, const char *FileName) {
	 // read data out of the file.
	 DEBUGMSG(sashnet, "SashUploader::InitializeUploadFromFile\n");

	 char *data;
	 int fd;
	 bool sizematch;
	 int filesize = FileSystem::FileSize(FileName);
	 if (filesize > -1){
		  data = new char[filesize+2];
		  data[0] = '\r';
		  data[1] = '\n';
		  FileSystem *fs = new FileSystem();
		  fd = fs->OpenFile(FileName, true);
		  sizematch = (fs->ReadFromFile(&(data[2]), filesize, fd) == filesize);
		  fs->CloseFile(fd);
		  delete fs;

		  if (! sizematch){
			   DEBUGMSG(sashnet, "Amount of data read from file does not match the filesize");
			   return NS_ERROR_FAILURE;
		  }

		  DEBUGMSG(sashnet, "Initializing upload for file %s, (%d bytes)\n",
				   FileName, filesize);

		  string cl = "Content-Length";
		  char *tempdata = new char[20];
		  sprintf(tempdata, "%d", filesize);
		  ((SashNet *)this)->SetRequestHeader(cl.c_str(), tempdata);
		  return (InitializeUploadFromBuffer(url, filesize+2, data));
	 } else {
		  DEBUGMSG(sashnet, "couldn't find file %s\n", FileName);
	 }
	 return NS_OK;
}

void SashUploader::StartHTTPTransfer() {
	 DEBUGMSG(sashnet, "SashUploader::StartHTTPTransfer\n");
	 SashNet::StartHTTPTransfer();

	 if (m_status == NET_TRANSFERRING){
		  DEBUGMSG(sashnet, "getting uploadChannel\n");
		  nsCOMPtr<nsIUploadChannel> uploadChannel = do_QueryInterface(m_httpChan);
		  DEBUGMSG(sashnet, "setting uploadStream\n");
		  uploadChannel->SetUploadStream(m_inputStream, nsnull, -1);
		  
		  DEBUGMSG(sashnet, "getting listener\n");
		  nsCOMPtr<nsIStreamListener> listener = do_QueryInterface((nsIStreamListener *) this);
		  
		  DEBUGMSG(sashnet, "opening async listener\n");
		  m_httpChan->AsyncOpen(listener, nsnull);
	 }
}

// --------------------- SashNet module declaration ----------------------
NS_DECL_CLASSINFO(SashNet);
NS_DECL_CLASSINFO(SashDownloader);
NS_DECL_CLASSINFO(SashUploader);

NS_GENERIC_FACTORY_CONSTRUCTOR(SashNet);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashDownloader);
NS_GENERIC_FACTORY_CONSTRUCTOR(SashUploader);

static nsModuleComponentInfo components[] = {
	 MODULE_COMPONENT_ENTRY(SashNet, SASHNET, "SashNet Object"),
	 MODULE_COMPONENT_ENTRY(SashDownloader, SASHDOWNLOADER, "SashDownloader Object"),
	 MODULE_COMPONENT_ENTRY(SashUploader, SASHUPLOADER, "SashUploader Object")
};

NS_IMPL_NSGETMODULE("SashNetModule", components)
