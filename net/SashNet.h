
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin, Andrew Chatham

*****************************************************************/

#ifndef _SASHNET_H
#define _SASHNET_H

#include "sashINet.h"

#include "nsIStreamListener.h"
#include "nsCOMPtr.h"
#include "nsIHttpChannel.h"
#include "nsIEventQueueService.h"
#include "nsIInputStream.h"
#include "nsIInterfaceRequestor.h"
#include "nsIProgressEventSink.h"

#include <vector>
#include <string>

#include "FileSystem.h"

typedef vector<pair<string, string> > HeaderMap;

#define SASHNET_CID {0xc3838e44, 0xffb0, 0x4e99, {0xa2, 0x1c, 0xd5, 0xd5, 0xd0, 0xe7, 0x9b, 0xfa}}
NS_DEFINE_CID(kSashNetCID, SASHNET_CID);

#define SASHNET_CONTRACT_ID "@gnome.org/SashXB/SashNet;1"

class SashNet : public sashINet, public nsIStreamListener, public nsIHttpHeaderVisitor,
				public nsIInterfaceRequestor, public nsIProgressEventSink
{
protected:
	 FileSystem fs;

	 HeaderMap m_RequestHeaders;
	 HeaderMap m_ResponseHeaders;

	 void SetHeader(HeaderMap & set, const char * header, const char * value);
	 string GetHeader(const HeaderMap & set, const char * header);
	 nsresult GetHeaderByIndex(const HeaderMap & set, PRUint32 i, char ** header, char ** value);

	 nsCOMPtr<nsIHttpChannel> m_httpChan;
	 nsCOMPtr<sashINetCallback> m_cb;
	 nsCOMPtr<nsIEventQueue> m_eventq;
	 vector<char> m_data;

	 string m_url, m_requestType, m_filename;
	 bool m_keepData;
	 int m_file;
	 
	 string statusString(int status);
	 int m_status, m_HTTPStatus;

	 int m_startTime;
	 int m_timeOut;

	 void Setup();
	 virtual void StartFileTransfer(const string & source, const string & dest);
	 virtual void StartHTTPTransfer();

	 void SetFinished();
	 string GetStatusMsg(nsresult status, const PRUnichar *statusText);
	 bool IsSuccessful(int httpStatus);

public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHINET
	 NS_DECL_NSIREQUESTOBSERVER
	 NS_DECL_NSISTREAMLISTENER
	 NS_DECL_NSIHTTPHEADERVISITOR
	 NS_DECL_NSIINTERFACEREQUESTOR
	 NS_DECL_NSIPROGRESSEVENTSINK

	 SashNet();
	 virtual ~SashNet();
};


#define SASHDOWNLOADER_CID {0xaef28d45, 0xe9af, 0x44c3, {0xae, 0xcd, 0x7d, 0x2c, 0x50, 0x78, 0x9e, 0x9a}}
NS_DEFINE_CID(kSashDownloaderCID, SASHDOWNLOADER_CID);

class SashDownloader : public SashNet, public sashIDownloader {
protected:
	 void StartHTTPTransfer();

public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHIDOWNLOADER

	 NS_FORWARD_SASHINET(SashNet::);

	 virtual ~SashDownloader();
};

#define SASHUPLOADER_CID {0xf7a732bb, 0x1490, 0x49f6, {0x8a, 0xd9, 0xaa, 0x77, 0xbc, 0x0c, 0x2a, 0x01}}
NS_DEFINE_CID(kSashUploaderCID, SASHUPLOADER_CID);

class SashUploader : public SashNet, public sashIUploader {
protected:
	 void StartFileTransfer(const string & source, const string & dest) {
		  SashNet::StartFileTransfer(dest, source);
	 }

	 void StartHTTPTransfer();

	 nsCOMPtr<nsIInputStream> m_inputStream;
public:
	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHIUPLOADER

	 NS_FORWARD_SASHINET(SashNet::);

	 SashUploader();
	 virtual ~SashUploader() {}
};

#endif
