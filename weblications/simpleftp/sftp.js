function onfilesendstart(conn, filename){
	print("starting to send " + filename);
}

function onfilereceivestart(conn, filename){
	print("starting to receive " + filename);
}

function onfilesent(conn, filename){
	print("sent " + filename);
}

function onfilereceived(conn, filename){
	print("received " + filename);
}

function onprogress(conn, filename, amt){
	print("received " + amt + "K of " + filename);
}

print ("trying to create new connection");
var ftp1 = new Sash.FTP.Connection();
print(ftp1);

print ("new connection created. trying to connect...");

ftp1.Connect("127.0.0.1", 21);
//ftp1.Connect("nereid.gps.caltech.edu", 21);
ftp1.OnFileSendStart = "onfilesendstart";
ftp1.OnFileReceiveStart = "onfilesendstart";

ftp1.OnFileSent = "onfilesent";
ftp1.OnFileReceived = "onfilereceived";
ftp1.OnProgress = "onprogress";
ftp1.NotifyInterval_K = 5;

if (ftp1.Connected){
	print ("connected");

	ftp1.Login("grant", "grantwang");
//	ftp1.Login("tok", "Gany.mede");

	print ("login message:");
	print (ftp1.LoginMessage);

	print("current directory: " + ftp1.currentDirectory);


/*
	ftp1.cd("grant");
	print("current directory: " + ftp1.currentDirectory);
	print ("cd'ing up, removing directory grant");
	ftp1.cd("..");
	ftp1.rmdir("grant");
*/

	var dir = ftp1.detailedlist();
	print ("current directory contents:");
	for (p in dir){
		 print (dir[p]);
	}	

	var dir2 = ftp1.list("g");
	print ("g directory contents:");
	for (p in dir2){
		 print (dir2[p]);
	}	

//	ftp1.get("tsb");
//	ftp1.put("numbers.txt");

//	print ("done listing\n");
	ftp1.Logout();
	Sash.SimpleLocation.Quit();

}else{
	print ("unconnected");
}
