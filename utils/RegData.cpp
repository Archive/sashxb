#include "RegData.h"
#include "RegTreeItem.h"
#include "RegValueDialog.h"
#include "debugmsg.h"

bool RegListDoubleClick = false;
int RegListSelectedRow = -1;

string GetCurrentKey() {
  if (!selectedItem || TreeItemIsRoot(selectedItem))
	return "";
  else
	return BuildPath(GetTreeItemPath(selectedItem), GetTreeItemName(selectedItem));
}

string GetCurrentValue() {
  if (RegListSelectedRow >= 0) {
	char * buf;
	gtk_clist_get_text(GTK_CLIST(widgets.RegList), RegListSelectedRow, 0, &buf);
	return buf;
  }
  return "";
}

gboolean on_RegList_clicked(GtkWidget *widget,
							GdkEventButton *event,
							gpointer user_data) 
{
  RegListDoubleClick = (event->type == GDK_2BUTTON_PRESS);

  return false;
}

void NewValue() {
  RegistryValue v;
  v.SetValueType(RVT_STRING);
  string name = "*new value*";
  if (EditValueDialog(v, name)) {
	registry->SetValue(GetCurrentKey(), name, &v);
	SetModified(true);
	update_key_view();
  }
}

void on_NewValueButton_clicked(GtkWidget * widget, void * data) {
  NewValue();
}

void EditValue() {
  if (RegListSelectedRow >= 0) { 
	string key = GetCurrentKey();
	string name = GetCurrentValue();
	string originalName = name;
	RegistryValue v;
	registry->QueryValue(key, name, &v);
	if (EditValueDialog(v, name)) {
	  if (name != originalName) {
		DEBUGMSG(regedit, "name changed to %s\n", name.c_str());
		registry->DeleteValue(key, originalName);
	  }
	  DEBUGMSG(regedit, "EditValue: Setting registry key (%s, %s)\n", key.c_str(), name.c_str());
	  registry->SetValue(key, name, &v);
	  DEBUGMSG(regedit, "EditValue: Updating key view\n");
	  update_key_view();
	  DEBUGMSG(regedit, "Setting modified flag\n");
	  SetModified(true);
	}
  }
  else
	printf("no value selected!\n");
}

void on_EditValueButton_clicked(GtkWidget * widget, void * data) {
  EditValue();
}

void on_RegList_select_row(GtkCList *clist,
						   gint row,
						   gint column,
						   GdkEventButton *event,
						   gpointer user_data)
{
  RegListSelectedRow = row;
  gtk_widget_set_sensitive(widgets.EditValueButton, true);
  gtk_widget_set_sensitive(widgets.EditValueMenu, true);
  if (RegListDoubleClick) {
	EditValue();
	DEBUGMSG(regedit, "select row returning\n");
  }

}

void on_RegList_unselect_row(GtkCList *clist,
						   gint row,
						   gint column,
						   GdkEventButton *event,
						   gpointer user_data)
{
  RegListSelectedRow = -1;
  gtk_widget_set_sensitive(widgets.EditValueButton, false);
  gtk_widget_set_sensitive(widgets.EditValueMenu, false);
}

void DeleteValue() {
  if (selectedItem && RegListSelectedRow >= 0) {
	string key = GetCurrentKey();
	string value = GetCurrentValue();
	registry->DeleteValue(key, value);
	SetModified(true);

	update_key_view();
  }
}

gboolean on_RegList_key_press(GtkWidget *widget,
							  GdkEventKey *event,
							  gpointer user_data) 
{
  if (event->keyval == GDK_Delete) {
	DeleteValue();
  }
  return false;
}

string RegistryValueDataString(RegistryValue & v) {
  char buf[128];
  string s;
  unsigned int i;

  switch (v.GetValueType()) {
  case RVT_STRING:
	return v.m_String;
  case RVT_NUMBER:
	snprintf(buf, sizeof(buf), "%f", v.m_Number);
	return buf;
  case RVT_ARRAY:
	s = "[";
	for (i = 0; i < v.m_Array.size(); i++)
	  s += v.m_Array[i] + ((i < v.m_Array.size() - 1) ? ", " : "");
	s += "]";

	return s;
  case RVT_BINARY:
	snprintf(buf, sizeof(buf), "[Binary Data -- %d bytes]", v.m_BinaryLength);
	return buf;
  default:
	return "invalid";
  }
}

string RegistryValueTypeString(RegistryValue & v) {
  switch (v.GetValueType()) {
  case RVT_STRING:
	return "String";
  case RVT_NUMBER:
	return "Number";
  case RVT_ARRAY:
	return "Array";
  case RVT_BINARY:
	return "Binary";
  default:
	return "Invalid";
  }
}

void add_row(RegistryValue & v, const string & name) {
  string type = RegistryValueTypeString(v);
  string data = RegistryValueDataString(v);
  char * row[] = {(char *) name.c_str(), (char *) type.c_str(), (char *) data.c_str(), NULL};

  gtk_clist_append(GTK_CLIST(widgets.RegList), row);
}

void update_key_view() {
  gtk_clist_freeze(GTK_CLIST(widgets.RegList));
  gtk_clist_clear(GTK_CLIST(widgets.RegList));

  if (selectedItem) {
	string key = GetCurrentKey();

	DEBUGMSG(regedit, "current key is %s\n", key.c_str());
	vector<string> values = registry->EnumValues(key);
	DEBUGMSG(regedit, "current key has %d values\n", values.size());
	for (unsigned int i = 0; i < values.size(); i++) {
	  RegistryValue v;
	  registry->QueryValue(key, values[i], &v);
	  add_row(v, values[i]);
	}
  }

  gtk_clist_thaw(GTK_CLIST(widgets.RegList));
}
