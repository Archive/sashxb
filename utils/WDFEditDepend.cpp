#include "WDFEdit.h"
#include "SashGUID.h"
#include "sash_version.h"

// -------------------- WDF editor Dependency tab -----------------
static int cur_dep_row = -1;
vector<WDFDependency> deps;

void select_dep_row(int row) {
  cur_dep_row = row;
  if (cur_dep_row == -1) {
	gtk_widget_set_sensitive(widgets.DepTable, false);
	gtk_widget_set_sensitive(widgets.RemoveDep, false);
	gtk_entry_set_text(GTK_ENTRY(widgets.DepGUID), "");
	gtk_entry_set_text(GTK_ENTRY(widgets.DepFile), "");
  }
  else {
	// Dependency GUID
	gtk_entry_set_text(GTK_ENTRY(widgets.DepGUID), deps[cur_dep_row].id.c_str());

	// Dependency file
	gtk_entry_set_text(GTK_ENTRY(widgets.DepFile), deps[cur_dep_row].filename.c_str());

	// Dependency title
	gtk_entry_set_text(GTK_ENTRY(widgets.DepTitle), deps[cur_dep_row].title.c_str());

	// version info
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.DepVersionMajorEdit), deps[cur_dep_row].required_version.major);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.DepVersionMajor1Edit), deps[cur_dep_row].required_version.major1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.DepVersionMinorEdit), deps[cur_dep_row].required_version.minor);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.DepVersionReleaseEdit), deps[cur_dep_row].required_version.maintenance);

	// Activate the edit pane
	gtk_widget_set_sensitive(widgets.DepTable, true);
	gtk_widget_set_sensitive(widgets.RemoveDep, true);
  }
}

void update_dependencies() {
  char * entry[5];

  gtk_clist_clear(GTK_CLIST(widgets.DepList));
  
  for (unsigned int i = 0; i < deps.size(); i++) {
	   string name = deps[i].title;
	string guid = deps[i].id;
	string extType = get_ext_type(deps[i].id);
	string filename = deps[i].filename;

	entry[0] = (char *) name.c_str();
	entry[1] = (char *) guid.c_str();
	entry[2] = (char *) extType.c_str();
	entry[3] = (char *) filename.c_str();
	entry[4] = NULL;

	gtk_clist_append(GTK_CLIST(widgets.DepList), entry);
  }

  select_dep_row(-1);
}

void on_DepList_select_row(GtkCList *clist,
						   gint row,
						   gint column,
						   GdkEventButton *event,
						   gpointer user_data) 
{
  select_dep_row(row);
}


void on_DepList_unselect_row(GtkCList *clist,
							 gint row,
							 gint column,
							 GdkEventButton *event,
							 gpointer user_data) 
{
  if (cur_dep_row == row)
	select_dep_row(-1);
}

void on_DepGUID_changed(GtkWidget * widget, void * data) {
  if (cur_dep_row < 0) return;

  deps[cur_dep_row].id = gtk_entry_get_text(GTK_ENTRY(widgets.DepGUID));
  gtk_clist_set_text(GTK_CLIST(widgets.DepList), cur_dep_row, 0, 
					 get_ext_name(deps[cur_dep_row].id).c_str());
  gtk_clist_set_text(GTK_CLIST(widgets.DepList), cur_dep_row, 1, deps[cur_dep_row].id.c_str());
  gtk_clist_set_text(GTK_CLIST(widgets.DepList), cur_dep_row, 2, 
					 get_ext_type(deps[cur_dep_row].id).c_str());
}

void on_DepFile_changed(GtkWidget * widget, void * data) {
  if (cur_dep_row < 0) return;

  deps[cur_dep_row].filename = gtk_entry_get_text(GTK_ENTRY(widgets.DepFile));
  gtk_clist_set_text(GTK_CLIST(widgets.DepList), cur_dep_row, 3, 
					 deps[cur_dep_row].filename.c_str());
}

void on_DepTitle_changed(GtkWidget * widget, void * data) {
  if (cur_dep_row < 0) return;


  deps[cur_dep_row].title = gtk_entry_get_text(GTK_ENTRY(widgets.DepTitle));
  gtk_clist_set_text(GTK_CLIST(widgets.DepList), cur_dep_row, 0, 
					 deps[cur_dep_row].title.c_str());
}

void on_NewDep_clicked(GtkWidget * widget, void * data) {
  int new_index;

  WDFDependency newDep;

  newDep.id = GetNewGUID();
  newDep.title = get_ext_name(newDep.id);
  new_index = deps.size();
  deps.push_back(newDep);

  update_dependencies();
  gtk_clist_select_row(GTK_CLIST(widgets.DepList), new_index, 0);
}

void on_RemoveDep_clicked(GtkWidget * widget, void * data) {
  vector<WDFDependency>::iterator a = deps.begin(), b = deps.end();
  int index = 0;
  while (a != b) {
	if (index == cur_dep_row)
	  deps.erase(a);
	++a;
	index++;
  }

  update_dependencies();
}

void on_ChooseDepButton_clicked(GtkWidget * widget, void * data) {
  string ext;
  bool is_web = (wdf->GetExtensionType() == SET_WEBLICATION);
  if (choose_ext(is_web, true, is_web, ext)) {
	gtk_entry_set_text(GTK_ENTRY(widgets.DepGUID), ext.c_str());
	gtk_entry_set_text(GTK_ENTRY(widgets.DepTitle), get_ext_name(ext).c_str());
	Version w = get_ext_version(ext);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.DepVersionMajorEdit), w.major);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.DepVersionMajor1Edit), w.major1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.DepVersionMinorEdit), w.minor);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.DepVersionReleaseEdit), w.maintenance);
  }
}

void on_DepVersionMajorEdit_changed(GtkWidget * widget, void * data) {
  if (cur_dep_row < 0) return;
  deps[cur_dep_row].required_version.major = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widgets.DepVersionMajorEdit));
}

void on_DepVersionMajor1Edit_changed(GtkWidget * widget, void * data) {
  if (cur_dep_row < 0) return;
  deps[cur_dep_row].required_version.major = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widgets.DepVersionMajor1Edit));
}

void on_DepVersionMinorEdit_changed(GtkWidget * widget, void * data) {
  if (cur_dep_row < 0) return;
  deps[cur_dep_row].required_version.minor = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widgets.DepVersionMinorEdit));
}

void on_DepVersionReleaseEdit_changed(GtkWidget * widget, void * data) {
  if (cur_dep_row < 0) return;
  deps[cur_dep_row].required_version.maintenance = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widgets.DepVersionReleaseEdit));
}

