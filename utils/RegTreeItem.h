#ifndef REG_TREE_ITEM_H
#define REG_TREE_ITEM_H

#include <string>
#include <gtk/gtk.h>

// GetTreeItemPath, GetTreeItemName: returns the path/name associated with a tree item
string GetTreeItemPath(GtkTreeItem * treeItem);
string GetTreeItemName(GtkTreeItem * treeItem);
GtkLabel * GetTreeItemLabel(GtkTreeItem * treeItem);
GtkTree * GetTreeItemTree(GtkTreeItem * treeItem);

// SetTreeItemPath, SetTreeItemName: sets the path/name associated with a tree item
void SetTreeItemPath(GtkTreeItem * treeItem, const string & path);
void SetTreeItemName(GtkTreeItem * treeItem, const string & name);
void SetTreeItemTree(GtkTreeItem * treeItem, GtkTree * tree);
void SetTreeItemLabel(GtkTreeItem * treeItem, GtkLabel * label);

// TreeItemIsRoot: returns true iff this is the root tree item
bool TreeItemIsRoot(GtkTreeItem * treeItem);
void TreeItemSetRoot(GtkTreeItem * treeItem, bool isRoot);

// BuildPath: returns the full path of a key given the initial path and the key name
string BuildPath(const string & path, const string & key);

// low level functions to get/set a tree item's data
void SetItemString(GtkTreeItem * treeItem, const string & which, const string & val);
string GetItemString(GtkTreeItem * treeItem, const string & which);

#endif
