#include "WDFEdit.h"
#include "SashSecurityDialog.h"
// ------------------------ General Tab -------------------------------------
void set_actions_page(bool visible) {
  if (visible && wdf->GetExtensionType() != SET_WEBLICATION) {
	gtk_widget_set_sensitive(widgets.ActionView, true);
  }
  else if (!visible && wdf->GetExtensionType() == SET_WEBLICATION) {
	gtk_widget_set_sensitive(widgets.ActionView, false);
  }
}

void update_general() {
  set_actions_page(wdf->GetExtensionType() == SET_WEBLICATION);

  gtk_entry_set_text(GTK_ENTRY(widgets.TitleEntry), wdf->GetTitle().c_str());
  gtk_entry_set_text(GTK_ENTRY(widgets.AbstractEntry), wdf->GetAbstract().c_str());
  gtk_entry_set_text(GTK_ENTRY(widgets.AuthorEntry), wdf->GetAuthor().c_str());
  gtk_entry_set_text(GTK_ENTRY(widgets.GUIDEntry), wdf->GetID().c_str());

  WDFVersion v = wdf->GetVersion();
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.VersionMajorEdit), v.weblication.major);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.VersionMajor1Edit), v.weblication.major1);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.VersionMinorEdit), v.weblication.minor);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.VersionReleaseEdit), v.weblication.maintenance);

  gtk_entry_set_text(GTK_ENTRY(widgets.MinSashEntry), v.sash.minimum.ToString().c_str());
  gtk_entry_set_text(GTK_ENTRY(widgets.RecSashEntry), v.sash.recommended.ToString().c_str());
  gtk_entry_set_text(GTK_ENTRY(widgets.MinMozillaEntry), v.mozilla.minimum.ToString().c_str());
  gtk_entry_set_text(GTK_ENTRY(widgets.RecMozillaEntry), v.mozilla.recommended.ToString().c_str());

  GtkWidget * WDFTypeToggle;
  switch (wdf->GetExtensionType()) {
  case SET_EXTENSION:
	WDFTypeToggle = widgets.TypeExtension;
	break;
  case SET_LOCATION:
	WDFTypeToggle = widgets.TypeLocation;
	break;
  case SET_WEBLICATION:
  default:
	WDFTypeToggle = widgets.TypeWeblication;
	break;
  }

  if (WDFTypeToggle)
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(WDFTypeToggle), true);
}

void on_TitleEntry_changed(GtkWidget * widget, void * data) {
  wdf->SetTitle(gtk_entry_get_text(GTK_ENTRY(widgets.TitleEntry)));
}

void on_AbstractEntry_changed(GtkWidget * widget, void * data) {
  wdf->SetAbstract(gtk_entry_get_text(GTK_ENTRY(widgets.AbstractEntry)));
}

void on_AuthorEntry_changed(GtkWidget * widget, void * data) {
  wdf->SetAuthor(gtk_entry_get_text(GTK_ENTRY(widgets.AuthorEntry)));
}

void on_GUIDEntry_changed(GtkWidget * widget, void * data) {
  wdf->SetID(gtk_entry_get_text(GTK_ENTRY(widgets.GUIDEntry)));
}

void on_GUIDGenerate_clicked(GtkWidget * widget, void * data) {
  generate_guid_text(GTK_ENTRY(widgets.GUIDEntry));
}

void on_TypeWeblication_toggled(GtkWidget * widget, void * data) {
  set_actions_page(true);
  wdf->SetExtensionType(SET_WEBLICATION);
}

void on_TypeExtension_toggled(GtkWidget * widget, void * data) {
  set_actions_page(false);
  wdf->SetExtensionType(SET_EXTENSION);
}

void on_TypeLocation_toggled(GtkWidget * widget, void * data) {
  set_actions_page(false);
  wdf->SetExtensionType(SET_LOCATION);
}

void on_VersionMajorEdit_changed(GtkWidget * widget, void * data) {
  WDFVersion v = wdf->GetVersion();
  v.weblication.major = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widgets.VersionMajorEdit));
  wdf->SetVersion(v);
}

void on_VersionMajor1Edit_changed(GtkWidget * widget, void * data) {
  WDFVersion v = wdf->GetVersion();
  v.weblication.major1 = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widgets.VersionMajor1Edit));
  wdf->SetVersion(v);
}

void on_VersionMinorEdit_changed(GtkWidget * widget, void * data) {
  WDFVersion v = wdf->GetVersion();
  v.weblication.minor = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widgets.VersionMinorEdit));
  wdf->SetVersion(v);
}

void on_VersionReleaseEdit_changed(GtkWidget * widget, void * data) {
  WDFVersion v = wdf->GetVersion();
  v.weblication.maintenance = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widgets.VersionReleaseEdit));
  wdf->SetVersion(v);
}

void on_MinSashEntry_changed(GtkWidget * widget, void * data) {
  WDFVersion v = wdf->GetVersion();
  v.sash.minimum = gtk_entry_get_text(GTK_ENTRY(widgets.MinSashEntry));
  wdf->SetVersion(v);
}

void on_RecSashEntry_changed(GtkWidget * widget, void * data) {
  WDFVersion v = wdf->GetVersion();
  v.sash.recommended = gtk_entry_get_text(GTK_ENTRY(widgets.RecSashEntry));
  wdf->SetVersion(v);
}

void on_MinMozillaEntry_changed(GtkWidget * widget, void * data) {
  WDFVersion v = wdf->GetVersion();
  v.mozilla.minimum = gtk_entry_get_text(GTK_ENTRY(widgets.MinMozillaEntry));
  wdf->SetVersion(v);
}

void on_RecMozillaEntry_changed(GtkWidget * widget, void * data) {
  WDFVersion v = wdf->GetVersion();
  v.mozilla.recommended = gtk_entry_get_text(GTK_ENTRY(widgets.RecMozillaEntry));
  wdf->SetVersion(v);
}

void on_SecurityButton_clicked(GtkWidget * widget, void * data) {
	 if (wdf->GetExtensionType() == SET_WEBLICATION){
		  SashSecurityDialog dialog;

		  // Load in global security settings.
		  DEBUGMSG(wdfedit, "Reading in global and dependency security settings\n");
		  // if the component to be installed is a weblication, 
		  // get rid of extraneous security settings
		  vector<string> a;

		  // Load in extension-specific security items.
		  vector<WDFDependency> b = wdf->GetDependencies();
		  vector<WDFDependency>::iterator ai = b.begin(), bi = b.end();
		  while (ai != bi) {
			   a.push_back(ai->id);
			   ++ai;
		  }
		  gInstMgr.GetAllGivenDependencies(a);

		  string secLocation = 
			   InstallationManager::StaticGetSashInstallDirectory() 
			   + "/" + GlobalSecurityFileName;

		  security = SecurityHash(secLocation, a);

		  wdf->GetSecurityItems(security);

		  if (dialog.Run(security, GTK_WINDOW(widgets.WDFApp))){
			   SetModified(true);
			   wdf->SetSecurityItems(security);
		  }

	 } else {
		  gtk_widget_show(widgets.ExtSecDialog);
		  InitSettingList();
	 }
}

void on_ImportRegistryButton_clicked(GtkWidget * widget, void * data) {
	 gtk_widget_show(widgets.RegistryFileSelect);
}

void on_ImportRegistry_Cancel(GtkWidget * widget, void * data) {
	 gtk_widget_hide(widgets.RegistryFileSelect);
}

void on_ImportRegistry_OK(GtkWidget * widget, void * data) {
	 gtk_widget_hide(widgets.RegistryFileSelect);
	 string fname = gtk_file_selection_get_filename(GTK_FILE_SELECTION(widgets.RegistryFileSelect));
	 Registry reg;
	 if (reg.OpenFile(fname)) {
		  wdf->SetRegistry(&reg, "");
		  SetModified(true);
	 }
	 else {
		  OutputMessage(("Unable to load file " + fname).c_str());
	 }
}
