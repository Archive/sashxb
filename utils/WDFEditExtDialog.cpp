#include "WDFEdit.h"

string extChoice = "";

void on_ExtList_select_row(GtkCList *clist,
						   gint row,
						   gint column,
						   GdkEventButton *event,
						   gpointer user_data) 
{
  char * text;
  gtk_clist_get_text(clist, row, 3, &text);
  extChoice = text;
}


void on_ExtList_unselect_row(GtkCList *clist,
						   gint row,
						   gint column,
						   GdkEventButton *event,
						   gpointer user_data) 
{

}

void add_ext_to_clist(vector<SashExtensionItem> exts, GtkCList * list, const char*  extType) {
  char * entry[5];

  for (unsigned int i = 0; i < exts.size(); i++) {
	entry[0] = (char *) g_strdup(exts[i].name.c_str());
	entry[1] = (char *) g_strdup(exts[i].version.c_str());
	entry[2] = (char *) g_strdup(extType);
	entry[3] = (char *) g_strdup(exts[i].guid.c_str());
	entry[4] = NULL;

	gtk_clist_append(list, entry);
  }
}

bool sort_exts(const SashExtensionItem& a, const SashExtensionItem& b) {
	 return (strcasecmp(a.name.c_str(), b.name.c_str()) < 0);
}

bool choose_ext(bool showWeb, bool showExt, bool showLoc, string & choice) {
  vector<SashExtensionItem> exts;
  gtk_clist_clear(GTK_CLIST(widgets.ExtList));

  if (showWeb) {
	exts = gInstMgr.GetInfoAllInstalled(SET_WEBLICATION);
	sort(exts.begin(), exts.end(), ptr_fun(sort_exts));
	add_ext_to_clist(exts, GTK_CLIST(widgets.ExtList), "Weblication");  
  }
  if (showExt) {
	exts = gInstMgr.GetInfoAllInstalled(SET_EXTENSION);
	sort(exts.begin(), exts.end(), ptr_fun(sort_exts));
	add_ext_to_clist(exts, GTK_CLIST(widgets.ExtList), "Extension");
  }
  if (showLoc) {
	exts = gInstMgr.GetInfoAllInstalled(SET_LOCATION);
	sort(exts.begin(), exts.end(), ptr_fun(sort_exts));
	add_ext_to_clist(exts, GTK_CLIST(widgets.ExtList), "Location");
  }

  int button = gnome_dialog_run_and_close(GNOME_DIALOG(widgets.ExtDialog));
  if (button == 0) {
	choice = extChoice;
	return true;
  } 

  return false;
}
