#ifndef REG_VALUE_DIALOG_H
#define REG_VALUE_DIALOG_H

#include "regedit.h"

bool EditValueDialog(RegistryValue & v, string & name);

extern "C" {
  void on_BinaryFileButton_clicked(GtkWidget * widget, void * data);
  void on_ValueType_toggled(GtkWidget * widget, void * data);
  void on_AddArrayValueButton_clicked(GtkWidget * widget, void * data);
  void on_RemoveArrayValueButton_clicked(GtkWidget * widget, void * data);
  void on_ArrayValueList_select_row(GtkCList *clist,
									gint row,
									gint column,
									GdkEventButton *event,
									gpointer user_data);
  void on_ArrayValueList_unselect_row(GtkCList *clist,
									  gint row,
									  gint column,
									  GdkEventButton *event,
									  gpointer user_data);
  void OnFileSelectOK(GtkWidget * widget, void * data);
  void OnFileSelectCancel(GtkWidget * widget, void * data);
}


#endif
