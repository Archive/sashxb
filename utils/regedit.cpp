#include "regedit.h"
#include "RegTree.h"
#include "RegTreeItem.h"
#include "InstallationManager.h"
#include "debugmsg.h"

Registry * registry = NULL;
string RegFileName = "";
GladeXML * gUI = NULL;
GtkTreeItem * selectedItem = NULL, *rootItem = NULL;
RegistryWidgets widgets;
bool gModified = false;

void DoOpen(const string & filename);
void DoNew();
void DoSave();

enum PendingOpType {
  OP_NONE,
  OP_QUIT,
  OP_OPEN,
  OP_NEW
};

struct PendingOp {
  PendingOpType op;
  string data;

  void Process() {
	switch (op) {
	case OP_QUIT:
	  gtk_main_quit();
	  break;
	case OP_OPEN:
	  DoOpen(data);
	  break;
	case OP_NEW:
	  DoNew();
	default:
	  break;
	}
	op = OP_NONE;
  }
};

PendingOp pend = {OP_NONE, ""};

void SetModified(bool modified) {
  gModified = modified;
  gtk_widget_set_sensitive(widgets.SaveButton, modified);
  gtk_widget_set_sensitive(widgets.SaveMenu, modified);
}

bool ModifyStringDialog(const string & title, const string & prompt, string & value) {
  gtk_window_set_title(GTK_WINDOW(widgets.StringEditDialog), title.c_str());
  gtk_label_set_text(GTK_LABEL(widgets.StringEditPrompt), prompt.c_str());
  gtk_entry_set_text(GTK_ENTRY(widgets.StringEditEntry), value.c_str());
  if (gnome_dialog_run_and_close(GNOME_DIALOG(widgets.StringEditDialog)) == 0) {
	value = GetEntryText(GTK_ENTRY(widgets.StringEditEntry));
	return true;
  }
  return false;
}

string GetEntryText(GtkEntry * entry) {
  char * text = gtk_editable_get_chars(GTK_EDITABLE(entry), 0, -1);
  string s = text;
  g_free(text);

  return s;
}

bool RegistryKeyExists(const string & path, const string & key) {
  vector<string> keys = registry->EnumKeys(path);
  for (unsigned int i = 0; i < keys.size(); i++) {
	if (key == keys[i])
	  return true;
  }
  return false;
}

void DoSave() {
  registry->WriteToDisk(RegFileName);
  string status = "File " + RegFileName + " saved.";
  gnome_appbar_set_status(GNOME_APPBAR(widgets.RegAppBar), status.c_str());
  SetModified(false);
  pend.Process();
}

void DoOpen(const string & file) {
  RegFileName = file;
  if (!registry)
	registry = new Registry();
  registry->OpenFile(RegFileName);
  string status = "File " + RegFileName + " opened.";
  gnome_appbar_set_status(GNOME_APPBAR(widgets.RegAppBar), status.c_str());
  SetModified(false);
  BuildRegistryTree();
}

void DoNew() {
  RegFileName = "";
  if (registry)
	delete registry;
  registry = new Registry();

  BuildRegistryTree();
}


bool QuerySave() {
  bool choice = OutputQuery("File modified. Do you want to save?");
  if (choice)
	FileSave(NULL, NULL);

  return choice;
}

void FileOpen(GtkWidget * widget, void * data) {
  gtk_widget_show(widgets.OpenDialog);
}

void OnOpenDialogOK(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.OpenDialog);
  string fname = gtk_file_selection_get_filename(GTK_FILE_SELECTION(widgets.OpenDialog)); 
  if (gModified) {
	if (QuerySave() && RegFileName == "") {
	  pend.op = OP_OPEN;
	  pend.data = fname;
	  return;
	}
  }
  DoOpen(fname);
}

void OnOpenDialogCancel(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.OpenDialog);
}


void FileNew(GtkWidget * widget, void * data) {
  printf("modified = %d\n", gModified);
  if (gModified) {
	if (QuerySave() && RegFileName == "") {
	  pend.op = OP_NEW;
	  return;
	}
  }
  DoNew();
}

void FileSave(GtkWidget * widget, void * data) {
  if (RegFileName == "")
	FileSaveAs(widget, data);
  else {
	DoSave();
  }
}

void FileSaveAs(GtkWidget * widget, void * data) {
  gtk_widget_show(widgets.SaveDialog);
}

void OnSaveDialogOK(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.SaveDialog);
  RegFileName = gtk_file_selection_get_filename(GTK_FILE_SELECTION(widgets.SaveDialog));
  gtk_label_set_text(GetTreeItemLabel(rootItem), RegFileName.c_str());
  FileSave(widget, data);
}

void OnSaveDialogCancel(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.SaveDialog);
}

int on_delete_hide(GtkWidget * widget, GdkEvent * event, void * data) {
  gtk_widget_hide(widget);
  return 1;
}

int on_RegApp_delete(GtkWidget * widget, GdkEvent * event, void * data) {
  if (gModified) {
	if (QuerySave()) {
	  pend.op = OP_QUIT;
	  return 1;
	}
  }

  gtk_main_quit();
  return 0;
}


int main(int argc, char * argv[]) {
  gnome_init("sash-registry-editor", "1.666", argc, argv);
  SashErrorPushMode(SASH_ERROR_GNOME);

  glade_gnome_init();

  string glade_file = GetSashShareDirectory() + "/regedit.glade";

  gUI = glade_xml_new(glade_file.c_str(), NULL);
  if (!gUI) {
	printf("Unable to open regedit.glade! Aborting.\n");
	exit(-1);
  }

  glade_xml_signal_autoconnect(gUI);
  widgets.Initialize(gUI);

  SashErrorSetWindow(widgets.RegApp);

  registry = new Registry();

  if (argc > 1)
	DoOpen(argv[1]);
  else
	DoNew();

  gtk_main();

  return 0;
}
