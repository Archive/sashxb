#ifndef WDFEDIT_CALLBACKS_H
#define WDFEDIT_CALLBACKS_H

#include <gtk/gtk.h>

extern "C" {

  // Main program
  void update_all();
  void DoNew();
  void DoOpen(string file);
  void DoSave();
  void on_modified(GtkWidget * widget, void * data);
  void SetModified(bool modified);
  void FileNew(GtkWidget * widget, void * data);
  void FileOpen(GtkWidget * widget, void * data);
  void FileSave(GtkWidget * widget, void * data);  
  void FileSaveAs(GtkWidget * widget, void * data);
  void OnOpenDialogOK(GtkWidget * widget, void * data);
  void OnOpenDialogCancel(GtkWidget * widget, void * data);
  void OnSaveDialogOK(GtkWidget * widget, void * data);
  void OnSaveDialogCancel(GtkWidget * widget, void * data);
  int on_WDFApp_delete(GtkWidget * widget, GdkEvent * event, void * data);
  int on_delete_hide(GtkWidget * widget, GdkEvent * event, void * data);
  void on_SecurityButton_clicked(GtkWidget * widget, void * data);
  void on_WDFNotebook_switch_page(GtkNotebook *notebook,
								  GtkNotebookPage *page,
								  gint page_num,
								  gpointer user_data);

  // Extension list dialog
  bool choose_ext(bool showWeb, bool showExt, bool showLoc, string & choice);
  void on_ExtList_select_row(GtkCList *clist,
							 gint row,
							 gint column,
							 GdkEventButton *event,
							 gpointer user_data);
  void on_ExtList_unselect_row(GtkCList *clist,
							   gint row,
							   gint column,
							   GdkEventButton *event,
							   gpointer user_data);


  // General tab
  void update_general();
  void on_TitleEntry_changed(GtkWidget * widget, void * data);
  void on_AbstractEntry_changed(GtkWidget * widget, void * data);
  void on_AuthorEntry_changed(GtkWidget * widget, void * data);
  void on_GUIDEntry_changed(GtkWidget * widget, void * data);
  void on_GUIDGenerate_clicked(GtkWidget * widget, void * data);
  void on_TypeWeblication_toggled(GtkWidget * widget, void * data);
  void on_TypeExtension_toggled(GtkWidget * widget, void * data);
  void on_TypeLocation_toggled(GtkWidget * widget, void * data);
  void on_VersionMajorEdit_changed(GtkWidget * widget, void * data);
  void on_VersionMajor1Edit_changed(GtkWidget * widget, void * data);
  void on_VersionMinorEdit_changed(GtkWidget * widget, void * data);
  void on_VersionReleaseEdit_changed(GtkWidget * widget, void * data);
  void on_MinSashEntry_changed(GtkWidget * widget, void * data);
  void on_RecSashEntry_changed(GtkWidget * widget, void * data);
  void on_MinMozillaEntry_changed(GtkWidget * widget, void * data);
  void on_RecMozillaEntry_changed(GtkWidget * widget, void * data);
  void on_ImportRegistryButton_clicked(GtkWidget * widget, void * data);
  void on_ImportRegistry_Cancel(GtkWidget * widget, void * data);
  void on_ImportRegistry_OK(GtkWidget * widget, void * data);

  // Action tab
  void init_actions();
  void update_actions();
  void on_ActionName_changed(GtkWidget * widget, void * data);
  void on_ActionGUID_changed(GtkWidget * widget, void * data);
  void on_LocationGUID_changed(GtkWidget * widget, void * data);
  void on_ActionRegistration_changed(GtkWidget * widget, void * data);
  void on_NewAction_clicked(GtkWidget * widget, void * data);
  void on_DeleteAction_clicked(GtkWidget * widget, void * data);
  void on_ActionGenGUID_clicked(GtkWidget * widget, void * data);
  void on_ChooseLocGUID_clicked(GtkWidget * widget, void * data);
  void on_ActionList_select_row(GtkCList *clist,
								gint row,
								gint column,
								GdkEventButton *event,
								gpointer user_data);
  void on_ActionList_unselect_row(GtkCList *clist,
								gint row,
								gint column,
								GdkEventButton *event,
								gpointer user_data);

  // Dependency tab
  void update_dependencies();
  void on_DepList_select_row(GtkCList *clist,
							 gint row,
							 gint column,
							 GdkEventButton *event,
							 gpointer user_data);
  void on_DepList_unselect_row(GtkCList *clist,
							   gint row,
							   gint column,
							   GdkEventButton *event,
							   gpointer user_data);
  void on_DepGUID_changed(GtkWidget * widget, void * data);
  void on_DepFile_changed(GtkWidget * widget, void * data);
  void on_DepTitle_changed(GtkWidget * widget, void * data);
  void on_NewDep_clicked(GtkWidget * widget, void * data);
  void on_RemoveDep_clicked(GtkWidget * widget, void * data);
  void on_ChooseDepButton_clicked(GtkWidget * widget, void * data);
  void on_DepVersionMajorEdit_changed(GtkWidget * widget, void * data);
  void on_DepVersionMajor1Edit_changed(GtkWidget * widget, void * data);
  void on_DepVersionMinorEdit_changed(GtkWidget * widget, void * data);
  void on_DepVersionReleaseEdit_changed(GtkWidget * widget, void * data);

  // Files tab
  void update_files();
  string file_loc_string(FileLocationType flt);
  void on_FileSelect_clicked(GtkWidget * widget, void * data);
  void on_FileSelect_cancel(GtkWidget * widget, void * data);
  void on_FileSelect_OK(GtkWidget * widget, void * data);
  void on_FileLoc_toggled(GtkWidget * widget, void * data);
  void on_FilePrecache_toggled(GtkWidget * widget, void * data);
  void on_FileList_select_row(GtkCList *clist,
							  gint row,
							  gint column,
							  GdkEventButton *event,
							  gpointer user_data);
  void on_FileList_unselect_row(GtkCList *clist,
								gint row,
								gint column,
								GdkEventButton *event,
								gpointer user_data);
  void on_FileName_changed(GtkWidget * widget, void * data);
  void on_AddFile_clicked(GtkWidget * widget, void * data);
  void on_RemoveFile_clicked(GtkWidget * widget, void * data);

  // Install tab
  void update_inst_pages();
  string inst_type_string(InstallScreenType t);
  void on_InstallList_select_row(GtkCList *clist,
								 gint row,
								 gint column,
								 GdkEventButton *event,
								 gpointer user_data);
  void on_InstallList_unselect_row(GtkCList *clist,
								   gint row,
								   gint column,
								   GdkEventButton *event,
								   gpointer user_data);
  void on_InstallTitle_changed(GtkWidget * widget, void * data);
  void on_InstallPage_changed(GtkWidget * widget, void * data);
  void on_NewPage_clicked(GtkWidget * widget, void * data);
  void on_RemovePage_clicked(GtkWidget * widget, void * data);
  void on_PageLoc_toggled(GtkWidget * widget, void * data);
  void on_PageBack_toggled(GtkWidget * widget, void * data);
  void on_PageNext_toggled(GtkWidget * widget, void * data);
  void on_InstallType_toggled(GtkWidget * widget, void * data);

  // Platform tab
  void init_platforms();
  void update_platforms();
  void on_platform_toggled(GtkWidget * widget, void * data);

  // Security tab
  void init_security();
  void update_security();
  GtkWidget * on_security_create(char * s1, char * s2, int i1, int i2);

	 // Extension Security Dialog (for creating custom security settings)
	 void InitSettingList();
	 void RefreshSettingList();
	 void on_ExtSecSettingList_select_row(GtkCList *clist,
										  gint row,
										  gint column,
										  GdkEventButton *event,
										  gpointer user_data);
	 void on_ExtSecSettingList_unselect_row(GtkCList *clist,
											gint row,
											gint column,
											GdkEventButton *event,
											gpointer user_data);
	 void on_ExtSecAddSetting_clicked (GtkWidget *widget, void *data);
	 void on_ExtSecDeleteSetting_clicked (GtkWidget *widget, void *data);
	 void on_ExtSecDescription_changed (GtkWidget *widget, void *data);
	 void on_ExtSecType_changed (GtkWidget *widget, void *data);
	 void on_ExtSecID_changed (GtkWidget *widget, void *data);
	 void on_ExtSecStrEnum_changed (GtkWidget *widget, void *data);
	 void on_ExtSecOK_clicked (GtkWidget *widget, void *data);
	 void on_ExtSecCancel_clicked (GtkWidget *widget, void *data);


}

void generate_guid_text(GtkEntry * entry);
string get_ext_type(const string& guid);
string get_ext_name(const string& guid);
Version get_ext_version(const string& guid);

#endif
