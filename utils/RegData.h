#ifndef REG_DATA_H
#define REG_DATA_H

#include "regedit.h"

void update_key_view();
string RegistryValueDataString(RegistryValue & v);
string GetCurrentKey();

extern "C" {
  void on_RegList_select_row(GtkCList *clist,
							 gint row,
							 gint column,
							 GdkEventButton *event,
							 gpointer user_data);

  void on_RegList_unselect_row(GtkCList *clist,
							   gint row,
							   gint column,
							   GdkEventButton *event,
							   gpointer user_data);

  gboolean on_RegList_clicked(GtkWidget *widget,
							  GdkEventButton *event,
							  gpointer user_data);

  void on_EditValueButton_clicked(GtkWidget * widget, void * data);
  void on_NewValueButton_clicked(GtkWidget * widget, void * data);
  gboolean on_RegList_key_press(GtkWidget *widget,
								GdkEventKey *event,
								gpointer user_data);
}

#endif
