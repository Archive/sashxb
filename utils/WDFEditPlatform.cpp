#include "WDFEdit.h"

bool platform_changing = false;
vector<string> platforms;

void init_platforms() {
}

void uncheck(void * data, void * user) {
  GtkWidget * w = (GtkWidget *) data;

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w), false);
}

void update_platforms() {
  GList * children = gtk_container_children(GTK_CONTAINER(widgets.PlatformView));

  platform_changing = true;
  g_list_foreach(children, uncheck, NULL);
  platform_changing = false;

  for (unsigned int i = 0; i < platforms.size(); i++) {
	string name = "Platform" + platforms[i];
	GtkWidget * w = glade_xml_get_widget(gUI, name.c_str());
	if (w) {
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w), true);
	}
  }
}

void on_platform_toggled(GtkWidget * widget, void * data) {
  if (!data || platform_changing) return;

  vector<string> newPlatforms;
  string p = (char *) data;
  unsigned int i;

  for (i = 0; i < platforms.size(); i++) {
	if (platforms[i] != p) {
	  newPlatforms.push_back(platforms[i]);
	}
  }

  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widget)))
	newPlatforms.push_back(p);

  platforms = newPlatforms;
}
