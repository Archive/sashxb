#include "RegTree.h"
#include "regedit.h"
#include "RegTreeItem.h"
#include "RegData.h"
#include "debugmsg.h"

#include <vector>

bool RegTreeDoubleClick = false;
void BuildTree(const string & path, GtkTree * cur);

void SetSelectedItem(GtkTreeItem * item) {
  bool selected = (item != NULL);
  selectedItem = item;
  gtk_widget_set_sensitive(widgets.NewValueButton, selected);
  gtk_widget_set_sensitive(widgets.NewValueMenu, selected);

  bool can_rename = (selected && !TreeItemIsRoot(item));
  gtk_widget_set_sensitive(widgets.RenameKeyButton, can_rename);
  gtk_widget_set_sensitive(widgets.RenameKeyMenu, can_rename);

  gtk_widget_set_sensitive(widgets.EditValueButton, false);
  gtk_widget_set_sensitive(widgets.EditValueMenu, false);
}

void SetData(GtkTreeItem * item, GtkLabel * label, 
			 const string & path, const string & name, bool root, GtkTree * parent) {
  SetTreeItemPath(item, path);
  SetTreeItemName(item, name);
  SetTreeItemTree(item, parent);
  SetTreeItemLabel(item, label);
  TreeItemSetRoot(item, root);
}

void SetSignals(GtkTree * item) {
  gtk_signal_connect(GTK_OBJECT(item), "select-child", 
					 GTK_SIGNAL_FUNC(on_RegTree_select_child), NULL);
  gtk_signal_connect(GTK_OBJECT(item), "unselect-child", 
					 GTK_SIGNAL_FUNC(on_RegTree_unselect_child), NULL);
  gtk_signal_connect(GTK_OBJECT(item), "button-press-event", 
					 GTK_SIGNAL_FUNC(on_RegTree_clicked), NULL);
}

GtkTreeItem * new_tree_item(const string & path, const string & name, 
							GtkTree * parent, bool root = false) 
{
  GtkTreeItem * item = GTK_TREE_ITEM(gtk_tree_item_new());
  GtkLabel * label = GTK_LABEL(gtk_label_new(name.c_str()));
  gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
  gtk_container_add(GTK_CONTAINER(item), GTK_WIDGET(label));
  gtk_widget_show(GTK_WIDGET(label));
  gtk_widget_show(GTK_WIDGET(item));
  SetData(item, label, path, name, root, parent);
  gtk_widget_add_events(GTK_WIDGET(item), GDK_KEY_PRESS_MASK);
  gtk_signal_connect(GTK_OBJECT(item), "key-press-event",
					 GTK_SIGNAL_FUNC(on_RegTree_key_press), NULL);

  return item;
}

void on_RegTree_unselect_child(GtkTree *tree,
							 GtkWidget *widget,
							 gpointer user_data) 
{
  SetSelectedItem(NULL);
  printf("unselecting child\n");

  update_key_view();
}

void RenameKey() {

  if (selectedItem) {
	bool root = TreeItemIsRoot(selectedItem);

	if (!root) {
	  string path = GetTreeItemPath(selectedItem);
	  string name = GetTreeItemName(selectedItem);
	  GtkLabel * label = GetTreeItemLabel(selectedItem);
	  string fullPath = BuildPath(path, name);
	  
	  string prompt = "Enter a new name for the key " + name + ":";
	  if (ModifyStringDialog("Rename Key", prompt, name)) {
		gtk_label_set_text(label, name.c_str());
		string newPath = BuildPath(path, name);
		DEBUGMSG(regedit, "Renaming %s to %s\n", fullPath.c_str(), newPath.c_str());
		if (!registry->Rename(fullPath, name))
		  OutputMessage("Rename failed!\n");
		SetTreeItemName(selectedItem, name);
		SetModified(true);

		int numSubKeys = registry->EnumKeys(newPath).size();
		if (numSubKeys > 0) {
		  gtk_tree_item_remove_subtree(selectedItem);
		  GtkWidget * newSubTree = gtk_tree_new();
		  SetSignals(GTK_TREE(newSubTree));
		  gtk_tree_item_set_subtree(selectedItem, newSubTree);
		  BuildTree(newPath, GTK_TREE(newSubTree));
		}

		update_key_view();
	  }
	}
  }
  else
	printf("No key selected!\n");
}

void on_RegTree_select_child(GtkTree *tree,
							 GtkWidget *widget,
							 gpointer user_data) 
{
  SetSelectedItem(GTK_TREE_ITEM(widget));

  DEBUGMSG(regedit, "Selected key (%s, %s)\n", GetTreeItemPath(selectedItem).c_str(),
		   GetTreeItemName(selectedItem).c_str());

  update_key_view();

  // if the user double-clicked, pop up the rename key dialog
  if (RegTreeDoubleClick)
	RenameKey();
}

gboolean on_RegTree_clicked(GtkWidget *widget,
							GdkEventButton *event,
							gpointer user_data) 
{
  RegTreeDoubleClick = (event->type == GDK_2BUTTON_PRESS);

  return false;
}

void on_RenameKeyButton_clicked(GtkWidget * widget, void * data) {
  RenameKey();
}

void DeleteSelected() {
  if (selectedItem) {
	if (!TreeItemIsRoot(selectedItem)) {
	  string key = BuildPath(GetTreeItemPath(selectedItem), GetTreeItemName(selectedItem));
	  registry->DeleteKey(key, true);
	  gtk_tree_remove_item(GetTreeItemTree(selectedItem), GTK_WIDGET(selectedItem));
	  SetSelectedItem(NULL);
	}
  }
  else {
	printf("No key selected!\n");
  }
}

gboolean on_RegTree_key_press(GtkWidget *widget,
							  GdkEventKey *event,
							  gpointer user_data) 
{
  if (event->keyval == GDK_Delete) {
	DeleteSelected();
  }
  return false;
}

void NewKey() {
  string subKey;
  if (ModifyStringDialog("New Key", "Enter the name of the new key:", subKey)) {
	if (subKey != "") {
	  string key = GetCurrentKey();
	  string path = BuildPath(key, subKey);
	  if (!RegistryKeyExists(key, subKey) && registry->CreateKey(BuildPath(key, subKey))) {
		GtkTreeItem * parentItem = selectedItem ? selectedItem : rootItem;
		GtkWidget * parent = GTK_TREE_ITEM_SUBTREE(parentItem);
		if (!parent) {
		  parent = gtk_tree_new();
		  SetSignals(GTK_TREE(parent));
		  gtk_tree_item_set_subtree(parentItem, parent);
		  DEBUGMSG(regedit, "creating new subtree\n");
		}
		DEBUGMSG(regedit, "new key (%s, %s)\n", key.c_str(), subKey.c_str());
		GtkTreeItem * newItem = new_tree_item(key, subKey, GTK_TREE(parent));
		gtk_tree_append(GTK_TREE(parent), GTK_WIDGET(newItem));
		SetModified(true);
	  }
	}
  }
}

void on_NewKeyButton_clicked(GtkWidget * widget, void * data) {
  NewKey();
}

void BuildTree(const string & path, GtkTree * cur) {
  vector<string> subkeys = registry->EnumKeys(path);

  for (unsigned int i = 0; i < subkeys.size(); i++) {
	GtkTreeItem * item = new_tree_item(path, subkeys[i], cur);
	gtk_tree_append(cur, GTK_WIDGET(item));

	string newPath = BuildPath(path, subkeys[i]);
	int numSubKeys = registry->EnumKeys(newPath).size();

	if (numSubKeys > 0) {
	  GtkTree * itemSubTree = GTK_TREE(gtk_tree_new());
	  gtk_tree_item_set_subtree(item, GTK_WIDGET(itemSubTree));
	  SetSignals(itemSubTree);
	  BuildTree(newPath, itemSubTree);
	}
  }
}

void BuildRegistryTree() {
  GtkTree * rootSubTree;
  string label = (RegFileName == "") ? "<untitled>" : RegFileName;

  SetSelectedItem(NULL);

  gtk_tree_clear_items(GTK_TREE(widgets.RegTree), 0, -1);
  rootItem = new_tree_item("", label, GTK_TREE(widgets.RegTree), true);
  gtk_tree_append(GTK_TREE(widgets.RegTree), GTK_WIDGET(rootItem));
  rootSubTree = GTK_TREE(gtk_tree_new());
  gtk_tree_item_set_subtree(rootItem, GTK_WIDGET(rootSubTree));
  SetSignals(rootSubTree);

  BuildTree("", rootSubTree);

  if (RegFileName != "")
	gtk_tree_item_expand(rootItem);

  update_key_view();
}
