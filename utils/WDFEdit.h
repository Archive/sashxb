#ifndef WDF_EDIT_H
#define WDF_EDIT_H

#include <gnome.h>
#include <glade/glade.h>
#include <string>

#include <string.h>
#include "wdf.h"
#include "WDFEditWidgets.h"
#include "WDFEditCallbacks.h"
#include "InstallationManager.h"
#include "debugmsg.h"

extern string gOpenFile;
extern GladeXML * gUI;
extern WDF * wdf;
extern Widgets widgets;
extern InstallationManager gInstMgr;

extern vector<WDFAction> actions;
extern vector<WDFDependency> deps;
extern vector<WDFInstallScreen> inst_pages;
extern vector<WDFFile> files;
extern vector<string> platforms;
extern SecurityHash security;
extern vector<SecurityItem> security_settings;



#endif
