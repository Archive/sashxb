#ifndef REGTREE_H
#define REGTREE_H

#include <gtk/gtk.h>
#include <string>

void BuildRegistryTree();

extern "C" {
  void on_RegTree_select_child(GtkTree *tree,
							   GtkWidget *widget,
							   gpointer user_data);
  void on_RegTree_unselect_child(GtkTree *tree,
								 GtkWidget *widget,
								 gpointer user_data);
  gboolean on_RegTree_clicked(GtkWidget *widget,
							  GdkEventButton *event,
							gpointer user_data);
  void on_RenameKeyButton_clicked(GtkWidget * widget, 
								  void * data);
  gboolean on_RegTree_key_press(GtkWidget *widget,
								GdkEventKey *event,
								gpointer user_data);
  void on_NewKeyButton_clicked(GtkWidget * widget, 
							   void * data);
}

#endif
