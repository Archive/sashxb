#include "RegTreeItem.h"
#include <stdio.h>

string BuildPath(const string & path, const string & key) {
  return (path == "") ? key : path + "\\" + key;
}

string GetItemString(GtkTreeItem * treeItem, const string & which) {
  char * val  = (char *)gtk_object_get_data(GTK_OBJECT(treeItem), which.c_str());
  if (val)
	return val;
  else
	return "";
}

void SetItemString(GtkTreeItem * treeItem, const string & which, const string & val) {
  char * curVal  = (char *)gtk_object_get_data(GTK_OBJECT(treeItem), which.c_str());
  if (curVal) {
	free(curVal);
  }
  gtk_object_set_data(GTK_OBJECT(treeItem), which.c_str(), strdup(val.c_str()));
}

// TreeItemIsRoot: returns true iff this is the root tree item
bool TreeItemIsRoot(GtkTreeItem * treeItem) {
  return (bool)gtk_object_get_data(GTK_OBJECT(treeItem), "root");
}

void TreeItemSetRoot(GtkTreeItem * treeItem, bool isRoot) {
  gtk_object_set_data(GTK_OBJECT(treeItem), "root", (void *)isRoot);
}

GtkLabel * GetTreeItemLabel(GtkTreeItem * treeItem) {
  return (GtkLabel *)gtk_object_get_data(GTK_OBJECT(treeItem), "label");
}

void SetTreeItemLabel(GtkTreeItem * treeItem, GtkLabel * label) {
  gtk_object_set_data(GTK_OBJECT(treeItem), "label", label);
}

void SetTreeItemTree(GtkTreeItem * treeItem, GtkTree * tree) {
  gtk_object_set_data(GTK_OBJECT(treeItem), "tree", tree);
}

GtkTree * GetTreeItemTree(GtkTreeItem * treeItem) {
  GtkTree * tree = (GtkTree *) gtk_object_get_data(GTK_OBJECT(treeItem), "tree");
  if (!tree)
	printf("Warning: tree item has no parent tree\n");

  return tree;
}

string GetTreeItemPath(GtkTreeItem * treeItem) {
  return GetItemString(treeItem, "path");
}

void SetTreeItemPath(GtkTreeItem * treeItem, const string & path) {
  SetItemString(treeItem, "path", path.c_str());
}

string GetTreeItemName(GtkTreeItem * treeItem) {
  return GetItemString(treeItem, "name");
}

void SetTreeItemName(GtkTreeItem * treeItem, const string & name) {
  SetItemString(treeItem, "name", name.c_str());
}

string GetTreeItemKey(GtkTreeItem * treeItem) {
  return BuildPath(GetTreeItemPath(treeItem), GetTreeItemName(treeItem));
}

