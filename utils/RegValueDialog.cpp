#include "RegValueDialog.h"
#include "FileSystem.h"
#include "debugmsg.h"

int type_index = -1;
string gBinaryFile = "";
int array_value_index = -1;

void SetupDialog(RegistryValue & v) {
  GtkToggleButton * valueType;
  char buf[128];
  char * rowData[2] = {NULL, NULL};
  unsigned int i;

  gtk_entry_set_text(GTK_ENTRY(widgets.StringValueEntry), "");
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.NumberValueEntry), 0.0);
  gtk_clist_clear(GTK_CLIST(widgets.ArrayValueList));
  gtk_label_set_text(GTK_LABEL(widgets.BinaryFileInfo), "Current Binary: None");

  type_index = (int) v.GetValueType();

  switch (v.GetValueType()) {
  case RVT_NUMBER:
	valueType = GTK_TOGGLE_BUTTON(widgets.NumberButton);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.NumberValueEntry), v.m_Number);
	break;
  case RVT_ARRAY:
	valueType = GTK_TOGGLE_BUTTON(widgets.ArrayButton);
	for (i = 0; i < v.m_Array.size(); i++) {
	  rowData[0] = (char *) v.m_Array[i].c_str();
	  gtk_clist_append(GTK_CLIST(widgets.ArrayValueList), rowData);
	}
	break;
  case RVT_BINARY:
	valueType = GTK_TOGGLE_BUTTON(widgets.BinaryButton);
	snprintf(buf, 128, "Current Binary: %d bytes", v.m_BinaryLength);
	gtk_label_set_text(GTK_LABEL(widgets.BinaryFileInfo), buf);
	break;
  case RVT_STRING:
  default:
	valueType = GTK_TOGGLE_BUTTON(widgets.StringButton);
	gtk_entry_set_text(GTK_ENTRY(widgets.StringValueEntry), v.m_String.c_str());
	break;
  }

  gtk_toggle_button_set_active(valueType, true);
}

void ExtractData(RegistryValue & v) {
  vector<string> arrayItems;
  FILE * f;
  char * buf;
  int i;

  DEBUGMSG(regedit, "type index is %d\n", type_index);

  v.SetValueType((RegistryValueType) type_index);
  switch (v.GetValueType()) {
  case RVT_STRING:
	v.m_String = GetEntryText(GTK_ENTRY(widgets.StringValueEntry));
	break;
  case RVT_NUMBER:
	v.m_Number = gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(widgets.NumberValueEntry));
	break;
  case RVT_ARRAY:
	DEBUGMSG(regedit, "Array has %d values:\n", GTK_CLIST(widgets.ArrayValueList)->rows);
    for (i = 0; i < GTK_CLIST(widgets.ArrayValueList)->rows; i++) {
	  buf = NULL;
	  gtk_clist_get_text(GTK_CLIST(widgets.ArrayValueList), i, 0, &buf);
	  DEBUGMSG(regedit, "Item %d: %s\n", i, buf);
	  arrayItems.push_back(buf);
	}
	v.m_Array = arrayItems;
	break;
  case RVT_BINARY:
	f = fopen(gBinaryFile.c_str(), "rb");
	if (f) {
	  fseek(f, 0, SEEK_END);
	  int size = ftell(f);
	  fseek(f, 0, SEEK_SET);
	  void * buf = malloc(size);
	  fread(buf, size, 1, f);
	  fclose(f);
	  v.m_BinaryLength = size;
	  v.m_Binary = buf;
	}
	else {
	  v.m_BinaryLength = 0;
	  v.m_Binary = NULL;
	}
	break;
  case RVT_INVALID:
	OutputError("Registry key is not valid\n");
	assert(false);
	break;
  }
}

void on_ValueType_toggled(GtkWidget * widget, void * data) {
  type_index = atoi((char *) data);

  gtk_notebook_set_page(GTK_NOTEBOOK(widgets.ValueNotebook), type_index);
}

void on_AddArrayValueButton_clicked(GtkWidget * widget, void * data) {
  string value;
  if (ModifyStringDialog("New array value", "Enter the new value", value)) {
	char * data[2] = {(char *) value.c_str(), NULL};
	gtk_clist_append(GTK_CLIST(widgets.ArrayValueList), data);
  }
}

void on_RemoveArrayValueButton_clicked(GtkWidget * widget, void * data) {
  gtk_clist_remove(GTK_CLIST(widgets.ArrayValueList), array_value_index);
  array_value_index = -1;
  gtk_widget_set_sensitive(widgets.RemoveArrayValueButton, false);
}

void on_BinaryFileButton_clicked(GtkWidget * widget, void * data) {
  gtk_widget_show(widgets.FileSelect);
}

void OnFileSelectOK(GtkWidget * widget, void * data) {
  char buf[128];

  gtk_widget_hide(widgets.FileSelect);
  gBinaryFile = gtk_file_selection_get_filename(GTK_FILE_SELECTION(widgets.FileSelect));
  snprintf(buf, 128, "Current Binary: %d bytes", FileSystem::FileSize(gBinaryFile));
  gtk_label_set_text(GTK_LABEL(widgets.BinaryFileInfo), buf);
}

void OnFileSelectCancel(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.FileSelect);
}

void on_ArrayValueList_select_row(GtkCList *clist,
								  gint row,
								  gint column,
								  GdkEventButton *event,
								  gpointer user_data)
{
  array_value_index = row; 
  gtk_widget_set_sensitive(widgets.RemoveArrayValueButton, true);
}

void on_ArrayValueList_unselect_row(GtkCList *clist,
									gint row,
									gint column,
									GdkEventButton *event,
									gpointer user_data)
{
  array_value_index = -1;
  gtk_widget_set_sensitive(widgets.RemoveArrayValueButton, false);
}

bool EditValueDialog(RegistryValue & v, string & name) {
  gtk_entry_set_text(GTK_ENTRY(widgets.ValueNameEntry), name.c_str());
  SetupDialog(v);

  if (gnome_dialog_run_and_close(GNOME_DIALOG(widgets.ValueDialog)) == 0) {
	ExtractData(v);
	name = GetEntryText(GTK_ENTRY(widgets.ValueNameEntry));
	DEBUGMSG(regedit, "returning from the EditValueDialog -- OK pressed\n");
	return true;
  }
  return false;
}
