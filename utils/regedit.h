#ifndef REGEDIT_H
#define REGEDIT_H

#include "Registry.h"
#include "RegistryWidgets.h"
#include <gtk/gtk.h>
#include <glade/glade.h>

extern Registry * registry;
extern string RegFileName;
extern RegistryWidgets widgets;
extern GtkTreeItem * selectedItem, *rootItem;

bool ModifyStringDialog(const string & title, const string & prompt, string & value);
void SetModified(bool modified);
bool RegistryKeyExists(const string & path, const string & key);
string GetEntryText(GtkEntry * entry);

extern "C" {
  void FileNew(GtkWidget * widget, void * data);
  void FileOpen(GtkWidget * widget, void * data);
  void FileSave(GtkWidget * widget, void * data);  
  void FileSaveAs(GtkWidget * widget, void * data);
  void OnOpenDialogOK(GtkWidget * widget, void * data);
  void OnOpenDialogCancel(GtkWidget * widget, void * data);
  void OnSaveDialogOK(GtkWidget * widget, void * data);
  void OnSaveDialogCancel(GtkWidget * widget, void * data);
  int on_RegApp_delete(GtkWidget * widget, GdkEvent * event, void * data);
  int on_delete_hide(GtkWidget * widget, GdkEvent * event, void * data);
}

#endif
