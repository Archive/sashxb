#include "WDFEdit.h"
#include "SashGUID.h"
#include "SashSecurityDialog.h"
#include "SecurityHash.h"
#include "sash_version.h"

string gOpenFile = "";
bool gModified = false;
GladeXML * gUI = NULL;
WDF * wdf = NULL;
Widgets widgets;
InstallationManager gInstMgr;
SecurityHash security;

enum PendingOpType {
  OP_NONE,
  OP_QUIT,
  OP_OPEN,
  OP_NEW
};

struct PendingOp {
  PendingOpType op;
  string data;

  void Process() {
	switch (op) {
	case OP_QUIT:
	  gtk_main_quit();
	  break;
	case OP_OPEN:
	  DoOpen(data);
	  break;
	case OP_NEW:
	  DoNew();
	default:
	  break;
	}
	op = OP_NONE;
  }
};

PendingOp pend = {OP_NONE, ""};

void SetWDFData() {
//  wdf->SetSecurityItems(security);
  wdf->SetActions(actions);
  wdf->SetDependencies(deps);
  wdf->SetInstallScreens(inst_pages);
  wdf->SetFiles(files);
  wdf->SetPlatforms(platforms);
}

void GetWDFData() {
  DEBUGMSG(wdfedit, "getting actions\n");
  actions = wdf->GetActions();
  DEBUGMSG(wdfedit, "getting deps\n");
  deps = wdf->GetDependencies();
  DEBUGMSG(wdfedit, "getting install screens\n");
  inst_pages = wdf->GetInstallScreens();
  DEBUGMSG(wdfedit, "getting files\n");
  files = wdf->GetFiles();
  DEBUGMSG(wdfedit, "getting platforms\n");
  platforms = wdf->GetPlatforms();
  DEBUGMSG(wdfedit, "getting security\n");

//  m_pSecurity = new SecurityHash(m_IM.GetSashInstallDirectory() + "/" + GlobalSecurityFileName, a);
//  vector<string> a;
//   security = SecurityHash(InstallationManager::StaticGetSashInstallDirectory() + "/" + GlobalSecurityFileName, a);
//   DEBUGMSG(ssp, "loaded from global security file (security.dat)\n");
//   security.Print();

//   DEBUGMSG(wdfedit, "before creating new SecurityHash\n");
//   security.Print();

//   DEBUGMSG(wdfedit, "creating new SecurityHash\n");
  security = SecurityHash();

  wdf->GetSecurityItems(security);
//   DEBUGMSG(ssp, "got security items from wdf file\n");
//   security.Print();

  DEBUGMSG(wdfedit, "returning\n");
}

void DoSave() {
  SetWDFData();
  DEBUGMSG(wdfedit, "saving\n");
  wdf->SaveToFile(gOpenFile);
  string status = "File " + gOpenFile + " saved.";
  gnome_appbar_set_status(GNOME_APPBAR(widgets.WDFAppBar), status.c_str());
  DEBUGMSG(wdfedit, "getting data\n");
  GetWDFData();
  DEBUGMSG(wdfedit, "updating\n");
  update_all();
  DEBUGMSG(wdfedit, "update done\n");
  SetModified(false);
  pend.Process();
}

void DoOpen(string file) {
  gOpenFile = file;
  if (!wdf)
	wdf = new WDF();
  if (! wdf->OpenAmbiguousFile(gOpenFile)){
	   wdf->SetExtensionType(SET_WEBLICATION);
  }
  string status = "File " + gOpenFile + " opened.";
  gnome_appbar_set_status(GNOME_APPBAR(widgets.WDFAppBar), status.c_str());
  GetWDFData();
  update_all();
  SetModified(false);
}

void DoNew() {
  gOpenFile = "";
  if (wdf)
	delete wdf;
  wdf = new WDF();

  // reasonable default
  wdf->SetExtensionType(SET_WEBLICATION);
  GetWDFData();
  platforms.push_back("Linux");
  WDFVersion v = {string("1.0.0.0"), {string(SASH_VERSION), string(SASH_VERSION)}, {string("0.9.9"), string("0.9.9")}};
  wdf->SetVersion(v);
  update_all();
  SetModified(false);
}

string get_ext_type(const string& guid) {
  SashExtensionItem ext = gInstMgr.GetInfo(guid);
  string ext_type;
  switch (ext.ext_type) {
  case SET_WEBLICATION:
	ext_type = "Weblication";
	break;
  case SET_EXTENSION:
	ext_type = "Extension";
	break;
  case SET_LOCATION:
	ext_type = "Location";
	break;
  default:
	ext_type = "Unknown";
	break;
  }

  return ext_type;
}

string get_ext_name(const string& guid) {
  SashExtensionItem ext = gInstMgr.GetInfo(guid);
  if (ext.ext_type == SET_UNDEFINED)
	return "<unknown>";
  else
	return ext.name;
}

Version get_ext_version(const string& guid) {
  SashExtensionItem ext = gInstMgr.GetInfo(guid);
  if (ext.ext_type == SET_UNDEFINED)
	return Version();
  else
	return Version(ext.version);
}

void SetModified(bool modified) {
  gModified = modified;
  gtk_widget_set_sensitive(widgets.SaveButton, modified);
}

bool QuerySave() {
  bool choice = OutputQuery("File modified. Do you want to save?");
  if (choice)
	FileSave(NULL, NULL);

  return choice;
}

void on_modified(GtkWidget * widget, void * data) {
  SetModified(true);
}

void generate_guid_text(GtkEntry * entry) {
  string guid = GetNewGUID();
  gtk_entry_set_text(entry, guid.c_str());

  string status = "GUID " + guid + " generated";
  gnome_appbar_set_status(GNOME_APPBAR(widgets.WDFAppBar), status.c_str());
}

void on_WDFNotebook_switch_page(GtkNotebook *notebook,
								GtkNotebookPage *page,
								gint page_num,
								gpointer user_data)
{
  gnome_appbar_set_status(GNOME_APPBAR(widgets.WDFAppBar), "");
}

void FileOpen(GtkWidget * widget, void * data) {
  gtk_widget_show(widgets.OpenDialog);
}

void OnOpenDialogOK(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.OpenDialog);
  string fname = gtk_file_selection_get_filename(GTK_FILE_SELECTION(widgets.OpenDialog)); 
  if (gModified) {
	if (QuerySave() && gOpenFile == "") {
	  pend.op = OP_OPEN;
	  pend.data = fname;
	  return;
	}
  }
  DoOpen(fname);
}

void OnOpenDialogCancel(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.OpenDialog);
}

void FileNew(GtkWidget * widget, void * data) {
  if (gModified) {
	if (QuerySave() && gOpenFile == "") {
	  pend.op = OP_NEW;
	  return;
	}
  }
  DoNew();
}

void FileSave(GtkWidget * widget, void * data) {
  if (gOpenFile == "")
	FileSaveAs(widget, data);
  else {
	DoSave();
  }
}

void FileSaveAs(GtkWidget * widget, void * data) {
  gtk_widget_show(widgets.SaveDialog);
}

void OnSaveDialogOK(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.SaveDialog);
  gOpenFile = gtk_file_selection_get_filename(GTK_FILE_SELECTION(widgets.SaveDialog));
  FileSave(widget, data);
}

void OnSaveDialogCancel(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.SaveDialog);
}

int on_delete_hide(GtkWidget * widget, GdkEvent * event, void * data) {
  gtk_widget_hide(widget);
  return 1;
}

int on_WDFApp_delete(GtkWidget * widget, GdkEvent * event, void * data) {
  if (gModified) {
	if (QuerySave()) {
//	  pend.op = OP_QUIT;
	  gtk_main_quit();
	  return 0;
	}
  }

  gtk_main_quit();
  return 0;
}

void update_all() {
  DEBUGMSG(wdfedit, "Updating general\n");
  update_general();
  DEBUGMSG(wdfedit, "Updating actions\n");
  update_actions();
  DEBUGMSG(wdfedit, "Updating dependencies\n");
  update_dependencies();
  DEBUGMSG(wdfedit, "Updating files\n");
  update_files();
  DEBUGMSG(wdfedit, "Updating install pages\n");
  update_inst_pages();
  DEBUGMSG(wdfedit, "Updating platforms\n");
  update_platforms();
  DEBUGMSG(wdfedit, "Updating security\n");
  update_security();
  DEBUGMSG(wdfedit, "Done update\n");
}

void init_all() {
  init_actions();
  init_platforms();
  init_security();
}

int main(int argc, char * argv[]) {
  gnome_init("sash-wdf-editor", "0.666", argc, argv);
  SashErrorPushMode(SASH_ERROR_GNOME);

  glade_gnome_init();

  string glade_file = GetSashShareDirectory() + "/wdf-editor.glade";
  
  gUI = glade_xml_new(glade_file.c_str(), NULL);
  if (!gUI) {
	printf("Unable to open wdf-editor.glade! Aborting.\n");
	exit(-1);
  }

  glade_xml_signal_autoconnect(gUI);
  widgets.Initialize(gUI);

  SashErrorSetWindow(widgets.WDFApp);

  init_all();

  if (argc > 1)
	DoOpen(argv[1]);
  else
	DoNew();

  gtk_main();
  
  return 0;
}
