#include "WDFEdit.h"
#include "SashSecurityDialog.h"
#include "SecurityHash.h"

static const char *SecurityTypeString[] = {
  "boolean",
  "number",
  "string",
  "string-enumeration",
  "string-array"
};

void init_security() 
{

}

void update_security() {
  //  meta_cleanup(widgets.SecurityView);

}

GtkWidget * on_security_create(char * s1, char * s2, int i1, int i2) {
  /*
  GtkWidget * security;

  string sec_file = gInstMgr.GetSashInstallDirectory() + "/security.dat";
  sec_hash = security_hash_new(sec_file);

  security = sash_security_pane_new();
  sash_security_pane_set_settings(SASH_SECURITY_PANE(security), sec_hash);
  */
  return gtk_label_new("security");
}

// Adding custom security settings.
vector<SecurityItem> security_settings;
static bool sec_selecting = false;
static int cur_sec_row = -1;
static bool str_enum_changed = false;

// Convert a security type to a string.
string SecurityTypeStr(SecurityType type){
	 string ret = (type >=0 && type < ST_NUM_VALUES) ? SecurityTypeString[type] : "";
	 return ret;
}

// Given a string, convert to a security type.
SecurityType settingType(string strtype){
	 for (int i=0; i<ST_NUM_VALUES; i++){
		  if (strtype == SecurityTypeStr((SecurityType)i)){
			   return (SecurityType)i;
		  }
	 }
	 return ST_INVALID;
}

// Convert a newline delimited char* to a vector of strings.
void cstrToStrVec (const char* cstr, vector<string> &strvec){
	 if (cstr == NULL) return;

	 int len = strlen (cstr);
	 if (len == 0) return;

	 char *nlpos = NULL;
	 char *currpos = (char *)cstr;
	 string currstr;
	 while (((currpos != nlpos) 
			 && (nlpos = strchr(currpos, '\n')) != NULL)) {
		  currstr = string(currpos, (nlpos - currpos) / sizeof(char));

		  if (currstr != "") {
			   strvec.push_back(currstr);
			   DEBUGMSG(wdfedit, "pushing back: %s\n",
						currstr.c_str());
		  }

		  if (strlen(nlpos) <= 1)
			   currpos = nlpos;
		  else {
			   currpos = nlpos + sizeof(char);
		  }
	 }
	 if (currpos != nlpos){
		  currstr = string (currpos);
		  if (currstr != ""){
			   strvec.push_back(currstr);
			   DEBUGMSG(wdfedit, "pushing back: %s\n",
						currstr.c_str());

		  }
	 }
}	 

// Store a string in the m_StringVals member of a security item.
void updateSecurityItemStrEnum(SecurityItem &item, const char* cstr){
	 if ((item.m_Type != ST_STRING_ENUM)
		 && (item.m_Type != ST_STRING_ARRAY))
		 return;
	 if (cstr == NULL) return;

	 vector<string> strvec;
	 cstrToStrVec(cstr, strvec);
	 
	 item.m_StringVals = strvec;

	 DEBUGMSG(wdfedit, "updateSecurityItemStrEnum: %d vals\n",
			  item.m_StringVals.size());

	 return;
}

// Enable or disable inputs depending on whether a row 
// is selected.
void allowSettingInput (bool allowed){
	 gtk_widget_set_sensitive(widgets.ExtSecDescription, allowed);	 
	 gtk_widget_set_sensitive(widgets.ExtSecType, allowed);	 
	 gtk_widget_set_sensitive(widgets.ExtSecID, allowed);	 

	 string setting = gtk_entry_get_text(GTK_ENTRY(widgets.ExtSecType));
	 gtk_widget_set_sensitive(widgets.ExtSecStrEnum, 
							  (allowed && 
							   ((setting == SecurityTypeStr(ST_STRING_ENUM))
								|| (setting == SecurityTypeStr(ST_STRING_ARRAY)))));
}

// Display options of a string enumeration.
void display_str_enum(){
	 gtk_text_freeze(GTK_TEXT(widgets.ExtSecStrEnum));
	 gtk_editable_delete_text(GTK_EDITABLE(widgets.ExtSecStrEnum), 
							  0, -1);

	 if ((security_settings[cur_sec_row].m_Type == ST_STRING_ENUM)
		 || (security_settings[cur_sec_row].m_Type == ST_STRING_ARRAY)){
		  // Grab the strings out of the array, append to 
		  // a new string.
		  string senum = "";

		  for (unsigned int i = 0; 
			   i < security_settings[cur_sec_row].m_StringVals.size(); 
			   i++){
			   senum += (security_settings[cur_sec_row].m_StringVals[i] + "\n");
		  }

		  DEBUGMSG(wdfedit, "constructed from SecurityItem string array: %s\n",
				   senum.c_str());
		  gtk_text_insert(GTK_TEXT(widgets.ExtSecStrEnum), 
						  NULL, NULL, NULL, 
						  senum.c_str(),
						  senum.length());
	 }
	 gtk_text_thaw(GTK_TEXT(widgets.ExtSecStrEnum));
}

// Select a row of the table.
void select_security_row(int row) {
	 sec_selecting = true;

	 if (str_enum_changed){

		  char *reg = gtk_editable_get_chars(GTK_EDITABLE(widgets.ExtSecStrEnum), 0, -1);
		  DEBUGMSG(wdfedit, "string read from box: %s\n",
				   reg);

		  updateSecurityItemStrEnum(security_settings[cur_sec_row], reg);
		  g_free(reg);
		  str_enum_changed = false;
	 }

	 cur_sec_row = row;
	 if (cur_sec_row == -1) {
		  gtk_widget_set_sensitive(widgets.ExtSecDeleteSetting, false);
		  gtk_entry_set_text(GTK_ENTRY(widgets.ExtSecDescription), "");
		  gtk_entry_set_text (GTK_ENTRY(widgets.ExtSecType),
							  SecurityTypeStr(ST_BOOL).c_str());

		  gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.ExtSecID), 0);
		  gtk_editable_delete_text(GTK_EDITABLE(widgets.ExtSecStrEnum), 0, -1);
		  allowSettingInput(false);

	 }
	 else {

		  // Security description.
		  gtk_entry_set_text(GTK_ENTRY(widgets.ExtSecDescription), 
							 security_settings[cur_sec_row].m_DescriptiveName.c_str());

		  // Security type.
		  gtk_entry_set_text (GTK_ENTRY(widgets.ExtSecType),
							  SecurityTypeStr(security_settings[cur_sec_row].m_Type).c_str());
	
		  // Security ID.
		  gtk_spin_button_set_value(GTK_SPIN_BUTTON(widgets.ExtSecID), 
									security_settings[cur_sec_row].m_ID);

		  // string enumeration.
		  display_str_enum();

		  gtk_widget_set_sensitive(widgets.ExtSecDeleteSetting, true);
		  allowSettingInput(true);
	 }
	 sec_selecting = false;
}

int securitySettingsMaxID(){
	 vector<SecurityItem>::iterator ai = security_settings.begin(),
		  bi = security_settings.end();

	 int maxID = -1;
	 while (ai != bi){
		  if ((*ai).m_ID > maxID){
			   maxID = (*ai).m_ID;
		  }
		  ++ai;
	 }
	 return maxID;
}

string int2String(int i){
	 string ret = "";
	 if (i<1000){
		  char num[4];
		  snprintf(num, 3, "%d", i);
		  ret = num;
	 }
	 return ret;
}

void GetCustomSecuritySettings(vector<SecurityItem>&v){
	 SecurityHash sh;
	 wdf->GetSecurityItems(sh);
	 sec_hash internalsh = sh.GetHash();
	 
	 SecurityItem currsi;
	 v.clear();

	 // iterate through the hash table, create an SecurityItem
	 // object out of each item.
	 sec_hash::iterator ai = internalsh.begin(),
		  bi = internalsh.end();

	 while (ai != bi){
		  SecurityItem& si = ai->second;
		  if (si.m_ID >= 0){
			   currsi.m_ID = si.m_ID;
			   currsi.m_DescriptiveName = si.m_DescriptiveName;
			   currsi.m_Type = si.m_Type;
			   currsi.m_StringVals = si.m_StringVals;
			   v.push_back(currsi);
		  }
		  ++ai;
	 }
}

void SetCustomSecuritySettings(vector<SecurityItem>&v){
	 SecurityHash sh;

	 // Grab the current security settings and clear them.
	 wdf->GetSecurityItems(sh);

	 // Grab the -1 element.
	 string guid = wdf->GetID();
	 string hashval = guid + "\\-1";
	 SecurityItem si = sh.Lookup(hashval);

	 // only fill in the -1 element if it hasn't already been defined,
	 // that is, if its type comes up as ST_INVALID.
	 if (si.m_Type == ST_INVALID){
		  si.m_Parent = guid;
		  si.m_Type = ST_BOOL; // for lack of anything better
		  si.m_DescriptiveName = wdf->GetTitle();
		  si.m_ID = -1;
	 }

	 // Clear the security hash.
	 sh.Clear();

     // Put the -1 element back.
	 sh.Insert(hashval, si);

 	 if (v.size() == 0) {
		  wdf->SetSecurityItems(sh);
		  return;
	 }

	 // iterate through the vector of SecurityItem objects,
	 // creating a hash entry for each one.
	 vector<SecurityItem>::iterator ai = v.begin(),
		  bi = v.end();

	 while (ai != bi){
		  hashval = guid + "\\" + int2String((*ai).m_ID);
		  si = sh.Lookup(hashval);

		  si.m_Parent = guid;
		  si.m_ID = ai->m_ID;
		  si.m_DescriptiveName = ai->m_DescriptiveName;
		  si.m_Type = ai->m_Type;

		  if ((si.m_Type == ST_STRING_ENUM) 
			  || (si.m_Type == ST_STRING_ARRAY))
			   si.m_StringVals = ai->m_StringVals;

		  sh.Insert(hashval, si);

		  DEBUGMSG(wdfedit, "inserting custom security setting (%s, %s) into v (%d strvals)\n",
				   hashval.c_str(), si.m_DescriptiveName.c_str(),
				   si.m_StringVals.size());
		  ++ai;
	 }
	 wdf->SetSecurityItems(sh);

	 security = sh;
}

// Extension Security Setting dialog.
void InitSettingList()
{
	 // Get the setting list out of the WDF and into the vector
	 // of SecuritySettings.
	 gtk_text_set_line_wrap(GTK_TEXT(widgets.ExtSecStrEnum), true);
	 GetCustomSecuritySettings(security_settings);
	 RefreshSettingList();
}

void RefreshSettingList()
{

	 // Based on the contents of the Security Setting vector, 
	 // refresh the setting list. 
	 // Call after an add or a delete.
	 // Is this necessary?
	 char * entry[3];
	 
	 gtk_clist_clear(GTK_CLIST(widgets.ExtSecSettingList));

	 for (unsigned int i = 0; i < security_settings.size(); i++) {		  
		  int id = security_settings[i].m_ID;
		  if (id >= 0){
			   string desc = security_settings[i].m_DescriptiveName;
			   string settingType 
					= SecurityTypeStr(security_settings[i].m_Type);
			   entry[0] = (char *) desc.c_str();
			   entry[1] = (char *) settingType.c_str();
			   entry[2] = (char *) int2String(id).c_str();
			   gtk_clist_append(GTK_CLIST(widgets.ExtSecSettingList), entry);
		  }
	 }
	 select_security_row(-1);
}

void on_ExtSecSettingList_select_row(GtkCList *clist,
									 gint row,
									 gint column,
									 GdkEventButton *event,
									 gpointer user_data)
{
	 select_security_row(row);
}

void on_ExtSecSettingList_unselect_row(GtkCList *clist,
									   gint row,
									   gint column,
									   GdkEventButton *event,
									   gpointer user_data)
{
	 if (cur_sec_row == row)
	   select_security_row(-1);
}

void on_ExtSecAddSetting_clicked (GtkWidget *widget, void *data)
{
	 // Add a new security setting. 
	 SecurityItem newSetting;
	 newSetting.m_ID = securitySettingsMaxID() + 1;
	 newSetting.m_DescriptiveName = "new setting";
	 newSetting.m_Type = ST_BOOL;

	 security_settings.push_back (newSetting);
	 
	 RefreshSettingList();
	 select_security_row(security_settings.size() - 1);
}

void on_ExtSecDeleteSetting_clicked (GtkWidget *widget, void *data)
{
	 // Delete a selected security setting.
	 
	 // Get the selected row.
	 vector<SecurityItem>::iterator ai = security_settings.begin(), 
		  bi = security_settings.end();
	 int index = 0;
	 while (ai != bi) {
		  if (index == cur_sec_row)
			   security_settings.erase(ai);
		  ++ai;
		  index++;
	 }
	 
	 RefreshSettingList();
}

void on_ExtSecDescription_changed (GtkWidget *widget, void *data)
{

	 // Update the security setting and the list as well.
	 if (cur_sec_row < 0) return;

	 security_settings[cur_sec_row].m_DescriptiveName = 
		  gtk_entry_get_text(GTK_ENTRY(widgets.ExtSecDescription));
	 gtk_clist_set_text(GTK_CLIST(widgets.ExtSecSettingList),
						cur_sec_row, 0, 
						security_settings[cur_sec_row].m_DescriptiveName.c_str());
}

void on_ExtSecType_changed (GtkWidget *widget, void *data)
{
	 // Update the security setting and the list as well.
	 if (cur_sec_row < 0) return;

	 // Grab the index.
	 string setting = gtk_entry_get_text(GTK_ENTRY(widgets.ExtSecType));
	 security_settings[cur_sec_row].m_Type = settingType(setting);
	 gtk_clist_set_text(GTK_CLIST(widgets.ExtSecSettingList),
						cur_sec_row, 1, 
						setting.c_str());

	 // If the type changes away from ST_STRING_ENUM, there's no need to
	 // clear the text box.
	 gtk_widget_set_sensitive(
		  widgets.ExtSecStrEnum, 
		  ((security_settings[cur_sec_row].m_Type == ST_STRING_ENUM) 
		   || (security_settings[cur_sec_row].m_Type == ST_STRING_ARRAY)));
}

void on_ExtSecID_changed (GtkWidget *widget, void *data)
{
	 // Update the security setting and the list as well.
	 if (cur_sec_row < 0) return;

	 int id = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widgets.ExtSecID));
	 security_settings[cur_sec_row].m_ID = id;
	 gtk_clist_set_text(GTK_CLIST(widgets.ExtSecSettingList),
						cur_sec_row, 2, 
						int2String(id).c_str());
}



void on_ExtSecStrEnum_changed (GtkWidget *widget, void *data){
	 if ((cur_sec_row < 0)  || sec_selecting) {
		  return;
	 }

	 if ((security_settings[cur_sec_row].m_Type == ST_STRING_ENUM)
		 || (security_settings[cur_sec_row].m_Type == ST_STRING_ARRAY)){
		  // may want to move this somewhere else, like every time you 
		  // change rows or click 'ok'.
		  str_enum_changed = true;
	 }
}

void on_ExtSecOK_clicked (GtkWidget *widget, void *data)
{
	 // Propagate the changes from the vector of SecuritySettings to
	 // the WDF file.
	 if (str_enum_changed){
		  char *reg = gtk_editable_get_chars(GTK_EDITABLE(widgets.ExtSecStrEnum), 0, -1);
		  DEBUGMSG(wdfedit, "string read from box: %s\n",
				   reg);

		  updateSecurityItemStrEnum(security_settings[cur_sec_row], reg);
		  g_free(reg);
		  str_enum_changed = false;
	 }

	 SetCustomSecuritySettings(security_settings);
	 gtk_widget_hide(widgets.ExtSecDialog);

}
void on_ExtSecCancel_clicked (GtkWidget *widget, void *data)
{
	 // Ignore changes made to the vector.
//	 security_settings.clear();
	 gtk_widget_hide(widgets.ExtSecDialog);

}
