#include "WDFEdit.h"
#include "SashGUID.h"

// -------------------- WDF editor Install tab -----------------
static int cur_inst_row = -1;
vector<WDFInstallScreen> inst_pages;

string inst_type_string(InstallScreenType t) {
  string s;

  switch (t) {
  case IST_LICENSE:
	s = "Licence";
	break;
  case IST_CUSTOM:
	s = "Custom";
	break;
  case IST_POSTSECURITY:
	s = "Post Security";
	break;
  case IST_FAILURE:
	s = "Failure";
	break;
  case IST_SECURITY:
	s = "Security";
	break;
  case IST_SUCCESS:
	s = "Success";
	break;
  case IST_SCRIPT:
	s = "Script";
	break;
  case IST_TEXT:
  default:
	s = "Text";
	break;
  }

  return s;
}

void select_inst_row(int row) {
  cur_inst_row = row;
  if (cur_inst_row == -1) {
	gtk_widget_set_sensitive(widgets.InstallTable, false);
	gtk_widget_set_sensitive(widgets.RemovePage, false);
	gtk_entry_set_text(GTK_ENTRY(widgets.InstallTitle), "");
	gtk_entry_set_text(GTK_ENTRY(widgets.InstallPage), "");
  }
  else {
	// Page title
	gtk_entry_set_text(GTK_ENTRY(widgets.InstallTitle),
					   inst_pages[cur_inst_row].title.c_str());

	// Page type
	GtkWidget * page_type;
	switch (inst_pages[cur_inst_row].type) {
	case IST_LICENSE:
	  page_type = widgets.InstallLicence;
	  break;
	case IST_CUSTOM:
	  page_type = widgets.InstallCustom;
	  break;
	case IST_POSTSECURITY:
	  page_type = widgets.InstallPostSecurity;
	  break;
	case IST_FAILURE:
	  page_type = widgets.InstallFailure;
	  break;
	case IST_SECURITY:
	  page_type = widgets.InstallSecurity;
	  break;
	case IST_SUCCESS:
	  page_type = widgets.InstallSuccess;
	  break;
	case IST_SCRIPT:
	  page_type = widgets.InstallScript;
	  break;
	case IST_TEXT:
	default:
	  page_type = widgets.InstallText;
	  break;
	}
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(page_type), true);

	// Install page
	gtk_entry_set_text(GTK_ENTRY(widgets.InstallPage),
					   inst_pages[cur_inst_row].page.file.c_str());

	// Install location
	GtkWidget * loc;
	switch (inst_pages[cur_inst_row].page.location) {
	case FLT_CACHE:
	  loc = widgets.InstallCache;
	  break;
	case FLT_SHAREDLOCATION:
	  loc = widgets.InstallShared;
	  break;
	case FLT_SERVER:
	  loc = widgets.InstallServer;
	  break;
	case FLT_WEBLICATIONPATH:
	  loc = widgets.InstallWeblicationPath;
	  break;
	case FLT_WEBLICATION:
	default:
	  loc = widgets.InstallWeblication;
	  break;
	}
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(loc), true);

	// Activate the edit pane
	gtk_widget_set_sensitive(widgets.InstallTable, true);
	gtk_widget_set_sensitive(widgets.RemovePage, true);
  }
}

void update_inst_pages() {
  char * entry[3];

  gtk_clist_clear(GTK_CLIST(widgets.InstallList));
  
  for (unsigned int i = 0; i < inst_pages.size(); i++) {
	   // verify valid location
	   if (inst_pages[i].page.location < 0 || 
		   inst_pages[i].page.location > FLT_NUM_VALUES) {
			inst_pages[i].page.location = FLT_WEBLICATION;
	   }

	entry[0] = (char *) inst_pages[i].title.c_str();
	entry[1] = (char *) inst_type_string(inst_pages[i].type).c_str();
	entry[2] = NULL;

	gtk_clist_append(GTK_CLIST(widgets.InstallList), entry);
  }

  select_inst_row(-1);
}

void on_InstallList_select_row(GtkCList *clist,
						   gint row,
						   gint column,
						   GdkEventButton *event,
						   gpointer user_data) 
{
  select_inst_row(row);
}


void on_InstallList_unselect_row(GtkCList *clist,
							 gint row,
							 gint column,
							 GdkEventButton *event,
							 gpointer user_data) 
{
  if (cur_inst_row == row)
	select_inst_row(-1);
}

void on_InstallTitle_changed(GtkWidget * widget, void * data) {
  if (cur_inst_row < 0) return;

  inst_pages[cur_inst_row].title = gtk_entry_get_text(GTK_ENTRY(widgets.InstallTitle));
  gtk_clist_set_text(GTK_CLIST(widgets.InstallList), cur_inst_row, 0, 
					 inst_pages[cur_inst_row].title.c_str());
}

void on_InstallPage_changed(GtkWidget * widget, void * data) {
  if (cur_inst_row < 0) return;

  inst_pages[cur_inst_row].page.file = gtk_entry_get_text(GTK_ENTRY(widgets.InstallPage));
}

void on_NewPage_clicked(GtkWidget * widget, void * data) {
  int new_index;

  WDFInstallScreen newPage;

  newPage.title = "*new page*";
  newPage.type = IST_TEXT;
  newPage.next = true;
  newPage.back = true;
  newPage.page.location = FLT_WEBLICATION;
  new_index = inst_pages.size();
  inst_pages.push_back(newPage);

  update_inst_pages();
  gtk_clist_select_row(GTK_CLIST(widgets.InstallList), new_index, 0);
}

void on_RemovePage_clicked(GtkWidget * widget, void * data) {
  vector<WDFInstallScreen>::iterator a = inst_pages.begin(), b = inst_pages.end();
  int index = 0;
  while (a != b) {
	if (index == cur_inst_row)
	  inst_pages.erase(a);
	++a;
	index++;
  }

  update_inst_pages();
}

void on_InstallType_toggled(GtkWidget * widget, void * data) {
  if (cur_inst_row < 0) return;

  InstallScreenType type = (InstallScreenType) atoi((char *) data);

  inst_pages[cur_inst_row].type = type;
  gtk_clist_set_text(GTK_CLIST(widgets.InstallList), cur_inst_row, 
					 1, inst_type_string(inst_pages[cur_inst_row].type).c_str());
}

void on_PageLoc_toggled(GtkWidget * widget, void * data) {
  if (cur_inst_row < 0) return;

  FileLocationType loc = (FileLocationType) atoi((char *) data);

  inst_pages[cur_inst_row].page.location = loc;
}

void on_PageBack_toggled(GtkWidget * widget, void * data) {
  if (cur_inst_row < 0) return;

  inst_pages[cur_inst_row].back = 
	gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widgets.PageBack));
}

void on_PageNext_toggled(GtkWidget * widget, void * data) {
  if (cur_inst_row < 0) return;

  inst_pages[cur_inst_row].next = 
	gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widgets.PageNext));
}
