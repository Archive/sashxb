#include "WDFEdit.h"
#include "SashGUID.h"

// -------------------- WDF editor Action tab -----------------
vector<WDFAction> actions;
static int cur_act_row = -1;
static bool act_selecting = false;

void init_actions() {
  //  gtk_widget_ref(widgets.ActionView);
  gtk_text_set_line_wrap(GTK_TEXT(widgets.ActionRegistration), true);
}

void select_action_row(int row) {
  act_selecting = true;
  DEBUGMSG(wdfedit, "Selecting row %d\n", row);

  cur_act_row = row;
  if (cur_act_row == -1) {
	gtk_widget_set_sensitive(widgets.ActionEditTable, false);
	gtk_widget_set_sensitive(widgets.DeleteAction, false);
	gtk_entry_set_text(GTK_ENTRY(widgets.ActionName), "");
	gtk_entry_set_text(GTK_ENTRY(widgets.ActionGUID), "");
	gtk_entry_set_text(GTK_ENTRY(widgets.LocationGUID), "");
	gtk_editable_delete_text(GTK_EDITABLE(widgets.ActionRegistration), 0, -1);
  }
  else {
	// Action Name
	gtk_entry_set_text(GTK_ENTRY(widgets.ActionName), 
					   actions[cur_act_row].name.c_str());

	// Action GUID
	gtk_entry_set_text(GTK_ENTRY(widgets.ActionGUID), 
					   actions[cur_act_row].id.c_str());

	// Location GUID
	gtk_entry_set_text(GTK_ENTRY(widgets.LocationGUID), 
					   actions[cur_act_row].locationid.c_str());

	// Registration string
	gtk_text_freeze(GTK_TEXT(widgets.ActionRegistration));
	gtk_editable_delete_text(GTK_EDITABLE(widgets.ActionRegistration), 0, -1);
	gtk_text_insert(GTK_TEXT(widgets.ActionRegistration), 
					NULL, NULL, NULL, 
					actions[cur_act_row].registration.c_str(),
					actions[cur_act_row].registration.length());
	gtk_text_thaw(GTK_TEXT(widgets.ActionRegistration));

	// Activate the edit pane
	gtk_widget_set_sensitive(widgets.ActionEditTable, true);
	gtk_widget_set_sensitive(widgets.DeleteAction, true);
  }

  act_selecting = false;
}

void update_actions() {
  char * entry[5];

  gtk_clist_clear(GTK_CLIST(widgets.ActionList));

  for (unsigned int i = 0; i < actions.size(); i++) {
	entry[0] = (char *) actions[i].name.c_str();
	entry[1] = (char *) actions[i].id.c_str();
	entry[2] = (char *) get_ext_name(actions[i].locationid).c_str();
	entry[3] = (char *) actions[i].locationid.c_str();
	entry[4] = NULL;

	gtk_clist_append(GTK_CLIST(widgets.ActionList), entry);
  }
  select_action_row(-1);
  DEBUGMSG(wdfedit, "Action update complete\n");
}

void on_ActionList_select_row(GtkCList *clist,
							  gint row,
							  gint column,
							  GdkEventButton *event,
							  gpointer user_data) 
{
  select_action_row(row);
}


void on_ActionList_unselect_row(GtkCList *clist,
								gint row,
								gint column,
								GdkEventButton *event,
								gpointer user_data) 
{
  if (cur_act_row == row)
	select_action_row(-1);
}

void on_ActionName_changed(GtkWidget * widget, void * data) {
  if (cur_act_row < 0) return;

  actions[cur_act_row].name = gtk_entry_get_text(GTK_ENTRY(widgets.ActionName));
  gtk_clist_set_text(GTK_CLIST(widgets.ActionList), cur_act_row, 0, actions[cur_act_row].name.c_str());
}

void on_ActionGUID_changed(GtkWidget * widget, void * data) {
  if (cur_act_row < 0) return;

  actions[cur_act_row].id = gtk_entry_get_text(GTK_ENTRY(widgets.ActionGUID));
  gtk_clist_set_text(GTK_CLIST(widgets.ActionList), cur_act_row, 1, actions[cur_act_row].id.c_str());
}

void on_LocationGUID_changed(GtkWidget * widget, void * data) {
  if (cur_act_row < 0) return;

  actions[cur_act_row].locationid = gtk_entry_get_text(GTK_ENTRY(widgets.LocationGUID));
  
  gtk_clist_set_text(GTK_CLIST(widgets.ActionList), cur_act_row, 2,
					 get_ext_name(actions[cur_act_row].locationid).c_str());
  gtk_clist_set_text(GTK_CLIST(widgets.ActionList), cur_act_row, 3,
					 actions[cur_act_row].locationid.c_str());
}

void on_ActionRegistration_changed(GtkWidget * widget, void * data) {
  char * reg;

  if (cur_act_row < 0 || act_selecting) return;

  reg = gtk_editable_get_chars(GTK_EDITABLE(widgets.ActionRegistration), 0, -1);
  actions[cur_act_row].registration = reg;
  g_free(reg);
}

void on_NewAction_clicked(GtkWidget * widget, void * data) {
  int new_index;

  WDFAction newAction;

  newAction.name = "*new action*";
  newAction.id = GetNewGUID();
  new_index = actions.size();
  actions.push_back(newAction);

  DEBUGMSG(wdfedit, "New action index is %d\n", new_index);

  update_actions();
  gtk_clist_select_row(GTK_CLIST(widgets.ActionList), new_index, 0);
}

void on_DeleteAction_clicked(GtkWidget * widget, void * data) {
  vector<WDFAction>::iterator a = actions.begin(), b = actions.end();
  int index = 0;
  while (a != b) {
	if (index == cur_act_row)
	  actions.erase(a);
	++a;
	index++;
  }

  update_actions();
}

void on_ActionGenGUID_clicked(GtkWidget * widget, void * data) {
  generate_guid_text(GTK_ENTRY(widgets.ActionGUID));
}

void on_ChooseLocGUID_clicked(GtkWidget * widget, void * data) {
  string loc;

  if (choose_ext(false, false, true, loc)) {
	gtk_entry_set_text(GTK_ENTRY(widgets.LocationGUID), loc.c_str());
  }
}
