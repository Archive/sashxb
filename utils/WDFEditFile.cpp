#include "WDFEdit.h"
#include "SashGUID.h"

// -------------------- WDF editor File tab -----------------
vector <WDFFile> files;
static int cur_file_row = -1;

string file_loc_string(FileLocationType flt) {
  string s;

  switch (flt) {
  case FLT_WEBLICATION:
	s = "Weblication";
	break;
  case FLT_CACHE:
	s = "Cache";
	break;
  case FLT_WEBLICATIONPATH:
	s = "Weblication path";
	break;
  case FLT_SHAREDLOCATION:
	s = "Shared Location";
	break;
  case FLT_SERVER:
	s = "Server";
	break;
  default:
	s = "<unknown>";
	break;
  }
  
  return s;
}

void select_file_row(int row) {
  cur_file_row = row;
  if (cur_file_row == -1) {
	gtk_widget_set_sensitive(widgets.FileTable, false);
	gtk_widget_set_sensitive(widgets.RemoveFile, false);
	gtk_entry_set_text(GTK_ENTRY(widgets.FileNameEntry), "");
  }
  else {
	// File name
	gtk_entry_set_text(GTK_ENTRY(widgets.FileNameEntry), files[cur_file_row].filename.c_str());

	// File location
	GtkWidget * loc;
	switch (files[cur_file_row].location) {
	case FLT_CACHE:
	  loc = widgets.FileCache;
	  break;
	case FLT_SHAREDLOCATION:
	  loc = widgets.FileShared;
	  break;
	case FLT_SERVER:
	  loc = widgets.FileServer;
	  break;
	case FLT_WEBLICATIONPATH:
	  loc = widgets.FileWeblicationPath;
	  break;
	case FLT_WEBLICATION:
	default:
	  loc = widgets.FileWeblication;
	  break;
	}
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(loc), true);

	// Precache flag
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widgets.FilePrecache),
								 files[cur_file_row].precache);
	// Activate the edit pane
	gtk_widget_set_sensitive(widgets.FileTable, true);
	gtk_widget_set_sensitive(widgets.RemoveFile, true);
  }
}

void update_files() {
  char * entry[3];

  gtk_clist_clear(GTK_CLIST(widgets.FileList));
  
  for (unsigned int i = 0; i < files.size(); i++) {
	entry[0] = (char *) files[i].filename.c_str();
	entry[1] = (char *) file_loc_string(files[i].location).c_str();
	entry[2] = NULL;

	gtk_clist_append(GTK_CLIST(widgets.FileList), entry);
  }

  select_file_row(-1);
}

void on_FileList_select_row(GtkCList *clist,
						   gint row,
						   gint column,
						   GdkEventButton *event,
						   gpointer user_data) 
{
  select_file_row(row);
}


void on_FileList_unselect_row(GtkCList *clist,
							 gint row,
							 gint column,
							 GdkEventButton *event,
							 gpointer user_data) 
{
  if (cur_file_row == row)
	select_file_row(-1);
}

void on_FileName_changed(GtkWidget * widget, void * data) {
  if (cur_file_row < 0) return;

  files[cur_file_row].filename = gtk_entry_get_text(GTK_ENTRY(widgets.FileNameEntry));
  gtk_clist_set_text(GTK_CLIST(widgets.FileList), cur_file_row, 0, 
					 files[cur_file_row].filename.c_str());
}

void on_AddFile_clicked(GtkWidget * widget, void * data) {
  int new_index;

  WDFFile newFile;

  newFile.filename = "*new file*";
  newFile.location = FLT_WEBLICATION;
  newFile.precache = false;
  new_index = files.size();
  files.push_back(newFile);

  update_files();
  gtk_clist_select_row(GTK_CLIST(widgets.FileList), new_index, 0);
}

void on_FileSelect_clicked(GtkWidget * widget, void * data) {
  gtk_widget_show(widgets.FileSelectDialog);
}

void on_FileSelect_cancel(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.FileSelectDialog);
}

void on_FileSelect_OK(GtkWidget * widget, void * data) {
  gtk_widget_hide(widgets.FileSelectDialog);

  string fname = gtk_file_selection_get_filename(GTK_FILE_SELECTION(widgets.FileSelectDialog));
  int start = fname.rfind('/') + 1; 
  string baseName = string(fname, start, fname.size() - start);

  gtk_entry_set_text(GTK_ENTRY(widgets.FileNameEntry), baseName.c_str());
}

void on_RemoveFile_clicked(GtkWidget * widget, void * data) {
  vector<WDFFile>::iterator a = files.begin(), b = files.end();
  int index = 0;
  while (a != b) {
	if (index == cur_file_row)
	  files.erase(a);
	++a;
	index++;
  }

  update_files();
}

void on_FileLoc_toggled(GtkWidget * widget, void * data) {
  if (cur_file_row < 0) return;

  FileLocationType loc = (FileLocationType) atoi((char *) data);

  files[cur_file_row].location = loc;
  gtk_clist_set_text(GTK_CLIST(widgets.FileList), cur_file_row, 
					 1, file_loc_string(files[cur_file_row].location).c_str());
}

void on_FilePrecache_toggled(GtkWidget * widget, void * data) {
  if (cur_file_row < 0) return;

  files[cur_file_row].precache = 
	gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(widgets.FilePrecache));
}
