dnl - Determine where and which version of SashXB is installed on the system
dnl - Author: Andrew Chatham

AC_DEFUN([CHECK_JAVA_CONNECT],[
	AC_MSG_CHECKING(for JDK Home)
	AC_ARG_WITH(jdk-home,
	  [  --with-jdk-home=dir      Specify the JDK home directory],[
	if test "x$withval" != "x"; then
		JDK_HOME=$withval
		JAVA_CONNECT_DIR=javaconnect
		JAVA_CONNECT_MAKE=javaconnect/Makefile
	fi
	], [])
	AC_SUBST(JDK_HOME)
	AC_SUBST(JAVA_CONNECT_DIR)
	AC_SUBST(JAVA_CONNECT_MAKE)
])

AC_DEFUN([CHECK_SASHXB_PATH],[
	AC_MSG_CHECKING(for SashXB)

	if test "x$prefix" != "xNONE"; then
		default_prefix=$prefix
	else
		default_prefix=$ac_default_prefix
	fi

	CFLAGS=${CFLAGS--O}

	AC_ARG_WITH(sashxb-prefix,
	  [  --with-sashxb-prefix=dir      Specify the SashXB prefix directory],[
	if test "x$withval" != "x"; then
		SASHXB_PREFIX=$withval

	dnl use the value from the environment variable
	elif test "x$SASHXB_PREFIX" == "x"; then
		SASHXB_PREFIX=$default_prefix
	fi
	], [
	SASHXB_PREFIX=$default_prefix
	if ! test -f "$SASHXB_PREFIX/include/sashxb/xpcomtools.h"; then
		SASHXB_PREFIX="/usr/local"
	fi
	if ! test -f "$SASHXB_PREFIX/include/sashxb/xpcomtools.h"; then
		SASHXB_PREFIX="/tmp/sash"
	fi
	if ! test -f "$SASHXB_PREFIX/include/sashxb/xpcomtools.h"; then
		SASHXB_PREFIX="/usr"
	fi
	])

	if ! test -f "$SASHXB_PREFIX/include/sashxb/xpcomtools.h"; then
		AC_MSG_ERROR("Could not find SashXB.")
	fi

	SASHXB_INCLUDE_DIR=$SASHXB_PREFIX/include/sashxb
	SASHXB_IDL_DIR=$SASHXB_PREFIX/share/idl/sashxb
	SASHXB_LIB_DIR=$SASHXB_PREFIX/lib/sashxb

	AC_SUBST(SASHXB_PREFIX)
	AC_SUBST(SASHXB_INCLUDE_DIR)
	AC_SUBST(SASHXB_IDL_DIR)
	AC_SUBST(SASHXB_LIB_DIR)
	AC_MSG_RESULT(found)
])


