
mozinclude=$1
target=$2

if [ ! -d $2/mozinclude ]
then mkdir $2/mozinclude
fi

for f in $1/*.h
	do ln -sf $f $2/mozinclude
done

if [ -d $1/obsolete ] 
then

if [ ! -d $2/include/obsolete ]
then mkdir $2/include/obsolete
fi

for f in $1/obsolete/*.h
	do ln -sf $f $2/mozinclude/obsolete
done

fi
