
mozinclude=$1
target=$2

if [ ! -d $2/mozidl ]
then mkdir $2/mozidl

current_dir=`pwd`
cd $2/mozidl

for f in $1/*.idl
	do $MOZILLA_LIB_DIR/xpidl -I$1 -m header $f
done

cd $current_dir

fi
