#include ./$(DEPTH)/config/autoconf.mk

##### standard includes. anything used by all Makefile.am's should be here

SASHINCLUDES = \
	-Wall \
	-DG_LOG_DOMAIN=\"$(PACKAGE)\" \
	-DGNOMELOCALEDIR=\""$(datadir)/locale"\" \
	-DTRACING \
	-DOSTYPE=\"$(uname -s)$(uname -r)\" \
	-DOJI \
	-DXP_UNIX \
	-DXP_UNIX \
	-DMOZILLA_CLIENT \
	-DIBMBIDI \
	-fexceptions \
	-fno-rtti \
	-fPIC \
	-I$(top_srcdir) \
	-I$(top_srcdir)/include \
	-I$(top_builddir)/include \
	$(GNOME_INCLUDEDIR) \
	-I$(top_srcdir)/intl \
	-I$(MOZILLA_INCLUDE_DIR)/string \
	-I$(MOZILLA_INCLUDE_DIR)/browser \
	-I$(MOZILLA_INCLUDE_DIR)/js \
	-I$(MOZILLA_INCLUDE_DIR)/necko \
	-I$(MOZILLA_INCLUDE_DIR)/xpcom \
	-I$(MOZILLA_INCLUDE_DIR)/layout \
	-I$(MOZILLA_INCLUDE_DIR)/xpconnect \
	-I$(MOZILLA_INCLUDE_DIR)/embed_base \
	-I$(MOZILLA_INCLUDE_DIR)/widget \
	-I$(MOZILLA_INCLUDE_DIR)/dom \
	-I$(MOZILLA_INCLUDE_DIR)/gfx \
	-I$(MOZILLA_INCLUDE_DIR)/nspr \
	-I$(MOZILLA_INCLUDE_DIR)/gtkembedmoz \
	-I$(MOZILLA_INCLUDE_DIR)/docshell \
	-I$(MOZILLA_INCLUDE_DIR)/pref \
	-I$(MOZILLA_INCLUDE_DIR)/caps \
	-I$(MOZILLA_INCLUDE_DIR)/webbrwsr \
	-I$(MOZILLA_INCLUDE_DIR)/content \
	-I$(top_srcdir)/tools \
	-I$(top_srcdir)/wdf \
	-I$(top_srcdir)/extensiontools \
	-I$(top_srcdir)/net \
	-I$(top_srcdir)/runtime \
	-I$(top_srcdir)/installer \
	-I$(top_srcdir)/instmanager \
	-I$(top_srcdir)/filesystem \
	-I$(top_srcdir)/ezxml \
	-I$(top_srcdir)/registry \
	-I$(top_srcdir)/protocolhandler \
	-I$(top_srcdir)/cachemanager \
	-I$(top_srcdir)/security \
	-I$(top_srcdir)/mozinclude \
	-I$(top_srcdir)/mozinclude/obsolete \
	`gnome-config libglade --cflags` \
	-D_REENTRANT  # for gthreads
	
JCINCLUDES = \
	$(SASHINCLUDES) \
	-I$(top_srcdir)/javaconnect/src/idl \
	-I$(top_srcdir)/javaconnect/src/jctools \
	-I$(top_srcdir)/javaconnect/src/jcjava \
	-I$(top_srcdir)/javaconnect/src/jcxpcom \
	$(JNI_INCLUDES)

JCXPIDLINCLUDES = \
	`libIDL-config --cflags`


MOZILLA_LIBS = \
	-L$(MOZILLA_LIB_DIR) \
	-L$(MOZILLA_LIB_DIR)/components \
	-lgkgfx \
	-lgtkembedmoz \
	-lgtksuperwin \
	-lgtkxtbin \
	-ljsj \
	-lmozjs \
	-lnspr4 \
	-lplc4 \
	-lplds4 \
	-lxpcom \
	-lxpistub
	
JC_LIBS = \
	-L$(MOZILLA_LIB_DIR) \
	-lxpcom \
	-lxpistub \
	-lnspr4 \
	-lplc4 \
	-lplds4 \
	-lgtkembedmoz \
	$(JNI_LIBS) \
	-ljvm

JC_XPIDL_LIBS = \
	`libIDL-config --libs`

	
##### lib macros -- you should only have to change library locations *here*

JCTOOLSLDFLAGS = \
	-L$(top_builddir)/javaconnect/src/jctools \
	-ljctools

SASHTOOLSLDFLAGS = \
	-L$(top_builddir)/tools \
	-lsashtools

SASHSECURITYLDFLAGS = \
	-L$(top_builddir)/security \
	-lsashsecurity

SASHLDFLAGS = \
	-L$(top_builddir)/sash \
	-lsash

INSTALLERLDFLAGS = \
	-L$(top_builddir)/installer \
	-linstaller

FILESYSTEMLDFLAGS = \
	-L$(top_builddir)/filesystem \
	-lfilesystem

INSTMANAGERLDFLAGS = \
	-L$(top_builddir)/instmanager \
	-linstmanager 

CACHEMANAGERLDFLAGS = \
	-L$(top_builddir)/cachemanager \
	-lcachemanager

SASHWDFLDFLAGS = \
	-L$(top_builddir)/wdf \
	-lsashwdf

EXTENSIONTOOLSLDFLAGS = \
	-L$(top_builddir)/extensiontools \
	-lsashextensiontools

EZXMLLDFLAGS = \
	-L$(top_builddir)/ezxml \
	-lezxml \
	$(GDOMELIBS)

SASHRUNTIMETOOLS = \
	-L$(top_builddir)/runtime \
	-lruntimetools

######### XPIDL macros. used by extensions, or anything that uses XPCOM ####

IDL = $(MOZILLA_UTIL_BIN_DIR)/xpidl
XPTLINK = $(MOZILLA_UTIL_BIN_DIR)/xpt_link
JCIDL= $(top_srcdir)/javaconnect/src/jcxpidl/jcxpidl

# you can replace IDLFLAGS or redefine it below if you need
# more idl include dirs.
IDLFLAGS = \
	-w \
	-I $(MOZILLA_IDL_DIR) \
	-I $(top_srcdir)/include \
	-I .

%.h: %.idl
	$(IDL) $(IDLFLAGS) -m header -o `basename $@ .h` $<

%.xpt: %.idl
	$(IDL) $(IDLFLAGS) -m typelib -o `basename $@ .xpt` $<

%.html: %.idl
	$(IDL) $(IDLFLAGS) -m doc -o `basename $@ .html` $<

%.java: %.idl
	/home/atevstef/devel/xpidl $(IDLFLAGS) -m jc `basename $@ .java` $<

%.h: %.java
	$(JAVAH) -classpath . org.mozilla.xpcom.`basename $@ .h`

%.class: %.java
	$(JAVAC) -classpath . -d . $<

PYTHON = `which python`

componentsdir = $(pkglibdir)/components

idldir = $(datadir)/idl/$(PACKAGE)

includedir = $(prefix)/include
pkgincludedir = $(includedir)/$(PACKAGE)

IDL_FILES =
JAVA_FILES =
JAVA_IDL_FILES =
JAVA_NATIVE_CLASSES =

skeldir = $(pkgdatadir)/sash-skel

defaultextsdir = $(pkgdatadir)/default-exts

