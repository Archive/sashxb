/***********************************************************************/
/*                                                                     */
/*                              CamlIDL                                */
/*                                                                     */
/*            Xavier Leroy, projet Cristal, INRIA Rocquencourt         */
/*                                                                     */
/*  Copyright 1999 Institut National de Recherche en Informatique et   */
/*  en Automatique.  All rights reserved.  This file is distributed    */
/*  under the terms of the GNU Library General Public License.         */
/*                                                                     */
/***********************************************************************/

/* $Id$ */

/* Helper functions for handling COM interfaces */

#include <string.h>
#include <stdio.h>

extern "C" {
#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/fail.h>
#include <caml/callback.h>
}

#include "camlxpidlruntime.h"
#include "comstuff.h"
#include <nsComponentManagerUtils.h>
#include <nsMemory.h>
#include <assert.h>

extern "C" {

int camlidl_num_components = 0;

value camlidl_lookup_method(char * name)
{
  static value * lookup_clos = NULL;

  if (lookup_clos == NULL) {
    lookup_clos = caml_named_value("Oo.new_method");
    if (lookup_clos == NULL) invalid_argument("Oo.new_method not registered");
  }
  return callback(*lookup_clos, copy_string(name));
}

static void camlidl_finalize_interface(value intf)
{
  interface IUnknown * i = (interface IUnknown *) Field(intf, 1);
  i->lpVtbl->Release(i);
}

value camlidl_pack_interface(void * intf, camlidl_ctx ctx)
{
  value res = alloc_final(2, camlidl_finalize_interface, 0, 1);
  Field(res, 1) = (value) intf;
  if (ctx != NULL && (ctx->flags & CAMLIDL_ADDREF)) {
    struct IUnknown * i = (struct IUnknown *) intf;
    i->lpVtbl->AddRef(i);
  }
  return res;
}

void * camlidl_unpack_interface(value vintf, camlidl_ctx ctx)
{
  struct IUnknown * intf = (struct IUnknown *) Field(vintf, 1);
  if (ctx != NULL && (ctx->flags & CAMLIDL_ADDREF)) {
	   intf->lpVtbl->AddRef(intf);
  }
  return (void *) intf;
}

/* Handle ClassInfo requests in Caml XPCOM objects */

struct IClassInfoVtbl {
  DECLARE_VTBL_PADDING
  HRESULT (*QueryInterface)(struct camlidl_intf * thisPtr,
                            IID * iid, void ** object);
  ULONG (*AddRef)(struct camlidl_intf * thisPtr);
  ULONG (*Release)(struct camlidl_intf * thisPtr);

	 // extra classinfo methods
	 HRESULT (*GetInterfaces)(struct camlidl_intf * thisPtr,
							  /*out*/ unsigned int *count,
							  /*out*/ IID ***GUID);
	 HRESULT (*GetHelperForLanguage)(struct camlidl_intf * thisPtr,
									 /*in*/ unsigned int language,
									 /*out*/ struct IUnknown **helper);
	 HRESULT (*GetContractID)(struct camlidl_intf * thisPtr,
							  /*out*/ char *(*contractID));
	 HRESULT (*GetClassDescription)(struct camlidl_intf * thisPtr,
									/*out*/ char *(*classDescription));
	 HRESULT (*GetClassID)(struct camlidl_intf * thisPtr,
						   /*out*/ IID **classID);
	 HRESULT (*GetImplementationLanguage)(struct camlidl_intf * thisPtr,
										  /*out*/ unsigned int *lang);
	 HRESULT (*GetFlags)(struct camlidl_intf * thisPtr,
						 /*out*/ unsigned int *flags);
	 HRESULT (*GetClassIDNoAlloc)(struct camlidl_intf * thisPtr,
								  /*out*/ IID *cid);
};

struct IClassInfoVtbl camlidl_classinfo_vtbl = {
  VTBL_PADDING
  camlidl_QueryInterface,
  camlidl_AddRef,
  camlidl_Release,
  camlidl_GetInterfaces,
  camlidl_GetHelperForLanguage,
  camlidl_GetContractID,
  camlidl_GetClassDescription,
  camlidl_GetClassID,
  camlidl_GetImplementationLanguage,
  camlidl_GetFlags,
  camlidl_GetClassIDNoAlloc
};

value camlidl_make_interface(void * vtbl, value caml_object, IID * iid,
                             int has_dispatch)
{
  struct camlidl_component * comp =
    (struct camlidl_component *) stat_alloc(sizeof(struct camlidl_component));
  comp->numintfs = 1;
  comp->refcount = 1;
  comp->factory = registered_xpcfactory; // XXX find a better way of doing this
  if (!comp->factory)
	   printf("CamlXPIDL WARNING: component factory is not set... bad things will happen\n");

  // setup classinfo interface
  comp->classinfo_intf.vtbl = &camlidl_classinfo_vtbl;
  comp->classinfo_intf.caml_object = caml_object;
  comp->classinfo_intf.iid = (nsIID *) &NS_GET_IID(nsIClassInfo);
  comp->classinfo_intf.comp = comp;

  // setup specified interface
  comp->intf[0].vtbl = vtbl;
  comp->intf[0].caml_object = caml_object;
  comp->intf[0].iid = iid;
  comp->intf[0].comp = comp;
#ifdef _WIN32
  comp->intf[0].typeinfo = has_dispatch ? camlidl_find_typeinfo(iid) : NULL;
#else
  if (has_dispatch)
    camlidl_error(0, "Com.make_xxx", "Dispatch interfaces not supported");
  comp->intf[0].typeinfo = NULL;
#endif
  register_global_root(&(comp->intf[0].caml_object));
  InterlockedIncrement(&camlidl_num_components);
  return camlidl_pack_interface(&(comp->intf[0]), NULL);
}


/* Basic methods (QueryInterface, AddRef, Release) for COM objects
   encapsulating a Caml object */

HRESULT STDMETHODCALLTYPE
camlidl_QueryInterface(struct camlidl_intf * pThis, nsIID * iid,
                       void ** object)
{
	 struct camlidl_component * comp = pThis->comp;
	 int i;

	 for (i = 0; i < comp->numintfs; i++) {
		  if (comp->intf[i].iid != NULL && (iid->Equals(*(comp->intf[i].iid)))) {
			   *object = (void *) &(comp->intf[i]);
			   InterlockedIncrement(&(comp->refcount));
			   return S_OK;
		  }
	 }
	 if (iid->Equals(IID_IUnknown)) {
		  *object = (void *) pThis;
		  InterlockedIncrement(&(comp->refcount));
		  return S_OK;
	 }
	 else if (iid->Equals(NS_GET_IID(nsIClassInfo))) {
		  *object =  (void *) (&comp->classinfo_intf);
		  InterlockedIncrement(&(comp->refcount));
		  return S_OK;
	 }
	 *object = NULL;
	 return E_NOINTERFACE;
}
  
ULONG STDMETHODCALLTYPE camlidl_AddRef(struct camlidl_intf * pThis)
{
  return InterlockedIncrement(&(pThis->comp->refcount));
}

ULONG STDMETHODCALLTYPE camlidl_Release(struct camlidl_intf * pThis)
{
  struct camlidl_component * comp = pThis->comp;
  ULONG newrefcount = InterlockedDecrement(&(comp->refcount));
  int i;

  if (newrefcount == 0) {
    for (i = 0; i < comp->numintfs; i++) {
      struct camlidl_intf * intf = &(comp->intf[i]);
      remove_global_root(&(intf->caml_object));
      if (intf->typeinfo != NULL) {
        struct IUnknown * i = (struct IUnknown *) intf->typeinfo;
        i->lpVtbl->Release(i);
      }
    }
    stat_free(comp);
    InterlockedDecrement(&camlidl_num_components);
  }
  return newrefcount;
}

/* nsIClassInfo methods */

HRESULT camlidl_GetInterfaces(struct camlidl_intf * pThis,
							  /*out*/ unsigned int *count,
							  /*out*/ nsIID *** ifaces) 
{
	 struct camlidl_component * comp = pThis->comp;

	 (*ifaces) = (nsIID **)nsMemory::Alloc(sizeof (nsIID *) * comp->numintfs);
	 for (int i = 0; i < comp->numintfs; i++) {
		  (*ifaces)[i] = (nsIID *)nsMemory::Clone(comp->intf[i].iid, sizeof(nsIID));
	 }
	 *count = comp->numintfs;
	 return S_OK;
}

HRESULT camlidl_GetHelperForLanguage(struct camlidl_intf * pThis,
									 /*in*/ unsigned int language,
									 /*out*/ struct IUnknown **helper)
{
	 *helper = NULL;
	 return S_OK;
}

HRESULT camlidl_GetContractID(struct camlidl_intf * pThis,
							  /*out*/ char *(*contractID))
{
	 nsIClassInfo * factory = pThis->comp->factory;
	 assert(factory != NULL);
	 return factory->GetContractID(contractID);
}

HRESULT camlidl_GetClassDescription(struct camlidl_intf * pThis,
									/*out*/ char *(*classDescription))
{
	 nsIClassInfo * factory = pThis->comp->factory;
	 assert(factory != NULL);
	 return factory->GetClassDescription(classDescription);
}

HRESULT camlidl_GetClassID(struct camlidl_intf * pThis,
						   /*out*/ IID **classID)
{
	 nsIClassInfo * factory = pThis->comp->factory;
	 assert(factory != NULL);
	 return factory->GetClassID(classID);
}

HRESULT camlidl_GetImplementationLanguage(struct camlidl_intf * pThis,
										  /*out*/ unsigned int *lang)
{
	 *lang = 1;
	 return S_OK;
}

HRESULT camlidl_GetFlags(struct camlidl_intf * pThis,
						 /*out*/ unsigned int *flags)
{
	 *flags = 0;
	 return S_OK;
}

HRESULT camlidl_GetClassIDNoAlloc(struct camlidl_intf * pThis,
								  /*out*/ IID *cid)
{
	 nsIClassInfo * factory = pThis->comp->factory;
	 assert(factory != NULL);
	 return factory->GetClassIDNoAlloc(cid);
}

/* Query a COM interface */
value camlidl_com_queryInterface(value vintf, value viid)
{
  void * res;
  HRESULT hr;
  interface IUnknown * intf =
    (interface IUnknown *) camlidl_unpack_interface(vintf, NULL);
  hr = intf->lpVtbl->QueryInterface(intf, &GUID_val(viid), &res);
  if (FAILED(hr))
    camlidl_error(hr, "Com.queryInterface", "Interface not available");
  return camlidl_pack_interface(res, NULL);
}

/* Combine the interfaces of two Caml components */

#define is_a_caml_interface(i) \
  ((void *) (((interface IUnknown *) i1)->lpVtbl->QueryInterface) == \
   (void *) camlidl_QueryInterface)

value camlidl_com_combine(value vintf1, value vintf2)
{
  struct camlidl_intf * i1, * i2;
  struct camlidl_component * c1, * c2, * c;
  int n, i;

  i1 = (camlidl_intf *) camlidl_unpack_interface(vintf1, NULL);
  i2 = (camlidl_intf *) camlidl_unpack_interface(vintf2, NULL);
  if (! is_a_caml_interface(i1) || ! is_a_caml_interface(i2))
    camlidl_error(CLASS_E_NOAGGREGATION, "Com.combine",
                  "Not a Caml interface");
  c1 = i1->comp;
  c2 = i2->comp;
  n = c1->numintfs + c2->numintfs;
  c = (struct camlidl_component *)
        stat_alloc(sizeof(struct camlidl_component) +
                   sizeof(struct camlidl_intf) * (n - 1));
  InterlockedIncrement(&camlidl_num_components);

  c->numintfs = n;
  c->refcount = 1;
  for (i = 0; i < c1->numintfs; i++)
    c->intf[i] = c1->intf[i];
  for (i = 0; i < c2->numintfs; i++)
    c->intf[c1->numintfs + i] = c2->intf[i];
  for (i = 0; i < n; i++) {
    register_global_root(&(c->intf[i].caml_object));
    c->intf[i].comp = c;
  }

  c->factory = c1->factory;
  c->classinfo_intf.vtbl = &camlidl_classinfo_vtbl;
  c->classinfo_intf.caml_object = c->intf[0].caml_object;
  c->classinfo_intf.iid = (nsIID *) &NS_GET_IID(nsIClassInfo);
  c->classinfo_intf.comp = c;


  return camlidl_pack_interface(c->intf + (i1 - c1->intf), NULL);
}

/* Create an instance of a component */

value camlidl_com_create_instance(value clsid, value iid)
{
  void * instance;
  HRESULT res;

  res = nsComponentManager::CreateInstance(GUID_val(clsid),
										   NULL,
										   GUID_val(iid),
										   &instance);
  if (FAILED(res)) camlidl_error(res, "Com.create_instance", NULL);
  return camlidl_pack_interface(instance, NULL);
}

/* Initialization, termination */

value camlidl_com_initialize(value unit)
{
#ifdef _WIN32
  OleInitialize(NULL);
#endif
  return Val_unit;
}

value camlidl_com_uninitialize(value unit)
{
#ifdef _WIN32
  OleUninitialize();
#endif
  return Val_unit;
}

/* Register a Caml component factory */

struct camlidl_comp * camlidl_registered_components = NULL;

value camlidl_com_register_factory(value compdata)
{
  struct camlidl_comp * c = (struct camlidl_comp *) stat_alloc(sizeof(struct camlidl_comp));
  c->compdata = compdata;
  register_global_root(&(c->compdata));
  c->next = camlidl_registered_components;
  camlidl_registered_components = c;
  return Val_unit;
}

/* Parse and allocate an UID */

value camlidl_com_parse_uid(value str)
{
  value res;
  int u1, u2, u3, u4, u5, u6, u7, u8, u9, u10, u11;

  if (string_length(str) != 36 ||
      sscanf(String_val(str),
             "%8x-%4x-%4x-%2x%2x-%2x%2x%2x%2x%2x%2x",
             &u1, &u2, &u3, &u4, &u5, &u6, &u7, &u8, &u9, &u10, &u11) != 11)
    camlidl_error(CO_E_IIDSTRING, "Com.clsid", "Badly formed GUID");
  res = alloc_small((sizeof(GUID) + sizeof(value) - 1) / sizeof(value),
                    Abstract_tag);
  GUID_val(res).m0 = u1;
  GUID_val(res).m1 = u2;
  GUID_val(res).m2 = u3;
  GUID_val(res).m3[0] = u4;
  GUID_val(res).m3[1] = u5;
  GUID_val(res).m3[2] = u6;
  GUID_val(res).m3[3] = u7;
  GUID_val(res).m3[4] = u8;
  GUID_val(res).m3[5] = u9;
  GUID_val(res).m3[6] = u10;
  GUID_val(res).m3[7] = u11;
  return res;
}

}
