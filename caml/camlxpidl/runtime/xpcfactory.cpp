
#include <string.h>

extern "C" {
#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/fail.h>
#include <caml/callback.h>
}

#include "camlxpidlruntime.h"
#include "comstuff.h"

#include <nsComponentManagerUtils.h>
#include <nsIFactory.h>
#include <nsIModule.h>
#include <nsIComponentRegistrar.h>
#include <nsMemory.h>

nsIClassInfo * registered_xpcfactory = NULL;

class camlComponentFactory : public nsIFactory, public nsIClassInfo
{
     struct camlidl_comp * comp;
	 long refcount;
public:
	 NS_DECL_ISUPPORTS

	 NS_IMETHOD GetInterfaces(PRUint32 * count, nsIID *** ifaces) {
		  return NS_ERROR_NOT_IMPLEMENTED;
	 }

	 NS_IMETHOD GetHelperForLanguage(PRUint32 language, nsISupports ** helper) {
		  *helper = NULL;
		  return NS_OK;
	 }

	 NS_IMETHOD GetContractID(char ** aContractID) {
		  *aContractID = strdup((char *)Field(comp->compdata, COMPDATA_VER_IND_PROG_ID));
		  return NS_OK;
	 }
	 
	 NS_IMETHOD GetClassDescription(char ** aClassDescription) {
		  *aClassDescription = strdup((char *)Field(comp->compdata, COMPDATA_FRIENDLY_NAME));
		  return NS_OK;
	 }

	 NS_IMETHOD GetClassID(nsCID ** aClassID) {
		  *aClassID = (nsCID *)nsMemory::Clone(&GUID_val(Field(comp->compdata, COMPDATA_CLSID)), 
											   sizeof(nsCID));
		  return NS_OK;
	 }

	 NS_IMETHOD GetClassIDNoAlloc(nsCID * aClassID) {
		  *aClassID = GUID_val(Field(comp->compdata, COMPDATA_CLSID));
		  return NS_OK;
	 }

	 NS_IMETHOD GetImplementationLanguage(PRUint32 * langp) {
		  return NS_ERROR_NOT_IMPLEMENTED;
	 }

	 NS_IMETHOD GetFlags(PRUint32 * flags) {
		  return NS_ERROR_NOT_IMPLEMENTED;
	 }

	 NS_IMETHOD CreateInstance(nsISupports * aOuter, const nsIID & nsid, void** object)
	 {
		  struct camlidl_ctx_struct ctx = { CAMLIDL_ADDREF, NULL };
		  // Aggregation is not supported yet
		  if (aOuter != NULL) return CLASS_E_NOAGGREGATION;
		  // Create the component
  		  registered_xpcfactory = this;
		  AddRef();
		  value vcomp =
			   callback(Field(comp->compdata, COMPDATA_CREATE), Val_unit);
		  IUnknown * comp = (IUnknown *) camlidl_unpack_interface(vcomp, &ctx);
		  registered_xpcfactory = NULL;
		  // Get the requested interface
		  HRESULT res = comp->lpVtbl->QueryInterface(comp, (IID *) &nsid, object);
		  // Release the initial pointer to the component
		  // (if QueryInterface failed, it will destroy itself)
		  comp->lpVtbl->Release(comp);
		  // Return result of QueryInterface
		  return res;
  }

  NS_IMETHOD LockFactory(PRBool lock)
  {
	   return NS_OK;
  }

  // Constructor
  camlComponentFactory(struct camlidl_comp * comp_init)
  {
    comp = comp_init;
  }

  camlComponentFactory() 
  {
	   NS_INIT_ISUPPORTS();
  }
	 
  virtual ~camlComponentFactory() {}

};

NS_IMPL_ISUPPORTS1(camlComponentFactory, nsIFactory);

class nsCamlModule : public nsIModule
{
	 NS_DECL_ISUPPORTS

	 NS_IMETHOD GetClassObject(nsIComponentManager * compMgr, const nsCID & cid, 
							 const nsIID & iid, void ** result)
	 {
		  struct camlidl_comp * c;
		  for (c = camlidl_registered_components; c != NULL; c = c->next) {
			   if (cid.Equals(GUID_val(Field(c->compdata, COMPDATA_CLSID)))) {
					// Create class factory
					camlComponentFactory * f = new camlComponentFactory(c);
					if (f == NULL) return NS_ERROR_OUT_OF_MEMORY;
					// Get requested interface
					nsresult res = f->QueryInterface(iid, result);
					// Return result of QueryInterface
					return res;
			   }
		  }
		  *result = NULL;
		  return NS_ERROR_FACTORY_NOT_REGISTERED;
		  // create an nsIFactory for cid
	 }

	 NS_IMETHOD RegisterSelf(nsIComponentManager * compMgr, nsIFile * aPath,
							 const char * registryLocation, const char * componentType)
	 {
		  struct camlidl_comp * c;
		  for (c = camlidl_registered_components; c != NULL; c = c->next) {
			   nsresult rv = NS_ERROR_FAILURE;
			   nsCOMPtr<nsIComponentRegistrar> registrar = do_QueryInterface(compMgr);
			   if (registrar) {
					nsCID comp_cid = GUID_val(Field(c->compdata, COMPDATA_CLSID));
					char * friendly_name = strdup((char *)Field(c->compdata, COMPDATA_FRIENDLY_NAME));
					char * contract_id = strdup((char *)Field(c->compdata, COMPDATA_VER_IND_PROG_ID));
					rv = registrar->RegisterFactoryLocation(
						 comp_cid,
						 friendly_name,
						 contract_id,
						 aPath,
						 registryLocation,
						 componentType);
			   }
			   if (NS_FAILED(rv)) {
					break;
			   }
		  }

		  return NS_OK;
	 }

	 NS_IMETHOD UnregisterSelf(nsIComponentManager * compMgr,
							 nsIFile * location, const char * type)
	 {
		  return NS_OK;
	 }

	 NS_IMETHOD CanUnload(nsIComponentManager * compManager, PRBool * unload) 
	 {
		  *unload = PR_FALSE;
		  return NS_OK;
	 }
	 
	 nsCamlModule()
	 {
		  NS_INIT_ISUPPORTS();
	 }

	 virtual ~nsCamlModule() {}
};

NS_IMPL_ISUPPORTS1(nsCamlModule, nsIModule);


extern "C" NS_EXPORT nsresult
NSGetModule(nsIComponentManager *servMgr, nsIFile * location, nsIModule ** result) 
{
	 char * argv[1] = {NULL};

	 caml_startup(argv);

	 // create a new nsIModule
	 *result = new nsCamlModule();
	 NS_ADDREF(*result);
	 return NS_OK;
}

nsIID IID_IUnknown = NS_ISUPPORTS_IID;
