open ISashObjects
open ISashFileSystem
open SashFileCommon
open SashFileUtils

class sashFolder = 
object
  inherit sashFileCommon as common
  
  method copy dest (v : nsIVariant) =
	copy_folder (common#path) dest

  method delete (v : nsIVariant) =
	delete_folder (common#path)

  method move dest (v : nsIVariant) =
	move_folder (common#path) dest

  method getType = Some "folder"
  method getIsFile = false
  method getIsFolder = true

  method getSize = folder_size (common#path)

  method getFiles = 0 (* make nsIVariant *)
  method getIsRootFolder = true
  method getSubFolders = 0 (* make nsIVariant *)

end
