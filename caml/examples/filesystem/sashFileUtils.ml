let id x = x

let trimLast s c =
  match String.length s with
	  0|1 -> s
	| n -> if s.[n-1] = c then 
		     String.sub s 0 (n-1)
	      else s

let splitFile path =
  (trimLast (Filename.dirname path) '/', Filename.basename path)

let getAbsolutePath path = 
  if Filename.is_relative path then path
  else (Unix.getcwd()) ^ "/" ^ path

let copy_file source dest = 
  try
	let in_file = open_in source in
	let out_file = open_out dest in
	let bufsize = 4096 in
	let buf = String.create bufsize in
	let rec do_copy() =
	  let len = input in_file buf 0 bufsize in
		if len > 0 then
		  output out_file buf 0 len; do_copy()
	in
	  do_copy();
	  close_in in_file;
	  close_out out_file;
	  true
  with _ -> false

let delete_file file =
  Unix.unlink file

let move_file source dest = 
	try
	  Unix.rename source dest
	with (Unix.Unix_error _) ->
	  if copy_file source dest then delete_file source

let file_size file = (Unix.stat file).Unix.st_size

let check_stat file value = 
  try
	(Unix.stat file).Unix.st_kind = value
  with (Unix.Unix_error _) -> false

let dir_entries dirname =
  try
	let dir = Unix.opendir dirname in
	let rec next_entry() = 
	  try 
		let entry = Unix.readdir dir in
		  if entry != "." && entry != ".." then 
			entry::next_entry()
		  else next_entry()
	  with End_of_file -> []
	in
	let entries = next_entry() in
	  Unix.closedir dir; entries
  with (Unix.Unix_error _) -> []

let rec copy_folder source dest =
  let do_copy source dest = 
	match (Unix.stat source).Unix.st_kind with
		Unix.S_REG -> copy_file source dest
	  | Unix.S_DIR -> copy_folder source dest
	  | _ ->	true
  in
  let entries = dir_entries source in
	try
	  let _ = if not(check_stat dest Unix.S_DIR) then 
		Unix.mkdir dest 438
	  in
	  let copy_result = List.map (fun s -> do_copy (source ^ s) (dest ^ s)) entries in
		List.for_all id copy_result
	with _ -> false

let rec delete_folder folder = 
  let do_delete name = 
	match (Unix.stat name).Unix.st_kind with
		Unix.S_REG -> Unix.unlink (folder ^ name)
	  | Unix.S_DIR -> delete_folder (folder ^ name)
	  | _ -> ()
  in
  let entries = dir_entries folder in
	List.iter do_delete entries;
	Unix.unlink folder

let move_folder source dest =
  try 
	Unix.rename source dest
  with (Unix.Unix_error _) ->
	if copy_folder source dest then delete_folder source

let rec folder_size folder =
  let entry_size name sum = match (Unix.stat name).Unix.st_kind with
		Unix.S_REG -> sum + (file_size name)
	  | Unix.S_DIR -> sum + (folder_size (folder ^ name))
	  | _ -> sum
  in
  let entries = dir_entries folder in
	List.fold_right entry_size entries 0
