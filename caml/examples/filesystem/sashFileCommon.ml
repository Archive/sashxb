open ISashObjects
open ISashFileSystem

let path_name (s : string) (fs : sashIFileSystem_class) =
  if Filename.is_relative s then
	match fs#getWeblicationDirectory with
		Some webdir -> (webdir ^ "/" ^ s, s)
	  | None -> raise (Failure "No weblication directory found")
  else
	(Filename.dirname s, Filename.basename s)

class sashFileCommon =
object (self)

  val mutable m_fs = None
  val mutable m_name = ""
  val mutable m_path = ""

  method path = m_path

  method initFileCommon 
	(act : iSashActionRuntime Xpcom.interface option)
	(cx : nativeJSContext) fs_iface fileName = 
	let fs = use_sashIFileSystem fs_iface in
	  m_fs <- Some fs;
	  self#setName fileName

  method getDateCreated = self#getDateLastModified

  method getDateLastAccessed = Some ""
  method getDateLastModified = Some ""

  method getDrive = "/"

  method getName = Some m_name

  method setName fileName =
	match m_fs with
		Some fs ->
		  let (path, name) = path_name fileName fs in
			m_path <- path;
			m_name <- name;
			fs#assertFSAccess m_path
	  | None -> raise (Xpcom.Error (0, "sashFileCommon", "No FileSystem object"))

  method getParentFolder = (None : sashIFolder Xpcom.interface option)

  method getPath = Some m_path

  method getShortName = Some m_name

  method getShortPath = Some m_path

  method getIsLocal = false

end
