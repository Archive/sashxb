open ISashFileSystem
open SashFileSystem
open SashFile
open SashFolder
open ISashObjects

let make_folder() = 
  make_sashIFolder (new sashFolder)

let make_file() = 
  make_sashIFile (new sashFile)

let make_filesystem() =
  let obj = new sashFileSystem in
  let ifs = make_sashIFileSystem obj in
  let iext = make_sashIExtension obj in
	Xpcom.combine ifs iext

let register factory clsid contract_id name = 
  Xpcom.register_factory
    { Xpcom.create = factory;
      Xpcom.clsid = Xpcom.clsid clsid;
      Xpcom.friendly_name = name;
      Xpcom.ver_ind_prog_id = contract_id;
      Xpcom.prog_id = contract_id ^ ";1" }
	
let _ = register make_folder "95f7f2a2-37e9-467a-93-f7-a4-74-d2-84-84-d0"
		  "@gnome.org/SashXB/filesystem/folder" "Sash Folder"
		  
let _ = register make_file "5F7997C1-1C65-4894-9B-B2-10-A7-81-28-F2-B0"
		  "@gnome.org/SashXB/filesystem/file" "Sash File"
		  
let _ = register make_folder "7B9943C4-54C0-4F96-A4-54-71-12-08-54-EC-AE"
		  "@gnome.org/SashXB/filesystem/filesystem" "Sash FileSystem"
