open ISashObjects
open ISashFileSystem
open SashFileUtils

class sashFileSystem =
object

  val mutable m_rt = None

  method getSashName = "Filesystem"

  method initialize (rt : iSashActionRuntime Xpcom.interface option)
	xml guid (cx : nativeJSContext) (obj : nativeJSObject) =
	m_rt <- rt;
	Printf.printf "Sash filesystem initialization:\n\txml=%s\n\tguid=%s\n" xml guid

  method cleanup = ()
	
  method processEvent = ()

  method assertFSAccess (path : string) = ()

  method getWeblicationDirectory = Some "blah"

  method buildPath path name =
	if path.[(String.length path) - 1] = '/' then 
	  Some (path ^ name)
	else Some (path ^ "/" ^ name)

  method moveFile source dest =
	move_file source dest

  method copyFile source dest (v : nsIVariant) =
	ignore(copy_file source dest)

  method deleteFile file (v : nsIVariant) = 
	delete_file file

  method moveFolder source dest =
	move_folder source dest

  method copyFolder source dest (v : nsIVariant) = 
	copy_folder source dest

  method deleteFolder folder (v : nsIVariant) = 
	delete_folder folder

  method fileExists file = 
	check_stat file Unix.S_REG

  method folderExists folder = 
	check_stat folder Unix.S_DIR

  method getAbsolutePathName path =
	Some (getAbsolutePath path)

  method getBaseName path =
	let (file, _) = splitFile path in Some file

  method getExtensionName path = 
	let (_, ext) = splitFile path in Some ext

  method getFileName path = 
	let (file, ext) = splitFile path in Some (file ^ ext)

  method getParentFolderName path =
  let abspath = trimLast (getAbsolutePath path) '/' in
	try
	  let i1 = String.rindex abspath '/' in
	  let i2 = String.rindex_from abspath i1 '/' in
		Some (String.sub abspath i1 (i2 - i1))
	with Not_found -> Some "/"

  method getTempName =
	Some (Filename.temp_file "sashXXXXXX" "")

  method isSymlink path =
	try
	  let path' = trimLast path '/' in
		(Unix.lstat path').Unix.st_kind = Unix.S_LNK
	with (Unix.Unix_error _) -> false

end
