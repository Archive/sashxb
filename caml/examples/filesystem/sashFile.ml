open ISashObjects
open ISashFileSystem
open SashFileCommon
open SashFileUtils

class sashFile = 
object
  inherit sashFileCommon as common
  
  method copy dest (v : nsIVariant) =
	copy_file (common#path) dest

  method delete (v : nsIVariant) =
	delete_file (common#path)

  method move dest (v : nsIVariant) =
	move_file (common#path) dest

  method getType = Some "file"
  method getIsFile = true
  method getIsFolder = false

  method getSize = file_size (common#path)
end
