type sashVariant

val variantGetString : sashVariant -> string
val make_sashVariant : ISashVariant.nsIVariant Xpcom.interface option -> sashVariant
