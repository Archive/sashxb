open ISashVariant
open Xpcom

type sashVariant = Empty | Bool of bool | Integer of int | Float of float | 
  String of string | Interface of iUnknown interface | Array of sashVariant array

let rec variantGetString v =
  match v with
	  Empty -> ""
	| Bool b -> string_of_bool b
	| Integer i -> string_of_int i
	| Float f -> string_of_float f
	| String s -> s
	| Interface _ -> "[XPCom interface]"
	| Array a -> Array.fold_left (fun s v -> s ^ ", " ^ (variantGetString v)) "" a

let rec make_sashVariant v_opt = 
  match v_opt with
	  None -> Empty
	| Some v_iface -> 
		let v = use_nsIVariant v_iface in
		let typ = v#getDataType in
		  (* Any of the string types *)
		  if typ = vTYPE_ASTRING or typ = vTYPE_CSTRING or typ = vTYPE_UTF8STRING or 
			 typ = vTYPE_WSTRING_SIZE_IS or typ = vTYPE_STRING_SIZE_IS or 
			 typ = vTYPE_CHAR_STR or typ = vTYPE_WCHAR_STR or typ = vTYPE_DOMSTRING then
			   match v#getAsString with
				   None -> Empty
				 | Some s -> String s

		  (* Boolean type *)
		  else if typ = vTYPE_BOOL then Bool (v#getAsBool)

		  (* Integer types *)
		  else if typ = vTYPE_INT8 or typ = vTYPE_INT16 or typ = vTYPE_INT32 or 
			typ = vTYPE_INT64 or typ = vTYPE_UINT8 or typ = vTYPE_UINT16 or 
			typ = vTYPE_UINT32 or typ = vTYPE_UINT64 or typ = vTYPE_CHAR or 
			typ = vTYPE_WCHAR then Integer (v#getAsInt32)
			  
		  (* Float types *)
		  else if typ = vTYPE_DOUBLE or typ = vTYPE_FLOAT then Float (v#getAsDouble)

		  (* Interface types *)
		  else if typ = vTYPE_INTERFACE or typ = vTYPE_INTERFACE_IS then
			match v#getAsISupports with
				None -> Empty
			  | Some iface -> Interface iface

		  (* Arrays *)
		  else if typ = vTYPE_ARRAY then
			let (items, _) = sashVariantGetArray v_opt in
			  Array (Array.map (fun v -> make_sashVariant v) items)

		  (* Any other type *)
		  else Empty
