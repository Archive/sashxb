/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): AJ Shankar

Reads wdf, does all necessary backend install steps

*****************************************************************/

// Installer.cpp
// handles installation of Weblication files

#include "Installer.h"
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <iomanip>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <strstream>
#include <errno.h>
#include <gnome.h>
#include "sashtools.h"
#include "wdf.h"
#include "GraphicalInstaller.h"
#include "CacheManager.h"
#include "InstallationManager.h"
#include "InstallDesktop.h"
#include "SashNet.h"
#include "SecurityHash.h"
#include "LocatorClient.h"
#include "WeblicationRegister.h"
#include "SashGUID.h"
#include "sash_version.h"

using namespace std;

int Installer::s_OverwriteRegistry = -1;
int Installer::s_InstallOverPrevious = -1;
int Installer::s_TextInstall = 0;
int Installer::s_ForceInstall = 0;
// number of major steps in an install that occur *after* the user install screens,
// not including the file installation
// Currently only create directories and create registry
// should be less confusing than this...
const int Installer::NUM_STEPS = 2;

Installer::Installer(const string& location_or_guid, const Version w) : 
	 m_InstallingOverPrevious(true),
	 m_RequiredVersion(w),
	 m_WDFLocation(location_or_guid),
	 m_pWDF(NULL), 
	 m_pGraphicalInstaller(NULL),
	 m_FromWeb(false),
	 m_pSecurity(NULL),
	 m_HasAborted(false) {
	 DEBUGMSG(installer, "Constructor with param %s\n", location_or_guid.c_str());
	 m_pLocator = new LocatorClient();
	 m_pDownloader = NULL;
}

Installer::~Installer() {
	 DEBUGMSG(installer, "Shutting down graphical installer...\n");
	 if (m_pGraphicalInstaller != NULL)
		  delete m_pGraphicalInstaller;  
	 DEBUGMSG(installer, "Deleting WDF...\n");
	 if (m_pWDF)
		  delete m_pWDF;  
	 if (m_pSecurity) 
	   delete m_pSecurity;
	 if (m_pLocator)
		  delete m_pLocator;
	 DEBUGMSG(installer, "Done destroying installer\n");
}

InstallResult Installer::Install() {
	 DEBUGMSG(installer, "Starting installation from %s ...\n", 
			  m_IM.GetSashInstallDirectory().c_str());
	 GetMozillaVersion();

	 // if we want to do a quick install, don't display any messages with gtk;
	 // use terminal instead
	 SashErrorPushMode(s_TextInstall || s_ForceInstall ? SASH_ERROR_TEXT : SASH_ERROR_GNOME);
  
	 // first, download the WDF file itself
	 if (! GetWDF()) return Failed();

	 // check to see if the weblication is dependent on any others that are not installed
	 if (! CheckDependencies()) return Failed();

	 if (s_TextInstall || s_ForceInstall) {
		  m_pGraphicalInstaller = new DummyGraphicalInstaller;
	 } else {
		  m_pGraphicalInstaller = new GraphicalInstaller(m_pWDF, m_pSecurity, 
														 m_BaseLocation, m_FromWeb, 
														 NUM_STEPS + m_Files.size());
		  // graphical installer -- license, etc.
		  if (! UserInstallScreens()) return Failed();
	 }

	 // creates weblication directories
	 if (! CreateDirectories()) return Failed();
    
	 // installs local files (cache, etc)
	 if (!InstallFiles()) return Failed();
  
	 // create the registry and insert initial information
	 // also creates the security registry
	 if (!CreateRegistry()) return Failed();
  
	 // add weblication icon to start menu
	 if (!AddGNOMEIcon()) return Failed();
  
	 // register the installation
	 DEBUGMSG(installer, "FINAL TITLE IS %s\n", m_pWDF->GetTitle().c_str());

	 // add stuff to the security hash and save it back to disk
	 if (m_pWDF->GetExtensionType() != SET_WEBLICATION) {
		  DEBUGMSG(installer, "Saving extension security settings to global security file...\n");
		  m_pSecurity->Save(m_IM.GetSashInstallDirectory() + "/" + GlobalSecurityFileName);
	 }

	 // have the installation manager store the details of the installation
	 m_IM.RegisterInstallation(m_pWDF, MakeRelativePathAbsolute(m_WDFLocation), m_UninstallFiles);

	 // finally, try letting the task manager know about the install
	 WeblicationRegister w;
	 w.webl_name = "installer-refresh";
	 w.registerMe();
	 
	 m_pGraphicalInstaller->Finish(true);

	 SashErrorPopMode();

	 return IR_SUCCEEDED;
} 

bool Installer::Abort() {
	 m_HasAborted = true;
	 if (m_pGraphicalInstaller) {
		  m_pGraphicalInstaller->Cancel();
		  return true;
	 } else {
		  return false;
	 }
}

InstallResult Installer::Failed() {
	 if (! m_InstallingOverPrevious && ! m_InstallDirectory.empty()) {
//		  FileSystem::DeleteFolder(m_InstallDirectory);
	 }

	 if (m_pGraphicalInstaller && ! m_HasAborted)
		  m_pGraphicalInstaller->Finish(false, m_Error);

	 SashErrorPopMode();
	 return m_Result;
} 

bool Installer::GetWDF() {
	 m_Result = IR_CANT_OPEN_WDF;
	 string my_wdf_loc;

	 // constructor param can be one of four things:
	 // filename | url | guid | name
	 // if it's a guid, see if we can find out where to get it
	 if (URLHasProtocol(m_WDFLocation)) {
		  // fetch from web, so nothing to do
	 } else if (FileSystem::FileExists(m_WDFLocation)) {
		  // simply open it up, so nothing to do
	 } else {
		  bool found = false;
		  DEBUGMSG(installer, "Param is guid or name; trying to find original WDF location\n");
		  if (VerifyGUID(m_WDFLocation)) {
			   if (m_IM.IsInstalled(m_WDFLocation)) {
					SashExtensionItem sei = m_IM.GetInfo(m_WDFLocation);
					if (! sei.wdf_location.empty()) {
						 m_WDFLocation = sei.wdf_location;
						 found = true;
					}
			   }
		  } 
		  if (! found) {
			   m_WDFLocation = m_pLocator->GetURL(m_WDFLocation, m_IM);
			   if (LocatorClient::IsError(m_WDFLocation)) {
					m_Error = "Locator failure -- " + m_WDFLocation;
					return false;
			   }
		  }
	 }

	 DEBUGMSG(installer, "Opening WDF file %s\n", m_WDFLocation.c_str());

//	 m_pGraphicalInstaller->SetStatusText("Analyzing installation information...");
    
	 // note where the WDF came from so we can get the rest of the files from there too
	 m_BaseLocation = URLGetDirectory(m_WDFLocation);
	 if ((m_BaseLocation.empty() || m_BaseLocation[0] != '/') && ! URLHasProtocol(m_WDFLocation)) {
		  char dir[PATH_MAX+1];
		  if (getcwd(dir, PATH_MAX)) {
			   m_BaseLocation = string(dir) + (m_BaseLocation.empty() ? "" : "/" + m_BaseLocation);
		  }
	 }
	 DEBUGMSG(installer, "Base location is %s\n", m_BaseLocation.c_str());

	 if (URLHasProtocol(m_WDFLocation)) {
		  DEBUGMSG(installer, "GetWDF: downloading WDF from URL %s...\n", m_WDFLocation.c_str());
		  my_wdf_loc = "/tmp/" + URLGetFileName(m_WDFLocation);
		  FileSystem::DeleteFile(my_wdf_loc);

		  nsCOMPtr<sashIDownloader> d;
		  NewSashFileDownloader(m_WDFLocation.c_str(), my_wdf_loc.c_str(), false, getter_AddRefs(d));
		  d->StartTransfer();
		  PRInt32 status;
		  d->ProcessUntilComplete(&status);
		  if (status != sashINet::NET_DONE) {
			   m_Error = "Error downloading WDF file!";
			   return false;
		  }

		  m_FromWeb = true;
	 } else {
		  my_wdf_loc = m_WDFLocation;
	 }

	 m_pWDF = new WDF(); 
	 DEBUGMSG(installer, "Trying to open local WDF from %s\n", my_wdf_loc.c_str());
	 if (! m_pWDF->OpenAmbiguousFile(my_wdf_loc)) {
		  m_Error = "Error opening WDF file!"; 
		  return false;
	 }
  
	 DEBUGMSG(installer, "WDF successfully initialized\n");

	 // delete the temporary WDF file if downloaded from the web
	 if (m_FromWeb)
		  FileSystem::DeleteFile(my_wdf_loc);

	 // get the guid
	 m_GUID = m_pWDF->GetID();
	 if (! VerifyGUID(m_GUID)) {
		  m_Error = "Sorry, the component has a corrupted or incorrect GUID.";
		  return false;
	 }

	 // make sure this weblication supports this platform
	 if (! WDFHasPlatform()) {
		  m_Error = "Sorry, the component does not support this platform!";
		  return false;
	 }

	 DEBUGMSG(installer, "Checking against minimum version %s\n", 
			  m_RequiredVersion.ToString().c_str());
	 if (! s_ForceInstall && m_RequiredVersion > m_pWDF->GetVersion().weblication) {
		  m_Result = IR_NOT_NEWER_VERSION;
		  m_Error = "This version of the component is not high enough to install.";
		  return false;
	 }

	 // see if version of Sash is good enough
	 WDFVersion versions = m_pWDF->GetVersion();
	 if (versions.sash.minimum > SASH_VERSION) {
		  m_Result = IR_NOT_NEWER_VERSION;
		  m_Error = "This component requires SashXB v" + versions.sash.minimum.ToString() +
			   "; the currently\ninstalled version is " + SASH_VERSION + ". Please"
			   " consider upgrading SashXB.";
		  return false;
	 }

	 // now check mozilla
	 Version moz = GetMozillaVersion();
	 if (versions.mozilla.minimum > moz) {
		  m_Result = IR_NOT_NEWER_VERSION;
		  m_Error = "This component requires Mozilla v" + versions.mozilla.minimum.ToString() +
			   "; the currently\ninstalled version is " + moz.ToString() + ". Please"
			   " consider upgrading Mozilla.";
		  return false;
	 }

	 // see if this weblication has been installed before
	 if (m_IM.IsInstalled(m_GUID)) {
		  if (s_InstallOverPrevious < 1) {
			   SashExtensionItem sei = m_IM.GetInfo(m_GUID);
			   SashExtensionType s = m_pWDF->GetExtensionType();
			   string descript = (s == SET_WEBLICATION ? "weblication" : 
								  (s == SET_LOCATION ? "location" : "extension"));
			   string old_version = sei.version;
			   Version curr_version = versions.weblication;
			   // always install over unless version is same or lower
               // but if forcing install, do it anyway
			   if (! (curr_version > old_version) && ! s_ForceInstall) {
					string query;
					if (curr_version == old_version) {
						 query = "The same version of the " + descript + " '" 
							  + m_pWDF->GetTitle() + "', v" + old_version +
							  ",\nhas already been installed on this system. Install over it?";
					} else {
						 query = "A newer version of the '"+ m_pWDF->GetTitle() + "' " 
							  + descript + ", v" + old_version + 
							  ",\nis already installed on this system. "
							  "Install v" + curr_version.ToString() + " over it?";
					}
					// otherwise, see if user had a command line preference
					// and failing that, ask the user 
					if (s_InstallOverPrevious == 0 || ! OutputQuery(query.c_str())) {
						 m_Result = IR_NOT_NEWER_VERSION;
						 m_Error = "User declined to install over previous installation.";
						 return false;
					}
			   }
		  }
	 } else {
  		  m_InstallingOverPrevious = false;
	 }


	 if (m_pWDF->GetExtensionType() == SET_UNDEFINED) {
		  m_Result = IR_UNRECOGNIZABLE_TYPE;
		  m_Error = "Component to be installed is not of any recognizable type!";
		  return false;
	 }
	 // now that we know the guid, set the install directory
	 m_InstallDirectory = m_IM.GetSashInstallDirectory() + "/" + m_GUID;
	 m_Files = m_pWDF->GetFiles();
  
//	 m_pGraphicalInstaller->AdvanceProgressBar();
  
	 DEBUGMSG(installer, "WDF title is %s\n", m_pWDF->GetTitle().c_str());
	 DEBUGMSG(installer, "WDF author is %s\n", m_pWDF->GetAuthor().c_str());
	 DEBUGMSG(installer, "[SUCCEEDED]\n");
	 return true;
}

bool not_good_enough(WDFDependency w, InstallationManager* IM) {
	 SashExtensionItem sei = IM->GetInfo(w.id);
	 if (! IM->IsInstalled(w.id) || w.required_version > sei.version) {
		  DEBUGMSG(installer, "Dependency %s not installed or not high enough version...\n",
				   w.id.c_str());
		  return false;
	 }
	 return true;
}

bool Installer::CheckDependencies() {
	 DEBUGMSG(installer, "Checking for dependencies for %s...\n", m_WDFLocation.c_str());
	 
//	 m_pGraphicalInstaller->SetStatusText("Checking weblication's dependencies...");
	 vector<WDFDependency> dc = m_pWDF->GetDependencies();
	 dc.erase(remove_if(dc.begin(), dc.end(), bind2nd(ptr_fun(not_good_enough), &m_IM)), dc.end());

	 if (! dc.empty()) {
		  DEBUGMSG(installer, "Found %d uninstalled dependencies\n", dc.size());
		  string query = "The component you are trying to install, " + m_pWDF->GetTitle() + 
			   ",\nrequires the following Sash components that are\nnot present or "
			   "up-to-date on your system: \n";
		  vector<WDFDependency>::iterator bi = dc.begin(), ei = dc.end();
		  while (bi != ei) {
			   query += bi->title + " v" + bi->required_version.ToString() + "\n";
			   ++bi;
		  }
		  query += "\nWould you like to install them now?";
		  m_Result = IR_FAILED_COMPONENT_INSTALLATION;
		  if (s_ForceInstall || OutputQuery(query.c_str())) {
			   vector<WDFDependency>::iterator bi = dc.begin(), ei = dc.end();
			   while (bi != ei) {
					DEBUGMSG(installer, "Attempting to install dependency at %s\n", bi->id.c_str());
					if (m_IM.IsInstalled(bi->id)) {
						 DEBUGMSG(installer, "Already installed by some other dependency; skipping\n");
						 ++bi;
						 continue;
					}
					string p1;
					// see if it has a file location
					if (! bi->filename.empty()) {
						 p1 = MakeRelativePathAbsolute(bi->filename);
					} else {
						 p1 = bi->id;
					}
					Installer i(p1, bi->required_version);
					if (i.Install() != IR_SUCCEEDED) {
						 m_Error = string("Failed installation of required component\n") + 
							  bi->title + " (" + bi->id + "):\n" + i.m_Error;
						 return false;
					}
					DEBUGMSG(installer, "Dependency installation successful!\n");
					// make sure to reload the global registry,
					// since almost certainly changes have been made
					m_IM.ReloadGlobalRegistry();
					++bi;
			   }
		  } else {
			   m_Error = "User declined to install required Sash components";
			   return false;
		  }
	 }

	 // read in the security hash from the global hash
	 // and the wdf, now that we have all dependencies
	 DEBUGMSG(installer, "Reading global security hash\n");
	 // if the component to be installed is a weblication,
	 // get rid of extraneous security settings
	 vector<string> a;
	 if (m_pWDF->GetExtensionType() == SET_WEBLICATION) {
		  // first, get all extensions and locations that have
		  // security settings this weblication cares about
		  vector<WDFDependency> b = m_pWDF->GetDependencies();
		  vector<WDFDependency>::iterator ai = b.begin(), bi = b.end();
		  while (ai != bi) {
			   a.push_back(ai->id);
			   ++ai;
		  }
		  m_IM.GetAllGivenDependencies(a, false /* want locations, too */);
	 }
	 m_pSecurity = new SecurityHash(m_IM.GetSashInstallDirectory() + "/" + GlobalSecurityFileName, a);
	 m_pWDF->GetSecurityItems(*m_pSecurity);
	 
//	 m_pGraphicalInstaller->AdvanceProgressBar();
	 DEBUGMSG(installer, "Finished installing all necessary dependencies\n");
	 return true;
}

bool Installer::UserInstallScreens() {

	 DEBUGMSG(installer, "Starting Graphical Installer\n");
	 m_pGraphicalInstaller->SetStatusText("Verifying weblication settings with user...");

	 vector<WDFInstallScreen> InstallScreens = m_pWDF->GetInstallScreens();
	 m_pGraphicalInstaller->AddScreens(InstallScreens);

	 DEBUGMSG(installer, "install screens added\n");
  
	 // returns false if user hits 'Cancel'
	 if (!m_pGraphicalInstaller->Begin()) {
		  DEBUGMSG(installer, "[FAILED]\n");
		  m_Result = IR_USER_CANCELED;
		  m_Error = "Install interrupted by user";
		  return false;
	 } 
  
	 // don't need to advance; each individual screen will do it
  
	 DEBUGMSG(installer, "[SUCCEEDED]\n");
	 return true;
}


bool Installer::CreateDirectories() {
  	 DEBUGMSG(installer, "Creating weblication directories...\n");
  
	 m_pGraphicalInstaller->SetStatusText("Creating weblication directory structure...");
  
	 m_Result = IR_CANT_CREATE_DIRECTORIES;
	 // weblication root directory
	 if (! FileSystem::CreateFolder(m_InstallDirectory) && ! FileSystem::FolderExists(m_InstallDirectory)) {
		  m_Error = "Creation of directory " + m_InstallDirectory + " failed!";
		  return false;
	 }

	 // cache directory
	 string dir = CacheDirectory(m_InstallDirectory);
	 if (! FileSystem::CreateFolder(dir) && ! FileSystem::FolderExists(dir)) {
		  m_Error = "Creation of cache directory failed!";
		  return false;
	 }
    
	 // data directory
	 dir = DataDirectory(m_InstallDirectory);
	 if (! FileSystem::CreateFolder(dir) && ! FileSystem::FolderExists(dir)) {
		  m_Error = "Creation of data directory failed!";
		  return false;
	 }
    
	 m_pGraphicalInstaller->AdvanceProgressBar();
  
	 DEBUGMSG(installer, "Installer: [SUCCEEDED]\n");
	 return true;
}

bool Installer::InstallFiles() {
  
	 DEBUGMSG(installer, "Installing files to '%s/'...\n", m_InstallDirectory.c_str());
  
	 m_pGraphicalInstaller->SetStatusText("Installing files...");
  
	 CacheManager cache(CacheDirectory(m_InstallDirectory), true);
    
	 for (unsigned int i = 0 ; i < m_Files.size() ; i++) {
 		  ostrstream ost;
		  // let the user know how far we've gotten
		  ost << "Downloading files (" << i+1 << " of " << m_Files.size() << ")..." << '\0';
		  m_pGraphicalInstaller->SetStatusText(ost.str());
      
		  const WDFFile& curr_file = m_Files[i];
      
		  DEBUGMSG(installer, "Copying file %s to location %d\n", 
				   curr_file.filename.c_str(),
				   curr_file.location);
      
		  string name = curr_file.filename;

		  // if the files are relative to the WDF, add the absolute URL
		  name = MakeRelativePathAbsolute(name);

		  m_Result = IR_CANT_GET_FILE;

		  if (curr_file.location == FLT_CACHE) {
			   if (curr_file.precache)
					cache.Populate(name);
			   else 
					cache.Add(name);
		  } else {
			   string target = URLGetFileName(name), f_target;
	  
			   // if the file is in the data directory, make sure to clean up after it
			   f_target = DataDirectory(m_InstallDirectory) + "/" + target;
		   
			   DEBUGMSG(installer, "Final file location: %s to %s\n", name.c_str(), 
						f_target.c_str());
			   if (m_FromWeb || URLGetProtocol(name) == "http") {
					DEBUGMSG(installer, "Fetching file %s from the web\n", name.c_str());
					string zoo = ost.str();
					sashINetCallback * dc = new InstallerDownloadCallback(zoo, 
											! (s_TextInstall || s_ForceInstall), m_pGraphicalInstaller);

					NewSashFileDownloader(name.c_str(), f_target.c_str(), false, 
										  getter_AddRefs(m_pDownloader));
					m_pGraphicalInstaller->Reset();
					m_pDownloader->SetStatusCallback(dc);
					m_pDownloader->StartTransfer();
					PRInt32 status;
					do {
						 if (m_pGraphicalInstaller->Update()) {
							  // user hit cancel
							  DEBUGMSG(installer, "User hit cancel; aborting download\n");
							  m_pDownloader->Abort();
						 }
						 m_pDownloader->Process(&status);
						 usleep(1);

					} while (status == sashINet::NET_TRANSFERRING);
					
					m_pDownloader = NULL;
					if (status == sashINet::NET_ERROR) {
						 m_Error = string("Error downloading file ") + target;
						 return false;
					}
					if (status == sashINet::NET_ABORTED) {
						 m_Error = string("User aborted downloading file ") + target;
						 return false;
					}
			   } else {
					if (!FileSystem::CopyFile(name, f_target, true)) {
						 m_Error = string("Error copying file ") + target;
						 return false;
					}
			   }
			   // see whether it needs uncompressing
			   if (! UncompressFile(DataDirectory(m_InstallDirectory), target)) 
					return false;
		  }
		  m_pGraphicalInstaller->AdvanceProgressBar();
	 }

	 DEBUGMSG(installer, "[SUCCEEDED]\n");
	 return true;
}

// why do computer people say 'uncompress' instead of 'decompress'?
bool Installer::UncompressFile(const string& dir, const string& file) {
	 // the ordering of this list is actually important
	 static char* comp[][3] = {{".tar.gz", "tar", "xfz"},
							   {".tar.bz2", "tar", "xfj"},
							   {".tar.bz", "tar", "xfj"},
							   {".bz", "bunzip2", "-fq"},
							   {".bz2", "bunzip2", "-fq"},
							   {".tbz2", "tar", "xfj"},
							   {".tbz", "tar", "xfj"},
							   {".tgz", "tar", "xfz"},
							   {".tar", "tar", "xf"},
							   {".gz", "gunzip", "-fq"},
							   {".zip", "unzip", "-oq"},
							   {0, 0}};

	 int len = file.length(), i = 0;
	 bool success = true;
	 while (comp[i][0] != 0) {
		  unsigned int pos = len - strlen(comp[i][0]);
		  if (strlen(comp[i][0]) <= file.length() && file.rfind(comp[i][0]) == pos) {
			   ostrstream ost;
			   ost << "Uncompressing file... [" << FileSystem::FileSize(dir + "/" + file) << 
					" bytes]" << '\0';
			   m_pGraphicalInstaller->SetStatusText(ost.str());
			   DEBUGMSG(installer, "Uncompressing %s file with command line '%s %s %s'\n",
						comp[i][0], comp[i][1], comp[i][2], file.c_str());

			   // first, see what files are already in the directory
			   vector<string> dirfiles = FileSystem::EnumFiles(dir);
			   sort(dirfiles.begin(), dirfiles.end());
			   int x, y;
			   // this isn't an issue currently, as files they copy can't be 
			   // set as executable, but in the future as we support custom
			   // install scripts, they may be
			   if (FileSystem::FileExists(dir + "/" + string(comp[i][1]))) {
					// check the path to see if "." is in there. actually this will trigger
					// some false positives
					string path(getenv("PATH"));
					if (path.find(".") != string::npos && (s_ForceInstall ||
						 OutputQuery("The installer is trying to uncompress the file '%s'\n"
									 "with the program '%s'; however, the weblication has\n"
									 "its own copy of '%s' in its install directory, and you\n"
									 "appear to have '.' in your path, so there is a chance\n"
									 "that the weblication's (potentially malicious) copy may\n"
									 "be executed instead of the system-wide copy."
									 "\n\nIt is strongly suggested that you abort this installation."
									 "\n\nDo you want to abort?", file.c_str(), comp[i][1], 
									 comp[i][1]))) {
						 m_Error = "Potentially malicious weblication.";
						 return false;
					}
			   }
			   if ((x = fork()) == 0) {
					errno = 0;
					if (chdir(dir.c_str())) exit(1);
					execlp(comp[i][1], comp[i][1], comp[i][2], file.c_str(), NULL);
					_exit(errno);
			   } else {
					waitpid(x, &y, 0);
					if (WIFEXITED(y)) {
						 int err = WEXITSTATUS(y);
						 if (err) {
							  DEBUGMSG(installer, "Uncompression error %d\n", err);
							  switch(err) {
							  case ENOENT:
								   m_Error = "You do not have the program '" 
										+ string(comp[i][1]) + "' necessary to uncompress\n"
										"a required data file, or (more likely) the program failed.";
								   break;
							  default:
								   m_Error = "Error uncompressing file '" + file + "': " 
										+ string(strerror(err));
							  }
							  success = false;
						 } else {
							  // let's see what files have changed
							  vector<string> newdirfiles = FileSystem::EnumFiles(dir);
							  sort(newdirfiles.begin(), newdirfiles.end());
							  set_difference(newdirfiles.begin(), newdirfiles.end(),
											 dirfiles.begin(), dirfiles.end(),
											 inserter(m_UninstallFiles, m_UninstallFiles.end()));
						 }
					} else {
						 m_Error = "Unknown error uncompressing file '" + file + "'!";
						 success = false;
					}
			   }
			   FileSystem::DeleteFile(dir + "/" + file);
			   return success;
		  }
		  i++;
	 }
	 m_UninstallFiles.push_back(file);

	 return success;
}

bool Installer::CreateRegistry() {
	 // calculate the proper registry location
	 // something like /home/[user]/.sash/[GUID]/[guid].dat
	 string registry_location = m_InstallDirectory + "/" + RegistryFileName(m_GUID);

	 bool do_delete_registry = true, registry_exists = false;
	 DEBUGMSG(installer, "Creating Registry '%s'\n", registry_location.c_str());

	 m_pGraphicalInstaller->SetStatusText("Creating weblication registry...");

	 // first, see if a previous registry exists; if it has keys, prompt for overwrite
	 if (FileSystem::FileExists(registry_location)) {
		  Registry t;
		  if (t.OpenFile(registry_location) && 
			  t.EnumKeys("").size() + t.EnumValues("").size() > 0) {
			   registry_exists = true;
			   if ( s_ForceInstall || s_OverwriteRegistry == 0 || (s_OverwriteRegistry == -1 
					&& ! OutputQuery("Registry information already exists for this weblication.\n"
								   "Would you like to remove the old registry before reinstalling?"))) {
					DEBUGMSG(installer, "Deleting old registry\n");
					do_delete_registry = false;
			   }
		  }
	 }
	 if (s_ForceInstall)
		  do_delete_registry = false;

	 Registry r;
	 if (!r.OpenFile(registry_location)) {
		  m_Result = IR_CANT_CREATE_REGISTRY;
		  m_Error = "Registry creation failed!";
		  return false;
	 }
	 if (do_delete_registry) {
		  if (FileSystem::FileExists(registry_location) && ! FileSystem::DeleteFile(registry_location)) {
			   m_Error = "Write permission denied for registry!";
			   return false;
		  }	 
	 }
	 if (! registry_exists || do_delete_registry) {
		  // import the default registry screens
		  DEBUGMSG(installer, "Starting to load up registry...\n");
		  m_pWDF->GetRegistry(&r);
		  DEBUGMSG(installer, "DONE LOADING REGISTRY\n");
  
		  r.WriteToDisk();
	 }
  
	 // save the WDF to disk in the right place
	 m_pWDF->SaveToFile(WDFFileName(m_InstallDirectory + "/" + m_GUID));
  
	 // save the security hash to disk if it's a weblication
	 if (m_pWDF->GetExtensionType() == SET_WEBLICATION) {
	   m_pSecurity->Save(SecurityFileName(m_InstallDirectory + "/" + m_GUID));
	 }

	 m_pGraphicalInstaller->AdvanceProgressBar();
  
	 DEBUGMSG(installer, "[SUCCEEDED]\n");
	 return true;
}

bool Installer::AddGNOMEIcon() {
  
	 DEBUGMSG(installer, "Adding GNOME start menu icon...\n");
  
	 // only make icons for weblications
	 if (m_pWDF->GetExtensionType() != SET_WEBLICATION)
		  return true;
  
	 m_pGraphicalInstaller->SetStatusText("Adding GNOME menu icon...");
  
	 const string exec_prog(RUNTIME_NAME); 
	 const string exec_param(m_GUID);
  
	 vector<string> vs(2);
	 vs.push_back(exec_prog);
	 vs.push_back(exec_param);
  
	 string desktop_file = create_desktop_file("sash-wbl-" + m_GUID,
											   m_pWDF->GetTitle(), vs,
											   "/usr/share/pixmaps/Bird.xpm");
  
	 m_UninstallFiles.push_back(desktop_file);
  
//	 m_pGraphicalInstaller->AdvanceProgressBar();
  
	 DEBUGMSG(installer, "[SUCCEEDED]\n");
	 return true;
}

bool Installer::WDFHasPlatform() {
	 vector<string> pc = m_pWDF->GetPlatforms();
	 vector<string>::iterator ai = pc.begin(), bi = pc.end();
	 return (find(ai, bi, PLATFORM) != bi);
}

string Installer::MakeRelativePathAbsolute(const string& rel) {

	 // if we're getting stuff from the web, we can only allow http or relative paths
	 if (m_FromWeb) {
		  if (URLGetProtocol(rel) == "file") {
			   // not allowed
			   return "";
		  } else if (URLHasProtocol(rel))
			   return rel;
		  else
			   return (m_BaseLocation + "/" + rel);
	 } else {
		  if (URLGetProtocol(rel) == "http") 
			   return rel;
		  else if (rel[0] == '/')
			   return rel;
		  else
			   // if it's local, must be relative
			   return (m_BaseLocation + "/" + rel);
	 }
}

NS_IMPL_ISUPPORTS1(InstallerDownloadCallback, sashINetCallback);

NS_IMETHODIMP InstallerDownloadCallback::UpdateProgress(PRUint32 p, PRUint32 pmax) {
	 if (m_Complete) return NS_OK;
	 ostrstream ost;
	 ost << m_Display << " [" << p << " of " << pmax << " bytes]" << '\0';
	 if (pmax > m_MaxSize) m_MaxSize = pmax;
	 if (m_Graphical) {
		  if (p >= m_LastP) {
			   m_pGraphicalInstaller->SetStatusText(ost.str());
			   m_pGraphicalInstaller->AdvanceProgressBar(false, pmax ? (double)p/pmax : pmax);
			   m_LastP = p;
		  }
	 } else {
		  unsigned int s = ost.pcount();
		  if (s > m_MaxLen) m_MaxLen = s + 1;
		  cout << setiosflags(ios::left) << setw(m_MaxLen) << ost.str() << '\r' << flush;
	 }
	 return NS_OK;
}

NS_IMETHODIMP InstallerDownloadCallback::UpdateStatus(const char * status) {
	 if (m_Complete) return NS_OK;
	 string s = status;
	 if (s == m_Status) { 
		  // in case there are any events anyway
		  return NS_OK;
	 }
	 m_Status = s;
	 s = m_Display + " [" + s + "]";
	 if (m_Graphical) {
		  m_pGraphicalInstaller->SetStatusText(s.c_str());
	 } else {
		  if (s.length() > m_MaxLen) m_MaxLen = s.length() + 1;
		  cout << setiosflags(ios::left) << setw(m_MaxLen) << s.c_str() << '\r' << flush;
	 }
	 return NS_OK;
}

NS_IMETHODIMP InstallerDownloadCallback::UpdateComplete() { 
	 ostrstream ost;
	 ost << m_Display << " [Complete: " << m_MaxSize << " bytes]" << '\0';
	 if (m_Graphical)
		  m_pGraphicalInstaller->SetStatusText(ost.str());
	 else 
		  cout << setiosflags(ios::left) << setw(m_MaxLen) << ost.str() << endl;
	 m_Complete = true;
	 return NS_OK;
}

NS_IMETHODIMP InstallerDownloadCallback::UpdateAbort() { 
	 m_Complete = true;
	 return NS_OK;
}

 
#include <nsIHttpProtocolHandler.h>
#include <nsCOMPtr.h>
#include <nsIServiceManager.h>
#include <nsString.h>

Version Installer::GetMozillaVersion() {
	 // Get httpHandler service.
	 nsresult rv = NS_OK;
	 nsCOMPtr <nsIHttpProtocolHandler> httpHandler(do_GetService("@mozilla.org/network/protocol;1?name=http", &rv));
	 if (! NS_SUCCEEDED(rv)) 
		  return Version();

	 nsCAutoString agent;
	 httpHandler->GetUserAgent(agent);
	 string ver = agent.get();
	 unsigned int pos = ver.find("rv:");
	 if (pos == string::npos) 
		  return Version();

	 // move past the rv:
	 pos += 3;
	 unsigned int endpos = pos;
	 while (isdigit(ver[endpos]) || ver[endpos] == '.')
		  ++endpos;

	 return Version(ver.substr(pos, endpos-pos));
}


