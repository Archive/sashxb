
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar

Reads wdf, does all necessary backend install steps

*****************************************************************/

#ifndef _INSTALLER_H_
#define _INSTALLER_H_

// Weblication installer
// AJ Shankar June/July 2000

#include <string>
#include <list>
#include "Registry.h"
#include "wdf.h"
#include "sashINet.h"
#include "GraphicalInstaller.h"
#include "InstallationManager.h"
#include "FileSystem.h"

class LocatorClient;

class InstallerDownloadCallback : public sashINetCallback {
public:
	 InstallerDownloadCallback(std::string& display, bool graphical, BaseInstaller* progbar) : 
		  m_Display(display),
		  m_Graphical(graphical), 
		  m_Complete(false),
		  m_pGraphicalInstaller(progbar),
		  m_LastP(0), m_MaxSize(0), m_MaxLen(0) {}
	 virtual ~InstallerDownloadCallback() {}

	 NS_DECL_ISUPPORTS
	 NS_DECL_SASHINETCALLBACK

private:
	 std::string m_Display;
	 std::string m_Status;
	 bool m_Graphical, m_Complete;
	 BaseInstaller* m_pGraphicalInstaller;
	 guint32 m_LastP, m_MaxSize;
	 unsigned int m_MaxLen;
};

enum InstallResult {
	 // failure codes:
	 IR_CANT_OPEN_WDF,
	 IR_NOT_NEWER_VERSION,
	 IR_UNRECOGNIZABLE_TYPE,
	 IR_FAILED_COMPONENT_INSTALLATION,
	 IR_USER_CANCELED,
	 IR_CANT_CREATE_DIRECTORIES,
	 IR_CANT_GET_FILE,
	 IR_CANT_CREATE_REGISTRY,
	 // success!
	 IR_SUCCEEDED
};

class Installer {
  
 public:
  // Instantiate the installer; takes either the location of the 
  // wdf file to install, or a guid. If the latter, it tries to 
  // find the wdf based on whether it's been installed before, or
  // through the locator service
  // If a version is passed, the installer will only install the component
  // if it is >= that version
  Installer(const std::string& location_or_guid, 
			const Version required_version = Version());
  ~Installer();
  
  // go ahead and install the program
  InstallResult Install();
  
  // call this to abort the installation
  // returns true if there is a graphical installer and 
  // caller must wait; otherwise caller can immediately exit
  bool Abort();

  // install directory
  // something like /home/[user]/.sash/[GUID]
  std::string m_InstallDirectory;

  // command-line options
  static int s_OverwriteRegistry;
  static int s_InstallOverPrevious;
  static int s_TextInstall;
  static int s_ForceInstall;

  // reports current error
  string m_Error;
  InstallResult m_Result;

 private:
  static const int NUM_STEPS;

  // failed installation function
  InstallResult Failed();

  // true if there is a previous installation
  bool m_InstallingOverPrevious;

  // downloader
  nsCOMPtr<sashIDownloader> m_pDownloader;

  // mininum version to install
  Version m_RequiredVersion;

  // location of the WDF file
  std::string m_WDFLocation;
  	 
  // guid of weblication
  std::string m_GUID;

  // list of files to get
  vector<WDFFile> m_Files;

  // wdf document pertaining to weblication
  WDF* m_pWDF;

  // nice graphical installer for users
  BaseInstaller* m_pGraphicalInstaller;
  
  // Installation manager helps us figure out what and where to install
  InstallationManager m_IM;
  
  // are we getting this from the web?
  bool m_FromWeb;
  // what location to prepend when downloading files
  string m_BaseLocation;

  // hash table of security settings
  SecurityHash* m_pSecurity;

  // locator client to get correct wdf urls
  LocatorClient *m_pLocator;

  // list of files to delete on uninstall
  std::vector<std::string> m_UninstallFiles;

  // gets the WDF file 
  bool GetWDF();
  
  // makes sure that all components on which this weblication depends are installed
  // and if not, offers to install them
  bool CheckDependencies();

  // passes off to graphical installer
  bool UserInstallScreens();
  
  // creates the weblication directory and subdirectories
  bool CreateDirectories();
  
  // copies the weblication files to the appropriate directories
  bool InstallFiles();
  
  // creates the weblication's registry 
  bool CreateRegistry();
  
  // creates shell script to clean up after weblication
  bool CreateUninstallScript();
  
  // add icon to gnome start menu
  bool AddGNOMEIcon();

  // Verifies that the weblication supports this platform
  bool WDFHasPlatform();

  // determines the installed mozilla version
  Version GetMozillaVersion();

  // takes a relative path (either local or web-based) and makes it absolute
  std::string MakeRelativePathAbsolute(const std::string& rel);

  // takes a file, checks its extension, and attempts to decompress it
  // if necessary
  bool UncompressFile(const std::string& dir, const std::string& file);

  bool m_HasAborted;
};

#endif // _INSTALLER_H_
