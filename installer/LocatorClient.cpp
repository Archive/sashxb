/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Client for the locator service

*****************************************************************/

#include "LocatorClient.h"
#include "sash_constants.h"
#include "debugmsg.h"
#include "sashINet.h"
#include "InstallationManager.h"
#include "Registry.h"
#include "FileSystem.h"
#include "XMLDocument.h"
#include "XMLNode.h"

LocatorClient::LocatorClient()
{
}

LocatorClient::~LocatorClient()
{
}

bool LocatorClient::IsError(const string& s) {
	 return (s.find("Error:") == 0);
}

void LocatorClient::GetLocatorURLs(vector<string>& list, 
							  const InstallationManager& im) {
	 bool use, fallback;
	 vector<string> l;

	 Registry r;
	 r.OpenFile(GetSashShareDirectory() + "/locator.dat");
	 RegistryValue rv;
	 r.QueryValue("", "LocatorURL", &rv);
	 assert(rv.GetValueType() == RVT_STRING);

	 im.GetLocatorServices(use, l, fallback);
	 if (use) {
		  list = l;
		  if (fallback)
			   list.push_back(rv.m_String);
	 } else {
		  list.push_back(rv.m_String);
	 }
}

string LocatorClient::GetURL(const string & guid, 
							 const InstallationManager& im) {
  DEBUGMSG(locator, "Getting url for guid %s\n", guid.c_str());

  string tempfile = "/tmp/" + FileSystem::GetTempName();
  string url;
  vector<string> urls;
  GetLocatorURLs(urls, im);
  if (urls.size() == 0) return "Error: Could not determine URL of locator service!";

  // guid, os, architecture, distribution
  url = "?guid=" + guid;

  SashSystemInfo info;
  GetSashSystemInfo(info);

  url += "&platform=" + info.OS;
  url += "&architecture=" + info.Architecture;
  url += "&distribution=" + info.Distribution;


  replace_if(url.begin(), url.end(), ptr_fun(isspace), '+');

  vector<string>::iterator ai = urls.begin(), bi = urls.end();
  string ret;
  while (ai != bi) {
	   DEBUGMSG(locator, "Asking locator url %s, in temp file %s\n", 
				ai->c_str(), tempfile.c_str());
	   DEBUGMSG(locator, "Params are %s\n", url.c_str());
	   nsCOMPtr<sashIDownloader> d;
	   NewSashFileDownloader((*ai + url).c_str(), tempfile.c_str(), false, getter_AddRefs(d));
	   d->StartTransfer();
	   PRInt32 status;
	   d->ProcessUntilComplete(&status);
	   if (status != sashINet::NET_DONE) {
			DEBUGMSG(locator, "Locator query failed\n");
			ret = "Error: Could not contact locator service at " + *ai + "!";
	   } else {
			ret = "";
			ifstream fin(tempfile.c_str());
			fin >> ret; // to strip whitespace
			char buf[4096];
			fin.getline(buf, 4095);
			ret += buf;
			FileSystem::DeleteFile(tempfile);
			if (ret == "") 
				 ret = "Error: Empty string returned!";
			if (! IsError(ret)) 
				 break;
	   }
  
	   ++ai;
  }

  DEBUGMSG(locator, "Got url \"%s\"\n", ret.c_str());
  return ret;
}

