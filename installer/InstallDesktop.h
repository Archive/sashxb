
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu

Lets the installer put shortcuts to weblications on the GNOME start menu

*****************************************************************/

#ifndef INSTALL_DESKTOP_H
#define INSTALL_DESKTOP_H

#include <string>
#include <vector>

// Create a GNOME .desktop file in the user's .gnome/apps/ directory.
//
// Takes in a GUID, and the weblication's name, executable file,
//  and icon.
//
// Returns the absolute path of the created .desktop file

std::string create_desktop_file(const std::string& GUID,
				const std::string& wbl_name, 
				const std::vector<std::string>& wbl_exec,
				const std::string& wbl_icon);

#endif //#ifndef INSTALL_DESKTOP_H
