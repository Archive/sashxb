#ifndef LOCATORCLIENT_H
#define LOCATORCLIENT_H


/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Chatham

Client for the locator service

*****************************************************************/

#include <string>
#include "InstallationManager.h"
class LocatorClient
{
 public:
	 LocatorClient();
	 ~LocatorClient();

	 // returns true if url represents an error message
	 static bool IsError(const std::string& s);
	/**
		 Ask the locator service for the url for the WDF for the requested guid.
		 Returns an empty string if the guid was not found in the locator service.
	*/
	string GetURL(const string & guid, const InstallationManager& im);
	
 private:
	void GetLocatorURLs(std::vector<std::string>& list, const InstallationManager& im);
};

#endif //LOCATORCLIENT_H
