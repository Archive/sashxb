
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu, AJ Shankar

Provides the Graphical front-end to the weblication installer

*****************************************************************/


//
// Graphical Installer for SashXB
//

#include "GraphicalInstaller.h"
#include <libgnomeui/gnome-appbar.h>
#include <gnome.h>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <strstream>
#include <glib.h>
#include "sash_error.h"
#include "debugmsg.h"
#include "wdf.h"
#include "url_tools.h"
#include "SashSecurityDialog.h"
#include "Installer.h"
#include "SecurityHash.h"

using namespace std;

GraphicalInstaller::GraphicalInstaller(WDF* wdf, SecurityHash * security, 
									   const string& base_path, 
									   const bool from_web, const int num_steps) 
	 :  m_BasePath(base_path),
		m_FromWeb(from_web),
		m_NumSteps(num_steps),
	 	m_CurStep(0),
	 	m_CurScreen(0),
		m_IsDone(false),
		m_InstallSuccess(false),
		m_HasSuccessPage(false),
		m_HasFailurePage(false),
		m_pSecurity(security),
		m_pWDF(wdf) {
	 m_Title = "Sash Installer - " + wdf->GetTitle() + " v" + wdf->GetVersion().weblication.ToString();
	 m_pWindow = NULL;
	 m_ScreenLoaded = true;
	 DEBUGMSG(installer, "Graphical installer about to init graphics\n");
	 InitGraphics();
}


GraphicalInstaller::~GraphicalInstaller() {
	 if (! m_SecurityFileName.empty()) {
		  FileSystem::DeleteFile(m_SecurityFileName);
	 }
	 if (! m_FinishFileName.empty()) {
		  FileSystem::DeleteFile(m_FinishFileName);
	 }

	 if (m_pWindow){
		  SashErrorSetWindow(NULL);
		  gtk_widget_hide_all(m_pWindow);
		  gtk_widget_destroy(m_pWindow);
	 }
}

void GraphicalInstaller::AddScreens(const vector<WDFInstallScreen>& screens) {
  bool has_security = false;

  vector<WDFInstallScreen>::const_iterator ai = screens.begin(), bi = screens.end();
  while (ai != bi) {
	   InstallScreenType ist = ai->type;
	   switch (ist) {
	   case IST_FAILURE:
			m_HasFailurePage = true;
			m_FailurePage = *ai;
			break;
	   case IST_SUCCESS:
			m_HasSuccessPage = true;
			m_SuccessPage = *ai;
			break;
			// intentionally fall through here
	   case IST_SECURITY:
			has_security = true;
	   case IST_LICENSE:
	   case IST_TEXT:
			m_Screens.push_back(*ai);
			++m_NumSteps;
			break;
	   default:
			break;
	   }
	   ++ai;
  }
  if (has_security == false) {
	   DEBUGMSG(installer, "No security page specified in WDF! -- Adding one now\n");
	   WDFInstallScreen w;
	   w.type = IST_SECURITY;
	   w.title = "Security";	
	   m_Screens.push_back(w);
  }
  DEBUGMSG(installer, "Total number of steps is %d\n", m_NumSteps);
}

bool GraphicalInstaller::Begin() {
  
  DEBUGMSG(installer, "trying gtk_signal_connect\n");

  gtk_signal_connect(GTK_OBJECT(m_pGecko), "net-stop",
		     GTK_SIGNAL_FUNC(LoadFinished), this);

  m_CurScreen = 0;
  DisplayScreen(m_Screens[m_CurScreen]);

  // start showing the window
  gtk_widget_show(m_pWindow);

  for (;;) {
	   if (Update())
			return m_InstallSuccess;
	   usleep(1);
  }
}

void GraphicalInstaller::ButtonBack(GtkWidget* apWidget, gpointer data) {
	GraphicalInstaller* g = (GraphicalInstaller *) data;
	if (g->m_CurScreen > 0) {
		 g->m_CurScreen--;
		 g->m_CurStep = g->m_CurScreen;
		 g->AdvanceProgressBar(false);
		 g->DisplayScreen(g->m_Screens[g->m_CurScreen]);
	}
}

void GraphicalInstaller::ButtonNext(GtkWidget* apWidget, gpointer data) {
	GraphicalInstaller* g = (GraphicalInstaller *) data;
	if (g->m_CurScreen < g->m_Screens.size() - 1) {
		 g->m_CurScreen++;
		 g->m_CurStep = g->m_CurScreen;
		 g->AdvanceProgressBar(false);
		 g->DisplayScreen(g->m_Screens[g->m_CurScreen]);
	} else if (g->m_CurScreen == g->m_Screens.size()-1){
		 DEBUGMSG(installer, "next clicked on last install screen, done for now.\n");
		 g->SetInstallStatus(true);
		 gtk_widget_set_sensitive(g->m_pPrev, false);
		 gtk_widget_set_sensitive(g->m_pNext, false);

	}
}

void GraphicalInstaller::Cancel() {
	 SetInstallStatus(false);
}

void GraphicalInstaller::ButtonCancel(GtkWidget* apWidget, gpointer data) {
  ((GraphicalInstaller *)data)->Cancel();
}

void GraphicalInstaller::ButtonFinish(GtkWidget* apWidget, gpointer data) {
	 GraphicalInstaller* g = (GraphicalInstaller *) data;
	 g->m_IsDone = true;
}


void GraphicalInstaller::LoadFinished(GtkMozEmbed* aEmbed, GraphicalInstaller* aGI) {
  DEBUGMSG(installer, "Load Finished!\n");
  if (aGI->m_CurScreen < aGI->m_Screens.size()){
	   if (aGI->m_Screens[aGI->m_CurScreen].type == IST_SECURITY)
			if (aGI->m_pWDF->GetExtensionType() == SET_WEBLICATION)
				 gtk_widget_show(aGI->m_pSecurityButton);

	   gtk_widget_set_sensitive(aGI->m_pNext, TRUE);
	   gtk_widget_set_sensitive(aGI->m_pAccept, TRUE);
  }
}

void GraphicalInstaller::LoadURL(const string& aURL) {
	 // Tell embedded Gecko engine to load url
	 string f_url(aURL);

	 if (URLHasProtocol(aURL)) {
		  // nothing to do
	 } else if (m_FromWeb) {
		  // url is from the web, so prepend base path
		  f_url = m_BasePath + "/" + f_url;
	 } else {
		  // url is local, so must make absolute for mozilla
		  if (m_BasePath != "") f_url = m_BasePath + "/" + f_url;
		  f_url = URLMakeLocalBrowserCompatible(f_url);
	 }
			   
	 DEBUGMSG(installer, "Installer loading page: %s\n", f_url.c_str());
	 gtk_moz_embed_load_url( GTK_MOZ_EMBED(m_pGecko), f_url.c_str());
	 
	 m_ScreenLoaded = false;
}

void GraphicalInstaller::DisplayScreen(const WDFInstallScreen& c) {
	 DEBUGMSG(installer, "DisplayScreen:\n");
	 DEBUGMSG(installer, "Setting Title.\n");
	 if (c.title.empty()) {
		  gtk_window_set_title( GTK_WINDOW(m_pWindow), (m_Title).c_str() );
	 } else {
		  gtk_window_set_title( GTK_WINDOW(m_pWindow), (m_Title + " - " + c.title).c_str() );
	 }
	 DEBUGMSG(installer, "Hiding button boxes.\n");
	 DEBUGMSG(installer, "cur screen index: %d\n", m_CurScreen);
	 if (c.type == IST_LICENSE){
		  gtk_widget_hide(m_pButtonBoxPrevNextCancel);
		  gtk_widget_show(m_pButtonBoxAcceptDecline);
		  gtk_widget_hide(m_pButtonBoxFinish);
		  
		  gtk_widget_set_sensitive(m_pAccept, FALSE);
		  gtk_widget_set_sensitive(m_pPrevAD, m_CurScreen != 0);
	 } else if ((c.type == IST_SUCCESS) || (c.type == IST_FAILURE)){
		  gtk_widget_hide(m_pButtonBoxPrevNextCancel);
		  gtk_widget_hide(m_pButtonBoxAcceptDecline);
		  gtk_widget_show(m_pButtonBoxFinish);
	 } else {
		  gtk_widget_show(m_pButtonBoxPrevNextCancel);
		  gtk_widget_hide(m_pButtonBoxAcceptDecline);
		  gtk_widget_hide(m_pButtonBoxFinish);
		  
		  gtk_widget_set_sensitive(m_pPrev, m_CurScreen != 0);
		  
	 }

	 // Show security screen if screen requests
	 if (c.type == IST_SECURITY){
		  DEBUGMSG(installer, "Showing security page.\n");
		  string o;
		  if (m_SecurityFileName.empty()) {
			   GenerateSecurityString(o);
			   m_SecurityFileName = "/tmp/" + FileSystem::GetTempName();
			   ofstream f(m_SecurityFileName.c_str());
			   f << o;
			   f.close();	
		  }
		  DEBUGMSG(installer, "Loading URL at %s\n", m_SecurityFileName.c_str());
		  gtk_moz_embed_load_url(GTK_MOZ_EMBED(m_pGecko), ("file://" + m_SecurityFileName).c_str());
		  gtk_widget_set_sensitive(m_pNext, FALSE);
	 } else {
		  DEBUGMSG(installer, "Loading URL, hiding security button.\n");
		  gtk_widget_hide(m_pSecurityButton);
		  LoadURL(c.page.file);
	 }

	 // Update status bar
	 ostrstream ost;
	 if (m_CurScreen < m_Screens.size()){
		  ost << "Installation screen "<< m_CurScreen+1 << " of " 
			  << m_Screens.size() << '\0';
		  SetStatusText(ost.str());
	 } 
  
}

void GraphicalInstaller::Finish(bool success, const string &err_message) { 
	 m_CurScreen = m_Screens.size();
	 m_IsDone = false;
	 gtk_widget_hide(m_pSecurityButton);
	 gtk_widget_hide(m_pButtonBoxPrevNextCancel);
	 gtk_widget_hide(m_pButtonBoxAcceptDecline);
	 gtk_widget_show(m_pButtonBoxFinish);
	 string s = (success ? "Success" : "Failure");
	 SetStatusText(("Install outcome: " + s).c_str());

	 gtk_window_set_title( GTK_WINDOW(m_pWindow), ("Sash Installer - " + m_Title + " - " + s).c_str() );
	 if (! success) {
		  GtkWidget* w = gtk_label_new(("Reason for failure: " + err_message).c_str());
		  gtk_box_pack_start(GTK_BOX(m_pErrorBox),
					  w,
					  FALSE /*expand*/, FALSE /*fill*/, 8 /*padding*/);
		  
		  gtk_widget_show_all(m_pErrorBox);
	 }
	 string ret;

	 // Add default finish pages here.
	 if (success && m_HasSuccessPage) {
		  DisplayScreen(m_SuccessPage);
	 } else if (! success && m_HasFailurePage) {
		  DisplayScreen(m_FailurePage);
	 } else {
		  if (success)
			   GenerateSuccessString(ret);
		  else 
			   GenerateFailureString(ret);
		  m_FinishFileName = "/tmp/" + FileSystem::GetTempName();
		  ofstream f(m_FinishFileName.c_str());
		  f << ret;
		  f.close();	

		  DEBUGMSG(installer, "Loading URL at %s\n", m_FinishFileName.c_str());
		  gtk_moz_embed_load_url(GTK_MOZ_EMBED(m_pGecko), ("file://" + m_FinishFileName).c_str());
	 }
	 for (;;) {
		  if (Update())
			   return;
		  usleep(1);
	 }
}

void GraphicalInstaller::InitGraphicsWindow() {
	 // Only perform the initialization once
	 m_pWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	 gtk_widget_set_usize( m_pWindow, 532, 374 );
	 SashErrorSetWindow(GTK_WINDOW(m_pWindow));
  
	 //embed gecko
	 m_pGecko = gtk_moz_embed_new();
	 if (!m_pGecko) {
		  OutputError("Could not create new embedded Mozilla widget. Aborting!");
		  abort();
	 }
}

void GraphicalInstaller::InitGraphicsButtons() {
	 // prev, next, cancel
	 GtkWidget *w = gnome_stock_button(GNOME_STOCK_BUTTON_CANCEL);
	 gtk_signal_connect(GTK_OBJECT(w), "clicked", 
						GTK_SIGNAL_FUNC(ButtonCancel), 
						(void *) this);
	 gtk_box_pack_end(GTK_BOX(m_pButtonBoxPrevNextCancel),
					  w,
					  FALSE /*expand*/, FALSE /*fill*/, 4 /*padding*/);
	 DEBUGMSG(installer, "added cancel button\n");

	 m_pNext = gnome_stock_button(GNOME_STOCK_BUTTON_NEXT);
	 gtk_signal_connect(GTK_OBJECT(m_pNext), "clicked", 
						GTK_SIGNAL_FUNC(ButtonNext), 
						(void *) this);
	 gtk_box_pack_end(GTK_BOX(m_pButtonBoxPrevNextCancel),
					  m_pNext,
					  FALSE /*expand*/, FALSE /*fill*/, 4 /*padding*/);
	 DEBUGMSG(installer, "added next button\n");

	 m_pPrev = gnome_stock_button(GNOME_STOCK_BUTTON_PREV);
	 gtk_signal_connect(GTK_OBJECT(m_pPrev), "clicked", 
						GTK_SIGNAL_FUNC(ButtonBack), 
						(void *) this);
	 gtk_box_pack_end(GTK_BOX(m_pButtonBoxPrevNextCancel),
					  m_pPrev,
					  FALSE /*expand*/, FALSE /*fill*/, 4 /*padding*/);
	 DEBUGMSG(installer, "added prev button\n");

	 // accept, decline
	 GtkWidget* pix = gnome_stock_pixmap_widget (m_pWindow,
												 GNOME_STOCK_BUTTON_NO);

	 w = gnome_pixmap_button(pix, "Decline");
	 gtk_signal_connect(GTK_OBJECT(w), "clicked", 
						GTK_SIGNAL_FUNC(ButtonCancel), 
						(void *) this);
	 gtk_box_pack_end(GTK_BOX(m_pButtonBoxAcceptDecline),
					  w,
					  FALSE /*expand*/, FALSE /*fill*/, 4 /*padding*/);
	 DEBUGMSG(installer, "added decline button\n");

	 pix = gnome_stock_pixmap_widget (m_pWindow,
									   GNOME_STOCK_BUTTON_YES);

	 m_pAccept = gnome_pixmap_button(pix, "Accept");
	 gtk_signal_connect(GTK_OBJECT(m_pAccept), "clicked", 
						GTK_SIGNAL_FUNC(ButtonNext), 
						(void *) this);
	 gtk_box_pack_end(GTK_BOX(m_pButtonBoxAcceptDecline),
					  m_pAccept,
					  FALSE /*expand*/, FALSE /*fill*/, 4 /*padding*/);
	 DEBUGMSG(installer, "added accept button\n");

	 m_pPrevAD = gnome_stock_button(GNOME_STOCK_BUTTON_PREV);
	 gtk_signal_connect(GTK_OBJECT(m_pPrevAD), "clicked", 
						GTK_SIGNAL_FUNC(ButtonBack), 
						(void *) this);
	 gtk_box_pack_end(GTK_BOX(m_pButtonBoxAcceptDecline),
					  m_pPrevAD,
					  FALSE /*expand*/, FALSE /*fill*/, 4 /*padding*/);
	 DEBUGMSG(installer, "added prev button\n");

	 // finish
	 pix = gnome_stock_pixmap_widget (m_pWindow,
									   GNOME_STOCK_BUTTON_OK);

	 w = gnome_pixmap_button(pix, "Finish");
	 gtk_signal_connect(GTK_OBJECT(w), "clicked", 
						GTK_SIGNAL_FUNC(ButtonFinish), 
						(void *) this);
	 gtk_box_pack_end(GTK_BOX(m_pButtonBoxFinish),
					  w,
					  FALSE /*expand*/, FALSE /*fill*/, 8 /*padding*/);
	 DEBUGMSG(installer, "added finish button\n");
}

void GraphicalInstaller::OnSecurityButtonClicked(GtkButton * button, void * data) {
	 GraphicalInstaller * installer = (GraphicalInstaller *) data;
	 SashSecurityDialog dialog;
	 dialog.Run(*(installer->m_pSecurity), GTK_WINDOW(installer->m_pWindow));
}

void GraphicalInstaller::InitGraphics() {

  InitGraphicsWindow();
  m_pSecurityButton = gtk_hbox_new(0, FALSE);
  GtkWidget * button = gtk_button_new_with_label("Edit Security Settings...");
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(m_pSecurityButton), button, false, false, 8);

  gtk_signal_connect(GTK_OBJECT(button), "clicked", 
					 GTK_SIGNAL_FUNC(GraphicalInstaller::OnSecurityButtonClicked), this);

  m_pButtonBoxPrevNextCancel = gtk_hbox_new(false, 0);
  m_pButtonBoxAcceptDecline = gtk_hbox_new(false, 0);
  m_pButtonBoxFinish = gtk_hbox_new(false, 0);
  m_pErrorBox = gtk_hbox_new(false, 0);

  InitGraphicsButtons();
  
  DEBUGMSG(installer, "Init graphics for installer\n");
  // status/progress bar
  m_pStatusBar = gnome_appbar_new(TRUE, TRUE /* status bar? */, 
				  GNOME_PREFERENCES_USER);
  // get progress bar from status bar
  m_pProgressBar = gnome_appbar_get_progress(GNOME_APPBAR(m_pStatusBar));
  
  // big column box for all widgets
  m_pWindowBox = gtk_vbox_new(false, 0); // column of boxes


  // we add a reference count to this window box so that we 
  // can remove it from the window without it deleting itself
  // it gets derefed in the destructor
  gtk_widget_ref(m_pWindowBox); 

  gtk_box_pack_start(GTK_BOX(m_pWindowBox), // add mozilla widget
		     m_pGecko, 
		     TRUE /*expand*/, TRUE /*fill*/, 0 /*padding*/);
  gtk_box_pack_start(GTK_BOX(m_pWindowBox), // add security tabs
		     m_pSecurityButton, 
		     FALSE /*expand*/, FALSE /*fill*/, 0 /*padding*/);
  gtk_box_pack_start(GTK_BOX(m_pWindowBox), // add row box of buttons
		     m_pErrorBox, 
		     FALSE /*expand*/, FALSE /*fill*/, 10  /*padding*/);
  gtk_box_pack_start(GTK_BOX(m_pWindowBox), // add row box of buttons
		     m_pButtonBoxPrevNextCancel, 
		     FALSE /*expand*/, FALSE /*fill*/, 10  /*padding*/);
  gtk_box_pack_start(GTK_BOX(m_pWindowBox), // add row box of buttons
		     m_pButtonBoxAcceptDecline, 
		     FALSE /*expand*/, FALSE /*fill*/, 10  /*padding*/);
  gtk_box_pack_start(GTK_BOX(m_pWindowBox), // add row box of buttons
		     m_pButtonBoxFinish, 
		     FALSE /*expand*/, FALSE /*fill*/, 10  /*padding*/);
  gtk_box_pack_start(GTK_BOX(m_pWindowBox), // add status bar
		     m_pStatusBar, 
		     FALSE /*expand*/, FALSE /*fill*/, 0 /*padding*/);
  gtk_widget_show_all(m_pWindowBox);
  gtk_widget_hide(m_pSecurityButton);
  gtk_widget_hide(m_pErrorBox);
  // Set gtk window title to current screen's title
  gtk_window_set_title( GTK_WINDOW(m_pWindow), m_Title.c_str() );

  gtk_container_add(GTK_CONTAINER(m_pWindow), m_pWindowBox);
}

void GraphicalInstaller::SetInstallStatus(bool aStatus) {
  m_IsDone = true;
  m_InstallSuccess = aStatus;
}

bool GraphicalInstaller::Update() const {
  while (gtk_events_pending()) {
    gtk_main_iteration();
  }
  return m_IsDone;
}

void GraphicalInstaller::Reset() {
	 m_IsDone = false;
}


void GraphicalInstaller::SetStatusText(const char* s) {
	 assert(s);

  gnome_appbar_set_status(GNOME_APPBAR(m_pStatusBar), s);
  Update();
}

void GraphicalInstaller::AdvanceProgressBar(bool increment_step, double micro_increment) { 
	 if (increment_step) ++m_CurStep;
	 // Update progress bar
	 gfloat pvalue =(gfloat)(m_CurStep) / (gfloat)m_NumSteps;
	 if (micro_increment > 0.) {
		  pvalue += micro_increment * 1/m_NumSteps;
	 } else { 
		  DEBUGMSG(installer, "Setting progress bar step to %d of %d\n", m_CurStep, m_NumSteps);
	 }

	 if (pvalue >= 1.0)
		  pvalue = 1.0;

	 gtk_progress_set_percentage(m_pProgressBar, pvalue);
	 Update();
}

void GraphicalInstaller::GenerateSecurityString(string& out) {
	 if (m_pWDF->GetExtensionType() == SET_WEBLICATION) {
		  sec_hash& h = m_pSecurity->GetHash();
		  sec_hash::iterator a = h.begin(), b = h.end();
		  string requests;

		  while (a != b) {
			   SecurityItem& i = a->second;
			   if (i.m_ID == -1) { ++a; continue; }
			   switch (i.m_Type) {
			   case ST_BOOL:
					if (i.m_Bool) {
						 requests += "<li>" + i.m_DescriptiveName + "</li>\n";
					}
					break;
			   case ST_NUMBER:
					if (i.m_Number > 0) {
						 char num[32];
						 snprintf(num, 31, "%f", i.m_Number);
						 requests += "<li>" + i.m_DescriptiveName + ": " + num + "</li>\n";
					}
					break;
			   case ST_STRING:
					if (! i.m_String.empty()) {
						 requests += "<li>" + i.m_DescriptiveName + ": " + i.m_String + "</li>\n";
					}
					break;
			   case ST_STRING_ENUM:
					if (i.m_StringIndex) {
						 requests += "<li>" + i.m_DescriptiveName + ": " + 
							  i.m_StringVals[i.m_StringIndex] + "</li>\n";
					}
					break;
			   case ST_STRING_ARRAY:
					if (i.m_StringVals.size()) {
						 requests += "<li>" + i.m_DescriptiveName + ": ";
						 for (vector<string>::iterator ai = i.m_StringVals.begin() ; 
							  ai < i.m_StringVals.end() ; ++ai) {
							  requests += "'" + *ai + "' ";
						 }
						 requests + "</li>\n";
					}
					break;
			   default: 
					break;
			   }
			   ++a;
		  }

 		  out = "<html><body bgcolor='#d4d4d4'><font face='verdana, arial'><h1>Security Permissions</h1>\n";
		  if (requests.empty()) {
			   out += "This weblication has not requested any native permissions.";
		  } else {
			   out += "This weblication has requested the the following native permissions: <ul>" + 
			   requests + "</ul>If you'd like to view or modify them, please click on the button below.";
		  }
	 } else {
		  out = "<html><body bgcolor='#d4d4d4'><font face='verdana, arial'><h1>Binary SashXB Component</h1>"
			   "The SashXB component you are trying to install contains binary code. This code cannot be "
			   "checked by our security engine and has unrestricted access to your computer. Only continue "
			   "with this installation if you are sure the component is from a trusted source.";
	 }
	 out += "<p><font size='-1'><b>Author</b>: " + m_pWDF->GetAuthor() + "<br>\n"
	        "<b>Title</b>: " + m_pWDF->GetTitle() + "<br>\n"
	        "<b>Version</b>: " + m_pWDF->GetVersion().weblication.ToString() + "<br>\n"
	        "<b>Abstract</b>: " + m_pWDF->GetAbstract() + "<br>\n"
	        "<b>Installing From</b>: " + m_BasePath + "</font>\n</p>"
	        "</font></body></html>";

}

void GraphicalInstaller::GenerateSuccessString(string& ret) {
 		  ret = "<html><body bgcolor='#d4d4d4'><font face='verdana, arial'><h1>Installation successful!</h1>\n";
		  ret += m_pWDF->GetTitle() + " v" + m_pWDF->GetVersion().weblication.ToString() + " was successfully installed.<p>You can access it from the SashXB Task Manager (sash-task-manager).";
	      ret +=  "</font></body></html>";
}

void GraphicalInstaller::GenerateFailureString(string& ret) {
	 ret = "<html><body bgcolor='#d4d4d4'><font face='verdana, arial'><h1>Installation failed!</h1>\n";
	 ret += m_pWDF->GetTitle() + " v" + m_pWDF->GetVersion().weblication.ToString() + " was not installed.";

	 ret +=  "</font></body></html>";
}
