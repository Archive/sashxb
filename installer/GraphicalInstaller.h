
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu, AJ Shankar

Provides the Graphical front-end to the weblication installer

*****************************************************************/

#ifndef GRAPHICAL_INSTALLER_H
#define GRAPHICAL_INSTALLER_H

#include <string>
#include <list>
#include <gtk/gtk.h>
#include <glib.h>
#include "wdf.h"
#include "gtkmozembed.h"

class BaseInstaller {
public:
	 virtual ~BaseInstaller() {}
	 virtual void SetStatusText(const char* s) = 0;
	 virtual void AdvanceProgressBar(bool increment_step = true, double micro_increment = 0.) = 0;
	 virtual void AddScreens(const vector<WDFInstallScreen>& screen) = 0;
	 virtual bool Update() const = 0;
	 virtual bool Begin() = 0;  
	 virtual void Reset() = 0;
	 virtual void Finish(bool succeeded, const string& err_message = "") = 0; 
	 virtual void Cancel() = 0;
};

class GraphicalInstaller : public BaseInstaller {
public:
	 // security is a pre-populated hash of security items; the installer
	 // invokes the SashSecurityPane widget to modify them if necessary
	 // base_path is the location to prepend to install screen URLs
	 GraphicalInstaller(WDF* w, SecurityHash * security, const std::string& base_path, 
						const bool from_web, const int num_other_steps);
	 GraphicalInstaller() {}
	 virtual ~GraphicalInstaller();
	 
	 // Start install screen interactivity
	 bool Begin();  
	 
	 void AddScreens(const vector<WDFInstallScreen>& screens);

	 void SetStatusText(const char* s);
	 // set first param to true if you want to have the step incremented for you;
	 // pass the second if you want the bar to advance a percentage of your current step
	 void AdvanceProgressBar(bool increment_step = true, double micro_increment = 0.);
	 
	 // Call Update() periodically to keep GTK and its events happy
	 // returns m_IsDone
	 bool Update() const;
	 
	 // sets m_IsDone to false, so that future Update()s will return something meaningful
	 void Reset();
	 
	 // for aborting
	 void Cancel();

	 // call on success or failure. 
	 // if wdf has a success or failure page, will display it;
	 // otherwise, will create a generic page
	 void Finish(bool succeeded, const string& err_message = "");
	 
 private:
	 // Initialize graphics/widgets
	 void InitGraphics();
	 void InitGraphicsWindow();  // Called only by InitGraphics()
	 void InitGraphicsButtons(); // Called only by InitGraphics()
	 
	 // Tell mozilla widget to load some URL
 	 void LoadURL(const string& aURL);
	 // Embedded gecko callback
	 static void LoadFinished(GtkMozEmbed* aEmbed, GraphicalInstaller* aGI);
	 
	 // Go to next screen
	 void NextScreen();
	 
	 // Go to previous screen
	 void PrevScreen();
  
	 void DisplayScreen(const WDFInstallScreen& c); 

	 // Update buttons
	 void UpdateButtons();
	 
	 // Prevent copying:
	 GraphicalInstaller& operator=(const GraphicalInstaller&);
	 GraphicalInstaller(const GraphicalInstaller&);
	 
	 // generate a description of the weblication's security settings
	 void GenerateSecurityString(string& out);
	 void GenerateSuccessString(string& ret);
	 void GenerateFailureString(string& ret);

	 // Button callbacks
	 static void ButtonBack(GtkWidget* apWidget, gpointer data);
	 static void ButtonNext(GtkWidget* apWidget, gpointer data);
	 static void ButtonFinish(GtkWidget* apWidget, gpointer data);
	 static void ButtonCancel(GtkWidget* apWidget, gpointer data);


	 static void OnSecurityButtonClicked(GtkButton * button, void * data);

  std::string m_Title;  // Install window title
  std::string m_BasePath; // the path to prepend to install screen urls
  const bool m_FromWeb; // are we getting install screen pages from the web?g
  
  int  m_NumSteps;      // Number of install steps
  int  m_CurStep;  // Current step
  unsigned int m_CurScreen;
  vector<WDFInstallScreen> m_Screens; // List of Screens (contain URLs etc)
  
  bool m_ScreenLoaded;   // false when mozilla widget is loading
  
  bool m_IsDone;         // true iff we finished installation successfully
  bool m_InstallSuccess; // true iff installation successful

  void GraphicalInstaller::SetInstallStatus(bool aStatus);

  bool m_HasSuccessPage, m_HasFailurePage;
  WDFInstallScreen m_SuccessPage, m_FailurePage;

  // GTK widgets:

  // the following two widgets are so that we can have a single install window which 
  // all install windows will use. We remove the Window Box from the pWindow
  // and replace it to make a new GraphicalInstaller
//  static 
  GtkWidget* m_pWindow;      // main window

  GtkWidget* m_pGecko;       // embedded Gecko rendering engine
  
  GtkWidget* m_pButtonBoxPrevNextCancel;   // box of buttons
  GtkWidget* m_pButtonBoxAcceptDecline;   // box of buttons
  GtkWidget* m_pButtonBoxFinish;   // box of buttons
  GtkWidget* m_pErrorBox; 

  GtkWidget* m_pPrev, *m_pNext, *m_pAccept, *m_pPrevAD;

  GtkWidget* m_pSecurityButton; // security box widgets
  
  GtkWidget*   m_pStatusBar; // status bar (owns m_pProgressBar)
  GtkProgress* m_pProgressBar;
  
  GtkWidget* m_pWindowBox;   // window box that contains all other widgets

  SecurityHash * m_pSecurity;
  WDF* m_pWDF;
  string m_SecurityFileName;
  string m_FinishFileName;
};


class DummyGraphicalInstaller : public BaseInstaller {
	 bool m_Cancel;
	 void SetStatusText(const char* s) {} 
	 void AdvanceProgressBar(bool increment_step = true, double micro_increment = 0.) { }
	 void AddScreens(const vector<WDFInstallScreen>& screen) { assert(false); }
	 bool Update() const { return m_Cancel; }
	 void Cancel() { m_Cancel = true; }
	 bool Begin() { assert(false); return false;}  
	 void Reset() { }
	 void Finish(bool succeeded, const string& err_message = "") { }
public:
	 DummyGraphicalInstaller() : m_Cancel(false) {}

};




#endif
