
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): Andrew Wu

Lets the installer put shortcuts to weblications on the GNOME start menu

*****************************************************************/


#include "InstallDesktop.h"
#include <iostream>
#include <glib.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-dentry.h>
#include <fstream>
#include <cassert>
#include "debugmsg.h"
using std::string;
using std::iostream;
using std::endl;
using std::ofstream;
using std::vector;

void create_sample_desktop_file(const string& filename) {
     ofstream tmp(filename.c_str());
     tmp << "[Desktop Entry]\n";
     tmp << "Name[en_US.ISO8859-1]=Empty Desktop";
}

string create_desktop_file(const string& GUID,
			   const string& wbl_name, 
			   const vector<string>& wbl_exec,
			   const string& wbl_icon) {
     const string gnome_home(getenv("HOME") + string("/.gnome/apps/"));
     const string desktop_name(GUID + ".desktop");
     const string desktop_file(gnome_home + desktop_name);

     // create an empty desktop file with GUID filename
     create_sample_desktop_file(desktop_file);

     DEBUGMSG(installer, "Creating desktop file [%s]\n", desktop_file.c_str());

     GnomeDesktopEntry* gde = gnome_desktop_entry_load_unconditional(desktop_file.c_str());

     // assert(gde);
     if (gde)
     {
	  // copy params into desktop entry
	  gde->name = strdup(wbl_name.c_str()); 
	  gde->icon = strdup(wbl_icon.c_str()); 

	  assert(wbl_exec.size() > 0);

	  // copy vector of strings into char**
	  gde->exec  = new char*[wbl_exec.size()];
	  for (unsigned int i=0; i < wbl_exec.size(); i++) {
	       gde->exec[i] = strdup(wbl_exec[i].c_str());
	  } 
	  gde->exec_length = wbl_exec.size();
     
	  gnome_desktop_entry_save(gde); // save to disk
	  // gnome_desktop_entry_free(gde); // free memory

	  // copy params into desktop entry
	  gde->name = strdup(wbl_name.c_str()); 
	  gde->icon = strdup(wbl_icon.c_str()); 

	  assert(wbl_exec.size() > 0);

	  // copy vector of strings into char**
	  gde->exec  = new char*[wbl_exec.size()];
	  for (unsigned int i=0; i < wbl_exec.size(); i++) {
	       gde->exec[i] = strdup(wbl_exec[i].c_str());
	  } 
	  gde->exec_length = wbl_exec.size();
       
	  gnome_desktop_entry_save(gde); // save to disk
	  // gnome_desktop_entry_free(gde); // free memory
     }
     return desktop_file;
}
