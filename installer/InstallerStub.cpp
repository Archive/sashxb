#include <popt.h>
#include <gnome.h>
#include <vector>
#include <signal.h>
#include "RuntimeTools.h"
#include "Installer.h"
#include "InstallationManager.h"
#include "SashGUID.h"
#include "WeblicationRegister.h"
#include "extensiontools.h"
#include "sash_version.h"

// options should include
// -u --uninstall                   uninstall the weblication
// -t --text-install                text-based installation
// -r --overwrite-registry [0|1]    (0, 1) => (never, always) overwrite previous registry, if it exists
// -o --install-over-previous [0|1] (0, 1) => (never, always) install over previous installation
// -f --force-quiet                 equivalent to -t -o 1 -r 0, plus picks safe answers to others queries
// -v --required-version w.x.y.z    only installs if component version is >= version specified here
int uninstall = false;
char* required_version = "0.0.0.0";
Installer* I = NULL;

static const struct poptOption options[] = {
	 {"uninstall", 'u', POPT_ARG_NONE, &uninstall, 0, N_("Uninstall the SashXB component")},
	 {"text-install", 't', POPT_ARG_NONE, &Installer::s_TextInstall, 0, N_("Text-based installation")},
	 {"overwrite-registry", 'r', POPT_ARG_INT, &Installer::s_OverwriteRegistry, 0, N_("(Never, always) overwrite previous registry"), N_("0,1")},
	 {"install-over-previous", 'o', POPT_ARG_INT, &Installer::s_InstallOverPrevious, 0, 
	   N_("(Never, always) install over previous installation"), N_("0,1")},
	 {"force-quiet", 'f', POPT_ARG_NONE, &Installer::s_ForceInstall, 0, N_("Equivalent to -t -o 1 -r 0, plus answers other queries safely")},
	 {"required-version", 'v', POPT_ARG_STRING, &required_version, 0, N_("Only installs if component version is >= version specified here"), N_("w.x.y.z")},
	 {NULL, '\0', 0, NULL, 0}
};

void PrintUsage() {
     cout << "SashXB Installer Usage:" << endl << endl;
	 cout << "To install a SashXB component:" << endl;
	 cout << "\tsash-install [name | URL | filename.wdf | GUID] [ -fortv ]" << endl << endl;
	 cout << "To uninstall a SashXB component:" << endl;
	 cout << "\tsash-install -u [name | filename.wdf | GUID] [ -ft ]" << endl << endl;
	 cout << "Try 'sash-install --help' for additional option information" << endl;
}


bool has_aborted = false;
void Cancel(int a) {
	 static bool done = false;
//	 signal(SIGINT, Cancel);
	 if (!done) { 
		  done = true; 
		  if (I) {
			   if (!I->Abort()) {
					cout << "Installation failed: Install interrupted by user" << endl;
					_exit(1);
			   } else {
					has_aborted = true;
			   }
		  }
	 }
}

int main(int argc, char* argv[]) {

	 poptContext c;
	 gnome_init_with_popt_table("SashXB Installer", ".1", argc, argv, options, 0, &c);

	 const char* filename = poptGetArg(c);
	 if (filename == NULL) {
		  PrintUsage(); 
		  exit(1);
	 }

	 vector<string> components;
	 components.push_back(GetSashComponentsDirectory());
	 SashRegisterSashRuntime(components); 
	 
	 SashErrorPushMode(Installer::s_TextInstall || Installer::s_ForceInstall 
					   ? SASH_ERROR_TEXT : SASH_ERROR_GNOME); 
	 while(filename) {
		  if (uninstall) {
			   static InstallationManager im;
			   string guid, descrip;
			   if (VerifyGUID(filename)) {
					guid = filename;
					descrip = im.GetInfo(guid).name;
			   } else {
					// first try opening it as a wdf file
					WDF w;
					if (w.OpenAmbiguousFile(filename)) {
						 guid = w.GetID();
						 descrip = w.GetTitle();
					} else {
						 // try doing it by name 
						 vector<SashExtensionItem> inst;
						 FindComponentByName(im, filename, inst);
						 if (inst.size() == 0) {
							  OutputError("Cannot identify '%s' " 
										  "as a filename, guid, or component name!", filename);
						 } else if (inst.size() > 1) {
							  string options;
							  for (unsigned int i = 0 ; i < inst.size() ; i++) 
								   options += "\t" + inst[i].name + "\n";
							  OutputError("%d possibilities for '%s':\n%sPlease be more specific!",
										  inst.size(), filename, options.c_str());
						 }
						 guid = inst[0].guid;
						 descrip = inst[0].name;
					}
			   } 

			   cout << "Uninstalling '" << descrip << "'..." << endl <<
					"\t(this might break other SashXB components!)" << endl;
			   im.Uninstall(guid, Installer::s_ForceInstall);

			   // finally, try letting the task manager know about the uninstall
			   WeblicationRegister w;
			   w.webl_name = "installer-refresh";
			   w.registerMe();
			   cout << "Finished." << endl;
		  } else {
			   signal(SIGINT, Cancel);
		  
			   I = new Installer(filename, Version(required_version));
			   InstallResult res = I->Install();
			   cout << (res == IR_SUCCEEDED ? "Installation succeeded!" : 
						"Installation failed: " + I->m_Error) << endl;
			   if (has_aborted) _exit(1);
			   delete I;
			   I = NULL;
		  }
		  filename = poptGetArg(c);
	 }
	 exit(0);
}
