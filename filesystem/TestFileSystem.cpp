
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar, Andrew Wu

Comprehensive test of the FileSystem class

*****************************************************************/

// Created July 2000

#include "FileSystem.h"
#include <string.h>
#include <string>

#include <fstream>
#include <glib.h>

void testCopyDelFolder(FileSystem& fs);
void testMoveFolder(FileSystem& fs);
void testFolderOps(FileSystem& fs);

// create 'filename', filling it optionally with 'size' bytes
void touch_file(const string& filename, int size = 0) {
     ofstream of(filename.c_str());

     for (int i=0; i < size; i++) {
	  of << "0";
     }
}

void unitTestCheckOutput(bool rv) {
     if (rv) {
	  cout << "OK" << endl;
     } else {
	  cout << "FAILED!" << endl;
	  exit(1);
     }
}

int main(int argc, char* argv[]) {
	 // replace this with a file you know to exist in this directory
	 // I can't use argv[0] because stupid libtool invokes some other executable
 	 string cpname = "TestFileSystem";
 	 FileSystem fs;

	 cout << endl;

	 cout << "Checking GetTempName() ... " ;
	 assert(FileSystem::GetTempName().length() > 0);
	 cout << " OK" << endl;

	 cout << "Checking FileExists()... " ;
	 unitTestCheckOutput(
	      fs.FileExists(cpname) && ! fs.FileExists(fs.GetTempName())); 

	 cout << "Checking CopyFile()... " << endl;
	 cout << "\tOverwrite... ";
	 unitTestCheckOutput(
	      fs.CopyFile(cpname, cpname + "tmp", true) && fs.FileExists(cpname + "tmp"));

	 cout << "\tNo overwrite... ";
	 unitTestCheckOutput(
	      ! fs.CopyFile(cpname, cpname + "tmp", false) && fs.FileExists(cpname + "tmp"));

	 cout << "Checking MoveFile()... ";
	 unitTestCheckOutput(
	      fs.MoveFile(cpname + "tmp", cpname + "mv") && 
	      fs.FileExists(cpname + "mv") && ! fs.FileExists(cpname+"tmp"));
	 
	 cout << "Checking DeleteFile()... ";
	 unitTestCheckOutput(fs.DeleteFile(cpname + "mv") && ! fs.FileExists(cpname + "mv"));

	 cout << endl;

	 cout << "Checking File I/O..." << endl;
	 cout << "Opening file for writing... ";
	 string tn = fs.GetTempName();
	 int fd = fs.OpenFile(tn, false);
	 unitTestCheckOutput(fd >= 0);
	 
	 cout << "Writing to file... ";
	 const char* data = "This is some data.";
	 unitTestCheckOutput(
	      fs.WriteToFile(data, strlen(data), fd) == (int) strlen(data));
	 
	 cout << "Closing file... ";
	 fs.CloseFile(fd);
	 cout << "OK" << endl;

	 cout << endl;

	 cout << "Testing write on closed file... ";
	 unitTestCheckOutput(
	      fs.WriteToFile(data, strlen(data), fd) == -1);

	 cout << endl;

	 cout << "Opening file for reading... ";
	 fd = fs.OpenFile(tn, true);
	 unitTestCheckOutput(fd >= 0);

	 cout << "Reading from file... ";
	 char data2[strlen(data) + 1];
	 fs.ReadFromFile(data2, strlen(data), fd);
	 data2[strlen(data)] = '\0';
	 unitTestCheckOutput(
	      ! strcmp(data, data2));

	 cout << "Closing file... ";
	 fs.CloseFile(fd);
	 cout << "OK" << endl;

	 fs.DeleteFile(tn);
	 cout << endl;

	 cout << "Checking IsLocal()... ";
	 gchar* blob = g_get_current_dir();
	 string dir = blob;
	 g_free(blob);
	 unitTestCheckOutput(fs.IsLocal("hello/goodbye", dir) && fs.IsLocal("hello/../goodbye", dir) && 
						 ! fs.IsLocal("hello/../../goodbye", dir) && fs.IsLocal(dir, dir) &&
						 fs.IsLocal(dir+"/"+"hi", dir) && ! fs.IsLocal(dir+"/..", dir) &&
						 ! fs.IsLocal("/etc/", dir) );

	 // Test folder operations:
	 cout << "Checking FolderExists()... " ;
	 unitTestCheckOutput(
	      fs.FolderExists("/usr") && ! fs.FolderExists(fs.GetTempName()) );

	 cout << "Checking CreateFolder()... " ;
	 string tfd = fs.GetTempName();
	 unitTestCheckOutput(
	      fs.CreateFolder(tfd) && fs.FolderExists(tfd) );
	 cout << "Checking CreateFolder() again... " ;
	 string tfe = fs.GetTempName();
	 string tff = fs.GetTempName();
	 unitTestCheckOutput(
	      fs.CreateFolder(tfe+"/"+tff) && fs.FolderExists(tfe+"/"+tff) );

	 cout << "Checking DeleteFolder()... " ;
	 unitTestCheckOutput(
	      fs.DeleteFolder(tfd) && ! fs.FolderExists(tfd) );
	 cout << "Checking DeleteFolder() again... " ;
	 unitTestCheckOutput(
	      fs.DeleteFolder(tfe) && ! fs.FolderExists(tfe) );

	 cout << "Checking FolderSize()... ";
	 int binsize = fs.FolderSize("/usr/bin");
	 cout << "Size of /usr/bin is " << binsize << endl;

	 cout << "Checking CreateSymlink()... ";
	 FileSystem::CreateSymlink("/usr/bin/", "test_link/");
	 unitTestCheckOutput(binsize == fs.FolderSize("test_link"));
	 cout << "Checking IsSymlink()... ";
 	 unitTestCheckOutput(FileSystem::IsSymlink("test_link/") &&
						 FileSystem::IsSymlink("test_link") &&
						 ! FileSystem::IsSymlink("TestFileSystem"));

	 cout << "Deleting symlink with DeleteFile()... ";
	 unitTestCheckOutput(FileSystem::DeleteFile("test_link"));

	 testCopyDelFolder(fs);
	 testMoveFolder(fs);
	 testFolderOps(fs);

 	 cout << endl << "All tests passed!" << endl << endl;
}

void testCopyDelFolder(FileSystem& fs) {
     // Test CopyFolder()
     cout << "Checking CopyFolder()... " ;
     string temp_name = fs.GetTempName();
     string temp_dir  = "/tmp/" + temp_name;   
     bool rv = fs.CreateFolder(temp_dir);
     assert(rv); // FileSystem::CreateFolder() failure

     string temp_dir_2 = temp_dir + "/" + temp_name + "-sub";
     rv = fs.CreateFolder(temp_dir_2);
     assert(rv); // FileSystem::CreateFolder() failure

     string test_postfix = "/test_copy_folder.tmp";

     string temp_file = temp_dir_2 + test_postfix;
     touch_file(temp_file);
     assert(FileSystem::FileExists(temp_file)); // TestFileSystem: touch_file() failure

     string temp_dir_dest = temp_dir + "-dest";
     string temp_dir_dest2 = temp_dir_dest + "/" + temp_name + "-sub";
     FileSystem::CopyFolder(temp_dir, temp_dir_dest);
     
     string check_file = temp_dir_dest2 + "/" + test_postfix;
     rv = FileSystem::FileExists(check_file);
     
//     cout << "FileSystem::FileExists(temp_dir_dest2 + \"/\" + test_postfix) = " 
// 	  << FileSystem::FileExists(temp_dir_dest2 + "/" + test_postfix) 
// 	  << check_file << endl;
     unitTestCheckOutput(rv);

     cout << "Checking recursive DeleteFolder()... " ;
     rv = FileSystem::DeleteFolder(temp_dir);
     rv = rv && FileSystem::DeleteFolder(temp_dir_dest);
     unitTestCheckOutput(rv);
}

void testMoveFolder(FileSystem& fs) {
     // Test MoveFolder()
     cout << "Checking MoveFolder()... " ;
     string tfd_source = "/tmp/" + fs.GetTempName() + "-move";
     string tfd_dest   = tfd_source + "-move-dest";

     bool rv = fs.CreateFolder(tfd_source);
     assert(rv); // FileSystem::CreateFolder() failure

     // put a folder in the temp created dir
     string tfn_postfix_file = "test_file_system.tmp";
     string tfn_postfix = "/" + tfn_postfix_file;
     touch_file(tfd_source + tfn_postfix);

     // make sure tfd_dest does not exist
     if (fs.FolderExists(tfd_dest)) {
	  fs.DeleteFolder(tfd_dest);
     }
     
     rv = fs.MoveFolder(tfd_source, tfd_dest);
     rv = rv && fs.FolderExists(tfd_dest) && !fs.FolderExists(tfd_source);
     rv = rv && FileSystem::FileExists(tfd_dest + tfn_postfix) ;
     // OK iff MoveFolder returns true, tfd_dest exists, tfd_source does not,
     //  and file in moved folder still exists
     unitTestCheckOutput(rv);

     rv = fs.DeleteFolder(tfd_dest);
     assert(rv); // FileSystem::DeleteFolder() failure

     // Test EnumFiles()
     cout << "Checking EnumFiles()... " ;

     rv = FileSystem::CreateFolder(tfd_source);
     assert(rv); // FileSystem::CreateFolder() failure
     touch_file(tfd_source + tfn_postfix);
     assert(FileSystem::FileExists(tfd_source + tfn_postfix)); // touch_file() failure

     vector<string> vs = FileSystem::EnumFiles(tfd_source);
     rv = (vs.size() == 1 && vs[0] == tfn_postfix_file);
     unitTestCheckOutput(rv);

     cout << "Checking EnumDirs()... " ;
     rv = FileSystem::CreateFolder(tfd_source + "/" + string("dir0"));
     assert(rv); // FileSystem::CreateFolder() failure
     vs = FileSystem::EnumDirs(tfd_source);
     rv = (vs.size() == 1 && vs[0] == "dir0");
     unitTestCheckOutput(rv);

     rv = FileSystem::DeleteFolder(tfd_source);
     assert(rv); // FileSystem::DeleteFolder() failure
}

void testFolderOps(FileSystem& fs) {
     cout << "Checking FileSize()... " ;
     
     string tmp_file = FileSystem::GetTempName();
     string tmp_url  = "/tmp/" + tmp_file;

     
     // make sure tmp_url does not exist
     if (FileSystem::FileExists(tmp_url)) {
		  FileSystem::DeleteFile(tmp_url);
     }
     
     int size = FileSystem::FileSize(tmp_url);
     bool rv0  = (size == -1);
     if (!rv0) {
	  cout << "[Error: FileSize() of non-existent file returned " << size
	       << " instead of " << "-1!] " << flush;
     }

     touch_file(tmp_url);
     size = FileSystem::FileSize(tmp_url);
     bool rv1 = (size == 0);
     if (!rv1) {
	  cout << "[Error: FileSize() of empty file returned " << size
	       << "instead of 0!] " << flush;
     }

     bool rv = FileSystem::DeleteFile(tmp_url);
     assert(rv); // FileSystem::DeleteFile() failure

     int bytes = 512;
     touch_file(tmp_url, bytes);
     size = FileSystem::FileSize(tmp_url);
     bool rv2 = (size == bytes);
     if (!rv2) {
	  cout << "[Error: FileSize() of " << bytes << " byte file returned "
	       << size << " instead!]" << flush;
     }

     rv = FileSystem::DeleteFile(tmp_url);
     assert(rv); // FileSystem::DeleteFile() failure

     unitTestCheckOutput(rv0 && rv1 && rv2);
}
