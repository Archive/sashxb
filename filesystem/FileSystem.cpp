
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar, Andrew Wu

Provides basic file system manipulation and file I/O

*****************************************************************/

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include "FileSystem.h"
#include "debugmsg.h"
#include <string>
#include <hash_map>

using std::string;
using std::hash_map;
using std::vector;

const int buf_size = 4096;

FileSystem::~FileSystem() {
  hash_map<int, FILE*>::iterator bi = m_FileHandles.begin(), ei = m_FileHandles.end();
  DEBUGMSG(filesystem, "Destructing... \n");
  while (bi != ei) {
    if ((*bi).second != NULL) {
      DEBUGMSG(filesystem, "Closing filehandle %d\n", (*bi).first);
      fclose((*bi).second);
    }
    ++bi;
  }

}

string FileSystem::GetTempName() {
  char name[] = "sashXXXXXX";
  mktemp(name);
  return string(name);
}

// -------------------------------

bool FileSystem::IsSymlink(const string& fname) {
	 struct stat s;
	 // get rid of trailling slash... otherwise OS will follow symlink
	 if (fname[fname.length()-1] == '/') {
		  string newname(fname);
		  newname.resize(newname.length()-1);
		  return (! lstat(newname.c_str(), &s) && S_ISLNK(s.st_mode));
	 } else 
		  return (! lstat(fname.c_str(), &s) && S_ISLNK(s.st_mode));
}

bool FileSystem::FileExists(const string& fname) {
  struct stat s;
  if (! stat(fname.c_str(), &s)) {
    return (S_ISREG(s.st_mode));
  }
  return false;
}

bool FileSystem::CopyFile(const string& source, 
			  const string& dest, 
			  const bool overwrite) {
  FILE *in = NULL;
  FILE *out = NULL;
  
  DEBUGMSG(filesystem, "copying file %s to %s ...\n", 
	   source.c_str(), dest.c_str());

  if (! overwrite && FileExists(dest) || ! FileExists(source)) {
    return false;
  }

  char buffer[buf_size];
  in = fopen(source.c_str(), "r");
  out = fopen(dest.c_str(), "w");
  if (out == NULL || in == NULL)
    goto failure;
  
  while(! feof(in)) {
    int read = fread(buffer, sizeof(char), buf_size, in);
    if (read == -1) 
      goto failure;
    int wrote = fwrite(buffer, sizeof(char), read, out);
    if (wrote == -1) 
      goto failure;
  }

  fclose(out);
  fclose(in);

  DEBUGMSG(filesystem, "[SUCCEEDED]\n");

  return true;

 failure:

  if (out != NULL)
    fclose(out);
  if (in != NULL)
    fclose(out);
  return false;
  
}

bool FileSystem::MoveFile(const string& source, const string& dest) {

  if (! rename(source.c_str(), dest.c_str())) {
    return true;
  } else if (CopyFile(source, dest, true)) {
    return DeleteFile(source);
  }

  return false;
}

bool FileSystem::DeleteFile(const string& fname) {
  DEBUGMSG(filesystem, "Deleting file %s\n", fname.c_str());

  return (! remove(fname.c_str()));
}

// -------------------------------

int FileSystem::OpenFile(const string& fname, const bool read) {
  int fdesc;

  DEBUGMSG(filesystem, "Opening file %s\n", fname.c_str());

  FILE* f = fopen(fname.c_str(), (read ? "r" : "w+"));
  if (f) {
    fdesc = m_HandleCount++;
    m_FileHandles[fdesc] = f;
    DEBUGMSG(filesystem, "[SUCCEEDED]\n");
  } else {
    fdesc = -1;
    DEBUGMSG(filesystem, "[FAILED]\n");
  }
	 
  return fdesc;
}

void FileSystem::CloseFile(const int fdesc) {
  FILE* f = m_FileHandles[fdesc];
  if (f) {
    DEBUGMSG(filesystem, "Closing filehandle %d\n", fdesc);

    fclose(f);
    m_FileHandles[fdesc] = NULL;
  }
}

int FileSystem::SetFilePos(const int fdesc, const int pos) {
  FILE* f = m_FileHandles[fdesc];
  if (f) {
    return fseek(f, pos, SEEK_SET);
  }
  return -1;
}

int FileSystem::WriteToFile(const char* data, const int length, const int fdesc) {
  FILE* f = m_FileHandles[fdesc];
  DEBUGMSG(filesystem, "Writing %d bytes to filehandle %d\n", 
	   length, fdesc);
  
  if (f) {
    return fwrite(data, sizeof(char), length, f);
  }
  return -1;
}

int FileSystem::ReadFromFile(char* data, const int length, const int fdesc) const {
  // FILE* f = m_FileHandles[fdesc];
     
  DEBUGMSG(filesystem, "Reading %d bytes from filehandle %d\n", length, fdesc);

  file_map::const_iterator it = m_FileHandles.find(fdesc);
  if (it == m_FileHandles.end()) {

    DEBUGMSG(filesystem, "Invalid filehandle %d!\n", fdesc);

    return -1;
  }
	 
  FILE* f = it->second;

  if (f) {
    return fread(data, sizeof(char), length, f);
  }
  return -1;
}

bool FileSystem::FolderExists(const string& dname) {
  struct stat s;
  if (! stat(dname.c_str(), &s)) {
    return (S_ISDIR(s.st_mode));
  }
  return false;

}

int FileSystem::FileSize(const string& fname) {
  struct stat s;
  if (stat(fname.c_str(), &s) == -1) {
    return -1;
  }

  return s.st_size;
}

int FileSystem::FileLastModified(const string& fname) {
  struct stat s;
  if (stat(fname.c_str(), &s) == -1) {
    return -1;
  }
  return s.st_mtime;
}

int FileSystem::FileLastAccessed(const string& fname) {
  struct stat s;
  if (stat(fname.c_str(), &s) == -1) {
    return -1;
  }

  return s.st_atime;
}

bool FileSystem::IsLocal(const string& path, const string& localdir) {
	 // vectorize path 
	 vector<string> s;
	 bool isabs = false;
	 unsigned int pos = 0, next = 0;
	 if (path[0] == '/') { 
		  isabs = true;
		  pos++;
	 }
	 do {
		  if (pos > path.size()) break;
		  next = path.find('/', pos);
		  string blah = path.substr(pos, next-pos);
		  if (blah == "..") {
			   if (s.size() < 1) {
					if (!isabs) return false;
			   } else {
					s.pop_back();
			   }
		  } else if (blah != ".") 
			   s.push_back(blah);
		  pos = next+1;
	 } while (next != string::npos);
			
	 if (path[0] != '/') return true;
	 string ne;
	 vector<string>::iterator ai = s.begin(), bi = s.end();
	 while (ai != bi) {
		  ne += "/" + *ai;
		  ++ai;
	 } 
	 return (ne.compare(localdir, 0, localdir.size()) == 0);
}

bool FileSystem::CreateSymlink(const string& resource, const string& link_name) {
	 return (symlink(resource.c_str(), link_name.c_str()) == 0);
}

// -------------------------------

//
// Generic folder recursing function that will apply FolderOp to all subfolders
//  and FileOp to all subfiles.
//
// Note that FolderOp will *not* be applied to the directory passed in (aSource),
//  but only to its subfolders!
//

template <class FolderOp, class FileOp>
bool FolderRecurse(FolderOp& aFolderOp, FileOp& aFileOp,
		   const string& aSource, const string& aDest = "") {
  //append / if necessary
  string source(aSource);
  if (source[source.size()-1] != '/') {
    source += '/';
  }

  string dest(aDest);
  if (dest[dest.size()-1] != '/') {
    dest += '/';
  }

  DIR* dir = opendir(source.c_str());

  if (!dir) {
    DEBUGMSG(filesystem, "[FolderRecurse] Couldn't opendir (%s)\n", 
	     source.c_str());
    return false;
  } 

  // copy all directory entries:
  dirent* d = readdir(dir);

  while (d) {
    string name(d->d_name);
    string source_name(source + name);
  
    if (FileSystem::FolderExists(source_name)) {
      // ignore blah/. and blah/..
      if (source_name.size() > 0 && source_name[ source_name.size()-1 ] != '.') {
	aFolderOp(source,name, dest,name);
      }
    } else {
      aFileOp(source,name, dest,name);
    }
    d = readdir(dir);
  }

  closedir(dir);

  return true;
}

// helper structs for FileSystem::CopyFolder
struct FolderCopy {
  bool operator()(const string& source_dir, const string& source_file, 
		  const string& dest_dir,   const string& dest_file) const {
    // copy folder (create, then recursively copy folder's contents)
    const string dest(dest_dir+dest_file);
    return FileSystem::CreateFolder(dest_dir) && 
      FileSystem::CreateFolder(dest) &&
      FileSystem::CopyFolder(source_dir+source_file, dest);
  }
};

struct FileCopy {
  bool operator()(const string& source_dir, const string& source_file, 
		  const string& dest_dir,   const string& dest_file) const {
    return FileSystem::CopyFile(source_dir+source_file,
				dest_dir+dest_file);
  }
};


bool FileSystem::CopyFolder(const string& aSource, const string& aDest) {
  DEBUGMSG(filesystem, "Copying folder %s to %s\n", 
	   aSource.c_str(), aDest.c_str());

  //ignore empty folder strings
  if ((aSource.size() == 0) || (aDest.size() == 0)) {
    return false;
  }

  FolderCopy foc;
  FileCopy   fic;

  //recurse, applying folder copy to folders, and file copy to files
  FolderRecurse<FolderCopy, FileCopy>(foc,fic, 
				      aSource, aDest);

  return true;
}

void FolderSizeHelper(const string& fname);
struct FolderSize {
	 static int total_size;
	 bool operator()(const string& foldername, const string& filename,
		  const string& ignored1,   const string& ignored2) {
	   const string folder(foldername + filename);
	   if (FileSystem::IsSymlink(folder)) return true;
	   FolderSizeHelper(folder);
	   return true;
  }
};
int FolderSize::total_size = 0;

struct FileSize {
  bool operator()(const string& foldername, const string& filename,
		  const string& ignored1,   const string& ignored2) {
	   const string file(foldername + filename);
	   if (FileSystem::IsSymlink(file)) return true;
	   FolderSize::total_size += FileSystem::FileSize(file);
	   return true;
  }
};

void FolderSizeHelper(const string& fname) {
  FolderSize fod;
  FileSize   fid;

  //recurse, sizing up folders.
  FolderRecurse<FolderSize, FileSize>(fod,fid, fname);
}

int FileSystem::FolderSize(const string& fname) {
  FolderSize::total_size = 0;
  FolderSizeHelper(fname);
  return FolderSize::total_size;
}

// helper structs for FileSystem::DeleteFolder
struct FolderDelete {
  bool operator()(const string& foldername, const string& filename,
		  const string& ignored1,   const string& ignored2) {
    // delete subfolders/files
    const string folder(foldername+filename);
    bool rv = FileSystem::DeleteFolder(folder);
    // delete the actual folder
    remove(folder.c_str());
    return rv;
  } 
};

struct FileDelete {
  bool operator()(const string& foldername, const string& filename,
		  const string& ignored1,   const string& ignored2) {
    return FileSystem::DeleteFile(foldername+filename);
  }
};

bool FileSystem::DeleteFolder(const string& fname) {
  FolderDelete fod;
  FileDelete   fid;

  //recurse, deleting folders.
  bool rv = FolderRecurse<FolderDelete, FileDelete>(
						    fod,fid, fname);

  //delete top folder
  return (! remove(fname.c_str())) && rv;
}


bool FileSystem::CreateFolder(const string& fname) {
	 unsigned int pos = 0;
	 if (fname[0] == '/') pos++;
	 do {
		  pos = fname.find('/', pos);
		  string subdir = fname.substr(0, pos);
		  if (!FolderExists(subdir) && mkdir(subdir.c_str(), S_IRWXU)) 
			   return false;
		  if (pos == string::npos) break;
		  pos++;
	 } while (pos < fname.length());
	 return true;
}

bool FileSystem::MoveFolder(const string& source, const string& dest) {

  if (! rename(source.c_str(), dest.c_str())) {
    return true;
  } else if (CopyFolder(source, dest)) {
    return DeleteFolder(source);
  }

  return false;

}

struct NoOp {
  bool operator()(const string& ignored0, const string& ignored1,
		  const string& ignored2, const string& ignored3) const {
    return true;
  }
};

struct PushbackSourceFileString {
  bool operator()(const string& source_dir, const string& source_file,
		  const string& dest_dir,   const string& dest_file) {
    m_strings.push_back(source_file);
    return true;
  }
     
  vector<string> getStrings() const {
    return m_strings;
  }
private:
  vector<string> m_strings;
};

vector<string> FileSystem::EnumFiles(const string& dname) {
  if (dname.size() == 0) {
    return vector<string>();
  }

  NoOp nop;
  PushbackSourceFileString files;

  FolderRecurse<NoOp /* folder */, PushbackSourceFileString>(
							     nop /* FolderOp */, files /* FileOp */,  dname);
  return files.getStrings();
}


vector<string> FileSystem::EnumDirs(const string& dname) {
  if (dname.size() == 0) {
    return vector<string>();
  }

  NoOp nop;
  PushbackSourceFileString dirs;

  FolderRecurse<PushbackSourceFileString, NoOp /* file */>(
							   dirs /* FolderOp */, nop /* FileOp */,  dname);
  return dirs.getStrings();
}


