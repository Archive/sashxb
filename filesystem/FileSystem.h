
/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar, Andrew Wu

Provides basic file system manipulation and file I/O

*****************************************************************/

// Filesystem extension
// provides basic filesystem functionality
// used internally to move files around

// AJ Shankar, Andrew Wu
// Created July 2000

#ifndef FILE_SYSTEM_H
#define FILE_SYSTEM_H


#include <string>
#include <hash_map>
#include <vector>
#include <cstdio>

class FileSystem {
public:
	 FileSystem() : m_HandleCount(0) { }
	 ~FileSystem();

	 // creates a temporary filename
	 static std::string GetTempName();

	 // copies a file from one location to another
	 // destination filename must be specified as part of the dest string
	 static bool CopyFile(const std::string& source, const std::string& dest, 
			      const bool overwrite = false);
	 // same as above except moves file
	 static bool MoveFile(const std::string& fname, const std::string& dest);
	 static bool DeleteFile(const std::string& fname);

	 // returns true if file exists and is a normal file
	 static bool FileExists(const std::string& fname);
	 // returns the length of a file. returns -1 if file is not valid.
	 static int FileSize(const std::string& fname);
	 static int FileLastModified(const std::string& fname);
	 static int FileLastAccessed(const std::string& fname);
	 // creates a symlink. delete symlinked dirs with deletefile! -- otherwise it'll deep-delete 
	 // the resource itself
	 static bool CreateSymlink(const std::string& resource, const std::string& link_name);
	 static bool IsSymlink(const std::string& fname);

	 // takes two arguments, the path to be checked, and the directory that is 
	 // considered local. The path to be checked can be relative or absolute;
	 // all ..'s will be condensed. The second argument must obviously be absolute
	 // returns true if the first path resolves to the second or a subdir of the second
	 static bool IsLocal(const std::string& path, const std::string& localdir);

	 // opens a file for reading (or writing if read = false), and returns a file descriptor
	 // or -1 if failed to open
	 int OpenFile(const std::string& fname, const bool read);
	 // closes the file pointed to by the file descriptor
	 void CloseFile(const int fdesc);
	 // writes length bytes of data to the file pointed to by the fd; 
	 // returns length written, or -1 if filehandle is invalid
	 int WriteToFile(const char* data, const int length, const int fdesc);
	 // reads length bytes from fdesc into data (memory must already have been allocated!);
	 // returns length read, or -1 if filehandle is invalid
	 int ReadFromFile(char* data, const int length, const int fdesc) const;
	 // sets the position of the file descriptor to pos bytes from the beginning
	 int SetFilePos(const int fdesc, const int pos);

	 // creates a folder with R, W, X permissions, and as many parent dirs as necessary
	 // (e.g. "/a/b/c" will create a and b, as needed, in addition to c)
	 static bool CreateFolder(const std::string& fname);
	 // returns true if the folder exists and is an actual directory
	 static bool FolderExists(const std::string& dname);
	 // returns the size of the folder and all contained subfolders and files;
	 // does not count symlinks!
	 static int FolderSize(const std::string& fname);

	 // operations on folders. results are undefined (read not tested)
	 //  if source is a substring of dest. CopyFolder and MoveFolder
	 //  will recursively copy/move on subfolders.
	 static bool DeleteFolder(const std::string& fname);
	 static bool CopyFolder(const std::string& source, const std::string& dest);
	 static bool MoveFolder(const std::string& fname, const std::string& dest);

	 // returns a vector of strings of the files in a directory
	 static std::vector<std::string> EnumFiles(const std::string& dname);

	 // returns a vector of strings of the subdirs in a directory
	 static std::vector<std::string> EnumDirs(const std::string& dname);


private:
	 FileSystem(const FileSystem&); // prevent copying
	 FileSystem& operator=(const FileSystem&);

	 typedef hash_map<int, FILE*> file_map;

	 // hash of open filehandles
	 file_map m_FileHandles;

	 // current handle count
	 int m_HandleCount;
};

#endif
