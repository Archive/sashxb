
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "nsISupports.idl"


[ptr] native nativeFile(FILE);
[ptr] native nativeJSContext(JSContext);

[scriptable, uuid(6506b551-575e-48d6-8dbd-8494cd96f78b)]
interface sashIJSEmbed : nsISupports
{
    void StartJSInterpreter();
	void ShutdownJSInterpreter();
	[noscript] void SetOutput(in nativeFile outputFile);
	void SetSecurityMode(in long mode);

	void EvalScript(in string script);
	void EvalScriptFile(in string fname);

    [noscript] readonly attribute nativeJSContext scriptContext;
};

%{ C++
#include <nsComponentManagerUtils.h>
#define SASHJSEMBED_CID {0xED354832, 0x6C1C, 0x4576, {0x96, 0x84, 0xD7, 0x6B, 0x4C, 0xC9, 0x37, 0x91}}

inline NS_COM nsresult 
NewSashJSEmbed(sashIJSEmbed ** jsEmbed)
{
   nsresult rv;
   rv = nsComponentManager::CreateInstance((nsCID)SASHJSEMBED_CID, NULL, NS_GET_IID(sashIJSEmbed),
							(void**)jsEmbed);
   return rv;
}
%}
