
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _SECMAN_H_
#define _SECMAN_H_
#include "sashISecurityManager.h"
#include "security.h"
#include "debugmsg.h"
#include <nsMemory.h>

// Functions to assert global security settings; if security standard is not met,
// they query the user; if user okays, functions return. Otherwise, they exit().
// So basically call these functions to assert settings and if they return at all
// you can continue

// To ensure that a security setting is met, call
//     AssertSecurity(security_manager, security_setting, val_to_check);

// If the setting is specific to your extension, call
//     AssertSecurity(security_manager, your_guid, security_setting, val_to_check);

// To find out to what a security setting is set, call
//     QuerySecurity(security_manager, security_setting, pointer_to_[bool,float,string]);
// and it will fill in the pointer for you. For string enums, pass in a float* and it
// will return the index of the enumerated value to which it is set.

// Similarly, to query a security setting specific to your extension, call
//     QuerySecurity(security_manager, your_guid, security_setting, pointer_to_[bool,float,string]);


inline void AssertSecurity(sashISecurityManager* s, const SecuritySetting setting, const PRBool val) {
	 DEBUGMSG(secman, "AssertSecurity on bool val\n");
	 s->AssertBool(setting, val);
}

 // works for Numbers or Std::StringEnums
inline void AssertSecurity(sashISecurityManager* s, const SecuritySetting setting, const float val) {
	 DEBUGMSG(secman, "AssertSecurity on num val\n");
	 s->AssertNumOrEnum(setting, val);
}

inline void AssertSecurity(sashISecurityManager* s, const SecuritySetting setting, const std::string& val) {
	 DEBUGMSG(secman, "AssertSecurity on string val\n");
	 s->AssertString(setting, val.c_str());
}

// functions to assert local security settings for your given extension
inline void AssertSecurity(sashISecurityManager* s, const std::string& extension_guid, const SecuritySetting setting, const PRBool val) {
	 DEBUGMSG(secman, "AssertSecurity on ext, bool val\n");
	 s->AssertExtBool(extension_guid.c_str(), setting, val);
}

inline void AssertSecurity(sashISecurityManager* s, const std::string& extension_guid, const SecuritySetting setting, const float val) {
	 DEBUGMSG(secman, "AssertSecurity on ext, num val\n");
	 s->AssertExtNumOrEnum(extension_guid.c_str(), setting, val);
}

inline void AssertSecurity(sashISecurityManager* s, const std::string& extension_guid, const SecuritySetting setting, const std::string& val) {
	 DEBUGMSG(secman, "AssertSecurity on ext, string val\n");
	 s->AssertExtString(extension_guid.c_str(), setting, val.c_str());
}
 
// functions to query global security settings; stores value in third argument
inline void QuerySecurity(sashISecurityManager* s, const SecuritySetting setting, PRBool* val) {
	 s->QueryBool(setting, val);
}

// works for Numbers or StringEnums
inline void QuerySecurity(sashISecurityManager* s, const SecuritySetting setting, float* val) {
	 s->QueryNumOrEnum(setting, val);
}

inline void QuerySecurity(sashISecurityManager* s, const SecuritySetting setting, std::string* val) {
	 char* ret;
	 s->QueryString(setting, &ret);
	 *val = ret;
	 nsMemory::Free(ret);
}

// functions to query local security settings for your given extension
inline void QuerySecurity(sashISecurityManager* s, const std::string& extension_guid, const SecuritySetting setting, PRBool* val) {
	 s->QueryExtBool(extension_guid.c_str(), setting, val);
}

inline void QuerySecurity(sashISecurityManager* s, const std::string& extension_guid, const SecuritySetting setting, float* val) {
	 s->QueryExtNumOrEnum(extension_guid.c_str(), setting, val);
}

inline void QuerySecurity(sashISecurityManager* s, const std::string& extension_guid, const SecuritySetting setting, std::string* val) {
	 char* ret;
	 s->QueryExtString(extension_guid.c_str(), setting, &ret);
	 *val = ret;
	 nsMemory::Free(ret);
}

// Security Violation.
inline void SecurityViolation(sashISecurityManager* s, const std::string& extension_guid, const SecuritySetting setting, const PRBool val) {
	 s->SecurityViolationBool(extension_guid.c_str(), setting, val);
}

inline void SecurityViolation(sashISecurityManager* s, const std::string& extension_guid, const SecuritySetting setting, const float val) {
	 s->SecurityViolationNumOrEnum(extension_guid.c_str(), setting, val);
}

inline void SecurityViolation(sashISecurityManager* s, const std::string& extension_guid, const SecuritySetting setting, const string &val) {
	 s->SecurityViolationString(extension_guid.c_str(), setting, val.c_str());
}



#endif // _SECMAN_H_
