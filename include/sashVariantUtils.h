
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef SASH_VARIANT_UTILS_H
#define SASH_VARIANT_UTILS_H

#include "nsIVariant.h"
#include "nsComponentManagerUtils.h"
#include "xpcomtools.h"
#include "xptinfo.h"
#include <string>
#include <vector>
#include <stdio.h>
#include <math.h>
#include "nsCRT.h"
#include "extensiontools.h"

inline NS_COM nsresult 
NewVariant(nsIVariant** aInstancePtrResult)
{
  nsCOMPtr <nsIWritableVariant> variant = do_CreateInstance(NS_VARIANT_CONTRACTID);
  *aInstancePtrResult = variant;
  NS_ADDREF(*aInstancePtrResult);

  return NS_OK;
}

inline void WritableVariantSetEmptyArray(nsIWritableVariant * wv) {
	 wv->SetAsEmptyArray();
}

inline PRUint16 VariantGetType(nsIVariant * v) {
  if (v) {
	PRUint16 vtype;
	v->GetDataType(&vtype);
	return vtype;
  }
  return nsIDataType::VTYPE_EMPTY;
}

inline bool VariantCheckType(nsIVariant * v, PRUint16 type) {
  return VariantGetType(v) == type;
}

// ------------- variant type query functions --------------
inline bool VariantIsBoolean(nsIVariant * v) {
  return VariantCheckType(v, nsIDataType::VTYPE_BOOL);
}

inline bool VariantIsString(nsIVariant * v) {
  PRUint16 type = VariantGetType(v);
  return (type == nsIDataType::VTYPE_ASTRING || type == nsIDataType::VTYPE_CHAR_STR ||
		  type == nsIDataType::VTYPE_WCHAR_STR || type == nsIDataType::VTYPE_STRING_SIZE_IS ||
		  type == nsIDataType::VTYPE_WSTRING_SIZE_IS);
}

inline bool VariantIsEmpty(nsIVariant * v) {
  PRInt16 vtype = VariantGetType(v);
  return (vtype == nsIDataType::VTYPE_EMPTY);
}

inline bool VariantIsNumber(nsIVariant * v) {
  PRInt16 vtype = VariantGetType(v);
  return (vtype >= nsIDataType::VTYPE_INT8 && vtype <= nsIDataType::VTYPE_DOUBLE);
}

inline bool VariantIsInteger(nsIVariant * v) {
  PRInt16 vtype = VariantGetType(v);
  return (vtype >= nsIDataType::VTYPE_INT8 && vtype <= nsIDataType::VTYPE_UINT64);
}

inline bool VariantIsFloat(nsIVariant * v) {
  PRInt16 vtype = VariantGetType(v);
  return (vtype >= nsIDataType::VTYPE_FLOAT && vtype <= nsIDataType::VTYPE_DOUBLE);
}

inline bool VariantIsInterface(nsIVariant * v) {
  PRInt16 vtype = VariantGetType(v);
  return (vtype >= nsIDataType::VTYPE_INTERFACE && vtype <= nsIDataType::VTYPE_INTERFACE_IS);
}

inline bool VariantIsArray(nsIVariant * v) {
  PRInt16 vtype = VariantGetType(v);
  return (vtype == nsIDataType::VTYPE_ARRAY || vtype == nsIDataType::VTYPE_EMPTY_ARRAY);
}

inline bool VariantHasValue(nsIVariant * v) {
  PRInt16 vtype = VariantGetType(v);
  return (vtype != nsIDataType::VTYPE_EMPTY && vtype != nsIDataType::VTYPE_EMPTY_ARRAY);
}

// --------------- variant value accessor functions ---------------
inline bool VariantGetBoolean(nsIVariant * v, bool defaultVal = false)
{
  bool isTrue = defaultVal;
  if (v) {
	PRBool bval;
	v->GetAsBool(&bval);
	isTrue = (bval == PR_TRUE);
  }
  return isTrue;
}

inline int VariantGetInteger(nsIVariant * v, int defaultVal = 0) {
  int val = defaultVal;
  if (v) {
	PRInt32 i;
	v->GetAsInt32(&i);
	val = i;
  }
  return val;
}

inline double VariantGetDouble(nsIVariant * v, double defaultVal = 0) {
  double val = defaultVal;
  if (v)
	v->GetAsDouble(&val);
  return val;
}

inline double VariantGetNumber(nsIVariant * v, double defaultVal = 0) {
  return VariantGetDouble(v, defaultVal);
}

inline string VariantGetString(nsIVariant * v, const string & defaultstr = string(""))
{
  string value = defaultstr;
	 
  if (v)
	XP_GET_STRING(v->GetAsString, value);
  return value;
}

inline nsISupports * VariantGetInterface(nsIVariant * v)
{
	 nsISupports * iface = NULL;
	 if (v)
	   v->GetAsISupports(&iface);
	 return iface;
}

inline void VariantGetArrayInfo(nsIVariant * v, PRUint32 & length, PRUint16 & type) {
  nsIID iid;
  void * data;

  length = 0;
  type = nsIDataType::VTYPE_EMPTY;
  if (v) {
	if (NS_SUCCEEDED(v->GetAsArray(&type, &iid, &length, &data)))
		 nsMemory::Free(data);
  }

}


inline PRUint32 VariantGetArrayLength(nsIVariant * v) {
  PRUint32 length;
  PRUint16 type;
  VariantGetArrayInfo(v, length, type);
  return length;
}

inline PRUint16 VariantGetArrayType(nsIVariant * v) {
  PRUint32 length;
  PRUint16 type;
  VariantGetArrayInfo(v, length, type);
  return type;
}

inline void VariantGetArray(nsIVariant * v, vector<nsIVariant *> & inVec) {
  nsIVariant ** items;
  PRUint32 count;
  inVec.clear();
  GetVariantArray(v, &items, &count);
  if (items) {
	for (PRUint32 i = 0; i < count; i++)
	  inVec.push_back(items[i]);
	delete [] items;
  }
}

inline void VariantGetArray(nsIVariant * v, vector<string> & strings) {
  strings.clear();
  if (VariantIsArray(v)) {
	vector<nsIVariant *> inVec;
	VariantGetArray(v, inVec);
	for (unsigned int i = 0; i < inVec.size(); i++) {
	  strings.push_back(VariantGetString(inVec[i]));
	}
  }
  else if (VariantHasValue(v)) {
	strings.push_back(VariantGetString(v));
  }
}

inline void VariantGetArray(nsIVariant * v, vector<int> & ints) {
  ints.clear();
  if (VariantIsArray(v)) {
	vector<nsIVariant *> inVec;
	VariantGetArray(v, inVec);
	for (unsigned int i = 0; i < inVec.size(); i++) {
	  ints.push_back(VariantGetInteger(inVec[i]));
	}
  }
  else if (VariantHasValue(v)) {
	ints.push_back(VariantGetInteger(v));
  }
}

inline void VariantGetArray(nsIVariant * v, vector<double> & doubles) {
  doubles.clear();
  if (VariantIsArray(v)) {
	vector<nsIVariant *> inVec;
	VariantGetArray(v, inVec);
	for (unsigned int i = 0; i < inVec.size(); i++) {
	  doubles.push_back(VariantGetNumber(inVec[i]));
	}
  }
  else if (VariantHasValue(v)) {
	doubles.push_back(VariantGetNumber(v));
  }
}

inline void VariantGetArray(nsIVariant * v, vector<nsISupports *> & ifaces) {
  ifaces.clear();
  if (VariantIsArray(v)) {
	   vector<nsIVariant *> inVec;
	   VariantGetArray(v, inVec);
	   for (unsigned int i = 0; i < inVec.size(); i++) {
			ifaces.push_back(VariantGetInterface(inVec[i]));
	   }
  }
  else if (VariantHasValue(v)) {
	   ifaces.push_back(VariantGetInterface(v));
  }
}

// ------------ variant value setting functions ---------
inline void VariantSetEmpty(nsIVariant * v) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv)
	wv->SetAsEmpty();
}

inline void VariantSetBoolean(nsIVariant * v, bool b) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv)
	wv->SetAsBool(b);
}

inline void VariantSetInteger(nsIVariant * v, int i) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv)
	wv->SetAsInt32(i);
}

inline void VariantSetNumber(nsIVariant * v, double d) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv)
	wv->SetAsDouble(d);
}

inline void VariantSetString(nsIVariant * v, string s) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv)
	wv->SetAsString(s.c_str());
}

inline void VariantSetStringWithSize(nsIVariant * v, int size, char * s) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv)
	wv->SetAsStringWithSize(size, s);
}

inline void VariantSetInterface(nsIVariant * v, nsISupports * iface, 
								nsIID iid = NS_GET_IID(nsISupports)) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv)
	wv->SetAsInterface(iid, iface);
}

inline void VariantSetArray(nsIVariant * v, const vector<string> & strArray) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv) {
	if (strArray.size() > 0) {
	  char ** strings = new (char *)[strArray.size()];
	  vector<string>::const_iterator ai = strArray.begin(), bi = strArray.end();
	  int i = 0;
	  while (ai != bi) {
		strings[i] = (char *) nsMemory::Clone(ai->c_str(), ai->size() + 1);
		++ai;
		i++;
	  }
	  wv->SetAsArray(nsIDataType::VTYPE_CHAR_STR, &NS_GET_IID(nsIVariant), 
					 strArray.size(), strings);
	  delete [] strings;
	}
	else {
	  WritableVariantSetEmptyArray(wv);
	}
  }
}

inline void VariantSetArray(nsIVariant * v, const vector<int> & intArray) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv) {
	if (intArray.size() > 0) {
	  int * ints = new int [intArray.size()];
	  vector<int>::const_iterator ai = intArray.begin(), bi = intArray.end();
	  int i = 0;
	  while (ai != bi) {
		ints[i] = *ai;
		++ai;
		i++;
	  }
	  wv->SetAsArray(nsIDataType::VTYPE_INT32, &NS_GET_IID(nsIVariant), 
					 intArray.size(), ints);
	  delete [] ints;
	}
	else {
		 WritableVariantSetEmptyArray(wv);
	}
  }
}

inline void VariantSetArray(nsIVariant * v, const vector<bool> & boolArray) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv) {
	if (boolArray.size() > 0) {
	  PRBool * bools = new PRBool [boolArray.size()];
	  vector<bool>::const_iterator ai = boolArray.begin(), bi = boolArray.end();
	  int i = 0;
	  while (ai != bi) {
		bools[i] = *ai;
		++ai;
		i++;
	  }
	  wv->SetAsArray(nsIDataType::VTYPE_BOOL, &NS_GET_IID(nsIVariant), 
					 boolArray.size(), bools);
	  delete [] bools;
	}
	else {
		 WritableVariantSetEmptyArray(wv);
	}
  }
}

inline void VariantSetArray(nsIVariant * v, const vector<double> & doubleArray) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv) {
	if (doubleArray.size() > 0) {
	  double * doubles = new double [doubleArray.size()];
	  vector<double>::const_iterator ai = doubleArray.begin(), bi = doubleArray.end();
	  int i = 0;
	  while (ai != bi) {
		doubles[i] = *ai;
		++ai;
		i++;
	  }
	  wv->SetAsArray(nsIDataType::VTYPE_DOUBLE, &NS_GET_IID(nsIVariant), 
					 doubleArray.size(), doubles);
	  delete [] doubles;
	}
	else {
		 WritableVariantSetEmptyArray(wv);
	}
  }
}

inline void VariantSetArray(nsIVariant * v, const vector<nsISupports *> & infArray) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv) {
	if (infArray.size() > 0) {
	  nsISupports ** infs = new (nsISupports*) [infArray.size()];
	  for (unsigned int i = 0; i < infArray.size(); i++)
		infs[i] = infArray[i];
	  wv->SetAsArray(nsIDataType::VTYPE_INTERFACE, &NS_GET_IID(nsISupports), 
					 infArray.size(), infs);
	  delete [] infs;
	}
	else {
		 WritableVariantSetEmptyArray(wv);
	}
  }
}

inline void VariantSetVariant(nsIVariant *v, nsIVariant *var){
	 nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
	 if (wv)
		  wv->SetFromVariant(var);
}

inline void VariantSetArray(nsIVariant * v, const vector<nsIVariant *> & infArray) {
  nsCOMPtr<nsIWritableVariant> wv = do_QueryInterface(v);
  if (wv) {
	if (infArray.size() > 0) {
	  nsIVariant ** infs = new (nsIVariant*) [infArray.size()];
	  vector<nsIVariant *>::const_iterator ai = infArray.begin(), bi = infArray.end();
	  int i = 0;
	  while (ai != bi) {
		   NewVariant(&infs[i]);
		   VariantSetVariant(infs[i], *ai);
		   ++ai;
		   i++;
	  }
	  wv->SetAsArray(nsIDataType::VTYPE_INTERFACE_IS, &NS_GET_IID(nsIVariant), 
					 infArray.size(), infs);
	  delete [] infs;
	}
	else {
		 WritableVariantSetEmptyArray(wv);
	}
  }
}


#endif
