
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "nsISupports.idl"
#include "nsISupportsPrimitives.idl"
#include "nsIPref.idl"
#include "nsIVariant.idl"

%{ C++
#include <nsComponentManagerUtils.h>
		class XMLNode;
%}

[ptr] native xmlnode_internal(XMLNode);

[scriptable, uuid(d0be873c-97ba-4e35-a020-8198efebf176)]
interface sashIXPXMLNode : nsISupports {

  readonly attribute string name;
  readonly attribute sashIXPXMLNode parent;
  attribute string text;
  attribute string xml;
  
  boolean hasAttribute(in string attribute_name);
  unsigned long hasAttributes(in string pattern);
  
  string getAttribute(in string attribute_name);
  boolean setAttribute(in string attribute_name, in string value);
  boolean removeAttribute(in string attribute_name);
  nsIVariant getAttributeNames(in string pattern);

  unsigned long hasChildNodes(in string pattern);
  nsIVariant getChildNodes(in string pattern);
  sashIXPXMLNode getFirstChildNode(in string pattern);
  sashIXPXMLNode getNextSibling(in string pattern);
  unsigned long removeChildNodes(in string pattern);
  boolean removeChild(in sashIXPXMLNode child);
  boolean appendChild(in sashIXPXMLNode child);
  unsigned long appendChildNodes(in nsIVariant children);

  boolean appendXML(in string xml);

  sashIXPXMLNode Clone();

  [noscript] void GetInternalNode(in xmlnode_internal retval);
  [noscript] void SetInternalNode(in xmlnode_internal retval);
	 [noscript] void Initialize(in string str);
	 boolean isNull();
};

%{ C++
#define XPXMLNODE_CID {0xbcb6645b, 0x836d, 0x4c5f, {0x98, 0x8f, 0x7d, 0xb4, 0x4b, 0x03, 0x45, 0x06}}

inline sashIXPXMLNode *NewSashXPXMLNode(XMLNode *xmn) {
	 sashIXPXMLNode *tmp;
	 nsresult res = nsComponentManager::CreateInstance((nsCID)XPXMLNODE_CID, NULL, NS_GET_IID(sashIXPXMLNode), (void **) &tmp); 
	 if (! NS_SUCCEEDED(res)) return NULL;
	 
	 if (xmn)
		  tmp->SetInternalNode(xmn);
	 return tmp;
}

%}
