
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _EZ_XML_TYPE_WRAPPER
#define _EZ_XML_TYPE_WRAPPER

// Stefan Atev, IBM Extreme Blue 2001
// Rewritten for Gdome2 by Andrew Chatham

#include <string>

typedef struct _GdomeNode GdomeNode;
typedef struct _GdomeDOMString GdomeDOMString;
typedef struct _GdomeDocument GdomeDocument;
typedef struct _GdomeDOMImplementation GdomeDOMImplementation;
typedef struct _GdomeElement GdomeElement;
typedef struct _GdomeAttr GdomeAttr;
typedef unsigned int GdomeException;

string g2str(GdomeDOMString *str);

typedef enum {
  NULL_NODE,
  UNKNOWN_NODE,
  ELEMENT_NODE,
  ATTRIBUTE_NODE,
  TEXT_NODE,
  CDATA_SECTION_NODE,
  COMMENT_NODE,
  DOCUMENT_NODE,
} NodeType;

NodeType nodeTypeConv(unsigned int gdomeType);

// use these to cast a private EZ_DOMNode if you want to
// force the use of a specific type. This is not type-safe!!!
#define ATTR(dom) (GDOME_A(dom))
#define CHARACTER_DATA(dom) (GDOME_CD(dom))
#define ELEMENT(dom) (GDOME_EL(dom))
#define NODE(dom) (GDOME_N(dom))
#define TEXT(dom) (GDOME_T(dom))

bool nodeIsType(GdomeNode *node, NodeType type);

inline bool
nodeIsAttr(GdomeNode *node)
{
  return nodeIsType(node, ATTRIBUTE_NODE);
}

inline bool
nodeIsCDATA(GdomeNode *node)
{
  return nodeIsType(node, CDATA_SECTION_NODE);
}

inline bool 
nodeIsElement(GdomeNode *node)
{
  return nodeIsType(node, ELEMENT_NODE);
}

inline bool
nodeIsText(GdomeNode *node)
{
  return nodeIsType(node, TEXT_NODE);
}

#define CHECK { printf("%s:%d\n", __FILE__, __LINE__); fflush(stdout); }

#endif // _EZ_XML_TYPE_WRAPPER
