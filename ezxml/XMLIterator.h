
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _EZ_XML_ITERATOR
#define _EZ_XML_ITERATOR

// Stefan Atev, IBM Extreme Blue 2001

#include <string>
#include <vector>
#include "XMLTypeWrapper.h"
#include "XMLTagMatch.h"
#include "XMLNode.h"

class XMLIterator {
  
  public:
  
    static const char XPATH_SEPARATOR= '/';
    
    /** 
	Construct an XMLIterator. xpath is a pattern or set of
	patterns separated by /'s.

	If it is a list of separated patterns, the iterator will recursively
	match all of the patterns and report the deepest child.

	So if the pattern is *s/ *e/ *id (without spaces), it will
	report the <fileid/> child if it is a part of
	<files><file><fileid/></file></files> but not if it is by
	itself (because its parents must match *s and *e.
    */
    XMLIterator(XMLNode root, const std::string &xpath= "");

    void begin();

    bool hasNext();

    XMLNode getNext();

    std::vector<XMLNode> getAll();
    
  private:
  
    // fill in patterns accordingly
    void splitPath(const std::string &xpath);
    
    void fillList(XMLNode current, unsigned int depth= 0);
  
    // the node that flies around to actually traverse the whole tree
    XMLNode base_root;
    std::vector<XMLNode> full_list;
    std::vector<std::string> patterns;
    bool relative;
    unsigned int index;
};

#endif // _EZ_XML_ITERATOR
