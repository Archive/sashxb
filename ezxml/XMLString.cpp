
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include <gdome.h>

#include "XMLString.h"
#include "XMLTypeWrapper.h"
#include "debugmsg.h"

ostream &
operator<<(ostream &os, const XMLString &str)
{
  os << str.getString();
  return os;
}

static GdomeDOMString *
str2g(const string & str)
{
  return gdome_str_mkref_dup(str.c_str());
}

string 
g2str(GdomeDOMString *str)
{
  if (!str)
    return "";

  string x(str->str);
  gdome_str_unref(str);
  return x;
}

XMLString::XMLString(GdomeDOMString *str = NULL)
{
  m_gstr = str;
}

XMLString::XMLString(const string & str)
{
  m_gstr = str2g(str);
}

XMLString::XMLString(const XMLString &other) :
  m_gstr(NULL)
{
  *this = other;
}

void
XMLString::replaceData(GdomeDOMString *in)
{
  gdome_str_unref(m_gstr);
  gdome_str_ref(in);
  m_gstr = in;
}

void
XMLString::appendData(const string & in)
{
  string res = g2str(m_gstr); // consumes the reference
  res += in;
  m_gstr = str2g(res);
}

XMLString::~XMLString()
{
  if (m_gstr) {
    gdome_str_unref(m_gstr);
	}
}

GdomeDOMString *
XMLString::getDOMString() const
{
  return m_gstr;
}

const char *
XMLString::c_str() const
{
  if (!m_gstr)
    return NULL;
  return m_gstr->str;
}


string
XMLString::getString() const
{
  if (!m_gstr)
    return "";
  gdome_str_ref(m_gstr); // will be consumed by g2str
  return g2str(m_gstr);
}

int 
XMLString::length() const
{
  return gdome_str_length(m_gstr);
}

char
XMLString::charAt(int pos) const
{
  if (length() == 0)
    return '\0';
  return getString()[0];
}

// Operators

XMLString &
XMLString::operator= (const XMLString &other)
{
  if (&other == this)
    return (*this);
  if (m_gstr)
    gdome_str_unref(m_gstr);
  m_gstr = other.m_gstr;
  if (m_gstr)
    gdome_str_ref(m_gstr);
  return (*this);
}

XMLString &
XMLString::operator= (const string & other)
{
  if (m_gstr)
    gdome_str_unref(m_gstr);
  m_gstr = str2g(other);
  return (*this);
}

XMLString  &
XMLString::operator+ (const XMLString &in)
{
  appendData(in.getString());
  return (*this);
}

void
XMLString::operator+= (const XMLString & in) 
{
  *this = *this + in;
}

bool
XMLString::operator== (const XMLString & other) const
{
  if (!m_gstr)
    return (other.m_gstr == NULL);
  return gdome_str_equal(m_gstr, other.m_gstr);
}

bool 
XMLString::operator== (const string & other) const
{
  return getString() == other;
}


XMLString::operator string() const
{
  return getString();
}





