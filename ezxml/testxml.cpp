
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "XMLDocument.h"
#include "XMLNode.h"
#include "XMLIterator.h"
#include <iostream>

static void
test_string()
{
  XMLString foo("start");
  foo.appendData("foo2");
  foo.appendData("foo3");
}

static void
createAndFreeDocument()
{
  XMLDocument d;
  XMLDocument d2 = d;
  XMLDocument d3(d2);
}

static void
test_child()
{
	cout << "Test child" << endl;
  XMLDocument d;
	bool ret;

	XMLNode achild("achild");
  ret = d.getRootNode().importChild(achild);
	assert(ret);
  achild.setAttribute("Hey", "you");
  achild.setAttribute("attr", "foo");
	XMLNode subchild("subchild");
  ret = achild.importChild(subchild);
	assert(ret);

	XMLNode secondchild("asecondchild");
  d.getRootNode().importChild(secondchild);
  XMLNode child = d.getRootNode().getFirstChildNode();
	assert(d.getRootNode().hasChildNodes() == 2);
	assert(child.getName() == "achild");
	assert(child.getAttribute("attr") == "foo");
}

static void 
test_CDATA()
{
  XMLDocument d;
  cout << "CDATA test" << endl;
  XMLNode text = d.createCDATASection("MyCDATA");
  XMLNode child = d.createNode("child");
  d.getRootNode().appendChild(child);
  child.appendChild(text);
	assert(d.getRootNode().getFirstChildNode().getText() == "MyCDATA");
  child.setText("FOOO", true);
	assert(d.getRootNode().getFirstChildNode().getText() == "FOOO");
}

static void 
file_test()
{
  static int run = 0;
  cerr << "File Test Run: " << ++run << endl;

  cout << "Loading test2.xml..." << endl;
  XMLDocument d2;
  d2.loadFromFile("test2.xml");

  XMLIterator it(d2.getRootNode(), "*file*");

  unsigned int attc = 0, tagc = 0, textc = 0;
  it.begin();
  while (it.hasNext()) {
    XMLNode tmp = it.getNext();
    ++tagc;
    attc += tmp.hasAttributes();
    XMLString text = tmp.getText();
    if (text != "") {
      ++textc;
    }
  }

  assert(textc == 0);
  assert(tagc == 33);
  assert(attc == 61);
	assert(textc == 0);

  XMLIterator it2(d2.getRootNode(), "*s/*e/*id");

  attc = 0;
  tagc = 0;
  textc = 0;

  it2.begin();
  while (it2.hasNext()) {
    XMLNode tmp = it2.getNext();
    ++tagc;
    attc += tmp.hasAttributes();
    if (tmp.getText() != "")
      ++textc;
  }

	/* Not sure these numbers are right */
	assert(tagc == 9);
	assert(attc == 16);
	assert(textc == 1);

  XMLIterator it3(d2.getRootNode());
  XMLDocument d3;
  d3.createRootNode("flatten");
  it3.begin();
  while (it3.hasNext()) {
    XMLNode tmp = it3.getNext();
		XMLNode empty(tmp.getName());
    d3.getRootNode().importChild(empty);
  }
	// Check this number
	assert(d3.getRootNode().hasChildNodes() == 177);
  d3.saveToFile("test3.xml");
}

static void
simple_tests()
{
  static int run = 0;
	bool ret;
  
  cout << "Simple Tests Run: " << ++run << endl;

  createAndFreeDocument();

  XMLDocument d("mything");
  XMLDocument doc;
  doc = d;
    
  XMLNode tmp;
  XMLNode &r = d.getRootNode();
	assert(r.getName() == "mything");
    
  tmp = XMLNode("sub1");
  ret = r.importChild(tmp);
	assert(ret);
	assert(tmp.getName() == "sub1");
	assert(r.getFirstChildNode().getName() == "sub1");
    
  XMLNode n2("sub2");
  ret = tmp.importChild(n2);
	assert(ret);
	XMLNode sub3("sub3");
  ret = r.importChild(sub3);
	assert(ret);
    
  d.getRootNode().setAttribute("Hey", "you");

  tmp = r.getFirstChildNode("sub1");
  tmp.setAttribute("id", "test setting attribute");
  tmp.appendChild(XMLNode("test_sub_tag1"));
  tmp.appendChild(XMLNode("test_sub_tag2"));
  tmp = tmp.getNextSibling();
  tmp.setAttribute("copy_of_sub1_attribute", 
									 r.getFirstChildNode("sub1").getAttribute("id"));

	assert(tmp.getAttribute("copy_of_sub1_attribute") == 
				 "test setting attribute");

  XMLNode sub1 = r.getFirstChildNode("sub1");
  assert(!sub1.isNull());
  r.removeChild(sub1);
	assert(r.hasChildNodes() == 1);
	assert(r.getFirstChildNode().getName() == "sub3");

  {
    XMLNode xmlwrite = r.getFirstChildNode("sub3");
    xmlwrite.setXML("<xmlwrite><insub at= \"bum\">test</insub></xmlwrite>");
		assert(r.getFirstChildNode().getName() == "xmlwrite");
    
		r.getFirstChildNode("xmlwrite").appendXML("<appendedxml id= \"bad\"   >THIS WAS APPENDED</appendedxml>");
		assert(xmlwrite.getFirstChildNode().getNextSibling().getName() 
					 == "appendedxml");
	}

  r.removeChildNodes();
	assert(r.hasChildNodes() == 0);
}

static char xml_data[] = "<doc>\
  <child1 attr=\"myattr\">\
    <foo/>\
  </child1>\
  <child2/>\
  <child3/>\
</doc>";

static void
from_string_tests()
{
	static int run = 0;
	XMLDocument d;
	bool ret = d.loadFromString(xml_data);
	assert(ret);
	XMLNode root = d.getRootNode();
	XMLNode child1(root.getFirstChildNode());
	assert(child1.hasChildNodes() == 1);
	assert(child1.getFirstChildNode().getName() == "foo");
	assert(child1.hasAttribute("attr"));
	cout << "Finished string loading test run " << ++run << endl;
}

int 
main(void) {
	while (1) {
		from_string_tests();
		test_child();
		test_string();
		test_CDATA();
		simple_tests();
		file_test();
	}
  return 0;
}

