
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef XMLSTRING_H
#define XMLSTRING_H

#include "XMLTypeWrapper.h"
#include <string>


/**
   An internal utility class for converting DOM strings and C++ strings
*/
class XMLString {
 public:

  //! Constructs an XMLString, consuming a reference
  XMLString(GdomeDOMString *str = NULL);

  XMLString(const string &str);
  XMLString(const XMLString &other);

  ~XMLString();


  XMLString &operator=(const XMLString &other);
  XMLString &operator=(const string & other);

  XMLString &operator+(const XMLString &other);
  void operator+= (const XMLString &in);

  bool operator==(const XMLString &other) const;
  bool operator==(const string &other) const;

  bool operator!=(const XMLString &other) const { return ! (*this == other); }
  bool operator!=(const string &other) const { return ! (*this == other); }

  operator string() const;

  /**********************************
   *       Modifier Functions
   **********************************/
  void appendData(const string & in);

  //! Set this GdomeDOMString as the data. Does not consume a reference 
  void replaceData(GdomeDOMString *in);


  /**********************************
   *       Accessor Functions
   **********************************/

  // Returns a borrowed reference
  GdomeDOMString *getDOMString() const;

  string getString() const;

  const char *c_str() const;

  int length() const;
  char charAt(int pos) const;

 private:

  GdomeDOMString *m_gstr;
};

ostream & operator<<(ostream &os, const XMLString &str);

#endif // XMLSTRING_H
