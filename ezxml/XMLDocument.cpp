
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):
  Stefan Atev, IBM Extreme Blue 2001
  Andrew Chatham, IBM Extreme Blue 2001

*****************************************************************/


#include <string>
#include <iostream>
#include <fstream>
#include <strstream>
#include <gdome.h>

#include "XMLDocument.h"
#include "XMLNode.h"
#include "XMLTypeWrapper.h"
#include "XMLString.h"
#include "debugmsg.h"

using namespace std;

ostream &
operator<<(ostream &os, const XMLDocument &doc)
{
  os << doc.saveToString();
  return os;
}

XMLDocument::XMLDocument(const string & name = "NEWDOC")
{
  GdomeException exc;
  m_domimpl = gdome_di_mkref();

  XMLString str(name);

  m_doc = gdome_di_createDocument(m_domimpl, NULL, str.getDOMString(), 
				  NULL, &exc);
  GdomeElement *root = gdome_doc_documentElement(m_doc, &exc);
  m_root = new XMLNode(root);
  gdome_el_unref(root, &exc);
}

XMLDocument::XMLDocument(const XMLDocument &in) :
  m_root(NULL), 
  m_doc(NULL), 
  m_domimpl(NULL)
{
  *this = in;
}

XMLDocument::~XMLDocument() {
  GdomeException exc;
  //assert(m_root != NULL);
  //delete m_root;
  if (m_root)
    delete m_root;
  if (m_doc)
    gdome_doc_unref(m_doc, &exc);
  gdome_di_unref(m_domimpl, &exc);
}
    
XMLDocument&
XMLDocument::operator=(const XMLDocument &in)
{
  GdomeException exc;

  if (&in == this)
    return *this;

  if (m_domimpl)
    gdome_di_unref(m_domimpl, &exc);
  m_domimpl = in.m_domimpl;
  gdome_di_ref(m_domimpl, &exc);

  if (m_doc)
    gdome_doc_unref(m_doc, &exc);
  m_doc = in.m_doc;
  gdome_doc_ref(m_doc, &exc);

  assert(in.m_root != NULL);
  if (m_root)
    delete m_root;
  m_root = new XMLNode(*in.m_root);
  return (*this);
}

bool
XMLDocument::loadFromFile(const string &file_name) {
  bool failed = false;
  GdomeException exc;

  if (m_doc)
    gdome_doc_unref(m_doc, &exc);

  m_doc = gdome_di_createDocFromURI(m_domimpl, file_name.c_str(), 
				    GDOME_LOAD_PARSING, &exc);
  DEBUGMSG(xml, "CreateDoc returned exc %d\n", exc);
  if (exc) failed = true;

  delete m_root;
  GdomeElement *root = gdome_doc_documentElement(m_doc, &exc);
  m_root = new XMLNode(NODE(root));
  gdome_el_unref(root, &exc);
  DEBUGMSG(xml, "Getting documentElement returned %d\n", exc);

  if (exc) failed = true;
  return !failed;
}
  
bool 
XMLDocument::saveToFile(const string &file_name) const
{
  GdomeException exc;
	return gdome_di_saveDocToFile(m_domimpl, m_doc, file_name.c_str(), 
																GDOME_SAVE_LIBXML_INDENT, &exc);
}
    
bool
XMLDocument::loadFromString(const string &xml) {
  bool failed = false;
  GdomeException exc;
  
  // If only createDocFromMemory's string argument were const...
  char *xml2 = g_strdup(xml.c_str());
  if (m_doc)
    gdome_doc_unref(m_doc, &exc);
  m_doc = gdome_di_createDocFromMemory(m_domimpl, xml2,
				       GDOME_LOAD_PARSING, &exc);
  g_free(xml2);

  if (exc) failed = true;
  delete m_root;
  GdomeElement *root = gdome_doc_documentElement(m_doc, &exc);
  m_root = new XMLNode(root);
  gdome_el_unref(root, &exc);
  if (exc) 
    failed = true;
  return !failed;
}

string 
XMLDocument::saveToString() const
{
  GdomeException exc;
  char *buff;
  if (!gdome_di_saveDocToMemory(m_domimpl, m_doc,
				&buff, GDOME_SAVE_STANDARD, &exc))
    return "";
  string str(buff);
  g_free(buff);
  return str;
}
    
XMLNode &
XMLDocument::getRootNode() const
{
  return *m_root;
}

void 
XMLDocument::createRootNode(const string tag_name) {
  *this = XMLDocument(tag_name);
}

XMLNode
XMLDocument::createNode(const string & tagName) const
{
  GdomeException exc;

  XMLString gtagName(tagName);
  GdomeElement *el = gdome_doc_createElement(m_doc, gtagName.getDOMString(), 
					     &exc);
  XMLNode ret(el);
  gdome_el_unref(el, &exc);
  return ret;
}

XMLNode
XMLDocument::createCDATASection(const string & data) const
{
  GdomeException exc;
  XMLString gdata(data);
  GdomeCDATASection *cdata = 
    gdome_doc_createCDATASection(m_doc, gdata.getDOMString(), &exc);
  XMLNode ret(NODE(cdata));
  gdome_cds_unref(cdata, &exc);
  return ret;
}

XMLString
XMLDocument::getXML()
{
  GdomeException exc;
  char *buff;
  gdome_di_saveDocToMemory(m_domimpl, m_doc, &buff, 
			   GDOME_SAVE_LIBXML_INDENT, 
			   &exc);
  XMLString ret(buff);
  free(buff);
  return ret;
}

