
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _EZ_XML_DOCUMENT_H
#define _EZ_XML_DOCUMENT_H

// Stefan Atev, IBM Extreme Blue 2001

#include <string>
#include "XMLTypeWrapper.h"
#include "XMLString.h"

class XMLDocument;
class XMLNode;

ostream & operator<<(ostream &os, const XMLDocument &doc);

class XMLDocument {
  
 public:
  
  XMLDocument(const string & name = "NEWDOC");
  XMLDocument(const XMLDocument &in);
  ~XMLDocument();

  XMLDocument &operator=(const XMLDocument &in);
  
  bool loadFromFile(const std::string &file_name);
  bool saveToFile(const std::string &file_name) const;
  
  bool loadFromString(const std::string &xml);
  std::string saveToString() const;
  
  //! Return a reference to the root node
  XMLNode &getRootNode() const;
  
  void createRootNode(const std::string tag_name);
  
  /**
     Return a new, empty node belonging to this document. It is not
     attached to the document
  */
  XMLNode createNode(const std::string & tagName) const;
  
  XMLNode createCDATASection(const std::string & data) const;

  XMLString getXML();
  
 private:
  
  friend class XMLNode;
  
  XMLNode *m_root;
  
  // Parser specific members
  
  GdomeDocument *m_doc;
  GdomeDOMImplementation *m_domimpl;
};

#endif // _EZ_XML_DOCUMENT_H
