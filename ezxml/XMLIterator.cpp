
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):
   Stefan Atev, IBM Extreme Blue 2001
   Andrew Chatham, IBM Extreme Blue 2001

*****************************************************************/


#include "XMLIterator.h"

XMLIterator::XMLIterator(XMLNode root, const string &xpath = "") 
{
  base_root = root;
  index = 0;
  if (xpath.length() > 0) 
    relative = (xpath[0] != XPATH_SEPARATOR);
  else
    relative = true;  // empty pattern == match everything
  splitPath(xpath);
  full_list.clear();
}

void 
XMLIterator::begin() 
{
  full_list.clear();
  if (patterns.size() == 1 && base_root.matchTag(patterns[0]))
    full_list.push_back(base_root);
  fillList(base_root);
  index = 0;
}

bool 
XMLIterator::hasNext() 
{
  return (index < full_list.size());
}

XMLNode 
XMLIterator::getNext() 
{
  XMLNode rv;
  if (index >= full_list.size()) 
    return rv;
  rv = full_list[index];
  ++index;
  return rv;
}

vector<XMLNode> 
XMLIterator::getAll() 
{
  return full_list;
}

void 
XMLIterator::fillList(XMLNode current, unsigned int depth = 0) 
{
  XMLNode tmp;
  if (current.isNull()) 
    return;
  
  if (depth == patterns.size() && current.isElement()) 
    full_list.push_back(current);
  
  if (depth >= patterns.size()) 
    return;
  
  if (!relative) {  // handle absolute case == easier
    tmp = current.getFirstChildNode(patterns[depth]);
    while (!tmp.isNull()) {
      fillList(tmp, depth + 1);
      tmp = tmp.getNextSibling(patterns[depth]);
    }
    return;
  }

  // now we know it's a relative story
  // restart search rooted at tmp
  tmp = current.getFirstChildNode(patterns[depth]);
  while (!tmp.isNull()) {
    fillList(tmp, depth + 1);  // still the case
    tmp = tmp.getNextSibling(patterns[depth]);
  }
  tmp = current.getFirstChildNode();
  while (!tmp.isNull()) {
    fillList(tmp);  
    tmp = tmp.getNextSibling();
  }
}
    
void 
XMLIterator::splitPath(const string &xpath) 
{
  unsigned int i, j;
  string tmp;
  patterns.clear();
  i = 0;
  if (!relative) 
    i = 1;
  while (i < xpath.length() && 
	 (j = xpath.substr(i, xpath.length() - i).find(XPATH_SEPARATOR)) != 
	 string::npos) {
    tmp = xpath.substr(i, j);
    patterns.push_back(tmp);
    i += j + 1;
  }

  if (i < xpath.length()) {
    tmp = xpath.substr(i, xpath.length() - i);
    patterns.push_back(tmp);
  }

  if (patterns.size() == 0) {
    patterns.push_back("");
  }
}
