
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):
   Stefan Atev, IBM Extreme Blue 2001
   Andrew Chatham, IBM Extreme Blue 2001

*****************************************************************/

#include <fnmatch.h>
#include "XMLTagMatch.h"
#include <gdome.h>

using namespace std;

bool 
tag_match(GdomeNode *n, const string &pattern) {
  GdomeException exc;
  bool rv;
  if (pattern == "")
    return true;
  
  string name = g2str(gdome_n_nodeName(n, &exc));
  // in the future we could add actual pattern matching here
  // use this routine consistently throughout XMLNode, please

  rv = (fnmatch(pattern.c_str(), name.c_str(), FNM_PATHNAME) == 0);
  
  return rv;
}

bool 
attr_match(const std::string &attribute_name, 
	   const std::string &value,  
	   const std::string &pattern) {
  // X-Path like matching should be added later
  bool rv;
  if (pattern == "")
    return true;
  rv = (fnmatch(pattern.c_str(), attribute_name.c_str(), FNM_FILE_NAME) == 0);
  return rv;
}
