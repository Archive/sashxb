
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):
  Stefan Atev, IBM Extreme Blue 2001
  Rewritten for gdome2 by Andrew Chatham, IBM Extreme Blue 2001

*****************************************************************/


#include "XMLNode.h"
#include "XMLTypeWrapper.h"
#include "XMLString.h"
#include <strstream>
#include <iostream>
#include <gdome.h>
#include <stdio.h>
#include "debugmsg.h"

using namespace std;

ostream &
operator<<(ostream  &os, const XMLNode &node)
{
  os << node.getXML().getString();
  return os;
}

// TODO: More of these map object things
// also, a GetNextSibling that does the refcounting lines for us

class AbstractAttrMap {
public:
  /** 
      returns false if an error occurred
  */
  bool doMap(GdomeElement *element) {
    GdomeException exc;
    GdomeNamedNodeMap *map = gdome_el_attributes(element, &exc);
    if (!map) return false;

    unsigned int length = gdome_nnm_length(map, &exc);
    for (unsigned int k = 0; k < length; k++) {
      GdomeAttr *attr = (GdomeAttr*) gdome_nnm_item(map, k, &exc);

      attrMapCond res = process(attr, &exc);

      if (res == STOP) 
	return true;
      else if (res == ERROR)
	return false;

      if (exc) return false;

      gdome_a_unref(attr, &exc);
      if (exc) return false;
    }

    gdome_nnm_unref(map, &exc);
    return true;
  }

protected:
  typedef enum {ERROR, STOP, OK} attrMapCond;
  /**
     return false on error
  */
  virtual attrMapCond process(GdomeAttr *, GdomeException *exc) = 0;
};


class AttributeCollector : public AbstractAttrMap
{
public:
  AttributeCollector(const string & pattern) {
    m_pattern = pattern;
  }
  virtual ~AttributeCollector() {}
  int getCount() { return m_names.size(); }
  vector<string> getNames() { return m_names; }

protected:
  vector<string> m_names;
  string m_pattern;

  virtual attrMapCond process(GdomeAttr *attr, GdomeException *exc) {
    if (tag_match(NODE(attr), m_pattern))
      m_names.push_back(g2str(gdome_a_name(attr, exc)));
    return OK;
  }
  
};


XMLNode::XMLNode() 
{
  m_node = NULL;
}

XMLNode::~XMLNode() 
{
  GdomeException exc;
  if (m_node)
    gdome_n_unref(m_node, &exc);
}

XMLNode::XMLNode(const string tag_name, XMLDocument doc) 
{
  GdomeException exc;
  XMLString str(tag_name);
  m_node = NODE(gdome_doc_createElement(doc.m_doc, str.getDOMString(), &exc));
}

XMLNode::XMLNode(GdomeNode *n) {
  GdomeException exc;
  m_node = n;
  gdome_n_ref(n, &exc);
}

XMLNode::XMLNode(GdomeElement *el) {
  GdomeException exc;
  m_node = NODE(el);
  gdome_n_ref(m_node, &exc);
}

XMLNode::XMLNode(const XMLNode &other) :
  m_node(NULL)
{
  *this = other;
}

XMLNode &
XMLNode::operator=(const XMLNode &other)
{
  if (&other == this)
    return (*this);

  GdomeException exc;
  if (m_node)
    gdome_n_unref(m_node, &exc);
  m_node = other.m_node;
  if (m_node) {
    gdome_n_ref(m_node, &exc);
  }
  return (*this);
}


//******************
// General routines
//******************

string XMLNode::getName() const {
  GdomeException exc;

  if (!nodeIsElement(m_node))
    return "NON-ELEMENT";

  return g2str(gdome_el_tagName(ELEMENT(m_node), &exc));
}

bool XMLNode::isNull() const {
  return (m_node == NULL);
}

bool XMLNode::matchTag(const std::string &pattern) {
  return tag_match(m_node, pattern);
}

NodeType
XMLNode::getType() const
{
  GdomeException exc;
  if (!m_node)
    return NULL_NODE;
  return nodeTypeConv(gdome_n_nodeType(m_node, &exc));
}

//*******************************
// Attribute accessing functions
//*******************************

bool 
XMLNode::hasAttribute(const string &attribute_name) const {
  GdomeException exc;

  if (!nodeIsElement(m_node))
    return false;
  
  XMLString str(attribute_name);
  bool ret = gdome_el_hasAttribute(ELEMENT(m_node), str.getDOMString(), &exc);
  return ret;
}



unsigned int 
XMLNode::hasAttributes(const std::string &pattern= "") const {
  if (!nodeIsElement(m_node))
    return 12345;

  AttributeCollector col(pattern);
  if (!col.doMap(ELEMENT(m_node)))
    return 12345;
  
  return col.getCount();
}




string 
XMLNode::getAttribute(const string &attribute_name) const {
  if (!nodeIsElement(m_node))
    return "";

  GdomeException exc;

  XMLString str(attribute_name);
  GdomeDOMString *attrValue = gdome_el_getAttribute(ELEMENT(m_node), 
						    str.getDOMString(), &exc);

  return g2str(attrValue);
}

bool 
XMLNode::setAttribute(const string &attribute_name, const string &value) {
  if (!nodeIsElement(m_node))
    return false;

  GdomeException exc;

  XMLString gname(attribute_name);
  XMLString gvalue(value);

  gdome_el_setAttribute(ELEMENT(m_node), gname.getDOMString(), 
			gvalue.getDOMString(), &exc);

  return (exc == 0);
}

bool 
XMLNode::removeAttribute(const string &attribute_name) {
  if (!nodeIsElement(m_node))
    return false;

  GdomeException exc;

  XMLString str(attribute_name);
  gdome_el_removeAttribute(ELEMENT(m_node), str.getDOMString(), &exc);
  return (exc == 0);
}

vector<string> 
XMLNode::getAttributeNames(const string &pattern= "") const {
  vector<string> ret;

  if (!nodeIsElement(m_node))
    return ret;

  AttributeCollector col(pattern);
  if (!col.doMap(ELEMENT(m_node)))
    return ret;

  ret = col.getNames();
  return ret;
}

//**********************************************
// Node access routines
//**********************************************

unsigned int 
XMLNode::hasChildNodes(const std::string pattern= "") const {
  unsigned int count = 0;
  GdomeException exc;

  GdomeNodeList *nodes = gdome_n_childNodes(m_node, &exc);
  if (!nodes) 
		return 0;

  unsigned long length = gdome_nl_length(nodes, &exc);
  for (unsigned int k = 0; k < length; k++) {
    GdomeNode *n = gdome_nl_item(nodes, k, &exc);
    if (!n) 
			continue;

		/* We do not return text and CDATA as children */
		if (nodeIsText(n) || nodeIsCDATA(n)) {
			gdome_n_unref(n, &exc);
			continue;
		}
    if (tag_match(n, pattern))
      count++;
    gdome_n_unref(n, &exc);
  }
  gdome_nl_unref(nodes, &exc);
  return count;
}

vector<XMLNode> 
XMLNode::getChildNodes(const std::string &pattern= "") const 
{
  GdomeException exc;
  vector<XMLNode> ret;

  GdomeNodeList *nodes = gdome_n_childNodes(m_node, &exc);
  if (!nodes) 
		return ret;

  unsigned long length = gdome_nl_length(nodes, &exc);

  for (unsigned int k = 0; k < length; k++) {
    GdomeNode *n = gdome_nl_item(nodes, k, &exc);
    if (!n) 
			continue;
		/* We do not return text and CDATA as children */
		if (nodeIsText(n) || nodeIsCDATA(n))
			continue;
    if (tag_match(n, pattern)) {
      ret.push_back(XMLNode(n));
    }
    gdome_n_unref(n, &exc);
  }

  gdome_nl_unref(nodes, &exc);
  return ret;
}
 
XMLNode 
XMLNode::getFirstChildNode(const std::string &pattern = "") const 
{
  GdomeException exc;
  XMLNode ret;
  
  GdomeNodeList *nodes = gdome_n_childNodes(m_node, &exc);
  if (!nodes) 
		return ret;
  
  unsigned long length = gdome_nl_length(nodes, &exc);
  for (unsigned int k = 0; k < length; k++) {
    
    GdomeNode *n = gdome_nl_item(nodes, k, &exc);
    
    if (!n) 
      continue;
		/* We do not return text and CDATA as children */
		if (nodeIsText(n) || nodeIsCDATA(n)) {
			gdome_n_unref(n, &exc);
			continue;
		}
    if (tag_match(n, pattern)) {
      ret = XMLNode(n);

      gdome_n_unref(n, &exc); 
      break;
    }
    gdome_n_unref(n, &exc);
  }
  gdome_nl_unref(nodes, &exc);
  return ret;
}

GdomeNode *
XMLNode::safeNextSibling(GdomeNode *m_node, GdomeException *exc) const
{
  GdomeException exc2;
  assert(m_node != NULL);
  assert(exc != NULL);
	GdomeNode *next;
	next = gdome_n_nextSibling(m_node, exc);
	gdome_n_unref(m_node, &exc2);
  if (!*exc) 
		*exc = exc2; // report this error too
  return next;
}

XMLNode 
XMLNode::getNextSibling(const std::string &pattern= "") const {
  GdomeException exc;
  XMLNode ret;
  
  if (m_node == NULL)
    return XMLNode();
  GdomeNode *cur = m_node;
  gdome_n_ref(cur, &exc);
  while ((cur = safeNextSibling(cur, &exc)) && !exc) {
		/* Don't return text or CDATA nodes */
		if (nodeIsText(cur) || nodeIsCDATA(cur)) {
			continue;
		}

    if (tag_match(cur, pattern)) {
      ret = XMLNode(cur);
      gdome_n_unref(cur, &exc);
      return ret;
    }
  }
  gdome_n_unref(cur, &exc);
  return ret;  
}

XMLNode 
XMLNode::getParent() const {
  GdomeException exc;
  return XMLNode(gdome_n_parentNode(m_node, &exc));
}

unsigned int 
XMLNode::removeChildNodes(const std::string &pattern= "") {
  unsigned int count = 0;
  GdomeException exc;

  GdomeNodeList *nodes = gdome_n_childNodes(m_node, &exc);
  if (!nodes) return 0;

  unsigned long length = gdome_nl_length(nodes, &exc);
  for (unsigned int k = 0; k < length; k++) {
    GdomeNode *cur = gdome_nl_item(nodes, k, &exc);
    if (!cur) continue;

    if (tag_match(cur, pattern)) {
      GdomeNode *temp = gdome_n_removeChild(m_node, cur, &exc);
      gdome_n_unref(temp, &exc);
      count++;
    }

    gdome_n_unref(cur, &exc);
  }
  gdome_nl_unref(nodes, &exc);
  return count;
}

bool 
XMLNode::removeChild(XMLNode child) {
  assert(!child.isNull());
  GdomeNode *n = child.m_node;
  GdomeException exc;
  GdomeNode *temp = gdome_n_removeChild(m_node, n, &exc);
  gdome_n_unref(temp, &exc);
  return (exc == 0);
}

XMLNode
XMLNode::clone() const
{
	GdomeException exc;
	GdomeNode *cloned = gdome_n_cloneNode(m_node, 1, &exc);
	XMLNode ret(cloned);
	gdome_n_unref(cloned, &exc);
	return ret;
}

bool
XMLNode::appendChild(XMLNode child) 
{
  assert(!child.isNull());
  GdomeException exc;
  GdomeNode *temp = gdome_n_appendChild(m_node, child.m_node, &exc);
  gdome_n_unref(temp, &exc);
  return (exc == 0);
}

bool
XMLNode::importChild(XMLNode &child)
{
  GdomeException exc, exc2;
  GdomeDocument *doc = gdome_n_ownerDocument(m_node, &exc);
	if (exc)
		return false;

  GdomeNode *imported = gdome_doc_importNode(doc, child.m_node, true, &exc);
  gdome_doc_unref(doc, &exc2);
	if (exc)
		return false;
  
  GdomeNode *temp = gdome_n_appendChild(m_node, imported, &exc);
	if (exc) {
		gdome_n_unref(imported, &exc);
		return false;
	}

	gdome_n_unref(child.m_node, &exc);
	// Assign the reference to imported to child
	child.m_node = imported;
	
  gdome_n_unref(temp, &exc);
  return (exc == 0);
}

unsigned int 
XMLNode::appendChildNodes(vector<XMLNode> &children) 
{
  int added = 0;
  for (unsigned int k = 0; k < children.size(); k++)
    if(appendChild(children[k])) added++;
  return added;
}

unsigned int 
XMLNode::importChildNodes(vector<XMLNode> &children)
{
  int added = 0;
  for (unsigned int k = 0; k < children.size(); k++)
    if(importChild(children[k])) added++;
  return added;
}

//***************************************************************
// Routines for getting/setting CDATA, Text and raw XML sections
//***************************************************************
    
XMLString 
XMLNode::getText() const 
{
  GdomeNode *traverse;
  GdomeException exc;
  string rv = "";
  if (!nodeIsElement(m_node))
    return XMLString("");
  
  traverse = gdome_n_firstChild(NODE(m_node), &exc);

  while (traverse != NULL) {
    
    // no text for non-empty tags
    if (!nodeIsText(traverse) && !nodeIsCDATA(traverse)) {
      gdome_n_unref(traverse, &exc);
      return XMLString("");
    }
    
    if (nodeIsText(traverse)) {
      string data = g2str(gdome_t_data(TEXT(traverse), &exc));
      rv += data;
    }
    if (nodeIsCDATA(traverse)) {
      string data = g2str(gdome_cd_data(CHARACTER_DATA(traverse), &exc));
      rv += data;
    }
    traverse = safeNextSibling(traverse, &exc);
  }
  return rv;
}

/** 
    Remove the <?xml..?> and document tags, so we can just report the
    tags for getXML()
*/
static XMLString &
stripPrologueAndDocument(XMLString & str)
{
  GdomeDOMString *copy = gdome_str_mkref_dup(str.getDOMString()->str);
  GdomeDOMString *start = gdome_str_mkref("<?xml version=");
  assert(gdome_str_startsWith(copy, start));
  gdome_str_unref(start);
  
  /* Skip the prologue */
  gchar *begin = copy->str;
  while (*begin != '>')
    begin++;
  begin++;

  /* Skip the openning document tag */
  while (*begin != '>')
    begin++;
  begin++;

  /* Go back from the end past the closing of the document tag */
  gchar *end = copy->str + gdome_str_length(copy);
  assert(*end == '\0');
  while (*end != '<')
    end--;
  *end = 0;

  /* Final string with nasty parts removed */
  GdomeDOMString * final = gdome_str_mkref_dup(begin);
  str.replaceData(final);
  gdome_str_unref(copy);
  gdome_str_unref(final);

  return str;
}

XMLString 
XMLNode::getXML() const {  
  ostrstream strout;
  assert(m_node != NULL);

  XMLDocument d;
	XMLNode copy = clone();
  d.getRootNode().importChild(copy);
  XMLString s = d.getXML();
  return stripPrologueAndDocument(s);
}

// First DELETE ALL PRE-EXNODEISTING TEXT and CDATA !!!
bool 
XMLNode::setText(const string &text, bool nodeIs_cdata = false) 
{

  GdomeNode *traverse;
  GdomeException exc;

  if (!nodeIsElement(m_node))
    return false;
  
//  if (text.length()== 0) 
//    return false;
  
  // check for non-text children. Abort if there are any
  traverse = gdome_n_firstChild(m_node, &exc);
  while (traverse != NULL) {
    if (!nodeIsText(traverse) && !nodeIsCDATA(traverse)) {
			gdome_n_unref(traverse, &exc);
      return false;
		}
    traverse = safeNextSibling(traverse, &exc);
  }

  // delete the data
  traverse = gdome_n_firstChild(m_node, &exc);
  while (traverse != NULL) {
    if (nodeIsText(traverse) || nodeIsCDATA(traverse)) {
      GdomeNode *temp = gdome_n_removeChild(m_node, traverse, &exc);
      gdome_n_unref(temp, &exc);
    }
    traverse = safeNextSibling(traverse, &exc);
  }
  
  XMLString data(text);
  GdomeDocument *doc = gdome_n_ownerDocument(m_node, &exc);
  GdomeNode *newNode;

  if (nodeIs_cdata) {
    newNode = NODE(gdome_doc_createCDATASection(doc, data.getDOMString(), 
												&exc));
  }
  else {
    newNode = NODE(gdome_doc_createTextNode(doc, 
					    data.getDOMString(), 
					    &exc));
  }
  
  GdomeNode *temp = gdome_n_appendChild(m_node, newNode, &exc);

  gdome_n_unref(temp, &exc);
  gdome_n_unref(newNode, &exc);
  gdome_doc_unref(doc, &exc);

  return true;
}

//**************************!!!!!!!!!!!!!!!
// IMPORTANT NOTE: Since we are parsing things by creating a new document,
// we should IMPORT all nodes into our current document. 
bool 
XMLNode::setXML(const string &xml) {
  GdomeException exc;

  if (!nodeIsElement(m_node))
    return false;
  
  XMLDocument tempdoc;
  tempdoc.loadFromString(xml);

  GdomeNode *parent;

  parent = gdome_n_parentNode(m_node, &exc);
  if (parent == NULL)
    return false;

  GdomeDocument *pardoc;
  pardoc = gdome_n_ownerDocument(m_node, &exc);
  if (exc) {
    gdome_n_unref(parent, &exc);
    return false;
  }

  GdomeNode *replacement = 
    NODE(gdome_doc_documentElement(tempdoc.m_doc, &exc));
  GdomeNode *imported = gdome_doc_importNode(pardoc, replacement, true, &exc);
  
	GdomeNode *oldChild = gdome_n_replaceChild(parent, imported, m_node, &exc);
	assert(exc == 0);
	gdome_n_unref(oldChild, &exc);
	assert(exc == 0);

  gdome_n_unref(parent, &exc);
  gdome_n_unref(replacement, &exc);
  gdome_doc_unref(pardoc, &exc);

  gdome_n_unref(m_node, &exc);
	// Consume the reference from importNode
  m_node = imported;

  return true;
}

bool 
XMLNode::appendXML(const string &xml) {
  if (!nodeIsElement(m_node))
    return false;

  XMLDocument tempdoc;
  tempdoc.loadFromString(xml);
  XMLNode replacement = *tempdoc.m_root;
  return importChild(replacement);
}
