
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _EZ_XML_NODE_H
#define _EZ_XML_NODE_H

// Stefan Atev, IBM Extreme Blue 2001
// Node printing function taken from DOMPrint in SashMo/tools

#include <string>
#include <vector>
#include <iosfwd>
#include "XMLTagMatch.h"
#include "XMLTypeWrapper.h"
#include "XMLDocument.h"
#include "XMLString.h"

class XMLNode;

ostream & operator<<(ostream &os, const XMLNode &doc);

// Functions from DOMPrint;
void outputContent(std::ostream &target, const GdomeDOMString * &toWrite);

class XMLNode {
  
 public:
  
  // These constructors do not consume a reference to the objects
  XMLNode(GdomeNode *n);
  XMLNode(GdomeElement *el);
  XMLNode(const XMLNode &);

  XMLNode();
  XMLNode(const std::string tag_name, XMLDocument doc = XMLDocument());
  ~XMLNode();
    
  //**************************
  // General access functions
  //**************************
    
  std::string getName() const;
  bool isNull() const;
  bool matchTag(const std::string &pattern);
    
  //**************************
  //      Operators
  //**************************
  XMLNode &operator=(const XMLNode &in);

  //**************************
  // Type query functions
  //**************************
  NodeType getType() const;
  inline bool isElement() const { return getType() == ELEMENT_NODE; }
  inline bool isAttr() const { return getType() == ATTRIBUTE_NODE; }
  inline bool isText() const { return getType() == TEXT_NODE; }
  inline bool isCDATA() const { return getType() == CDATA_SECTION_NODE; }
  inline bool isComment() const { return getType() == COMMENT_NODE; }
    
  //******************************************************
  // Attribute access functions
  // If attribute is not available or the current XMLNode
  // is not really an element, nothing bad happens
  // return value of TRUE means success
  //******************************************************
  
  bool hasAttribute(const std::string &attribute_name) const;
  unsigned int hasAttributes(const std::string &pattern= "") const;
  std::string getAttribute(const std::string &attribute_name) const;
  bool setAttribute(const std::string &attribute_name, const std::string &value);
  bool removeAttribute(const std::string &attribute_name);
  std::vector<std::string> getAttributeNames(const std::string &pattern= "") const;
    
  //**********************************************
  // node access routines
  // if tag_name is not "" it will only refer to
  // children with that have a that same tag name
  //**********************************************
    
  unsigned int hasChildNodes(const std::string pattern= "") const;
  vector<XMLNode> getChildNodes(const std::string &pattern= "") const;
  XMLNode getFirstChildNode(const std::string &pattern= "") const;
  XMLNode getNextSibling(const std::string &pattern= "") const;
  XMLNode getParent() const;
  unsigned int removeChildNodes(const std::string &pattern= "");
  bool removeChild(XMLNode child);

	XMLNode clone() const;
  bool appendChild(XMLNode child);
  bool importChild(XMLNode &child);
  unsigned int appendChildNodes(vector<XMLNode> &children);

  //! Import the children into the current document and append them
  unsigned int importChildNodes(vector<XMLNode> &children);
    
  //***************************************************************
  // Routines for getting/setting CDATA, Text and raw XML sections
  //***************************************************************
    
  XMLString getText() const;
  XMLString getXML() const;
  bool setText(const std::string &text, bool is_cdata= false);
  bool setXML(const std::string &xml);
  bool appendXML(const std::string &xml);

 private:
  
  /**
     Returns a pointer to the next sibling, reducing the refcount on "node"
     even if there's an error
  */
  GdomeNode *safeNextSibling(GdomeNode *node, GdomeException *exc) const;

  friend class XMLDocument;
    
  GdomeNode *m_node;
};

#endif // _EZ_XML_NODE_H
