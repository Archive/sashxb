
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include <gdome.h>
#include "XMLTypeWrapper.h"

bool 
nodeIsType(GdomeNode *node, NodeType type)
{
  GdomeException exc;
  return (node != NULL && 
					nodeTypeConv(gdome_n_nodeType(node, &exc)) == type);
}


#define MYCASE(x) case GDOME_ ## x: return x;

NodeType
nodeTypeConv(unsigned int gdomeType)
{
	switch (gdomeType) {
		MYCASE(ELEMENT_NODE);
		MYCASE(ATTRIBUTE_NODE);
		MYCASE(TEXT_NODE);
		MYCASE(CDATA_SECTION_NODE);
		MYCASE(COMMENT_NODE);
		MYCASE(DOCUMENT_NODE);
	default:
		return UNKNOWN_NODE;
	}
}
