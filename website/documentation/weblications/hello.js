// Usually we'll assign variables to useful objects.
// The following are used to access elements in the glade GUI.

// The glade file that contains all of the GUI information.
// Since we're using the Console location, we have to explicitly
// tell the Glade extension what UI files to use.
var gladefile = new Sash.Glade.GladeFile("hello.glade");

// The text entry element that contains the name.
var nameWidget = gladefile.getWidget("username");

// The label element that contains text.
var responseWidget = gladefile.getWidget("response");

// The function that we associate with a button click.
function on_ok_clicked(){

    // Grab the text out of the username text entry field.
    var name = nameWidget.getText();

    // some default value.
    if (name == "") {
        name = "world";
    }

    // for debugging
    printsys("Saying hello to  " + name);

    // set the text in the label area.
    responseWidget.setText ("hello, " + name);
}
