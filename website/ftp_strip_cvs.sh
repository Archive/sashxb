if [ $# -ne 3 ]; then
	echo
	echo This script strips CVS dirs from a directory structure
	echo and FTPs what\'s left to a specified server.
	echo
	echo Run as
	echo $0 remote_host remote_dir username
	echo
	exit 127
fi
echo Copying to temp dir...
cp -r `pwd` /tmp/$2
cd /tmp/$2
echo Removing CVS dirs...
rm -rf `find . -path './*CVS'`
cd /tmp
echo FTPing to $1/$2...
ncftpput -E -u $3 -R $1 . $2
echo Cleaning up temp dir...
rm -rf /tmp/$2
