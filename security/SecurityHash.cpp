
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#include "SecurityHash.h"
#include "SecurityItem.h"
#include "Registry.h"
#include "debugmsg.h"
#include <algorithm>
using namespace std;

// security_hash_new: creates a new security hash given the 
//   file name of a security registry
SecurityHash::SecurityHash(const string& filename, const vector<string> goodkeys, Registry *preg= NULL) 
{
	 Registry *reg;
	 Registry workReg;
	 
	 if (filename != "") {
	   if (!preg && !workReg.OpenFile(filename)) {
		 OutputMessage("Warning: Could not open file %s\n", filename.c_str());
		 return;
	   }
	 }
	 if (preg)
	   reg= preg;
	 else
	   reg= &workReg;
	 vector<string> keys = reg->EnumKeys("");
	 vector<string> subkeys;

	 for (unsigned int keyIndex = 0; keyIndex < keys.size(); keyIndex++) {
		  string& key = keys[keyIndex];

		  DEBUGMSG(security, "adding key %s (goodkey size is %d)\n", key.c_str(), goodkeys.size());
		  // Top level keys are extension names or "Global"
		  // we only want the keys that are in goodkeys, or Global
		  if (key != SECURITY_GLOBAL && goodkeys.size() > 0 &&
			  find(goodkeys.begin(), goodkeys.end(), key) == goodkeys.end()) {
			   continue;
		  }
		  
		  DEBUGMSG(security, "continuing with key %s\n", key.c_str());

		  // for each top level key...
		  subkeys = reg->EnumKeys(key);
		  for (unsigned int subKeyIndex = 0; subKeyIndex < subkeys.size(); subKeyIndex++) {
			   // the subkey is the security setting name
			   string keyPath = key + "\\" + subkeys[subKeyIndex];

			   RegistryValue tagVals[NUM_SECURITY_TAG_TYPES];
			   for (int tag = 0; tag < NUM_SECURITY_TAG_TYPES; tag++) {
					reg->QueryValue(keyPath, SecurityItem::SecurityTagTypeString[tag], &tagVals[tag]);
					if (tagVals[tag].GetValueType() == RVT_INVALID) {
						 // set a default value if the tag was not found
						 tagVals[tag].SetValueType(RVT_NUMBER);
						 tagVals[tag].m_Number = 0;
					}
			   }

			   SecurityItem item(key, subkeys[subKeyIndex], tagVals);
			   m_secHash[keyPath] = item;
		  }
	 }
}

SecurityItem SecurityHash::Lookup(const string & key) {
  if (m_secHash.count(key) >= 1) {
	DEBUGMSG(security, "Found key %s\n", key.c_str());
	return m_secHash[key];
  }
  else {
	DEBUGMSG(security, "Key %s not found\n", key.c_str());
	return SecurityItem();
  }
}

void SecurityHash::Insert(const string & key, const SecurityItem & item) {
  DEBUGMSG(security, "Inserting key %s with type %d\n", key.c_str(), (int) item.m_Type);
  m_secHash[key] = item;
}

void SecurityHash::Save(const string & filename) {
  Registry reg;
  sec_hash::iterator a = m_secHash.begin(), b = m_secHash.end();
  while (a != b) {
	DEBUGMSG(security, "Saving key %s\n", a->first.c_str());
	a->second.WriteToRegistry(reg, a->first);
	++a;
  } 
  reg.WriteToDisk(filename);
}

void SecurityHash::Print() {
  cout << "Hash has " << m_secHash.size() << " key/value pairs" << endl;
  sec_hash::iterator a = m_secHash.begin(), b = m_secHash.end();
  while (a != b) {
	cout << "Key: " << a->first << endl;
	a->second.Print();
	++a;
  }	
}
