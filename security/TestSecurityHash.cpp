
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "SecurityHash.h"

#include <iostream.h>

const string reg1 = "security.dat";
const string reg2 = "security2.dat";

int main() 
{
  SecurityHash * sec_hash;

  cout << "Opening registry " << reg1 << endl;
  sec_hash = new SecurityHash(reg1);

  cout << endl << "Hash table size: " << sec_hash->Size()
       << endl << endl;
  cout << "Saving registry " << reg2 << endl;
  sec_hash->Save(reg2);

  cout << "Deleting security hash" << endl;
  delete sec_hash;

  cout << "Test succeded" << endl;

  return 0;
}
