
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

// AJ Shankar, John Corwin

#ifndef _SECURITY_ITEM_H_
#define _SECURITY_ITEM_H_

#include <string>
#include <vector>
#include "security.h"
#include "Registry.h"

enum SecurityType {
  ST_INVALID = -1,
  ST_BOOL = 0,
  ST_NUMBER,
  ST_STRING,
  ST_STRING_ENUM,
  ST_STRING_ARRAY,
  ST_NUM_VALUES
};

enum SecurityTagTypes {
	 SECURITY_DESCRIPTION_TAG,
	 SECURITY_TYPE_TAG,
	 SECURITY_VALUE_TAG,
	 SECURITY_ENUM_VALUE_TAG,
	 NUM_SECURITY_TAG_TYPES
};

struct SecurityItem {
protected:
  void make_registry_values(RegistryValue v[NUM_SECURITY_TAG_TYPES]);

public:

  static char * SecurityTagTypeString[];

  std::string m_Parent;
  int m_ID;
  std::string m_DescriptiveName;
  
  SecurityType m_Type;
  
  bool m_Bool;
  float m_Number;
  std::string m_String;
  std::vector<std::string> m_StringVals;
  int m_StringIndex;

  SecurityItem(std::string p, int i, std::string d, SecurityType t) {
	SecurityItem();
	m_Parent = p; m_ID = i; m_DescriptiveName = d, m_Type = t;
  }

  SecurityItem() {
    m_Type = ST_INVALID;
    m_Bool = false;
    m_Number = 0.0;
    m_StringIndex = 0;
  }

  SecurityItem(const string & parent, const string & name, const RegistryValue v[NUM_SECURITY_TAG_TYPES]);

  void WriteToRegistry(Registry & reg, const string & keyName);
  void Print();

};

inline bool operator<(const SecurityItem & a, const SecurityItem & b) { 
	 return a.m_ID < b.m_ID; 
}


#endif //_SECURITY_ITEM_H_
