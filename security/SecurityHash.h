
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): John Corwin

*****************************************************************/

#ifndef _SECURITY_HASH_H
#define _SECURITY_HASH_H

#include <glib.h>
#include <string>
#include <vector>
#include <hash_map>
#include "SecurityItem.h"

class Registry;

// code to make a hashtable with string keys
struct strhash {
	 static hash<const char*> h;
	 size_t operator()(const string& s) const {
		  return h(s.c_str());
	 }
};
hash<const char*> strhash::h;
typedef hash_map<string, SecurityItem, strhash> sec_hash;

// SecurityHash: manages security item key/value pairs
class SecurityHash
{
 protected:
  sec_hash m_secHash;

 public:

  SecurityHash(const string& filename = "", vector<string> goodkeys = vector<string>(), Registry *preg= NULL);

  // Lookup: get the security item for the given key
  SecurityItem Lookup(const string & key);

  // Insert: add a security item for a particular key
  void Insert(const string & key, const SecurityItem & item);

  // Size: the number of security items in the table
  unsigned int Size() { return m_secHash.size(); }
 
  // GetHash: returns a reference to the internal hashmap
  sec_hash & GetHash() { return m_secHash; }

  // Save: save the security hash to a file
  void Save(const string & filename);

  // Clear: delete the contents of the hash 
  void Clear() { m_secHash.clear();}

  // Print: print each key/value pair in the table
  void Print();
};

// made "public" so the WDE can use it for free
void security_item_write_to_registry(void * key, void * value, void * user);

#endif
