#include "SecurityItem.h"
#include "debugmsg.h"

char * SecurityItem::SecurityTagTypeString[] = {
  "Description",
  "Type",
  "Value",
  "EnumValue"
};

SecurityItem::SecurityItem(const string & parent, const string & name, const RegistryValue v[NUM_SECURITY_TAG_TYPES]) {
  m_Parent = parent;
  m_ID = strtol(name.c_str(), NULL, 10);
  m_DescriptiveName = v[0].m_String;      // Descriptive name
  m_Type = (SecurityType) v[1].m_Number;  // value type
  m_StringIndex = -1;
  DEBUGMSG(security, "New security item %s with type %s (#%f)\n", 
		   (parent + "/" + name).c_str(),
		   SecurityItem::SecurityTagTypeString[m_Type],
		   v[1].m_Number);
  
  // Security key value
  switch (m_Type) {
  case ST_BOOL:
	m_Bool = v[2].m_Number;
	break;
  case ST_NUMBER:
	m_Number = v[2].m_Number;
	break;
  case ST_STRING:
	m_String = v[2].m_String;
	break;
  case ST_STRING_ENUM:
	m_StringVals = v[2].m_Array;
	m_StringIndex = (int) v[3].m_Number;
	DEBUGMSG(security, "set string index to %d\n", m_StringIndex);
	break;
  case ST_STRING_ARRAY:
	m_StringVals = v[2].m_Array;
	break;
  default:
	break;
  }
}

void SecurityItem::Print()
{
  cout << "\tID: " << m_ID << endl;
  cout << "\tParent: " << m_Parent << endl;
  cout << "\tName: " << m_DescriptiveName << endl;
  cout << "\tType: " << m_Type << endl;
  cout << endl;
}

// make_registry_values: make a set of registry values from a SecurityItem
void SecurityItem::make_registry_values(RegistryValue v[NUM_SECURITY_TAG_TYPES])
{
  // description 
  v[0].SetValueType(RVT_STRING);
  v[0].m_String = m_DescriptiveName;

  // default string index value
  v[3].SetValueType(RVT_NUMBER);
  v[3].m_Number = 0;

  // type and value
  v[1].SetValueType(RVT_NUMBER);
  switch (m_Type) {
  case ST_BOOL:
	v[1].m_Number = ST_BOOL;
	v[2].SetValueType(RVT_NUMBER);
	v[2].m_Number = m_Bool;
	break;
  case ST_NUMBER:
	v[1].m_Number = ST_NUMBER;
	v[2].SetValueType(RVT_NUMBER);
	v[2].m_Number = m_Number;
	break;
  case ST_STRING:
	v[1].m_Number = ST_STRING;
	v[2].SetValueType(RVT_STRING);
	v[2].m_String = m_String;
	break;
  case ST_STRING_ENUM:
	v[1].m_Number = ST_STRING_ENUM;
	v[2].SetValueType(RVT_ARRAY);
	v[2].m_Array = m_StringVals;
	// enum value
	DEBUGMSG(security, "string index is %d\n", m_StringIndex);
	v[3].m_Number = m_StringIndex;
	break;
  case ST_STRING_ARRAY:
	v[1].m_Number = ST_STRING_ARRAY;
	v[2].SetValueType(RVT_ARRAY);
	v[2].m_Array = m_StringVals;
	break;
  default:
	break;
  }
}

// security_item_write_to_registry: write a SecurityItem to a registry
void SecurityItem::WriteToRegistry(Registry & reg, const string & keyName)
{
	 RegistryValue tags[NUM_SECURITY_TAG_TYPES];

	 make_registry_values(tags);

	 for (int tag = 0; tag < NUM_SECURITY_TAG_TYPES; tag++)
		  reg.SetValue(keyName, SecurityTagTypeString[tag], &tags[tag]);
}
