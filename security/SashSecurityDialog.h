#ifndef SASH_SECURITY_DIALOG_H
#define SASH_SECURITY_DIALOG_H

#include "SecurityHash.h"
#include <gnome.h>
#include <glade/glade.h>

class SashSecurityDialog
{
 public:
  typedef pair<string, SecurityItem> sec_item;

  SashSecurityDialog();
  bool Run(SecurityHash & settings, GtkWindow * parent);

 protected:
  static SashSecurityDialog * s_ActiveDialog;
  SecurityHash m_working;
  GtkWidget * m_dialog;

  static void OnBoolChanged(GtkToggleButton * button, char * key);
  static void OnNumberChanged(GtkEditable * entry, char * key);
  static void OnStringChanged(GtkEditable * entry, char * key);
  static void OnStringEnumChanged(GtkEditable * entry, char * key);

  void SetupDialog(GtkWindow * parent);
  void SetupGroup(GtkWidget * page_box, vector<sec_item> & items);
  void GetSecurityGroups(vector<string> & groups);
  void GetGroupSettings(const string & group, vector<sec_item> & settings);
  void GetGroupName(vector<sec_item> &items, string &ret);
  void SetComboEntries(GtkWidget * combo, const SecurityItem &item);

};

inline bool operator<(const SashSecurityDialog::sec_item & a, const SashSecurityDialog::sec_item & b) {
	 return a.second.m_ID < b.second.m_ID;
}

#endif
