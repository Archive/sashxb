
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "SecurityManager.h"
#include "SecurityItem.h"
#include "SecurityHash.h"
#include "debugmsg.h"
#include "xpcomtools.h"
#include "nsIFactory.h"
#include "nsIComponentManager.h"
#include "nsComponentManagerUtils.h"
#include "nsIServiceManager.h"
#include "nsIGenericFactory.h"
#include "sashVariantUtils.h"
#include "sashILocation2.h"
#include <unistd.h>

using namespace std;

NS_IMPL_THREADSAFE_ISUPPORTS1(SecurityManager, sashISecurityManager)

SecurityManager::SecurityManager() : m_pSecuritySettings(NULL) {
  NS_INIT_ISUPPORTS();
  m_loc = NULL;
}

SecurityManager::~SecurityManager() {
	 if (m_pSecuritySettings) 
	   delete m_pSecuritySettings;
}

NS_IMETHODIMP SecurityManager::SetLocation(nsISupports * loc) {
	 m_loc = loc;
	 return NS_OK;
}

void SecurityManager::QuitLocation() {
	 if (m_loc) {
		  nsCOMPtr<sashILocation2> loc2 = do_QueryInterface(m_loc);
		  if (loc2) {
			   loc2->Quit();
			   _exit(1);
		  }
		  else {
			   nsCOMPtr<sashILocation> loc1 = do_QueryInterface(m_loc);
			   if (loc1)
					loc1->Quit();
			   else
					_exit(1);
		  }
	 }
	 else {
		  _exit(1);
	 }
}

NS_IMETHODIMP SecurityManager::Initialize(const char* file) {
 	 m_SecurityFile = file;
 	 m_pSecuritySettings = new SecurityHash(m_SecurityFile);

 	 if (!m_pSecuritySettings) 
 		  OutputError("Could not load security file!");
	 return NS_OK;
}

NS_IMETHODIMP SecurityManager::AssertBool(SecuritySetting s, PRBool val) {
	 return AssertExtBool(SECURITY_GLOBAL.c_str(), s, val);
}

NS_IMETHODIMP SecurityManager::AssertNumOrEnum(SecuritySetting s, float val) {
	 return AssertExtNumOrEnum(SECURITY_GLOBAL.c_str(), s, val);
}

NS_IMETHODIMP SecurityManager::AssertString(SecuritySetting s, const char* val) {
	 return AssertExtString(SECURITY_GLOBAL.c_str(), s, val);
}

NS_IMETHODIMP SecurityManager::AssertExtBool(const char* guid, SecuritySetting ss, PRBool val) {
	 DEBUGMSG(security, "AssertExtBool\n");
	 SecurityItem s = GetSecurityItem(guid, ss);
	 if (s.m_Bool == val) return NS_OK;
	 SecurityViolation(s, (bool) val);
	 SetSecurityItem(guid, ss, s);
	 return NS_OK;
}

NS_IMETHODIMP SecurityManager::AssertExtNumOrEnum(const char* guid, SecuritySetting ss, float val) {
	 DEBUGMSG(security, "AssertExtNumOrEnum\n");
	 SecurityItem s = GetSecurityItem(guid, ss);
	 if (s.m_Type == ST_STRING_ENUM && s.m_StringIndex >= val) return NS_OK;
	 if (s.m_Type == ST_NUMBER && s.m_Number >= val) return NS_OK;
	 DEBUGMSG(security, "Security value \"%s\" = %d\n", s.m_DescriptiveName.c_str(), 
			  s.m_StringIndex);
	 SecurityViolation(s, val);
	 DEBUGMSG(security, "value is now \"%s\" = %d\n", s.m_DescriptiveName.c_str(), 
			  s.m_StringIndex);
	 SetSecurityItem(guid, ss, s);

	 return NS_OK;
}

NS_IMETHODIMP SecurityManager::AssertExtString(const char* guid, SecuritySetting ss, const char* val) {
	 string strval = val;
	 DEBUGMSG(security, "AssertExtString\n");
	 SecurityItem s = GetSecurityItem(guid, ss);

	 if (s.m_Type == ST_STRING){
		  if (s.m_String == strval) return NS_OK;
	 } else if (s.m_Type == ST_STRING_ARRAY){
		  DEBUGMSG(security, "looking for: %s\n", strval.c_str());
		  if (find(s.m_StringVals.begin(), s.m_StringVals.end(),
				   strval) != s.m_StringVals.end())
			   return NS_OK;
	 }

	 SecurityViolation(s, strval);
	 SetSecurityItem(guid, ss, s);
	 return NS_OK;
}

NS_IMETHODIMP SecurityManager::QueryBool(SecuritySetting s, PRBool* val) {
	 return QueryExtBool(SECURITY_GLOBAL.c_str(), s, val);
}

NS_IMETHODIMP SecurityManager::QueryNumOrEnum(SecuritySetting s, float* val) {
	 return QueryExtNumOrEnum(SECURITY_GLOBAL.c_str(), s, val);
}

NS_IMETHODIMP SecurityManager::QueryString(SecuritySetting s, char** val) {
	 return QueryExtString(SECURITY_GLOBAL.c_str(), s, val);
}

/* void QueryStringArray (in long setting, [retval] out nsIVariant strarr); */
NS_IMETHODIMP SecurityManager::QueryStringArray(SecuritySetting s, 
												nsIVariant **strarr)
{
	 return QueryExtStringArray(SECURITY_GLOBAL.c_str(), s, strarr);
}


NS_IMETHODIMP SecurityManager::QueryExtBool(const char* guid, SecuritySetting ss, PRBool* val) {
	 SecurityItem s = GetSecurityItem(guid, ss);
	 *val = s.m_Bool;
	 return NS_OK;
}

NS_IMETHODIMP SecurityManager::QueryExtNumOrEnum(const char* guid, SecuritySetting ss, float* val) {
	 SecurityItem s = GetSecurityItem(guid, ss);
	 if (s.m_Type == ST_STRING_ENUM)
		  *val = s.m_StringIndex;
	 if (s.m_Type == ST_NUMBER) 
		  *val = s.m_Number;
	 return NS_OK;
}

NS_IMETHODIMP SecurityManager::QueryExtString(const char* guid, SecuritySetting ss, char** val) {
	 SecurityItem s = GetSecurityItem(guid, ss);
	 XP_COPY_STRING(s.m_String.c_str(), val);
	 return NS_OK;
}

/* void QueryExtStringArray (in string extension_guid, in long setting, [retval] out nsIVariant strarr); */
NS_IMETHODIMP SecurityManager::QueryExtStringArray(const char *guid,
												   SecuritySetting ss, 
												   nsIVariant **strarr)
{
	 SecurityItem s = GetSecurityItem(guid, ss);
	 if (NewVariant(strarr) != NS_OK) return NS_ERROR_FAILURE;
	 VariantSetArray(*strarr, s.m_StringVals);
	 return NS_OK;
}

void SecurityManager::SaveSecurityHash() {
  m_pSecuritySettings->Save(m_SecurityFile);
}

SecurityItem SecurityManager::GetSecurityItem(const string& guid, const SecuritySetting ss) {
	 char* strnum = g_strdup_printf("%d", ss);
	 SecurityItem s = m_pSecuritySettings->Lookup(guid + "\\" + strnum);
	 g_free(strnum);
	 if (s.m_Type == ST_INVALID)
		  OutputError("Invalid security check from %s: %d!", guid.c_str(), ss);

	 return s;
}

void SecurityManager::SetSecurityItem(const string& guid, const SecuritySetting ss,
									  const SecurityItem & s) 
{
	 char* strnum = g_strdup_printf("%d", ss);
	 m_pSecuritySettings->Insert(guid + "\\" + strnum, s);
	 g_free(strnum);
	 SaveSecurityHash();
}

/* void SecurityViolationBool (in string extension_guid, in boolean val); */
NS_IMETHODIMP SecurityManager::SecurityViolationBool(const char *extension_guid, SecuritySetting ss, PRBool val)
{
	 SecurityItem s = GetSecurityItem(extension_guid, ss);
	 SecurityViolation(s, (bool)val);
	 return NS_OK;
}

/* void SecurityViolationNumOrEnum (in string extension_guid, in long val); */
NS_IMETHODIMP SecurityManager::SecurityViolationNumOrEnum(const char *extension_guid, SecuritySetting ss, float val)
{
	 SecurityItem s = GetSecurityItem(extension_guid, ss);
	 SecurityViolation(s, (float)val);
	 return NS_OK;
}

/* void SecurityViolationString (in string extension_guid, in string val); */
NS_IMETHODIMP SecurityManager::SecurityViolationString(const char *extension_guid, SecuritySetting ss, const char *val)
{
	 SecurityItem s = GetSecurityItem(extension_guid, ss);
	 SecurityViolation(s, val);
	 return NS_OK;
}

void SecurityManager::SecurityViolation(SecurityItem & s, const bool val) {
	 DEBUGMSG(security, "security violation for bool val\n");
	 string what = val ? "wants" : "does not want";
	 int i = Output3Choice("Deny", "Allow Once", "Allow Always",  "Security Violation!\n\n"
						   "The weblication %s to %s; "
						   "Do you want to allow it to continue?", what.c_str(), s.m_DescriptiveName.c_str());
	 if (i == 2) {
		  s.m_Bool = val;
	 } else if (i < 1) {
		  QuitLocation();
	 }
}

void SecurityManager::SecurityViolation(SecurityItem & s, const float val) {
	 DEBUGMSG(security, "security violation for float val\n");
	 string wants, gets;
	 if (s.m_Type == ST_STRING_ENUM) {
		  wants = s.m_StringVals[(int)val];
		  gets = s.m_StringVals[s.m_StringIndex];
	 } else {
		  char *w, *g;
		  wants = w = g_strdup_printf("%f", val);
		  gets = g = g_strdup_printf("%f", s.m_Number);
		  delete w; delete g;
	 }
		  
	 int i = Output3Choice("Deny", "Allow Once", "Allow Always",  "Security Violation!\n\n"
						   "The weblication wants '%s' "
						   "to be '%s' or greater but it is '%s'.\n\n"
						   "Do you want to allow it to continue?", s.m_DescriptiveName.c_str(), wants.c_str(), gets.c_str());
	 if (i == 2) { 
		  if (s.m_Type == ST_STRING_ENUM)
			   s.m_StringIndex = (int) val;
		  else 
			   s.m_Number = val;

	 } else if (i < 1) {
		  QuitLocation();
	 }
}

void SecurityManager::SecurityViolation(SecurityItem & s, const string& val) {
	 DEBUGMSG(security, "security violation for string val\n");
	 string allowablestr;

	 if (s.m_Type == ST_STRING_ARRAY){
		  DEBUGMSG(security, "security violation for string array\n");
		  allowablestr = "in\n(";

		  vector<string>::iterator ai = s.m_StringVals.begin(),
			   bi = s.m_StringVals.end();
		  while (ai != bi){

			   allowablestr += ('\'' + (*ai) + '\'');
			   if ((ai + 1) != bi){
					allowablestr += ", ";
			   }

			   ++ai;
		  }
		  allowablestr += ");\n";
	 } else {
		  DEBUGMSG(security, "security violation for string\n");
		  allowablestr = '\'' + s.m_String + '\'' + ';';
	 }
	 int i = Output3Choice("Deny", "Allow Once", 
						   "Allow Always", "Security Violation!\n\n" 
						   "The weblication wants '%s' to be '%s', "
						   "but it is supposed to be %s "
						   "Do you want to allow it to continue?", 
						   s.m_DescriptiveName.c_str(), 
						   val.c_str(), allowablestr.c_str());
	 if (i == 2) { 
		  s.m_String = val;
	 } else if (i < 1) {
		  QuitLocation();
	 }
}

NS_GENERIC_FACTORY_CONSTRUCTOR(SecurityManager)

  static nsModuleComponentInfo components[] = {
    { "SecurityManager component",
      SECURITYMANAGER_CID,
      SECURITYMANAGER_CONTRACT_ID,
      SecurityManagerConstructor,
      NULL,
      NULL
    }
  };

NS_IMPL_NSGETMODULE("SecurityManagerModule", components)

