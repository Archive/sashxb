
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

// file to generate first global security.dat
// AJ Shankar

#include "Registry.h"
#include "wdf.h"
#include <string>
#include "SecurityItem.h"
#include <stdlib.h>
#include <glib.h>
#include "SecurityHash.h"
#include <iostream.h>

using namespace std;

void print(const string & key, const SecurityItem & i) {
	 cout << key << ": " << i.m_DescriptiveName << endl;
}

int main(int argc, char* argv[]) {
	 string g = SECURITY_GLOBAL;
	 SecurityItem i[GSS_NUM_VALUES];
	 vector<string> arys[GSS_NUM_VALUES];
	 arys[GSS_REGISTRY_ACCESS].push_back("none");
	 arys[GSS_REGISTRY_ACCESS].push_back("local");
	 arys[GSS_REGISTRY_ACCESS].push_back("all");
	 arys[GSS_FILESYSTEM_ACCESS] = arys[GSS_REGISTRY_ACCESS];
	 arys[GSS_JAVA].push_back("low");
	 arys[GSS_JAVA].push_back("medium");
	 arys[GSS_JAVA].push_back("high");

	 i[GSS_REGISTRY_ACCESS] = SecurityItem(g, GSS_REGISTRY_ACCESS, "Registry access level", ST_STRING_ENUM);
	 i[GSS_FILESYSTEM_ACCESS] = SecurityItem(g, GSS_FILESYSTEM_ACCESS, "Filesystem access level", ST_STRING_ENUM);
	 i[GSS_JAVA] = SecurityItem(g, GSS_JAVA, "Java power", ST_STRING_ENUM);
  i[GSS_CHANNEL_USE] = SecurityItem(g, GSS_CHANNEL_USE, "Allow Channels", ST_BOOL);
  i[GSS_PROCESS_SPAWNING] = SecurityItem(g, GSS_PROCESS_SPAWNING, "Allow starting of other applications", ST_BOOL);
  i[GSS_PRINTING] = SecurityItem(g, GSS_PRINTING, "Allow printing", ST_BOOL);
  i[GSS_CLIPBOARD] = SecurityItem(g, GSS_CLIPBOARD, "Allow copying and pasting from the clipboard", ST_BOOL);
  i[GSS_REGISTER] = SecurityItem(g, GSS_REGISTER, "Allow COM object registration", ST_BOOL);
  i[GSS_DOWNLOAD] = SecurityItem(g, GSS_DOWNLOAD, "Allow uploads/downloads", ST_BOOL);
  i[GSS_ACTION_SPAWNING] = SecurityItem(g, GSS_ACTION_SPAWNING, "Allow starting of other weblications", ST_BOOL);
  i[GSS_RESTART_BROWSER] = SecurityItem(g, GSS_RESTART_BROWSER, "Allow the weblication to restart Mozilla", ST_BOOL);
  i[GSS_RPC] = SecurityItem(g, GSS_RPC, "Allow XML RPC/SOAP", ST_BOOL);

  
  Registry r;
  r.OpenFile("security.dat");
  for (int j = 0 ; j < GSS_NUM_VALUES ; j++) {
	   char* ch = g_strdup_printf("%d", i[j].m_ID);
	   string path = g + "\\" + ch;
	   delete[] ch;

	   RegistryValue rv;
	   rv.SetValueType(RVT_NUMBER);
	   rv.m_Number = i[j].m_Type;
	   r.SetValue(path, SecurityItem::SecurityTagTypeString[SECURITY_TYPE_TAG], &rv);
	   
	   rv.SetValueType(RVT_STRING);
	   rv.m_String = i[j].m_DescriptiveName;
	   r.SetValue(path, SecurityItem::SecurityTagTypeString[SECURITY_DESCRIPTION_TAG], &rv);

	   if (i[j].m_Type == ST_STRING_ENUM) {
			rv.SetValueType(RVT_ARRAY);
			rv.m_Array = arys[j];
			r.SetValue(path, SecurityItem::SecurityTagTypeString[SECURITY_VALUE_TAG], &rv);
	   }
  }
  // set the global descriptive name
  RegistryValue oo;
  oo.SetValueType(RVT_STRING);
  oo.m_String = SECURITY_GLOBAL;
  r.SetValue(g + "\\" + SECURITY_KEY_DESCRIP, SecurityItem::SecurityTagTypeString[SECURITY_DESCRIPTION_TAG], &oo);
  r.WriteToDisk();

  SecurityHash hash("security.dat");
  sec_hash::iterator a = hash.GetHash().begin(), b = hash.GetHash().end();
  while (a != b) {
	print(a->first, a->second);
	++a;
  }
}

