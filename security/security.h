
/***************************************************************
    SashXB for Linux
    The SashXB Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _SECURITY_H_
#define _SECURITY_H_
#include <string>

typedef int SecuritySetting;

enum GlobalSecuritySetting {
  GSS_REGISTRY_ACCESS = 0,
  GSS_CHANNEL_USE,
  GSS_PROCESS_SPAWNING,
  GSS_PRINTING,
  GSS_CLIPBOARD,
  GSS_REGISTER,
  GSS_DOWNLOAD,
  GSS_ACTION_SPAWNING,
  GSS_FILESYSTEM_ACCESS,
  GSS_RESTART_BROWSER,
  GSS_JAVA,
  GSS_RPC,
  GSS_NUM_VALUES,
  GSS_UNDEFINED = -1
};

enum RegistrySecuritySettings {
	 RSS_NONE = 0,
	 RSS_LOCAL,
	 RSS_GLOBAL
};

enum FilesystemSecuritySettings {
	 FSS_NONE = 0,
	 FSS_LOCAL,
	 FSS_GLOBAL
};

enum JavaSecuritySettings {
	 JSS_LOW,
	 JSS_MEDIUM,
	 JSS_HIGH
};

const std::string SECURITY_GLOBAL = "Global";
const std::string SECURITY_KEY_DESCRIP = "-1";

#endif //  _SECURITY_H_
