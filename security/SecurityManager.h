/***************************************************************
    Sash for Linux
    The Sash Runtime for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashxb@sashxb.org

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************

Contributor(s): AJ Shankar, John Corwin

The security manager class. Provides methods for an extension or location
to query and assert security settings.

*****************************************************************/

#ifndef _SECURITYMANAGER_H_
#define _SECURITYMANAGER_H_

#include <assert.h>
#include <string>
#include <vector>
#include <nsComponentManagerUtils.h>
#include <glib.h>
#include "wdf.h"
#include "SecurityHash.h"
#include "nsID.h"
#include "nsIFactory.h"
#include "sashISecurityManager.h"


#define SECURITYMANAGER_CID {0xfa72d34e, 0x0b20, 0x43ad, {0x8f, 0xfa, 0xd1, 0x5f, 0x85, 0x53, 0x16, 0xe2}}
NS_DEFINE_CID(kSecurityManagerCID, SECURITYMANAGER_CID);

class SecurityManager : public sashISecurityManager {
private:
  // queries the hash for the security item in question
  SecurityItem GetSecurityItem(const std::string& guid, const SecuritySetting ss);
  void SetSecurityItem(const string& guid, const SecuritySetting ss,
					   const SecurityItem & s);

  // queries the user about a security violation
  // updates the security settings if the user requests it
  void SecurityViolation(SecurityItem & s, const bool val);
  void SecurityViolation(SecurityItem & s, const float val);
  void SecurityViolation(SecurityItem & s, const string& val);
  void SaveSecurityHash();

  void QuitLocation();

  std::string m_SecurityFile;
  SecurityHash * m_pSecuritySettings;
  nsCOMPtr<nsISupports> m_loc;

public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHISECURITYMANAGER

	   SecurityManager();
  virtual ~SecurityManager();

};


#endif // _SECURITYMANAGER_H_
