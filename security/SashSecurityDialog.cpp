#include "SashSecurityDialog.h"
#include "sash_constants.cpp"
#include <gnome.h>
#include <math.h>

SashSecurityDialog * SashSecurityDialog::s_ActiveDialog = NULL;

SashSecurityDialog::SashSecurityDialog() {
}

void SashSecurityDialog::OnBoolChanged(GtkToggleButton * button, char * key) {
	 SecurityItem item = s_ActiveDialog->m_working.Lookup(key);
	 assert(item.m_Type == ST_BOOL);
	 item.m_Bool = gtk_toggle_button_get_active(button);
	 s_ActiveDialog->m_working.Insert(key, item);
}

void SashSecurityDialog::OnNumberChanged(GtkEditable * entry, char * key) {
	 SecurityItem item = s_ActiveDialog->m_working.Lookup(key);
	 assert(item.m_Type == ST_NUMBER);
	 char * text = gtk_editable_get_chars(entry, 0, -1);
	 item.m_Number = atoi(text);	 
	 s_ActiveDialog->m_working.Insert(key, item);
	 g_free(text);
}

void SashSecurityDialog::OnStringChanged(GtkEditable * entry, char * key) {
	 SecurityItem item = s_ActiveDialog->m_working.Lookup(key);
	 assert(item.m_Type == ST_STRING);
	 char * text = gtk_editable_get_chars(entry, 0, -1);
	 item.m_String = text;	 
	 s_ActiveDialog->m_working.Insert(key, item);
	 g_free(text);
}

void SashSecurityDialog::OnStringEnumChanged(GtkEditable * entry, char * key) {
	 SecurityItem item = s_ActiveDialog->m_working.Lookup(key);
	 assert(item.m_Type == ST_STRING_ENUM);
	 char * text = gtk_editable_get_chars(entry, 0, -1);
	 for (unsigned int i = 0; i < item.m_StringVals.size(); i++)
		  if (item.m_StringVals[i] == text)
			   item.m_StringIndex = i;
	 s_ActiveDialog->m_working.Insert(key, item);
	 g_free(text);
}


void SashSecurityDialog::GetGroupName(vector<sec_item> &items,
									  string &ret){
	 ret = "";
	 vector<sec_item>::iterator ai = items.begin(),
		  bi = items.end();
	 while (ai != bi){
		 if (ai->second.m_ID == -1){
			  ret = ai->second.m_DescriptiveName;
			  items.erase(ai);
			  return;
		 }
		  ++ai;
	 }
}


void SashSecurityDialog::GetSecurityGroups(vector<string> & groups) {
  sec_hash::iterator a = m_working.GetHash().begin(), 
	b = m_working.GetHash().end();

  while (a != b) {
	   if (find(groups.begin(), groups.end(), a->second.m_Parent) 
		   == groups.end()){
			groups.push_back(a->second.m_Parent);
	   }
	   ++a;
  }
}

void SashSecurityDialog::GetGroupSettings(const string & group,
										  vector<sec_item> & settings) {
  sec_hash::iterator a = m_working.GetHash().begin(), 
	b = m_working.GetHash().end();
  while (a != b) {
	   if (a->second.m_Parent == group) {
			sec_item item(a->first, a->second);
			settings.push_back(item);
	   }
	++a;
  }
}

// used to draw the list of custom security string array setting.
void SashSecurityDialog::SetComboEntries(GtkWidget * combo, const SecurityItem &item){
	 GList *items = NULL;
	 
	 DEBUGMSG(ssd, "constructing new item list\n");
	 for (unsigned int i = 0; i < item.m_StringVals.size(); i++) {
		  items = g_list_append(items, (void *)item.m_StringVals[i].c_str());
	 }
	 gtk_combo_set_popdown_strings(GTK_COMBO(combo), items);
}

void SashSecurityDialog::SetupGroup(GtkWidget * page_box, vector<sec_item> & items) {
	 GtkWidget * item_widget;
	 DEBUGMSG(ssd, "setting up group, %d items\n", items.size());

	 sort(items.begin(), items.end());

	 for (unsigned int i = 0; i < items.size(); i++) {
		  switch (items[i].second.m_Type) {
		  case ST_BOOL: 
		  {
			   item_widget = gtk_check_button_new_with_label(items[i].second.m_DescriptiveName.c_str());
			   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(item_widget), 
											items[i].second.m_Bool);
			   gtk_signal_connect(GTK_OBJECT(item_widget), "toggled",
								  GTK_SIGNAL_FUNC(SashSecurityDialog::OnBoolChanged),
								  (void *)items[i].first.c_str());
			   break;
		  }
		  case ST_NUMBER:
		  {
			   item_widget = gtk_hbox_new(TRUE, 0);

			   GtkWidget * label = gtk_label_new(items[i].second.m_DescriptiveName.c_str());
			   gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
			   gtk_widget_show(label);
			   gtk_box_pack_start(GTK_BOX(item_widget), label, TRUE, TRUE, 4);

			   GtkObject * spin_adj = gtk_adjustment_new(items[i].second.m_Number, 
														 -HUGE_VAL, HUGE_VAL, 1.0, 10.0, 10.0);
			   GtkWidget * spin_button = gtk_spin_button_new(GTK_ADJUSTMENT(spin_adj), 1.0, 2);
			   gtk_signal_connect(GTK_OBJECT(spin_button), "changed",
								  GTK_SIGNAL_FUNC(SashSecurityDialog::OnNumberChanged),
								  (void *)items[i].first.c_str());
  			   gtk_widget_show(spin_button);
			   gtk_box_pack_end(GTK_BOX(item_widget), spin_button, TRUE, TRUE, 4);
			   break;			   
		  }
		  case ST_STRING:
		  {
			   item_widget = gtk_hbox_new(TRUE, 0);

			   GtkWidget * label = gtk_label_new(items[i].second.m_DescriptiveName.c_str());
			   gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
			   gtk_widget_show(label);
			   gtk_box_pack_start(GTK_BOX(item_widget), label, TRUE, TRUE, 4);

			   GtkWidget * string_entry = gtk_entry_new();
			   gtk_entry_set_text(GTK_ENTRY(string_entry), items[i].second.m_String.c_str());
			   gtk_signal_connect(GTK_OBJECT(string_entry), "changed",
								  GTK_SIGNAL_FUNC(SashSecurityDialog::OnStringChanged),
								  (void *)items[i].first.c_str());
			   gtk_widget_show(string_entry);
			   gtk_box_pack_end(GTK_BOX(item_widget), string_entry, TRUE, TRUE, 4);
			   break;
		  }
		  case ST_STRING_ENUM:
		  {
			   item_widget = gtk_hbox_new(TRUE, 0);

			   GtkWidget * label = gtk_label_new(items[i].second.m_DescriptiveName.c_str());
			   gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
			   gtk_widget_show(label);
			   gtk_box_pack_start(GTK_BOX(item_widget), label, TRUE, TRUE, 4);

			   GtkWidget * combo = gtk_combo_new();
			   SetComboEntries(combo, items[i].second);
			   gtk_combo_set_value_in_list(GTK_COMBO(combo), TRUE, FALSE);
			   int str_index = items[i].second.m_StringIndex;
			   if (str_index >= 0 && str_index < (int)items[i].second.m_StringVals.size())
					gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(combo)->entry), 
									   items[i].second.m_StringVals[str_index].c_str());
			   gtk_signal_connect(GTK_OBJECT(GTK_COMBO(combo)->entry), "changed",
								  GTK_SIGNAL_FUNC(SashSecurityDialog::OnStringEnumChanged),
								  (void *)items[i].first.c_str());
			   gtk_widget_show(combo);
			   gtk_box_pack_end(GTK_BOX(item_widget), combo, TRUE, TRUE, 4);
			   break;
		  }
		  default:
			   item_widget = NULL;
			   printf("Unknown security type: %d (setting %s)\n", items[i].second.m_Type,
					  items[i].second.m_DescriptiveName.c_str());
		  }
		  if (item_widget) {
			   gtk_widget_show(item_widget);
			   gtk_box_pack_start(GTK_BOX(page_box), item_widget, FALSE, FALSE, 0);
		  }
	 }
}

void SashSecurityDialog::SetupDialog(GtkWindow * parent) {
	 m_dialog = gnome_dialog_new("Security", GNOME_STOCK_BUTTON_OK, 
										   GNOME_STOCK_BUTTON_CANCEL, NULL);
	 if (parent)
		  gnome_dialog_set_parent(GNOME_DIALOG(m_dialog), parent);

	 GtkWidget * notebook = gtk_notebook_new();
	 gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (m_dialog)->vbox), notebook, TRUE, TRUE, 0);
	 vector<string> groups;
	 GetSecurityGroups(groups);
	 for (unsigned int i = 0; i < groups.size(); i++) { 
		  DEBUGMSG(ssd, "Security Group: %s\n", groups[i].c_str());
		  GtkWidget * page_box = gtk_vbox_new(FALSE, 4);
		  gtk_container_set_border_width(GTK_CONTAINER(page_box), 4);
		  gtk_widget_show(page_box);
		  vector<sec_item> items;
		  GetGroupSettings(groups[i], items);
		  string grpname;
		  GetGroupName(items, grpname);

		  SetupGroup(page_box, items);

		  GtkWidget * page_label = gtk_label_new(grpname.c_str());
		  gtk_widget_show(page_label);
		  gtk_notebook_prepend_page(GTK_NOTEBOOK(notebook), page_box, page_label);
	 }
	 gtk_notebook_set_page(GTK_NOTEBOOK(notebook), 0);
	 gtk_widget_show(notebook);
	 gtk_widget_show(m_dialog);
}

bool SashSecurityDialog::Run(SecurityHash & settings, GtkWindow * parent) {
  s_ActiveDialog = this;
  m_working = settings;

  SetupDialog(parent);
  int OK = (gnome_dialog_run_and_close(GNOME_DIALOG(m_dialog)) == 0);
  if (OK) {
	   settings = m_working;
  }

  s_ActiveDialog = NULL;
  return OK;
}
