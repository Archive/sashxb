package Entry;
use Data::Dumper;
sub new {
  my ($proto, $u, $p, $d, $a) = @_;
  my $self = {};
  bless ($self, ref($proto) || $proto);
  $self->Url($u) if $u;
  $self->Platforms($p) if ref($p);
  $self->Distributions($d) if ref($d);
  $self->Architectures($a) if ref($a);
  return $self;

}
sub Matches {
  my ($self, $p, $d, $a) = @_;
  return ($self->{Platforms}{$p} &&
		  $self->{Distributions}{$d} &&
		  $self->{Architectures}{$a});
}

sub Overlaps {
  my ($self, $o) = @_;
  my @p, @d, @a;
  foreach ($o->Platforms()) {
	push @p, $_ if $self->{Platforms}{$_};
  }
  foreach ($o->Distributions()) {
	push @d, $_ if $self->{Distributions}{$_};
  }
  foreach ($o->Architectures()) {
	push @a, $_ if $self->{Architectures}{$_};
  }
  return (scalar @p && scalar @d && scalar @a,
		  \@p, \@d, \@a);
}

sub Url {
  my ($self, $p) = @_;
  $self->{Url} = $p if ($p);
  return $self->{Url};
}

sub Platforms ($;\@) {
  my ($self, $p) = @_;
  $self->{Platforms} = {map {$_ => 1} @$p} if (defined $p);
  return keys %{$self->{Platforms}};
}

sub Distributions {
  my ($self, $p) = @_;
  $self->{Distributions} = {map {$_ => 1} @$p} if (defined $p);
  return keys %{$self->{Distributions}};
}

sub Architectures {
  my ($self, $p) = @_;
  $self->{Architectures} = {map {$_ => 1} @$p} if (defined $p);
  return keys %{$self->{Architectures}};
}

sub AddPlatforms {
  my ($self, @p) = @_;
  foreach(@p) {
	$self->{Platforms}{$_} = 1;
  }
}

sub AddDistributions {
  my ($self, @p) = @_;
  foreach(@p) {
	$self->{Distributions}{$_} = 1;
  }
}

sub AddArchitectures {
  my ($self, @p) = @_;
  foreach(@p) {
	$self->{Architectures}{$_} = 1;
  }
}

1;
