package Common;
require Exporter;
@ISA = (Exporter);
@EXPORT = qw(Platforms Distributions Architectures DataFilename Normalize);

# inline these babies
sub Platforms () {
  ("Linux");
}

sub Distributions () {
  ("RedHat", "Debian", "TurboLinux", "Mandrake", "SuSE", "YellowDog", Others);
}

sub Architectures () {
  ("x86");
}

sub DataFilename () {
  "locator.dat";
}

sub Others () {
  "Others";
}


sub Normalize($$$) {
  my @a = @_;
  if (! grep { $_ eq $a[1] } Distributions) {
	$a[1] = Others;
  }
  return @a;
}

1;
