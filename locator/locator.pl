#!/usr/bin/perl

use Manager;
use Common;
use CGI qw(param);

my $manager = new Manager(DataFilename);

my ($g, $p, $d, $a) = (param("guid"), param("platform"), param("distribution"), param("architecture"));
my $curtime = localtime(time);

open(out, ">>locator.log");
print out join("::", ($ENV{REMOTE_ADDR}, $g, $p, $d, $a));
unless ($g && $p && $a && $d) {
  print out "\n";
  die "Bad CGI args!";
}
my $u = $manager->GetUrl($g, $p, $d, $a);
print out "::".$u."::".$curtime."\n";

print "Content-type: text/html\n\n";
print $u;

exit;
