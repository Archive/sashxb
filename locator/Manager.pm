package Manager;
use Data::Dumper;
use Component;
use Common;

sub new {
  my ($proto, $filename) = @_;
  my $self;
  if ($filename) {
	# load in from Data::Dumper
	open("in", $filename);
	flock(in, LOCK_SH);
	my $tmp = $/; 
	undef $/;
	my $l = <in>;
	eval $l;
	flock(in, LOCK_UN);
	close "in";
	$/ = $tmp;
	$self = {} unless $self;
	$self->{Filename} = $filename;
  } else {
	$self = {};
  }
  bless ($self, ref($proto) || $proto);
  return $self;
}

sub SaveFile {
  my ($self, $f) = @_;
  $self->{Filename} = $f || $self->{Filename};
  print("Can't save: no filename provided!"), exit unless $self->{Filename};
  $Data::Dumper::Indent = 1;
  $Data::Dumper::Terse = 0;
  open("out", ">".$self->{Filename});
  flock(out, LOCK_EX);
  print out Data::Dumper->Dump([$self], ["self"]);
  flock(in, LOCK_UN);
  close "out";
}

sub AddComponent {
  # p, d, a are refs to arrays
  my ($self, $component) = @_;
  return $self->{Components}{$component->GUID} = $component;
}

sub RemoveComponent {
  my ($self, $guid) = @_;
  delete $self->{Components}{$guid};
}

sub Components {
  my ($self, @p) = @_;
  $self->{Components} = {map {$_->GUID() => $_} @p} if (scalar @p);
  return values %{$self->{Components}};
}

sub GetComponent {
  my ($self, $guid) = @_;
  return $self->{Components}{$guid};
}

sub GetUrl {
  my ($self, $guid, $plat, $dist, $arch) = @_;
  ($plat, $dist, $arch) = Normalize($plat, $dist, $arch);
  return $self->{Components}{$guid}->GetUrl($plat, $dist, $arch)
	if $self->{Components}{$guid};
  my @possibilities =
	grep { $_->[0] =~ /^$guid/i }
	  map { [$_->Name, $_->GUID] }
		values %{$self->{Components}};
  return "Error: Could not find component $guid!"
	unless (scalar @possibilities);
  return $self->{Components}{$possibilities[0]->[1]}->GetUrl($plat, $dist, $arch)
	if (scalar @possibilities == 1);

  my @exact = grep { length($_->[0]) == length($guid) } @possibilities;
  return $self->{Components}{$exact[0]->[1]}->GetUrl($plat, $dist, $arch)
	if (scalar @exact == 1);

  return "Error: Ambiguous component $guid matches " .
	join(", ", map { '"' . $_->[0] . '"' } @possibilities);
}

1;
