#!/usr/bin/perl

use Manager;
use Component;
use Entry;
use Data::Dumper;
use Common;
use CGI ':cgi-lib';

my $color="#dddddd";
my $manager = new Manager(DataFilename);
my %params;
%params = Vars;
my %functions = (
				 delete_entry => \&DeleteEntry,
				 edit_entry => \&EditEntry,
				 add_entry => \&AddEntry,
				 delete_component => \&DeleteComponent,
				 edit_component => \&EditComponent,
				 add_component => \&AddComponent,
				 save_entry => \&SaveEntry
				 );

print <<top;
Content-type: text/html


<html><head><title>Locator Configuration</title></head><body>
<font size=+2 face="verdana, arial"><b>Locator Configuration</b></font>
<hr noshade>
top

if (! defined($functions{$params{"action"}})) {
  GenerateManagerHTML($manager);
} else {
  $functions{$params{"action"}}->();
}
print "</body></html>";


sub Error {
  my ($e) = @_;
  print "Error: $e!";
  exit;
}

sub GetComponent {
  my $g = $params{"component_guid"};
  my $component = $manager->GetComponent($g);
  return $component || Error("No component found");
}

sub GetEntry {
  my ($component) = @_;
  my $u = $params{"entry_url"};
  my $entry = $component->GetEntry($u);
  return $entry || Error("No entry found");
}

sub DeleteEntry {
  my $component = GetComponent;
  my $entry = GetEntry($component);
  $component->RemoveEntry($entry->Url);
  $manager->SaveFile();
  GenerateComponentHTML($component);
}

sub EditEntry {
  my $component = GetComponent;
  my $entry = GetEntry($component);
  GenerateEntryHTML($component, $entry);
}

sub AddEntry {
  my $component = GetComponent;
  my $entry = new Entry("", [Platforms], [Distributions], [Architectures]);
  GenerateEntryHTML($component, $entry);
}

sub DeleteComponent {
  my $component = GetComponent;
  $manager->RemoveComponent($component->GUID);
  $manager->SaveFile();
  GenerateManagerHTML($manager);
}

sub EditComponent {
  my $component = GetComponent;
  GenerateComponentHTML($component);
}

sub AddComponent {
  my $g = $params{"component_guid"};
  my $n = $params{"component_name"};
  my $component = new Component($n, $g);
  $manager->AddComponent($component);
  $manager->SaveFile();
  my $entry = new Entry("", [Platforms], [Distributions], [Architectures]);
  GenerateEntryHTML($component, $entry);
}

sub SaveEntry {
  my $component = GetComponent;
  my $eu = $params{"entry_url"};
  my $new_url = $params{"url"};
  Error("Please specify a URL!") unless $new_url;
  my $entry = $component->GetEntry($eu);
  if ($entry) {
	$component->RemoveEntry($eu);
  } elsif (! $entry) {
	$entry = new Entry();
  }
  $entry->Url($new_url);
  $entry->Platforms([split('\0', $params{'Platforms'})]);
  $entry->Distributions([split('\0', $params{'Distributions'})]);
  $entry->Architectures([split('\0', $params{'Architectures'})]);

  my (@res) = $component->AddEntry($entry);
  if (scalar @res) {
	Error("This entry conflicts with '$res[0]' w.r.t. ".
		  join(", ", map {"[".(join(", ", @$_))."]"} @res[1..3])." -- please reconfigure it!");
  }
  $manager->SaveFile();
  GenerateComponentHTML($component);
}

sub GenerateManagerHTML {
  my ($m) = @_;
  print <<table_top;
<script language="javascript">
function del_component(name, guid) {
  if (confirm("Are you sure you want to delete the '" + name + "' component?")) {
	set_n_go("delete_component", guid);
  }
}

function set_n_go(val, guid) {
  document.entries.action.value = val;
  document.entries.component_guid.value = guid;
  document.entries.submit();
}

</script>
<form name="entries" method="post">
<input type="hidden" name="action">
<input type="hidden" name="component_guid" value="$cg">

<table cellpadding=3><tr>
<td><b>Component Name</b></td>
<td><b>GUID</b></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
table_top
  my $col;
  foreach(sort {$a->Name cmp $b->Name} $m->Components) {
	$col = ($col ? "" : $color);
	my ($n, $g) = ($_->Name, $_->GUID);
	print <<line;
<tr>
  <td bgcolor="$col">$n</td>
  <td bgcolor="$col">$g</td>
  <td bgcolor="$col"><input type="button" onclick="set_n_go('edit_component', '$g');" value="Edit..."></td>
  <td bgcolor="$col"><input type="button" onclick="del_component('$n', '$g');" value="Delete..."></td>
</tr>
line
  }
  print <<table_bottom;
</table>
<hr noshade>
Name: <input type="text" size=20 name="component_name"> &nbsp;
GUID: <input type="text" size=40 name="guid"> &nbsp;
<input type="button" onclick="set_n_go('add_component', document.entries.guid.value);" 
  value="Add New Component..."><p>
</form>
table_bottom

}

sub GenerateComponentHTML {
  my ($c) = @_;
  my $cg = $c->GUID;
  my $cn = $c->Name;
  print <<table_top;
<font size=+1><b>$cn</b> $cg</font>
<script language="javascript">
function del_entry(url) {
  if (confirm("Are you sure you want to delete the '" + url + "' entry?")) {
	set_n_go("delete_entry", url);
  }
}

function set_n_go(val, url) {
  document.entries.action.value = val;
  document.entries.entry_url.value = url;
  document.entries.submit();
}

</script>
<form name="entries" method="post">
<input type="hidden" name="entry_url">
<input type="hidden" name="action">
<input type="hidden" name="component_guid" value="$cg">

<table cellpadding=3><tr>
<td><b>URL</b></td>
<td><b>Platforms</b></td>
<td><b>Distributions</b></td>
<td><b>Architectures</b></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
table_top
  my $col;
  foreach($c->Entries) {
	$col = ($col ? "" : $color);
	my ($u, $p, $d, $a) = ($_->Url, join(", ", $_->Platforms),
						   join(", ", $_->Distributions), join(", ", $_->Architectures));
	print <<line;
<tr>
  <td bgcolor="$col"><a href="$u">$u</a></td>
  <td bgcolor="$col">$p</td>
  <td bgcolor="$col">$d</td>
  <td bgcolor="$col">$a</td>
  <td bgcolor="$col"><input type="button" onclick="set_n_go('edit_entry', '$u');" value="Edit..."></td>
  <td bgcolor="$col"><input type="button" onclick="del_entry('$u');" value="Delete..."></td>
</tr>
line
  }
  print <<table_bottom;
</table><p>
<input type="button" onclick="set_n_go('add_entry', '');" value="Add New Entry...">
<hr noshade>
<a href="javascript:set_n_go('', '')">View All Components...</a>

</form>
table_bottom

}

sub GenerateEntryHTML {
  my ($c, $e) = @_;
  my $url = $e->Url;
  my $cg = $c->GUID;
  my $cn = $c->Name;
  print <<top;
<font size=+1><b>$cn</b> $cg</font>
<p><script language="javascript">
function set_n_go(val) {
  document.entries.action.value = val;
  document.entries.submit();
}

</script>
<form method="post" name="entries">
<input type="hidden" name="action">
<input type="hidden" name="entry_url" value="$url">
<b>URL:</b> <input type="text" size=80 name="url" value="$url">
<p>
<table>
<tr><td width='33%' valign='top'>
top

  DisplayList("Platforms", [Platforms], [$e->Platforms]);
  print "</td><td width='33%' valign='top'>";
  DisplayList("Distributions", [Distributions], [$e->Distributions]);
  print "</td><td width='33%' valign='top'>";
  DisplayList("Architectures", [Architectures], [$e->Architectures]);
print <<bottom;
</td></tr></table>
<input type="hidden" name="component_guid" value="$cg">

<input type="button" onclick="set_n_go('save_entry')" value="Save Entry"><p>
<hr noshade>
<a href="javascript:set_n_go('edit_component')">View '$cn'...</a> &nbsp; | &nbsp; 
<a href="javascript:set_n_go('')">View Component Manager</a>

</form>
bottom

}

sub DisplayList {
  my ($name, $all, $good) = @_;
  print "<font size=+1><b>$name</b></font><br>\n";
  my %data;
  foreach (@$all) {
	$data{$_} = 0;
  }
  foreach (@$good) {
	$data{$_} = 1 if exists $data{$_};
  }
  foreach (sort keys %data) {
	print "<input name='$name' value='$_' type='checkbox'", ($data{$_} ? " checked" : ""), "> $_<br>\n";
  }
}
