package Component;
use Entry;

sub new {
  my ($proto, $name, $guid) = @_;
  my $self = {};
  bless ($self, ref($proto) || $proto);
  $self->Name($name) if $name;
  $self->GUID($guid) if $guid;
  return $self;
}

sub Name {
  my ($self, $p) = @_;
  $self->{Name} = $p if ($p);
  return $self->{Name};
}

sub GUID {
  my ($self, $p) = @_;
  $self->{GUID} = $p if ($p);
  return $self->{GUID};
}

sub AddEntry {
  # p, d, a are refs to arrays
  my ($self, $entry) = @_;
  my $e = $self->{Entries}{$entry->Url};
  if ($e) {
	$e->AddPlatforms($entry->Platforms);
	$e->AddDistributions($entry->Distributions);
	$e->AddArchitectures($entry->Architectures);
  } else {
	$self->{Entries}{$entry->Url} = $e = $entry;
  }
  # check for conflicts
  foreach (values %{$self->{Entries}}) {
	next if $e == $_;
	my @res = $e->Overlaps($_);
	if (shift @res) {
	  unshift @res, $_->Url;
	  # found a conflict
	  return (@res);
	}
  }
  return ();
}

sub RemoveEntry {
  my ($self, $url) = @_;
  delete $self->{Entries}{$url};
}

sub GetEntry {
  my ($self, $url) = @_;
  return $self->{Entries}{$url};
}

sub Entries {
  my ($self, @p) = @_;
  $self->{Entries} = {map {$_ => 1} @p} if (scalar @p);
  return values %{$self->{Entries}};
}

sub GetUrl {
  my ($self, $p, $d, $a) = @_;
  foreach (values %{$self->{Entries}}) {
	return $_->Url() if ($_->Matches($p, $d, $a));
  }
  return "Error: Component ".$self->Name." does not exist for your type of system: $d $p on $a!";
}

1;
